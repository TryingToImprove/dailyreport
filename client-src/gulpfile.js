var gulp = require("gulp"),
    browserSync = require("browser-sync"),
    browserify = require("browserify"),
    source = require('vinyl-source-stream'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat');

var libs = [
  'react/addons'
];

// Handles the javascript
gulp.task("javascript-application", function() {
    var b = browserify({
            debug: true,
            transform: [
                ["babelify"]
            ],
            external: libs
        });
    
    b.add("./src/scripts/app.js");
    
    return b.bundle()      
        .pipe(source('app.js'))
        .pipe(gulp.dest('dist/scripts'));  
});

// Copy the vendor javascript to the dist-folder
gulp.task("javascript-vendor", function() {
    return gulp.src([
                "./src/scripts/vendor/**/jquery-2.1.3.min.js",
                "./src/scripts/vendor/**/!(jquery-2.1.3.min.js)*.js"
            ])
            .pipe(concat("vendors.js"))
            .pipe(gulp.dest("dist/scripts"));
});

// Copy the HTML to the dist-folder
gulp.task("pages", function() {
    return gulp.src("./src/index.html")
            .pipe(gulp.dest("dist"));
});

// Copy the content to the dist-folder
gulp.task("content", function() {
    return gulp.src("./src/content/**/*.*")
            .pipe(gulp.dest("dist/content"));
});

// Handles the styling
gulp.task("styles", function() {
    return gulp.src(['./src/styles/*.scss', './src/styles/**/*.css'])            
            .pipe(sass({
                errLogToConsole: true
            }))
            .pipe(concat("main.css"))
            .pipe(gulp.dest('dist/styles'));
});

gulp.task("default", ["javascript-application", "javascript-vendor", "pages", "content", "styles"], function() {    
    browserSync({
        server: {
            baseDir: "./dist/"
        },
        online: false
    });
    
    gulp.watch("src/scripts/vendor/**/*.js", ["javascript-vendor", browserSync.reload]);
    gulp.watch(["src/scripts/**/*.js", "!src/scripts/vendor/**/*.js"], ['javascript-application', browserSync.reload]); 
    gulp.watch("src/index.html", ['pages', browserSync.reload]);
    gulp.watch("src/content/**/*.*", ["content", browserSync.reload]);
    gulp.watch("src/styles/**.*", ['styles', browserSync.reload]);
});