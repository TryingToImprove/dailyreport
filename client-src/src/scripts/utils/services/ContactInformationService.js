// Normalization
import { normalize, arrayOf } from "normalizr";
import { ContactInformation } from "./Schemas";

import Condition from "../Condition";
import ApplicationConfiguration from "../../ApplicationConfiguration";
import WebClient from "../WebClient";

export default {
    patient: {
        findAll: function(patientId) {   
            Condition.requires(patientId).isNotNullOrUndefined()
                                         .isANumber()
                                         .isGreaterThan(0);
            
            return WebClient.request(ApplicationConfiguration.serverUrl + "Api/ContactInformation/GetByPatientId", {
                    type: "GET",
                    data: {
                        patientId: patientId
                    }
                })
                .then(function(response){
                    return normalize(response, arrayOf(ContactInformation));
                });;
        },
        add: function(patientId, label, value) {
            Condition.requires(patientId).isNotNullOrUndefined()
                                         .isANumber()
                                         .isGreaterThan(0);

            Condition.requires(label).isNotNullOrUndefined()
                                     .isNotEmptyOrWhitespace();

            Condition.requires(value).isNotNullOrUndefined()
                                     .isNotEmptyOrWhitespace();
            
            return WebClient.request(ApplicationConfiguration.serverUrl + "Api/ContactInformation/PatientAdd?patientId=" + patientId, {
                    type: "POST",
                    data: {
                        type: label,
                        value: value
                    }
                })
                .then(function(response){
                    return normalize(response, ContactInformation);
                });
        }
    },
    getTypes: function() {        
        return WebClient.request(ApplicationConfiguration.serverUrl + "Api/ContactInformation/GetTypes", {
            type: "GET"
        });
    },
    remove: function(contactInformationId) {
        Condition.requires(contactInformationId).isNotNullOrUndefined()
                                                .isANumber()
                                                .isGreaterThan(0);
        
        return WebClient.request(ApplicationConfiguration.serverUrl + "Api/ContactInformation/Remove?contactInformationId=" + contactInformationId, {
                type: "DELETE"
            })
            .then(function(response){
                return normalize(response, ContactInformation);
            });
    }
};