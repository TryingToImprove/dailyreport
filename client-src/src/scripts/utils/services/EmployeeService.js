(function () {
    "use strict";
    
    var Condition = require("../Condition"),
        ApplicationConfiguration = require("../../ApplicationConfiguration"),
        WebClient = require("../WebClient");
    
    module.exports = {
        findAll: function() {        
            return WebClient.request(ApplicationConfiguration.serverUrl + "/Api/Employee/FindAll", {
                type: "GET"
            });
        },
        get: function(employeeId) { 
            Condition.requires(employeeId).isNotNullOrUndefined()
                                         .isANumber()
                                         .isGreaterThan(0);
                    
            return WebClient.request(ApplicationConfiguration.serverUrl + "/Api/Employee/Get/" + employeeId, {
                type: "GET"
            });
        }
    };
}());