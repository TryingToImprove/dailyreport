import { normalize, arrayOf } from 'normalizr';
import { Medication } from './Schemas';

var Condition = require("../Condition"),
    ApplicationConfiguration = require("../../ApplicationConfiguration"),
    WebClient = require("../WebClient"),
    Service = {
    search: function(query){
        Condition.requires(query).isNotNullOrUndefined()
                                 .isNotEmptyOrWhitespace();

        return WebClient.request(ApplicationConfiguration.serverUrl + "/Api/Medication/Search", {
                type: "GET",
                data: {
                    query: query
                }
            })
            .then(function(response) {
                return normalize(response, arrayOf(Medication));
            });
    }
};

export default Service;