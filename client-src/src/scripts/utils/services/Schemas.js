import { arrayOf, Schema } from "normalizr";

var Employee = new Schema("employees");

var Medication = new Schema("medications");

var MedicationHistoryItem = new Schema("historyItems");
MedicationHistoryItem.define({
    employee: Employee,
    medication: Medication
});

var ContactGroup = new Schema("contactGroups");

var Contact = new Schema("contacts");

var ContactInformation = new Schema("contactInformations");

var Address = new Schema("addresses");

export default {
    Employee: Employee,

    Medication: Medication,
    MedicationHistoryItem: MedicationHistoryItem,

    Contact: Contact,
    ContactGroup: ContactGroup,

    ContactInformation: ContactInformation,

    Address: Address
};