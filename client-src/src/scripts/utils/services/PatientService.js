import { normalize, arrayOf } from "normalizr";
import { MedicationHistoryItem, Contact, ContactGroup, Address } from "./Schemas";

var Condition = require("../Condition"),
    ApplicationConfiguration = require("../../ApplicationConfiguration"),
    WebClient = require("../WebClient");

module.exports = {
    findAll: function() {
        return WebClient.request(ApplicationConfiguration.serverUrl + "/Api/Patient/FindAll", {
            type: "GET"
        });
    },
    get: function(patientId) {
        Condition.requires(patientId).isNotNullOrUndefined()
                                     .isANumber()
                                     .isGreaterThan(0);

        return WebClient.request(ApplicationConfiguration.serverUrl + "/Api/Patient/Get/" + patientId, {
            type: "GET"
        });
    },
    update: function(patientId, firstname, lastname, birthDate, securityNumber) {
        Condition.requires(patientId).isNotNullOrUndefined()
                                     .isANumber()
                                     .isGreaterThan(0);

        Condition.requires(firstname).isNotNullOrUndefined()
                                    .isNotEmptyOrWhitespace();

        Condition.requires(lastname).isNotNullOrUndefined()
                                    .isNotEmptyOrWhitespace();

        Condition.requires(birthDate).isNotNullOrUndefined()
                                    .isNotEmptyOrWhitespace();

        return WebClient.request(ApplicationConfiguration.serverUrl + "/Api/Patient/Update/" + patientId, {
            type: "PUT",
            data: {
                firstname: firstname,
                lastname: lastname,
                birthDate: birthDate,
                securityNumber: securityNumber
            }
        });
    },
    create: function(firstname, lastname, contactInformations, contactPersons) {
        Condition.requires(firstname).isNotNullOrUndefined()
                                    .isNotEmptyOrWhitespace();

        Condition.requires(lastname).isNotNullOrUndefined()
                                    .isNotEmptyOrWhitespace();

        return WebClient.request(ApplicationConfiguration.serverUrl + "/Api/Patient/Create", {
            type: "POST",
            data: {
                firstname: firstname,
                lastname: lastname,
                contactInformations: contactInformations,
                contactPersons: contactPersons
            }
        });
    },
    fetchPosts: function(patientId) {
        Condition.requires(patientId).isNotNullOrUndefined()
                                     .isANumber()
                                     .isGreaterThan(0);

        return WebClient.request(ApplicationConfiguration.serverUrl + "/Api/Logbook/GetByPatientId", {
            type: "GET",
            data: {
                patientId: patientId
            }
        });
    },
    contactPersons: {
        findAll: function(patientId) {
            Condition.requires(patientId).isNotNullOrUndefined()
                                         .isANumber()
                                         .isGreaterThan(0);

            return WebClient.request(ApplicationConfiguration.serverUrl + "/Api/ContactPerson/FindAll", {
                type: "GET",
                data: {
                    patientId: patientId
                }
            });
        },
        findCurrents: function(patientId) {
            Condition.requires(patientId).isNotNullOrUndefined()
                                         .isANumber()
                                         .isGreaterThan(0);

            return WebClient.request(ApplicationConfiguration.serverUrl + "/Api/ContactPerson/FindCurrents", {
                type: "GET",
                data: {
                    patientId: patientId
                }
            });
        },
        add: function(patientId, employeeId, completePrevious) {
            Condition.requires(patientId).isNotNullOrUndefined()
                                         .isANumber()
                                         .isGreaterThan(0);


            Condition.requires(employeeId).isNotNullOrUndefined()
                                         .isANumber()
                                         .isGreaterThan(0);

            Condition.requires(completePrevious).isNotNullOrUndefined();

            return WebClient.request(ApplicationConfiguration.serverUrl + "/Api/ContactPerson/Add?patientId=" + patientId, {
                type: "POST",
                data: {
                    employeeId: employeeId,
                    completePrevious: completePrevious
                }
            });
        },
        complete: function(relationshipId) {
            Condition.requires(relationshipId).isNotNullOrUndefined()
                                         .isANumber()
                                         .isGreaterThan(0);

            return WebClient.request(ApplicationConfiguration.serverUrl + "/Api/ContactPerson/Complete", {
                type: "PUT",
                data: {
                    id: relationshipId
                }
            });
        }
    },
    timeline: {
        findAll: function(patientId) {
            Condition.requires(patientId).isNotNullOrUndefined()
                                         .isANumber()
                                         .isGreaterThan(0);

            return WebClient.request(ApplicationConfiguration.serverUrl + "/Api/Timeline/FindByPatient", {
                type: "GET",
                data: {
                    patientId: patientId
                }
            });
        }
    },

    contacts: {
        findAll: function(patientId) {
            Condition.requires(patientId).isNotNullOrUndefined()
                                         .isANumber()
                                         .isGreaterThan(0);

            return WebClient.request(ApplicationConfiguration.serverUrl + "/Api/Contacts/FindAll", {
                  type: "GET",
                  data: {
                      patientId: patientId
                  }
              })
              .then(function(response){
                  return normalize(response, arrayOf(Contact));
              });
        },

        findFrequent: function(patientId) {
            Condition.requires(patientId).isNotNullOrUndefined()
                                         .isANumber()
                                         .isGreaterThan(0);

            return WebClient.request(ApplicationConfiguration.serverUrl + "/Api/Contacts/FindFrequent", {
                  type: "GET",
                  data: {
                      patientId: patientId
                  }
              })
              .then(function(response){
                  return normalize(response, arrayOf(Contact));
              });
        },

        findByGroup: function(patientId, groupId) {
            Condition.requires(patientId).isNotNullOrUndefined()
                                         .isANumber()
                                         .isGreaterThan(0);

            Condition.requires(groupId).isNotNullOrUndefined()
                                         .isANumber()
                                         .isGreaterThan(0);

            return WebClient.request(ApplicationConfiguration.serverUrl + "/Api/Contacts/FindByGroup", {
                  type: "GET",
                  data: {
                      patientId: patientId,
                      groupId: groupId
                  }
              })
              .then(function(response){
                  return normalize(response, arrayOf(Contact));
              });
        },

        add: function(patientId, contact) {
            Condition.requires(patientId).isNotNullOrUndefined()
                                         .isANumber()
                                         .isGreaterThan(0);

            Condition.requires(contact).isNotNullOrUndefined();

            Condition.requires(contact.firstname).isNotNullOrUndefined()
                                                 .isNotEmptyOrWhitespace();

            Condition.requires(contact.lastname).isNotNullOrUndefined()
                                                .isNotEmptyOrWhitespace();

            Condition.requires(contact.groups).isNotNullOrUndefined()
                                              .isArray()
                                              .each(function(item) {
                                                    Condition.requires(item).isNotNullOrUndefined()
                                                                            .isNotEmptyOrWhitespace();
                                              });

            Condition.requires(contact.title).isNotNullOrUndefined().isNotEmptyOrWhitespace();

            return WebClient.request(ApplicationConfiguration.serverUrl + "/Api/Contacts/Add?patientId=" + patientId, {
                  type: "POST",
                  data: contact
              })
              .then(function(response){
                  return normalize(response, Contact);
              });
        },

        getGroups: function(patientId) {
            Condition.requires(patientId).isNotNullOrUndefined()
                                         .isANumber()
                                         .isGreaterThan(0);

            return WebClient.request(ApplicationConfiguration.serverUrl + "/Api/Contacts/GetGroups", {
                  type: "GET",
                  data: {
                      patientId: patientId
                  }
              })
              .then(function(response){
                  return normalize(response, arrayOf(ContactGroup));
              });;
        }
    },

    post: {
        create: function(patients, message) {
            Condition.requires(message).isNotNullOrUndefined()
                                       .isNotEmptyOrWhitespace(0);

            Condition.requires(patients).isNotNullOrUndefined()
                                              .isArray()
                                              .isNotEmpty()
                                              .each(function(item) {
                                                    Condition.requires(item).isNotNullOrUndefined()
                                                                          .isANumber(0)
                                                                          .isGreaterThan(0);
                                              });

            return WebClient.request(ApplicationConfiguration.serverUrl + "/Api/Post/Create", {
                type: "POST",
                data: {
                    patients: patients,
                    description: message
                }
            });
        }
    },

    address: {
      findAll(patientId) {
        Condition.requires(patientId).isNotNullOrUndefined()
                                     .isANumber()
                                     .isGreaterThan(0);

        return WebClient.request(ApplicationConfiguration.serverUrl + `/Api/Patient/${patientId}/Address/FindAll`, {
            type: "GET"
          })
          .then(function(response){
              return normalize(response, arrayOf(Address));
          });
      },
      findPrimary(patientId) {
        Condition.requires(patientId).isNotNullOrUndefined()
                                     .isANumber()
                                     .isGreaterThan(0);

        return WebClient.request(ApplicationConfiguration.serverUrl + `/Api/Patient/${patientId}/Address/FindPrimary`, {
            type: "GET"
          })
          .then(function(response){
              return normalize(response, arrayOf(Address));
          });
      },
      add(patientId, addressRequest) {
        Condition.requires(patientId).isNotNullOrUndefined()
                                     .isANumber()
                                     .isGreaterThan(0);

        Condition.requires(addressRequest.street).isNotNullOrUndefined()
                                   .isNotEmptyOrWhitespace(0);

        Condition.requires(addressRequest.houseNumber).isNotNullOrUndefined()
                                   .isNotEmptyOrWhitespace(0);

        Condition.requires(addressRequest.city).isNotNullOrUndefined()
                                   .isNotEmptyOrWhitespace(0);

        Condition.requires(addressRequest.postalCode).isNotNullOrUndefined()
                                   .isNotEmptyOrWhitespace(0);

        Condition.requires(addressRequest.isPrimary).isNotNullOrUndefined();

        Condition.requires(addressRequest.clearCurrentPrimary).isNotNullOrUndefined();

        return WebClient.request(ApplicationConfiguration.serverUrl + `/Api/Patient/${patientId}/Address/Add`, {
            type: "POST",
            data: addressRequest
          })
          .then(function(response){
              return normalize(response, Address);
          });
      },
      update(patientId, addressId, addressRequest) {
        Condition.requires(patientId).isNotNullOrUndefined()
                                     .isANumber()
                                     .isGreaterThan(0);

        Condition.requires(addressId).isNotNullOrUndefined()
                                     .isANumber()
                                     .isGreaterThan(0);

        Condition.requires(addressRequest).isNotNullOrUndefined();

        return WebClient.request(ApplicationConfiguration.serverUrl + `/Api/Patient/${patientId}/Address/Update/${addressId}`, {
            type: "PUT",
            data: addressRequest
          })
          .then(function(response){
              return normalize(response, Address);
          });
      },
      findById(patientId, addressId) {
        Condition.requires(patientId).isNotNullOrUndefined()
                                     .isANumber()
                                     .isGreaterThan(0);

        Condition.requires(addressId).isNotNullOrUndefined()
                                     .isANumber()
                                     .isGreaterThan(0);

        return WebClient.request(ApplicationConfiguration.serverUrl + `/Api/Patient/${patientId}/Address/Find/${addressId}`, {
            type: "GET"
          })
          .then(function(response){
              return normalize(response, Address);
          });
      },
      remove(patientId, addressId) {
        Condition.requires(patientId).isNotNullOrUndefined()
                                     .isANumber()
                                     .isGreaterThan(0);

        Condition.requires(addressId).isNotNullOrUndefined()
                                     .isANumber()
                                     .isGreaterThan(0);

        return WebClient.request(ApplicationConfiguration.serverUrl + `/Api/Patient/${patientId}/Address/Remove`, {
            type: "DELETE",
            data: {
              addressId: addressId
            }
          });
      }
    },

    medication: {
        getSchema: function(patientId) {
            Condition.requires(patientId).isNotNullOrUndefined()
                                         .isANumber()
                                         .isGreaterThan(0);

            return WebClient.request(ApplicationConfiguration.serverUrl + "/Api/MedicationSchedule/GetPatientSchema", {
                type: "GET",
                data: {
                    id: patientId
                }
            });
        },
        findAll: function(patientId) {
            Condition.requires(patientId).isNotNullOrUndefined()
                                         .isANumber()
                                         .isGreaterThan(0);

            return WebClient.request(ApplicationConfiguration.serverUrl + "/Api/MedicationSchedule/FindAll", {
                type: "GET",
                data: {
                    id: patientId
                }
            });
        },
        findAllToday: function(patientId) {
            Condition.requires(patientId).isNotNullOrUndefined()
                                         .isANumber()
                                         .isGreaterThan(0);

            return WebClient.request(ApplicationConfiguration.serverUrl + "/Api/MedicationSchedule/FindAllToday", {
                type: "GET",
                data: {
                    id: patientId
                }
            });
        },
        complete: function(scheduleId, description) {
            Condition.requires(scheduleId).isNotNullOrUndefined()
                                         .isANumber()
                                         .isGreaterThan(0);

            return WebClient.request(ApplicationConfiguration.serverUrl + "/Api/MedicationSchedule/Complete/" + scheduleId, {
                type: "PUT",
                data: {
                    description: description || 'Ikke udfyldt' || null
                }
            });
        },
        create: function(patientId, schedule) {
            Condition.requires(patientId).isNotNullOrUndefined()
                                         .isANumber()
                                         .isGreaterThan(0);

            Condition.requires(schedule).isNotNullOrUndefined();

            Condition.requires(schedule.startDate).isNotNullOrUndefined()
                                                  .isDate();

            Condition.requires(schedule.items).isNotNullOrUndefined()
                                              .isArray()
                                              .isNotEmpty()
                                              .each(function(item) {
                                                  Condition.requires(item).isNotNullOrUndefined();

                                                  Condition.requires(item.medicationId).isNotNullOrUndefined()
                                                                                       .isGreaterThan(0);

                                                  Condition.requires(item.amouth).isNotNullOrUndefined()
                                                                                 .isGreaterThan(0);

                                                  Condition.requires(item.executeTime).isNotNullOrUndefined();
                                              });

            // We need to transform the start date to ISO
            schedule.startDate = schedule.startDate.toISOString();

            return WebClient.request(ApplicationConfiguration.serverUrl + "/Api/MedicationSchedule/CreatePatientSchema", {
                type: "POST",
                data: {
                    patientId: patientId,
                    startDate: schedule.startDate,
                    items: schedule.items
                }
            });
        },

        history: {
            findAll: function(patientId) {
                Condition.requires(patientId).isNotNullOrUndefined()
                                             .isANumber()
                                             .isGreaterThan(0);

                return WebClient.request(ApplicationConfiguration.serverUrl + "/Api/MedicationScheduleHistory/FindAll", {
                        type: "GET",
                        data: {
                            patientId: patientId
                        }
                    })
                    .then(function(response){
                        return normalize(response, arrayOf(MedicationHistoryItem));
                    });
            },

            add: function(medicationScheduleItemId) {
                Condition.requires(medicationScheduleItemId).isNotNullOrUndefined()
                                             .isANumber()
                                             .isGreaterThan(0);

                return WebClient.request(ApplicationConfiguration.serverUrl + "/Api/MedicationScheduleHistory/Add", {
                        type: "POST",
                        data: {
                            medicationScheduleItemId: medicationScheduleItemId
                        }
                    })
                    .then(function(response){
                        return normalize(response, MedicationHistoryItem);
                    });
            }
        }
    }
};