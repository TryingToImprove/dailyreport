(function () {
    "use strict";
    
    var Condition = require("../Condition"),
        ApplicationConfiguration = require("../../ApplicationConfiguration"),
        WebClient = require("../WebClient"),
        merge = require("react/lib/merge"),
        Service = {
        authenticate: function(username, password){
            Condition.requires(username).isNotNullOrUndefined()
                                        .isNotEmptyOrWhitespace();
            
            Condition.requires(password).isNotNullOrUndefined()
                                        .isNotEmptyOrWhitespace();
        
            var deferred = $.Deferred();
            
            WebClient.request(ApplicationConfiguration.serverUrl + "token", {
                type: "POST",
                data: {
                    grant_type: "password",
                    username: username,
                    password: password
                }
            })
            .then(function(response) {                            
                // Lets save the access_token as a header on WebClient
                WebClient.setHeaders({
                    Authorization: response.token_type + " " + response.access_token
                }); 
                
                Service.getDetails()
                    .then(function(detailsResponse) {
                        deferred.resolve(merge(response,detailsResponse));
                    }, function() { deferred.reject(); });
                
            }, function() { deferred.reject(); });
            
            return deferred.promise();
        },        
        
        reauthenticate: function(authToken) {       
            Condition.requires(authToken).isNotNullOrUndefined()
                                         .isNotEmptyOrWhitespace();
            
            // Lets save the access_token as a header on WebClient
            WebClient.setHeaders({
                Authorization: authToken
            }); 
            
            return Service.getDetails();
        },
            
        getDetails: function() {
            return WebClient.request(ApplicationConfiguration.serverUrl + "Api/Account/Details", {
                type: "GET"
            });
        },
            
        register: function(firstname, lastname, organizationName, email){
            Condition.requires(firstname).isNotNullOrUndefined()
                                        .isNotEmptyOrWhitespace();
            
            Condition.requires(lastname).isNotNullOrUndefined()
                                        .isNotEmptyOrWhitespace();
            
            Condition.requires(organizationName).isNotNullOrUndefined()
                                        .isNotEmptyOrWhitespace();
            
            Condition.requires(email).isNotNullOrUndefined()
                                        .isNotEmptyOrWhitespace();
        
            return WebClient.request(ApplicationConfiguration.serverUrl + "Api/Account/Register", {
                type: "POST",
                data: {
                    firstname: firstname,
                    lastname: lastname,
                    organizationName: organizationName,
                    email: email
                }
            });
        }
    };
    
    module.exports = Service;
}());   