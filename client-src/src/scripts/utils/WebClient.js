(function () {
    "use strict";
    
    var _headers = {};
    
    module.exports = {
        setHeaders: function(headers){
            _headers = headers;
        },
        request: function(url, options){
            options = options || {};
            
            // Map url to jquery options
            options.url = url;
            options.headers = _headers;
            
            return $.ajax(options);
        }
    };
}());   