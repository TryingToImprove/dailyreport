(function() {
    "use strict";
    
    require("moment/locale/da");
    
    var moment = require("moment");
    
    moment.locale("da");
    
    module.exports = moment;
}());