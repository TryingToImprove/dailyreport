(function () {
    "use strict";
    
    module.exports = function(func) {
        var cbArgs = [];

        for(var i = 1; i < arguments.length; i += 1){
            cbArgs.push(arguments[i]);
        }

        return function () {
            var args = cbArgs;

            for(var i = 0; i < arguments.length; i += 1){
                args.push(arguments[i]);
            }

            func.apply(this, args);
        };
    };
}());