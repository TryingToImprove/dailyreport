(function() {
    "use strict";
    
    function Condition(value){
        this.value = value;
    }
    
    Condition.prototype.isNotNullOrUndefined = function(){
        this.isNotNull();
        this.isNotUndefined();
        
        return this;
    };
    
    Condition.prototype.isNotEmptyOrWhitespace = function(){
        this.isNotEmpty();
        this.isNotWhitespace();
        
        return this;
    };
    
    Condition.prototype.isNotUndefined = function(){
        if(typeof(this.value) !== "undefined")
            return this;

        throw new Error("value cannot be undefined");
    };    
    
    Condition.prototype.isNotNull = function(){
        if(this.value !== null)
            return this;

        throw new Error("value cannot be null");
    };
    
    Condition.prototype.isNotEmpty = function(){
        var len = Array.isArray(this.value) ? this.value.length : this.value.length;
     
        if(len > 0)
            return this;

        throw new Error("value cannot be empty");
    };
    
    Condition.prototype.isNotWhitespace = function(){
        if(this.value !== " ")
            return this;

        throw new Error("value cannot be a whitespace");
    };    
    
    Condition.prototype.isANumber = function(){
        if(typeof(this.value) === "number")
            return this;

        throw new Error("value is not a number");
    };    
    
    Condition.prototype.isGreaterOrEqualThan = function(num){
        var value = Array.isArray(this.value) ? this.value.length : this.value;
        
        if(value >= num)
            return this;

        throw new Error("value is not equal or greater than: " + num);
    };    
    
    Condition.prototype.isGreaterThan = function(num){
        var value = Array.isArray(this.value) ? this.value.length : this.value;
  
        if(value > num)
            return this;

        throw new Error("value is not greater than: " + num);
    };    
    
    Condition.prototype.isDate = function(){
        if ((this.value instanceof Date) && !isNaN(this.value.valueOf()))
            return this;
                
        throw new Error("value is not a date");
    };   
    
    Condition.prototype.isArray = function(){
        if (Array.isArray(this.value))
            return this;
                
        throw new Error("value is not a array");
    };
    
    Condition.prototype.each = function(cb) {
        this.value.forEach(function(item, index) {
            try {
                cb(item);
            }
            catch(err){
                throw new Error("Condition failed for object #" + index + "; " + err.message)
            }
        });
            
        return this;
    }
    
    module.exports = {
        requires: function(value){
            return new Condition(value);
        }
    };
}());