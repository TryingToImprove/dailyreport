(function() {
    "use strict";
    
    var AppDispatcher = require("../dispatcher/AppDispatcher"),
        BaseStore = require("./BaseStore"),
        Immutable = require("immutable"),
        
        PatientConstants = require("../constants/PatientConstants"),
        LoadingConstants = require("../constants/LoadingConstants"),
        
        merge = require("react/lib/merge"),
                
        Store = merge(BaseStore, {     
            data: {},
            
            getInitialState() {
                return {
                    loadingState: LoadingConstants.LOADING_PASSIVE,
                    entries: Immutable.Map()
                };
            },
            
            getLoadingState(patientId) {
                return this.get(patientId).loadingState;
            },    
            
            findAll(patientId) {
                var a = this.get(patientId).entries
                    .groupBy(x => {
                        var year = x.entryDateTime.getFullYear(),
                            month = x.entryDateTime.getMonth(),
                            date = x.entryDateTime.getDate();

                        return new Date(year, month, date);
                    })
                    .map((value, key) => {
                        return {
                            key: key,
                            value: value.sortBy(x => x.entryDateTime)
                                        .reverse()
                                        .toArray()
                        };
                    })
                    .sortBy(x => {
                        return x.key;
                    })
                    .reverse();
                
                return a;
            },    
            
            /* REGISTER TO THE DISPATCHER */
            dispatcherIndex: AppDispatcher.register((payload) => {
                var action = payload.action;
                
                switch(action.type) {                        
                    case PatientConstants.TIMELINE_FIND_ALL_STARTED:
                        Store.get(action.patientId).loadingState = LoadingConstants.LOADING_WAITING;
                                                
                        Store.emitChange();
                        break;
                                                
                    case PatientConstants.TIMELINE_FIND_ALL_SUCCESS:
                        Store.get(action.patientId).loadingState = LoadingConstants.LOADING_COMPLETED; 
                        
                        var entries = action.entries.reduce((obj, x) => {
                            x.entryDateTime = new Date(x.entryDateTime);
                            obj[x.uniqueKey] = x;
                            return obj;
                        }, {});
                        
                        Store.get(action.patientId).entries = Store.get(action.patientId).entries.merge(Immutable.Map(entries));
                       
                        Store.emitChange();
                        break;
                        
                    case PatientConstants.TIMELINE_FIND_ALL_FAILED:
                        Store.get(action.patientId).loadingState = LoadingConstants.LOADING_COMPLETED;     
                                                
                        Store.emitChange();                  
                        break;
                }
                                        
                return true;
            })
        });
                
     module.exports = Store;
}());
