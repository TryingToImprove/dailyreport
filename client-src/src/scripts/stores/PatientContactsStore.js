(function() {
    "use strict";   

    var AppDispatcher = require("../dispatcher/AppDispatcher"),
        BaseStore = require("./BaseStore"),
        Immutable = require('immutable'),

        Condition = require("../utils/Condition"),
        
        PatientConstants = require("../constants/PatientConstants"),
        LoadingConstants = require("../constants/LoadingConstants"),
        
        merge = require("react/lib/merge"),

        Store = merge(BaseStore, {    
            data: {},
            
            getInitialState: function() {
                return {
                    contacts: Immutable.Map(),
                    groups: Immutable.Map()
                };                    
            },
            
            findAll: function (patientId) {
                Condition.requires(patientId).isNotNullOrUndefined();

                return this.get(patientId).contacts;
            },
            
            findByGroup: function (patientId, groupId) {
                Condition.requires(groupId).isNotNullOrUndefined()
                                         .isANumber()
                                         .isGreaterThan(0);

                return this.get(patientId).contacts.filter(x => {
                    for (var i = 0; i < x.groups.length; i += 1) {
                        if(x.groups[i].id === groupId)
                            return true;
                    }

                    return false;
                });
            },
            
            findFrequent(patientId) {
                Condition.requires(patientId).isNotNullOrUndefined();

                return this.get(patientId).contacts.take(10);
            },

            getGroups: function (patientId) {
                Condition.requires(patientId).isNotNullOrUndefined();

                return this.get(patientId).groups;
            },
            
            /* REGISTER TO THE DISPATCHER */
            dispatcherIndex: AppDispatcher.register(function(payload) {
                var action = payload.action;
                
                switch(action.type) {                                      
                    case PatientConstants.CONTACTS_FIND_ALL_SUCCESS:       
                    case PatientConstants.CONTACTS_FIND_FREQUENT_SUCCESS:    
                    case PatientConstants.CONTACTS_FIND_BY_GROUP_SUCCESS:                 
                        Store.get(action.patientId).contacts = Store.get(action.patientId).contacts.merge(Immutable.Map(action.contacts));
                        Store.get(action.patientId).groups = Store.get(action.patientId).groups.merge(Immutable.Map(action.groups));

                        Store.emitChange();
                        break;       

                    case PatientConstants.CONTACTS_ADD_SUCCESS:     
                        //TODO: Add code for adding the contact to the Store..
                              
                        Store.emitChange();
                        break;                      

                    case PatientConstants.CONTACTS_GET_GROUPS_SUCCESS: 
                        Store.get(action.patientId).groups = Store.get(action.patientId).groups.merge(Immutable.Map(action.groups));
                        
                        Store.emitChange();
                        break;
                }
                                        
                return true;
            })
        });
                
     module.exports = Store;
}());
