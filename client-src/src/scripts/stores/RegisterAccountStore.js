(function() {
    "use strict";
    
    var AppDispatcher = require("../dispatcher/AppDispatcher"),
        BaseStore = require("./BaseStore"),
        AccountConstants = require("../constants/AccountConstants"),
        
        merge = require("react/lib/merge"),
        
        data = {
            isLoading: false,
            creationState: "passive",
            employee: null,
        },
        
        Store = merge(BaseStore, {        
            getLoadingState: function () {
                return data.isLoading;
            },
            getCreationState: function () {
                return data.creationState;
            },
            getEmployee: function () {
                return data.employee;
            },

            /* REGISTER TO THE DISPATCHER */
            dispatcherIndex: AppDispatcher.register(function(payload) {
                var action = payload.action;
                
                switch(action.type){
                    case AccountConstants.REGISTER_RESET:
                        data.isLoading = false;
                        data.creationState = "passive";
                        data.employee = null;
                        
                        Store.emitChange();
                        break;
                        
                    case AccountConstants.REGISTER_STARTED:
                        data.isLoading = true;
                        data.creationState = "passive";
                                                
                        Store.emitChange();
                        break;
                        
                    case AccountConstants.REGISTER_SUCCESS:
                        data.isLoading = false;      
                        data.creationState = "success";
                        data.employee = action.employee;
                        
                        Store.emitChange();                  
                        break;
                        
                    case AccountConstants.REGISTER_FAILED:
                        data.isLoading = false;      
                        data.creationState = "failed";
                        
                        Store.emitChange();                  
                        break;
                }
                
                return true;
            })
        });
                
     module.exports = Store;
}());
