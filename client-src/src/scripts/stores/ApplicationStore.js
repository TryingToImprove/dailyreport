(function() {
    "use strict";
    
    var AppDispatcher = require("../dispatcher/AppDispatcher"),
        BaseStore = require("./BaseStore"),        
        AuthenticationConstants = require("../constants/AuthenticationConstants"),
        
        merge = require("react/lib/merge"),
        
        data = {
            isAuthenticated: false,
            currentAccount: null,
            currentOrganization: null
        },
        
        Store = merge(BaseStore, {      
            getAuthenticationState: function () {
                return data.isAuthenticated;
            },   
            getCurrentAccount: function () {
                return data.currentAccount;
            }, 
            getCurrentOrganization: function () {
                return data.currentOrganization;
            },

            /* REGISTER TO THE DISPATCHER */
            dispatcherIndex: AppDispatcher.register(function(payload) {
                var action = payload.action;
                
                switch(action.type){
                    case AuthenticationConstants.AUTHENTICATION_SUCCESS:
                        data.isAuthenticated = true;
                        data.currentAccount = action.account;
                        data.currentOrganization = action.organization;
                                                
                        Store.emitChange();
                        break;
                    
                    case AuthenticationConstants.AUTHENTICATION_FAILED:
                        data.isAuthenticated = false;
                                                
                        Store.emitChange();
                        break;                        
                        
                    case AuthenticationConstants.UNAUTHENTICATE:
                        data.isAuthenticated = false;
                        data.currentAccount = null;
                        data.currentOrganization = null;
                        
                        Store.emitChange();
                        break;
                }
                    
                return true;
            })
        });
                
     module.exports = Store;
}());