(function() {
    "use strict";
    
    var AppDispatcher = require("../dispatcher/AppDispatcher"),
        BaseStore = require("./BaseStore"),
        
        PatientConstants = require("../constants/PatientConstants"),
        LoadingConstants = require("../constants/LoadingConstants"),
        ContactInformationConstants = require("../constants/ContactInformationConstants"),
        
        merge = require("react/lib/merge"),
                
        data = {
            firstname: "",
            lastname: "",
            contactInformations: [],
            contactInformationTypes: [],
            selectedContactPersons: []
        },
        
        addContactInformation = function(dataTypeId, value) {            
            var dataTypeName = data.contactInformationTypes.filter(function(type) { 
                return type.id == dataTypeId; 
            })[0].typeName;
            
            data.contactInformations.push({
                dataType: dataTypeId,
                dataTypeName: dataTypeName,
                data: value
            });
        },
        
        toggleContactPerson = function(employeeId){
            var index = data.selectedContactPersons.indexOf(employeeId);
            
            if (index === -1) {
                data.selectedContactPersons.push(employeeId);
                return;
            }
            
            data.selectedContactPersons.splice(index, 1);
        },
        
        Store = merge(BaseStore, {        
            getFirstname: function() {
                return data.firstname;
            },        
            getLastname: function() {
                return data.lastname;
            },     
            getContactInformations: function() {
                return data.contactInformations;
            },
            getContactPersons: function() {
                return data.selectedContactPersons;
            },
            
            
            /* REGISTER TO THE DISPATCHER */
            dispatcherIndex: AppDispatcher.register(function(payload) {
                var action = payload.action;
                
                switch(action.type){                        
                    case PatientConstants.CREATE_WIZARD_PERSONAL:
                        data.firstname = action.firstname;
                        data.lastname = action.lastname;
                                                
                        Store.emitChange();                  
                        break;        
                        
                    case PatientConstants.CREATE_WIZARD_ADD_CONTACT_INFORMATION:
                        addContactInformation(action.dataType, action.data);
                        
                        Store.emitChange();                  
                        break;
                        
                    case PatientConstants.CREATE_SUCCESS:
                        data.firstname = "";
                        data.lastname = "";
                        data.contactInformations = [];
                        
                        Store.emitChange();                  
                        break;
                        
                    case PatientConstants.CREATE_WIZARD_TOGGLE_CONTACT_PERSON: 
                        toggleContactPerson(action.employeeId);
                        
                        Store.emitChange();
                        break;
                        
                    case ContactInformationConstants.GET_TYPES_SUCCESS:
                        data.contactInformationTypes = action.types;
                        
                        Store.emitChange();                  
                        break;
                }
                                        
                return true;
            })
        });
                
     module.exports = Store;
}());
