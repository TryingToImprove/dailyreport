(function(){
    "use strict";
    
    var merge = require("react/lib/merge"),
        EventEmitter = require("events").EventEmitter,
        
        CHANGE_EVENT = "change";
    
    module.exports = merge(EventEmitter.prototype, {
        emitChange: function () {
            this.emit(CHANGE_EVENT);
        },

        addChangeListener: function (callback) {
            this.on(CHANGE_EVENT, callback);
        },

        removeChangeListener: function (callback) {
            this.removeListener(CHANGE_EVENT, callback);
        },
        
        get: function(key) {
            if (typeof(this.data[key]) === "undefined") {
                this.data[key] = this.getInitialState();
            }
            
            return this.data[key];
        }
    });
    
}());