(function() {
    "use strict";
    
    var AppDispatcher = require("../dispatcher/AppDispatcher"),
        BaseStore = require("./BaseStore"),
        AuthenticationConstants = require("../constants/AuthenticationConstants"),
        
        merge = require("react/lib/merge"),
        
        data = {
            isLoading: false,
            authenticationState: "passive"
        },
        
        Store = merge(BaseStore, {        
            getLoadingState: function () {
                return data.isLoading;
            }, 
            getAuthenticationState: function () {
                return data.authenticationState;
            },   

            /* REGISTER TO THE DISPATCHER */
            dispatcherIndex: AppDispatcher.register(function(payload) {
                var action = payload.action;
                
                switch(action.type){
                    case AuthenticationConstants.AUTHENTICATION_STARTED:
                        data.isLoading = true;
                        data.authenticationState = "passive";
                             
                        Store.emitChange();
                        break;
                        
                    case AuthenticationConstants.AUTHENTICATION_SUCCESS:
                        data.isLoading = false;      
                        data.authenticationState = "success";
                        
                        Store.emitChange();                  
                        break;  
                        
                    case AuthenticationConstants.AUTHENTICATION_FAILED:
                        data.isLoading = false;      
                        data.authenticationState = "failed";
                        
                        Store.emitChange();                  
                        break;  
                        
                    case AuthenticationConstants.AUTHENTICATE_RESET:
                        data.isLoading = false;      
                        data.authenticationState = "passive";
                        
                        Store.emitChange();                  
                        break;  
                }
                
                return true;
            })
        });
                
     module.exports = Store;
}());
