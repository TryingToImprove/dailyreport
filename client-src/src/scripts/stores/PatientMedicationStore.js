var AppDispatcher = require("../dispatcher/AppDispatcher"),
    BaseStore = require("./BaseStore"),
    Immutable = require("immutable"),

    CreateMedicationConstants = require("../constants/CreateMedicationConstants"),
    PatientConstants = require("../constants/PatientConstants"),

    merge = require("react/lib/merge"),

    Store = merge(BaseStore, {        
        data: {},

        getInitialState() {
            return {
                schedules: Immutable.Map(),
                schema: Immutable.Map()
            };
        },

        getSchema(patientId) {
            var result = Immutable.List(),
                currentDate = new Date().setHours(0, 0, 0, 0);

            this.get(patientId).schedules
                .filter(item => !item.isComplted)
                .map(item => item.schedules)
                .forEach(item => {
                    item.forEach(x => {
                        var isActive = (x.nextTime !== null && new Date(x.nextTime).setHours(0, 0, 0, 0) === currentDate);

                        if (isActive) {                            
                            result = result.push(x) ;
                        }
                    });
                });

            return result.toArray();
        },

        findAll(patientId) {
            return this.get(patientId).schedules
                .filter(x => !x.isCompleted)
                .toArray();
        },

        /* REGISTER TO THE DISPATCHER */
        dispatcherIndex: AppDispatcher.register(payload => {
            var action = payload.action;

            switch(action.type) {                                
                case PatientConstants.MEDICATION_FIND_ALL_SUCCESS:
                    var schedules = action.schedules.reduce((obj, x) => {
                        obj[x.id] = x;
                        return obj; 
                    }, {});

                    Store.get(action.patientId).schedules = Store.get(action.patientId).schedules.merge(Immutable.Map(schedules));

                    Store.emitChange();
                    break;     

                case PatientConstants.MEDICATION_FIND_ALL_TODAY_SUCCESS:
                    var schedules = action.schedules.reduce((obj, x) => {
                        obj[x.id] = x;
                        return obj; 
                    }, {});

                    Store.get(action.patientId).schedules = Store.get(action.patientId).schedules.merge(Immutable.Map(schedules));

                    Store.emitChange();
                    break;      

                case PatientConstants.MEDICATION_COMPLETE_SUCCESS:                        
                    Store.get(action.patientId).schedules = Store.get(action.patientId).schedules.remove(action.schedule.id.toString());

                    Store.emitChange();
                    break;

                case CreateMedicationConstants.SAVE_SUCCESS:
                    var schema = action.schedule.schedules.reduce(function(obj, x) {
                        obj[x.id] = x;
                        return obj;
                    }, {});

                    Store.get(action.patientId).schema = Store.get(action.patientId).schema.merge(Immutable.Map(schema));

                    Store.emitChange();
                    break;
            }

            return true;
        })
    });

export default Store;
