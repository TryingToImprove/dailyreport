(function() {
    "use strict";
    
    var AppDispatcher = require("../dispatcher/AppDispatcher"),
        BaseStore = require("./BaseStore"),
        Immutable = require("immutable"),
        
        PatientConstants = require("../constants/PatientConstants"), 
        
        merge = require("react/lib/merge"),        
        
        Store = merge(BaseStore, {  
            data: {},
            
            getInitialState: function(){
                return {
                    contactInformations: Immutable.Map()
                };
            },
            
            findAll: function (patientId) {
                return this.get(patientId).contactInformations.filter(x=> x.isActive);
            },
            
            /* REGISTER TO THE DISPATCHER */
            dispatcherIndex: AppDispatcher.register(function(payload) {
                var action = payload.action;
                
                switch(action.type) {                                                       
                    case PatientConstants.FETCH_CONTACT_INFORMATIONS_SUCCESS:
                    case PatientConstants.CONTACT_INFORMATION_ADD_SUCCESS:         
                    case PatientConstants.CONTACT_INFORMATION_REMOVE_SUCCESS:  //TODO: This should not be on patientconstants?? :)                                     
                        Store.get(action.patientId).contactInformations = Store.get(action.patientId).contactInformations.merge(Immutable.Map(action.contactInformations));
                        
                        Store.emitChange();
                        break;                        
                }
                                        
                return true;
            })
        });
                
     module.exports = Store;
}());
