import AppDispatcher from "../dispatcher/AppDispatcher";
import BaseStore from "./BaseStore";
import Immutable from "immutable";
import GeocodingConstants from "../constants/GeocodingConstants";
import merge from "react/lib/merge";

var Store = merge(BaseStore, {     
    data: {},
    
    getInitialState: function() {
        return {
            results: Immutable.List()
        };
    },
    
    find: function (query) {
        return this.get(query).results;
    },    
    
    /* REGISTER TO THE DISPATCHER */
    dispatcherIndex: AppDispatcher.register(function(payload) {
        var action = payload.action;
        
        switch(action.type) {                        
            case GeocodingConstants.AUTOCOMPLETE_SUCCESS:
                Store.get(action.query).results = Immutable.List(action.results);
                                        
                Store.emitChange();
                break;                
        }
                                
        return true;
    })
});
        
 export default Store;
