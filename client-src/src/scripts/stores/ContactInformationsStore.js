(function() {
    "use strict";
    
    var AppDispatcher = require("../dispatcher/AppDispatcher"),
        BaseStore = require("./BaseStore"),
        Immutable = require("immutable"),
        
        ContactInformationConstants = require("../constants/ContactInformationConstants"),
        
        merge = require("react/lib/merge"),
                
        Store = merge(BaseStore, {  
            data: {
                types: Immutable.List()
            },
            
            getTypes: function() {
                return this.data.types.toArray();
            },
            
            /* REGISTER TO THE DISPATCHER */
            dispatcherIndex: AppDispatcher.register(function(payload) {
                var action = payload.action;
                
                switch(action.type){                      
                    case ContactInformationConstants.GET_TYPES_SUCCESS:
                       Store.data.types = Store.data.types.merge(Immutable.List(action.types));
                        
                        Store.emitChange();                  
                        break;   
                }
                                        
                return true;
            })
        });
                
     module.exports = Store;
}());
