(function() {
    "use strict";
    
    var AppDispatcher = require("../dispatcher/AppDispatcher"),
        BaseStore = require("./BaseStore"),
        Immutable = require("immutable"),
        
        EmployeeConstants = require("../constants/EmployeeConstants"),
        PatientConstants = require("../constants/PatientConstants"),
        
        merge = require("react/lib/merge"),
                
        Store = merge(BaseStore, {     
            data: {
                employees: Immutable.Map()
            },
            
            get: function(employeeId){
                return this.data.employees.get(employeeId.toString(), null);
            },
            
            findAll: function() {
                return this.data.employees.toArray();
            },
            
            /* REGISTER TO THE DISPATCHER */
            dispatcherIndex: AppDispatcher.register(function(payload) {
                var action = payload.action;
                
                switch(action.type){                      
                    case EmployeeConstants.FIND_ALL_SUCCESS:
                        var employees = action.employees.reduce((obj, x) => {
                            obj[x.id] = x;
                            return obj;
                        }, {});
                        
                        Store.data.employees = Store.data.employees.merge(Immutable.Map(employees));   
                                                
                        Store.emitChange();                  
                        break;      
                        
                    case EmployeeConstants.GET_SUCCESS:
                        Store.data.employees = Store.data.employees.set(action.employee.id.toString(), action.employee);   
                                                
                        Store.emitChange();
                        break;
                        
                    case PatientConstants.MEDICATION_HISTORY_FIND_ALL_SUCCESS:
                    case PatientConstants.MEDICATION_HISTORY_ADD_SUCCESS:     
                        Store.data.employees = Store.data.employees.merge(Immutable.Map(action.employees));   
                        
                        Store.emitChange();
                        break;
                }
                                         
                return true;
            })
        });
                
     module.exports = Store;
}());
