(function() {
    "use strict";
    
    var AppDispatcher = require("../dispatcher/AppDispatcher"),
        BaseStore = require("./BaseStore"),
        Immutable = require("immutable"),
        
        MedicationConstants = require("../constants/MedicationConstants"),
        PatientConstants = require("../constants/PatientConstants"),
        
        merge = require("react/lib/merge"),
        
        Store = merge(BaseStore, {        
            data: {
                medications: Immutable.Map()
            },
            
            getMedications: function () {
                return this.data.medications.toArray();
            },    
            
            get: function (medicationId) {
                return this.data.medications.get(medicationId.toString());
            },    
            
            /* REGISTER TO THE DISPATCHER */
            dispatcherIndex: AppDispatcher.register(function(payload) {
                var action = payload.action;
                
                switch(action.type) {                                              
                    case MedicationConstants.SEARCH_SUCCESS:              
                    case PatientConstants.MEDICATION_HISTORY_FIND_ALL_SUCCESS:
                    case PatientConstants.MEDICATION_HISTORY_ADD_SUCCESS:        
                        Store.data.medications = Store.data.medications.merge(Immutable.Map(action.medications));   
                        
                        Store.emitChange();
                        break;           
                } 
                                        
                return true;
            })
        });
                
     module.exports = Store;
}());
