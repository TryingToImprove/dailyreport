(function() {
    "use strict";
        
    // Dependencies
    var AppDispatcher = require("../dispatcher/AppDispatcher");
    var BaseStore = require("./BaseStore");
    var merge = require("react/lib/merge");
    var Immutable = require('immutable');
    
    // Constants
    var PatientConstants = require("../constants/PatientConstants");
    var LoadingConstants = require("../constants/LoadingConstants");
            
    // Helpers                    
    var Store = merge(BaseStore, {
        data: {
            loadingState: LoadingConstants.LOADING_PASSIVE,
            patients: Immutable.Map()
        },
        
        getLoadingState: function () {
            return this.data.loadingState;
        },    
        
        get: function (patientId) {
            return this.data.patients.get(patientId.toString(), null);
        },  
        
        
        findAll: function() {
            return this.data.patients.sortBy(x => x.firstname)
                                      .toArray();
        },

        /* REGISTER TO THE DISPATCHER */
        dispatcherIndex: AppDispatcher.register(function(payload) {
            var action = payload.action;

            switch(action.type){
                case PatientConstants.GET_SUCCESS:
                    Store.data.patients = Store.data.patients.set(action.patient.id.toString(), action.patient);
                    
                    Store.emitChange();
                    break;

                case PatientConstants.UPDATE_SUCCESS:
                    Store.data.patients = Store.data.patients.set(action.patient.id.toString(), action.patient);
                                    
                    Store.emitChange();
                    break;
                    
                    
                case PatientConstants.FIND_ALL_STARTED:
                    Store.data.loadingState = LoadingConstants.LOADING_WAITING;

                    Store.emitChange();
                    break;

                case PatientConstants.FIND_ALL_SUCCESS:
                    Store.data.loadingState = LoadingConstants.LOADING_COMPLETED; 
                        
                    var patients = action.patients.reduce((obj, x) => {
                                                         obj[x.id] = x;
                                                         return obj;
                                                     }, {});
                    
                    Store.data.patients = Store.data.patients.merge(Immutable.Map(patients));

                    Store.emitChange();
                    break;

                case PatientConstants.FIND_ALL_FAILED:
                    Store.data.loadingState = LoadingConstants.LOADING_COMPLETED;

                    Store.emitChange();                  
                    break;
                
                case PatientConstants.CREATE_SUCCESS:
                    Store.data.patients.set(action.patient.id.toString(), action.patient);
                    
                    
                    Store.emitChange();
                    break;
            }

            return true;
        })
    });
    
    module.exports = Store;
}());
