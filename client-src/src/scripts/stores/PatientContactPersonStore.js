(function() {
    "use strict";
    
    var AppDispatcher = require("../dispatcher/AppDispatcher"),
        BaseStore = require("./BaseStore"),
        Immutable = require('immutable'),
        
        PatientConstants = require("../constants/PatientConstants"),
        LoadingConstants = require("../constants/LoadingConstants"),
        
        merge = require("react/lib/merge"),
        
        periodComparator = function(a, b) {                
            var timeStarted = {
                    a: new Date(a.timeStarted),
                    b: new Date(b.timeStarted)
                },
                timeEnded = {
                    a: new Date(a.timeEnded),
                    b: new Date(b.timeEnded)
                };

              return timeEnded.a < timeEnded.b ? -1 : timeEnded.a > timeEnded.b ? 1 :
                     timeStarted.a > timeStarted.b ? -1 : timeStarted.a < timeStarted.b ? 1 : 0;
        },
        
        mergeResults = function(patientId, results) {
            var dictionary = results.reduce((obj, x) => {
                                 obj[x.id] = x;
                                 return obj;
                             }, {});
            
            return Store.get(patientId).contactPersons.merge(Immutable.Map(dictionary));
        },
        
        data = {
            contactPerson: {
                loadingState: LoadingConstants.LOADING_PASSIVE,
                contactPersons: null
            },
            selectedContactPerson: null
        },
        
        Store = merge(BaseStore, {    
            data: {},
            
            getInitialState: function() {
                return {
                    contactPersons: Immutable.Map(),
                    selectedContactPerson: null
                };                    
            },
            
            findAll: function (patientId) {
                return this.get(patientId).contactPersons
                                          .sort(periodComparator)
                                          .toArray();
            },
            
            getSelectedContactPerson: function () {
                return data.selectedContactPerson;
            },
            
            getActives: function (patientId) {
                return this.get(patientId).contactPersons
                                          .filter(contactPerson => !contactPerson.isEnded)
                                          .sort(periodComparator)
                                          .toArray();
            },
            
            /* REGISTER TO THE DISPATCHER */
            dispatcherIndex: AppDispatcher.register(function(payload) {
                var action = payload.action;
                
                switch(action.type) {                                                              
                    case PatientConstants.FETCH_ALL_CONTACT_PERSONS_SUCCESS:
                        Store.get(action.patientId).contactPersons = mergeResults(action.patientId, action.contactPersons);
                                                
                        Store.emitChange();
                        break;
                        
                    case PatientConstants.CREATE_CONTACT_PERSONS_SELECTED:
                        data.selectedContactPerson = action.selectedContactPersonId;     
                                                
                        Store.emitChange();                  
                        break;
                            
                    case PatientConstants.FETCH_CURRENT_CONTACT_PERSONS_SUCCESS:                 
                        Store.get(action.patientId).contactPersons = mergeResults(action.patientId, action.contactPersons);
                        
                        Store.emitChange();
                        break;
                        
                    case PatientConstants.ADD_CONTACT_PERSON_SUCCESS:
                        data.selectedContactPerson = null;
                           
                        var temp = Store.get(action.patientId).contactPersons;
                        
                        if (action.completePrevious) {
                            temp = temp.map(contactPerson => {
                                contactPerson.isEnded = true;
                                contactPerson.timeEnded = action.contactPerson.timeStarted;
                                return contactPerson;
                            });
                        }
                        
                        temp = temp.set(action.contactPerson.id.toString(), action.contactPerson);
                         
                        Store.get(action.patientId).contactPersons = temp;
                        
                        Store.emitChange();                  
                        break;
                        
                    case PatientConstants.COMPLETE_CONTACT_PERSON_SUCCESS:   
                        var contactPersons = Store.get(action.patientId).contactPersons
                                                  .set(action.contactPerson.id.toString(), action.contactPerson);
                        
                        Store.get(action.patientId).contactPersons = contactPersons;
                        
                        Store.emitChange();                  
                        break;
                }
                                        
                return true;
            })
        });
                
     module.exports = Store;
}());
