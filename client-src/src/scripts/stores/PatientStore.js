(function() {
    "use strict";
    
    var AppDispatcher = require("../dispatcher/AppDispatcher"),
        BaseStore = require("./BaseStore"),
        
        PatientConstants = require("../constants/PatientConstants"),
        LoadingConstants = require("../constants/LoadingConstants"),
        
        merge = require("react/lib/merge"),
        
        
        data = {
            isCreating: false,
            isUpdating: false
        },
        
        Store = merge(BaseStore, {        
            getIsUpdatingState: function() {
                return data.isUpdating;
            },        
            getIsCreatingState: function() {
                return data.isCreating;
            },
            
            /* REGISTER TO THE DISPATCHER */
            dispatcherIndex: AppDispatcher.register(function(payload) {
                var action = payload.action;
                
                switch(action.type){                        
                    case PatientConstants.UPDATE_FAILED:
                    case PatientConstants.UPDATE_SUCCESS:
                        data.isUpdating = false;     
                                                
                        Store.emitChange();                  
                        break;
                        
                    case PatientConstants.UPDATE_STARTED:
                        data.isUpdating = true;
                        
                        Store.emitChange();                  
                        break;            
                        
                    case PatientConstants.CREATE_STARTED:
                        data.isCreating = true;
                        
                        Store.emitChange();                  
                        break;
                        
                    case PatientConstants.CREATE_FAILED:
                        data.isCreating = false;     
                                                
                        Store.emitChange();              
                        break;
                        
                    case PatientConstants.CREATE_SUCCESS:
                        data.isCreating = false;     
                                                
                        Store.emitChange();                  
                        break;                        
                }
                                        
                return true;
            })
        });
                
     module.exports = Store;
}());
