// Dependencies
var AppDispatcher = require("../dispatcher/AppDispatcher");
var BaseStore = require("./BaseStore");
var Immutable = require("immutable");

// Constants
var CreateMedicationConstants = require("../constants/CreateMedicationConstants");
var PatientConstants = require("../constants/PatientConstants");

// Stores
import EmployeesStore from './EmployeesStore';
import MedicationStore from './MedicationStore';

// TODO: Do not use merge for stores
var merge = require("react/lib/merge");

var Store = merge(BaseStore, {
    data: {},

    getInitialState() {
        return {
            addresses: Immutable.Map()
        };
    },

    findAll(patientId) {
        return this.get(patientId).addresses.sortBy(x => !x.isPrimary);
    },

    findPrimary(patientId) {
        return this.get(patientId).addresses.filter(x => x.isPrimary)
    },

    findById(patientId, addressId) {
        return this.get(patientId).addresses.get(addressId);
    },

    setAddresses(patientId, addresses){
        this.get(patientId).addresses = this.findAll(patientId).merge(Immutable.Map(addresses));
    },

    clearCurrentPrimary(patientId) {
        this.get(patientId).addresses = this.get(patientId).addresses.map(x => {
            x.isPrimary = false
            return x;
        });
    },

    /* REGISTER TO THE DISPATCHER */
    dispatcherIndex: AppDispatcher.register(payload => {
        var action = payload.action;

        switch(action.type) {
            case PatientConstants.ADDRESS_ADD_SUCCESS:
            case PatientConstants.ADDRESS_UPDATE_SUCCESS:
                if (action.clearCurrentPrimary)
                    Store.clearCurrentPrimary(action.patientId);

                Store.setAddresses(action.patientId, action.addresses);

                Store.emitChange();
                break;

            case PatientConstants.ADDRESS_FIND_ALL_SUCCESS:
            case PatientConstants.ADDRESS_FIND_PRIMARY_SUCCESS:
            case PatientConstants.ADDRESS_FIND_BY_ID_SUCCESS:
                Store.setAddresses(action.patientId, action.addresses);

                Store.emitChange();
                break;

            case PatientConstants.ADDRESS_REMOVE_SUCCESS:
                Store.get(action.patientId).addresses = Store.get(action.patientId).addresses.remove(action.addressId.toString());

                Store.emitChange();
                break;
        }

        return true;
    })
});

export default Store;
