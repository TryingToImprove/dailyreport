(function() {
    "use strict";
    
    var AppDispatcher = require("../dispatcher/AppDispatcher"),
        BaseStore = require("./BaseStore"),
        
        PatientConstants = require("../constants/PatientConstants"),
        LoadingConstants = require("../constants/LoadingConstants"),
        
        merge = require("react/lib/merge"),
                
        Store = merge(BaseStore, {     
            data: {},
            
            getInitialState: function() {
                return {
                    loadingState: LoadingConstants.LOADING_PASSIVE,
                    posts: null
                };
            },
            
            getLoadingState: function (patientId) {
                return this.get(patientId).loadingState;
            },    
            
            findAll: function (patientId) {
                return this.get(patientId).posts;
            },    
            
            /* REGISTER TO THE DISPATCHER */
            dispatcherIndex: AppDispatcher.register(function(payload) {
                var action = payload.action;
                
                switch(action.type) {                        
                    case PatientConstants.FETCH_POSTS_STARTED:
                        Store.get(action.patientId).loadingState = LoadingConstants.LOADING_WAITING;
                                                
                        Store.emitChange();
                        break;
                                                
                    case PatientConstants.FETCH_POSTS_SUCCESS:
                        Store.get(action.patientId).loadingState = LoadingConstants.LOADING_COMPLETED; 
                        Store.get(action.patientId).posts = action.posts;
                                                                   
                        Store.emitChange();
                        break;
                        
                    case PatientConstants.FETCH_POSTS_FAILED:
                        Store.get(action.patientId).loadingState = LoadingConstants.LOADING_COMPLETED;     
                                                
                        Store.emitChange();                  
                        break;
                }
                                        
                return true;
            })
        });
                
     module.exports = Store;
}());
