// Dependencies
var AppDispatcher = require("../dispatcher/AppDispatcher");
var BaseStore = require("./BaseStore");
var Immutable = require("immutable");

// Constants
var CreateMedicationConstants = require("../constants/CreateMedicationConstants");
var PatientConstants = require("../constants/PatientConstants");

// Stores
import EmployeesStore from './EmployeesStore';
import MedicationStore from './MedicationStore';
    
// TODO: Do not use merge for stores
var merge = require("react/lib/merge");

var map = function(historyItem) {
    historyItem.employee = EmployeesStore.get(historyItem.employee.id || historyItem.employee);
    historyItem.medication = MedicationStore.get(historyItem.medication.id || historyItem.medication);
    return historyItem;
};

var Store = merge(BaseStore, {        
    data: {},

    getInitialState() {
        return {
            histories: Immutable.Map()
        };
    },

    findAll(patientId) {
        return this.get(patientId).histories.map(map).toArray();
    },

    /* REGISTER TO THE DISPATCHER */
    dispatcherIndex: AppDispatcher.register(payload => {
        var action = payload.action;

        switch(action.type) {         
            case PatientConstants.MEDICATION_HISTORY_FIND_ALL_SUCCESS:
            case PatientConstants.MEDICATION_HISTORY_ADD_SUCCESS:
                AppDispatcher.waitFor([EmployeesStore.dispatcherIndex, MedicationStore.dispatcherIndex]);
                
                var histories = Immutable.Map(action.histories);
                
                Store.get(action.patientId).histories = Store.get(action.patientId).histories.merge(histories);

                Store.emitChange();
                break;       
        }

        return true;
    })
});

export default Store;
