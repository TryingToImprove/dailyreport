/**
 *
 * @jsx React.DOM
 */

// Dependencies
var React = require("react");
var { Link } = require("react-router");     

// Stores
var PatientStore = require("../../stores/PatientProfileStore");
var ApplicationStore = require("../../stores/ApplicationStore");
       
// Constants
var LoadingConstants = require("../../constants/LoadingConstants");
        
// Actions
var { PatientActions } = require("../../actions/client/PatientActions");
        
// Components
var { Text } = require("../../components/utils/loading");        
var PatientTable = require("../../components/patient/PatientTable");
    
export default React.createClass({
    statics: {
        willTransitionTo: function () {
            PatientActions.findAll();
        }
    },

    getState() {
        return {
            loadingState: PatientStore.getLoadingState(),
            patients: PatientStore.findAll(),
            organization: ApplicationStore.getCurrentOrganization()
        };
    },
    
    getInitialState() {
        return this.getState();
    },

    componentDidMount() {
        ApplicationStore.addChangeListener(this.onChange);
        PatientStore.addChangeListener(this.onChange);
    },

    componentWillUnmount() {
        ApplicationStore.removeChangeListener(this.onChange);
        PatientStore.removeChangeListener(this.onChange);
    },

    getCurrentView() {
        var patients = this.state.patients;

        if (this.state.patients === null) {
            return (     
                /*jshint ignore:start */               
                <TextLoadingComponent text="Henter patienter.." />
                /*jshint ignore:end */
            );  
        } 

        if (patients.length === 0) {
            return (     
                /*jshint ignore:start */               
                <p className="text-center text-muted">
                    <b>{this.state.organization.name}</b> har ingen tilknyttede patienter. <br />
                    <Link to="patientCreate">Tilføj en ny patient</Link>
                </p>                    
                /*jshint ignore:end */
            );
        } 

        return (     
            /*jshint ignore:start */               
            <PatientTable patients={this.state.patients} />                   
            /*jshint ignore:end */
        );      

       return [];
    },

    render() {
        var currentView = this.getCurrentView();

        return (
            /*jshint ignore:start */
            <div>
                <h1 className="page-header">Patienter</h1>
                {currentView}
            </div>
            /*jshint ignore:end */
        );
    },

    onChange() {
        this.setState(this.getState());
    }
});