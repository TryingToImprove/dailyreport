/**
 *
 * @jsx React.DOM
 */

// Dependencies
var React = require("react");
var { State } = require("react-router");

// Stores
var PatientStore = require("../../stores/PatientStore");
var PatientCreateStore = require("../../stores/PatientCreateStore");
var EmployeesStore = require("../../stores/EmployeesStore");

// Actions
var { PatientActions } = require("../../actions/client/PatientActions");
var EmployeeActions = require("../../actions/client/EmployeeActions");

// Components
var { Progress } = require("../../components/utils/loading");
var { Wizard, WizardItem } = require("../../components/utils/wizard");
var WizardSteps = {
    Personal: require("../../components/patient/create/PersonalWizardStep"),
    ContactInformation: require("../../components/patient/create/ContactInformationWizardStep"),
    ContactPerson: require("../../components/patient/create/ContactPersonWizardStep"),
    Finish: require("../../components/patient/create/FinishWizardStep")
};
        
export default React.createClass({
    mixins: [State], 

    getState() {
        return {
            isCreating: PatientStore.getIsCreatingState()
        };
    },

    getInitialState() {
        return this.getState();
    },

    componentDidMount() {
        PatientStore.addChangeListener(this.onChange);
    },

    componentWillUnmount() {
        PatientStore.removeChangeListener(this.onChange);
    },

    render() {
        if (this.state.isCreating) {
           return (
                /*jshint ignore:start */
                <Progress />
                /*jshint ignore:end */
            );
        }

        var steps = [WizardSteps.Personal, WizardSteps.ContactInformation, WizardSteps.ContactPerson, WizardSteps.Finish];

        return (
            /*jshint ignore:start */
            <div>
                <h1 className="page-header">Tilføj patient</h1>
                <Wizard steps={steps} />
            </div>
            /*jshint ignore:end */
        );
    },

    onChange() {
        this.setState(this.getState());
    }
});