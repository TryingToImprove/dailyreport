/**
 *
 * @jsx React.DOM
 */

// Dependencies
var React = require("react");
var { State, Link } = require("react-router");

// Stores
var PatientMedicationStore = require("../../../stores/PatientMedicationStore");
var MedicationStore = require("../../../stores/MedicationStore");
var PatientStore = require("../../../stores/PatientStore");

// Actions
var { PatientMedicationActions } = require("../../../actions/client/PatientActions");
var MedicationActions = require("../../../actions/client/MedicationActions");
var CreateMedicationScheduleActions = require("../../../actions/client/CreateMedicationScheduleActions");

// Constants
var LoadingConstants = require("../../../constants/LoadingConstants");

// Components
var MedicationTypeaheadComponent = require("../../../components/medication/medicationTypeahead");
var MedicationSchema = require("../../../components/medication/schema");
var CreateSingleScheduleFormComponent = require("../../../components/medication/createSingleScheduleForm");

var ProvideSingleMedication = React.createClass({
    handleSave(schedule) {
        CreateMedicationScheduleActions.createSingle(this.props.patient.id, schedule);
    },

    render() {                        
        return (
            /*jshint ignore:start */
            <div className="well">
                <CreateSingleScheduleFormComponent onSave={this.handleSave} />
                <p><b>Bemærk:</b>  Brug kunne denne formular hvis patienten ikke tager denne medicin fast.</p>
            </div>
            /*jshint ignore:end */
        );
    }
});

export default React.createClass({
    statics: {
        willTransitionTo(transition, params) {
            var patientId = parseInt(params.patientId);

            PatientMedicationActions.findAllToday(patientId);
        }
    },

    mixins: [State], 

    getState() {
        var patientId = this.props.patient.id;

        return {
            schema: PatientMedicationStore.getSchema(patientId)
        };
    },

    getInitialState() {
        return this.getState();
    },

    componentDidMount() {
        PatientMedicationStore.addChangeListener(this.onChange);
    },

    componentWillUnmount() {
        PatientMedicationStore.removeChangeListener(this.onChange);
    },

    render() {
        var patient = this.props.patient;

        return (
            /*jshint ignore:start */
            <div>
                <div className="row">
                    <div className="col-md-8">
                        <h4>Dagens skema</h4>
                        <MedicationSchema patientId={patient.id} schema={this.state.schema} />
                    </div>

                    <div className="col-md-4">
                        <h4>Udlevering af andet medicin</h4>
                        <ProvideSingleMedication patient={patient} />
                    </div>
                </div>
            </div>
            /*jshint ignore:end */
        );
    },

    onChange() {
        this.setState(this.getState());
    }
});