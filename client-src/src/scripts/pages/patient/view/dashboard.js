/**
 *
 * @jsx React.DOM
 */

// Dependencies
var React = require("react");
var { State, Link } = require("react-router");

// Stores
var PatientTimelineStore = require("../../../stores/PatientTimelineStore");

// Actions
var { PatientTimelineActions } = require("../../../actions/client/PatientActions");

// Constants
var LoadingConstants = require("../../../constants/LoadingConstants");

// Components
var PanelComponent = require("../../../components/utils/panel");
var TimelineComponent = require("../../../components/timeline/timeline");
var { Text } = require("../../../components/utils/loading");
var TimelinePostFormComponent = require("../../../components/patient/timelinePostForm");


export default React.createClass({
    statics: {
        willTransitionTo(transition, params) {
            var patientId = parseInt(params.patientId);

            PatientTimelineActions.findAll(patientId);
        }
    },
    
    getState() {
        var patientId = this.props.patient.id;
        
        return {
            loadingState: PatientTimelineStore.getLoadingState(patientId),
            entries: PatientTimelineStore.findAll(patientId)
        };
    },

    getInitialState() {
        return this.getState();
    },

    componentDidMount() {
        PatientTimelineStore.addChangeListener(this.onChange);
    },

    componentWillUnmount() {
        PatientTimelineStore.removeChangeListener(this.onChange);
    },

    mixins: [State], 

    render() {
        var patient = this.props.patient,
            patientName = patient.firstname + " " + patient.lastname;
        
        return (
            /*jshint ignore:start */
            <div>
                <h1 className="page-header">{patientName}</h1>

                <div className="row">
                    <div className="col-md-7">
                        <TimelinePostFormComponent patientId={this.props.patient.id} className="" />
                        <TimelineComponent key="timeline" entries={this.state.entries} />
                    </div>
                </div>
            </div>
            /*jshint ignore:end */
        );
    },
    
    onChange() {
        this.setState(this.getState());
    }
});