/**
 *
 * @jsx React.DOM
 */

// Dependencies
var React = require("react");

// Stores

// Models
const { AddressRequest } = require("../../../models");

// Actions
var { PatientAddressActions } = require("../../../actions/client/PatientActions");

// Constants

// Components
var AddressForm = require("../../../Components/AddressForm");

export default React.createClass({
    handleSubmit(street, houseNumber, city, postalCode, isPrimary) {
        const { patient } = this.props;

        PatientAddressActions.add(patient.id, new AddressRequest({
            street: street,
            houseNumber: houseNumber,
            city: city,
            postalCode: postalCode,
            isPrimary: isPrimary,
            clearCurrentPrimary: isPrimary
        }));
    },

    render() {
        const state = this.state;
        const { patient } = this.props;

        const patientName = `${patient.firstname} ${patient.lastname}`;

        return (
            /*jshint ignore:start */
            <div>
                <AddressForm onSubmit={this.handleSubmit} />
            </div>
            /*jshint ignore:end */
        );
    }
});