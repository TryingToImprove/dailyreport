/**
 *
 * @jsx React.DOM
 */

// Dependencies
var React = require("react");
var { State } = require("react-router");

// Stores
var EmployeesStore = require("../../../stores/EmployeesStore");
var PatientContactPersonStore = require("../../../stores/PatientContactPersonStore");

// Actions
var { PatientContactPersonActions } = require("../../../actions/client/PatientActions");
var EmployeeActions = require("../../../actions/client/EmployeeActions");

// Components
var CreateContactPersonForm  = require('../../../components/patient/createContactPersonForm');
var ContactPersonsTable = require('../../../components/patient/tableContactPerson');

export default React.createClass({
    statics: {
        willTransitionTo(transition, params) {
            var patientId = parseInt(params.patientId);

            // Temporay fix, for employee typeahead
            EmployeeActions.findAll();
        
            // Fetch all the contact persons
            PatientContactPersonActions.findAll(patientId); 
        }
    },

    mixins: [State],    
    
    getInitialState() {
        return this.getState();
    },

    getState() {
        var patientId = this.props.patient.id;

        return {                 
            contactPersons: PatientContactPersonStore.findAll(patientId)
        };
    },
    
    componentWillMount() {
        PatientContactPersonStore.addChangeListener(this.onChange);
    },

    componentWillUnmount() {
        PatientContactPersonStore.removeChangeListener(this.onChange);
    },

    render() {
        var patient = this.props.patient,
            patientName = patient.firstname + " " + patient.lastname;

        return (
            /*jshint ignore:start */
            <div>
                <h1 className="page-header">{patientName}</h1>

                <div className="row">
                    <div className="col col-md-7">
                        <ContactPersonsTable contactPersons={this.state.contactPersons} patient={patient} />
                    </div>
                    <div className="col col-md-5">
                        <CreateContactPersonForm patientId={this.props.patient.id} />
                    </div>
                </div>
            </div>
            /*jshint ignore:end */
        );
    },

    onChange() {
        this.setState(this.getState());
    }
});