/**
 *
 * @jsx React.DOM
 */

// Dependencies
var React = require("react");
var { State, Link } = require("react-router");

import { PureRenderMixin } from "react/addons";

// Stores       
var PatientContactsStore = require("../../../stores/PatientContactsStore");

// Actions
var { PatientContactsActions } = require("../../../actions/client/PatientActions");

// Constants                
var LoadingConstants = require("../../../constants/LoadingConstants");

// Components        
var { Text } = require("../../../components/utils/loading");
var ContactInformationFormItem = require("../../../components/contactInformationFormItem");

export default React.createClass({
    statics: {
        willTransitionTo (transition, params) {
            var patientId = parseInt(params.patientId);

            PatientContactsActions.getGroups(patientId);
        }
    },

    getState() {
        var patientId = this.props.patient.id;

        return {
            groups: PatientContactsStore.getGroups(patientId)
        };
    },

    componentDidMount() {
        PatientContactsStore.addChangeListener(this.onChange);
    },

    componentWillUnmount() {
        PatientContactsStore.removeChangeListener(this.onChange);
    },

    getInitialState() {
        return this.getState()
    },

    mixins: [State], 

    handleSubmit(e) {
        var firstnameDOM = this.refs.firstname.getDOMNode(),
            lastnameDOM = this.refs.lastname.getDOMNode(),
            titleDOM = this.refs.title.getDOMNode(),
            groupsDOM = this.refs.groups.getDOMNode(),
            contactInformations = this.refs.contactInformations;

        // Execute actions
        PatientContactsActions.add(this.props.patient.id, {
            firstname: firstnameDOM.value,
            lastname: lastnameDOM.value,
            title: titleDOM.value,
            groups: groupsDOM.value.split(" "),
            contactInformations: contactInformations.getValue()
        });

        e.preventDefault();
    },

    addContactInformation(e) {
        // Add a new contact information item
        this.refs.contactInformations.add(1);

        e.preventDefault();
    },

    render() {
        return (
            /*jshint ignore:start */
            <div>
                <form onSubmit={this.handleSubmit} key="new-contact-form" className="form-horizontal">
                    <div className="form-group">
                        <label className="col-sm-2 control-label">Fornavn</label>
                        <div className="col-sm-10">
                            <input type="text" className="form-control" ref="firstname" />
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="col-sm-2 control-label">Efternavn</label>
                        <div className="col-sm-10">
                            <input type="text" className="form-control" ref="lastname" />
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="col-sm-2 control-label">Titel</label>
                        <div className="col-sm-10">
                            <input type="text" className="form-control" ref="title" />
                            <span className="help-block">Kontaktens titel, eks. mor, far, sagsbehandler, skolelærer.</span>
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="col-sm-2 control-label">Grupper</label>
                        <div className="col-sm-10">
                            <input type="text" className="form-control" ref="groups" />
                        </div>
                    </div>

                    <div className="form-group">
                        <label className="col-sm-2 control-label">Kontaktinformationer</label>
                        <div className="col-sm-10">
                            <ContactInformationFormItem ref="contactInformations" classes={null} initialAmount={1} />

                            <button className="btn btn-default" onClick={this.addContactInformation}>
                                <span className="glyphicon glyphicon-plus"></span> Tilføj ny Kontaktinformation
                            </button>  
                        </div>
                    </div>

                    <div className="form-group">
                        <div className="col-sm-offset-2 col-md-10">
                            <input type="submit" value="Gem kontakt" className="btn btn-success" />
                        </div>
                    </div>

                </form>
            </div>
            /*jshint ignore:end */
        );
    },
    
    onChange() {
        this.setState(this.getState());
    }
});