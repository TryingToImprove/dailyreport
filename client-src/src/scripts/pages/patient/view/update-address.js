/**
 *
 * @jsx React.DOM
 */

// Dependencies
var React = require("react");

// Stores
var PatientAddressStore = require("../../../stores/PatientAddressStore");

// Models
const { AddressRequest } = require("../../../models");

// Actions
var { PatientAddressActions } = require("../../../actions/client/PatientActions");

// Constants

// Components
var AddressForm = require("../../../Components/AddressForm");

export default React.createClass({
    statics: {
        willTransitionTo(transition, params) {
            const addressId = parseInt(params.addressId);
            const patientId = parseInt(params.patientId);

            PatientAddressActions.findById(patientId, addressId);
        }
    },

    getState() {
        const { patient, addressId } = this.props;

        return {
            address: PatientAddressStore.findById(patient.id, addressId)
        }
    },

    getInitialState() {
        return this.getState();
    },

    componentDidMount() {
        PatientAddressStore.addChangeListener(this.onChange);
    },

    componentWillUnmount() {
        PatientAddressStore.removeChangeListener(this.onChange);
    },

    handleSubmit(street, houseNumber, city, postalCode, isPrimary) {
        const { patient } = this.props;
        const { address } = this.state;

        PatientAddressActions.update(patient.id, address.id, new AddressRequest({
            street: street,
            houseNumber: houseNumber,
            city: city,
            postalCode: postalCode,
            isPrimary: isPrimary,
            clearCurrentPrimary: isPrimary
        }));
    },

    handleRemove() {
        const { patient } = this.props;
        const { address } = this.state;

        PatientAddressActions.remove(patient.id, address.id);
    },

    render() {
        const { address } = this.state;
        const { patient } = this.props;

        const patientName = `${patient.firstname} ${patient.lastname}`;

        if (typeof(address) === "undefined")
            return null;

        return (
            /*jshint ignore:start */
            <div>
                <AddressForm key={"addressForm-" + address.id} disableRemove={false} onRemove={this.handleRemove} onSubmit={this.handleSubmit} {...address} />
            </div>
            /*jshint ignore:end */
        );
    },

    onChange() {
        this.replaceState(this.getState());
    }
});