/**
 *
 * @jsx React.DOM
 */

// Dependecies
var React = require("react");
var { State, Link } = require("react-router");

// Models
var ScheduleType = require("../../../domain/models/ScheduleType");

// Stores
var PatientMedicationStore = require("../../../stores/PatientMedicationStore");

// Components
var MedicationScheduleComponent = require("../../../components/medication/medicationSchedule");

// Actions
var { PatientMedicationActions } = require("../../../actions/client/PatientActions");

// Helpers    
var moment = require("../../../utils/moment");

// React Component
export default React.createClass({
    statics: {
        willTransitionTo(transition, params) {
            var patientId = parseInt(params.patientId);

            PatientMedicationActions.findAll(patientId);
        }
    },

    getState() {
        var patientId = this.props.patient.id;

        return {
            schedules: PatientMedicationStore.findAll(patientId)
        };
    },

    getInitialState() {
        return this.getState();
    },

    componentDidMount() {
        PatientMedicationStore.addChangeListener(this.onChange);
    },

    componentWillUnmount() {
        PatientMedicationStore.removeChangeListener(this.onChange);
    },

    mixins: [State], 

    renderMedicationScheduleTables() {
        var patientId = this.props.patient.id;

        if (this.state.schedules === null) {   
            return (
                /*jshint ignore:start */
                <p>Henter medicin skemaer</p>
                /*jshint ignore:end */
            );
        }

        return (
            /*jshint ignore:start */
            <MedicationScheduleComponent patientId={patientId} schedules={this.state.schedules} />
            /*jshint ignore:end */
        );
    }, 

    render() {            
        return (
            /*jshint ignore:start */
            <div>
                <h4>Medicinering</h4>
                {this.renderMedicationScheduleTables()}
            </div>
            /*jshint ignore:end */
        );
    },

    onChange() {
        this.setState(this.getState());
    }
});