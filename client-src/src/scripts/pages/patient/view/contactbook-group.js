/**
 *
 * @jsx React.DOM
 */

// Dependencies
var React = require("react");
import { PureRenderMixin } from "react/addons";
var { State, Link } = require("react-router");

// Stores
var PatientContactsStore = require("../../../stores/PatientContactsStore");

// Actions
var {  PatientContactsActions } = require("../../../actions/client/PatientActions");

// Constants

// Components
import ContactItem from "../../../components/contactItem";

export default React.createClass({
    statics: {
        willTransitionTo (transition, params) {
            var patientId = parseInt(params.patientId);
            var groupId = parseInt(params.groupId);

            PatientContactsActions.findByGroup(patientId, groupId);
        }
    },

    getState() {
        var patientId = this.props.patient.id;
        var groupId = this.props.groupId;

        return {
            contacts: PatientContactsStore.findByGroup(patientId, parseInt(groupId))
        };
    },

    componentDidMount() {
        PatientContactsStore.addChangeListener(this.onChange);
    },

    componentWillUnmount() {
        PatientContactsStore.removeChangeListener(this.onChange);
    },

    getInitialState() {
        return this.getState()
    },

    mixins: [State, PureRenderMixin],

    render() {
        var patient = this.props.patient,
            patientName = patient.firstname + " " + patient.lastname;

        var contacts = this.state.contacts.map(x => {
            return (
                /*jshint ignore:start*/
                <ContactItem {...x} key={x.id} />
                /*jshint ignore:end*/
            );
        }).toArray();

        return (
            /*jshint ignore:start */
            <div>
                <div key="contacts">
                    {contacts}
                </div>
            </div>
            /*jshint ignore:end */
        );
    },

    onChange() {
        this.setState(this.getState());
    }
});