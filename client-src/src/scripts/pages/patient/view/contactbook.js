/**
 *
 * @jsx React.DOM
 */

// Dependencies
var React = require("react");
var { State, Link } = require("react-router");

// Stores       
var PatientContactsStore = require("../../../stores/PatientContactsStore");

// Actions
var {  PatientContactsActions } = require("../../../actions/client/PatientActions");

// Constants         

// Components        
import ContactItem from "../../../components/contactItem";

export default React.createClass({
    statics: {
        willTransitionTo (transition, params) {
            var patientId = parseInt(params.patientId);

            PatientContactsActions.findFrequent(patientId);
        }
    },

    getState() {
        var patientId = this.props.patient.id;

        return {
            frequentContacts: PatientContactsStore.findFrequent(patientId)
        };
    },

    componentDidMount() {
        PatientContactsStore.addChangeListener(this.onChange);
    },

    componentWillUnmount() {
        PatientContactsStore.removeChangeListener(this.onChange);
    },

    getInitialState() {
        return this.getState()
    },

    mixins: [State], 

    render() {
        var patient = this.props.patient,
            patientName = patient.firstname + " " + patient.lastname;

        var frequentContacts = this.state.frequentContacts.map(x => {
            return (
                /*jshint ignore:start*/
                <ContactItem {...x} key={x.id} />
                /*jshint ignore:end*/
            );
        }).toArray();

        return (
            /*jshint ignore:start */
            <div>
                <div key="contacts">
                    {frequentContacts}
                </div>
            </div>
            /*jshint ignore:end */
        );
    },
    
    onChange() {
        this.setState(this.getState());
    }
});