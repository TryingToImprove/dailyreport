/**
 *
 * @jsx React.DOM
 */

// Dependecies
var React = require("react");
var { State } = require("react-router");

// Components
var CreateScheduleFormComponent = require("../../../components/medication/createScheduleForm");

// Actions
var CreateMedicationScheduleActions = require("../../../actions/client/CreateMedicationScheduleActions");

// React Component
export default React.createClass({
    
    mixins: [State], 

    handleSave(schedule) {
        CreateMedicationScheduleActions.save(this.props.patient.id, schedule);
    },

    render() {
        return (
            /*jshint ignore:start */
            <div>
                <h1 className="page-header">Opret nyt medicinskema</h1>               
                <CreateScheduleFormComponent onSave={this.handleSave}  />
            </div>
            /*jshint ignore:end */
        );
    }
});