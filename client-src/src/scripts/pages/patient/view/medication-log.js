/**
 *
 * @jsx React.DOM
 */

// Dependecies
var React = require("react");
var { State, Link } = require("react-router");

// Models
var ScheduleType = require("../../../domain/models/ScheduleType");

// Stores
var PatientMedicationHistoryStore = require("../../../stores/PatientMedicationHistoryStore");

// Components
var MedicationHistoryTableComponent = require("../../../components/medication/medicationHistoryTable");

// Actions
var { PatientMedicationActions } = require("../../../actions/client/PatientActions");

// Helpers    
var moment = require("../../../utils/moment");

// React Component
export default React.createClass({
    statics: {
        willTransitionTo(transition, params) {
            var patientId = parseInt(params.patientId);

            PatientMedicationActions.history.findAll(patientId);
        }
    },

    getState() {
        var patientId = this.props.patient.id;

        return {
            histories: PatientMedicationHistoryStore.findAll(patientId)
        };
    },

    getInitialState() {
        return this.getState();
    },

    componentDidMount() {
        PatientMedicationHistoryStore.addChangeListener(this.onChange);
    },

    componentWillUnmount() {
        PatientMedicationHistoryStore.removeChangeListener(this.onChange);
    },

    mixins: [State], 
    
    render() {            
        return (
            /*jshint ignore:start */
            <div>
                <MedicationHistoryTableComponent histories={this.state.histories} />
            </div>
            /*jshint ignore:end */
        );
    },

    onChange() {
        this.setState(this.getState());
    }
});