/**
 *
 * @jsx React.DOM
 */

// Dependencies
var React = require("react");
var { Link, State } = require("react-router");

// Stores
var PatientContactInformationStore = require("../../../stores/PatientContactInformationStore");
var PatientContactPersonStore = require("../../../stores/PatientContactPersonStore");
var PatientStore = require("../../../stores/PatientStore");
var PatientAddressStore = require("../../../stores/PatientAddressStore");

// Actions
var { PatientActions, PatientContactPersonActions, PatientAddressActions, PatientContactInformationActions } = require("../../../actions/client/PatientActions");

// Constants
var LoadingConstants = require("../../../constants/LoadingConstants");

// Components
var { Text } = require("../../../components/utils/loading");
var ContactPersonsComponent = require("../../../components/patient/contactPersons");
var ContactInformationFormItem = require("../../../components/contactInformationFormItem");
var AddressItem = require("../../../Components/AddressItem");

var UpdatePatientFormComponent = React.createClass({
    getState() {
        return {
            isUpdating: PatientStore.getIsUpdatingState()
        };
    },

    componentDidMount() {
        PatientStore.addChangeListener(this.onChange);
    },

    componentWillUnmount() {
        PatientStore.removeChangeListener(this.onChange);
    },

    getInitialState() {
        return this.getState();
    },

    render(){
        return (
            /*jshint ignore:start*/
            <form onSubmit={this.handleSubmit}>
                <div className="form-horizontal">
                    <div className="form-group">
                        <label className="control-label col-md-4">Fornavn</label>
                        <div className="col-md-8">
                            <input type="text" ref="firstname" className="form-control" defaultValue={this.props.firstname} />
                        </div>
                    </div>

                    <div className="form-group">
                        <label className="control-label col-md-4">Efternavn</label>
                        <div className="col-md-8">
                            <input type="text" ref="lastname" className="form-control" defaultValue={this.props.lastname} />
                        </div>
                    </div>

                    <div className="form-group">
                        <label className="control-label col-md-4">Fødselsdag</label>
                        <div className="col-md-8">
                            <input type="text" ref="birthDate" className="form-control" defaultValue={this.props.birthDate} />
                        </div>
                    </div>

                    <div className="form-group">
                        <label className="control-label col-md-4">CPR</label>
                        <div className="col-md-8">
                            <input type="text" ref="securityNumber" className="form-control" defaultValue={this.props.securityNumber} />
                        </div>
                    </div>

                    <div className="form-group">
                        <div className="col-md-offset-2 col-md-10">
                            <input type="submit" disabled={this.state.isUpdating} value="Gem ændringer" className="btn btn-default" />
                        </div>
                    </div>
                </div>
            </form>
            /*jshint ignore:end*/
        );
    },
    handleSubmit() {
        var patientId = this.props.patientId,
            firstname = this.refs.firstname.getDOMNode().value,
            lastname = this.refs.lastname.getDOMNode().value,
            birthDate = this.refs.birthDate.getDOMNode().value,
            securityNumber = this.refs.securityNumber.getDOMNode().value;

        PatientActions.update(patientId, firstname, lastname, birthDate, securityNumber);
    },

    onChange() {
        this.setState(this.getState());
    }
});

var ContactInformationItemComponent = React.createClass({
    handleRemove(e) {
        PatientContactInformationActions.remove(this.props.patientId, this.props.id);

        e.preventDefault()
    },

    render() {
        var type = this.props.type,
            value = this.props.data;

        return (
            /*jshint ignore:start*/
            <div>
                <b>{type}</b><br />
                {value}

                <a href="#" onClick={this.handleRemove}>Fjern</a>
            </div>
            /*jshint ignore:end*/
        );
    }
});

var ContactInformationsComponent = React.createClass({
    getState() {
        var patientId = this.props.patient.id;

        return {
            contactInformations: PatientContactInformationStore.findAll(patientId)
        };
    },

    componentDidMount() {
        PatientContactInformationStore.addChangeListener(this.onChange);
    },

    componentWillUnmount() {
        PatientContactInformationStore.removeChangeListener(this.onChange);
    },

    getInitialState() {
        return this.getState();
    },

    saveContactInformation(e) {
        var contactInformation = this.refs.contactInformations.getValue()[0];

        PatientContactInformationActions.add(this.props.patient.id, contactInformation.label, contactInformation.value);

        e.preventDefault();
    },

    render() {
        var patientId = this.props.patient.id;
        var items = this.state.contactInformations.map(item => (
            /*jshint ignore:start*/
            <ContactInformationItemComponent patientId={patientId} {...item} />
            /*jshint ignore:end*/
        )).toArray();

        return (
            /*jshint ignore:start */
            <div>
                <h3 className="no-top-margin">Kontaktinformationer</h3>
                {items}
                <form className="form-horizontal" onSubmit={this.saveContactInformation}>
                    <ContactInformationFormItem ref="contactInformations" classes={null} disableRemove={true} initialAmount={1} />

                    <button type="submit" className="btn btn-default">
                        <span className="glyphicon glyphicon-plus"></span> Gem kontaktinformation
                    </button>
                </form>
            </div>
            /*jshint ignore:end*/
        );
    },

    onChange() {
        this.setState(this.getState());
    }
});

export default React.createClass({
    statics: {
        willTransitionTo (transition, params) {
            var patientId = parseInt(params.patientId);

            PatientContactInformationActions.findAll(patientId);
            PatientContactPersonActions.findCurrents(patientId);
            PatientAddressActions.findPrimary(patientId);
        }
    },

    getState() {
        var patientId = this.props.patient.id;

        return {
            contactPersons: PatientContactPersonStore.getActives(patientId),
            addresses: PatientAddressStore.findPrimary(patientId)
        };
    },

    componentDidMount() {
        PatientContactPersonStore.addChangeListener(this.onChange);
    },

    componentWillUnmount() {
        PatientContactPersonStore.removeChangeListener(this.onChange);
    },

    getInitialState() {
        return this.getState()
    },

    mixins: [State],

    render() {
        const state = this.state;
        const { patient } = this.props;
        const patientName = `${patient.firstname} ${patient.lastname}`;

        const addresses = state.addresses.map(x => {
            return (
                /*jshint ignore:start */
                <AddressItem key={x.id} patientId={patient.id} {...x} />
                /*jshint ignore:end */
            );
        }).toArray();

        return (
            /*jshint ignore:start */
            <div>
                <h1 className="page-header">{patientName}</h1>
                <div className="row">
                    <div className="col-md-7">
                        <UpdatePatientFormComponent patientId={patient.id} firstname={patient.firstname} lastname={patient.lastname} birthDate={patient.birthDate} />
                    </div>
                    <div className="col-md-5">
                        <ContactInformationsComponent patient={patient} />
                        <hr />
                        <Link to="patientAddress" params={{patientId: patient.id}}>Tilføj adresse</Link>
                        {addresses}
                        <hr />
                        <section>
                            <h3>Kontaktpersoner</h3>
                            <ContactPersonsComponent patient={patient} contactPersons={this.state.contactPersons} />
                        </section>
                    </div>
                </div>
            </div>
            /*jshint ignore:end */
        );
    },

    onChange() {
        this.setState(this.getState())
    }
});