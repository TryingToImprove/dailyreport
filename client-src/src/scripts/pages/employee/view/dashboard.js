/**
 *
 * @jsx React.DOM
 */

(function () {
    "use strict";

    var React = require("react"),
        Router = require("react-router");
   
    module.exports = React.createClass({        
        mixins: [Router.State], 
                
        render: function() {
            var employee = this.props.employee,
                name = employee.firstname + " " + employee.lastname;

            return (
                /*jshint ignore:start */
                <div>
                    <h1 className="page-header">{name}</h1>
                    
                </div>
                /*jshint ignore:end */
            );
        },
        
    });
}());