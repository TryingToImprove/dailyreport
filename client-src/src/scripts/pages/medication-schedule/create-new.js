/**
 *
 * @jsx React.DOM
 */

// Dependencies
var React = require("react");

// Components
var CreateScheduleFormComponent = require("../../components/medication/createScheduleForm");

// React Component
export default React.createClass({
    render() {
        return (
            /*jshint ignore:start */
            <div>
                <h1 className="page-header">Opret nyt medicinskema</h1>               
                <CreateScheduleFormComponent patientId={1020} />
            </div>
            /*jshint ignore:end */
        );
    }
});