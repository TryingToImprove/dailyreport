/**
 *
 * @jsx React.DOM
 */

// Dependencies
var React = require("react");
var { Link } = require("react-router");

export default React.createClass({
    render() {
        return (
            /*jshint ignore:start */
            <div className="page-header">
                <Link to="medicationScheduleCreateNew" className="btn pull-right btn-success">Opret nyt skema</Link>
                <h1>Medicinskemaer</h1>               
            </div>
            /*jshint ignore:end */
        );
    }
});