/**
 *
 * @jsx React.DOM
 */

(function () {
    "use strict";

    var React = require("react"),
        Router = require("react-router"),
        AuthenticationActions = require("../../actions/client/AuthenticationActions"),
        
        PanelComponent = require("../../components/utils/panel"),
        
        ApplicationStore = require("../../stores/ApplicationStore"),
        AuthenticationStore = require("../../stores/AuthenticationStore"),
        
        Router = require("react-router"),
        Link = Router.Link;
        
    function getState(isFirstName) {
        return {
            isLoading: AuthenticationStore.getLoadingState(),
            authenticationState: AuthenticationStore.getAuthenticationState()
        };
    }
    
    var AuthenticationFormComponent = React.createClass({        
        getInitialState: function() {
            return getState();
        },
        
        componentDidMount: function() {
            AuthenticationStore.addChangeListener(this.onChange);
        },
        
        componentWillUnmount: function() {
            AuthenticationStore.removeChangeListener(this.onChange);
        },
        
        render: function() {
            /*jshint ignore:start */
            var stateMessage = (this.state.authenticationState === "failed")
                ? (<span className="help-block">Vi kunne ikke logge dig ind</span>)
                : undefined;
            /*jshint ignore:end */
                   
            return (
                /*jshint ignore:start */                
                <form onSubmit={this.handleSubmit} className="form-horizontal">
                    <div className="form-group">
                        <label className="col-md-2 control-label">Brugerid:</label>
                        <div className="col-md-10">
                            <input type="text" className="form-control" ref="username" defaultValue="olla" />
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="col-md-2 control-label">Kodeord:</label>
                        <div className="col-md-10">
                            <input type="password" className="form-control" ref="password" defaultValue="123123qwe" />
                        </div>
                    </div>        
                    {stateMessage}
                    <div className="form-group">
                        <div className="col-md-offset-2 col-md-10">
                            <input type="submit" disabled={this.state.isLoading} value="Log ind" className="btn btn-default" />
                        </div>
                    </div>
                </form>
                /*jshint ignore:end */
            );
        },
        handleSubmit: function (e) {
            e.preventDefault();
                                    
            var username = this.refs.username.getDOMNode().value,
                password = this.refs.password.getDOMNode().value;
            
            this.props.onAuthenticate(username, password);
        },
        onChange: function(){
            this.setState(getState());
        }
    });
   
    module.exports = React.createClass({
        statics: {
            willTransitionTo() {
                AuthenticationActions.resetAuthenticate();
            }
        },
                                                        
        mixins: [Router.State],
                                                                
        render: function() {
            return (
                /*jshint ignore:start */
                <div className="container">
                    <div className="row">
                        <div className="col-md-6 col-md-offset-3">
                            <h1 className="page-header">Dagsrapporten</h1>   
                
                            <PanelComponent title="Log ind med din konto">
                                <AuthenticationFormComponent onAuthenticate={this.onAuthenticate} />
                            </PanelComponent>                
                
                            <ul className="list-unstyled">
                                <li><Link to="registerAccount"><b>Prøv dagsrapporten</b> - opret en prøve konto</Link></li>
                                <li><a href="#">Problemer med at logget ind?</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                /*jshint ignore:end */
            );
        },
            
        onAuthenticate: function(username, password) {
                        
            var query = this.getQuery(),
                authenticationOptions = {};
            
            if (typeof(query.redirectUrl) !== "undefined") {
                authenticationOptions.redirectUrl = query.redirectUrl;
            }
            
            AuthenticationActions.tryAuthenticate(username, password, authenticationOptions);
        }
    });
}());