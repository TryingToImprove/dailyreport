/**
 *
 * @jsx React.DOM
 */

(function () {
    "use strict";

    var React = require("react"),
        Router = require("react-router"),
        AccountAcounts = require("../../actions/client/AccountActions"),
        
        PanelComponent = require("../../components/utils/panel"),
        
        RegisterAccountStore = require("../../stores/RegisterAccountStore"),
        
        Router = require("react-router"),
        Link = Router.Link;

    function getState() {
        return {
            isLoading: RegisterAccountStore.getLoadingState(),
            creationState: RegisterAccountStore.getCreationState(),
            employee: RegisterAccountStore.getEmployee()
        };
    }
    
    var RegisterAccountFormComponent = React.createClass({
        render: function() {            
            var stateMessage = (this.props.creationState === "failed")
                ? (
                    <span className="help-block">Beklager, men der skete en fejl og vi kunne ikke tilmelde dig..</span>
                )
                : undefined;
                   
            return (
                /*jshint ignore:start */                
                <form onSubmit={this.handleSubmit} className="form-horizontal">            
                    <div className="form-group">
                        <label className="col-md-2 control-label">Fornavn:</label>
                        <div className="col-md-4">
                            <input type="text" className="form-control" ref="firstname" />
                        </div>
                        <label className="col-md-2 control-label">Efternavn:</label>
                        <div className="col-md-4">
                            <input type="text" className="form-control" ref="lastname" />
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="col-md-2 control-label">Firma:</label>
                        <div className="col-md-10">
                            <input type="text" className="form-control" ref="organizationName" />
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="col-md-2 control-label">Email:</label>
                        <div className="col-md-10">
                            <input type="text" className="form-control" ref="email" />
                        </div>
                    </div>
                     <div className="form-group">
                        <div className="col-md-offset-2 col-md-10">
                            <input type="submit" value="Prøv nu" disabled={this.props.isLoading} className="pull-left btn btn-default" />
                            {stateMessage}
                        </div>
                    </div>
                </form>
                /*jshint ignore:end */
            );
        },
        handleSubmit: function (e) {
            e.preventDefault();
            
            var firstname = this.refs.firstname.getDOMNode().value,
                lastname = this.refs.lastname.getDOMNode().value,
                organizationName = this.refs.organizationName.getDOMNode().value,
                email = this.refs.email.getDOMNode().value;
                
            AccountAcounts.register(firstname, lastname, organizationName, email);
        }
    });
    
    var AccountRegisteredComponent = React.createClass({
        render: function() {
            var employee = this.props.employee;
            
            return (
                /*jshint ignore:start */      
                <p>
                    Din konto er blevet oprettet, og er er blevet en sendt en mail til {employee.email} med et link til at fuldføre registeringen
                </p>
                /*jshint ignore:end */
            );
        }
    });
   
    module.exports = React.createClass({
        statics: {
            willTransitionTo() {
               AccountAcounts.resetRegistration();
            }
        },
        
        mixins: [Router.State],
        
        getInitialState: function() {
            return getState();
        },
                
        componentDidMount: function() {
            RegisterAccountStore.addChangeListener(this.onChange);
        },
        componentWillUnmount: function() {
            RegisterAccountStore.removeChangeListener(this.onChange);
        },
        
        getCurrentView: function() {
            switch(this.state.creationState) {
                case "success": 
                    return (
                        /*jshint ignore:start */
                        <PanelComponent title="Din konto er blevet oprettet">
                            <AccountRegisteredComponent employee={this.state.employee} />
                        </PanelComponent>    
                        /*jshint ignore:end */
                    );
            }
            
            return (
                /*jshint ignore:start */
                <PanelComponent title="Prøv en gratis konto">
                    <RegisterAccountFormComponent creationState={this.state.creationState} isLoading={this.state.isLoading} />
                </PanelComponent>    
                /*jshint ignore:end */
            );  
        },
        
        render: function() {
            var currentView = this.getCurrentView();
                        
            return (
                /*jshint ignore:start */
                <div className="container">
                    <div className="row">
                        <div className="col-md-10 col-md-offset-1">
                            <h1 className="page-header">Dagsrapporten</h1>  
                
                            <div className="row">
                                <div className="col-md-9">
                                    {currentView}              

                                    <ul className="list-unstyled">
                                        <li><Link to="authentication">Tilbage til log ind</Link></li>
                                    </ul>
                                </div>
                                <div className="col-md-3">
                                    <h4>Fordele ved Dagsrapporten</h4>
                                    <ul>
                                        <li>Høj sikkerhed</li>
                                        <li>Medicinering</li>
                                        <li>Logbøger</li>
                                        <li>Kontaktbøger</li>
                                        <li><a href="#">Og meget mere</a></li>
                                    </ul>

                                    <h4>Brug for hjælp?</h4>
                                    <p>
                                        Hvis du har brug for hjælp, må du meget gerne kontakt os på mail <a href="#">kontakt@dagsrapporten.dk</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                /*jshint ignore:end */
            );
        },
        
        onChange: function() {
            this.setState(getState());
        }
    });
}());