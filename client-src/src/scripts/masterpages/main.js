/**
 *
 * @jsx React.DOM
 */

// Dependencies
var React = require("react");
var { RouteHandler, State } = require("react-router");

// Stores
var ApplicationStore = require("../stores/ApplicationStore");

// Components
var TopBarComponent = require("../components/topbar");
var NavigationComponent = require("../components/navigation");

var TransitionGroup = require('react/lib/ReactCSSTransitionGroup'); 

function getState() {
    return {
        isAuthenticated: ApplicationStore.getAuthenticationState(),
        account: ApplicationStore.getCurrentAccount(),
        organization: ApplicationStore.getCurrentOrganization()
    };
}

export default React.createClass({
    mixins: [State],
    
    getInitialState() {
        return getState();
    },
    
    componentDidMount() {
        ApplicationStore.addChangeListener(this.onChange);
    },
    
    componentWillUnmount() {
        ApplicationStore.removeChangeListener(this.onChange);
    },
    
    render() {       
        var navigation = null;

        if (this.state.isAuthenticated) {
            navigation = (
                /*jshint ignore:start */ 
                <NavigationComponent isAuthenticated={this.state.isAuthenticated} organization={this.state.organization} />
                /*jshint ignore:end */
            );
        }

        return (
            /*jshint ignore:start */
            <div>
                <TopBarComponent isAuthenticated={this.state.isAuthenticated} account={this.state.account} />
                {navigation}
                <div className="body-content section-body-content">
                    <RouteHandler {...this.getParams()}/>
                </div>
            </div>
            /*jshint ignore:end */
        );
    },
    onChange(){
        this.setState(getState());
    }
});