/**
 *
 * @jsx React.DOM
 */
/**
 *
 * @jsx React.DOM
 */

// Dependencies
var React = require("react");
var { State, Link, RouteHandler } = require("react-router");

// Stores       
var PatientContactPersonStore = require("../stores/PatientContactPersonStore");
var PatientContactsStore = require("../stores/PatientContactsStore");

// Actions
var { PatientContactPersonActions, PatientContactsActions } = require("../actions/client/PatientActions");

// Constants                

// Components        
var ContactPersonsComponent = require("../components/patient/contactPersons");

export default React.createClass({
    statics: {
        willTransitionTo (transition, params) {
            var patientId = parseInt(params.patientId);

            PatientContactPersonActions.findCurrents(patientId);
            PatientContactsActions.getGroups(patientId);
        }
    },

    getState() {
        var patientId = this.props.patient.id;

        return {
            contactPersons: PatientContactPersonStore.getActives(patientId),
            groups: PatientContactsStore.getGroups(patientId)
        };
    },

    componentDidMount() {
        PatientContactPersonStore.addChangeListener(this.onChange);
        PatientContactsStore.addChangeListener(this.onChange);
    },

    componentWillUnmount() {
        PatientContactPersonStore.removeChangeListener(this.onChange);
        PatientContactsStore.removeChangeListener(this.onChange);
    },

    getInitialState() {
        return this.getState()
    },

    mixins: [State], 

    render() {
        var patient = this.props.patient,
            patientName = patient.firstname + " " + patient.lastname;

        var groups = this.state.groups.map(x => {
            return (
                /*jshint ignore:start*/
                <Link className="list-group-item" to="patientContactbookGroup" params={{ patientId: patient.id, groupId: x.id }} key={x.id}>{x.name}</Link> 
                /*jshint ignore:end*/
            );
        }).toArray();

        return (
            /*jshint ignore:start */
            <div>
                <div className="row">
                    <div className="col-md-8">
                        <RouteHandler {...this.props} />
                    </div>
                    <div className="col-md-4">
                        <section>
                            <h5>Kontaktpersoner</h5>
                            <ContactPersonsComponent patient={patient} contactPersons={this.state.contactPersons} />
                        </section>
                        
                        <hr />

                        <div className="list-group">
                            {groups}
                        </div>
                    </div>
                </div>
            </div>
            /*jshint ignore:end */
        );
    },
    
    onChange() {
        this.setState(this.getState());
    }
});