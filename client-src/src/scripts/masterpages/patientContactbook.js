/**
 *
 * @jsx React.DOM
 */

// Dependencies
var React = require("react");
var { RouteHandler, Link, State } = require("react-router");

module.exports = React.createClass({
   render() {
        return (
            /*jshint ignore:start */
            <div>
                <div className="page-header">
                    <Link to="patientContactbookNew" params={{patientId: this.props.patient.id}} className="btn pull-right btn-success">Opret nyt kontakt</Link>
                    <h1>Kontaktbog</h1>               
                </div>

                <RouteHandler {...this.props} />
            </div>
            /*jshint ignore:end */
        );
    }
});