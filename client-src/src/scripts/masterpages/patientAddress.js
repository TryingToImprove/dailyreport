/**
 *
 * @jsx React.DOM
 */

// Dependencies
var React = require("react");
var { RouteHandler, Link, State } = require("react-router");

var { PatientAddressActions } = require("../actions/client/PatientActions");
var PatientAddressStore = require("../stores/PatientAddressStore");
var AddressItem = require("../Components/AddressItem");

module.exports = React.createClass({
    statics: {
        willTransitionTo (transition, params) {
            const patientId = parseInt(params.patientId);

            PatientAddressActions.findAll(patientId)
        }
    },

    getState() {
        const { patient } = this.props;

        return {
            addresses: PatientAddressStore.findAll(patient.id)
        }
    },

    getInitialState() {
        return this.getState();
    },

    mixins: [State],

    componentDidMount() {
        PatientAddressStore.addChangeListener(this.onChange);
    },

    componentWillUnmount() {
        PatientAddressStore.removeChangeListener(this.onChange);
    },

    render() {
        const state = this.state;
        const { patient } = this.props;

        const patientName = `${patient.firstname} ${patient.lastname}`;

        const addresses = state.addresses.map(x => {
            return (
                /*jshint ignore:start */
                <AddressItem key={x.id} patientId={patient.id} {...x} />
                /*jshint ignore:end */
            );
        }).toArray();

        return (
            /*jshint ignore:start */
            <div>
                <div className="page-header">
                    <h1>{patientName}</h1>
                </div>

                <div className="row">
                    <div className="col-md-7">
                        <RouteHandler {...this.props} />
                    </div>

                    <div className="col-md-5">
                        {addresses}
                    </div>
                </div>
            </div>
            /*jshint ignore:end */
        );
    },

    onChange() {
        this.setState(this.getState());
    }
});