/**
 *
 * @jsx React.DOM
 */

// Dependencies
var React = require("react");
var { State, RouteHandler, Link } = require("react-router");

// Stores
var EmployeesStore = require("../stores/EmployeesStore");

// Actions
var EmployeeActions = require("../actions/client/EmployeeActions");

// Constants
var LoadingConstants = require("../constants/LoadingConstants");

// Components
var { Progress } = require("../components/utils/loading");

export default React.createClass({
    statics: {
        willTransitionTo: function(transition, params) {
            var employeeId = parseInt(params.employeeId);

            EmployeeActions.get(employeeId);
        }
    },

    mixins: [State], 
    
    getState(){
        var employeeId = parseInt(this.props.employeeId);

        return {
            employee: EmployeesStore.get(employeeId)
        };
    },

    getInitialState() {
        // Return the state
        return this.getState();
    },

    componentDidMount() {
        EmployeesStore.addChangeListener(this.onChange);
    },

    componentWillUnmount() {
        EmployeesStore.removeChangeListener(this.onChange);
    },

    render() {
        if (this.state.employee === null) {
            return (
                /*jshint ignore:start */
                <Progress text="Henter informationer omkring den ansatte" />
                /*jshint ignore:end */
            );            
        }

        return (
            /*jshint ignore:start */
                <div className="row">
                    <div className="col-md-2">
                        Hallo...
                    </div>
                    <div className="col-md-10">
                        <RouteHandler employee={this.state.employee} />
                    </div>
                </div>
            /*jshint ignore:end */
        );
    },

    onChange() {
        this.setState(this.getState());
    }
});