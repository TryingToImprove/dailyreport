/**
 *
 * @jsx React.DOM
 */

// Depencenies
var React = require("react");
var { RouteHandler } = require("react-router");

export default React.createClass({
    statics: {
        requiredAuthorized: true
    },

    render() {
        return (
            /*jshint ignore:start */
            <RouteHandler />
            /*jshint ignore:end */
        );
    }
});