/**
 *
 * @jsx React.DOM
 */

// Dependencies
var React = require("react");
var { RouteHandler, Link, State } = require("react-router");

// Stores
var PatientProfileStore = require("../stores/PatientProfileStore");

// Actions
var { PatientActions } = require("../actions/client/PatientActions");

// Constants
var LoadingConstants = require("../constants/LoadingConstants");

// Components
var ProgressLoadingComponent = require("../components/utils/loading").Progress;
var ListItemLink = require("../components/utils/listItemLink");
var TransitionGroup = require('react/lib/ReactCSSTransitionGroup'); 

module.exports = React.createClass({
    statics: {
        willTransitionTo(transition, params) {
            var patientId = parseInt(params.patientId);

            PatientActions.get(patientId);
        }
    },

    mixins: [State], 

    getState() { 
        return {
            patient: PatientProfileStore.get(this.props.patientId)
        };
    },

    getInitialState(){
        // Return the state
        return this.getState();
    },

    componentDidMount() {
        PatientProfileStore.addChangeListener(this.onChange);
    },

    componentWillUnmount() {
        PatientProfileStore.removeChangeListener(this.onChange);
    },

    render() {
        var patient = this.state.patient;

        if (this.state.patient === null) {
            return (
                /*jshint ignore:start */
                <ProgressLoadingComponent text="Henter informationer omkring patienten" />
                /*jshint ignore:end */
            );            
        }

        return (
            /*jshint ignore:start */
                <div className="row">
                    <div className="col-md-2">
                        <ul className="list-group">
                            <li className="list-group-header">{patient.firstname + " " +patient.lastname}</li>
                            <ListItemLink to="patient" params={{patientId: patient.id}}>Tidslinje</ListItemLink>
                            <ListItemLink to="patientAbout" params={{patientId: patient.id}}>Informationer</ListItemLink>
                            <ListItemLink to="patientContactPersons" params={{patientId: patient.id}}>Kontakpersoner</ListItemLink>
                            <ListItemLink routeName="patientMedicationMaster" to="patientMedication" params={{patientId: patient.id}}>Medicinering</ListItemLink>
                            <ListItemLink routeName="patientContactbookMaster" to="patientContactbook" params={{patientId: patient.id}}>Kontaktbog</ListItemLink>
                            <li className="list-group-item"><a href="#">#Handleplaner</a></li>
                        </ul>
                    </div>
                    <div className="col-md-10">
                        <RouteHandler {...this.props} patient={patient} />
                    </div>
                </div>
            /*jshint ignore:end */
        );
    },

    onChange() {
        this.setState(this.getState());
    }
});