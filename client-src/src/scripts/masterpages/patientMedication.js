/**
 *
 * @jsx React.DOM
 */

// Dependencies
var React = require("react");
var { RouteHandler, Link, State } = require("react-router");

module.exports = React.createClass({
   render() {
        return (
            /*jshint ignore:start */
            <div>
                <div className="page-header">
                    <Link to="patientMedicationNew" params={{patientId: this.props.patient.id}} className="btn pull-right btn-success">Opret nyt skema</Link>
                    <Link to="patientMedicationDetailed" params={{patientId: this.props.patient.id}} className="btn pull-right">Detaljeret visning</Link>
                    <Link to="patientMedicationHistory" params={{patientId: this.props.patient.id}} className="btn pull-right">Medicinlog</Link>
                    <Link to="patientMedication" params={{patientId: this.props.patient.id}} className="btn pull-right">Oversigt</Link>
                    <h1>Medicinskema</h1>               
                </div>

                <RouteHandler {...this.props} />
            </div>
            /*jshint ignore:end */
        );
    }
});