/**
 *
 * @jsx React.DOM
 */

// Dependencies
var React = require("react");
var { RouteHandler } = require("react-router");
var ApplicationConfiguration = require("../ApplicationConfiguration");

// Stores
var ApplicationStore = require("../stores/ApplicationStore");

export default React.createClass({
    statics: {
         willTransitionTo: function(transition) {
            if (!ApplicationStore.getAuthenticationState()) {        
                var redirectUrl = transition.path;

                if (transition.path === ApplicationConfiguration.defaultAuthenticatedUrl) {
                    redirectUrl = undefined;
                }

                transition.redirect(ApplicationConfiguration.authenticationUrl, {}, { redirectUrl: redirectUrl});
            }
        }
    },
    
    render() {
        return (
            /*jshint ignore:start */
            <RouteHandler {...this.props}/>
            /*jshint ignore:end */
        );
    }
});