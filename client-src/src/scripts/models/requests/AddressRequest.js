export default class AddressRequest {
    constructor(data){
        this.street = data.street;
        this.houseNumber = data.houseNumber;
        this.city = data.city;
        this.postalCode = data.postalCode;
        this.isPrimary = data.isPrimary;
        this.clearCurrentPrimary = data.clearCurrentPrimary || false;
    }
};