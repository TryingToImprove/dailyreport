/**
 *
 * @jsx React.DOM
 */

(function () {
    "use strict";

    var React = require("react"),
        Router = require('react-router'),
        Route = Router.Route,
        DefaultRoute = Router.DefaultRoute,

        Masterpages = {
            Main: require("./masterpages/main"),
            Patient: require("./masterpages/patient"),
            PatientMedication: require("./masterpages/patientMedication"),
            PatientContactbook: require("./masterpages/patientContactbook"),
            PatientContactbookGroup: require("./masterpages/patientContactbookGroup"),
            PatientAddress: require("./masterpages/patientAddress"),
            Profile: require("./masterpages/profile"),
            Employee: require("./masterpages/employee"),
            MedicationSchedule: require("./masterpages/medication-schedule")
        },

        Modules = {
            Authenticated: require("./masterpages/authenticated")
        },

        Pages = {
            Patient: {
                Index: require("./pages/patient/index"),
                Create: require("./pages/patient/create"),
                View : {
                    Dashboard: require("./pages/patient/view/dashboard"),
                    About: require("./pages/patient/view/about"),
                    AddAddress: require("./pages/patient/view/add-address"),
                    UpdateAddress: require("./pages/patient/view/update-address"),
                    ContactPersons: require("./pages/patient/view/contactPersons"),
                    Medication: require("./pages/patient/view/medication"),
                    MedicationNew: require("./pages/patient/view/medication-new"),
                    MedicationDetailed: require("./pages/patient/view/medication-detailed"),
                    MedicationLog: require("./pages/patient/view/medication-log"),
                    Contactbook: require("./pages/patient/view/contactbook"),
                    ContactbookNew: require("./pages/patient/view/contactbook-new"),
                    ContactbookGroup: require("./pages/patient/view/contactbook-group")
                }
            },
            Account: {
                SignIn: require("./pages/account/signin"),
                Register: require("./pages/account/registerAccount")
            },
            Profile: {
                Index: require("./pages/profile/index")
            },
            Employee: {
                View: {
                    Dashboard: require("./pages/employee/view/dashboard")
                }
            },
            MedicationSchedule: {
                Index: require("./pages/medication-schedule/index"),
                CreateNew: require("./pages/medication-schedule/create-new")
            },
            Dashboard: require("./pages/dashboard")
        };

    module.exports = Router.create({
        location: Router.HashLocation,
        routes: (
            /*jshint ignore:start */
            <Route handler={Masterpages.Main}>
                <Route name="authentication" path="/account/sign-in" handler={Pages.Account.SignIn} />
                <Route name="registerAccount" path="/account/register" handler={Pages.Account.Register} />

                <Route name="authenticated" handler={Modules.Authenticated}>
                    <Route path="/" name="dashboard" handler={Pages.Dashboard} />

                    <Route name="myProfile" path="/my" handler={Masterpages.Profile}>
                        <DefaultRoute handler={Pages.Profile.Index} />
                    </Route>

                    <Route path="/employee/view/:employeeId" name="employeeLayout" handler={Masterpages.Employee}>
                        <Route name="employee" path="/employee/view/:employeeId"  handler={Pages.Employee.View.Dashboard} />
                    </Route>

                    <Route path="/medication/schedules" handler={Masterpages.MedicationSchedule}>
                        <Route name="medicationScheduleHome" path="/medication/schedules" handler={Pages.MedicationSchedule.Index} />
                        <Route name="medicationScheduleCreateNew" path="/medication/schedules/new" handler={Pages.MedicationSchedule.CreateNew} />
                    </Route>

                    <Route name="patients" path="/patients" handler={Pages.Patient.Index} />
                    <Route name="patientCreate" path="/patients/create" handler={Pages.Patient.Create} />
                    <Route path="/patients/view/:patientId" name="patientLayout" handler={Masterpages.Patient}>
                        <Route name="patient" path="/patients/view/:patientId" handler={Pages.Patient.View.Dashboard} />
                        <Route name="patientAbout" path="/patients/view/:patientId/about" handler={Pages.Patient.View.About} />
                        <Route name="patientAddressMaster" handler={Masterpages.PatientAddress}>
                            <Route name="patientAddress" path="/patients/view/:patientId/about/address" handler={Pages.Patient.View.AddAddress} />
                            <Route name="patientUpdateAddress" path="/patients/view/:patientId/about/address/update/:addressId" handler={Pages.Patient.View.UpdateAddress} />
                        </Route>
                        <Route name="patientContactPersons" path="/patients/view/:patientId/contactpersons" handler={Pages.Patient.View.ContactPersons} />
                        <Route name="patientMedicationMaster" handler={Masterpages.PatientMedication}>
                            <Route name="patientMedication" path="/patients/view/:patientId/medication" handler={Pages.Patient.View.Medication} />
                            <Route name="patientMedicationDetailed" path="/patients/view/:patientId/medication/detailed" handler={Pages.Patient.View.MedicationDetailed} />
                            <Route name="patientMedicationNew" path="/patients/view/:patientId/medication/new" handler={Pages.Patient.View.MedicationNew} />
                            <Route name="patientMedicationHistory" path="/patients/view/:patientId/medication/history" handler={Pages.Patient.View.MedicationLog} />
                        </Route>
                        <Route name="patientContactbookMaster" handler={Masterpages.PatientContactbook}>
                            <Route name="patientContactbookGroups" handler={Masterpages.PatientContactbookGroup}>
                                <Route name="patientContactbook" path="/patients/view/:patientId/contactbook" handler={Pages.Patient.View.Contactbook} />
                                <Route name="patientContactbookGroup" path="/patients/view/:patientId/contactbook/group/:groupId" handler={Pages.Patient.View.ContactbookGroup} />
                            </Route>
                            <Route name="patientContactbookNew" path="/patients/view/:patientId/contactbook/new" handler={Pages.Patient.View.ContactbookNew} />
                        </Route>
                    </Route>
                </Route>
            </Route>
            /*jshint ignore:end */
        )
    });

}());