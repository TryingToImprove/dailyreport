(function () {
    "use strict";
    
    var AppDispatcher = require("../../dispatcher/AppDispatcher"),
        WebClient = require("../../utils/WebClient"),
        
        AuthenticationConstants = require("../../constants/AuthenticationConstants"),
        
        ApplicationConfiguration = require("../../ApplicationConfiguration");
        
    module.exports = {
        authenticateStarted: function () {
            AppDispatcher.handleServerAction({
                type: AuthenticationConstants.AUTHENTICATION_STARTED
            });
        },
        authenticateSuccess: function (response, authenticationOptions) {
            AppDispatcher.handleServerAction({
                type: AuthenticationConstants.AUTHENTICATION_SUCCESS,
                account: response.account,
                organization: response.organization
            });

            // Lets save the token
            localStorage.setItem("auth_token", response.token_type + " " + response.access_token);
            
            var router = require("../../router");
                        
            if (typeof(authenticationOptions.redirectUrl) !== "undefined") {
                // If there were a redirectUrl, then we redirect the user
                router.transitionTo(authenticationOptions.redirectUrl);
            } else {
                // The user did not have a redirectUrl, so we are use the default url
                router.transitionTo(ApplicationConfiguration.defaultAuthenticatedUrl);
            }
        },
        authenticateFailed: function () {
            AppDispatcher.handleServerAction({
                type: AuthenticationConstants.AUTHENTICATION_FAILED
            });
        },
        
        unauthenticate: function() {
            // Lets remove the headers on the WebClient
            WebClient.setHeaders({});
            localStorage.removeItem("auth_token");
        },
        
        reauthenticateSuccess: function (response) {
            AppDispatcher.handleServerAction({
                type: AuthenticationConstants.AUTHENTICATION_SUCCESS,
                account: response.account,
                organization: response.organization
            });
        },
    };
}());