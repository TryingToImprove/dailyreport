import AppDispatcher from "../../dispatcher/AppDispatcher";
import PatientConstants from "../../constants/PatientConstants";

export default {
    findAllStarted: function () {
        AppDispatcher.handleServerAction({
            type: PatientConstants.TIMELINE_FIND_ALL_STARTED
        });
    },
    findAllSuccess: function (patientId, response) {
        AppDispatcher.handleServerAction({
            type: PatientConstants.TIMELINE_FIND_ALL_SUCCESS,
            patientId: patientId,
            entries: response
        });
    },
    findAllFailed: function () {
        AppDispatcher.handleServerAction({
            type: PatientConstants.TIMELINE_FIND_ALL_FAILED
        });
    }
};