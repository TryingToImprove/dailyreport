var AppDispatcher = require("../../dispatcher/AppDispatcher"),

    TimelineActions = require("../client/patientActions/Timeline"),

    PatientConstants = require("../../constants/PatientConstants");

export default {
    findAllStarted: function () {
        AppDispatcher.handleServerAction({
            type: PatientConstants.FIND_ALL_STARTED
        });
    },
    findAllSuccess: function (response) {
        AppDispatcher.handleServerAction({
            type: PatientConstants.FIND_ALL_SUCCESS,
            patients: response
        });
    },
    findAllFailed: function () {
        AppDispatcher.handleServerAction({
            type: PatientConstants.FIND_ALL_FAILED
        });
    },


    getStarted: function (patientId) {
        AppDispatcher.handleServerAction({
            type: PatientConstants.GET_STARTED,
            patientId: patientId
        });
    },
    getSuccess: function (response) {
        AppDispatcher.handleServerAction({
            type: PatientConstants.GET_SUCCESS,
            patient: response
        });
    },
    getFailed: function () {
        AppDispatcher.handleServerAction({
            type: PatientConstants.GET_FAILED
        });
    },


    fetchPostsStarted: function (patientId) {
        AppDispatcher.handleServerAction({
            type: PatientConstants.FETCH_POSTS_STARTED,
            patientId: patientId
        });
    },
    fetchPostsSuccess: function (patientId, response) {
        AppDispatcher.handleServerAction({
            type: PatientConstants.FETCH_POSTS_SUCCESS,
            posts: response,
            patientId: patientId
        });
    },
    fetchPostsFailed: function (patientId) {
        AppDispatcher.handleServerAction({
            type: PatientConstants.FETCH_POSTS_FAILED,
            patientId: patientId
        });
    },


    fetchContactInformationsStarted: function (patientId) {
        AppDispatcher.handleServerAction({
            type: PatientConstants.FETCH_CONTACT_INFORMATIONS_STARTED,
            patientId: patientId
        });
    },
    fetchContactInformationsSuccess: function (patientId, response) {
        AppDispatcher.handleServerAction({
            type: PatientConstants.FETCH_CONTACT_INFORMATIONS_SUCCESS,
            contactInformations: response.entities.contactInformations,
            patientId: patientId
        });
    },
    fetchContactInformationsFailed: function (patientId) {
        AppDispatcher.handleServerAction({
            type: PatientConstants.FETCH_CONTACT_INFORMATIONS_FAILED,
            patientId: patientId
        });
    },


    fetchCurrentContactPersonsStarted: function (patientId) {
        AppDispatcher.handleServerAction({
            patientId: patientId,
            type: PatientConstants.FETCH_CURRENT_CONTACT_PERSONS_STARTED
        });
    },
    fetchCurrentContactPersonsSuccess: function (patientId, response) {
        AppDispatcher.handleServerAction({
            patientId: patientId,
            type: PatientConstants.FETCH_CURRENT_CONTACT_PERSONS_SUCCESS,
            contactPersons: response
        });
    },
    fetchCurrentContactPersonsFailed: function (patientId) {
        AppDispatcher.handleServerAction({
            patientId: patientId,
            type: PatientConstants.FETCH_CURRENT_CONTACT_PERSONS_FAILED
        });
    },


    fetchAllContactPersonsStarted: function (patientId) {
        AppDispatcher.handleServerAction({
            patientId: patientId,
            type: PatientConstants.FETCH_ALL_CONTACT_PERSONS_STARTED
        });
    },
    fetchAllContactPersonsSuccess: function (patientId, response) {
        AppDispatcher.handleServerAction({
            patientId: patientId,
            type: PatientConstants.FETCH_ALL_CONTACT_PERSONS_SUCCESS,
            contactPersons: response
        });
    },
    fetchAllALLContactPersonsFailed: function (patientId) {
        AppDispatcher.handleServerAction({
            patientId: patientId,
            type: PatientConstants.FETCH_ALL_CONTACT_PERSONS_FAILED
        });
    },


    addContactPersonStarted: function (patientId) {
        AppDispatcher.handleServerAction({
            type: PatientConstants.ADD_CONTACT_PERSON_STARTED,
            patientId: patientId
        });
    },
    addContactPersonSuccess: function (patientId, completePrevious, response) {
        AppDispatcher.handleServerAction({
            type: PatientConstants.ADD_CONTACT_PERSON_SUCCESS,
            contactPerson: response,
            completePrevious: completePrevious,
            patientId: patientId
        });
    },
    addContactPersonFailed: function (patientId) {
        AppDispatcher.handleServerAction({
            type: PatientConstants.ADD_CONTACT_PERSON_FAILED,
            patientId: patientId
        });
    },


    completeContactPersonStarted: function (patientId) {
        AppDispatcher.handleServerAction({
            type: PatientConstants.COMPLETE_CONTACT_PERSON_STARTED,
            patientId: patientId
        });
    },
    completeContactPersonSuccess: function (patientId, response) {
        AppDispatcher.handleServerAction({
            type: PatientConstants.COMPLETE_CONTACT_PERSON_SUCCESS,
            contactPerson: response,
            patientId: patientId
        });
    },
    completeContactPersonFailed: function (patientId) {
        AppDispatcher.handleServerAction({
            type: PatientConstants.COMPLETE_CONTACT_PERSON_FAILED,
            patientId: patientId
        });
    },


    updateStarted: function () {
        AppDispatcher.handleServerAction({
            type: PatientConstants.UPDATE_STARTED
        });
    },
    updateSuccess: function (response) {
        AppDispatcher.handleServerAction({
            type: PatientConstants.UPDATE_SUCCESS,
            patient: response
        });
    },
    updateFailed: function () {
        AppDispatcher.handleServerAction({
            type: PatientConstants.UPDATE_FAILED
        });
    },


    createStarted: function () {
        AppDispatcher.handleServerAction({
            type: PatientConstants.CREATE_STARTED
        });
    },
    createSuccess: function (response) {
        AppDispatcher.handleServerAction({
            type: PatientConstants.CREATE_SUCCESS,
            patient: response
        });

        // Redirect to the patient
        var router = require("../../router");
        router.transitionTo("patient", { patientId: response.id });
    },
    createFailed: function () {
        AppDispatcher.handleServerAction({
            type: PatientConstants.CREATE_FAILED
        });
    },

    contactInformation: {
        addStarted: function (patientId) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.CONTACT_INFORMATION_ADD_STARTED,
                patientId: patientId
            });
        },
        addSuccess: function (patientId, response) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.CONTACT_INFORMATION_ADD_SUCCESS,
                contactInformations: response.entities.contactInformations,
                patientId: patientId
            });
        },
        addFailed: function (patientId) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.CONTACT_INFORMATION_ADD_FAILED,
                patientId: patientId
            });
        },

        removeStarted: function (patientId) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.CONTACT_INFORMATION_REMOVE_STARTED,
                patientId: patientId
            });
        },
        removeSuccess: function (patientId, response) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.CONTACT_INFORMATION_REMOVE_SUCCESS,
                contactInformations: response.entities.contactInformations,
                patientId: patientId
            });
        },
        removeFailed: function (patientId) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.CONTACT_INFORMATION_REMOVE_FAILED,
                patientId: patientId
            });
        }
    },

    address: {
        addStarted: function (patientId) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.ADDRESS_ADD_STARTED,
                patientId: patientId
            });
        },
        addSuccess: function (patientId, clearCurrentPrimary, response) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.ADDRESS_ADD_SUCCESS,
                addresses: response.entities.addresses,
                patientId: patientId,
                clearCurrentPrimary: clearCurrentPrimary
            });
        },
        addFailed: function (patientId) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.ADDRESS_ADD_FAILED,
                patientId: patientId
            });
        },

        findAllStarted: function (patientId) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.ADDRESS_FIND_ALL_STARTED,
                patientId: patientId
            });
        },
        findAllSuccess: function (patientId, response) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.ADDRESS_FIND_ALL_SUCCESS,
                addresses: response.entities.addresses,
                patientId: patientId
            });
        },
        findAllFailed: function (patientId) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.ADDRESS_FIND_ALL_FAILED,
                patientId: patientId
            });
        },

        findPrimaryStarted: function (patientId) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.ADDRESS_FIND_PRIMARY_STARTED,
                patientId: patientId
            });
        },
        findPrimarySuccess: function (patientId, response) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.ADDRESS_FIND_PRIMARY_SUCCESS,
                addresses: response.entities.addresses,
                patientId: patientId
            });
        },
        findPrimaryFailed: function (patientId) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.ADDRESS_FIND_PRIMARY_FAILED,
                patientId: patientId
            });
        },

        removeStarted: function (patientId) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.ADDRESS_REMOVE_STARTED,
                patientId: patientId
            });
        },
        removeSuccess: function (patientId, addressId) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.ADDRESS_REMOVE_SUCCESS,
                addressId: addressId,
                patientId: patientId
            });
        },
        removeFailed: function (patientId) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.ADDRESS_REMOVE_FAILED,
                patientId: patientId
            });
        },

        updateStarted: function (patientId) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.ADDRESS_UPDATE_STARTED,
                patientId: patientId
            });
        },
        updateSuccess: function (patientId, clearCurrentPrimary, response) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.ADDRESS_UPDATE_SUCCESS,
                addresses: response.entities.addresses,
                patientId: patientId,
                clearCurrentPrimary: clearCurrentPrimary
            });
        },
        updateFailed: function (patientId) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.ADDRESS_UPDATE_FAILED,
                patientId: patientId
            });
        },

        findByIdStarted: function (patientId) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.ADDRESS_FIND_BY_ID_STARTED,
                patientId: patientId
            });
        },
        findByIdSuccess: function (patientId, response) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.ADDRESS_FIND_BY_ID_SUCCESS,
                addresses: response.entities.addresses,
                patientId: patientId
            });
        },
        findByIdFailed: function (patientId) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.ADDRESS_FIND_BY_ID_FAILED,
                patientId: patientId
            });
        }
    },

    post: {
        createStarted(patientId) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.POST_CREATE_STARTED,
                patientId: patientId
            });
        },
        createSuccess(patientId, response){
            AppDispatcher.handleServerAction({
                type: PatientConstants.POST_CREATE_SUCESS,
                patientId: patientId,
                data: response
            });

            TimelineActions.findAll(patientId);
        },
        createFailed(patientId) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.POST_CREATE_FAILED,
                patientId: patientId
            });
        }
    },

    medication: {
        getSchemaStarted: function (patientId) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.MEDICATION_GET_SCHEMA_STARTED,
                patientId: patientId
            });
        },
        getSchemaSuccess: function (patientId, response) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.MEDICATION_GET_SCHEMA_SUCCESS,
                schema: response,
                patientId: patientId
            });
        },
        getSchemaFailed: function (patientId) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.MEDICATION_GET_SCHEMA_FAILED,
                patientId: patientId
            });
        },


        findAllStarted: function (patientId) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.MEDICATION_FIND_ALL_STARTED,
                patientId: patientId
            });
        },
        findAllSuccess: function (patientId, response) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.MEDICATION_FIND_ALL_SUCCESS,
                schedules: response,
                patientId: patientId
            });
        },
        findAllFailed: function (patientId) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.MEDICATION_FIND_ALL_FAILED,
                patientId: patientId
            });
        },


        findAllTodayStarted: function (patientId) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.MEDICATION_FIND_ALL_TODAY_STARTED,
                patientId: patientId
            });
        },
        findAllTodaySuccess: function (patientId, response) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.MEDICATION_FIND_ALL_TODAY_SUCCESS,
                schedules: response,
                patientId: patientId
            });
        },
        findAllTodayFailed: function (patientId) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.MEDICATION_FIND_ALL_TODAY_FAILED,
                patientId: patientId
            });
        },


        completeStarted: function (patientId) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.MEDICATION_COMPLETE_STARTED,
                patientId: patientId
            });
        },
        completeSuccess: function (patientId, response) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.MEDICATION_COMPLETE_SUCCESS,
                schedule: response,
                patientId: patientId
            });
        },
        completeFailed: function (patientId) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.MEDICATION_COMPLETE_FAILED,
                patientId: patientId
            });
        },

        history: {
            findAllStarted: function (patientId) {
                AppDispatcher.handleServerAction({
                    type: PatientConstants.MEDICATION_HISTORY_FIND_ALL_STARTED,
                    patientId: patientId
                });
            },
            findAllSuccess: function (patientId, response) {
                AppDispatcher.handleServerAction({
                    type: PatientConstants.MEDICATION_HISTORY_FIND_ALL_SUCCESS,
                    histories: response.entities.historyItems,
                    employees: response.entities.employees,
                    medications: response.entities.medications,
                    patientId: patientId
                });
            },
            findAllFailed: function (patientId) {
                AppDispatcher.handleServerAction({
                    type: PatientConstants.MEDICATION_HISTORY_FIND_ALL_FAILED,
                    patientId: patientId
                });
            },


            addStarted: function (patientId) {
                AppDispatcher.handleServerAction({
                    type: PatientConstants.MEDICATION_HISTORY_ADD_STARTED,
                    patientId: patientId
                });
            },
            addSuccess: function (patientId, response) {
                AppDispatcher.handleServerAction({
                    type: PatientConstants.MEDICATION_HISTORY_ADD_SUCCESS,
                    histories: response.entities.historyItems,
                    employees: response.entities.employees,
                    medications: response.entities.medications,
                    patientId: patientId
                });
            },
            addFailed: function (patientId) {
                AppDispatcher.handleServerAction({
                    type: PatientConstants.MEDICATION_HISTORY_ADDL_FAILED,
                    patientId: patientId
                });
            }
        }
    },

    contacts: {
        findAllStarted: function (patientId) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.CONTACTS_FIND_ALL_STARTED,
                patientId: patientId
            });
        },
        findAllSuccess: function (patientId, response) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.CONTACTS_FIND_ALL_SUCCESS,
                groups: response.entities.contactGroups,
                contacts: response.entities.contacts,
                patientId: patientId
            });
        },
        findAllFailed: function (patientId) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.CONTACTS_FIND_ALL_FAILED,
                patientId: patientId
            });
        },

        findByGroupStarted: function (patientId) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.CONTACTS_FIND_BY_GROUP_STARTED,
                patientId: patientId
            });
        },
        findByGroupSuccess: function (patientId, response) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.CONTACTS_FIND_BY_GROUP_SUCCESS,
                groups: response.entities.contactGroups,
                contacts: response.entities.contacts,
                patientId: patientId
            });
        },
        findByGroupFailed: function (patientId) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.CONTACTS_FIND_BY_GROUP_FAILED,
                patientId: patientId
            });
        },


        addStarted: function (patientId) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.CONTACTS_ADD_STARTED,
                patientId: patientId
            });
        },
        addSuccess: function (patientId, response) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.CONTACTS_ADD_SUCCESS,
                contact: response.entities.contact,
                patientId: patientId
            });
        },
        addFailed: function (patientId) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.CONTACTS_ADD_FAILED,
                patientId: patientId
            });
        },


        findFrequentStarted: function (patientId) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.CONTACTS_FIND_FREQUENT_STARTED,
                patientId: patientId
            });
        },
        findFrequentSuccess: function (patientId, response) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.CONTACTS_FIND_FREQUENT_SUCCESS,
                groups: response.entities.contactGroups,
                contacts: response.entities.contacts,
                patientId: patientId
            });
        },
        findFrequentFailed: function (patientId) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.CONTACTS_FIND_FREQUENT_FAILED,
                patientId: patientId
            });
        },


        getGroupsStarted: function (patientId) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.CONTACTS_GET_GROUPS_STARTED,
                patientId: patientId
            });
        },
        getGroupsSuccess: function (patientId, response) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.CONTACTS_GET_GROUPS_SUCCESS,
                groups: response.entities.contactGroups,
                patientId: patientId
            });
        },
        getGroupsFailed: function (patientId) {
            AppDispatcher.handleServerAction({
                type: PatientConstants.CONTACTS_GET_GROUPS_FAILED,
                patientId: patientId
            });
        }
    }
};