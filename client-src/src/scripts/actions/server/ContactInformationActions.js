(function () {
    "use strict";
    
    var AppDispatcher = require("../../dispatcher/AppDispatcher"),
        
        ContactInformationConstants = require("../../constants/ContactInformationConstants");
        
    module.exports = {
        getTypesStarted: function () {
            AppDispatcher.handleServerAction({
                type: ContactInformationConstants.GET_TYPES_STARTED
            });
        },
        getTypesSuccess: function (response) {
            AppDispatcher.handleServerAction({
                type: ContactInformationConstants.GET_TYPES_SUCCESS,
                types: response
            });
        },
        getTypesFailed: function () {
            AppDispatcher.handleServerAction({
                type: ContactInformationConstants.GET_TYPES_FAILED
            });
        }
    };
}());