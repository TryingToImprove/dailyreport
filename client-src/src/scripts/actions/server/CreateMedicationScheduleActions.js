(function () {
    "use strict";
    
    var CreateMedicationConstants = require("../../constants/CreateMedicationConstants"),
        
        AppDispatcher = require("../../dispatcher/AppDispatcher");
        
    module.exports = {        
        saveSuccess: function(patientId, schedule) {            
            var router = require("../../router");

            router.transitionTo("patientMedication", {
                patientId: patientId
            });
            
            AppDispatcher.handleServerAction({
                type: CreateMedicationConstants.SAVE_SUCCESS,
                schedule: schedule,
                patientId: patientId
            });
        },
              
        createSingleSuccess: function(patientId, schedule) {   
            AppDispatcher.handleServerAction({
                type: CreateMedicationConstants.SAVE_SUCCESS,
                schedule: schedule,
                patientId: patientId
            });
        }
    };
}());