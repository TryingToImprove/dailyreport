(function () {
    "use strict";
    
    var AppDispatcher = require("../../dispatcher/AppDispatcher"),
        
        EmployeeConstants = require("../../constants/EmployeeConstants");
        
    module.exports = {
        findAllStarted: function () {
            AppDispatcher.handleServerAction({
                type: EmployeeConstants.FIND_ALL_STARTED
            });
        },
        findAllSuccess: function (response) {
            AppDispatcher.handleServerAction({
                type: EmployeeConstants.FIND_ALL_SUCCESS,
                employees: response
            });
        },
        findAllFailed: function () {
            AppDispatcher.handleServerAction({
                type: EmployeeConstants.FIND_ALL_FAILED
            });
        },
        
        
        getStarted: function () {
            AppDispatcher.handleServerAction({
                type: EmployeeConstants.GET_STARTED
            });
        },
        getSuccess: function (response) {
            AppDispatcher.handleServerAction({
                type: EmployeeConstants.GET_SUCCESS,
                employee: response
            });
        },
        getFailed: function () {
            AppDispatcher.handleServerAction({
                type: EmployeeConstants.GET_FAILED
            });
        }
    };
}());