(function () {
    "use strict";
    
    var AppDispatcher = require("../../dispatcher/AppDispatcher"),
        
        MedicationConstants = require("../../constants/MedicationConstants");
        
    module.exports = {
        searchStarted: function () {
            AppDispatcher.handleServerAction({
                type: MedicationConstants.SEARCH_STARTED
            });
        },
        searchSuccess: function (response) {
            AppDispatcher.handleServerAction({
                type: MedicationConstants.SEARCH_SUCCESS,
                medications: response.entities.medications
            });
        },
        searchFailed: function () {
            AppDispatcher.handleServerAction({
                type: MedicationConstants.SEARCH_FAILED
            });
        }
    };
}());