import AppDispatcher from "../../dispatcher/AppDispatcher";
import GeocodingConstants from "../../constants/GeocodingConstants";

export default {
	autocompleteSuccess(response) {
		AppDispatcher.handleServerAction({
			type: GeocodingConstants.AUTOCOMPLETE_SUCCESS,
			results: response.results,
			query: response.query
		});
	}
}