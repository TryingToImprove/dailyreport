(function () {
    "use strict";
    
    var AppDispatcher = require("../../dispatcher/AppDispatcher"),
        
        AccountConstants = require("../../constants/AccountConstants");
        
    module.exports = {
        registerStarted: function () {
            AppDispatcher.handleServerAction({
                type: AccountConstants.REGISTER_STARTED
            });
        },
        registerSuccess: function (response) {
            AppDispatcher.handleServerAction({
                type: AccountConstants.REGISTER_SUCCESS,
                employee: response
            });
        },
        registerFailed: function () {
            AppDispatcher.handleServerAction({
                type: AccountConstants.REGISTER_FAILED
            });
        }
    };
}());