(function () {
    "use strict";
    
    var EmployeeService = require("../../utils/services/EmployeeService"),
                
        EmployeeActions = require("../../actions/server/EmployeeActions");
        
    module.exports = {
        findAll: function(){        
            // Notify listeners that the authentication is started
            EmployeeActions.findAllStarted();
            
            // Call the API
            EmployeeService.findAll()
                .then(EmployeeActions.findAllSuccess, EmployeeActions.findAllFailed);
        },
        get: function(employeeId){        
            // Notify listeners that the authentication is started
            EmployeeActions.getStarted();
            
            // Call the API
            EmployeeService.get(employeeId)
                .then(EmployeeActions.getSuccess, EmployeeActions.getFailed);
        }
    };
}());