(function () {
    "use strict";
    
    var ContactInformationService = require("../../utils/services/ContactInformationService"),
                
        ContactInformationActions = require("../../actions/server/ContactInformationActions");
        
    module.exports = {
        getTypes: function(){        
            // Notify listeners that the authentication is started
            ContactInformationActions.getTypesStarted();
            
            // Call the API
            ContactInformationService.getTypes()
                .then(ContactInformationActions.getTypesSuccess, ContactInformationActions.getTypesFailed);
        }
    };
}());