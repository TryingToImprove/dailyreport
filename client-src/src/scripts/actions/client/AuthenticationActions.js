(function () {
    "use strict";
    
    var AccountService = require("../../utils/services/AccountService"),
        
        AppDispatcher = require("../../dispatcher/AppDispatcher"),
        
        AuthenticationConstants = require("../../constants/AuthenticationConstants"),
                
        AuthenticationActions = require("../../actions/server/AuthenticationActions");
        
    module.exports = {
        resetAuthenticate: function() {
             AppDispatcher.handleViewAction({
                type: AuthenticationConstants.AUTHENTICATE_RESET
            });                    
        },
        
        tryAuthenticate: function(username, password, authenticationOptions){
            var onSuccess = function(response) {
                    AuthenticationActions.authenticateSuccess(response, authenticationOptions);
                },
                onFailure = AuthenticationActions.authenticateFailed;
            
            // Notify listeners that the authentication is started
            AuthenticationActions.authenticateStarted();
            
            // Call the API
            try {
                AccountService.authenticate(username, password)
                    .then(onSuccess, onFailure);
            }
            catch(error) {
                onFailure();
            }
        },
        
        unauthenticate: function() {
            AppDispatcher.handleViewAction({
                type: AuthenticationConstants.UNAUTHENTICATE
            });
                    
            AuthenticationActions.unauthenticate();
        }
    };
}());