export default {
    PatientActions: require("./patientActions/Patient"),
    PatientContactInformationActions: require("./patientActions/ContactInformation"),
    PatientContactPersonActions: require("./patientActions/ContactPerson"),
    PatientMedicationActions: require("./patientActions/Medication"),
    PatientPostActions: require("./patientActions/Post"),
    PatientTimelineActions: require("./patientActions/Timeline"),
    PatientContactsActions: require("./patientActions/Contacts"),
    PatientAddressActions: require("./patientActions/Address")
};