(function () {
    "use strict";
    
    var MedicationService = require("../../utils/services/MedicationService"),
                
        MedicationActions = require("../../actions/server/MedicationActions");
        
    module.exports = {
        search: function(query) {        
            // Notify listeners that the authentication is started
            MedicationActions.searchStarted();
            
            // Call the API
            MedicationService.search(query)
                .then(MedicationActions.searchSuccess, MedicationActions.searchFailed);
        }
    };
}());