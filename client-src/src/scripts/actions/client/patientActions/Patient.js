var AppDispatcher = require("../../../dispatcher/AppDispatcher");
var PatientActions = require("../../../actions/server/PatientActions");

var PatientConstants = require("../../../constants/PatientConstants");

var PatientService = require("../../../utils/services/PatientService");
var ContactInformationService = require("../../../utils/services/ContactInformationService");
                
var cb = require("../../../utils/cb");

export default {
    findAll(){        
        // Notify listeners that the authentication is started
        PatientActions.findAllStarted();

        // Call the API
        PatientService.findAll()
            .then(PatientActions.findAllSuccess, PatientActions.findAllFailed);
    },
    get(patientId){        
        // Notify listeners that the authentication is started
        PatientActions.getStarted(patientId);

        // Call the API
        PatientService.get(patientId)
            .then(PatientActions.getSuccess, PatientActions.getFailed);
    },
    update(patientId, firstname, lastname, birthDate, securityNumber) {
        // Notify listeners that the authentication is started
        PatientActions.updateStarted();

        // Call the API
        PatientService.update(patientId, firstname, lastname, birthDate, securityNumber)
            .then(PatientActions.updateSuccess, PatientActions.updateFailed);
    },
    
    create(firstname, lastname, contactInformations, contactPersons) {
        // Notify listeners that the authentication is started
        PatientActions.createStarted();

        // Call the API
        PatientService.create(firstname, lastname, contactInformations, contactPersons)
            .then(PatientActions.createSuccess, PatientActions.createFailed);
    },

    // TODO REMOVE THIS SHIT! AND MAKE GOOD CODE INSTEAD
    createWizardPersonal(firstname, lastname) {
        AppDispatcher.handleViewAction({
            type: PatientConstants.CREATE_WIZARD_PERSONAL,
            firstname: firstname,
            lastname: lastname
        });
    },
    createWizardAddContactInformation(dataType, data) {
        AppDispatcher.handleViewAction({
            type: PatientConstants.CREATE_WIZARD_ADD_CONTACT_INFORMATION,
            dataType: dataType,
            data: data
        });
    },
    createWizardContactPerson(employeeId) {
        AppDispatcher.handleViewAction({
            type: PatientConstants.CREATE_WIZARD_TOGGLE_CONTACT_PERSON,
            employeeId: employeeId
        });
    }
}