var AppDispatcher = require("../../../dispatcher/AppDispatcher");
var PatientActions = require("../../../actions/server/PatientActions");

var PatientConstants = require("../../../constants/PatientConstants");

var PatientService = require("../../../utils/services/PatientService");
var ContactInformationService = require("../../../utils/services/ContactInformationService");
                
var cb = require("../../../utils/cb");

export default {
    getSchema(patientId) {
        // Notify listeners that the authentication is started
        PatientActions.medication.getSchemaStarted(patientId);

        // Call the API
        PatientService.medication.getSchema(patientId)
            .then(cb(PatientActions.medication.getSchemaSuccess, patientId), cb(PatientActions.medication.getSchemaFailed, patientId));
    },
    findAll(patientId) {
        PatientActions.medication.findAllStarted(patientId);

        PatientService.medication.findAll(patientId)
            .then(cb(PatientActions.medication.findAllSuccess, patientId),
                  cb(PatientActions.medication.findAllFailed, patientId));
    },
    findAllToday(patientId) {
        PatientActions.medication.findAllTodayStarted(patientId);

        PatientService.medication.findAllToday(patientId)
            .then(cb(PatientActions.medication.findAllTodaySuccess, patientId),
                  cb(PatientActions.medication.findAllTodayFailed, patientId));
    },
    complete(patientId, scheduleId) {
        PatientActions.medication.completeStarted(patientId);

        PatientService.medication.complete(scheduleId)
            .then(cb(PatientActions.medication.completeSuccess, patientId),
                  cb(PatientActions.medication.completeFailed, patientId));
    },
    
    history: {
        findAll(patientId) {
            PatientActions.medication.history.findAllStarted(patientId);

            PatientService.medication.history.findAll(patientId)
                .then(cb(PatientActions.medication.history.findAllSuccess, patientId),
                      cb(PatientActions.medication.history.findAllFailed, patientId));
        },
        
        add(patientId, medicationScheduleItemId) {
            PatientActions.medication.history.addStarted(patientId);
            
            PatientService.medication.history.add(medicationScheduleItemId)
                .then(cb(PatientActions.medication.history.addSuccess, patientId),
                      cb(PatientActions.medication.history.addFailed, patientId));
        }
    }
}