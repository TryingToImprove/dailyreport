var AppDispatcher = require("../../../dispatcher/AppDispatcher");
var PatientActions = require("../../../actions/server/PatientActions");

var PatientConstants = require("../../../constants/PatientConstants");

var PatientService = require("../../../utils/services/PatientService");
var ContactInformationService = require("../../../utils/services/ContactInformationService");
                
var cb = require("../../../utils/cb");

export default {
    findCurrents(patientId) {            
        // Notify listeners that the authentication is started
        PatientActions.fetchCurrentContactPersonsStarted(patientId);

        // Call the API
        PatientService.contactPersons.findCurrents(patientId)
            .then(cb(PatientActions.fetchCurrentContactPersonsSuccess, patientId), 
                  cb(PatientActions.fetchCurrentContactPersonsFailed, patientId));
    },
    findAll(patientId) {            
        // Notify listeners that the authentication is started
        PatientActions.fetchAllContactPersonsStarted(patientId);

        // Call the API
        PatientService.contactPersons.findAll(patientId)
            .then(cb(PatientActions.fetchAllContactPersonsSuccess, patientId), 
                  cb(PatientActions.fetchAllContactPersonsFailed, patientId));
    },
    createContactPersonSelected(employeeId) {
        AppDispatcher.handleViewAction({
            type: PatientConstants.CREATE_CONTACT_PERSONS_SELECTED,
            selectedContactPersonId: employeeId
        });
    },
    add(patientId, employeeId, completePrevious) {            
        // Notify listeners that the authentication is started
        PatientActions.addContactPersonStarted(patientId);

        // Call the API
        PatientService.contactPersons.add(patientId, employeeId, completePrevious)
            .then(cb(PatientActions.addContactPersonSuccess, patientId, completePrevious), 
                  cb(PatientActions.addContactPersonFailed, patientId));
    },
    complete(patientId, relationshipId) {
        // Notify listeners that the authentication is started
        PatientActions.completeContactPersonStarted(patientId);

        // Call the API
        PatientService.contactPersons.complete(relationshipId)
            .then(cb(PatientActions.completeContactPersonSuccess, patientId), cb(PatientActions.completeContactPersonFailed, patientId));
    }
}