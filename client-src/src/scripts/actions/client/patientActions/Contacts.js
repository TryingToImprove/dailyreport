var AppDispatcher = require("../../../dispatcher/AppDispatcher");
var PatientActions = require("../../../actions/server/PatientActions");

var PatientConstants = require("../../../constants/PatientConstants");

var PatientService = require("../../../utils/services/PatientService");
var ContactInformationService = require("../../../utils/services/ContactInformationService");
                
var cb = require("../../../utils/cb");

export default {
    findAll(patientId) {            
        // Notify listeners that the authentication is started
        PatientActions.contacts.findAllStarted(patientId);

        // Call the API
        PatientService.contacts.findAll(patientId)
            .then(cb(PatientActions.contacts.findAllSuccess, patientId), 
                  cb(PatientActions.contacts.findAllFailed, patientId));
    }, 
    findFrequent(patientId) {            
        // Notify listeners that the authentication is started
        PatientActions.contacts.findFrequentStarted(patientId);

        // Call the API
        PatientService.contacts.findFrequent(patientId)
            .then(cb(PatientActions.contacts.findFrequentSuccess, patientId), 
                  cb(PatientActions.contacts.findFrequentFailed, patientId));
    }, 
    findByGroup(patientId, groupId) {            
        // Notify listeners that the authentication is started
        PatientActions.contacts.findByGroupStarted(patientId);

        // Call the API
        PatientService.contacts.findByGroup(patientId, groupId)
            .then(cb(PatientActions.contacts.findByGroupSuccess, patientId), 
                  cb(PatientActions.contacts.findByGroupFailed, patientId));
    },    
    getGroups(patientId) {            
        // Notify listeners that the authentication is started
        PatientActions.contacts.getGroupsStarted(patientId);

        // Call the API
        PatientService.contacts.getGroups(patientId)
            .then(cb(PatientActions.contacts.getGroupsSuccess, patientId), 
                  cb(PatientActions.contacts.getGroupsFailed, patientId));
    },    
    add(patientId, contact) {            
        // Notify listeners that the authentication is started
        PatientActions.contacts.addStarted(patientId);

        // Call the API
        PatientService.contacts.add(patientId, contact)
            .then(cb(PatientActions.contacts.addSuccess, patientId), 
                  cb(PatientActions.contacts.addFailed, patientId));
    }
}