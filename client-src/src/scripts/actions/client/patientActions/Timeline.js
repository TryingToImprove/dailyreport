var TimelineActions = require("../../../actions/server/TimelineActions");

var PatientService = require("../../../utils/services/PatientService");
                
var cb = require("../../../utils/cb");

export default {
    findAll(patientId){        
        // Notify listeners that the authentication is started
        TimelineActions.findAllStarted(patientId);

        // Call the API
        PatientService.timeline.findAll(patientId)
            .then(cb(TimelineActions.findAllSuccess, patientId), 
                  cb(TimelineActions.findAllFailed, patientId));
    } 
};