var AppDispatcher = require("../../../dispatcher/AppDispatcher");
var PatientActions = require("../../../actions/server/PatientActions");

var PatientConstants = require("../../../constants/PatientConstants");

var PatientService = require("../../../utils/services/PatientService");
var ContactInformationService = require("../../../utils/services/ContactInformationService");
                
var cb = require("../../../utils/cb");

export default {
    findAll(patientId) {
        // Notify listeners that the authentication is started
        PatientActions.fetchContactInformationsStarted(patientId);

        // Call the API
        ContactInformationService.patient.findAll(patientId)
            .then(cb(PatientActions.fetchContactInformationsSuccess, patientId), 
                  cb(PatientActions.fetchContactInformationsFailed, patientId));
    },
    add(patientId, label, value){
        PatientActions.contactInformation.addStarted(patientId);

        ContactInformationService.patient.add(patientId, label, value)
            .then(cb(PatientActions.contactInformation.addSuccess, patientId),
                  cb(PatientActions.contactInformation.addFailed, patientId));
    },
    remove(patientId, contactInformationId){
        PatientActions.contactInformation.removeStarted(patientId);

        ContactInformationService.remove(contactInformationId)
            .then(cb(PatientActions.contactInformation.removeSuccess, patientId),
                  cb(PatientActions.contactInformation.removeFailed, patientId));
    }
}