var AppDispatcher = require("../../../dispatcher/AppDispatcher");
var PatientActions = require("../../../actions/server/PatientActions");

var PatientConstants = require("../../../constants/PatientConstants");

var PatientService = require("../../../utils/services/PatientService");
var ContactInformationService = require("../../../utils/services/ContactInformationService");
                
var cb = require("../../../utils/cb");

export default {
    findAll(patientId){        
        // Notify listeners that the authentication is started
        PatientActions.fetchPostsStarted(patientId);

        // Call the API
        PatientService.fetchPosts(patientId)
            .then(cb(PatientActions.fetchPostsSuccess, patientId), 
                  cb(PatientActions.fetchPostsFailed, patientId));
    },
    create(patientId, patients, message) {
    	PatientActions.post.createStarted(patientId);

    	PatientService.post.create(patients, message)
			.then(cb(PatientActions.post.createSuccess, patientId),
				  cb(PatientActions.post.createFailed, patientId));
    } 
};