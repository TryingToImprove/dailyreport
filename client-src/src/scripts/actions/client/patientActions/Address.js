var AppDispatcher = require("../../../dispatcher/AppDispatcher");
var PatientActions = require("../../../actions/server/PatientActions");

var PatientConstants = require("../../../constants/PatientConstants");

var PatientService = require("../../../utils/services/PatientService");

var cb = require("../../../utils/cb");

export default {
    findAll(patientId) {
        // Notify listeners that the authentication is started
        PatientActions.address.findAllStarted(patientId);

        // Call the API
        PatientService.address.findAll(patientId)
            .then(cb(PatientActions.address.findAllSuccess, patientId),
                  cb(PatientActions.address.findAllFailed, patientId));
    },
    findPrimary(patientId) {
        // Notify listeners that the authentication is started
        PatientActions.address.findPrimaryStarted(patientId);

        // Call the API
        PatientService.address.findPrimary(patientId)
            .then(cb(PatientActions.address.findPrimarySuccess, patientId),
                  cb(PatientActions.address.findPrimaryFailed, patientId));
    },
    findById(patientId, addressId) {
        // Notify listeners that the authentication is started
        PatientActions.address.findByIdStarted(patientId);

        // Call the API
        PatientService.address.findById(patientId, addressId)
            .then(cb(PatientActions.address.findByIdSuccess, patientId),
                  cb(PatientActions.address.findByIdFailed, patientId));
    },
    add(patientId, addressRequest) {
        PatientActions.address.addStarted(patientId);

        PatientService.address.add(patientId, addressRequest)
            .then(cb(PatientActions.address.addSuccess, patientId, addressRequest.clearCurrentPrimary),
                  cb(PatientActions.address.addFailed, patientId));
    },
    remove(patientId, addressId) {
        PatientActions.address.removeStarted(patientId);

        PatientService.address.remove(patientId, addressId)
            .then(cb(PatientActions.address.removeSuccess, patientId, addressId),
                  cb(PatientActions.address.removeFailed, patientId));
    },
    update(patientId, addressId, addressRequest) {
        PatientActions.address.updateStarted(patientId);

        PatientService.address.update(patientId, addressId, addressRequest)
            .then(cb(PatientActions.address.updateSuccess, patientId, addressRequest.clearCurrentPrimary),
                  cb(PatientActions.address.updateFailed));
    }
}