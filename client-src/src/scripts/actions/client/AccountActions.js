(function () {
    "use strict";
            
    var AccountService = require("../../utils/services/AccountService"),
                
        AccountActions = require("../../actions/server/AccountActions"),
        
        AccountConstants = require("../../constants/AccountConstants"),
        
        AppDispatcher = require("../../dispatcher/AppDispatcher");
    
    
    module.exports = {
        resetRegistration: function() {
            AppDispatcher.handleViewAction({
                type: AccountConstants.REGISTER_RESET
            });
        },
        
        register: function(firstname, lastname, organizationName, email) {
            var onFailure = AccountActions.registerFailed;
            
            // Notify listeners that the authentication is started
            AccountActions.registerStarted();
            
            // Call the API
            try {
                AccountService.register(firstname, lastname, organizationName, email)
                    .then(AccountActions.registerSuccess, onFailure);
            }
            catch(error) {
                onFailure();
            }
        }
    };
}());