import GeocodingService from "../../utils/services/GeocodingService";
import GeocodingActions from "../server/GeocodingActions";

export default {
	autocomplete(query) {
		console.log("autocomplete;", query);
		GeocodingService.autocomplete(query)
			.then((response) => { GeocodingActions.autocompleteSuccess(response); });
	}
}