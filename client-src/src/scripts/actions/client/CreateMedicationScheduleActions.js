(function () {
    "use strict";
    
    var CreateMedicationConstants = require("../../constants/CreateMedicationConstants"),
        
        CreateMedicationScheduleActions = require("../server/CreateMedicationScheduleActions"),
        
        PatientService = require("../../utils/services/PatientService"),
        
        AppDispatcher = require("../../dispatcher/AppDispatcher"),
        
        cb = require("../../utils/cb");
        
    module.exports = {        
        medicationSelected: function(medication) {
            AppDispatcher.handleViewAction({
                type: CreateMedicationConstants.MEDICATION_SELECTED,
                medication: medication
            });
        },
               
        save: function(patientId, schedule) {
            PatientService.medication.create(patientId, schedule)
                .then(cb(CreateMedicationScheduleActions.saveSuccess, patientId));
        },
               
        createSingle: function(patientId, schedule) {
            PatientService.medication.create(patientId, schedule)
                .then(cb(CreateMedicationScheduleActions.createSingleSuccess, patientId));
        },
               
        reset: function() {
            AppDispatcher.handleViewAction({
                type: CreateMedicationConstants.RESET
            });
        }
    };
}());