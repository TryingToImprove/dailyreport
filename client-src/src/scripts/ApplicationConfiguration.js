(function(){
    "use strict";
    
    module.exports = {
        serverUrl: "http://localhost:7989/",
        
        defaultAuthenticatedUrl: "/",
        authenticationUrl: "authentication"
    };
}());
    