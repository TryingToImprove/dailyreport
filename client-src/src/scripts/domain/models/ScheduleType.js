/**
 *
 * @jsx React.DOM
 */

(function () {
    "use strict";
    
    module.exports = {
        Single: 0,
        Multiple: 1
    };

}());