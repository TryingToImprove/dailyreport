(function () {
    "use strict";
    
    var constance = require("constance");
        
    module.exports = constance("ACCOUNT_", {
        REGISTER_STARTED: null,
        REGISTER_SUCCESS: null,
        REGISTER_FAILED: null,
        REGISTER_RESET: null
    });
}());