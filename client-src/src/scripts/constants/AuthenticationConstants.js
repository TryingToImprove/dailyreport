(function () {
    "use strict";
    
     var constance = require("constance");
        
    module.exports = constance("AUTHENTICATION_",{
        AUTHENTICATION_STARTED: null,
        AUTHENTICATION_SUCCESS: null,
        AUTHENTICATION_FAILED: null,
        AUTHENTICATE_RESET: null,
        
        UNAUTHENTICATE: null,
        
        REAUTHENTICATION_SUCCESS: null
    });
}());