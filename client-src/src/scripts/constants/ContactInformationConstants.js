(function () {
    "use strict";
    
     var constance = require("constance");
        
    module.exports = constance("CONTACT_INFORMATION_",{
        GET_TYPES_STARTED: null,
        GET_TYPES_SUCCESS: null,
        GET_TYPES_FAILED: null
    });
}());