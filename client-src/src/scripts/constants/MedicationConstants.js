(function () {
    "use strict";
    
     var constance = require("constance");
        
    module.exports = constance("MEDICATION_",{
        SEARCH_STARTED: null,
        SEARCH_SUCCESS: null,
        SEARCH_FAILED: null
    });
}());