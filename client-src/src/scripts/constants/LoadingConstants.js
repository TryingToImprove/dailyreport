(function () {
    "use strict";
    
     var constance = require("constance");
        
    module.exports = constance("LOADING_",{
        LOADING_PASSIVE: null,
        LOADING_COMPLETED: null,
        LOADING_WAITING: null,        
        LOADING_FAILED: null
    });
}());