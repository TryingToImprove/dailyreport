(function () {
    "use strict";
    
    var constance = require("constance");
        
    module.exports = constance("CREATE_MEDICATION_", {
        MEDICATION_SELECTED: null,
        SAVE: null,
        RESET: null,
        SAVE_SUCCESS: null
    });
    
}());