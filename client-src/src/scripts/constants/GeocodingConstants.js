(function () {
    "use strict";
    
     var constance = require("constance");
        
    module.exports = constance("GEOCODING_", {
        AUTOCOMPLETE_SUCCESS: null
    });
}());