(function () {
    "use strict";
    
     var constance = require("constance");
        
    module.exports = constance("EMPLOYEE_", {
        FIND_ALL_STARTED: null,
        FIND_ALL_SUCCESS: null,
        FIND_ALL_FAILED: null,
        
        
        GET_STARTED: null,
        GET_SUCCESS: null,
        GET_FAILED: null
    });
}());