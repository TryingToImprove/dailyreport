var Dispatcher = require('./Dispatcher'),
    copyProperties = require('react/lib/copyProperties');

function createPayload(source, action) {
    return {
      source: source,
      action: action
    };
}

module.exports = copyProperties(new Dispatcher(), { 
    /**
    * @param {object} action The details of the action, including the action's
    * type and additional data coming from the server.
    */
    handleServerAction: function(action) {
        this.dispatch(createPayload("SERVER_ACTION", action));
    },

    /**
    * @param {object} action The details of the action, including the action's
    * type and additional data coming from the view.
    */
    handleViewAction: function(action) {
        this.dispatch(createPayload("VIEW_ACTION", action));
    }
});