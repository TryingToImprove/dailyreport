/**
 *
 * @jsx React.DOM
 */

(function () {
    "use strict";

    var React = require("react");
       
    var ProgressComponent = React.createClass({
        render: function() {
            var text = this.props.text || "Indlæser",
                progressStyle = {
                    width: "100%"
                };
            
            return (
                /*jshint ignore:start */
                <div className="progress">
                    <div className="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style={progressStyle}>
                        <span className="sr-only">{text}</span>
                    </div>
                </div>
                /*jshint ignore:end */
            );
        }
    });
    
   var TextComponent = React.createClass({
        render: function() {
            var text = this.props.text || "";
            
            return (
                /*jshint ignore:start */
                <p className="text-center text-muted">
                    <b>Vent venligst..</b><br /> {text}
                </p>          
                /*jshint ignore:end */
            );
        }
    });
    
    module.exports = {
        Progress: ProgressComponent,
        Text: TextComponent
    }
}());