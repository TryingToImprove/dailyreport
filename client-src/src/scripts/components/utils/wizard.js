/**
 *
 * @jsx React.DOM
 */

(function () {
    "use strict";

    var React = require("react/addons");
       
    var Step = React.createClass({
        onSubmit: function(e) {
            e.preventDefault();
            
            // Trigger the step
            if (typeof(this.props.onSubmit) !== "undefined") {
                this.props.onSubmit(this.refs.step);
            }
            
            // Go to next step
            this.props.nextStep();
        },
        previousStep: function(e){
            e.preventDefault();
            
            this.props.previousStep();
        },
        render: function() {
            var ComponentClass = this.props.component,
                content = (       
                    /*jshint ignore:start */     
                    <ComponentClass ref="step" />
                    /*jshint ignore:end */
                ),
                previousButton = this.props.index > 0
                    ? (<li className="previous"><a href="#" onClick={this.previousStep}><span aria-hidden="true">&larr;</span> Tilbage</a></li>)
                    : [];
                        
            if (this.props.isForm) {
                return (       
                    /*jshint ignore:start */     
                    <form onSubmit={this.onSubmit}>
                        {content}

                        <nav>
                            <ul className="pager">
                                {previousButton}
                                <li className="next">
                                    <a href="#" onClick={this.onSubmit}>
                                        <span>{this.props.nextStepText}</span> <span aria-hidden="true">&rarr;</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </form>
                    /*jshint ignore:end */
                );
            }
            
            return (       
                /*jshint ignore:start */     
                <div>
                    {content}

                    <nav>
                        <ul className="pager">
                            {previousButton}
                            <li className="next">
                                <a href="#" onClick={this.onSubmit}>
                                    <span>{this.props.nextStepText}</span> <span aria-hidden="true">&rarr;</span>
                                </a>
                            </li>   
                        </ul>
                    </nav>
                </div>
                /*jshint ignore:end */
            );
        }
    });
    
    var WizardComponent = React.createClass({
        getState: function(){
            return {
                currentStepIndex: 0
            };
        },
        
        getInitialState: function(){
            return this.getState();
        },
        
        getTabs: function(currentIndex, steps) {
            var goToStepFunc = this.goToStep;
            
            return steps.map(function(step, index) {
                var classes = React.addons.classSet({
                        "list-group-item": true,
                        "active": (index === currentIndex),
                        "disabled": (currentIndex < index)
                    }),
                    action = function(e) {
                        e.preventDefault();
                        
                        if (currentIndex < index) {
                            return;
                        }
                        
                        goToStepFunc(index);
                    };

                return (
                    /*jshint ignore:start */
                    <a href="#" onClick={action} className={classes}>{step.name}</a>
                    /*jshint ignore:end */
                );
            });
        },
        
        render: function() {      
            if (this.state.currentStepIndex >= this.props.steps.length) {
                return [];
            }
            
            var currentStepIndex = this.state.currentStepIndex,
                steps = this.props.steps,
                tabs = this.getTabs(currentStepIndex, steps),
                currentStep = this.props.steps[currentStepIndex],
                component = React.createElement(Step, {
                    key: "step-" + currentStepIndex,
                    isForm: (typeof(currentStep.isForm) === "undefined" || currentStep.isForm),
                    onSubmit: currentStep.onSubmit,
                    component: currentStep.component,
                    previousStep: this.previousStep,
                    nextStep: this.nextStep,
                    nextStepText: currentStep.nextStepText || "Videre",
                    index: currentStepIndex
                });
        
            if (typeof(currentStep.onShown) !== "undefined") {
                currentStep.onShown();
            }
            
            return (
                /*jshint ignore:start */
                <div className="row">
                    <div className="col-md-2">
                        <div className="list-group">
                            {tabs}
                        </div>
                    </div>
                    <div className="col-md-10">
                        {component}
                    </div>
                </div>
                /*jshint ignore:end */
            );
        },
        nextStep: function() {
            var state = this.state;
            
            this.changeStep(state.currentStepIndex + 1);
        },
        goToStep: function(stepIndex) {            
            this.changeStep(stepIndex);
        },
        previousStep: function() {
            var state = this.state;
            
            this.changeStep(state.currentStepIndex - 1);;
        },
        changeStep: function(newStepIndex) {            
            if (newStepIndex < 0) {
                return;
            }
            
            if (newStepIndex >= this.props.steps.lenth) {
                return;
            }
            
            var state = this.state;
            state.currentStepIndex = newStepIndex;
                        
            this.setState(state);
        }
    });
    
    module.exports = {
        Wizard: WizardComponent
    };
    
}());