/**
 *
 * @jsx React.DOM
 */

(function () {
    "use strict";

    var React = require("react"),
        TitleComponent = React.createClass({
            render: function() {
                var title = this.props.title;
                
                if (typeof(title) === "undefined" || title.length === 0) {
                    return [];
                }
                    
                return (
                    /*jshint ignore:start */
                    <div className="panel-heading">
                        <h3 className="panel-title">{this.props.title}</h3>
                    </div>
                    /*jshint ignore:end */
                );
            
            }
        });
    
    
    
    module.exports = React.createClass({
        render: function() {
            return (
                /*jshint ignore:start */
                <div className="panel panel-primary">
                    <TitleComponent title={this.props.title} />
                
                    <div className="panel-body">
                        {this.props.children}
                    </div>
                </div>
                /*jshint ignore:end */
            );
        }
    });
}());