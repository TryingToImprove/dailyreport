/**
 *
 * @jsx React.DOM
 */

(function () {
    "use strict";

    var React = require("react"),
        TitleComponent = React.createClass({
            render: function() {
                var title = this.props.title;
                
                if (typeof(title) === "undefined" || title.length === 0) {
                    return [];
                }
                    
                return (
                    /*jshint ignore:start */
                    <div className="panel-heading">
                        <h3 className="panel-title">{this.props.title}</h3>
                    </div>
                    /*jshint ignore:end */
                );
            
            }
        });
    
    var CommentComponent = React.createClass({
        render: function() {
            var author = this.props.author,
                publishDate = this.props.publishDate,
                description = this.props.children;
            
            
            return (
                /*jshint ignore:start*/                
                <article className="comment">
                    <a className="comment-img" href="#non">
                        <img src="http://lorempixum.com/50/50/people/1" alt="" width="50" height="50" />
                    </a>
                    <div className="comment-body">
                        <div className="text">
                            <p>{description}</p>
                        </div>
                        <p className="attribution">by <a href="#non">{author}</a> at {publishDate}</p>
                    </div>
                </article> 
                /*jshint ignore:end*/
            );
        }
    });
    
    var CommentListComponent = React.createClass({
        render: function() {
            var items = this.props.children;
            
            return (
                /*jshint ignore:start */
                <section className="comments">
                    {items}
                </section>
                /*jshint ignore:end */
            );
        }
    });
    
    module.exports = {
        List: CommentListComponent,
        ListItem: CommentComponent
    };
}());