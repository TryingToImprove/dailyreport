/**
 *
 * @jsx React.DOM
 */

(function () {
    "use strict";

    var React = require("react"),
        
        Router = require("react-router"),
        Link = Router.Link,
        
        classnames = require("classnames");
    
    module.exports = React.createClass({
        mixins: [Router.State],

        render: function(){
            var to = this.props.to,
                params = this.props.params,
                classes = classnames("list-group-item", {
                    "active": this.isActive(this.props.routeName || to, params, null)
                });
            
            return (
                /*jshint ignore:start*/
                <li className={classes}>
                    <Link to={to} params={params}>{this.props.children}</Link>
                </li>
                /*jshint ignore:end*/
            );
        }                                 
    });
}());