/**
 *
 * @jsx React.DOM
 */

// Dependencies
var React = require("react");
var { Link } = require("react-router");

// Stores       

// Actions

// Constants         

// Components        

export default React.createClass({

    render() {
        var contactType = this.props.contactType ? this.props.contactType.name : null;
        var contactGroups = this.props.groups.map(y => {
            return (
                /*jshint ignore:start*/
                <span key={y.id}>{y.name}</span>
                /*jshint ignore:end*/
            );
        });
        var contactInformations = this.props.contactInformations.map(y => {
            return (
                /*jshint ignore:start*/
                <span key={y.id}>{y.type} = {y.data}</span>
                /*jshint ignore:end*/
            );
        });

        return (
            /*jshint ignore:start*/
            <div className="media" key={this.props.id}>
                <div className="media-body">
                    <h4 className="media-heading">{this.props.firstname + " " + this.props.lastname}</h4>
                    <p>{contactType} - {contactGroups}</p>
                    <p>{contactInformations}</p>
                </div>
            </div>
            /*jshint ignore:end*/
        );
    }
});