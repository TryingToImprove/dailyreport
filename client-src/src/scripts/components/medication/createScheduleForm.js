/**
 *
 * @jsx React.DOM
 */

(function () {
    "use strict";

    var React = require("react");
    
    // Components
    var MedicationTypeaheadComponent = require("../../components/medication/medicationTypeahead");
    var RepeatIntervalComponent = require("../../components/medication/repeatInterval");
   
    // React Component
    module.exports = React.createClass({        
        propTypes: {
            onSave: React.PropTypes.func
        },
        
        getInitialState: function() {
            return {
                selectedMedication: null
            };
        },
                
        handleSubmit: function (e){            
            var medication = this.state.selectedMedication,
                amount = this.refs.amount.getDOMNode().value,
                executeTime = this.refs.executeTime.getDOMNode().value,
                repeatInterval = this.refs.repeatInterval.getDOMNode().value;
                        
            var schedule ={
                startDate: new Date(),
                items: [{
                    medicationId: medication.id,
                    amouth: amount,
                    executeTime: executeTime,
                    repeatInterval: repeatInterval
                }]
            };
        
            // Pass the onSave up
            this.props.onSave(schedule);
            
            e.preventDefault();
        },
        
        handleMedicationSelected: function(medication) {
            this.setState({ selectedMedication: medication });
        },
        
        render: function() {
            return (
                /*jshint ignore:start */
                <form className="form-horizontal" onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <label className="control-label col-md-4">Medicin:</label>
                        <div className="col-md-8">                                        
                            <MedicationTypeaheadComponent onOptionSelected={this.handleMedicationSelected} />
                        </div>
                    </div>

                    <div className="form-group">
                        <label className="control-label col-md-4">Mængde:</label>
                        <div className="col-md-8">
                            <input type="number" className="form-control" min="1" defaultValue="1" required ref="amount" />
                        </div>
                    </div>

                    <div className="form-group">
                        <label className="control-label col-md-4">Tidpunkt på dagen:</label>
                        <div className="col-md-8">
                            <input type="time" className="form-control" required ref="executeTime" />
                        </div>
                    </div>

                    <div className="form-group">
                        <label className="control-label col-md-4">Gentag efter:</label>
                        <div className="col-md-8">
                            <RepeatIntervalComponent ref="repeatInterval" />
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="col-md-offset-4 col-sm-8">
                            <button type="submit" className="btn btn-primary">Gem</button>
                        </div>
                    </div>
                </form>
                /*jshint ignore:end */
            );
        }
    });
}());