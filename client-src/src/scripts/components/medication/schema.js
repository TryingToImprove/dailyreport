/**
 *
 * @jsx React.DOM
 */

(function () {
    "use strict";

    var React = require("react"),
        
        TextLoadingComponent = require("../utils/loading").Text;
    
    var SchemaTableRow = require("./schemaTableRow");
        
    
    module.exports = React.createClass({
        propTypes: {
            schema: React.PropTypes.array,
            patientId: React.PropTypes.number
        },
        
        render: function() {
            var schema = this.props.schema,
                patientId = this.props.patientId;
            
            if (typeof(schema) === "undefined" || schema === null) {
                return  (                    
                    /*jshint ignore:start */
                    <TextLoadingComponent text="Henter medicinskema" />
                    /*jshint ignore:end */
                );
            }
            
            if (schema.length === 0){  
                return  (                    
                    /*jshint ignore:start */
                    <p className="text-muted">Patienten modtager ikke medicin</p>
                    /*jshint ignore:end */
                );
            }
            
            var schemaTableRows = schema.map(function(schemaItem) {
                return (                    
                    /*jshint ignore:start */
                    <SchemaTableRow key={"schemaItem-" + schemaItem.id} patientId={patientId} schedule={schemaItem} />
                    /*jshint ignore:end */
                );
            });
            
            return (
                /*jshint ignore:start */
                <table className="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Medicin</th>
                            <th>Dosis</th>
                            <th>Kommentar</th>
                            <th>Næste gang</th>
                        </tr>
                    </thead>
                    <tbody>
                        {schemaTableRows}
                    </tbody>
                </table>
                /*jshint ignore:end */
            );
        }
    });
}());