/**
 *
 * @jsx React.DOM
 */

(function () {
    "use strict";

    var React = require("react");
    
    function createOption(value, text) {
        return {
            value: value,
            text: text
        };
    }
    
    module.exports = React.createClass({
        getInitialState: function() {
            var options = [
                createOption(0, "Skal ikke gentages"),
                createOption(1, "Hver dag"),
                createOption(2, "Hver anden dag"),
                createOption(7, "Hver uge")
            ];
            
            for(var i = 3; i < 32; i++){
                if ([0, 1, 2, 7].indexOf(i) === -1) {
                    options.push(createOption(i, "Hver " + i + ". dag"));
                }
            }
            
            return { 
                options: options
            };
        },
                                 
        render: function() {
            var options = this.state.options.map(function(option) {
                return (
                    /*jshint ignore:start */
                    <option value={option.value} key={option.value}>{option.text}</option>
                    /*jshint ignore:end */
                );
            });
            
            return (
                /*jshint ignore:start */
                <select>
                    {options}
                </select>
                /*jshint ignore:end */
            );
        }
    });
}());