/**
 *
 * @jsx React.DOM
 */

(function () {
    "use strict";

    var React = require("react");
    
    // Components
    var MedicationTypeaheadComponent = require("../../components/medication/medicationTypeahead");
    var RepeatIntervalComponent = require("../../components/medication/repeatInterval");
   
    // React Component
    module.exports = React.createClass({        
        propTypes: {
            onSave: React.PropTypes.func
        },
        
        getInitialState: function() {
            return {
                selectedMedication: null
            };
        },
                
        handleSubmit: function (e){            
            var currentDate = new Date(),
                medication = this.state.selectedMedication,
                amount = this.refs.amount.getDOMNode().value,
                executeTime = currentDate.getHours() + ":" + currentDate.getMinutes();
                        
            var schedule ={
                startDate: new Date(),
                items: [{
                    medicationId: medication.id,
                    amouth: amount,
                    executeTime: executeTime,
                    repeatInterval: 0
                }]
            };
        
            // Pass the onSave up
            this.props.onSave(schedule);
            
            e.preventDefault();
        },
        
        handleMedicationSelected: function(medication) {
            this.setState({ selectedMedication: medication });
        },
        
        render: function() {
            
            let weightAndMesaure = (this.state.selectedMedication !== null)
                ? this.state.selectedMedication.weight + this.state.selectedMedication.measure
                : ""; 
            
            return (
                /*jshint ignore:start */
                <form className="form-horizontal" onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <label className="control-label col-md-4">Medicin:</label>
                        <div className="col-md-8">                                
                            <MedicationTypeaheadComponent onOptionSelected={this.handleMedicationSelected} />
                        </div>
                    </div>

                    <div className="form-group">
                        <label className="control-label col-md-4">Mængde:</label>
                        <div className="col-md-8">
                            <div className="input-group">
                                <input type="number" ref="amount" min="1" defaultValue="1" required className="form-control" />                
                                <div className="input-group-addon">
                                    {weightAndMesaure}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="form-group">
                        <label className="control-label col-md-4">Begrundelse:</label>
                        <div className="col-md-8">
                            <textarea className="form-control" rows="3"></textarea>
                        </div>
                    </div>
                    
                    <div className="text-right">
                        <button type="submit" className="btn btn-default">Register udlevering</button>
                    </div>
                </form>
                /*jshint ignore:end */
            );
        }
    });
}());