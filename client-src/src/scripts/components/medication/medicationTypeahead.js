/**
 *
 * @jsx React.DOM
 */

(function () {
    "use strict";

    var React = require("react"),
        
        MedicationStore = require("../../stores/MedicationStore"),
        
        Typeahead = require('react-typeahead').Typeahead,
        
        MedicationActions = require("../../actions/client/MedicationActions");


    var typaheadClassSet = {
        input: "form-control",
        results: "list-group",
        listItem: "list-group-item",
        hover: "active"
    };
    
    function getState() {
         return {
            medications: MedicationStore.getMedications()
         };
    }
    
    function formatter(medication){
        return medication.name;
    }
    
    module.exports = React.createClass({
        getInitialState: function(){
            return getState();
        },

        componentDidMount: function() {
            MedicationStore.addChangeListener(this.onChange);
        },

        componentWillUnmount: function() {
            MedicationStore.removeChangeListener(this.onChange);
        },

        medicationSearch: function(value) {
            MedicationActions.search(value);
        },

        render: function() {
            return (
                /*jshint ignore:start */
                <Typeahead ref="search" 
                           options={this.state.medications} 
                           className={this.props.className} 
                           onChange={this.medicationSearch} 
                           formatter={formatter} 
                           onOptionSelected={this.props.onOptionSelected} 
                           customClasses={typaheadClassSet} />            
                /*jshint ignore:end */
            );
        },

        onChange: function() {
            this.setState(getState());
        }
    });
}());