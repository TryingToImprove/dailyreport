/**
 *
 * @jsx React.DOM
 */

var React = require("react");

var { PatientMedicationActions } = require("../../actions/client/PatientActions");

export default React.createClass({
    propTypes: {
        patientId: React.PropTypes.number,
        schedule: React.PropTypes.object
    },
    
    handleClick(e) {
        var scheduleItem = this.props.schedule,
            patientId = this.props.patientId;
                
        // Send event
        PatientMedicationActions.history.add(patientId, scheduleItem.id);
        
        e.preventDefault();
    },
    
    render() {   
        var item = this.props.schedule,
            medication = item.medication;

        return (
            /*jshint ignore:start */
            <tr className={this.props.className}>
                <td>{medication.name}</td>
                <td>{item.amouth + "x" + medication.weight + medication.measure}</td>
                <td>Skal tages foran den ansatte</td>
                <td>KL. {item.executeTime} - <a href="#" onClick={this.handleClick}>Marker som givet</a></td>
            </tr>
            /*jshint ignore:end */
        );
    }
});