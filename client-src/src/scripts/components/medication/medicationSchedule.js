/**
 *
 * @jsx React.DOM
 */

// Dependecies
var React = require("react");
var { State, Link } = require("react-router");

// Models
var ScheduleType = require("../../domain/models/ScheduleType");

// Components
var SchemaComponent = require("./schema");
var SchemaTableRowComponent = require("./schemaTableRow");

// Actions
var { PatientMedicationActions } = require("../../actions/client/PatientActions");

// Helpers    
var moment = require("../../utils/moment");

// React Component
var GroupComponent = React.createClass({
    handleCompleteClick() {
        PatientMedicationActions.complete(this.props.patientId, this.props.schedule.id);
    },
    
    renderSchedules() {
        var patientId = this.props.patientId;
        
        return this.props.schedule.schedules.map((schedule) => {
            return (
                <SchemaTableRowComponent schedule={schedule} patientId={patientId} className="active" />
            );
        });
    },

    render() {
        var schedule = this.props.schedule,
            date = moment(schedule.startDate),
            nextTime = (schedule.scheduleType === ScheduleType.Multiple) ? moment(schedule.nextTime).format("DD MMMM") : 'Enkel';
        
        return (
            /*jshint ignore:start */
            <div className="panel panel-default">
                <div className="panel-body">
                    <div className="pull-right">
                     <button className="btn btn-danger btn-xs" onClick={this.handleCompleteClick}>Afslut medicinering</button>
                    </div>
                    Påbegyndt d. {date.format("DD MMMM YYYY")} næste gang: {nextTime}
                </div>
                <table className="table table-striped">   
                    <tbody>
                         {this.renderSchedules()}
                    </tbody>
                </table>
            </div>
            /*jshint ignore:end */
        );
    }
});

export default React.createClass({

    renderGroups() {
        var patientId = this.props.patientId;
        
        return this.props.schedules.map((schedule) => {
            return (
                /*jshint ignore:start */
                <GroupComponent key={'medicationScheduleGroup-' + schedule.id} patientId={patientId} schedule={schedule} />
                /*jshint ignore:end */
            )
        });
    },
    
    render() {
        var scheduleGroups = this.props.scjedules;
        return (
            /*jshint ignore:start */
            <div>{this.renderGroups()}</div>
            /*jshint ignore:end */
        );
    }});