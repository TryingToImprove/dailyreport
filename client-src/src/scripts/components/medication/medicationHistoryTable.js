/**
 *
 * @jsx React.DOM
 */

// Dependecies
var React = require("react");
var { Link } = require("react-router");

// Utils
var moment = require("../../utils/moment");


export default React.createClass({
    render() {
        var historyRows = this.props.histories.map(history => {
            var issuedDate = new Date(history.timeCreated);

            return (
                /*jshint ignore:start*/
                <tr>
                    <td>{moment(issuedDate).format("DD/MM-YY[ kl. ]HH:mm")}</td>
                    <td><Link to="employee" params={{ employeeId: history.employee.id }}>{history.employee.firstname + ' ' + history.employee.lastname}</Link></td>
                    <td>{history.medication.name}</td>
                </tr>
                /*jshint ignore:end*/
            );
        });
        
        return (
            /*jshint ignore:start*/
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Udleveret</th>
                        <th>Ansat</th>
                        <th>Medicin</th>
                    </tr>
                </thead>
                <tbody>
                    {historyRows}
                </tbody>
            </table>
            /*jshint ignore:end*/
        );
    }
});