/**
 *
 * @jsx React.DOM
 */

// Dependencies
var React = require("react");
import { Link } from "react-router";

// Stores

// Models
const { AddressRequest } = require("../models");

// Actions
var { PatientAddressActions } = require("../actions/client/PatientActions");

// Constants

// Components
export default React.createClass({
    propTypes: {
        street: React.PropTypes.string.isRequired,
        houseNumber: React.PropTypes.string.isRequired,
        postalCode: React.PropTypes.string.isRequired,
        city: React.PropTypes.string.isRequired
    },

    handleClickRemove(e) {
        const { patientId, id } = this.props;

        PatientAddressActions.remove(patientId, id);

        e.preventDefault();
    },

    handleClickPrimary(e) {
        const { patientId, id } = this.props;

        PatientAddressActions.update(patientId, id, new AddressRequest({
            isPrimary: true,
            clearCurrentPrimary: true
        }));

        e.preventDefault();
    },

    render() {
        const { patientId, id, street, isPrimary, houseNumber, postalCode, city } = this.props;

        const address = `${street} ${houseNumber}`;
        const town = `${postalCode} ${city}`;
        const primary = isPrimary ? "Nuværende adresse" : undefined;

        return (
            /*jshint ignore:start*/
            <div>
                <b>{address} {primary}</b><br />
                {town} - <button onClick={this.handleClickRemove}>Fjern</button> - <button onClick={this.handleClickPrimary}>Sæt som nuværende</button>
                <div>
                    <Link to="patientUpdateAddress" params={{ patientId: patientId, addressId: id }}>Update</Link>
                </div>
            </div>
            /*jshint ignore:end*/
        );
    }
});