/**
 *
 * @jsx React.DOM
 */

// Dependencies
var React = require("react");

// Stores
var GeocodingStore = require("../stores/GeocodingStore");

// Actions
var GeocodingActions = require("../actions/client/GeocodingActions");
var debounce = require("../utils/debounce");

// Constants

// Components

var AddressComponent = React.createClass({
    propTypes: {
        query: React.PropTypes.string.isRequired,
        onOptionSelected: React.PropTypes.func.isRequired
    },

    getState() {
        return {
            query: this.props.query,
            results: GeocodingStore.find(this.props.query)
        }
    },

    getInitialState() {
        return this.getState();
    },

    componentDidMount() {
        GeocodingStore.addChangeListener(this.onChange);
    },

    componentWillUnmount() {
        GeocodingStore.removeChangeListener(this.onChange);
    },

    handleOptionSelected(e, option) {
        this.props.onOptionSelected(option);

        e.preventDefault();
    },

    render() {
        if (this.state.results.size === 0)
            return null;

        var addresses = this.state.results.map(x => {
            return (
                /*jshint ignore:start*/
                <li key={x.id}>
                    <a href="#" onClick={(e) => { this.handleOptionSelected(e, x) }.bind(this) }>{x.address}</a>
                </li>
                /*jshint ignore:end*/
            );
        }.bind(this)).toArray();

        return (
            /*jshint ignore:start*/
            <ul>
                {addresses}
            </ul>
            /*jshint ignore:end*/
        );
    },

    onChange() {
        this.setState(this.getState());
    }
});

var AddressSearch = React.createClass({
    propTypes: {
        onOptionSelected: React.PropTypes.func.isRequired
    },

    getInitialState() {
        return {
            query: ""
        };
    },

    handleQueryChange() {
        var query = this.refs.query.getDOMNode().value;

        this.setState({
            query: query
        });

        GeocodingActions.autocomplete(query);
    },

    handleOptionSelected(option) {
        this.props.onOptionSelected(option);
    },

    render() {
        return (
            /*jshint ignore:start*/
            <div className="well">
                <input type="text" className="form-control" ref="query" placeholder="Søg efter adresse" onChange={this.handleQueryChange} />
                <AddressComponent onOptionSelected={this.handleOptionSelected} query={this.state.query} key={this.state.query} />
            </div>
            /*jshint ignore:end*/
        );
    }
})

export default React.createClass({
    propTypes: {
        onSubmit: React.PropTypes.func.isRequired
    },

    getDefaultProps() {
        return {
            disableRemove: true
        }
    },

    getDefaultState() {
        return {
            street: this.props.street || "",
            houseNumber: this.props.houseNumber || "",
            postalCode: this.props.postalCode || "",
            city: this.props.city || "",
            isPrimary: this.props.isPrimary || false
        };
    },

    getInitialState() {
        return this.getDefaultState();
    },

    componentWillReceiveProps(nextProps) {
        this.setState({
            street: this.props.street === this.state.street ? nextProps.street : this.state.street,
            houseNumber: this.props.houseNumber === this.state.houseNumber ? nextProps.houseNumber : this.state.houseNumber,
            postalCode: this.props.postalCode === this.state.postalCode ? nextProps.postalCode : this.state.postalCode,
            city: this.props.city === this.state.city ? nextProps.city : this.state.city,
            isPrimary: this.props.isPrimary === this.state.isPrimary ? nextProps.isPrimary : this.state.isPrimary,
        });
    },

    handleOptionSelected(option) {
        this.setState({
            street: option.street,
            houseNumber: option.houseNumber,
            postalCode: option.postalCode,
            city: option.city
        });
    },

    handleInputChange(prop, value) {
        let currentState = this.state;
        currentState[prop] = value;

        this.setState(currentState);
    },

    handleSubmit(e) {
        let state = this.state;

        this.props.onSubmit(state.street, state.houseNumber, state.city, state.postalCode, state.isPrimary);
        this.resetState();

        e.preventDefault();
    },

    handleClickRemove(e) {
        if (!this.props.disableRemove)
            this.props.onRemove();

        e.preventDefault();
    },

    resetState() {
        this.setState(this.getDefaultState());
    },

    render() {
        const { className, disableRemove } = this.props;

        const removeButton = (disableRemove) ? null : (
                <button className="btn btn-danger" onClick={this.handleClickRemove}>Slet adresse</button>
            );

        return (
            /*jshint ignore:start*/
            <div className={className}>
                <AddressSearch onOptionSelected={this.handleOptionSelected} />

                <form className="form-horizontal" onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <label className="col-md-2 control-label">Vej:</label>
                        <div className="col-md-7">
                            <input type="text" className="form-control" onChange={(e) => { this.handleInputChange("street", e.target.value); }.bind(this)} value={this.state.street} />
                        </div>

                        <label className="col-md-1 control-label">Nr:</label>
                        <div className="col-md-2">
                            <input type="text" className="form-control" onChange={(e) => { this.handleInputChange("houseNumber", e.target.value); }.bind(this)} value={this.state.houseNumber} />
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="col-md-2 control-label">Postnummer:</label>
                        <div className="col-md-2">
                            <input type="text" className="form-control" onChange={(e) => { this.handleInputChange("postalCode", e.target.value); }.bind(this)} value={this.state.postalCode} />
                        </div>

                        <label className="col-md-1 control-label">By:</label>
                        <div className="col-md-7">
                            <input type="text" className="form-control" onChange={(e) => { this.handleInputChange("city", e.target.value); }.bind(this)} value={this.state.city} />
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="col-sm-offset-2 col-sm-10">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" checked={this.state.isPrimary} onChange={(e) => { this.handleInputChange("isPrimary", e.target.checked); }.bind(this) } />
                                    Nuværende adresse
                                </label>
                            </div>
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="col-sm-offset-2 col-sm-10">
                            <button type="submit" className="btn btn-primary">Gem adresse</button>
                            {removeButton}
                        </div>
                    </div>
                </form>
            </div>
            /*jshint ignore:end*/
        );
    }
});