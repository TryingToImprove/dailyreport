/**
 *
 * @jsx React.DOM
 */

// Dependencies
var React = require("react"); 

// Stores
var EmployeesStore = require("../../stores/EmployeesStore");

// Actions
var EmployeeActions = require("../../actions/client/EmployeeActions");

// Components
var { Typeahead } = require("react-typeahead");

export default React.createClass({
    statics: {
        onLoad() {
            EmployeeActions.findAll();
        }
    },
    
    getState() {
        return {
            employees: EmployeesStore.findAll()
        };
    },

    getInitialState() {        
        return this.getState();
    },

    componentDidMount() {
        EmployeesStore.addChangeListener(this.onChange);
    },

    componentWillUnmount() {
        EmployeesStore.removeChangeListener(this.onChange);
    },
    
    //TODO: Add seach flux
    
    render() {
        return (
            /*jshint ignore:start */
            <Typeahead options={this.state.employees} 
                customClasses={{
                    input: "form-control",
                    results: "list-group",
                    listItem: "list-group-item",
                    hover: "active"
                }} 
                onOptionSelected={this.props.onOptionSelected}
                formatter={(employee) => `${employee.firstname} ${employee.lastname}`} />
            /*jshint ignore:end */
        );
    },
                                 
    onChange() {
        this.setState(this.getState());
    }
});