/**
 *
 * @jsx React.DOM
 */

// Dependencies
var React = require("react");
// Stores       

// Actions

// Constants                

// Components     

export default React.createClass({
    getInitialState() {
        return {
        }
    },

    render() {
        return (
            /*jshint ignore:start */
            <select ref="label" className={this.props.className}>
                <option>Telefonnummer</option>
                <option>Mobil</option>
                <option>Hjemmeside</option>
                <option>Email</option>
            </select>
            /*jshint ignore:end */
        );
    }
});