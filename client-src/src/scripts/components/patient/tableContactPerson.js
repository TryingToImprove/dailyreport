/**
 *
 * @jsx React.DOM
 */

// Dependencies
var React = require("react"); 
var { Link } = require("react-router");

// Actions
var { PatientContactPersonActions } = require("../../actions/client/PatientActions");

// Components
var CompleteContactPersonForm = require("./completeContactPersonForm");
var { Text } = require("../utils/loading");

export default React.createClass({
    renderRows() {
        var patient = this.props.patient;
        
        return this.props.contactPersons.map(contactPerson => {
            return (
                /*jshint ignore:start*/
                <tr key={contactPerson.id}>
                    <td>
                        <Link to="employee" params={{employeeId: contactPerson.employee.id}}>
                            {contactPerson.employee.firstname + ' '  + contactPerson.employee.lastname}
                        </Link>
                    </td>
                    <td>{contactPerson.timeStarted}</td>
                    <td><CompleteContactPersonForm patientId={patient.id} contactPerson={contactPerson} /></td>
                </tr>
                /*jshint ignore:end*/
            );
        });
    },
    
    render() {
        if (this.props.contactPersons === null) {
            return (
                /*jshint ignore:start*/
                <Text text="Henter kontaktpersoner" />
                /*jshint ignore:end*/
            );
        }

        return (
            /*jshint ignore:start*/
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Navn</th>
                        <th>Periode</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {this.renderRows()}
                </tbody>
            </table>
            /*jshint ignore:end*/
        );            
    }
});