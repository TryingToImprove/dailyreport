/**
 *
 * @jsx React.DOM
 */

(function () {
    "use strict";

    var React = require("react"),
       
        PatientCreateStore = require("../../../stores/PatientCreateStore"),
        
        PatientActions = require("../../../actions/client/PatientActions");
    
    module.exports = {
        name: "Færdig",
        onSubmit: function() {
            var firstname = PatientCreateStore.getFirstname(),
                lastname = PatientCreateStore.getLastname(),
                contactInformations = PatientCreateStore.getContactInformations().map(function(contactInformation) {
                    return {
                        dataTypeId: contactInformation.dataType,
                        data: contactInformation.data
                    };
                }),
                contactPersons = PatientCreateStore.getContactPersons();
            
            PatientActions.create(firstname, lastname, contactInformations, contactPersons);
        },
        nextStepText: "Godkend og gem",
        component: React.createClass({
           getState: function() {
                return {
                    firstname: PatientCreateStore.getFirstname(),
                    lastname: PatientCreateStore.getLastname()            
                };
            },

            componentDidMount: function() {
                PatientCreateStore.addChangeListener(this.onChange);
            },

            componentWillUnmount: function() {
                PatientCreateStore.removeChangeListener(this.onChange);
            },
            
            getInitialState: function() {
                return this.getState();
            },

            render: function(){ 
                return (
                    /*jshint ignore:start*/
                    <div className="form-horizontal">
                        <div className="form-group">
                            <label className="control-label col-md-2">Fornavn</label>
                            <div className="col-md-10">
                                {this.state.firstname}
                            </div>
                        </div>

                        <div className="form-group">
                            <label className="control-label col-md-2">Efternavn</label>
                            <div className="col-md-10">
                                {this.state.lastname}
                            </div>
                        </div>
                    </div>
                    /*jshint ignore:end*/
                );
            },
            onChange: function() {
                this.setState(this.getState());
            }
        })
    };
}());