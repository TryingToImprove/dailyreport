/**
 *
 * @jsx React.DOM
 */

(function () {
    "use strict";

    var React = require("react"),
       
        PatientCreateStore = require("../../../stores/PatientCreateStore"),
        
        PatientActions = require("../../../actions/client/PatientActions");
    
    module.exports = {
        name: "Personlige oplysninger",
        onSubmit: function(component) {
            var firstname = component.refs.firstname.getDOMNode().value,
                lastname = component.refs.lastname.getDOMNode().value;

            PatientActions.createWizardPersonal(firstname, lastname);
        },
        component: React.createClass({
           getState: function() {
                return {
                    firstname: PatientCreateStore.getFirstname(),
                    lastname: PatientCreateStore.getLastname()            
                };
            },

            componentDidMount: function() {
                PatientCreateStore.addChangeListener(this.onChange);
            },

            componentWillUnmount: function() {
                PatientCreateStore.removeChangeListener(this.onChange);
            },
            
            getInitialState: function() {
                return this.getState();
            },

            render: function(){ 
                return (
                    /*jshint ignore:start*/
                    <div>
                        <h3 className="no-top-margin">Personlige oplysninger</h3>
                        <p>Udfyld de personlige oplysninger omkring patienten..</p>
                        
                        <div className="clearfix">
                            <div className="form-horizontal col-md-6">
                                <div className="form-group">
                                    <label className="control-label col-md-2">Fornavn</label>
                                    <div className="col-md-10">
                                        <input type="text" ref="firstname" className="form-control" defaultValue={this.state.firstname} />
                                    </div>
                                </div>

                                <div className="form-group">
                                    <label className="control-label col-md-2">Efternavn</label>
                                    <div className="col-md-10">
                                        <input type="text" ref="lastname" className="form-control" defaultValue={this.state.lastname} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    /*jshint ignore:end*/
                );
            },
            onChange: function() {
                this.setState(this.getState());
            }
        })
    };
}());