/**
 *
 * @jsx React.DOM
 */

(function () {
    "use strict";

    var React = require("react"),
       
        PatientCreateStore = require("../../../stores/PatientCreateStore"),
        EmployeesStore = require("../../../stores/EmployeesStore"),
        
        PatientActions = require("../../../actions/client/PatientActions"),
        EmployeeActions = require("../../../actions/client/EmployeeActions");
    
    module.exports = {
        name: "Kontaktpersoner",
        isForm: false,
        onShown: function() {
            EmployeeActions.findAll();
        },
        component: React.createClass({
           getState: function() {
                return {
                    employees: EmployeesStore.findAll(),
                    selectedContactPersons: PatientCreateStore.getContactPersons()            
                };
            },

            componentDidMount: function() {
                PatientCreateStore.addChangeListener(this.onChange);            
                EmployeesStore.addChangeListener(this.onChange);
            },

            componentWillUnmount: function() {
                PatientCreateStore.removeChangeListener(this.onChange);
                EmployeesStore.removeChangeListener(this.onChange);
            },
                    
            getInitialState: function() {
                return this.getState();
            },

            render: function(){ 
                var personChecklist = (this.state.employees !== null) ? this.state.employees.map(function(employee) {
                        var isChecked = this.state.selectedContactPersons.indexOf(employee.id) >= 0;
                    
                        return (
                            /*jshint ignore:start*/
                            <div className="checkbox">
                                <label>
                                    <input type="checkbox" value={employee.id} onChange={this.onCheckedChange.bind(this, employee.id)} checked={isChecked} /> 
                                    {" " + employee.firstname + " " + employee.lastname}
                                </label>
                            </div> 
                            /*jshint ignore:end*/
                        );
                    }.bind(this)) : (
                            /*jshint ignore:start*/
                            <p className="text-muted">
                                Vent venligst, henter mulige kontaktpersoenr
                            </p>
                            /*jshint ignore:end*/
                        );
                
                return (
                    /*jshint ignore:start*/
                    <div>
                        <h3 className="no-top-margin">Vælg kontaktpersoner</h3>
                        <p>De valgte kontaktpesroner vil automatisk få besked hvis der er sker noget som omhandler personen,<br />og vil giver mulighed for at at andre ansatte kan se hvem de skal kontakte, hvis de har spørgsmål eller problemer</p>
                        <div className="form-horizontal">
                            <div className="form-group">
                                {personChecklist}
                            </div>
                        </div>
                    </div>
                    /*jshint ignore:end*/
                );
            },
            
            onChange: function() {
                this.setState(this.getState());
            },
            
            onCheckedChange: function(employeeId) {
                PatientActions.createWizardContactPerson(employeeId);
            }
        })
    };
}());