/**
 *
 * @jsx React.DOM
 */

(function () {
    "use strict";

    var React = require("react"),
       
        PatientCreateStore = require("../../../stores/PatientCreateStore"),
        ContactInformationsStore = require("../../../stores/ContactInformationsStore"),
        
        TextLoadingComponent = require("../../utils/loading").Text,
        
        PatientActions = require("../../../actions/client/PatientActions"),
        ContactInformationActions = require("../../../actions/client/ContactInformationActions");
    
    var ContactInformationItemComponent = React.createClass({
        render: function(){
            var type = this.props.type,
                value = this.props.value;
            
            return (
                /*jshint ignore:start*/
                <div>
                    <b>{type}</b><br />
                    {value}
                </div>
                /*jshint ignore:end*/
            );
        }
    });
    
    var ContactInformationsComponent = React.createClass({
        getState: function() {
            return {
                contactInformations: PatientCreateStore.getContactInformations()
            };
        },
        
        componentDidMount: function() {
            PatientCreateStore.addChangeListener(this.onChange);
        },

        componentWillUnmount: function() {
            PatientCreateStore.removeChangeListener(this.onChange);
        },
        
        getInitialState: function() {
            return this.getState();
        },
        
        render: function() {
            
            if (this.state.contactInformations === null || this.state.contactInformations.length === 0) {
                return (
                    /*jshint ignore:start*/
                    <TextLoadingComponent text="Der er ikke tilføjet nogen kontaktoplysninger til patienten" />
                    /*jshint ignore:end*/
                );
            }
            
            var items = this.state.contactInformations.map(function(item) {                
                return (
                    /*jshint ignore:start*/
                    <ContactInformationItemComponent type={item.dataTypeName} value={item.data} />
                    /*jshint ignore:end*/
                );
            });
            
            return (
                /*jshint ignore:start */  
                <div>
                    <h3 className="no-top-margin">Kontaktinformationer</h3>
                    {items}
                </div>
                /*jshint ignore:end*/
            );
        },
        
        onChange: function() {
            this.setState(this.getState());
        }
    });
    
    module.exports = {
        name: "Kontakt oplysninger",
        isForm: false,
        onShown: function() {
            ContactInformationActions.getTypes();
        },
        onSubmit: function(component) {
            var dataTypeDOM = component.refs.dataType.getDOMNode(),
                dataDOM = component.refs.data.getDOMNode();

            if (dataTypeDOM.value === '' || dataDOM.value === '') {
                return;
            }
            
            // Add the contact information
            PatientActions.createWizardAddContactInformation(dataTypeDOM.value, dataDOM.value);
            
        },
        component: React.createClass({            
            getState: function() {
                return {
                    dataType: '',
                    data: '',
                    contactInformationTypes: ContactInformationsStore.getTypes()
                };
            },
                        
            componentDidMount: function() {
                PatientCreateStore.addChangeListener(this.onChange);
                ContactInformationsStore.addChangeListener(this.onChange);
            },

            componentWillUnmount: function() {
                PatientCreateStore.removeChangeListener(this.onChange);
                ContactInformationsStore.removeChangeListener(this.onChange);
            },
            
            getInitialState: function() {
                return this.getState();
            },

            render: function(){ 
                var typeOptions = this.state.contactInformationTypes.map(function(contactInformationType) {                 
                    return (
                        /*jshint ignore:start*/
                        <option value={contactInformationType.id}>{contactInformationType.typeName}</option>
                        /*jshint ignore:end*/
                    );
                });
                
                return (
                    /*jshint ignore:start*/
                    <div className="row">
                        <div className="col-md-6">
                            <form className="form-horizontal" onSubmit={this.handleAdd}>
                                <div className="form-group">
                                    <label className="control-label col-md-2">Type</label>
                                    <div className="col-md-10">
                                        <select ref="dataType" className="form-control" defaultValue={this.state.dataType}>
                                            {typeOptions}
                                        </select>
                                    </div>
                                </div>

                                <div className="form-group">
                                    <label className="control-label col-md-2">Data</label>
                                    <div className="col-md-10">
                                        <input type="text" ref="data" className="form-control" defaultValue={this.state.data} />
                                    </div>
                                </div>
                    
                                <div className="form-group">
                                    <div className="col-md-offset-2 col-md-10">
                                        <input type="submit" className="btn btn-default" value="Tilføj" />
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div className="col-md-6">
                            <ContactInformationsComponent />
                        </div>
                    </div>
                    /*jshint ignore:end*/
                );
            },
            
            handleAdd: function() {
                var dataTypeDOM = this.refs.dataType.getDOMNode(),
                    dataDOM = this.refs.data.getDOMNode();

                // Add the contact information
                PatientActions.createWizardAddContactInformation(dataTypeDOM.value, dataDOM.value);
                
                // Reset the dom
                dataTypeDOM.value = '';
                dataDOM.value = '';
            },
            
            onChange: function() {
                this.setState(this.getState());
            }
        })
    };
}());