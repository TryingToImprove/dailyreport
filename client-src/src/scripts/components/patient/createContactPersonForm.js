/**
 *
 * @jsx React.DOM
 */

// Dependencies
var React = require("react"); 

// Actions
var { PatientContactPersonActions } = require("../../actions/client/PatientActions");

// Components
var EmployeeTypeahead = require("../employee/employeeTypeahead");
var { Typeahead } = require("react-typeahead");

export default React.createClass({    
    onOptionSelected(employee) {
        this.setState({ 
            employee: employee
        });
    },
        
    render() {
        return (
            <form className="form-horizontal" onSubmit={this.handleSubmit}>
                <h3>Tilføj ny kontaktperson</h3>
                <div className="form-group">
                    <label className="control-label col-md-4">Vælg ansat</label>
                    <div className="col-md-8">
                        <EmployeeTypeahead onOptionSelected={this.onOptionSelected} />
                    </div>
                </div>
                <div className="form-group">
                    <div className="checkbox col-md-8 col-md-offset-4">
                        <label>
                            <input type="checkbox" ref="completePrevious" /> Afslut igangværende kontaktpersoner
                        </label>
                    </div>
                </div>
                <div className="form-group">
                    <div className="col-md-offset-4 col-md-8">
                        <input type="submit" value="Tilføj" className="btn btn-success" />
                    </div>
                </div>
            </form>
        );
    },

    handleSubmit(e) {
        var employeeId = this.state.employee.id,
            completePrevious = this.refs.completePrevious.getDOMNode().checked;
        
        PatientContactPersonActions.add(this.props.patientId, employeeId, completePrevious);

        e.preventDefault();
    }
});