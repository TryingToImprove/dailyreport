/**
 *
 * @jsx React.DOM
 */

(function () {
    "use strict";

    var React = require("react"),
        
        Router = require("react-router"),
        Link = Router.Link;
    
    var PatientTableRow = React.createClass({
        render: function() {
            var patient = this.props.patient;
            
            return (                    
                /*jshint ignore:start */
                <tr>
                    <td><Link to="patient" params={{patientId: patient.id}}>{patient.firstname + " " +patient.lastname}</Link></td>
                    <td>{patient.timeCreated}</td>
                </tr>
                /*jshint ignore:end */
            );
        }
    });
    
    module.exports = React.createClass({
        render: function() {
            var patients = this.props.patients;
            
            if (typeof(patients) === "undefined" || patients.length === 0) {
                patients = [];
            }
            
            var patientTableRows = patients.map(function(patient) {
                return (                    
                    /*jshint ignore:start */
                    <PatientTableRow patient={patient} key={"patient-" + patient.id} />
                    /*jshint ignore:end */
                );
            });
            
            return (
                /*jshint ignore:start */
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Navn</th>
                            <th>Patient siden</th>
                        </tr>
                    </thead>
                    <tbody>
                        {patientTableRows}
                    </tbody>
                </table>
                /*jshint ignore:end */
            );
        }
    });
}());