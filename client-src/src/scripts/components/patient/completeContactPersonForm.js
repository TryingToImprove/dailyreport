/**
 *
 * @jsx React.DOM
 */

var React = require("react");
var { PatientContactPersonActions } = require("../../actions/client/PatientActions");

export default React.createClass({
    render() {
        var contactPerson = this.props.contactPerson;

        if (contactPerson.isEnded) {
            return (
                /*jshint ignore:start*/
                <span>{contactPerson.timeEnded}</span>
                /*jshint ignore:end*/
            )
        }

        /*jshint ignore:start*/
        return (
            <form onSubmit={this.handleSubmit}>
                <input type="submit" className="btn btn-xs btn-danger" value="Afslut periode" />
            </form>
        );
        /*jshint ignore:end*/
    },

    handleSubmit(e) {
        var contactPerson = this.props.contactPerson;

        PatientContactPersonActions.complete(this.props.patientId, contactPerson.id);

        e.preventDefault();
    }
});