/**
 *
 * @jsx React.DOM
 */

// Dependencies
var React = require("react");
var { Link } = require("react-router");

// Constants                
var LoadingConstants = require("../../constants/LoadingConstants");

// Components        
var { Text } = require("../utils/loading");

export default React.createClass({
    render() {
        if (this.props.contactPersons === null) {
            return (
                /*jshint ignore:start*/
                <Text text="Henter kontaktpersoner" />
                /*jshint ignore:end*/
            );
        }

        if (this.props.contactPersons.length === 0) {
            return (
                /*jshint ignore:start*/
                <Text text="Patienten har ingen kontaktpersoner" />
                /*jshint ignore:end*/
            );
        }

        var items = this.props.contactPersons.map(item => {
            var employee = item.employee;

            return (
                /*jshint ignore:start*/
                <li key={item.id}>
                    <Link to="employee" params={{employeeId: employee.id}}>
                        {employee.firstname + " " + employee.lastname}
                    </Link>
                </li>
                /*jshint ignore:end*/
            );
        });

        return (
            /*jshint ignore:start */  
            <ul>
                {items}
            </ul>
            /*jshint ignore:end*/
        );
    }
});