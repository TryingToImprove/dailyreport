/**
 *
 * @jsx React.DOM
 */

// Dependencies
import React from "react";

// Actions
import { PatientPostActions } from "../../actions/client/PatientActions";

// Componemnts
import TextareaComponent from "react-textarea-autosize";

export default React.createClass({
    propTypes: {
        patientId: React.PropTypes.number
    },

    handleSubmit(e) {
        var $postDescription = this.refs.postDescription.getDOMNode(),
            postDescription = $postDescription.value;

        PatientPostActions.create(this.props.patientId, [this.props.patientId], postDescription);

        $postDescription.value = '';

        e.preventDefault();
    },

    handleKeyDown(e) {
        var isCtrl = e.ctrlKey,
            isEnter = (e.keyCode === 13);
        
        if (isCtrl && isEnter) {
            this.handleSubmit(e);
        }
    },

    render() {
        return (
            /*jshint ignore:start */
            <form className={this.props.className} ref="form">
                <div className="form-group">
                    <TextareaComponent placeholder="Skriv tekst.." onKeyDown={this.handleKeyDown} className="form-control" ref="postDescription" rows={1} />
                </div>
                <div className="text-right">
                    <input type="submit" value="Skriv" className="btn btn-default" />
                </div>
            </form>
            /*jshint ignore:end */
        );
    }
});