/**
 *
 * @jsx React.DOM
 */

(function () {
    "use strict";

    var React = require("react"),        
        
        Router = require("react-router"),
        Link = Router.Link;
   
    module.exports = React.createClass({
        render: function() {
            return (
                /*jshint ignore:start */
                <nav className="navbar navbar-toolbar navbar-default navbar-subnavbar-fixed-top">
                    <Link to="/" className="navbar-brand">{this.props.organization.name}</Link>  
                
                    <ul className="nav navbar-nav">
                        <li>
                            <a className="dropdown-toggle" data-toggle="dropdown">Patienter</a>      
                            <ul className="dropdown-menu" role="menu">
                                <li><Link to="patients">Oversigt</Link></li>
                                <li><Link to="medicationScheduleHome">Medicinskema</Link></li>
                                <li className="divider"></li>
                                <li><Link to="patientCreate">Tilføj ny patient</Link></li>
                            </ul>
                        </li>
                        <li><Link to="patients">Logbog</Link></li> 
                        <li><Link to="patients">Indstillinger</Link></li> 
                     </ul>
                </nav>
                /*jshint ignore:end */
            );
        }
    });
}());