/**
 *
 * @jsx React.DOM
 */

(function () {
    "use strict";

    var React = require("react"),    
        
        AuthenticationActions = require("../actions/client/AuthenticationActions"),    
        
        Router = require("react-router"),
        Link = Router.Link;
   
    module.exports = React.createClass({
        getAuthenticationStateArea: function() {
            if (this.props.isAuthenticated) {
                var account = this.props.account;
                
                return (
                    /*jshint ignore:start */
                    <div>
                    <p className="navbar-text">
                        Logget ind som: <b>
                            <Link to="myProfile">
                                {account.firstname} {account.lastname}
                            </Link>
                        </b>
                    </p>
                    <ul className="nav navbar-nav">
                        <li><a href="#" onClick={this.handleUnauthenticationClick}>Log ud</a></li>
                    </ul>
                    </div>
                    /*jshint ignore:end */
                );
            }
            
            return (
                /*jshint ignore:start */
                <ul className="nav navbar-nav">
                    <li><Link to="authentication">Log ind</Link></li>
                </ul>
                /*jshint ignore:end */
            );
        },
        
        render: function() {
            var authenticationStateArea = this.getAuthenticationStateArea();
            
            return (
                /*jshint ignore:start */
                <div className="navbar navbar-default navbar-fixed-top">
                    <div className="navbar-header">
                        <button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                        </button>
                        
                        <Link to="/" className="navbar-brand">Dagsrapporten</Link>  
                    </div>
                    <div className="navbar-collapse collapse">
                        <div className="navbar-right">
                            {authenticationStateArea}
                        </div>
                    </div>
                </div>
                /*jshint ignore:end */
            );
        },
        
        handleUnauthenticationClick: function(e) {
            e.preventDefault();
            
            AuthenticationActions.unauthenticate();
        }
    });
}());