/**
 *
 * @jsx React.DOM
 */

// Dependencies
var React = require("react");
var Immutable = require("immutable");

// Stores       

// Actions

// Constants                

// Components        
var ContactInformationTypeSelector = require("./contactInformationTypeSelector");

var ContactInformationListItem = React.createClass({
    propTypes: {
        id: React.PropTypes.number.isRequired,
        onRemove: React.PropTypes.func.isRequired
    },

    getValue() {
        var label = this.refs.label.getDOMNode().value,
            value = this.refs.value.getDOMNode().value;

        if (label === "" || value === "") 
            return null;

        return {
            label: label,
            value: value
        }
    },
    
    handleRemove(e) {
        this.props.onRemove(this.props.id);

        e.preventDefault();
    },

    renderValueSideWithoutRemove() {
        return (
            <div className="col-sm-9">
                <input type="text" ref="value" className="form-control" placeholder="Value" />
            </div>
        );
    },

    renderValue() {
        return [
            <div className="col-sm-4">
                <input type="text" ref="value" className="form-control" placeholder="Value" />
            </div>,
            <div className="col-sm-5">
                <button onClick={this.handleRemove} className="btn btn-xs">
                    <span className="glyphicon glyphicon-remove"></span> Fjern
                </button>  
            </div>
        ];
    },

    render() {  
        var valueSide = this.props.disableRemove
            ? this.renderValueSideWithoutRemove()
            : this.renderValue();

        return (
            /*jshint ignore:start */
            <div className={this.props.className}>
                <div className="form-group">
                    <div className="col-sm-3">
                        <ContactInformationTypeSelector ref="label" className="form-control" />
                    </div>
                    {valueSide}
                </div>
            </div>
            /*jshint ignore:end */
        );
    }
});

export default React.createClass({
    getInitialState() {
        return {
            count: 0,
            items: Immutable.Map()
        }
    },

    componentWillMount() {
        var amount = this.props.initialAmount || 0;
        this.add(amount);
    },

    remove(id) {
        var state = this.state;
        state.items = state.items.remove(id);

        this.setState(state);
    },

    add(amount) {
        var state = this.state;
        
        for(var i = 0; i < amount; i++) {
            state.items = state.items.set(state.count++, {});
        }

        this.setState(state);    
    },

    getValue() {
        var result = [];

        for (var ref in this.refs) {
            if (this.refs.hasOwnProperty(ref)) { 
                var value = this.refs[ref].getValue();

                if(value !== null) {
                    result.push(value);
                }
            }
        }

        return result;
    },

    handleAdd(e) {
        this.add(1);

        e.preventDefault();
    },

    render() {  
        var classes = this.props.classes || {
            "item": ""
        };

        var items = this.state.items.map((value, key) => {
            var itemClasses = classes.item || null;

            return (                
                /*jshint ignore:start */
                <ContactInformationListItem disableRemove={this.props.disableRemove || false} ref={"item-" + key} key={"item-" + key} onRemove={this.remove} className={itemClasses} id={key} />
                /*jshint ignore:end */
            );
        }.bind(this)).toArray();

        return (
            /*jshint ignore:start */
            <div>
                {items}
            </div>
            /*jshint ignore:end */
        );
    }
});