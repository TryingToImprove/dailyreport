/**
 *
 * @jsx React.DOM
 */

// Dependencies
var React = require("react"); 
var Immutable = require("immutable");
var moment = require("../../utils/moment");

// Entries
var ContactPersonComponent = require("./components/ContactPersonEntry");
var ContactPersonEndedComponent = require("./components/ContactPersonEndedEntry");
var MedicationScheduleComponent = require("./components/MedicationScheduleEntry");
var MedicationScheduleCompleteComplete = require("./components/MedicationScheduleCompleteEntry");
var PatientPostComponent = require("./components/PatientPostEntry");

export default React.createClass({
    renderEntry(entry) {
        //TODO: Give a unique valid id
        var key = entry.uniqueKey;
        
        if ([ "ContactPerson"].indexOf(entry.entryKey) >= 0)
            return (
                /*jshint ignore:start*/
                <ContactPersonComponent key={key} {...entry.entryData} entryKey={entry.entryKey} entryDateTime={entry.entryDateTime} />
                /*jshint ignore:end*/
            ); 
        
        if (["ContactPersonEnded"].indexOf(entry.entryKey) >= 0)
            return (
                /*jshint ignore:start*/
                <ContactPersonEndedComponent key={key} {...entry.entryData} entryKey={entry.entryKey} entryDateTime={entry.entryDateTime} />
                /*jshint ignore:end*/
            ); 
        
        if (["MedicationSchedule"].indexOf(entry.entryKey) >= 0)
            return (
                /*jshint ignore:start*/
                <MedicationScheduleComponent key={key} {...entry.entryData} entryKey={entry.entryKey} entryDateTime={entry.entryDateTime} />
                /*jshint ignore:end*/
            ); 
        
        if (["MedicationSchedulesComplete"].indexOf(entry.entryKey) >= 0)
            return (
                /*jshint ignore:start*/
                <MedicationScheduleCompleteComplete key={key} {...entry.entryData} entryKey={entry.entryKey} entryDateTime={entry.entryDateTime} />
                /*jshint ignore:end*/
            ); 
        
        if (["PatientPost"].indexOf(entry.entryKey) >= 0)
            return (
                /*jshint ignore:start*/
                <PatientPostComponent key={key} {...entry.entryData} entryKey={entry.entryKey} entryDateTime={entry.entryDateTime} />
                /*jshint ignore:end*/
            ); 
    
        return undefined;
    },
    
    renderGroups() {
        return this.props.entries.map((entry) => {
            var entries = entry.value.map(x => this.renderEntry(x)),
                timestamp = moment(entry.key),
                title = timestamp.format("D. MMM YY"),
                reactKey = timestamp.format("[timeline-section-]DD-MM-YY");
            
            return (
                /*jshint ignore:start*/
                <section key={reactKey}>
                    <div className="title">
                        <h1>{title}</h1>
                    </div>

                    <div className="content">
                        {entries}
                    </div>
                </section>
                /*jshint ignore:end*/
            );
        }.bind(this)).toArray();
    },
    
    render() {
        return (
            /*jshint ignore:start*/
            <div className="timeline">
                {this.renderGroups()}
            </div>
            /*jshint ignore:end*/
        );
    }
});
