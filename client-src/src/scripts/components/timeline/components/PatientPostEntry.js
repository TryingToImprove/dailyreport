/**
 *
 * @jsx React.DOM
 */

// Dependencies
var React = require("react"); 
import { Link } from "react-router";

export default React.createClass({
    renderEmployeeLink() {
        var employee = this.props.employee,
            name = `${employee.firstname} ${employee.lastname}`;
        
        return (
            /*jshint ignore:start*/
            <Link className="profile-title__header" to="employee" params={{ employeeId: employee.id }}>
                {name}
            </Link>
            /*jshint ignore:end*/
        );  
    },
    render() {
        
        return (
            /*jshint ignore:start*/
            <div className="entry">
                <div className="history">
                    <div className="timeline-post">
                        <div className="profile-title clearfix">
                            <img src="http://lorempixel.com/output/people-q-c-75-75-3.jpg" alt="Picture" className="profile-title__pic" />
                            <div className="profile-title__content">                       
                                {this.renderEmployeeLink()}
                                <div><span className="time">skrevet kl. 14:43</span></div>
                            </div>
                        </div>
                        <div className="post-content">
                            {this.props.description}
                        </div>
                    </div>
                </div>
            </div>
            /*jshint ignore:end*/
        );    
    }
});