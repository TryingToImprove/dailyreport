/**
 *
 * @jsx React.DOM
 */

// Dependencies
var React = require("react"); 

export default React.createClass({
    render() {
        var medicationList = this.props.medications.map(x => {

            return (
                /*jshint ignore:start*/
                <tr key={x.id}>
                    <td>{x.medication.name}</td>
                    <td>{x.amouth}</td>
                    <td>{x.executeTime}</td>
                    <td></td>
                </tr>
                /*jshint ignore:end*/
            );
        });

        return (
            /*jshint ignore:start*/
            <div className="entry">
                <div className="history">                
                    <div className="timeline-post">
                        <div className="profile-title clearfix">
                            <img className="profile-title__icon" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAJjgAACY4B8ZaolQAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAUUSURBVHic7ZtdaBxVFMd/Z2PS0g2mpWo+nqwUjcZWWlFXtPhRLaiUIn4gteCLitBWfVIE6ZPosxj1TYpYqdRGsRhFBLWCttq0xj74LfGDGrWNSasxrUmOD2eWvZnM7O5sZnZ22f3DZZk795xz53/PPefOnbuiqjQyMml3IG00PAFnBVWKSA64PiGbe1X1u4R0R0YgAdjDP5OQza+BmiGg4adAk4C0O5A2mgSk3YG00fAEoKrzCtABnA+8CGhMZbunc0mQzbRK4DpAVSeACRGZcKpniZa/W4CVzvWfqjoSQb4qCFsIBeGUqvaW21hEuoDfonepumj4GNDwBMybAiIiwJXA7cCWGG09KSIrgAFV/SZGvQuDL/qvBL4kOIqfiBJdgXND9CgwCCxLOwOoaoEA4BbgryKdfiWycjhURN/3wKVpEyCqioj0AUeAVsc5JoEh4AvgKLBLVSejeJeILAfuAy4ELgcu89kYBXq9tJsaBNjP3NEZADpjdzfoBQ76bD2X6hQA7vF16NWEDbZhXpW3NwNckhYBGeBGxxtOANuSdDdVPYNNizwyJLf9VhIZYLVz/ZGqjiVtVFWHgR+dqtVhbZNGBljlXB+pom3X1qrQVgkjAyxxrv+uom3XVraKdueg4ZfCTQLS7kDaaBJQiZCIPCsioyHlgbg7mSSi7Ai56AA6Q+6lFtErQXMKpN2BtFFyCojIUqDLV91RROQ8EfFvno6o6lQRmXYR2YGtCHuwTZl9qjpYqn9xwH0TfDTg7e0hwjc1yi25AL07y5DbBWSTfBusNAhWC5u933uTMlAPMWCziFydlPJ6IABga1KK64WAjSKyKAnF9ULA2cCGJBSXEwQngd99dR3A4pD2pzwZF2ci9isIdwH7YtAzD0XTYMjG5k7CU1ccOoLKONCWxKZovaADuE1EYu1zra8D/BgAVER+xb4vHPDKUImVZijqgYAZ4GOgG7gIeBn7wLIRuNNr85+IDFMg5ICq/lCO8koJOAwsDblXluESmAY+BF4H3lDVP0RkK9AP7FbVd0WkDVgD5JyyzSuIyHEKhBwEPlPVk0HGIgewOArBQfA94H7gnID23Zg3vFREZw+WcYKC6AwwDDwCtDsyNUXAuhIy+7Ev2IHZAPv4Wk5GGQUurqUsMO793l2i3R5s6t0ccr/cd4ZO4H0R6awVAn7C3POOEmluLzaCYUTlItjsAR6sFQIUG91u4NrQRqrHgE+ATV4Q9CMKAQBbMsz9SHlBRAULgWtrBCMASk+DN7FF0Vq3UkSWYQcxoiCbAT53Kq6IqKAiiEgLlsLyGFLVb7GtsFLT4Cvv9wYRcd9HrsIOe0TBvxnsHE8eORG5NaKSSvA40O5cH/Z+92D7j+uKyOblngZOisghEXke27qLit1grvgPhRRxDOhLMP1tAE479n7Gy8vYKbVp4O0i8m8BU8ATwGvY9Klkn3Ic6Morfdh3cwrYgc2z1hgeOgtcg63kZh07s8BNvrb93r3HfPWCeY4CT/nudQGbsP85fYC9khd7+OPAWtXCKTHxBK8LcJMZb1QWglaCN1/6VXW7W+EFs0Esog8B7wCLgPXYgHyKkRZ6Ys2LMX1YXMhhC6TFmOcNYKtJO8fssNgOvMDcEUqqTGGj2RLiMa3YaP7iyIxh6/xMrFMywPh6LChNJ/DgpzFPK/uAJLACWJ5UTBLPSJAbZbFUtYaFf/Acw9z5qNopsZpBKAGNglpZCqeGhifgf73hvoMciTITAAAAAElFTkSuQmCC8afffc2b60974eca7dd42b1266324df1"/>
                            <div className="profile-title__content">                       
                                <span className="profile-title__header">Opstartede en ny medicinering</span>
                                <div><span className="time">skrevet kl. 14:43</span></div>
                            </div>
                        </div>
                        <table className="post-table table">
                            <thead>
                                <tr>
                                    <th>Medicin</th>
                                    <th>Mængde</th>
                                    <th>Tidspunkt</th>
                                    <th>Gentag</th>
                                </tr>
                            </thead>    
                            <tbody>
                                {medicationList}
                            </tbody>
                        </table>
                    </div>                     
                </div>
            </div>
            /*jshint ignore:end*/
        );    
    }
});