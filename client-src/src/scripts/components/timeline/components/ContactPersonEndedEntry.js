/**
 *
 * @jsx React.DOM
 */

// Dependencies
var React = require("react"); 
import { Link } from "react-router";

export default React.createClass({
    renderEmployeeLink() {
        var employee = this.props.employee,
            name = `${employee.firstname} ${employee.lastname}`;
        
        return (
            /*jshint ignore:start*/
            <Link to="employee" params={{ employeeId: employee.id }}>
                {name}
            </Link>
            /*jshint ignore:end*/
        );  
    },
    render() {        
        return (
            /*jshint ignore:start*/
            <div className="entry">
                <div className="history">
                    Afsluttede {this.renderEmployeeLink()} som kontaktperson <span className="time">kl. 14:43</span>
                </div>
            </div>
            /*jshint ignore:end*/
        );    
    }
});