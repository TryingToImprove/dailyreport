/**
 *
 * @jsx React.DOM
 */

(function () {
    "use strict";

    var React = require("react/addons"),
        AccountService = require("./utils/services/AccountService"),
        AuthenticationActions = require("./actions/server/AuthenticationActions"),
        AppDispatcher = require("./dispatcher/AppDispatcher"),
        AuthenticationConstants = require("./constants/AuthenticationConstants"),
        router = require("./router");
    
    // Lets check if the user is authenticated
    var auth_token = localStorage.getItem("auth_token");
    if (auth_token !== null) {
         // Call the API 
        AccountService.reauthenticate(auth_token)
            .then(function(response) {
                AuthenticationActions.reauthenticateSuccess(response);

                run();
            });

    } else {
        run();
    }
    
    function run() {
        router.run(function (Handler, state) {        
            /*jshint ignore:start */
            React.render(<Handler />, document.body);
            /*jshint ignore:end */
        });
    }
    
    
}());