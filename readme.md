# DailyReport

DailyReport (Dagsrapporten) er et administrations program til opholdsteder. Programmets fokus ligger i

- Sikkerhed
- Brugervenlighed
- Performence

## Opgavestyring
Der bliver brugt trello til at håndtere de forskellige opgaver; https://trello.com/b/qOcN4jSH/udvikling (kræver godkendelse)

## Open Source projekter der er kommet til pga. projektet
Der er kommet nogle open source projekter ud af at lave DailyReport, som muligvis er brugbare for andre, og som sådan ingen har værdi har i sig selv.

- [MessageQuerer](https://github.com/TryingToImprove/MessageQueuer)
- [Denmark-City-Structure](https://github.com/TryingToImprove/denmark-city-structure)

Derudover er der blevet forked og sendt pull requests til 
- [react-typeahead](https://github.com/TryingToImprove/react-typeahead)