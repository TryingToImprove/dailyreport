/****** Object:  Table [dbo].[Address]    Script Date: 1/13/2015 1:34:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Address](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Street] [nvarchar](150) NOT NULL,
	[TownId] [int] NOT NULL,
	[HouseNumber] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_Address] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Address_Postalcode]    Script Date: 1/13/2015 1:34:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Address_Postalcode](
	[AddressId] [int] NOT NULL,
	[Postalcode] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_Address_Postalcode] PRIMARY KEY CLUSTERED 
(
	[AddressId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AuthenticationIdentity]    Script Date: 1/13/2015 1:34:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AuthenticationIdentity](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[TimeCreated] [datetime] NOT NULL,
 CONSTRAINT [PK_AuthenticationIdentity] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AuthenticationIdentityKey]    Script Date: 1/13/2015 1:34:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AuthenticationIdentityKey](
	[AuthenticationKey] [nvarchar](128) NOT NULL,
	[TimeCreated] [datetime] NOT NULL,
 CONSTRAINT [PK_AuthenticationIdentityKey] PRIMARY KEY CLUSTERED 
(
	[AuthenticationKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AuthenticationIdentityKey_AuthenticationIdentity]    Script Date: 1/13/2015 1:34:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AuthenticationIdentityKey_AuthenticationIdentity](
	[AuthenticationIdentityId] [int] NOT NULL,
	[AuthenticationKey] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_AuthenticationIdentityKey_AuthenticationIdentity] PRIMARY KEY CLUSTERED 
(
	[AuthenticationKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AuthenticationLog]    Script Date: 1/13/2015 1:34:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AuthenticationLog](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TimeCreated] [datetime] NOT NULL,
	[AuthenticationId] [int] NOT NULL,
 CONSTRAINT [PK_AuthenticationLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AuthenticationPassword]    Script Date: 1/13/2015 1:34:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AuthenticationPassword](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AuthenticationIdentityId] [int] NOT NULL,
	[TimeCreated] [date] NOT NULL,
	[Password] [nvarchar](max) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_AuthenticationPasswords] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Contact]    Script Date: 1/13/2015 1:34:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contact](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Firstname] [nvarchar](50) NOT NULL,
	[Lastname] [nvarchar](50) NOT NULL,
	[TimeCreated] [datetime] NOT NULL,
 CONSTRAINT [PK_Contact] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Contact_Address]    Script Date: 1/13/2015 1:34:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contact_Address](
	[ContactId] [int] NOT NULL,
	[AddressId] [int] NOT NULL,
 CONSTRAINT [PK_Contact_Address] PRIMARY KEY CLUSTERED 
(
	[ContactId] ASC,
	[AddressId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Contact_ContactGroup]    Script Date: 1/13/2015 1:34:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contact_ContactGroup](
	[ContactId] [int] NOT NULL,
	[ContactGroupId] [int] NOT NULL,
 CONSTRAINT [PK_Contact_ContactGroup] PRIMARY KEY CLUSTERED 
(
	[ContactId] ASC,
	[ContactGroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Contact_ContactInformation]    Script Date: 1/13/2015 1:34:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contact_ContactInformation](
	[ContactId] [int] NOT NULL,
	[ContactInformationId] [int] NOT NULL,
 CONSTRAINT [PK_Contact_ContactInformation] PRIMARY KEY CLUSTERED 
(
	[ContactInformationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Contact_ContactType]    Script Date: 1/13/2015 1:34:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contact_ContactType](
	[ContactId] [int] NOT NULL,
	[ContactTypeId] [int] NOT NULL,
 CONSTRAINT [PK_Contact_ContactType] PRIMARY KEY CLUSTERED 
(
	[ContactId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Contact_Patient]    Script Date: 1/13/2015 1:34:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contact_Patient](
	[ContactId] [int] NOT NULL,
	[PatientId] [int] NOT NULL,
 CONSTRAINT [PK_Contact_Patient] PRIMARY KEY CLUSTERED 
(
	[ContactId] ASC,
	[PatientId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ContactGroup]    Script Date: 1/13/2015 1:34:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContactGroup](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_ContactGroup] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ContactInformation]    Script Date: 1/13/2015 1:34:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContactInformation](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ContactInformationTypeId] [int] NOT NULL,
	[Data] [text] NOT NULL,
	[TimeCreated] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_ContactInformation] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ContactInformationType]    Script Date: 1/13/2015 1:34:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContactInformationType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TypeName] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_ContactInformationType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ContactType]    Script Date: 1/13/2015 1:34:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContactType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_ContactType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Country]    Script Date: 1/13/2015 1:34:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Country](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Description]    Script Date: 1/13/2015 1:34:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Description](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Text] [text] NOT NULL,
	[TimeCreated] [datetime] NOT NULL,
	[EmployeeId] [int] NOT NULL,
 CONSTRAINT [PK_Description] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Employee]    Script Date: 1/13/2015 1:34:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationId] [int] NOT NULL,
	[Firstname] [nvarchar](150) NOT NULL,
	[Lastname] [nvarchar](150) NOT NULL,
	[TimeCreated] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Employee_IsActive]  DEFAULT ((1)),
	[Email] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Employee_ContactInformation]    Script Date: 1/13/2015 1:34:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee_ContactInformation](
	[ContactInformationId] [int] NOT NULL,
	[EmployeeId] [int] NOT NULL,
 CONSTRAINT [PK_Employee_ContactInformation] PRIMARY KEY CLUSTERED 
(
	[ContactInformationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EmployeeAuthenticationIdentityKey]    Script Date: 1/13/2015 1:34:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmployeeAuthenticationIdentityKey](
	[EmployeeId] [int] NOT NULL,
	[AuthenticationKey] [nvarchar](128) NOT NULL,
	[TimeCreated] [datetime] NOT NULL,
 CONSTRAINT [PK_EmployeeAuthenticationIdentityKey] PRIMARY KEY CLUSTERED 
(
	[EmployeeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Measure]    Script Date: 1/13/2015 1:34:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Measure](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Measure] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Medication]    Script Date: 1/13/2015 1:34:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Medication](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[MedicationBrandId] [int] NOT NULL,
	[MedicationTypeId] [int] NOT NULL,
	[MeasureId] [int] NOT NULL,
	[Weight] [decimal](16, 2) NOT NULL,
 CONSTRAINT [PK_Medication] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MedicationBrand]    Script Date: 1/13/2015 1:34:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MedicationBrand](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
 CONSTRAINT [PK_MedicationBrand] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MedicationSchedule]    Script Date: 1/13/2015 1:34:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MedicationSchedule](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PatientId] [int] NOT NULL,
	[TimeCreated] [datetime] NOT NULL,
	[StartDate] [date] NOT NULL,
 CONSTRAINT [PK_MedicationSchedule] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MedicationScheduleComplete]    Script Date: 1/13/2015 1:34:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MedicationScheduleComplete](
	[MedicationScheduleId] [int] NOT NULL,
	[TimeCreated] [datetime] NOT NULL,
	[Description] [text] NOT NULL,
	[EmployeeId] [int] NOT NULL,
 CONSTRAINT [PK_MedicationScheduleComplete] PRIMARY KEY CLUSTERED 
(
	[MedicationScheduleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MedicationScheduleItem]    Script Date: 1/13/2015 1:34:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MedicationScheduleItem](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MedicationScheduleId] [int] NOT NULL,
	[MedicationId] [int] NOT NULL,
	[ExecuteTime] [time](7) NOT NULL,
	[Amouth] [decimal](16, 2) NOT NULL,
 CONSTRAINT [PK_MedicationScheduleItem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MedicationScheduleItemHistory]    Script Date: 1/13/2015 1:34:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MedicationScheduleItemHistory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MedicationScheduleItemId] [int] NOT NULL,
	[TimeCreated] [datetime] NOT NULL,
 CONSTRAINT [PK_MedicationScheduleItemHistory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MedicationScheduleItemHistoryEmployee]    Script Date: 1/13/2015 1:34:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MedicationScheduleItemHistoryEmployee](
	[MedicationScheduleItemHistoryId] [int] NOT NULL,
	[EmployeeId] [int] NOT NULL,
 CONSTRAINT [PK_MedicationScheduleItemHistoryEmployee] PRIMARY KEY CLUSTERED 
(
	[MedicationScheduleItemHistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MedicationScheduleItemRepeat]    Script Date: 1/13/2015 1:34:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MedicationScheduleItemRepeat](
	[MedicationScheduleItemId] [int] NOT NULL,
	[RepeatTimeSpan] [bigint] NOT NULL,
 CONSTRAINT [PK_MedicationScheduleItemRepeat] PRIMARY KEY CLUSTERED 
(
	[MedicationScheduleItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MedicationType]    Script Date: 1/13/2015 1:34:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MedicationType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_MedicationType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Municipality]    Script Date: 1/13/2015 1:34:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Municipality](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[RegionId] [int] NOT NULL,
 CONSTRAINT [PK_Municipality] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Organization]    Script Date: 1/13/2015 1:34:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Organization](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[TimeCreated] [datetime] NOT NULL,
 CONSTRAINT [PK_Organization] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Patient]    Script Date: 1/13/2015 1:34:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Patient](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationId] [int] NOT NULL,
	[Firstname] [nvarchar](150) NOT NULL,
	[Lastname] [nvarchar](150) NOT NULL,
	[TimeCreated] [datetime] NOT NULL,
	[BirthDate] [date] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_Patient] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Patient_ContactInformation]    Script Date: 1/13/2015 1:34:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Patient_ContactInformation](
	[ContactInformationId] [int] NOT NULL,
	[PatientId] [int] NOT NULL,
 CONSTRAINT [PK_Patient_ContactInformation] PRIMARY KEY CLUSTERED 
(
	[ContactInformationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PatientContactPersonRelationship]    Script Date: 1/13/2015 1:34:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PatientContactPersonRelationship](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PatientId] [int] NOT NULL,
	[EmployeeId] [int] NOT NULL,
	[TimeCreated] [datetime] NOT NULL,
 CONSTRAINT [PK_PatientContactPersonRelationship] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PatientContactPersonRelationshipEnded]    Script Date: 1/13/2015 1:34:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PatientContactPersonRelationshipEnded](
	[PatientContactPersonRelationshipId] [int] NOT NULL,
	[TimeCreated] [datetime] NOT NULL,
 CONSTRAINT [PK_PatientContactPersonRelationshipEnded] PRIMARY KEY CLUSTERED 
(
	[PatientContactPersonRelationshipId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PatientDescription]    Script Date: 1/13/2015 1:34:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PatientDescription](
	[PatientId] [int] NOT NULL,
	[DescriptionId] [int] NOT NULL,
 CONSTRAINT [PK_PatientDescription] PRIMARY KEY CLUSTERED 
(
	[PatientId] ASC,
	[DescriptionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PatientPost]    Script Date: 1/13/2015 1:34:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PatientPost](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeId] [int] NOT NULL,
	[TimeCreated] [datetime] NOT NULL,
	[Description] [text] NOT NULL,
	[OrganizationId] [int] NOT NULL,
 CONSTRAINT [PK_PatientPost] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PatientPost_Patient]    Script Date: 1/13/2015 1:34:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PatientPost_Patient](
	[PatientId] [int] NOT NULL,
	[PatientPostId] [int] NOT NULL,
 CONSTRAINT [PK_PatientPost_Patient] PRIMARY KEY CLUSTERED 
(
	[PatientId] ASC,
	[PatientPostId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Region]    Script Date: 1/13/2015 1:34:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Region](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[CountryId] [int] NOT NULL,
 CONSTRAINT [PK_Region] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Town]    Script Date: 1/13/2015 1:34:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Town](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[RegionId] [int] NOT NULL,
 CONSTRAINT [PK_Town] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TownMunicipality]    Script Date: 1/13/2015 1:34:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TownMunicipality](
	[TownId] [int] NOT NULL,
	[MunicipalityId] [int] NOT NULL,
 CONSTRAINT [PK_TownMunicipality] PRIMARY KEY CLUSTERED 
(
	[TownId] ASC,
	[MunicipalityId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TownPostalcode]    Script Date: 1/13/2015 1:34:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TownPostalcode](
	[TownId] [int] NOT NULL,
	[Postalcode] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_TownPostalcode] PRIMARY KEY CLUSTERED 
(
	[TownId] ASC,
	[Postalcode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[AuthenticationIdentity] ON 

INSERT [dbo].[AuthenticationIdentity] ([Id], [Username], [IsActive], [TimeCreated]) VALUES (12, N'JegErGud', 1, CAST(N'2015-01-04 18:21:38.777' AS DateTime))
INSERT [dbo].[AuthenticationIdentity] ([Id], [Username], [IsActive], [TimeCreated]) VALUES (13, N'kim larsen', 1, CAST(N'2015-01-04 18:40:19.257' AS DateTime))
INSERT [dbo].[AuthenticationIdentity] ([Id], [Username], [IsActive], [TimeCreated]) VALUES (14, N'oliver', 1, CAST(N'2015-01-07 19:31:20.860' AS DateTime))
INSERT [dbo].[AuthenticationIdentity] ([Id], [Username], [IsActive], [TimeCreated]) VALUES (16, N'OLL', 1, CAST(N'2015-01-07 19:36:31.890' AS DateTime))
INSERT [dbo].[AuthenticationIdentity] ([Id], [Username], [IsActive], [TimeCreated]) VALUES (17, N'olla', 1, CAST(N'2015-01-10 16:34:31.873' AS DateTime))
INSERT [dbo].[AuthenticationIdentity] ([Id], [Username], [IsActive], [TimeCreated]) VALUES (18, N'diru', 1, CAST(N'2015-01-10 17:11:39.340' AS DateTime))
INSERT [dbo].[AuthenticationIdentity] ([Id], [Username], [IsActive], [TimeCreated]) VALUES (19, N'ditte-rusuunen', 1, CAST(N'2015-01-10 18:17:16.567' AS DateTime))
INSERT [dbo].[AuthenticationIdentity] ([Id], [Username], [IsActive], [TimeCreated]) VALUES (20, N'oliver__lassen', 1, CAST(N'2015-01-10 18:18:21.147' AS DateTime))
SET IDENTITY_INSERT [dbo].[AuthenticationIdentity] OFF
INSERT [dbo].[AuthenticationIdentityKey] ([AuthenticationKey], [TimeCreated]) VALUES (N'1095776546DA522DEC08E929CA57EB65', CAST(N'2015-01-10 18:17:36.427' AS DateTime))
INSERT [dbo].[AuthenticationIdentityKey] ([AuthenticationKey], [TimeCreated]) VALUES (N'18116FAF38DBC57250743BB56E40166E', CAST(N'2015-01-10 18:16:33.103' AS DateTime))
INSERT [dbo].[AuthenticationIdentityKey] ([AuthenticationKey], [TimeCreated]) VALUES (N'3B1824CFB1C295DABF86874F5EAD9AAD', CAST(N'2015-01-04 18:41:56.117' AS DateTime))
INSERT [dbo].[AuthenticationIdentityKey] ([AuthenticationKey], [TimeCreated]) VALUES (N'5712B1D3124E5E52F851894C28F624F9', CAST(N'2015-01-04 17:11:11.947' AS DateTime))
INSERT [dbo].[AuthenticationIdentityKey] ([AuthenticationKey], [TimeCreated]) VALUES (N'58E1B71EECE98384E63A2F18FB0375EE', CAST(N'2015-01-04 18:39:48.807' AS DateTime))
INSERT [dbo].[AuthenticationIdentityKey] ([AuthenticationKey], [TimeCreated]) VALUES (N'5E7FDE30677F8FFA5D096E9EA64603E7', CAST(N'2015-01-10 16:33:46.070' AS DateTime))
INSERT [dbo].[AuthenticationIdentityKey] ([AuthenticationKey], [TimeCreated]) VALUES (N'65D54D4CAA92F48B7E044D20E424B4B8', CAST(N'2015-01-10 18:01:45.383' AS DateTime))
INSERT [dbo].[AuthenticationIdentityKey] ([AuthenticationKey], [TimeCreated]) VALUES (N'6E13313AB563E6090F60ACC77D316D8E', CAST(N'2015-01-04 17:19:04.167' AS DateTime))
INSERT [dbo].[AuthenticationIdentityKey] ([AuthenticationKey], [TimeCreated]) VALUES (N'7531A1B9D263D52A5935111AE7E9F329', CAST(N'2015-01-04 17:19:15.937' AS DateTime))
INSERT [dbo].[AuthenticationIdentityKey] ([AuthenticationKey], [TimeCreated]) VALUES (N'80B697A9A72498EB702F8468CAAB4A9B', CAST(N'2015-01-04 18:36:35.253' AS DateTime))
INSERT [dbo].[AuthenticationIdentityKey] ([AuthenticationKey], [TimeCreated]) VALUES (N'9ACE36DEFB058552C88B0F9D69DAEF99', CAST(N'2015-01-07 19:35:11.583' AS DateTime))
INSERT [dbo].[AuthenticationIdentityKey] ([AuthenticationKey], [TimeCreated]) VALUES (N'AC6F6ADD9ACDA1B95B00113B7F001FFA', CAST(N'2015-01-04 17:58:02.133' AS DateTime))
INSERT [dbo].[AuthenticationIdentityKey] ([AuthenticationKey], [TimeCreated]) VALUES (N'B25918DE2415A72A7CAC5F54273FA380', CAST(N'2015-01-07 19:16:10.800' AS DateTime))
INSERT [dbo].[AuthenticationIdentityKey] ([AuthenticationKey], [TimeCreated]) VALUES (N'E8017CBFC4A5555231A9D497632603CC', CAST(N'2015-01-10 17:10:31.373' AS DateTime))
INSERT [dbo].[AuthenticationIdentityKey] ([AuthenticationKey], [TimeCreated]) VALUES (N'EBF613EA18A4BA7C40D2309D251B7D83', CAST(N'2015-01-04 17:20:15.717' AS DateTime))
INSERT [dbo].[AuthenticationIdentityKey] ([AuthenticationKey], [TimeCreated]) VALUES (N'F64029F092185AFC61DDCBB653EBF98B', CAST(N'2015-01-04 17:12:18.297' AS DateTime))
INSERT [dbo].[AuthenticationIdentityKey] ([AuthenticationKey], [TimeCreated]) VALUES (N'F8BB1C27ABA1300F7C530437B52931E9', CAST(N'2015-01-10 17:12:15.943' AS DateTime))
INSERT [dbo].[AuthenticationIdentityKey_AuthenticationIdentity] ([AuthenticationIdentityId], [AuthenticationKey]) VALUES (12, N'AC6F6ADD9ACDA1B95B00113B7F001FFA')
INSERT [dbo].[AuthenticationIdentityKey_AuthenticationIdentity] ([AuthenticationIdentityId], [AuthenticationKey]) VALUES (13, N'58E1B71EECE98384E63A2F18FB0375EE')
INSERT [dbo].[AuthenticationIdentityKey_AuthenticationIdentity] ([AuthenticationIdentityId], [AuthenticationKey]) VALUES (14, N'B25918DE2415A72A7CAC5F54273FA380')
INSERT [dbo].[AuthenticationIdentityKey_AuthenticationIdentity] ([AuthenticationIdentityId], [AuthenticationKey]) VALUES (16, N'9ACE36DEFB058552C88B0F9D69DAEF99')
INSERT [dbo].[AuthenticationIdentityKey_AuthenticationIdentity] ([AuthenticationIdentityId], [AuthenticationKey]) VALUES (17, N'5E7FDE30677F8FFA5D096E9EA64603E7')
INSERT [dbo].[AuthenticationIdentityKey_AuthenticationIdentity] ([AuthenticationIdentityId], [AuthenticationKey]) VALUES (18, N'E8017CBFC4A5555231A9D497632603CC')
INSERT [dbo].[AuthenticationIdentityKey_AuthenticationIdentity] ([AuthenticationIdentityId], [AuthenticationKey]) VALUES (19, N'18116FAF38DBC57250743BB56E40166E')
INSERT [dbo].[AuthenticationIdentityKey_AuthenticationIdentity] ([AuthenticationIdentityId], [AuthenticationKey]) VALUES (20, N'1095776546DA522DEC08E929CA57EB65')
SET IDENTITY_INSERT [dbo].[AuthenticationLog] ON 

INSERT [dbo].[AuthenticationLog] ([Id], [TimeCreated], [AuthenticationId]) VALUES (2023, CAST(N'2015-01-07 19:36:32.173' AS DateTime), 16)
INSERT [dbo].[AuthenticationLog] ([Id], [TimeCreated], [AuthenticationId]) VALUES (2024, CAST(N'2015-01-10 16:34:32.830' AS DateTime), 17)
INSERT [dbo].[AuthenticationLog] ([Id], [TimeCreated], [AuthenticationId]) VALUES (2025, CAST(N'2015-01-10 16:37:17.970' AS DateTime), 17)
INSERT [dbo].[AuthenticationLog] ([Id], [TimeCreated], [AuthenticationId]) VALUES (2026, CAST(N'2015-01-10 17:11:39.497' AS DateTime), 18)
INSERT [dbo].[AuthenticationLog] ([Id], [TimeCreated], [AuthenticationId]) VALUES (2027, CAST(N'2015-01-10 18:17:17.670' AS DateTime), 19)
INSERT [dbo].[AuthenticationLog] ([Id], [TimeCreated], [AuthenticationId]) VALUES (2028, CAST(N'2015-01-10 18:18:21.257' AS DateTime), 20)
INSERT [dbo].[AuthenticationLog] ([Id], [TimeCreated], [AuthenticationId]) VALUES (2029, CAST(N'2015-01-10 20:10:47.957' AS DateTime), 17)
INSERT [dbo].[AuthenticationLog] ([Id], [TimeCreated], [AuthenticationId]) VALUES (2030, CAST(N'2015-01-10 20:10:48.233' AS DateTime), 17)
INSERT [dbo].[AuthenticationLog] ([Id], [TimeCreated], [AuthenticationId]) VALUES (2031, CAST(N'2015-01-11 15:17:45.053' AS DateTime), 17)
SET IDENTITY_INSERT [dbo].[AuthenticationLog] OFF
SET IDENTITY_INSERT [dbo].[AuthenticationPassword] ON 

INSERT [dbo].[AuthenticationPassword] ([Id], [AuthenticationIdentityId], [TimeCreated], [Password], [IsActive]) VALUES (4, 12, CAST(N'2015-01-04' AS Date), N'123123wqe', 1)
INSERT [dbo].[AuthenticationPassword] ([Id], [AuthenticationIdentityId], [TimeCreated], [Password], [IsActive]) VALUES (5, 13, CAST(N'2015-01-04' AS Date), N'123123qwe', 1)
INSERT [dbo].[AuthenticationPassword] ([Id], [AuthenticationIdentityId], [TimeCreated], [Password], [IsActive]) VALUES (6, 14, CAST(N'2015-01-07' AS Date), N'123123qwe', 1)
INSERT [dbo].[AuthenticationPassword] ([Id], [AuthenticationIdentityId], [TimeCreated], [Password], [IsActive]) VALUES (8, 16, CAST(N'2015-01-07' AS Date), N'123123qwe', 1)
INSERT [dbo].[AuthenticationPassword] ([Id], [AuthenticationIdentityId], [TimeCreated], [Password], [IsActive]) VALUES (9, 17, CAST(N'2015-01-10' AS Date), N'9A8C21C4C9948BF0CDAEC218703DB7C8', 1)
INSERT [dbo].[AuthenticationPassword] ([Id], [AuthenticationIdentityId], [TimeCreated], [Password], [IsActive]) VALUES (10, 18, CAST(N'2015-01-10' AS Date), N'9A8C21C4C9948BF0CDAEC218703DB7C8', 1)
INSERT [dbo].[AuthenticationPassword] ([Id], [AuthenticationIdentityId], [TimeCreated], [Password], [IsActive]) VALUES (11, 19, CAST(N'2015-01-10' AS Date), N'D84254E863FC2CA376BC558DC1FFDDCD', 1)
INSERT [dbo].[AuthenticationPassword] ([Id], [AuthenticationIdentityId], [TimeCreated], [Password], [IsActive]) VALUES (12, 20, CAST(N'2015-01-10' AS Date), N'869A60D98B9BAE4B4164846B588DE365', 1)
SET IDENTITY_INSERT [dbo].[AuthenticationPassword] OFF
SET IDENTITY_INSERT [dbo].[Contact] ON 

INSERT [dbo].[Contact] ([Id], [Firstname], [Lastname], [TimeCreated]) VALUES (2, N'Kim', N'Larsen', CAST(N'2014-12-03 14:25:21.453' AS DateTime))
INSERT [dbo].[Contact] ([Id], [Firstname], [Lastname], [TimeCreated]) VALUES (3, N'Malene', N'Danielsen', CAST(N'2014-12-03 15:10:41.050' AS DateTime))
INSERT [dbo].[Contact] ([Id], [Firstname], [Lastname], [TimeCreated]) VALUES (4, N'Mette', N'Lassen', CAST(N'2014-12-03 15:12:50.560' AS DateTime))
INSERT [dbo].[Contact] ([Id], [Firstname], [Lastname], [TimeCreated]) VALUES (5, N'James', N'Lassen', CAST(N'2014-12-03 15:14:41.153' AS DateTime))
INSERT [dbo].[Contact] ([Id], [Firstname], [Lastname], [TimeCreated]) VALUES (22, N'Lisette', N'Jørgensen', CAST(N'2014-12-05 16:29:47.180' AS DateTime))
INSERT [dbo].[Contact] ([Id], [Firstname], [Lastname], [TimeCreated]) VALUES (23, N'Ditte', N'Ruusunen', CAST(N'2014-12-05 16:30:26.280' AS DateTime))
INSERT [dbo].[Contact] ([Id], [Firstname], [Lastname], [TimeCreated]) VALUES (25, N'Rolf', N'Sørensen', CAST(N'2014-12-05 17:41:56.370' AS DateTime))
INSERT [dbo].[Contact] ([Id], [Firstname], [Lastname], [TimeCreated]) VALUES (1025, N'Søren', N'Sørensen', CAST(N'2014-12-14 14:56:05.957' AS DateTime))
INSERT [dbo].[Contact] ([Id], [Firstname], [Lastname], [TimeCreated]) VALUES (1026, N'Mikkel', N'Mikkelsen', CAST(N'2014-12-14 15:10:47.067' AS DateTime))
SET IDENTITY_INSERT [dbo].[Contact] OFF
INSERT [dbo].[Contact_ContactGroup] ([ContactId], [ContactGroupId]) VALUES (2, 1)
INSERT [dbo].[Contact_ContactGroup] ([ContactId], [ContactGroupId]) VALUES (4, 1)
INSERT [dbo].[Contact_ContactGroup] ([ContactId], [ContactGroupId]) VALUES (25, 1)
INSERT [dbo].[Contact_ContactGroup] ([ContactId], [ContactGroupId]) VALUES (1025, 1)
INSERT [dbo].[Contact_ContactGroup] ([ContactId], [ContactGroupId]) VALUES (1026, 1)
INSERT [dbo].[Contact_ContactGroup] ([ContactId], [ContactGroupId]) VALUES (2, 2)
INSERT [dbo].[Contact_ContactGroup] ([ContactId], [ContactGroupId]) VALUES (3, 3)
INSERT [dbo].[Contact_ContactGroup] ([ContactId], [ContactGroupId]) VALUES (4, 4)
INSERT [dbo].[Contact_ContactGroup] ([ContactId], [ContactGroupId]) VALUES (5, 4)
INSERT [dbo].[Contact_ContactGroup] ([ContactId], [ContactGroupId]) VALUES (22, 4)
INSERT [dbo].[Contact_ContactGroup] ([ContactId], [ContactGroupId]) VALUES (23, 4)
INSERT [dbo].[Contact_ContactGroup] ([ContactId], [ContactGroupId]) VALUES (25, 5)
INSERT [dbo].[Contact_ContactInformation] ([ContactId], [ContactInformationId]) VALUES (2, 11)
INSERT [dbo].[Contact_ContactInformation] ([ContactId], [ContactInformationId]) VALUES (2, 1030)
INSERT [dbo].[Contact_ContactInformation] ([ContactId], [ContactInformationId]) VALUES (23, 1031)
INSERT [dbo].[Contact_ContactInformation] ([ContactId], [ContactInformationId]) VALUES (23, 1032)
INSERT [dbo].[Contact_ContactInformation] ([ContactId], [ContactInformationId]) VALUES (23, 1033)
INSERT [dbo].[Contact_ContactInformation] ([ContactId], [ContactInformationId]) VALUES (3, 1034)
INSERT [dbo].[Contact_ContactInformation] ([ContactId], [ContactInformationId]) VALUES (1026, 1035)
INSERT [dbo].[Contact_ContactInformation] ([ContactId], [ContactInformationId]) VALUES (22, 1036)
INSERT [dbo].[Contact_ContactType] ([ContactId], [ContactTypeId]) VALUES (2, 1)
INSERT [dbo].[Contact_ContactType] ([ContactId], [ContactTypeId]) VALUES (3, 2)
INSERT [dbo].[Contact_ContactType] ([ContactId], [ContactTypeId]) VALUES (22, 4)
INSERT [dbo].[Contact_ContactType] ([ContactId], [ContactTypeId]) VALUES (23, 5)
INSERT [dbo].[Contact_Patient] ([ContactId], [PatientId]) VALUES (2, 1)
INSERT [dbo].[Contact_Patient] ([ContactId], [PatientId]) VALUES (3, 1)
INSERT [dbo].[Contact_Patient] ([ContactId], [PatientId]) VALUES (4, 1)
INSERT [dbo].[Contact_Patient] ([ContactId], [PatientId]) VALUES (5, 1)
INSERT [dbo].[Contact_Patient] ([ContactId], [PatientId]) VALUES (22, 1)
INSERT [dbo].[Contact_Patient] ([ContactId], [PatientId]) VALUES (23, 1)
INSERT [dbo].[Contact_Patient] ([ContactId], [PatientId]) VALUES (25, 1)
INSERT [dbo].[Contact_Patient] ([ContactId], [PatientId]) VALUES (1025, 3)
INSERT [dbo].[Contact_Patient] ([ContactId], [PatientId]) VALUES (1026, 3)
SET IDENTITY_INSERT [dbo].[ContactGroup] ON 

INSERT [dbo].[ContactGroup] ([Id], [Name]) VALUES (1007, N'd')
INSERT [dbo].[ContactGroup] ([Id], [Name]) VALUES (1008, N'dummy2')
INSERT [dbo].[ContactGroup] ([Id], [Name]) VALUES (1009, N'dummy3')
INSERT [dbo].[ContactGroup] ([Id], [Name]) VALUES (4, N'Familie')
INSERT [dbo].[ContactGroup] ([Id], [Name]) VALUES (1024, N'Klassekammerat')
INSERT [dbo].[ContactGroup] ([Id], [Name]) VALUES (3, N'Kommune')
INSERT [dbo].[ContactGroup] ([Id], [Name]) VALUES (5, N'Skole')
INSERT [dbo].[ContactGroup] ([Id], [Name]) VALUES (1022, N'Tester1')
INSERT [dbo].[ContactGroup] ([Id], [Name]) VALUES (1023, N'Tester2')
INSERT [dbo].[ContactGroup] ([Id], [Name]) VALUES (2, N'Udvikler')
INSERT [dbo].[ContactGroup] ([Id], [Name]) VALUES (1, N'Venner')
SET IDENTITY_INSERT [dbo].[ContactGroup] OFF
SET IDENTITY_INSERT [dbo].[ContactInformation] ON 

INSERT [dbo].[ContactInformation] ([Id], [ContactInformationTypeId], [Data], [TimeCreated], [IsActive]) VALUES (1, 1, N'+45 27150869', CAST(N'2014-11-18 20:51:16.167' AS DateTime), 1)
INSERT [dbo].[ContactInformation] ([Id], [ContactInformationTypeId], [Data], [TimeCreated], [IsActive]) VALUES (2, 2, N'privat.oliver.lassen@gmail.com', CAST(N'2014-11-18 20:51:16.643' AS DateTime), 1)
INSERT [dbo].[ContactInformation] ([Id], [ContactInformationTypeId], [Data], [TimeCreated], [IsActive]) VALUES (3, 3, N'http://google123.com', CAST(N'2014-11-18 20:51:16.647' AS DateTime), 0)
INSERT [dbo].[ContactInformation] ([Id], [ContactInformationTypeId], [Data], [TimeCreated], [IsActive]) VALUES (4, 3, N'https://www.facebook.com/leo.lassen', CAST(N'2014-11-18 20:55:22.667' AS DateTime), 1)
INSERT [dbo].[ContactInformation] ([Id], [ContactInformationTypeId], [Data], [TimeCreated], [IsActive]) VALUES (5, 2, N'+45 88888890', CAST(N'2014-11-18 22:12:14.647' AS DateTime), 0)
INSERT [dbo].[ContactInformation] ([Id], [ContactInformationTypeId], [Data], [TimeCreated], [IsActive]) VALUES (6, 2, N'+45 88888886', CAST(N'2014-11-18 22:12:46.500' AS DateTime), 0)
INSERT [dbo].[ContactInformation] ([Id], [ContactInformationTypeId], [Data], [TimeCreated], [IsActive]) VALUES (7, 4, N'+45 88888888', CAST(N'2014-11-18 22:13:08.503' AS DateTime), 0)
INSERT [dbo].[ContactInformation] ([Id], [ContactInformationTypeId], [Data], [TimeCreated], [IsActive]) VALUES (8, 3, N'http://linked.com', CAST(N'2014-11-18 22:13:40.263' AS DateTime), 0)
INSERT [dbo].[ContactInformation] ([Id], [ContactInformationTypeId], [Data], [TimeCreated], [IsActive]) VALUES (9, 1, N'+45 61310569', CAST(N'2014-11-20 14:27:00.763' AS DateTime), 0)
INSERT [dbo].[ContactInformation] ([Id], [ContactInformationTypeId], [Data], [TimeCreated], [IsActive]) VALUES (10, 3, N'http://soze.dk', CAST(N'2014-11-22 17:32:20.033' AS DateTime), 1)
INSERT [dbo].[ContactInformation] ([Id], [ContactInformationTypeId], [Data], [TimeCreated], [IsActive]) VALUES (11, 2, N'kim@soze.dk', CAST(N'2014-11-22 17:48:50.743' AS DateTime), 1)
INSERT [dbo].[ContactInformation] ([Id], [ContactInformationTypeId], [Data], [TimeCreated], [IsActive]) VALUES (12, 4, N'14813680', CAST(N'2014-11-24 20:58:13.113' AS DateTime), 1)
INSERT [dbo].[ContactInformation] ([Id], [ContactInformationTypeId], [Data], [TimeCreated], [IsActive]) VALUES (1010, 4, N'+48 88 88 88 88', CAST(N'2014-12-01 16:50:54.257' AS DateTime), 0)
INSERT [dbo].[ContactInformation] ([Id], [ContactInformationTypeId], [Data], [TimeCreated], [IsActive]) VALUES (1011, 4, N'+48 88 88 88 88', CAST(N'2014-12-01 16:50:54.850' AS DateTime), 0)
INSERT [dbo].[ContactInformation] ([Id], [ContactInformationTypeId], [Data], [TimeCreated], [IsActive]) VALUES (1012, 2, N'lis-jo@mail.dk', CAST(N'2014-12-01 17:06:41.730' AS DateTime), 1)
INSERT [dbo].[ContactInformation] ([Id], [ContactInformationTypeId], [Data], [TimeCreated], [IsActive]) VALUES (1013, 1, N'+45 22948148', CAST(N'2014-12-01 17:07:06.900' AS DateTime), 1)
INSERT [dbo].[ContactInformation] ([Id], [ContactInformationTypeId], [Data], [TimeCreated], [IsActive]) VALUES (1014, 1, N'+45 60648696', CAST(N'2014-12-01 17:08:45.150' AS DateTime), 1)
INSERT [dbo].[ContactInformation] ([Id], [ContactInformationTypeId], [Data], [TimeCreated], [IsActive]) VALUES (1030, 3, N'http://soze.dk', CAST(N'2014-12-14 14:51:33.380' AS DateTime), 1)
INSERT [dbo].[ContactInformation] ([Id], [ContactInformationTypeId], [Data], [TimeCreated], [IsActive]) VALUES (1031, 1, N'+45 60646932', CAST(N'2014-12-14 14:52:28.743' AS DateTime), 1)
INSERT [dbo].[ContactInformation] ([Id], [ContactInformationTypeId], [Data], [TimeCreated], [IsActive]) VALUES (1032, 2, N'ditte.ruusunen@gmail.com', CAST(N'2014-12-14 14:52:36.560' AS DateTime), 1)
INSERT [dbo].[ContactInformation] ([Id], [ContactInformationTypeId], [Data], [TimeCreated], [IsActive]) VALUES (1033, 2, N'ditte529@hotmail.com', CAST(N'2014-12-14 14:52:50.797' AS DateTime), 1)
INSERT [dbo].[ContactInformation] ([Id], [ContactInformationTypeId], [Data], [TimeCreated], [IsActive]) VALUES (1034, 2, N'malene.danielsen@egedal.dk', CAST(N'2014-12-14 14:53:26.120' AS DateTime), 1)
INSERT [dbo].[ContactInformation] ([Id], [ContactInformationTypeId], [Data], [TimeCreated], [IsActive]) VALUES (1035, 2, N'mikkel@mikkelsen.com', CAST(N'2014-12-14 15:10:57.617' AS DateTime), 1)
INSERT [dbo].[ContactInformation] ([Id], [ContactInformationTypeId], [Data], [TimeCreated], [IsActive]) VALUES (1036, 1, N'+45 22948148', CAST(N'2014-12-15 17:04:04.500' AS DateTime), 1)
INSERT [dbo].[ContactInformation] ([Id], [ContactInformationTypeId], [Data], [TimeCreated], [IsActive]) VALUES (1037, 1, N'+45 27150869', CAST(N'2015-01-07 19:37:43.763' AS DateTime), 1)
INSERT [dbo].[ContactInformation] ([Id], [ContactInformationTypeId], [Data], [TimeCreated], [IsActive]) VALUES (1038, 3, N'http://dagsrapporten.dk', CAST(N'2015-01-07 19:37:58.613' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[ContactInformation] OFF
SET IDENTITY_INSERT [dbo].[ContactInformationType] ON 

INSERT [dbo].[ContactInformationType] ([Id], [TypeName]) VALUES (2, N'E-mail')
INSERT [dbo].[ContactInformationType] ([Id], [TypeName]) VALUES (4, N'Hjemmenummer')
INSERT [dbo].[ContactInformationType] ([Id], [TypeName]) VALUES (3, N'Hjemmeside')
INSERT [dbo].[ContactInformationType] ([Id], [TypeName]) VALUES (1, N'Mobil')
SET IDENTITY_INSERT [dbo].[ContactInformationType] OFF
SET IDENTITY_INSERT [dbo].[ContactType] ON 

INSERT [dbo].[ContactType] ([Id], [Name]) VALUES (5, N'Kæreste')
INSERT [dbo].[ContactType] ([Id], [Name]) VALUES (6, N'Kammerat')
INSERT [dbo].[ContactType] ([Id], [Name]) VALUES (7, N'Kammerat123')
INSERT [dbo].[ContactType] ([Id], [Name]) VALUES (1, N'Mentor')
INSERT [dbo].[ContactType] ([Id], [Name]) VALUES (4, N'Mor')
INSERT [dbo].[ContactType] ([Id], [Name]) VALUES (2, N'Sagbehandler')
SET IDENTITY_INSERT [dbo].[ContactType] OFF
SET IDENTITY_INSERT [dbo].[Country] ON 

INSERT [dbo].[Country] ([Id], [Name]) VALUES (1, N'Danmark')
SET IDENTITY_INSERT [dbo].[Country] OFF
SET IDENTITY_INSERT [dbo].[Description] ON 

INSERT [dbo].[Description] ([Id], [Text], [TimeCreated], [EmployeeId]) VALUES (1, N'TEST', CAST(N'2014-08-28 22:01:15.250' AS DateTime), 1)
INSERT [dbo].[Description] ([Id], [Text], [TimeCreated], [EmployeeId]) VALUES (2, N'TEST', CAST(N'2014-08-28 22:01:36.700' AS DateTime), 1)
INSERT [dbo].[Description] ([Id], [Text], [TimeCreated], [EmployeeId]) VALUES (3, N'TEST', CAST(N'2014-08-28 22:01:37.533' AS DateTime), 1)
INSERT [dbo].[Description] ([Id], [Text], [TimeCreated], [EmployeeId]) VALUES (4, N'TEST', CAST(N'2014-08-28 22:01:38.060' AS DateTime), 1)
INSERT [dbo].[Description] ([Id], [Text], [TimeCreated], [EmployeeId]) VALUES (5, N'TEST', CAST(N'2014-08-28 22:01:38.363' AS DateTime), 1)
INSERT [dbo].[Description] ([Id], [Text], [TimeCreated], [EmployeeId]) VALUES (6, N'TEST', CAST(N'2014-08-28 22:01:38.687' AS DateTime), 1)
INSERT [dbo].[Description] ([Id], [Text], [TimeCreated], [EmployeeId]) VALUES (7, N'TEST', CAST(N'2014-08-28 22:01:38.990' AS DateTime), 1)
INSERT [dbo].[Description] ([Id], [Text], [TimeCreated], [EmployeeId]) VALUES (8, N'TEST', CAST(N'2014-08-28 22:01:39.297' AS DateTime), 1)
INSERT [dbo].[Description] ([Id], [Text], [TimeCreated], [EmployeeId]) VALUES (9, N'TEST', CAST(N'2014-08-28 22:01:39.700' AS DateTime), 1)
INSERT [dbo].[Description] ([Id], [Text], [TimeCreated], [EmployeeId]) VALUES (10, N'TEST', CAST(N'2014-08-28 22:01:40.003' AS DateTime), 1)
INSERT [dbo].[Description] ([Id], [Text], [TimeCreated], [EmployeeId]) VALUES (11, N'TEST', CAST(N'2014-08-28 22:01:40.457' AS DateTime), 1)
INSERT [dbo].[Description] ([Id], [Text], [TimeCreated], [EmployeeId]) VALUES (12, N'Ditte er en lækker og dejlig pige. Hun er kærester med @Oliver. Hun har et højt temperament, så man skal passe på hvad man siger til hende. Ellers er hun en bare en perfekt tøs! ;)', CAST(N'2014-08-28 22:30:24.307' AS DateTime), 1)
INSERT [dbo].[Description] ([Id], [Text], [TimeCreated], [EmployeeId]) VALUES (13, N'Ditte er en lækker og dejlig pige. Hun er kærester med @Oliver. Hun har et højt temperament, så man skal passe på hvad man siger til hende. Ellers er hun en bare en perfekt tøs! ;) 2123123', CAST(N'2014-08-31 18:12:37.483' AS DateTime), 7)
INSERT [dbo].[Description] ([Id], [Text], [TimeCreated], [EmployeeId]) VALUES (14, N'Okay, vi går nu', CAST(N'2014-08-31 18:13:10.353' AS DateTime), 7)
INSERT [dbo].[Description] ([Id], [Text], [TimeCreated], [EmployeeId]) VALUES (15, N'Oliver er en klog fyr, som bor i Husum sammen med sin kæreste @Ditte Ruusunen. Han er klarer sig udemærket! ', CAST(N'2014-08-31 19:38:56.187' AS DateTime), 7)
INSERT [dbo].[Description] ([Id], [Text], [TimeCreated], [EmployeeId]) VALUES (16, N'Ditte er en lækker og dejlig pige. Hun er kærester med @Oliver. Hun har et højt temperament, så man skal passe på hvad man siger til hende. Ellers er hun en bare en perfekt tøs! ;)', CAST(N'2014-08-31 19:52:02.177' AS DateTime), 7)
INSERT [dbo].[Description] ([Id], [Text], [TimeCreated], [EmployeeId]) VALUES (17, N'Kim er én rigtigt dygtig webudvikler. Han har lært mig en hel del..', CAST(N'2014-11-22 17:47:23.617' AS DateTime), 1)
INSERT [dbo].[Description] ([Id], [Text], [TimeCreated], [EmployeeId]) VALUES (1017, N'Det her er en beskrivelse', CAST(N'2015-01-10 20:11:19.253' AS DateTime), 23)
SET IDENTITY_INSERT [dbo].[Description] OFF
SET IDENTITY_INSERT [dbo].[Employee] ON 

INSERT [dbo].[Employee] ([Id], [OrganizationId], [Firstname], [Lastname], [TimeCreated], [IsActive], [Email]) VALUES (1, 1, N'Lisette', N'Jørgensen', CAST(N'2014-08-16 14:48:44.000' AS DateTime), 1, N'DummyEmail-1')
INSERT [dbo].[Employee] ([Id], [OrganizationId], [Firstname], [Lastname], [TimeCreated], [IsActive], [Email]) VALUES (2, 1, N'Helle', N'Sinding', CAST(N'2014-08-16 14:48:44.000' AS DateTime), 1, N'DummyEmail-2')
INSERT [dbo].[Employee] ([Id], [OrganizationId], [Firstname], [Lastname], [TimeCreated], [IsActive], [Email]) VALUES (3, 1, N'Kim', N'Larsen', CAST(N'2014-08-17 21:51:40.397' AS DateTime), 0, N'DummyEmail-3')
INSERT [dbo].[Employee] ([Id], [OrganizationId], [Firstname], [Lastname], [TimeCreated], [IsActive], [Email]) VALUES (4, 1, N'Jens', N'Jensen', CAST(N'2014-08-17 21:52:26.880' AS DateTime), 0, N'DummyEmail-4')
INSERT [dbo].[Employee] ([Id], [OrganizationId], [Firstname], [Lastname], [TimeCreated], [IsActive], [Email]) VALUES (5, 1, N'Simon', N'Spies Jørgensen', CAST(N'2014-08-18 21:23:34.910' AS DateTime), 0, N'DummyEmail-5')
INSERT [dbo].[Employee] ([Id], [OrganizationId], [Firstname], [Lastname], [TimeCreated], [IsActive], [Email]) VALUES (6, 2, N'Jan', N'Fyllgraf', CAST(N'2014-08-19 20:10:40.773' AS DateTime), 1, N'DummyEmail-6')
INSERT [dbo].[Employee] ([Id], [OrganizationId], [Firstname], [Lastname], [TimeCreated], [IsActive], [Email]) VALUES (7, 2, N'Gordo', N'Gusatovic', CAST(N'2014-08-19 20:12:28.177' AS DateTime), 1, N'DummyEmail-7')
INSERT [dbo].[Employee] ([Id], [OrganizationId], [Firstname], [Lastname], [TimeCreated], [IsActive], [Email]) VALUES (8, 3, N'René', N'Sørensen', CAST(N'2014-08-19 20:25:58.217' AS DateTime), 1, N'DummyEmail-8')
INSERT [dbo].[Employee] ([Id], [OrganizationId], [Firstname], [Lastname], [TimeCreated], [IsActive], [Email]) VALUES (9, 1, N'Oliver', N'Lassen', CAST(N'2015-01-04 17:11:11.930' AS DateTime), 1, N'privat.oliver.lassen@gmail.com')
INSERT [dbo].[Employee] ([Id], [OrganizationId], [Firstname], [Lastname], [TimeCreated], [IsActive], [Email]) VALUES (10, 1, N'Oliver', N'Lassen', CAST(N'2015-01-04 17:12:18.290' AS DateTime), 1, N'privat.oliver.lassen@gmail.com')
INSERT [dbo].[Employee] ([Id], [OrganizationId], [Firstname], [Lastname], [TimeCreated], [IsActive], [Email]) VALUES (11, 1, N'Oliver', N'Lassen', CAST(N'2015-01-04 17:19:04.017' AS DateTime), 1, N'privat.oliver.lassen+dr1@gmail.com')
INSERT [dbo].[Employee] ([Id], [OrganizationId], [Firstname], [Lastname], [TimeCreated], [IsActive], [Email]) VALUES (12, 1, N'Oliver', N'Lassen', CAST(N'2015-01-04 17:19:15.933' AS DateTime), 1, N'privat.oliver.lassen+dr1@gmail.com')
INSERT [dbo].[Employee] ([Id], [OrganizationId], [Firstname], [Lastname], [TimeCreated], [IsActive], [Email]) VALUES (13, 1, N'Oliver', N'Lassen', CAST(N'2015-01-04 17:20:15.713' AS DateTime), 1, N'privat.oliver.lassen+dr2@gmail.com')
INSERT [dbo].[Employee] ([Id], [OrganizationId], [Firstname], [Lastname], [TimeCreated], [IsActive], [Email]) VALUES (17, 1, N'Kim', N'Larsen', CAST(N'2015-01-04 17:58:01.610' AS DateTime), 1, N'privat.oliver.lassen+dr4@gmail.com')
INSERT [dbo].[Employee] ([Id], [OrganizationId], [Firstname], [Lastname], [TimeCreated], [IsActive], [Email]) VALUES (18, 1, N'Mads', N'Mikkelsen', CAST(N'2015-01-04 18:36:35.230' AS DateTime), 1, N'privat.oliver.lassen+dr5@gmail.com')
INSERT [dbo].[Employee] ([Id], [OrganizationId], [Firstname], [Lastname], [TimeCreated], [IsActive], [Email]) VALUES (19, 1, N'Simon', N'Simonsen', CAST(N'2015-01-04 18:39:48.807' AS DateTime), 1, N'privat.oliver.lassen+dr6@gmail.com')
INSERT [dbo].[Employee] ([Id], [OrganizationId], [Firstname], [Lastname], [TimeCreated], [IsActive], [Email]) VALUES (20, 1, N'Ditte', N'Ruusunen', CAST(N'2015-01-04 18:41:56.117' AS DateTime), 1, N'privat.oliver.lassen+dr8@gmail.com')
INSERT [dbo].[Employee] ([Id], [OrganizationId], [Firstname], [Lastname], [TimeCreated], [IsActive], [Email]) VALUES (21, 8, N'Kim', N'Larsen', CAST(N'2015-01-07 19:16:10.520' AS DateTime), 1, N'privat.oliver.lassen+dr6@gmail.com')
INSERT [dbo].[Employee] ([Id], [OrganizationId], [Firstname], [Lastname], [TimeCreated], [IsActive], [Email]) VALUES (22, 9, N'Oliver', N'Lassen', CAST(N'2015-01-07 19:35:11.573' AS DateTime), 1, N'privat.oliver.lassen+dr7@gmail.com')
INSERT [dbo].[Employee] ([Id], [OrganizationId], [Firstname], [Lastname], [TimeCreated], [IsActive], [Email]) VALUES (23, 10, N'Oliver', N'Lassen', CAST(N'2015-01-10 16:33:45.977' AS DateTime), 1, N'privat.oliver.lassen+dr9@gmail.com')
INSERT [dbo].[Employee] ([Id], [OrganizationId], [Firstname], [Lastname], [TimeCreated], [IsActive], [Email]) VALUES (24, 10, N'Ditte', N'Ruusunen', CAST(N'2015-01-10 17:10:31.363' AS DateTime), 1, N'privat.oliver.lassen+dr10@gmail.com')
INSERT [dbo].[Employee] ([Id], [OrganizationId], [Firstname], [Lastname], [TimeCreated], [IsActive], [Email]) VALUES (25, 10, N'Sarah', N'Lassen', CAST(N'2015-01-10 17:12:15.943' AS DateTime), 1, N'privat.oliver.lassen+dr11@gmail.com')
INSERT [dbo].[Employee] ([Id], [OrganizationId], [Firstname], [Lastname], [TimeCreated], [IsActive], [Email]) VALUES (26, 10, N'Mikkel', N'Sørensen', CAST(N'2015-01-10 18:01:45.217' AS DateTime), 1, N'privat.oliver.lassen+dr11@gmail.com')
INSERT [dbo].[Employee] ([Id], [OrganizationId], [Firstname], [Lastname], [TimeCreated], [IsActive], [Email]) VALUES (27, 11, N'Ditte', N'Ruusunen', CAST(N'2015-01-10 18:16:33.077' AS DateTime), 1, N'privat.oliver.lassen+dr12@gmail.com')
INSERT [dbo].[Employee] ([Id], [OrganizationId], [Firstname], [Lastname], [TimeCreated], [IsActive], [Email]) VALUES (28, 11, N'Oliver', N'Lassen', CAST(N'2015-01-10 18:17:36.427' AS DateTime), 1, N'privat.oliver.lassen+dr13@gmail.com')
SET IDENTITY_INSERT [dbo].[Employee] OFF
INSERT [dbo].[Employee_ContactInformation] ([ContactInformationId], [EmployeeId]) VALUES (3, 1)
INSERT [dbo].[Employee_ContactInformation] ([ContactInformationId], [EmployeeId]) VALUES (1012, 1)
INSERT [dbo].[Employee_ContactInformation] ([ContactInformationId], [EmployeeId]) VALUES (1013, 1)
INSERT [dbo].[Employee_ContactInformation] ([ContactInformationId], [EmployeeId]) VALUES (1014, 2)
INSERT [dbo].[Employee_ContactInformation] ([ContactInformationId], [EmployeeId]) VALUES (1037, 22)
INSERT [dbo].[Employee_ContactInformation] ([ContactInformationId], [EmployeeId]) VALUES (1038, 22)
INSERT [dbo].[EmployeeAuthenticationIdentityKey] ([EmployeeId], [AuthenticationKey], [TimeCreated]) VALUES (17, N'AC6F6ADD9ACDA1B95B00113B7F001FFA', CAST(N'2015-01-04 17:58:01.610' AS DateTime))
INSERT [dbo].[EmployeeAuthenticationIdentityKey] ([EmployeeId], [AuthenticationKey], [TimeCreated]) VALUES (18, N'80B697A9A72498EB702F8468CAAB4A9B', CAST(N'2015-01-04 18:36:35.230' AS DateTime))
INSERT [dbo].[EmployeeAuthenticationIdentityKey] ([EmployeeId], [AuthenticationKey], [TimeCreated]) VALUES (19, N'58E1B71EECE98384E63A2F18FB0375EE', CAST(N'2015-01-04 18:39:48.807' AS DateTime))
INSERT [dbo].[EmployeeAuthenticationIdentityKey] ([EmployeeId], [AuthenticationKey], [TimeCreated]) VALUES (20, N'3B1824CFB1C295DABF86874F5EAD9AAD', CAST(N'2015-01-04 18:41:56.117' AS DateTime))
INSERT [dbo].[EmployeeAuthenticationIdentityKey] ([EmployeeId], [AuthenticationKey], [TimeCreated]) VALUES (21, N'B25918DE2415A72A7CAC5F54273FA380', CAST(N'2015-01-07 19:16:10.520' AS DateTime))
INSERT [dbo].[EmployeeAuthenticationIdentityKey] ([EmployeeId], [AuthenticationKey], [TimeCreated]) VALUES (22, N'9ACE36DEFB058552C88B0F9D69DAEF99', CAST(N'2015-01-07 19:35:11.573' AS DateTime))
INSERT [dbo].[EmployeeAuthenticationIdentityKey] ([EmployeeId], [AuthenticationKey], [TimeCreated]) VALUES (23, N'5E7FDE30677F8FFA5D096E9EA64603E7', CAST(N'2015-01-10 16:33:45.977' AS DateTime))
INSERT [dbo].[EmployeeAuthenticationIdentityKey] ([EmployeeId], [AuthenticationKey], [TimeCreated]) VALUES (24, N'E8017CBFC4A5555231A9D497632603CC', CAST(N'2015-01-10 17:10:31.363' AS DateTime))
INSERT [dbo].[EmployeeAuthenticationIdentityKey] ([EmployeeId], [AuthenticationKey], [TimeCreated]) VALUES (25, N'F8BB1C27ABA1300F7C530437B52931E9', CAST(N'2015-01-10 17:12:15.943' AS DateTime))
INSERT [dbo].[EmployeeAuthenticationIdentityKey] ([EmployeeId], [AuthenticationKey], [TimeCreated]) VALUES (26, N'65D54D4CAA92F48B7E044D20E424B4B8', CAST(N'2015-01-10 18:01:45.217' AS DateTime))
INSERT [dbo].[EmployeeAuthenticationIdentityKey] ([EmployeeId], [AuthenticationKey], [TimeCreated]) VALUES (27, N'18116FAF38DBC57250743BB56E40166E', CAST(N'2015-01-10 18:16:33.077' AS DateTime))
INSERT [dbo].[EmployeeAuthenticationIdentityKey] ([EmployeeId], [AuthenticationKey], [TimeCreated]) VALUES (28, N'1095776546DA522DEC08E929CA57EB65', CAST(N'2015-01-10 18:17:36.427' AS DateTime))
SET IDENTITY_INSERT [dbo].[Measure] ON 

INSERT [dbo].[Measure] ([Id], [Name]) VALUES (1, N'ml')
INSERT [dbo].[Measure] ([Id], [Name]) VALUES (2, N'mg')
INSERT [dbo].[Measure] ([Id], [Name]) VALUES (3, N'g')
SET IDENTITY_INSERT [dbo].[Measure] OFF
SET IDENTITY_INSERT [dbo].[Medication] ON 

INSERT [dbo].[Medication] ([Id], [Name], [MedicationBrandId], [MedicationTypeId], [MeasureId], [Weight]) VALUES (1, N'Baldrian', 5, 1, 1, CAST(5.00 AS Decimal(16, 2)))
INSERT [dbo].[Medication] ([Id], [Name], [MedicationBrandId], [MedicationTypeId], [MeasureId], [Weight]) VALUES (2, N'afasdf', 2, 1, 1, CAST(5.00 AS Decimal(16, 2)))
INSERT [dbo].[Medication] ([Id], [Name], [MedicationBrandId], [MedicationTypeId], [MeasureId], [Weight]) VALUES (4, N'afasdfasdf', 2, 1, 1, CAST(5.00 AS Decimal(16, 2)))
INSERT [dbo].[Medication] ([Id], [Name], [MedicationBrandId], [MedicationTypeId], [MeasureId], [Weight]) VALUES (5, N'ASDASD', 6, 1, 1, CAST(5.00 AS Decimal(16, 2)))
INSERT [dbo].[Medication] ([Id], [Name], [MedicationBrandId], [MedicationTypeId], [MeasureId], [Weight]) VALUES (6, N'fghgfh', 1, 1, 1, CAST(5.00 AS Decimal(16, 2)))
INSERT [dbo].[Medication] ([Id], [Name], [MedicationBrandId], [MedicationTypeId], [MeasureId], [Weight]) VALUES (7, N'Methotrexat', 8, 1, 2, CAST(2.50 AS Decimal(16, 2)))
INSERT [dbo].[Medication] ([Id], [Name], [MedicationBrandId], [MedicationTypeId], [MeasureId], [Weight]) VALUES (9, N'sdsd', 6, 3, 1, CAST(0.00 AS Decimal(16, 2)))
INSERT [dbo].[Medication] ([Id], [Name], [MedicationBrandId], [MedicationTypeId], [MeasureId], [Weight]) VALUES (10, N'asd', 6, 2, 1, CAST(3.50 AS Decimal(16, 2)))
INSERT [dbo].[Medication] ([Id], [Name], [MedicationBrandId], [MedicationTypeId], [MeasureId], [Weight]) VALUES (11, N'Rwar', 6, 3, 1, CAST(43.00 AS Decimal(16, 2)))
INSERT [dbo].[Medication] ([Id], [Name], [MedicationBrandId], [MedicationTypeId], [MeasureId], [Weight]) VALUES (12, N'gfhgfhhk', 5, 1, 1, CAST(6.00 AS Decimal(16, 2)))
INSERT [dbo].[Medication] ([Id], [Name], [MedicationBrandId], [MedicationTypeId], [MeasureId], [Weight]) VALUES (13, N'Units', 9, 1, 1, CAST(6.00 AS Decimal(16, 2)))
SET IDENTITY_INSERT [dbo].[Medication] OFF
SET IDENTITY_INSERT [dbo].[MedicationBrand] ON 

INSERT [dbo].[MedicationBrand] ([Id], [Name]) VALUES (6, N'Bla bla')
INSERT [dbo].[MedicationBrand] ([Id], [Name]) VALUES (2, N'LEO Pharma')
INSERT [dbo].[MedicationBrand] ([Id], [Name]) VALUES (1, N'Novo Nordisk')
INSERT [dbo].[MedicationBrand] ([Id], [Name]) VALUES (9, N'Osherove')
INSERT [dbo].[MedicationBrand] ([Id], [Name]) VALUES (5, N'Randers')
INSERT [dbo].[MedicationBrand] ([Id], [Name]) VALUES (8, N'Sandoz')
SET IDENTITY_INSERT [dbo].[MedicationBrand] OFF
SET IDENTITY_INSERT [dbo].[MedicationSchedule] ON 

INSERT [dbo].[MedicationSchedule] ([Id], [PatientId], [TimeCreated], [StartDate]) VALUES (1, 1, CAST(N'2014-09-03 20:40:01.330' AS DateTime), CAST(N'2014-09-03' AS Date))
INSERT [dbo].[MedicationSchedule] ([Id], [PatientId], [TimeCreated], [StartDate]) VALUES (2, 1, CAST(N'2014-09-03 20:40:36.357' AS DateTime), CAST(N'2014-09-03' AS Date))
INSERT [dbo].[MedicationSchedule] ([Id], [PatientId], [TimeCreated], [StartDate]) VALUES (3, 1, CAST(N'2014-09-03 20:41:05.963' AS DateTime), CAST(N'2014-09-03' AS Date))
INSERT [dbo].[MedicationSchedule] ([Id], [PatientId], [TimeCreated], [StartDate]) VALUES (4, 2, CAST(N'2014-09-10 19:51:05.813' AS DateTime), CAST(N'2014-09-10' AS Date))
INSERT [dbo].[MedicationSchedule] ([Id], [PatientId], [TimeCreated], [StartDate]) VALUES (8, 2, CAST(N'2014-09-24 21:04:23.737' AS DateTime), CAST(N'2014-09-24' AS Date))
INSERT [dbo].[MedicationSchedule] ([Id], [PatientId], [TimeCreated], [StartDate]) VALUES (9, 3, CAST(N'2014-09-24 21:05:31.753' AS DateTime), CAST(N'2014-09-24' AS Date))
INSERT [dbo].[MedicationSchedule] ([Id], [PatientId], [TimeCreated], [StartDate]) VALUES (10, 1, CAST(N'2014-09-24 21:23:18.860' AS DateTime), CAST(N'2014-09-24' AS Date))
INSERT [dbo].[MedicationSchedule] ([Id], [PatientId], [TimeCreated], [StartDate]) VALUES (11, 4, CAST(N'2014-09-26 11:27:17.667' AS DateTime), CAST(N'2014-09-26' AS Date))
INSERT [dbo].[MedicationSchedule] ([Id], [PatientId], [TimeCreated], [StartDate]) VALUES (12, 2, CAST(N'2014-09-28 11:51:28.987' AS DateTime), CAST(N'2014-09-28' AS Date))
INSERT [dbo].[MedicationSchedule] ([Id], [PatientId], [TimeCreated], [StartDate]) VALUES (13, 2, CAST(N'2014-09-28 16:06:58.177' AS DateTime), CAST(N'2014-09-26' AS Date))
INSERT [dbo].[MedicationSchedule] ([Id], [PatientId], [TimeCreated], [StartDate]) VALUES (14, 2, CAST(N'2014-09-28 16:07:29.563' AS DateTime), CAST(N'2014-09-26' AS Date))
INSERT [dbo].[MedicationSchedule] ([Id], [PatientId], [TimeCreated], [StartDate]) VALUES (15, 1, CAST(N'2014-09-28 18:02:42.250' AS DateTime), CAST(N'2014-09-28' AS Date))
INSERT [dbo].[MedicationSchedule] ([Id], [PatientId], [TimeCreated], [StartDate]) VALUES (16, 4, CAST(N'2014-10-05 17:39:52.020' AS DateTime), CAST(N'2014-10-05' AS Date))
INSERT [dbo].[MedicationSchedule] ([Id], [PatientId], [TimeCreated], [StartDate]) VALUES (17, 3, CAST(N'2014-10-05 17:42:31.490' AS DateTime), CAST(N'2014-10-04' AS Date))
INSERT [dbo].[MedicationSchedule] ([Id], [PatientId], [TimeCreated], [StartDate]) VALUES (18, 1, CAST(N'2014-10-05 17:48:35.843' AS DateTime), CAST(N'2014-10-04' AS Date))
INSERT [dbo].[MedicationSchedule] ([Id], [PatientId], [TimeCreated], [StartDate]) VALUES (19, 1, CAST(N'2014-10-05 17:56:41.917' AS DateTime), CAST(N'2014-10-05' AS Date))
INSERT [dbo].[MedicationSchedule] ([Id], [PatientId], [TimeCreated], [StartDate]) VALUES (20, 4, CAST(N'2014-10-05 17:57:07.023' AS DateTime), CAST(N'2014-10-05' AS Date))
INSERT [dbo].[MedicationSchedule] ([Id], [PatientId], [TimeCreated], [StartDate]) VALUES (21, 3, CAST(N'2014-10-05 17:57:28.733' AS DateTime), CAST(N'2014-10-05' AS Date))
INSERT [dbo].[MedicationSchedule] ([Id], [PatientId], [TimeCreated], [StartDate]) VALUES (1020, 1, CAST(N'2014-10-11 18:10:36.090' AS DateTime), CAST(N'2014-10-11' AS Date))
SET IDENTITY_INSERT [dbo].[MedicationSchedule] OFF
INSERT [dbo].[MedicationScheduleComplete] ([MedicationScheduleId], [TimeCreated], [Description], [EmployeeId]) VALUES (3, CAST(N'2014-09-28 13:53:58.997' AS DateTime), N'Efter aftale med kommunen har vi besluttet at stoppe medicineringen', 1)
INSERT [dbo].[MedicationScheduleComplete] ([MedicationScheduleId], [TimeCreated], [Description], [EmployeeId]) VALUES (4, CAST(N'2014-09-28 14:59:30.123' AS DateTime), N'Hun gider ikke at tage medicinen længere, da hun ikke føler at det hjælper.', 1)
INSERT [dbo].[MedicationScheduleComplete] ([MedicationScheduleId], [TimeCreated], [Description], [EmployeeId]) VALUES (8, CAST(N'2014-09-28 16:02:51.513' AS DateTime), N'Ikke angivet', 1)
INSERT [dbo].[MedicationScheduleComplete] ([MedicationScheduleId], [TimeCreated], [Description], [EmployeeId]) VALUES (9, CAST(N'2014-09-28 15:39:32.450' AS DateTime), N'Ikke angivet...', 1)
INSERT [dbo].[MedicationScheduleComplete] ([MedicationScheduleId], [TimeCreated], [Description], [EmployeeId]) VALUES (10, CAST(N'2014-09-28 15:05:53.230' AS DateTime), N'Efter personalemøde har vi aftalt ikke længere at give Oliver medicin', 1)
INSERT [dbo].[MedicationScheduleComplete] ([MedicationScheduleId], [TimeCreated], [Description], [EmployeeId]) VALUES (11, CAST(N'2014-09-28 15:39:51.937' AS DateTime), N'Hun har ikke længere brug for baldrian da hun sover fint', 1)
INSERT [dbo].[MedicationScheduleComplete] ([MedicationScheduleId], [TimeCreated], [Description], [EmployeeId]) VALUES (12, CAST(N'2014-09-28 16:02:35.237' AS DateTime), N'Ikke angivet..', 1)
INSERT [dbo].[MedicationScheduleComplete] ([MedicationScheduleId], [TimeCreated], [Description], [EmployeeId]) VALUES (13, CAST(N'2014-09-28 16:07:12.847' AS DateTime), N'Forkert oprettet', 1)
INSERT [dbo].[MedicationScheduleComplete] ([MedicationScheduleId], [TimeCreated], [Description], [EmployeeId]) VALUES (16, CAST(N'2014-10-05 17:42:06.637' AS DateTime), N'Det var bare en test', 2)
INSERT [dbo].[MedicationScheduleComplete] ([MedicationScheduleId], [TimeCreated], [Description], [EmployeeId]) VALUES (17, CAST(N'2014-10-05 17:48:07.620' AS DateTime), N'Det var bare en test', 2)
SET IDENTITY_INSERT [dbo].[MedicationScheduleItem] ON 

INSERT [dbo].[MedicationScheduleItem] ([Id], [MedicationScheduleId], [MedicationId], [ExecuteTime], [Amouth]) VALUES (3, 3, 1, CAST(N'20:41:05.9700000' AS Time), CAST(1.00 AS Decimal(16, 2)))
INSERT [dbo].[MedicationScheduleItem] ([Id], [MedicationScheduleId], [MedicationId], [ExecuteTime], [Amouth]) VALUES (4, 3, 2, CAST(N'20:44:48.1400000' AS Time), CAST(1.00 AS Decimal(16, 2)))
INSERT [dbo].[MedicationScheduleItem] ([Id], [MedicationScheduleId], [MedicationId], [ExecuteTime], [Amouth]) VALUES (9, 3, 4, CAST(N'20:08:37.0200000' AS Time), CAST(1.00 AS Decimal(16, 2)))
INSERT [dbo].[MedicationScheduleItem] ([Id], [MedicationScheduleId], [MedicationId], [ExecuteTime], [Amouth]) VALUES (10, 4, 7, CAST(N'23:44:48' AS Time), CAST(6.00 AS Decimal(16, 2)))
INSERT [dbo].[MedicationScheduleItem] ([Id], [MedicationScheduleId], [MedicationId], [ExecuteTime], [Amouth]) VALUES (13, 8, 7, CAST(N'07:00:00' AS Time), CAST(1.00 AS Decimal(16, 2)))
INSERT [dbo].[MedicationScheduleItem] ([Id], [MedicationScheduleId], [MedicationId], [ExecuteTime], [Amouth]) VALUES (14, 9, 1, CAST(N'22:00:00' AS Time), CAST(2.00 AS Decimal(16, 2)))
INSERT [dbo].[MedicationScheduleItem] ([Id], [MedicationScheduleId], [MedicationId], [ExecuteTime], [Amouth]) VALUES (15, 10, 1, CAST(N'07:00:00' AS Time), CAST(2.00 AS Decimal(16, 2)))
INSERT [dbo].[MedicationScheduleItem] ([Id], [MedicationScheduleId], [MedicationId], [ExecuteTime], [Amouth]) VALUES (16, 10, 1, CAST(N'15:30:00' AS Time), CAST(2.00 AS Decimal(16, 2)))
INSERT [dbo].[MedicationScheduleItem] ([Id], [MedicationScheduleId], [MedicationId], [ExecuteTime], [Amouth]) VALUES (17, 10, 1, CAST(N'22:00:00' AS Time), CAST(2.00 AS Decimal(16, 2)))
INSERT [dbo].[MedicationScheduleItem] ([Id], [MedicationScheduleId], [MedicationId], [ExecuteTime], [Amouth]) VALUES (18, 10, 7, CAST(N'19:00:00' AS Time), CAST(1.00 AS Decimal(16, 2)))
INSERT [dbo].[MedicationScheduleItem] ([Id], [MedicationScheduleId], [MedicationId], [ExecuteTime], [Amouth]) VALUES (19, 11, 1, CAST(N'10:00:00' AS Time), CAST(1.00 AS Decimal(16, 2)))
INSERT [dbo].[MedicationScheduleItem] ([Id], [MedicationScheduleId], [MedicationId], [ExecuteTime], [Amouth]) VALUES (20, 11, 1, CAST(N'11:00:00' AS Time), CAST(2.00 AS Decimal(16, 2)))
INSERT [dbo].[MedicationScheduleItem] ([Id], [MedicationScheduleId], [MedicationId], [ExecuteTime], [Amouth]) VALUES (21, 11, 1, CAST(N'12:00:00' AS Time), CAST(3.00 AS Decimal(16, 2)))
INSERT [dbo].[MedicationScheduleItem] ([Id], [MedicationScheduleId], [MedicationId], [ExecuteTime], [Amouth]) VALUES (22, 11, 1, CAST(N'20:00:00' AS Time), CAST(4.00 AS Decimal(16, 2)))
INSERT [dbo].[MedicationScheduleItem] ([Id], [MedicationScheduleId], [MedicationId], [ExecuteTime], [Amouth]) VALUES (23, 12, 7, CAST(N'19:00:00' AS Time), CAST(6.00 AS Decimal(16, 2)))
INSERT [dbo].[MedicationScheduleItem] ([Id], [MedicationScheduleId], [MedicationId], [ExecuteTime], [Amouth]) VALUES (24, 12, 1, CAST(N'10:00:00' AS Time), CAST(1.00 AS Decimal(16, 2)))
INSERT [dbo].[MedicationScheduleItem] ([Id], [MedicationScheduleId], [MedicationId], [ExecuteTime], [Amouth]) VALUES (25, 12, 1, CAST(N'12:00:00' AS Time), CAST(1.00 AS Decimal(16, 2)))
INSERT [dbo].[MedicationScheduleItem] ([Id], [MedicationScheduleId], [MedicationId], [ExecuteTime], [Amouth]) VALUES (26, 12, 1, CAST(N'14:00:00' AS Time), CAST(1.00 AS Decimal(16, 2)))
INSERT [dbo].[MedicationScheduleItem] ([Id], [MedicationScheduleId], [MedicationId], [ExecuteTime], [Amouth]) VALUES (27, 13, 7, CAST(N'19:00:00' AS Time), CAST(6.00 AS Decimal(16, 2)))
INSERT [dbo].[MedicationScheduleItem] ([Id], [MedicationScheduleId], [MedicationId], [ExecuteTime], [Amouth]) VALUES (28, 14, 7, CAST(N'19:00:00' AS Time), CAST(6.00 AS Decimal(16, 2)))
INSERT [dbo].[MedicationScheduleItem] ([Id], [MedicationScheduleId], [MedicationId], [ExecuteTime], [Amouth]) VALUES (29, 15, 1, CAST(N'18:30:00' AS Time), CAST(2.00 AS Decimal(16, 2)))
INSERT [dbo].[MedicationScheduleItem] ([Id], [MedicationScheduleId], [MedicationId], [ExecuteTime], [Amouth]) VALUES (30, 16, 7, CAST(N'18:00:00' AS Time), CAST(2.00 AS Decimal(16, 2)))
INSERT [dbo].[MedicationScheduleItem] ([Id], [MedicationScheduleId], [MedicationId], [ExecuteTime], [Amouth]) VALUES (31, 17, 1, CAST(N'18:00:00' AS Time), CAST(1.00 AS Decimal(16, 2)))
INSERT [dbo].[MedicationScheduleItem] ([Id], [MedicationScheduleId], [MedicationId], [ExecuteTime], [Amouth]) VALUES (32, 18, 1, CAST(N'18:15:00' AS Time), CAST(2.00 AS Decimal(16, 2)))
INSERT [dbo].[MedicationScheduleItem] ([Id], [MedicationScheduleId], [MedicationId], [ExecuteTime], [Amouth]) VALUES (33, 19, 7, CAST(N'19:00:00' AS Time), CAST(1.00 AS Decimal(16, 2)))
INSERT [dbo].[MedicationScheduleItem] ([Id], [MedicationScheduleId], [MedicationId], [ExecuteTime], [Amouth]) VALUES (34, 20, 1, CAST(N'21:00:00' AS Time), CAST(2.00 AS Decimal(16, 2)))
INSERT [dbo].[MedicationScheduleItem] ([Id], [MedicationScheduleId], [MedicationId], [ExecuteTime], [Amouth]) VALUES (35, 21, 1, CAST(N'20:30:00' AS Time), CAST(2.00 AS Decimal(16, 2)))
INSERT [dbo].[MedicationScheduleItem] ([Id], [MedicationScheduleId], [MedicationId], [ExecuteTime], [Amouth]) VALUES (1034, 1020, 13, CAST(N'14:00:00' AS Time), CAST(1.00 AS Decimal(16, 2)))
SET IDENTITY_INSERT [dbo].[MedicationScheduleItem] OFF
SET IDENTITY_INSERT [dbo].[MedicationScheduleItemHistory] ON 

INSERT [dbo].[MedicationScheduleItemHistory] ([Id], [MedicationScheduleItemId], [TimeCreated]) VALUES (5, 3, CAST(N'2014-09-12 13:07:01.883' AS DateTime))
INSERT [dbo].[MedicationScheduleItemHistory] ([Id], [MedicationScheduleItemId], [TimeCreated]) VALUES (6, 3, CAST(N'2014-09-12 13:08:32.080' AS DateTime))
INSERT [dbo].[MedicationScheduleItemHistory] ([Id], [MedicationScheduleItemId], [TimeCreated]) VALUES (7, 28, CAST(N'2014-09-28 17:27:09.543' AS DateTime))
INSERT [dbo].[MedicationScheduleItemHistory] ([Id], [MedicationScheduleItemId], [TimeCreated]) VALUES (8, 28, CAST(N'2014-09-28 18:00:58.227' AS DateTime))
INSERT [dbo].[MedicationScheduleItemHistory] ([Id], [MedicationScheduleItemId], [TimeCreated]) VALUES (9, 28, CAST(N'2014-09-28 18:06:40.633' AS DateTime))
INSERT [dbo].[MedicationScheduleItemHistory] ([Id], [MedicationScheduleItemId], [TimeCreated]) VALUES (10, 28, CAST(N'2014-09-28 18:10:31.293' AS DateTime))
INSERT [dbo].[MedicationScheduleItemHistory] ([Id], [MedicationScheduleItemId], [TimeCreated]) VALUES (11, 29, CAST(N'2014-09-28 18:33:46.537' AS DateTime))
INSERT [dbo].[MedicationScheduleItemHistory] ([Id], [MedicationScheduleItemId], [TimeCreated]) VALUES (12, 28, CAST(N'2014-10-05 17:08:06.660' AS DateTime))
INSERT [dbo].[MedicationScheduleItemHistory] ([Id], [MedicationScheduleItemId], [TimeCreated]) VALUES (13, 30, CAST(N'2014-10-05 17:39:59.893' AS DateTime))
INSERT [dbo].[MedicationScheduleItemHistory] ([Id], [MedicationScheduleItemId], [TimeCreated]) VALUES (14, 29, CAST(N'2014-10-05 17:41:37.367' AS DateTime))
INSERT [dbo].[MedicationScheduleItemHistory] ([Id], [MedicationScheduleItemId], [TimeCreated]) VALUES (15, 31, CAST(N'2014-10-05 17:42:45.360' AS DateTime))
INSERT [dbo].[MedicationScheduleItemHistory] ([Id], [MedicationScheduleItemId], [TimeCreated]) VALUES (16, 31, CAST(N'2014-10-05 17:45:52.910' AS DateTime))
INSERT [dbo].[MedicationScheduleItemHistory] ([Id], [MedicationScheduleItemId], [TimeCreated]) VALUES (17, 31, CAST(N'2014-10-05 17:46:25.507' AS DateTime))
INSERT [dbo].[MedicationScheduleItemHistory] ([Id], [MedicationScheduleItemId], [TimeCreated]) VALUES (18, 31, CAST(N'2014-10-05 17:46:34.150' AS DateTime))
INSERT [dbo].[MedicationScheduleItemHistory] ([Id], [MedicationScheduleItemId], [TimeCreated]) VALUES (19, 32, CAST(N'2014-10-05 17:49:02.460' AS DateTime))
INSERT [dbo].[MedicationScheduleItemHistory] ([Id], [MedicationScheduleItemId], [TimeCreated]) VALUES (20, 32, CAST(N'2014-10-05 17:49:09.447' AS DateTime))
INSERT [dbo].[MedicationScheduleItemHistory] ([Id], [MedicationScheduleItemId], [TimeCreated]) VALUES (21, 32, CAST(N'2014-10-05 18:24:54.373' AS DateTime))
INSERT [dbo].[MedicationScheduleItemHistory] ([Id], [MedicationScheduleItemId], [TimeCreated]) VALUES (22, 33, CAST(N'2014-10-05 18:25:03.993' AS DateTime))
INSERT [dbo].[MedicationScheduleItemHistory] ([Id], [MedicationScheduleItemId], [TimeCreated]) VALUES (1021, 29, CAST(N'2014-12-01 14:54:21.680' AS DateTime))
SET IDENTITY_INSERT [dbo].[MedicationScheduleItemHistory] OFF
INSERT [dbo].[MedicationScheduleItemHistoryEmployee] ([MedicationScheduleItemHistoryId], [EmployeeId]) VALUES (7, 1)
INSERT [dbo].[MedicationScheduleItemHistoryEmployee] ([MedicationScheduleItemHistoryId], [EmployeeId]) VALUES (8, 1)
INSERT [dbo].[MedicationScheduleItemHistoryEmployee] ([MedicationScheduleItemHistoryId], [EmployeeId]) VALUES (9, 2)
INSERT [dbo].[MedicationScheduleItemHistoryEmployee] ([MedicationScheduleItemHistoryId], [EmployeeId]) VALUES (10, 2)
INSERT [dbo].[MedicationScheduleItemHistoryEmployee] ([MedicationScheduleItemHistoryId], [EmployeeId]) VALUES (11, 2)
INSERT [dbo].[MedicationScheduleItemHistoryEmployee] ([MedicationScheduleItemHistoryId], [EmployeeId]) VALUES (12, 2)
INSERT [dbo].[MedicationScheduleItemHistoryEmployee] ([MedicationScheduleItemHistoryId], [EmployeeId]) VALUES (13, 2)
INSERT [dbo].[MedicationScheduleItemHistoryEmployee] ([MedicationScheduleItemHistoryId], [EmployeeId]) VALUES (14, 2)
INSERT [dbo].[MedicationScheduleItemHistoryEmployee] ([MedicationScheduleItemHistoryId], [EmployeeId]) VALUES (15, 2)
INSERT [dbo].[MedicationScheduleItemHistoryEmployee] ([MedicationScheduleItemHistoryId], [EmployeeId]) VALUES (16, 2)
INSERT [dbo].[MedicationScheduleItemHistoryEmployee] ([MedicationScheduleItemHistoryId], [EmployeeId]) VALUES (17, 2)
INSERT [dbo].[MedicationScheduleItemHistoryEmployee] ([MedicationScheduleItemHistoryId], [EmployeeId]) VALUES (18, 2)
INSERT [dbo].[MedicationScheduleItemHistoryEmployee] ([MedicationScheduleItemHistoryId], [EmployeeId]) VALUES (19, 2)
INSERT [dbo].[MedicationScheduleItemHistoryEmployee] ([MedicationScheduleItemHistoryId], [EmployeeId]) VALUES (20, 2)
INSERT [dbo].[MedicationScheduleItemHistoryEmployee] ([MedicationScheduleItemHistoryId], [EmployeeId]) VALUES (21, 2)
INSERT [dbo].[MedicationScheduleItemHistoryEmployee] ([MedicationScheduleItemHistoryId], [EmployeeId]) VALUES (22, 2)
INSERT [dbo].[MedicationScheduleItemHistoryEmployee] ([MedicationScheduleItemHistoryId], [EmployeeId]) VALUES (1021, 1)
INSERT [dbo].[MedicationScheduleItemRepeat] ([MedicationScheduleItemId], [RepeatTimeSpan]) VALUES (19, 864000000000)
INSERT [dbo].[MedicationScheduleItemRepeat] ([MedicationScheduleItemId], [RepeatTimeSpan]) VALUES (20, 864000000000)
INSERT [dbo].[MedicationScheduleItemRepeat] ([MedicationScheduleItemId], [RepeatTimeSpan]) VALUES (21, 864000000000)
INSERT [dbo].[MedicationScheduleItemRepeat] ([MedicationScheduleItemId], [RepeatTimeSpan]) VALUES (23, 6048000000000)
INSERT [dbo].[MedicationScheduleItemRepeat] ([MedicationScheduleItemId], [RepeatTimeSpan]) VALUES (24, 864000000000)
INSERT [dbo].[MedicationScheduleItemRepeat] ([MedicationScheduleItemId], [RepeatTimeSpan]) VALUES (25, 864000000000)
INSERT [dbo].[MedicationScheduleItemRepeat] ([MedicationScheduleItemId], [RepeatTimeSpan]) VALUES (26, 864000000000)
INSERT [dbo].[MedicationScheduleItemRepeat] ([MedicationScheduleItemId], [RepeatTimeSpan]) VALUES (28, 6048000000000)
INSERT [dbo].[MedicationScheduleItemRepeat] ([MedicationScheduleItemId], [RepeatTimeSpan]) VALUES (30, 6048000000000)
INSERT [dbo].[MedicationScheduleItemRepeat] ([MedicationScheduleItemId], [RepeatTimeSpan]) VALUES (31, 864000000000)
INSERT [dbo].[MedicationScheduleItemRepeat] ([MedicationScheduleItemId], [RepeatTimeSpan]) VALUES (32, 864000000000)
INSERT [dbo].[MedicationScheduleItemRepeat] ([MedicationScheduleItemId], [RepeatTimeSpan]) VALUES (33, 864000000000)
INSERT [dbo].[MedicationScheduleItemRepeat] ([MedicationScheduleItemId], [RepeatTimeSpan]) VALUES (34, 864000000000)
INSERT [dbo].[MedicationScheduleItemRepeat] ([MedicationScheduleItemId], [RepeatTimeSpan]) VALUES (35, 864000000000)
INSERT [dbo].[MedicationScheduleItemRepeat] ([MedicationScheduleItemId], [RepeatTimeSpan]) VALUES (1034, 864000000000)
SET IDENTITY_INSERT [dbo].[MedicationType] ON 

INSERT [dbo].[MedicationType] ([Id], [Name]) VALUES (1, N'Tabletter')
INSERT [dbo].[MedicationType] ([Id], [Name]) VALUES (2, N'Væske')
INSERT [dbo].[MedicationType] ([Id], [Name]) VALUES (3, N'Indsprøjtning')
SET IDENTITY_INSERT [dbo].[MedicationType] OFF
SET IDENTITY_INSERT [dbo].[Municipality] ON 

INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (1, N'Aarhus Kommune', 1)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (2, N'Favrskov Kommune', 1)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (3, N'Hedensted Kommune', 1)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (4, N'Herning Kommune', 1)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (5, N'Holstebro Kommune', 1)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (6, N'Horsens Kommune', 1)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (7, N'Ikast-Brande Kommune', 1)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (8, N'Lemvig Kommune', 1)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (9, N'Norddjurs Kommune', 1)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (10, N'Odder Kommune', 1)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (11, N'Randers Kommune', 1)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (12, N'Ringkøbing-Skjern Kommune', 1)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (13, N'Samsø Kommune', 1)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (14, N'Silkeborg Kommune', 1)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (15, N'Skanderborg Kommune', 1)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (16, N'Skive Kommune', 1)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (17, N'Struer Kommune', 1)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (18, N'Syddjurs Kommune', 1)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (19, N'Viborg Kommune', 1)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (20, N'Faxe Kommune', 2)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (21, N'Greve Kommune', 2)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (22, N'Guldborgsund Kommune', 2)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (23, N'Holbæk Kommune', 2)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (24, N'Kalundborg Kommune', 2)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (25, N'Køge Kommune', 2)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (26, N'Lejre Kommune', 2)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (27, N'Lolland Kommune', 2)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (28, N'Næstved Kommune', 2)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (29, N'Odsherred Kommune', 2)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (30, N'Ringsted Kommune', 2)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (31, N'Roskilde Kommune', 2)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (32, N'Slagelse Kommune', 2)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (33, N'Solrød Kommune', 2)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (34, N'Sorø Kommune', 2)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (35, N'Stevns Kommune', 2)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (36, N'Vordingborg Kommune', 2)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (37, N'Aalborg Kommune', 3)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (38, N'Brønderslev Kommune', 3)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (39, N'Frederikshavn Kommune', 3)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (40, N'Hjørring Kommune', 3)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (41, N'Jammerbugt Kommune', 3)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (42, N'Læsø Kommune', 3)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (43, N'Mariagerfjord Kommune', 3)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (44, N'Morsø Kommune', 3)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (45, N'Rebild Kommune', 3)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (46, N'Thisted Kommune', 3)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (47, N'Vesthimmerlands Kommune', 3)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (48, N'Albertslund Kommune', 4)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (49, N'Allerød Kommune', 4)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (50, N'Ballerup Kommune', 4)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (51, N'Bornholm Kommune', 4)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (52, N'Brøndby Kommune', 4)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (53, N'Christiansø', 4)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (54, N'Dragør Kommune', 4)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (55, N'Egedal Kommune', 4)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (56, N'Fredensborg Kommune', 4)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (57, N'Frederiksberg Kommune', 4)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (58, N'Frederikssund Kommune', 4)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (59, N'Furesø Kommune', 4)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (60, N'Gentofte Kommune', 4)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (61, N'Gladsaxe Kommune', 4)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (62, N'Glostrup Kommune', 4)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (63, N'Gribskov Kommune', 4)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (64, N'Halsnæs Kommune', 4)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (65, N'Helsingør Kommune', 4)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (66, N'Herlev Kommune', 4)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (67, N'Hillerød Kommune', 4)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (68, N'Høje-Taastrup Kommune', 4)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (69, N'Hørsholm Kommune', 4)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (70, N'Hvidovre Kommune', 4)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (71, N'Ishøj Kommune', 4)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (72, N'Københavns Kommune', 4)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (73, N'Lyngby-Taarbæk Kommune', 4)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (74, N'Rødovre Kommune', 4)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (75, N'Rudersdal Kommune', 4)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (76, N'Tårnby Kommune', 4)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (77, N'Vallensbæk Kommune', 4)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (78, N'Aabenraa Kommune', 5)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (79, N'Ærø Kommune', 5)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (80, N'Assens Kommune', 5)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (81, N'Billund Kommune', 5)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (82, N'Esbjerg Kommune', 5)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (83, N'Faaborg-Midtfyn Kommune', 5)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (84, N'Fanø Kommune', 5)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (85, N'Fredericia Kommune', 5)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (86, N'Haderslev Kommune', 5)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (87, N'Kerteminde Kommune', 5)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (88, N'Kolding Kommune', 5)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (89, N'Langeland Kommune', 5)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (90, N'Middelfart Kommune', 5)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (91, N'Nordfyns Kommune', 5)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (92, N'Nyborg Kommune', 5)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (93, N'Odense Kommune', 5)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (94, N'Sønderborg Kommune', 5)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (95, N'Svendborg Kommune', 5)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (96, N'Tønder Kommune', 5)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (97, N'Varde Kommune', 5)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (98, N'Vejen Kommune', 5)
INSERT [dbo].[Municipality] ([Id], [Name], [RegionId]) VALUES (99, N'Vejle Kommune', 5)
GO
SET IDENTITY_INSERT [dbo].[Municipality] OFF
SET IDENTITY_INSERT [dbo].[Organization] ON 

INSERT [dbo].[Organization] ([Id], [Name], [TimeCreated]) VALUES (1, N'Olivers Bosted', CAST(N'2014-08-08 20:56:00.000' AS DateTime))
INSERT [dbo].[Organization] ([Id], [Name], [TimeCreated]) VALUES (2, N'Teglgårdslund', CAST(N'2014-08-08 20:56:00.000' AS DateTime))
INSERT [dbo].[Organization] ([Id], [Name], [TimeCreated]) VALUES (3, N'Lærlingehuset', CAST(N'2014-08-16 16:21:55.873' AS DateTime))
INSERT [dbo].[Organization] ([Id], [Name], [TimeCreated]) VALUES (7, N'Hjulsøgård', CAST(N'2014-08-23 20:57:39.453' AS DateTime))
INSERT [dbo].[Organization] ([Id], [Name], [TimeCreated]) VALUES (8, N'Mørkhøjvej', CAST(N'2015-01-07 19:16:03.973' AS DateTime))
INSERT [dbo].[Organization] ([Id], [Name], [TimeCreated]) VALUES (9, N'Husumvej', CAST(N'2015-01-07 19:35:11.360' AS DateTime))
INSERT [dbo].[Organization] ([Id], [Name], [TimeCreated]) VALUES (10, N'DailyReport', CAST(N'2015-01-10 16:33:40.213' AS DateTime))
INSERT [dbo].[Organization] ([Id], [Name], [TimeCreated]) VALUES (11, N'Lystoftebakken', CAST(N'2015-01-10 18:16:32.873' AS DateTime))
SET IDENTITY_INSERT [dbo].[Organization] OFF
SET IDENTITY_INSERT [dbo].[Patient] ON 

INSERT [dbo].[Patient] ([Id], [OrganizationId], [Firstname], [Lastname], [TimeCreated], [BirthDate], [IsActive]) VALUES (1, 1, N'Oliver', N'Lassen', CAST(N'2014-08-19 22:09:19.823' AS DateTime), CAST(N'1993-06-19' AS Date), 1)
INSERT [dbo].[Patient] ([Id], [OrganizationId], [Firstname], [Lastname], [TimeCreated], [BirthDate], [IsActive]) VALUES (2, 1, N'Ditte', N'Ruusunen', CAST(N'2014-08-19 22:09:19.880' AS DateTime), CAST(N'1993-07-04' AS Date), 1)
INSERT [dbo].[Patient] ([Id], [OrganizationId], [Firstname], [Lastname], [TimeCreated], [BirthDate], [IsActive]) VALUES (3, 1, N'Jens', N'Jensen', CAST(N'2014-08-20 19:04:13.113' AS DateTime), CAST(N'1990-01-02' AS Date), 1)
INSERT [dbo].[Patient] ([Id], [OrganizationId], [Firstname], [Lastname], [TimeCreated], [BirthDate], [IsActive]) VALUES (4, 1, N'Mille', N'Ruusunen', CAST(N'2014-08-20 19:35:07.773' AS DateTime), CAST(N'1991-04-18' AS Date), 1)
INSERT [dbo].[Patient] ([Id], [OrganizationId], [Firstname], [Lastname], [TimeCreated], [BirthDate], [IsActive]) VALUES (5, 1, N'Kim', N'Larsen', CAST(N'2014-11-22 17:22:22.327' AS DateTime), CAST(N'2001-11-17' AS Date), 1)
INSERT [dbo].[Patient] ([Id], [OrganizationId], [Firstname], [Lastname], [TimeCreated], [BirthDate], [IsActive]) VALUES (1005, 11, N'Kasper', N'Madsen', CAST(N'2015-01-10 19:51:59.757' AS DateTime), CAST(N'2000-01-02' AS Date), 1)
INSERT [dbo].[Patient] ([Id], [OrganizationId], [Firstname], [Lastname], [TimeCreated], [BirthDate], [IsActive]) VALUES (1006, 11, N'Kasper2', N'Madsen2', CAST(N'2015-01-10 19:52:14.680' AS DateTime), CAST(N'2000-01-02' AS Date), 1)
INSERT [dbo].[Patient] ([Id], [OrganizationId], [Firstname], [Lastname], [TimeCreated], [BirthDate], [IsActive]) VALUES (1007, 11, N'Kaspe3', N'Madsen2', CAST(N'2015-01-10 19:53:50.573' AS DateTime), CAST(N'2000-01-02' AS Date), 1)
INSERT [dbo].[Patient] ([Id], [OrganizationId], [Firstname], [Lastname], [TimeCreated], [BirthDate], [IsActive]) VALUES (1008, 11, N'Kaspe4', N'Madsen2', CAST(N'2015-01-10 19:55:15.210' AS DateTime), CAST(N'2000-01-02' AS Date), 1)
INSERT [dbo].[Patient] ([Id], [OrganizationId], [Firstname], [Lastname], [TimeCreated], [BirthDate], [IsActive]) VALUES (1009, 11, N'Kaspe4', N'Madsen2', CAST(N'2015-01-10 19:55:39.730' AS DateTime), CAST(N'2000-01-02' AS Date), 1)
INSERT [dbo].[Patient] ([Id], [OrganizationId], [Firstname], [Lastname], [TimeCreated], [BirthDate], [IsActive]) VALUES (1010, 11, N'Kaspe5', N'Madsen2', CAST(N'2015-01-10 19:55:45.363' AS DateTime), CAST(N'2000-01-02' AS Date), 1)
INSERT [dbo].[Patient] ([Id], [OrganizationId], [Firstname], [Lastname], [TimeCreated], [BirthDate], [IsActive]) VALUES (1011, 11, N'Kaspe6', N'Madsen2', CAST(N'2015-01-10 19:56:13.513' AS DateTime), CAST(N'2000-01-02' AS Date), 1)
INSERT [dbo].[Patient] ([Id], [OrganizationId], [Firstname], [Lastname], [TimeCreated], [BirthDate], [IsActive]) VALUES (1013, 11, N'Kaspe7', N'Madsen2', CAST(N'2015-01-10 19:58:37.800' AS DateTime), CAST(N'2000-01-02' AS Date), 1)
INSERT [dbo].[Patient] ([Id], [OrganizationId], [Firstname], [Lastname], [TimeCreated], [BirthDate], [IsActive]) VALUES (1014, 10, N'Mads', N'Kaspersen', CAST(N'2015-01-10 20:11:19.150' AS DateTime), CAST(N'2000-01-02' AS Date), 1)
SET IDENTITY_INSERT [dbo].[Patient] OFF
INSERT [dbo].[Patient_ContactInformation] ([ContactInformationId], [PatientId]) VALUES (1, 1)
INSERT [dbo].[Patient_ContactInformation] ([ContactInformationId], [PatientId]) VALUES (2, 1)
INSERT [dbo].[Patient_ContactInformation] ([ContactInformationId], [PatientId]) VALUES (4, 1)
INSERT [dbo].[Patient_ContactInformation] ([ContactInformationId], [PatientId]) VALUES (5, 1)
INSERT [dbo].[Patient_ContactInformation] ([ContactInformationId], [PatientId]) VALUES (6, 1)
INSERT [dbo].[Patient_ContactInformation] ([ContactInformationId], [PatientId]) VALUES (7, 1)
INSERT [dbo].[Patient_ContactInformation] ([ContactInformationId], [PatientId]) VALUES (8, 1)
INSERT [dbo].[Patient_ContactInformation] ([ContactInformationId], [PatientId]) VALUES (9, 1)
INSERT [dbo].[Patient_ContactInformation] ([ContactInformationId], [PatientId]) VALUES (10, 5)
INSERT [dbo].[Patient_ContactInformation] ([ContactInformationId], [PatientId]) VALUES (11, 5)
INSERT [dbo].[Patient_ContactInformation] ([ContactInformationId], [PatientId]) VALUES (12, 4)
INSERT [dbo].[Patient_ContactInformation] ([ContactInformationId], [PatientId]) VALUES (1010, 1)
INSERT [dbo].[Patient_ContactInformation] ([ContactInformationId], [PatientId]) VALUES (1011, 1)
SET IDENTITY_INSERT [dbo].[PatientContactPersonRelationship] ON 

INSERT [dbo].[PatientContactPersonRelationship] ([Id], [PatientId], [EmployeeId], [TimeCreated]) VALUES (1057, 3, 1, CAST(N'2014-10-13 21:35:55.350' AS DateTime))
INSERT [dbo].[PatientContactPersonRelationship] ([Id], [PatientId], [EmployeeId], [TimeCreated]) VALUES (1058, 3, 2, CAST(N'2014-10-13 21:36:07.550' AS DateTime))
INSERT [dbo].[PatientContactPersonRelationship] ([Id], [PatientId], [EmployeeId], [TimeCreated]) VALUES (1059, 3, 1, CAST(N'2014-10-13 22:18:09.153' AS DateTime))
INSERT [dbo].[PatientContactPersonRelationship] ([Id], [PatientId], [EmployeeId], [TimeCreated]) VALUES (1060, 3, 2, CAST(N'2014-10-13 22:18:31.297' AS DateTime))
INSERT [dbo].[PatientContactPersonRelationship] ([Id], [PatientId], [EmployeeId], [TimeCreated]) VALUES (1061, 3, 1, CAST(N'2014-10-13 22:18:34.337' AS DateTime))
INSERT [dbo].[PatientContactPersonRelationship] ([Id], [PatientId], [EmployeeId], [TimeCreated]) VALUES (1062, 1, 1, CAST(N'2014-11-18 18:58:42.377' AS DateTime))
INSERT [dbo].[PatientContactPersonRelationship] ([Id], [PatientId], [EmployeeId], [TimeCreated]) VALUES (1063, 1, 2, CAST(N'2014-11-20 19:13:34.450' AS DateTime))
INSERT [dbo].[PatientContactPersonRelationship] ([Id], [PatientId], [EmployeeId], [TimeCreated]) VALUES (1064, 1, 1, CAST(N'2014-11-20 19:13:42.440' AS DateTime))
INSERT [dbo].[PatientContactPersonRelationship] ([Id], [PatientId], [EmployeeId], [TimeCreated]) VALUES (1065, 5, 1, CAST(N'2014-11-22 17:24:38.883' AS DateTime))
INSERT [dbo].[PatientContactPersonRelationship] ([Id], [PatientId], [EmployeeId], [TimeCreated]) VALUES (1066, 5, 2, CAST(N'2014-11-22 17:24:49.533' AS DateTime))
INSERT [dbo].[PatientContactPersonRelationship] ([Id], [PatientId], [EmployeeId], [TimeCreated]) VALUES (1067, 4, 2, CAST(N'2014-11-24 20:58:23.767' AS DateTime))
INSERT [dbo].[PatientContactPersonRelationship] ([Id], [PatientId], [EmployeeId], [TimeCreated]) VALUES (2065, 1006, 27, CAST(N'2015-01-10 19:52:14.737' AS DateTime))
INSERT [dbo].[PatientContactPersonRelationship] ([Id], [PatientId], [EmployeeId], [TimeCreated]) VALUES (2066, 1006, 28, CAST(N'2015-01-10 19:52:14.983' AS DateTime))
INSERT [dbo].[PatientContactPersonRelationship] ([Id], [PatientId], [EmployeeId], [TimeCreated]) VALUES (2067, 1007, 27, CAST(N'2015-01-10 19:53:50.620' AS DateTime))
INSERT [dbo].[PatientContactPersonRelationship] ([Id], [PatientId], [EmployeeId], [TimeCreated]) VALUES (2068, 1010, 27, CAST(N'2015-01-10 19:55:45.377' AS DateTime))
INSERT [dbo].[PatientContactPersonRelationship] ([Id], [PatientId], [EmployeeId], [TimeCreated]) VALUES (2069, 1010, 28, CAST(N'2015-01-10 19:55:45.630' AS DateTime))
INSERT [dbo].[PatientContactPersonRelationship] ([Id], [PatientId], [EmployeeId], [TimeCreated]) VALUES (2070, 1011, 27, CAST(N'2015-01-10 19:56:13.630' AS DateTime))
INSERT [dbo].[PatientContactPersonRelationship] ([Id], [PatientId], [EmployeeId], [TimeCreated]) VALUES (2071, 1013, 27, CAST(N'2015-01-10 19:58:37.953' AS DateTime))
INSERT [dbo].[PatientContactPersonRelationship] ([Id], [PatientId], [EmployeeId], [TimeCreated]) VALUES (2072, 1013, 28, CAST(N'2015-01-10 19:58:38.097' AS DateTime))
SET IDENTITY_INSERT [dbo].[PatientContactPersonRelationship] OFF
INSERT [dbo].[PatientContactPersonRelationshipEnded] ([PatientContactPersonRelationshipId], [TimeCreated]) VALUES (1057, CAST(N'2014-10-13 21:36:07.550' AS DateTime))
INSERT [dbo].[PatientContactPersonRelationshipEnded] ([PatientContactPersonRelationshipId], [TimeCreated]) VALUES (1058, CAST(N'2014-10-13 22:18:09.153' AS DateTime))
INSERT [dbo].[PatientContactPersonRelationshipEnded] ([PatientContactPersonRelationshipId], [TimeCreated]) VALUES (1059, CAST(N'2014-10-13 22:18:31.297' AS DateTime))
INSERT [dbo].[PatientContactPersonRelationshipEnded] ([PatientContactPersonRelationshipId], [TimeCreated]) VALUES (1062, CAST(N'2014-11-20 19:13:34.450' AS DateTime))
INSERT [dbo].[PatientContactPersonRelationshipEnded] ([PatientContactPersonRelationshipId], [TimeCreated]) VALUES (1063, CAST(N'2014-11-24 20:53:12.307' AS DateTime))
INSERT [dbo].[PatientContactPersonRelationshipEnded] ([PatientContactPersonRelationshipId], [TimeCreated]) VALUES (1065, CAST(N'2014-11-22 17:24:49.533' AS DateTime))
INSERT [dbo].[PatientDescription] ([PatientId], [DescriptionId]) VALUES (1, 15)
INSERT [dbo].[PatientDescription] ([PatientId], [DescriptionId]) VALUES (2, 2)
INSERT [dbo].[PatientDescription] ([PatientId], [DescriptionId]) VALUES (2, 3)
INSERT [dbo].[PatientDescription] ([PatientId], [DescriptionId]) VALUES (2, 4)
INSERT [dbo].[PatientDescription] ([PatientId], [DescriptionId]) VALUES (2, 5)
INSERT [dbo].[PatientDescription] ([PatientId], [DescriptionId]) VALUES (2, 6)
INSERT [dbo].[PatientDescription] ([PatientId], [DescriptionId]) VALUES (2, 7)
INSERT [dbo].[PatientDescription] ([PatientId], [DescriptionId]) VALUES (2, 8)
INSERT [dbo].[PatientDescription] ([PatientId], [DescriptionId]) VALUES (2, 9)
INSERT [dbo].[PatientDescription] ([PatientId], [DescriptionId]) VALUES (2, 10)
INSERT [dbo].[PatientDescription] ([PatientId], [DescriptionId]) VALUES (2, 11)
INSERT [dbo].[PatientDescription] ([PatientId], [DescriptionId]) VALUES (2, 12)
INSERT [dbo].[PatientDescription] ([PatientId], [DescriptionId]) VALUES (2, 13)
INSERT [dbo].[PatientDescription] ([PatientId], [DescriptionId]) VALUES (2, 14)
INSERT [dbo].[PatientDescription] ([PatientId], [DescriptionId]) VALUES (2, 16)
INSERT [dbo].[PatientDescription] ([PatientId], [DescriptionId]) VALUES (5, 17)
INSERT [dbo].[PatientDescription] ([PatientId], [DescriptionId]) VALUES (1014, 1017)
SET IDENTITY_INSERT [dbo].[PatientPost] ON 

INSERT [dbo].[PatientPost] ([Id], [EmployeeId], [TimeCreated], [Description], [OrganizationId]) VALUES (8, 1, CAST(N'2014-08-23 19:53:03.253' AS DateTime), N'sadfsdf', 1)
INSERT [dbo].[PatientPost] ([Id], [EmployeeId], [TimeCreated], [Description], [OrganizationId]) VALUES (13, 2, CAST(N'2014-08-25 21:29:03.267' AS DateTime), N'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.

Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.

Neque porro quisquam est, qui dolorem', 1)
INSERT [dbo].[PatientPost] ([Id], [EmployeeId], [TimeCreated], [Description], [OrganizationId]) VALUES (14, 3, CAST(N'2014-08-25 21:30:28.697' AS DateTime), N'isus sed libero blandit rutrum. Vivamus egestas ligula sit amet lectus egestas volutpat. Aliquam vestibulum id orci et cursus. In hac habitasse platea dictumst. Sed sodales mi est, sit amet tempor ligula ultrices vel. Morbi vitae condimentum orci. Aliquam vel efficitur urna. Donec nec mauris massa.

Maecenas vel nisl nibh. Nullam rhoncus tincidunt vehicula. Donec molestie varius est sit amet malesuada. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed tristique, tellus ut cursus pulvinar, arcu diam rhoncus urna, non imperdiet mauris mauris non sem. Donec a laoreet nisl. Donec ultricies ut augue porttitor hendrerit. Maecenas tristique sit am', 1)
INSERT [dbo].[PatientPost] ([Id], [EmployeeId], [TimeCreated], [Description], [OrganizationId]) VALUES (15, 1, CAST(N'2014-08-25 22:13:52.957' AS DateTime), N'netus et malesuada fames ac turpis egestas. Sed tristique, tellus ut cursus pulvinar, arcu diam rhoncus urna, non imperdiet mauris mauris non sem. Donec a laoreet nisl. Donec ultricies ut augue porttitor hendrerit. Maecenas tristique sit amet turpis vel eleifend. Sed suscipit blandit justo quis venenatis. Nunc mollis leo eu neque bibendum, eu ultricies arcu vehicula. Maecenas', 1)
INSERT [dbo].[PatientPost] ([Id], [EmployeeId], [TimeCreated], [Description], [OrganizationId]) VALUES (16, 1, CAST(N'2014-08-25 22:15:16.790' AS DateTime), N'it risus sed libero blandit rutrum. Vivamus egestas ligula sit amet lectus egestas volutpat. Aliquam vestibulum id orci et cursus. In hac habitasse platea dictumst. Sed sodales mi est, sit amet tempor ligula ultrices vel. Morbi vitae condimentum orci. Aliquam vel efficitur urna. Done', 1)
INSERT [dbo].[PatientPost] ([Id], [EmployeeId], [TimeCreated], [Description], [OrganizationId]) VALUES (17, 1, CAST(N'2014-08-25 22:19:25.687' AS DateTime), N'Will ''e ''eckerslike be reet. Chuffin'' nora mardy bum gi'' o''er aye tha what. God''s own county soft lad ee by gum ey up tell thi summat for nowt. Sup wi'' ''im gerritetten face like a slapped arse where''s tha bin by ''eck where there''s muck there''s brass. Appens as may - See more at: http://tlipsum.appspot.com/#sthash.splgbmBc.dpuf', 1)
INSERT [dbo].[PatientPost] ([Id], [EmployeeId], [TimeCreated], [Description], [OrganizationId]) VALUES (18, 1, CAST(N'2014-08-25 22:22:12.123' AS DateTime), N'ed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo invent', 1)
INSERT [dbo].[PatientPost] ([Id], [EmployeeId], [TimeCreated], [Description], [OrganizationId]) VALUES (19, 1, CAST(N'2014-08-25 22:22:24.297' AS DateTime), N'ed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo invent sdas ed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo invented ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo invent', 1)
INSERT [dbo].[PatientPost] ([Id], [EmployeeId], [TimeCreated], [Description], [OrganizationId]) VALUES (20, 1, CAST(N'2014-08-25 22:22:43.963' AS DateTime), N'ed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo invent ed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo invent', 1)
INSERT [dbo].[PatientPost] ([Id], [EmployeeId], [TimeCreated], [Description], [OrganizationId]) VALUES (21, 1, CAST(N'2014-08-31 13:29:49.253' AS DateTime), N'Test', 1)
INSERT [dbo].[PatientPost] ([Id], [EmployeeId], [TimeCreated], [Description], [OrganizationId]) VALUES (22, 7, CAST(N'2014-08-31 13:33:08.353' AS DateTime), N'asdfasdf', 1)
SET IDENTITY_INSERT [dbo].[PatientPost] OFF
INSERT [dbo].[PatientPost_Patient] ([PatientId], [PatientPostId]) VALUES (1, 8)
INSERT [dbo].[PatientPost_Patient] ([PatientId], [PatientPostId]) VALUES (1, 13)
INSERT [dbo].[PatientPost_Patient] ([PatientId], [PatientPostId]) VALUES (1, 14)
INSERT [dbo].[PatientPost_Patient] ([PatientId], [PatientPostId]) VALUES (1, 15)
INSERT [dbo].[PatientPost_Patient] ([PatientId], [PatientPostId]) VALUES (1, 16)
INSERT [dbo].[PatientPost_Patient] ([PatientId], [PatientPostId]) VALUES (1, 17)
INSERT [dbo].[PatientPost_Patient] ([PatientId], [PatientPostId]) VALUES (1, 20)
INSERT [dbo].[PatientPost_Patient] ([PatientId], [PatientPostId]) VALUES (1, 21)
INSERT [dbo].[PatientPost_Patient] ([PatientId], [PatientPostId]) VALUES (2, 13)
INSERT [dbo].[PatientPost_Patient] ([PatientId], [PatientPostId]) VALUES (2, 14)
INSERT [dbo].[PatientPost_Patient] ([PatientId], [PatientPostId]) VALUES (2, 20)
INSERT [dbo].[PatientPost_Patient] ([PatientId], [PatientPostId]) VALUES (3, 14)
INSERT [dbo].[PatientPost_Patient] ([PatientId], [PatientPostId]) VALUES (3, 18)
INSERT [dbo].[PatientPost_Patient] ([PatientId], [PatientPostId]) VALUES (3, 20)
INSERT [dbo].[PatientPost_Patient] ([PatientId], [PatientPostId]) VALUES (3, 22)
INSERT [dbo].[PatientPost_Patient] ([PatientId], [PatientPostId]) VALUES (4, 8)
INSERT [dbo].[PatientPost_Patient] ([PatientId], [PatientPostId]) VALUES (4, 14)
INSERT [dbo].[PatientPost_Patient] ([PatientId], [PatientPostId]) VALUES (4, 19)
INSERT [dbo].[PatientPost_Patient] ([PatientId], [PatientPostId]) VALUES (4, 20)
SET IDENTITY_INSERT [dbo].[Region] ON 

INSERT [dbo].[Region] ([Id], [Name], [CountryId]) VALUES (1, N'Region Midtjylland', 1)
INSERT [dbo].[Region] ([Id], [Name], [CountryId]) VALUES (2, N'Region Sjælland', 1)
INSERT [dbo].[Region] ([Id], [Name], [CountryId]) VALUES (3, N'Region Nordjylland', 1)
INSERT [dbo].[Region] ([Id], [Name], [CountryId]) VALUES (4, N'Region Hovedstaden', 1)
INSERT [dbo].[Region] ([Id], [Name], [CountryId]) VALUES (5, N'Region Syddanmark', 1)
SET IDENTITY_INSERT [dbo].[Region] OFF
SET IDENTITY_INSERT [dbo].[Town] ON 

INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (621, N'Aabenraa', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (622, N'Aabybro', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (623, N'Aakirkeby', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (624, N'Aalborg', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (625, N'Aalborg Øst', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (626, N'Aalborg SØ', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (627, N'Aalborg SV', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (628, N'Aalestrup', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (629, N'Aalestrup', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (630, N'Aarhus C', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (631, N'Aarhus N', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (632, N'Aarhus V', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (633, N'Aars', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (634, N'Aarup', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (635, N'Åbyhøj', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (636, N'Ærøskøbing', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (637, N'Agedrup', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (638, N'Agerbæk', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (639, N'Agerskov', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (640, N'Ålbæk', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (641, N'Albertslund', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (642, N'Allerød', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (643, N'Allingåbro', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (644, N'Allinge', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (645, N'Almind', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (646, N'Ålsgårde', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (647, N'Anholt', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (648, N'Ans By', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (649, N'Ansager', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (650, N'Arden', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (651, N'Årre', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (652, N'Årslev', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (653, N'Asaa', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (654, N'Askeby', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (655, N'Asnæs', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (656, N'Asperup', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (657, N'Assens', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (658, N'Augustenborg', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (659, N'Aulum', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (660, N'Auning', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (661, N'Bække', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (662, N'Bækmarksbro', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (663, N'Bælum', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (664, N'Bagenkop', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (665, N'Bagsværd', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (666, N'Balle', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (667, N'Ballerup', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (668, N'Bandholm', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (669, N'Barrit', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (670, N'Beder', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (671, N'Bedsted Thy', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (672, N'Bevtoft', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (673, N'Billum', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (674, N'Billund', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (675, N'Bindslev', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (676, N'Birkerød', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (677, N'Bjæverskov', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (678, N'Bjerringbro', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (679, N'Bjert', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (680, N'Blåvand', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (681, N'Blokhus', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (682, N'Blommenslyst', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (683, N'Boeslunde', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (684, N'Bogense', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (685, N'Bogø By', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (686, N'Bolderslev', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (687, N'Bording', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (688, N'Børkop', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (689, N'Borre', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (690, N'Borup', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (691, N'Bøvlingbjerg', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (692, N'Brabrand', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (693, N'Brædstrup', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (694, N'Bramming', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (695, N'Brande', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (696, N'Brande', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (697, N'Branderup J', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (698, N'Bredebro', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (699, N'Bredsten', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (700, N'Brenderup Fyn', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (701, N'Broager', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (702, N'Broby', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (703, N'Brøndby', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (704, N'Brøndby Strand', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (705, N'Brønderslev', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (706, N'Brønshøj', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (707, N'Brørup', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (708, N'Brovst', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (709, N'Bryrup', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (710, N'Bylderup-Bov', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (711, N'Charlottenlund', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (712, N'Christiansfeld', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (713, N'Dalby', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (714, N'Dalmose', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (715, N'Dannemare', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (716, N'Daugård', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (717, N'Dianalund', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (718, N'Dragør', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (719, N'Dronninglund', 3)
GO
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (720, N'Dronningmølle', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (721, N'Dybvad', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (722, N'Dyssegård', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (723, N'Ebberup', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (724, N'Ebeltoft', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (725, N'Egå', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (726, N'Egernsund', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (727, N'Egtved', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (728, N'Ejby', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (729, N'Ejstrupholm', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (730, N'Ejstrupholm', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (731, N'Engesvang', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (732, N'Errindlev', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (733, N'Erslev', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (734, N'Esbjerg', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (735, N'Esbjerg N', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (736, N'Esbjerg Ø', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (737, N'Esbjerg V', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (738, N'Eskebjerg', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (739, N'Eskilstrup', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (740, N'Espergærde', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (741, N'Faaborg', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (742, N'Fanø', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (743, N'Fårevejle', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (744, N'Farsø', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (745, N'Farum', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (746, N'Fårup', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (747, N'Fårup', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (748, N'Fårvang', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (749, N'Faxe', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (750, N'Faxe Ladeplads', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (751, N'Fejø', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (752, N'Ferritslev Fyn', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (753, N'Fjenneslev', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (754, N'Fjerritslev', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (755, N'Flemming', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (756, N'Føllenslev', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (757, N'Føvling', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (758, N'Fredensborg', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (759, N'Fredericia', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (760, N'Frederiksberg', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (761, N'Frederiksberg C', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (762, N'Frederikshavn', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (763, N'Frederikssund', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (764, N'Frederiksværk', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (765, N'Frørup', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (766, N'Frøstrup', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (767, N'Fuglebjerg', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (768, N'Fur', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (769, N'Gadbjerg', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (770, N'Gadstrup', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (771, N'Galten', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (772, N'Gandrup', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (773, N'Gedser', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (774, N'Gedsted', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (775, N'Gedsted', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (776, N'Gedved', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (777, N'Gelsted', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (778, N'Gentofte', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (779, N'Gesten', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (780, N'Gilleleje', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (781, N'Gislev', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (782, N'Gislinge', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (783, N'Gistrup', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (784, N'Give', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (785, N'Give', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (786, N'Gjerlev J', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (787, N'Gjern', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (788, N'Glamsbjerg', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (789, N'Glejbjerg', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (790, N'Glesborg', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (791, N'Glostrup', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (792, N'Glumsø', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (793, N'Gørding', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (794, N'Gørlev', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (795, N'Gørløse', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (796, N'Græsted', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (797, N'Gram', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (798, N'Gråsten', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (799, N'Gredstedbro', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (800, N'Grenaa', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (801, N'Greve', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (802, N'Grevinge', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (803, N'Grindsted', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (804, N'Gudbjerg Sydfyn', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (805, N'Gudhjem', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (806, N'Gudme', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (807, N'Guldborg', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (808, N'Haarby', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (809, N'Haderslev', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (810, N'Haderup', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (811, N'Hadsten', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (812, N'Hadsund', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (813, N'Hals', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (814, N'Hammel', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (815, N'Hampen', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (816, N'Hanstholm', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (817, N'Harboøre', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (818, N'Hårlev', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (819, N'Harlev J', 1)
GO
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (820, N'Harndrup', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (821, N'Harpelunde', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (822, N'Hasle', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (823, N'Haslev', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (824, N'Hasselager', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (825, N'Havdrup', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (826, N'Havndal', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (827, N'Havndal', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (828, N'Hedehusene', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (829, N'Hedehusene', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (830, N'Hedensted', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (831, N'Hejls', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (832, N'Hejnsvig', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (833, N'Hellebæk', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (834, N'Hellerup', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (835, N'Helsinge', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (836, N'Helsingør', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (837, N'Hemmet', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (838, N'Henne', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (839, N'Herfølge', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (840, N'Herlev', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (841, N'Herlufmagle', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (842, N'Herning', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (843, N'Hesselager', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (844, N'Hillerød', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (845, N'Hinnerup', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (846, N'Hirtshals', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (847, N'Hjallerup', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (848, N'Hjerm', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (849, N'Hjørring', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (850, N'Hjortshøj', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (851, N'Hobro', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (852, N'Hobro', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (853, N'Højbjerg', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (854, N'Højby', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (855, N'Høje Taastrup', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (856, N'Højer', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (857, N'Højslev', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (858, N'Holbæk', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (859, N'Holeby', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (860, N'Holmegaard', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (861, N'Holstebro', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (862, N'Holsted', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (863, N'Holte', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (864, N'Høng', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (865, N'Horbelev', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (866, N'Hornbæk', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (867, N'Hørning', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (868, N'Hornslet', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (869, N'Hornsyld', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (870, N'Horsens', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (871, N'Hørsholm', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (872, N'Horslunde', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (873, N'Hørve', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (874, N'Hovborg', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (875, N'Hovedgård', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (876, N'Humble', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (877, N'Humlebæk', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (878, N'Hundested', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (879, N'Hundslund', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (880, N'Hurup Thy', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (881, N'Hurup Thy', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (882, N'Hvalsø', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (883, N'Hvide Sande', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (884, N'Hvidovre', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (885, N'Idestrup', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (886, N'Ikast', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (887, N'Ishøj', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (888, N'Jægerspris', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (889, N'Janderup Vestj', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (890, N'Jelling', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (891, N'Jerslev J', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (892, N'Jerslev Sjælland', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (893, N'Jerup', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (894, N'Jordrup', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (895, N'Juelsminde', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (896, N'Jyderup', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (897, N'Jyllinge', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (898, N'Jystrup Midtsj', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (899, N'Kalundborg', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (900, N'Kalvehave', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (901, N'Karby', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (902, N'Karise', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (903, N'Karlslunde', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (904, N'Karrebæksminde', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (905, N'Karup J', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (906, N'Kastrup', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (907, N'Kerteminde', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (908, N'Kettinge', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (909, N'Kibæk', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (910, N'Kirke Eskilstrup', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (911, N'Kirke Hyllinge', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (912, N'Kirke Såby', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (913, N'Kjellerup', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (914, N'Klampenborg', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (915, N'Klarup', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (916, N'Klemensker', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (917, N'Klippinge', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (918, N'Klovborg', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (919, N'Knebel', 1)
GO
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (920, N'København C', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (921, N'København K', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (922, N'København N', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (923, N'København NV', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (924, N'København Ø', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (925, N'København S', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (926, N'København SV', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (927, N'København V', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (928, N'Københavns Pakkecent', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (929, N'Køge', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (930, N'Kokkedal', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (931, N'Kolding', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (932, N'Kolind', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (933, N'Kongens Lyngby', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (934, N'Kongerslev', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (935, N'Korsør', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (936, N'Kruså', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (937, N'Kværndrup', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (938, N'Kvistgård', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (939, N'Læsø', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (940, N'Langå', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (941, N'Langebæk', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (942, N'Langeskov', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (943, N'Låsby', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (944, N'Lejre', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (945, N'Lem St', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (946, N'Lemming', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (947, N'Lemvig', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (948, N'Lille Skensved', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (949, N'Lintrup', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (950, N'Liseleje', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (951, N'Løgstør', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (952, N'Løgstrup', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (953, N'Løgumkloster', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (954, N'Løkken', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (955, N'Løsning', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (956, N'Lundby', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (957, N'Lunderskov', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (958, N'Lynge', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (959, N'Lystrup', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (960, N'Malling', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (961, N'Måløv', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (962, N'Mariager', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (963, N'Mariager', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (964, N'Maribo', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (965, N'Mårslet', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (966, N'Marslev', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (967, N'Marstal', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (968, N'Martofte', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (969, N'Melby', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (970, N'Mern', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (971, N'Mesinge', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (972, N'Middelfart', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (973, N'Millinge', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (974, N'Møldrup', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (975, N'Møldrup', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (976, N'Mørke', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (977, N'Mørkøv', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (978, N'Morud', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (979, N'Munke Bjergby', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (980, N'Munkebo', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (981, N'Nærum', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (982, N'Næstved', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (983, N'Nakskov', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (984, N'Nexø', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (985, N'Nibe', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (986, N'Nimtofte', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (987, N'Nivå', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (988, N'Nørager', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (989, N'Nordborg', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (990, N'Nordhavn', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (991, N'Nørre Aaby', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (992, N'Nørre Alslev', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (993, N'Nørre Asmindrup', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (994, N'Nørre Nebel', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (995, N'Nørre Nebel', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (996, N'Nørre Snede', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (997, N'Nørreballe', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (998, N'Nørresundby', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (999, N'Nyborg', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1000, N'Nykøbing F', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1001, N'Nykøbing M', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1002, N'Nykøbing Sj', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1003, N'Nyrup', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1004, N'Nysted', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1005, N'Odder', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1006, N'Odense C', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1007, N'Odense M', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1008, N'Odense N', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1009, N'Odense NØ', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1010, N'Odense NV', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1011, N'Odense S', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1012, N'Odense SØ', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1013, N'Odense SV', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1014, N'Odense V', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1015, N'Oksbøl', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1016, N'Ølgod', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1017, N'Ølgod', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1018, N'Ølsted', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1019, N'Ølstykke', 4)
GO
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1020, N'Ørbæk', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1021, N'Ørnhøj', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1022, N'Ørsted', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1023, N'Ørum Djurs', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1024, N'Østbirk', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1025, N'Øster Assels', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1026, N'Øster Ulslev', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1027, N'Østermarie', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1028, N'Østervrå', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1029, N'Otterup', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1030, N'Oure', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1031, N'Outrup', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1032, N'Padborg', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1033, N'Pandrup', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1034, N'Præstø', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1035, N'Randbøl', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1036, N'Randers C', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1037, N'Randers NØ', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1038, N'Randers NV', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1039, N'Randers SØ', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1040, N'Randers SV', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1041, N'Ranum', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1042, N'Rask Mølle', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1043, N'Redsted M', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1044, N'Regstrup', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1045, N'Ribe', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1046, N'Ringe', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1047, N'Ringkøbing', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1048, N'Ringsted', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1049, N'Risskov', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1050, N'Risskov Ø', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1051, N'Rødby', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1052, N'Rødding', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1053, N'Rødekro', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1054, N'Rødkærsbro', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1055, N'Rødovre', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1056, N'Rødvig Stevns', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1057, N'Rømø', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1058, N'Rønde', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1059, N'Rønne', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1060, N'Rønnede', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1061, N'Rørvig', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1062, N'Roskilde', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1063, N'Roskilde', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1064, N'Roslev', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1065, N'Rude', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1066, N'Rudkøbing', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1067, N'Ruds Vedby', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1068, N'Rungsted Kyst', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1069, N'Ry', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1070, N'Rynkeby', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1071, N'Ryomgård', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1072, N'Ryslinge', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1073, N'Sabro', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1074, N'Sæby', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1075, N'Sakskøbing', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1076, N'Saltum', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1077, N'Samsø', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1078, N'Sandved', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1079, N'Sejerø', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1080, N'Silkeborg', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1081, N'Sindal', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1082, N'Sjællands Odde', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1083, N'Sjølund', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1084, N'Skælskør', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1085, N'Skærbæk', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1086, N'Skævinge', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1087, N'Skagen', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1088, N'Skals', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1089, N'Skamby', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1090, N'Skanderborg', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1091, N'Skårup Fyn', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1092, N'Skibby', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1093, N'Skive', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1094, N'Skjern', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1095, N'Skodsborg', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1096, N'Skødstrup', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1097, N'Skørping', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1098, N'Skovlunde', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1099, N'Slagelse', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1100, N'Slangerup', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1101, N'Smørum', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1102, N'Snedsted', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1103, N'Snekkersten', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1104, N'Snertinge', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1105, N'Søborg', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1106, N'Søby Ærø', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1107, N'Solbjerg', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1108, N'Søllested', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1109, N'Solrød Strand', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1110, N'Sommersted', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1111, N'Sønder Felding', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1112, N'Sønder Omme', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1113, N'Sønder Omme', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1114, N'Sønder Stenderup', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1115, N'Sønderborg', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1116, N'Søndersø', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1117, N'Sorø', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1118, N'Sorring', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1119, N'Sørvad', 1)
GO
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1120, N'Spentrup', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1121, N'Spjald', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1122, N'Sporup', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1123, N'Spøttrup', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1124, N'Stakroge', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1125, N'Stakroge', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1126, N'Stege', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1127, N'Stenderup', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1128, N'Stenlille', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1129, N'Stenløse', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1130, N'Stenstrup', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1131, N'Stensved', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1132, N'Stoholm Jyll', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1133, N'Stokkemarke', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1134, N'Store Fuglede', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1135, N'Store Heddinge', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1136, N'Store Merløse', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1137, N'Storvorde', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1138, N'Stouby', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1139, N'Støvring', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1140, N'Strandby', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1141, N'Strøby', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1142, N'Struer', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1143, N'Stubbekøbing', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1144, N'Suldrup', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1145, N'Sulsted', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1146, N'Sunds', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1147, N'Svaneke', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1148, N'Svebølle', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1149, N'Svendborg', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1150, N'Svenstrup J', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1151, N'Svinninge', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1152, N'Sydals', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1153, N'Taastrup', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1154, N'Tappernøje', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1155, N'Tarm', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1156, N'Tarm', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1157, N'Tårs', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1158, N'Terndrup', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1159, N'Them', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1160, N'Thisted', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1161, N'Thorsø', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1162, N'Thyborøn', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1163, N'Thyholm', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1164, N'Tikøb', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1165, N'Tilst', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1166, N'Tim', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1167, N'Tinglev', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1168, N'Tistrup', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1169, N'Tisvildeleje', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1170, N'Tjæreborg', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1171, N'Tjele', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1172, N'Toftlund', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1173, N'Tølløse', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1174, N'Tommerup', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1175, N'Tønder', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1176, N'Toreby L', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1177, N'Torrig L', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1178, N'Tørring', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1179, N'Tørring', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1180, N'Tranbjerg J', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1181, N'Tranekær', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1182, N'Trige', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1183, N'Trustrup', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1184, N'Tune', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1185, N'Tureby', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1186, N'Tylstrup', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1187, N'Udland', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1188, N'Ugerløse', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1189, N'Uldum', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1190, N'Ulfborg', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1191, N'Ullerslev', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1192, N'Ulstrup', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1193, N'Vadum', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1194, N'Væggerløse', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1195, N'Værløse', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1196, N'Valby', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1197, N'Vallensbæk', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1198, N'Vallensbæk Strand', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1199, N'Vamdrup', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1200, N'Vandel', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1201, N'Vanløse', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1202, N'Varde', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1203, N'Vedbæk', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1204, N'Veflinge', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1205, N'Vejby', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1206, N'Vejen', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1207, N'Vejers Strand', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1208, N'Vejle', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1209, N'Vejle', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1210, N'Vejle Øst', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1211, N'Vejle Øst', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1212, N'Vejstrup', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1213, N'Veksø Sjælland', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1214, N'Veksø Sjælland', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1215, N'Vemb', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1216, N'Vemmelev', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1217, N'Vesløs', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1218, N'Vestbjerg', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1219, N'Vester Skerninge', 5)
GO
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1220, N'Vesterborg', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1221, N'Vestervig', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1222, N'Viborg', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1223, N'Viby J', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1224, N'Viby Sjælland', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1225, N'Videbæk', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1226, N'Vig', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1227, N'Vildbjerg', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1228, N'Vildbjerg', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1229, N'Vils', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1230, N'Vinderup', 1)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1231, N'Vipperød', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1232, N'Virum', 4)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1233, N'Vissenbjerg', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1234, N'Viuf', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1235, N'Vodskov', 3)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1236, N'Vojens', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1237, N'Vonge', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1238, N'Vorbasse', 5)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1239, N'Vordingborg', 2)
INSERT [dbo].[Town] ([Id], [Name], [RegionId]) VALUES (1240, N'Vrå', 3)
SET IDENTITY_INSERT [dbo].[Town] OFF
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (621, 78)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (622, 37)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (622, 38)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (622, 41)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (623, 51)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (624, 37)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (625, 37)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (626, 37)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (627, 37)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (628, 19)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (628, 43)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (628, 45)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (628, 47)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (629, 19)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (629, 43)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (629, 45)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (629, 47)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (630, 1)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (631, 1)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (632, 1)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (633, 45)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (633, 47)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (634, 80)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (634, 90)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (634, 91)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (635, 1)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (636, 79)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (637, 93)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (638, 97)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (638, 98)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (639, 78)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (639, 86)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (639, 96)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (640, 39)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (640, 40)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (641, 48)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (641, 52)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (642, 49)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (642, 56)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (642, 67)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (643, 9)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (644, 51)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (645, 88)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (646, 65)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (647, 9)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (648, 14)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (648, 19)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (649, 81)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (649, 97)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (650, 43)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (650, 45)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (651, 82)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (651, 97)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (652, 83)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (653, 38)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (653, 39)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (654, 36)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (655, 29)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (656, 90)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (657, 80)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (658, 94)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (659, 4)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (660, 9)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (660, 18)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (661, 98)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (662, 8)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (662, 17)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (663, 45)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (664, 89)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (665, 59)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (665, 61)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (665, 66)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (666, 9)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (666, 18)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (667, 48)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (667, 50)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (667, 55)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (668, 27)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (669, 3)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (670, 1)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (671, 46)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (672, 86)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (672, 96)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (673, 97)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (674, 81)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (674, 99)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (675, 39)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (675, 40)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (676, 49)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (676, 59)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (676, 75)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (677, 25)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (678, 2)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (678, 19)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (679, 88)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (680, 97)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (681, 41)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (682, 91)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (682, 93)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (683, 32)
GO
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (684, 91)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (685, 36)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (686, 78)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (687, 7)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (687, 14)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (688, 99)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (689, 36)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (690, 25)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (690, 30)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (690, 31)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (691, 8)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (692, 1)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (693, 6)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (693, 14)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (694, 82)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (695, 4)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (695, 7)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (695, 99)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (696, 4)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (696, 7)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (696, 99)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (697, 96)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (698, 96)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (699, 99)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (700, 90)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (701, 94)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (702, 83)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (703, 52)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (704, 52)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (704, 70)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (705, 38)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (705, 41)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (706, 72)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (707, 98)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (708, 41)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (709, 6)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (709, 7)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (709, 14)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (710, 78)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (711, 60)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (712, 86)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (712, 88)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (713, 87)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (714, 28)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (714, 32)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (715, 27)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (716, 3)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (717, 24)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (717, 34)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (718, 54)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (718, 76)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (719, 38)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (719, 39)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (720, 63)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (721, 38)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (721, 39)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (722, 60)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (723, 80)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (724, 18)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (725, 1)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (726, 94)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (727, 88)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (727, 99)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (728, 90)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (729, 7)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (729, 99)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (730, 7)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (730, 99)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (731, 7)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (731, 14)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (732, 27)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (733, 44)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (734, 82)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (735, 82)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (736, 82)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (737, 82)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (738, 24)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (739, 22)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (740, 56)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (740, 65)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (741, 83)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (742, 84)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (743, 29)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (744, 47)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (745, 49)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (745, 55)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (745, 59)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (746, 11)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (746, 43)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (747, 11)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (747, 43)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (748, 2)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (748, 14)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (749, 20)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (749, 28)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (750, 20)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (751, 27)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (752, 83)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (752, 87)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (753, 30)
GO
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (753, 34)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (754, 41)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (755, 3)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (755, 6)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (756, 24)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (756, 29)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (757, 82)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (757, 98)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (758, 56)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (758, 65)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (758, 67)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (759, 85)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (759, 99)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (760, 57)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (760, 72)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (761, 57)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (761, 72)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (762, 39)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (762, 40)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (763, 58)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (763, 64)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (763, 67)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (764, 63)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (764, 64)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (765, 92)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (766, 46)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (767, 28)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (767, 32)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (768, 16)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (769, 99)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (770, 31)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (771, 1)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (771, 15)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (772, 37)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (773, 22)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (774, 19)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (774, 47)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (775, 19)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (775, 47)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (776, 6)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (776, 15)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (777, 90)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (778, 60)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (778, 61)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (779, 98)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (780, 63)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (781, 83)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (781, 95)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (782, 23)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (783, 37)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (783, 45)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (784, 3)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (784, 99)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (785, 3)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (785, 99)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (786, 11)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (787, 14)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (788, 80)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (789, 98)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (790, 9)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (791, 48)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (791, 52)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (791, 62)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (792, 28)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (793, 82)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (793, 98)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (794, 24)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (795, 67)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (796, 63)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (796, 65)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (796, 67)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (797, 86)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (798, 94)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (799, 82)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (800, 9)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (801, 21)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (802, 29)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (803, 81)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (803, 97)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (803, 99)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (804, 95)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (805, 51)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (805, 53)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (806, 95)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (807, 22)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (808, 80)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (809, 86)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (809, 88)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (810, 4)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (810, 5)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (810, 19)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (811, 2)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (812, 43)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (812, 45)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (813, 37)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (813, 38)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (814, 2)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (814, 14)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (815, 7)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (815, 14)
GO
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (816, 46)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (817, 8)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (818, 20)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (818, 35)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (819, 1)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (820, 90)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (821, 27)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (822, 51)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (823, 20)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (823, 28)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (823, 30)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (824, 1)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (825, 31)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (825, 33)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (826, 11)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (826, 43)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (827, 11)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (827, 43)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (828, 21)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (828, 31)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (828, 68)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (828, 71)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (829, 21)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (829, 31)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (829, 68)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (829, 71)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (830, 3)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (831, 86)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (831, 88)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (832, 81)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (833, 65)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (834, 60)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (834, 72)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (835, 63)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (835, 67)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (836, 65)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (837, 12)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (838, 97)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (839, 25)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (840, 50)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (840, 61)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (840, 66)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (840, 74)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (841, 20)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (841, 28)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (842, 4)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (842, 7)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (842, 12)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (843, 92)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (843, 95)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (844, 49)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (844, 56)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (844, 67)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (845, 1)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (845, 2)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (846, 40)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (847, 37)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (847, 38)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (848, 5)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (848, 17)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (849, 40)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (850, 1)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (851, 19)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (851, 43)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (851, 45)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (852, 19)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (852, 43)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (852, 45)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (853, 1)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (854, 29)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (855, 68)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (856, 96)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (857, 16)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (857, 19)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (858, 23)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (859, 27)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (860, 20)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (860, 28)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (861, 4)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (861, 5)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (861, 17)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (862, 82)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (862, 98)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (863, 73)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (863, 75)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (864, 24)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (864, 32)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (865, 22)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (866, 63)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (866, 65)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (867, 1)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (867, 15)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (868, 1)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (868, 18)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (869, 3)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (869, 6)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (870, 3)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (870, 6)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (871, 56)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (871, 69)
GO
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (871, 75)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (872, 27)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (873, 24)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (873, 29)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (874, 81)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (874, 97)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (874, 98)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (875, 6)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (876, 89)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (877, 56)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (877, 65)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (878, 64)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (879, 10)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (880, 17)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (880, 46)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (881, 17)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (881, 46)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (882, 23)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (882, 26)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (882, 30)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (883, 12)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (884, 52)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (884, 70)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (885, 22)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (886, 7)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (887, 71)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (888, 58)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (889, 97)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (890, 99)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (891, 38)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (891, 40)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (892, 24)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (893, 39)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (894, 88)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (895, 3)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (896, 23)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (896, 24)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (897, 31)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (898, 25)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (898, 30)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (899, 24)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (900, 36)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (901, 44)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (902, 20)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (902, 35)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (903, 21)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (903, 33)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (904, 28)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (905, 4)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (905, 14)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (905, 19)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (906, 72)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (906, 76)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (907, 87)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (908, 22)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (909, 4)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (909, 12)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (910, 23)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (910, 30)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (911, 26)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (912, 26)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (913, 14)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (913, 19)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (914, 60)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (914, 73)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (915, 37)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (916, 51)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (917, 35)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (918, 6)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (918, 7)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (919, 18)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (920, 72)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (921, 72)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (922, 57)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (922, 72)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (923, 72)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (924, 72)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (925, 72)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (925, 76)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (926, 72)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (927, 72)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (927, 76)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (928, 52)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (929, 25)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (929, 35)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (930, 56)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (930, 69)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (931, 88)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (932, 9)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (932, 18)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (933, 60)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (933, 61)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (933, 73)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (933, 75)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (934, 37)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (934, 45)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (935, 32)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (936, 78)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (937, 83)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (937, 95)
GO
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (938, 56)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (938, 65)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (939, 42)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (940, 2)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (940, 11)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (941, 36)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (942, 83)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (942, 87)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (942, 92)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (943, 15)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (944, 25)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (944, 26)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (944, 31)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (945, 12)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (946, 14)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (947, 8)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (948, 25)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (948, 31)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (948, 33)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (949, 98)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (950, 64)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (951, 41)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (951, 47)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (952, 19)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (953, 96)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (954, 38)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (954, 40)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (954, 41)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (955, 3)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (956, 28)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (956, 36)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (957, 88)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (958, 49)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (958, 55)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (958, 59)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (958, 67)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (959, 1)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (960, 1)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (960, 10)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (961, 50)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (962, 11)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (962, 43)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (963, 11)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (963, 43)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (964, 22)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (964, 27)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (965, 1)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (966, 87)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (967, 79)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (968, 87)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (969, 64)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (970, 36)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (971, 87)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (972, 90)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (973, 80)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (973, 83)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (974, 19)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (974, 43)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (975, 19)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (975, 43)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (976, 18)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (977, 23)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (978, 91)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (979, 32)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (979, 34)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (980, 87)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (980, 93)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (981, 75)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (982, 28)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (983, 27)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (984, 51)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (985, 37)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (985, 45)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (985, 47)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (986, 9)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (986, 18)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (987, 56)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (988, 45)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (988, 47)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (989, 88)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (989, 94)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (990, 72)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (991, 90)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (992, 22)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (993, 29)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (994, 12)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (994, 97)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (995, 12)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (995, 97)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (996, 3)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (996, 7)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (997, 27)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (998, 37)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (999, 92)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1000, 22)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1001, 44)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1002, 29)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1003, 23)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1003, 34)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1004, 22)
GO
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1005, 1)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1005, 10)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1006, 93)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1007, 93)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1008, 91)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1008, 93)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1009, 93)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1010, 93)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1011, 93)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1012, 93)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1013, 93)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1014, 93)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1015, 97)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1016, 12)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1016, 97)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1017, 12)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1017, 97)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1018, 64)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1018, 67)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1019, 55)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1020, 83)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1020, 92)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1021, 4)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1021, 12)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1022, 9)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1023, 9)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1024, 6)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1025, 44)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1026, 22)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1026, 27)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1027, 51)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1028, 38)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1028, 39)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1028, 40)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1029, 91)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1030, 95)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1031, 97)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1032, 78)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1033, 41)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1034, 36)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1035, 99)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1036, 11)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1037, 11)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1038, 11)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1039, 2)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1039, 11)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1040, 2)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1040, 11)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1041, 47)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1042, 3)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1043, 44)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1044, 23)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1045, 82)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1046, 83)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1047, 12)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1048, 20)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1048, 23)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1048, 25)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1048, 28)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1048, 30)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1048, 34)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1049, 1)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1050, 1)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1051, 27)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1052, 86)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1052, 98)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1053, 78)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1054, 14)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1054, 19)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1055, 70)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1055, 72)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1055, 74)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1056, 35)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1057, 96)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1058, 18)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1059, 51)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1060, 20)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1060, 28)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1061, 29)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1062, 26)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1062, 31)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1062, 68)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1063, 26)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1063, 31)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1063, 68)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1064, 16)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1065, 28)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1065, 32)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1066, 89)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1067, 24)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1067, 34)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1068, 69)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1069, 6)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1069, 14)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1069, 15)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1070, 87)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1071, 9)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1071, 18)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1072, 83)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1073, 1)
GO
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1073, 2)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1073, 15)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1074, 39)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1075, 22)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1076, 41)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1077, 13)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1078, 28)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1078, 32)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1079, 24)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1080, 7)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1080, 14)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1080, 15)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1081, 39)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1081, 40)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1082, 29)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1083, 88)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1084, 32)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1085, 96)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1086, 64)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1086, 67)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1087, 39)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1088, 19)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1089, 91)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1090, 1)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1090, 6)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1090, 10)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1090, 15)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1091, 95)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1092, 58)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1093, 5)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1093, 16)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1093, 19)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1094, 12)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1095, 75)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1096, 1)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1097, 45)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1098, 50)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1098, 66)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1099, 24)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1099, 28)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1099, 32)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1099, 34)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1100, 49)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1100, 55)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1100, 58)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1100, 67)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1101, 55)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1102, 46)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1103, 65)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1104, 23)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1104, 24)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1105, 61)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1105, 72)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1106, 79)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1107, 1)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1108, 27)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1109, 33)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1110, 86)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1110, 88)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1111, 4)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1111, 12)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1112, 4)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1112, 81)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1113, 4)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1113, 81)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1114, 88)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1115, 94)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1116, 91)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1117, 30)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1117, 32)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1117, 34)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1118, 2)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1118, 14)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1119, 4)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1120, 11)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1121, 12)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1122, 2)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1122, 14)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1122, 15)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1123, 16)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1124, 4)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1124, 12)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1124, 81)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1125, 4)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1125, 12)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1125, 81)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1126, 36)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1127, 3)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1127, 6)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1128, 23)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1128, 34)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1129, 55)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1130, 83)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1130, 95)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1131, 36)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1132, 19)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1133, 27)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1134, 24)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1135, 35)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1136, 23)
GO
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1136, 30)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1136, 34)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1137, 37)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1138, 3)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1139, 37)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1139, 45)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1140, 39)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1141, 35)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1142, 5)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1142, 8)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1142, 17)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1143, 22)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1144, 45)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1144, 47)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1145, 37)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1145, 41)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1146, 4)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1146, 7)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1147, 51)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1148, 23)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1148, 24)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1149, 95)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1150, 37)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1150, 45)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1151, 23)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1152, 94)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1153, 68)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1154, 20)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1154, 28)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1154, 36)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1155, 12)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1155, 97)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1156, 12)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1156, 97)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1157, 39)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1157, 40)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1158, 45)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1159, 14)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1160, 46)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1161, 2)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1161, 14)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1162, 8)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1163, 17)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1164, 65)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1165, 1)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1166, 12)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1167, 78)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1168, 97)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1169, 63)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1170, 82)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1171, 19)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1172, 86)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1172, 96)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1173, 23)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1173, 26)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1174, 80)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1175, 96)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1176, 22)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1177, 27)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1178, 3)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1178, 7)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1178, 99)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1179, 3)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1179, 7)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1179, 99)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1180, 1)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1181, 89)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1182, 1)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1182, 2)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1183, 9)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1183, 18)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1184, 21)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1184, 31)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1185, 20)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1185, 25)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1185, 35)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1186, 37)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1186, 38)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1186, 41)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1187, 72)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1188, 23)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1189, 3)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1190, 4)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1190, 5)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1190, 8)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1190, 12)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1191, 87)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1191, 92)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1192, 2)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1192, 11)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1192, 19)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1193, 37)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1193, 41)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1194, 22)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1195, 50)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1195, 55)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1195, 59)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1195, 61)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1195, 73)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1196, 57)
GO
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1196, 72)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1197, 77)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1198, 77)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1199, 88)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1200, 99)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1201, 57)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1201, 72)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1202, 97)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1203, 69)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1203, 75)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1204, 91)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1205, 63)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1206, 98)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1207, 97)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1208, 3)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1208, 99)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1209, 3)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1209, 99)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1210, 3)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1210, 99)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1211, 3)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1211, 99)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1212, 95)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1213, 31)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1213, 55)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1214, 31)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1214, 55)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1215, 5)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1215, 8)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1216, 32)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1217, 46)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1218, 37)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1219, 83)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1219, 95)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1220, 27)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1221, 46)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1222, 14)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1222, 19)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1223, 1)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1224, 25)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1224, 31)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1225, 12)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1226, 29)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1227, 4)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1227, 12)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1227, 72)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1228, 4)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1228, 12)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1228, 72)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1229, 44)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1230, 4)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1230, 5)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1230, 16)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1230, 17)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1231, 23)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1232, 73)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1232, 75)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1233, 80)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1233, 91)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1233, 93)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1234, 88)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1234, 99)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1235, 37)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1236, 86)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1237, 99)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1238, 81)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1238, 98)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1238, 99)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1239, 36)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1240, 38)
INSERT [dbo].[TownMunicipality] ([TownId], [MunicipalityId]) VALUES (1240, 40)
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (621, N'6200')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (622, N'9440')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (623, N'3720')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (624, N'9000')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (625, N'9220')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (626, N'9210')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (627, N'9200')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (628, N'9620')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (629, N'9620')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (630, N'8000')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (631, N'8200')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (632, N'8210')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (633, N'9600')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (634, N'5560')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (635, N'8230')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (636, N'5970')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (637, N'5320')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (638, N'6753')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (639, N'6534')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (640, N'9982')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (641, N'2620')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (642, N'3450')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (643, N'8961')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (644, N'3770')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (645, N'6051')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (646, N'3140')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (647, N'8592')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (648, N'8643')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (649, N'6823')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (650, N'9510')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (651, N'6818')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (652, N'5792')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (653, N'9340')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (654, N'4792')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (655, N'4550')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (656, N'5466')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (657, N'5610')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (658, N'6440')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (659, N'7490')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (660, N'8963')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (661, N'6622')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (662, N'7660')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (663, N'9574')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (664, N'5935')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (665, N'2880')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (666, N'8444')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (667, N'2750')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (668, N'4941')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (669, N'7150')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (670, N'8330')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (671, N'7755')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (672, N'6541')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (673, N'6852')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (674, N'7190')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (675, N'9881')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (676, N'3460')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (677, N'4632')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (678, N'8850')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (679, N'6091')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (680, N'6857')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (681, N'9492')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (682, N'5491')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (683, N'4242')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (684, N'5400')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (685, N'4793')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (686, N'6392')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (687, N'7441')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (688, N'7080')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (689, N'4791')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (690, N'4140')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (691, N'7650')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (692, N'8220')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (693, N'8740')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (694, N'6740')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (695, N'7330')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (696, N'7330')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (697, N'6535')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (698, N'6261')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (699, N'7182')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (700, N'5464')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (701, N'6310')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (702, N'5672')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (703, N'2605')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (704, N'2660')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (705, N'9700')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (706, N'2700')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (707, N'6650')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (708, N'9460')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (709, N'8654')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (710, N'6372')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (711, N'2920')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (712, N'6070')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (713, N'5380')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (714, N'4261')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (715, N'4983')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (716, N'8721')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (717, N'4293')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (718, N'2791')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (719, N'9330')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (720, N'3120')
GO
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (721, N'9352')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (722, N'2870')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (723, N'5631')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (724, N'8400')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (725, N'8250')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (726, N'6320')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (727, N'6040')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (728, N'5592')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (729, N'7361')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (730, N'7361')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (731, N'7442')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (732, N'4895')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (733, N'7950')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (734, N'6700')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (735, N'6715')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (736, N'6705')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (737, N'6710')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (738, N'4593')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (739, N'4863')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (740, N'3060')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (741, N'5600')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (742, N'6720')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (743, N'4540')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (744, N'9640')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (745, N'3520')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (746, N'8990')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (747, N'8990')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (748, N'8882')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (749, N'4640')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (750, N'4654')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (751, N'4944')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (752, N'5863')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (753, N'4173')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (754, N'9690')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (755, N'8762')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (756, N'4591')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (757, N'6683')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (758, N'3480')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (759, N'7000')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (759, N'7007')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (760, N'2000')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1800')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1801')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1802')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1803')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1804')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1805')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1806')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1807')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1808')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1809')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1810')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1811')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1812')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1813')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1814')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1815')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1816')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1817')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1818')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1819')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1820')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1822')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1823')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1824')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1825')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1826')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1827')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1828')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1829')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1850')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1851')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1852')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1853')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1854')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1855')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1856')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1857')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1860')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1861')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1862')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1863')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1864')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1865')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1866')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1867')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1868')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1870')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1871')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1872')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1873')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1874')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1875')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1876')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1877')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1878')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1879')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1900')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1901')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1902')
GO
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1903')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1904')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1905')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1906')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1908')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1909')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1910')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1911')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1912')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1913')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1914')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1915')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1916')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1917')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1920')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1921')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1922')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1923')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1924')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1925')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1926')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1927')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1928')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1950')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1951')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1952')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1953')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1954')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1955')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1956')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1957')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1958')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1959')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1960')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1961')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1962')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1963')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1964')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1965')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1966')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1967')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1970')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1971')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1972')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1973')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (761, N'1974')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (762, N'9900')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (763, N'3600')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (764, N'3300')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (765, N'5871')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (766, N'7741')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (767, N'4250')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (768, N'7884')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (769, N'7321')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (770, N'4621')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (771, N'8464')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (772, N'9362')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (773, N'4874')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (774, N'9631')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (775, N'9631')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (776, N'8751')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (777, N'5591')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (778, N'2820')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (779, N'6621')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (780, N'3250')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (781, N'5854')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (782, N'4532')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (783, N'9260')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (784, N'7323')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (785, N'7323')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (786, N'8983')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (787, N'8883')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (788, N'5620')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (789, N'6752')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (790, N'8585')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (791, N'2600')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (792, N'4171')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (793, N'6690')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (794, N'4281')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (795, N'3330')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (796, N'3230')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (797, N'6510')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (798, N'6300')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (799, N'6771')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (800, N'8500')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (801, N'2670')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (802, N'4571')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (803, N'7200')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (804, N'5892')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (805, N'3760')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (806, N'5884')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (807, N'4862')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (808, N'5683')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (809, N'6100')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (810, N'7540')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (811, N'8370')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (812, N'9560')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (813, N'9370')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (814, N'8450')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (815, N'7362')
GO
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (816, N'7730')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (817, N'7673')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (818, N'4652')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (819, N'8462')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (820, N'5463')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (821, N'4912')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (822, N'3790')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (823, N'4690')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (824, N'8361')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (825, N'4622')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (826, N'8970')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (827, N'8970')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (828, N'2640')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (829, N'2640')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (830, N'8722')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (831, N'6094')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (832, N'7250')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (833, N'3150')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (834, N'2900')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (835, N'3200')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (836, N'3000')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (837, N'6893')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (838, N'6854')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (839, N'4681')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (840, N'2730')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (841, N'4160')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (842, N'7400')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (843, N'5874')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (844, N'3400')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (845, N'8382')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (846, N'9850')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (847, N'9320')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (848, N'7560')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (849, N'9800')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (850, N'8530')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (851, N'9500')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (852, N'9500')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (853, N'8270')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (854, N'4573')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (855, N'800')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (856, N'6280')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (857, N'7840')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (858, N'4300')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (859, N'4960')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (860, N'4684')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (861, N'7500')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (862, N'6670')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (863, N'2840')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (864, N'4270')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (865, N'4871')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (866, N'3100')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (867, N'8362')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (868, N'8543')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (869, N'8783')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (870, N'8700')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (871, N'2970')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (872, N'4913')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (873, N'4534')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (874, N'6682')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (875, N'8732')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (876, N'5932')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (877, N'3050')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (878, N'3390')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (879, N'8350')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (880, N'7760')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (881, N'7760')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (882, N'4330')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (883, N'6960')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (884, N'2650')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (885, N'4872')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (886, N'7430')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (887, N'2635')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (888, N'3630')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (889, N'6851')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (890, N'7300')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (891, N'9740')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (892, N'4490')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (893, N'9981')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (894, N'6064')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (895, N'7130')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (896, N'4450')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (897, N'4040')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (898, N'4174')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (899, N'4400')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (900, N'4771')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (901, N'7960')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (902, N'4653')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (903, N'2690')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (904, N'4736')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (905, N'7470')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (906, N'2770')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (907, N'5300')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (908, N'4892')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (909, N'6933')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (910, N'4360')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (911, N'4070')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (912, N'4060')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (913, N'8620')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (914, N'2930')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (915, N'9270')
GO
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (916, N'3782')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (917, N'4672')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (918, N'8765')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (919, N'8420')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (920, N'900')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (920, N'999')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1000')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1050')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1051')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1052')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1053')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1054')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1055')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1056')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1057')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1058')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1059')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1060')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1061')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1062')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1063')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1064')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1065')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1066')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1067')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1068')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1069')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1070')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1071')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1072')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1073')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1074')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1092')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1093')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1095')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1098')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1100')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1101')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1102')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1103')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1104')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1105')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1106')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1107')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1110')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1111')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1112')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1113')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1114')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1115')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1116')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1117')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1118')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1119')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1120')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1121')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1122')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1123')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1124')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1125')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1126')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1127')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1128')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1129')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1130')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1131')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1140')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1147')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1148')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1150')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1151')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1152')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1153')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1154')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1155')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1156')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1157')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1158')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1159')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1160')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1161')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1162')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1164')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1165')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1166')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1167')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1168')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1169')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1170')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1171')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1172')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1173')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1174')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1175')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1200')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1201')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1202')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1203')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1204')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1205')
GO
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1206')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1207')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1208')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1209')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1210')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1211')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1213')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1214')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1215')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1216')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1217')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1218')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1219')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1220')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1221')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1240')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1250')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1251')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1253')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1254')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1255')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1256')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1257')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1259')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1260')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1261')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1263')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1264')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1265')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1266')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1267')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1268')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1270')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1271')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1291')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1300')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1301')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1302')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1303')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1304')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1306')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1307')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1308')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1309')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1310')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1311')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1312')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1313')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1314')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1315')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1316')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1317')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1318')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1319')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1320')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1321')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1322')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1323')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1324')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1325')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1326')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1327')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1328')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1329')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1350')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1352')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1353')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1354')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1355')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1356')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1357')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1358')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1359')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1360')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1361')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1362')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1363')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1364')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1365')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1366')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1367')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1368')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1369')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1370')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1371')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1400')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1401')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1402')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1403')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1406')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1407')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1408')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1409')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1410')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1411')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1412')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1413')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1414')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1415')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1416')
GO
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1417')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1418')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1419')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1420')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1421')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1422')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1423')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1424')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1425')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1426')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1427')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1428')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1429')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1430')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1431')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1432')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1433')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1434')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1435')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1436')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1437')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1438')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1439')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1440')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1441')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1448')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1450')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1451')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1452')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1453')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1454')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1455')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1456')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1457')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1458')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1459')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1460')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1462')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1463')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1464')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1466')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1467')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1468')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1470')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1471')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (921, N'1472')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (922, N'2200')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (923, N'2400')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (924, N'2100')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (925, N'2300')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (926, N'2450')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1500')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1532')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1533')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1550')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1551')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1552')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1553')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1554')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1555')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1556')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1557')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1558')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1559')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1560')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1561')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1562')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1563')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1564')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1566')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1567')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1568')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1569')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1570')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1571')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1572')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1573')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1574')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1575')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1576')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1577')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1592')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1599')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1600')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1601')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1602')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1603')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1604')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1606')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1607')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1608')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1609')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1610')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1611')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1612')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1613')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1614')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1615')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1616')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1617')
GO
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1618')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1619')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1620')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1621')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1622')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1623')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1624')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1630')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1631')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1632')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1633')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1634')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1635')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1650')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1651')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1652')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1653')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1654')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1655')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1656')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1657')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1658')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1659')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1660')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1661')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1662')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1663')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1664')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1665')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1666')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1667')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1668')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1669')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1670')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1671')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1672')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1673')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1674')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1675')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1676')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1677')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1699')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1700')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1701')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1702')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1703')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1704')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1705')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1706')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1707')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1708')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1709')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1710')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1711')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1712')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1714')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1715')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1716')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1717')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1718')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1719')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1720')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1721')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1722')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1723')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1724')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1725')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1726')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1727')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1728')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1729')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1730')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1731')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1732')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1733')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1734')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1735')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1736')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1737')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1738')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1739')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1749')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1750')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1751')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1752')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1753')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1754')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1755')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1756')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1757')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1758')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1759')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1760')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1761')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1762')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1763')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1764')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1765')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1766')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1770')
GO
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1771')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1772')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1773')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1774')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1775')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1777')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1780')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1785')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1786')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1787')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1790')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (927, N'1799')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (928, N'917')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (929, N'4600')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (930, N'2980')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (931, N'6000')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (932, N'8560')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (933, N'2800')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (934, N'9293')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (935, N'4220')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (936, N'6340')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (937, N'5772')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (938, N'3490')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (939, N'9940')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (940, N'8870')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (941, N'4772')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (942, N'5550')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (943, N'8670')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (944, N'4320')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (945, N'6940')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (946, N'8632')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (947, N'7620')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (948, N'4623')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (949, N'6660')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (950, N'3360')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (951, N'9670')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (952, N'8831')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (953, N'6240')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (954, N'9480')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (955, N'8723')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (956, N'4750')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (957, N'6640')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (958, N'3540')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (959, N'8520')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (960, N'8340')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (961, N'2760')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (962, N'9550')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (963, N'9550')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (964, N'4930')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (965, N'8320')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (966, N'5290')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (967, N'5960')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (968, N'5390')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (969, N'3370')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (970, N'4735')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (971, N'5370')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (972, N'5500')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (973, N'5642')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (974, N'9632')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (975, N'9632')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (976, N'8544')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (977, N'4440')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (978, N'5462')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (979, N'4190')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (980, N'5330')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (981, N'2850')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (982, N'4700')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (983, N'4900')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (984, N'3730')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (985, N'9240')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (986, N'8581')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (987, N'2990')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (988, N'9610')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (989, N'6430')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (990, N'2150')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (991, N'5580')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (992, N'4840')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (993, N'4572')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (994, N'6830')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (995, N'6830')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (996, N'8766')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (997, N'4951')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (998, N'9400')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (999, N'5800')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1000, N'4800')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1001, N'7900')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1002, N'4500')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1003, N'4296')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1004, N'4880')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1005, N'8300')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1006, N'5000')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1007, N'5230')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1008, N'5270')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1009, N'5240')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1010, N'5210')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1011, N'5260')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1012, N'5220')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1013, N'5250')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1014, N'5200')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1015, N'6840')
GO
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1016, N'6870')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1017, N'6870')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1018, N'3310')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1019, N'3650')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1020, N'5853')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1021, N'6973')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1022, N'8950')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1023, N'8586')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1024, N'8752')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1025, N'7990')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1026, N'4894')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1027, N'3751')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1028, N'9750')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1029, N'5450')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1030, N'5883')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1031, N'6855')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1032, N'6330')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1033, N'9490')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1034, N'4720')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1035, N'7183')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1036, N'8900')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1037, N'8930')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1038, N'8920')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1039, N'8960')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1040, N'8940')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1041, N'9681')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1042, N'8763')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1043, N'7970')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1044, N'4420')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1045, N'6760')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1046, N'5750')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1047, N'6950')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1048, N'4100')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1049, N'8240')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1050, N'8245')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1051, N'4970')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1052, N'6630')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1053, N'6230')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1054, N'8840')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1055, N'2610')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1056, N'4673')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1057, N'6792')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1058, N'8410')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1059, N'3700')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1060, N'4683')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1061, N'4581')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1062, N'4000')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1063, N'4000')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1064, N'7870')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1065, N'4243')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1066, N'5900')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1067, N'4291')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1068, N'2960')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1069, N'8680')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1070, N'5350')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1071, N'8550')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1072, N'5856')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1073, N'8471')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1074, N'9300')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1075, N'4990')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1076, N'9493')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1077, N'8305')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1078, N'4262')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1079, N'4592')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1080, N'8600')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1081, N'9870')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1082, N'4583')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1083, N'6093')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1084, N'4230')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1085, N'6780')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1086, N'3320')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1087, N'9990')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1088, N'8832')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1089, N'5485')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1090, N'8660')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1091, N'5881')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1092, N'4050')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1093, N'7800')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1094, N'6900')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1095, N'2942')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1096, N'8541')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1097, N'9520')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1098, N'2740')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1099, N'4200')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1100, N'3550')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1101, N'2765')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1102, N'7752')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1103, N'3070')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1104, N'4460')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1105, N'2860')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1106, N'5985')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1107, N'8355')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1108, N'4920')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1109, N'2680')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1110, N'6560')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1111, N'7280')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1112, N'7260')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1113, N'7260')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1114, N'6092')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1115, N'6400')
GO
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1116, N'5471')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1117, N'4180')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1118, N'8641')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1119, N'7550')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1120, N'8981')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1121, N'6971')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1122, N'8472')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1123, N'7860')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1124, N'7270')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1125, N'7270')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1126, N'4780')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1127, N'8781')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1128, N'4295')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1129, N'3660')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1130, N'5771')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1131, N'4773')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1132, N'7850')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1133, N'4952')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1134, N'4480')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1135, N'4660')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1136, N'4370')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1137, N'9280')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1138, N'7140')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1139, N'9530')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1140, N'9970')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1141, N'4671')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1142, N'7600')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1143, N'4850')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1144, N'9541')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1145, N'9381')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1146, N'7451')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1147, N'3740')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1148, N'4470')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1149, N'5700')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1150, N'9230')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1151, N'4520')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1152, N'6470')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1153, N'2630')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1154, N'4733')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1155, N'6880')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1156, N'6880')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1157, N'9830')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1158, N'9575')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1159, N'8653')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1160, N'7700')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1161, N'8881')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1162, N'7680')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1163, N'7790')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1164, N'3080')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1165, N'8381')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1166, N'6980')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1167, N'6360')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1168, N'6862')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1169, N'3220')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1170, N'6731')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1171, N'8830')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1172, N'6520')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1173, N'4340')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1174, N'5690')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1175, N'6270')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1176, N'4891')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1177, N'4943')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1178, N'7160')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1179, N'7160')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1180, N'8310')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1181, N'5953')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1182, N'8380')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1183, N'8570')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1184, N'4030')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1185, N'4682')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1186, N'9382')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1187, N'960')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1188, N'4350')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1189, N'7171')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1190, N'6990')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1191, N'5540')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1192, N'8860')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1193, N'9430')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1194, N'4873')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1195, N'3500')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1196, N'2500')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1197, N'2625')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1198, N'2665')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1199, N'6580')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1200, N'7184')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1201, N'2720')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1202, N'6800')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1203, N'2950')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1204, N'5474')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1205, N'3210')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1206, N'6600')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1207, N'6853')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1208, N'7100')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1209, N'7100')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1210, N'7120')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1211, N'7120')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1212, N'5882')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1213, N'3670')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1214, N'3670')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1215, N'7570')
GO
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1216, N'4241')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1217, N'7742')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1218, N'9380')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1219, N'5762')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1220, N'4953')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1221, N'7770')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1222, N'8800')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1223, N'8260')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1224, N'4130')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1225, N'6920')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1226, N'4560')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1227, N'7480')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1228, N'7480')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1229, N'7980')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1230, N'7830')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1231, N'4390')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1232, N'2830')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1233, N'5492')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1234, N'6052')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1235, N'9310')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1236, N'6500')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1237, N'7173')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1238, N'6623')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1239, N'4760')
INSERT [dbo].[TownPostalcode] ([TownId], [Postalcode]) VALUES (1240, N'9760')
/****** Object:  Index [IX_AuthenticationIdentityKey_AuthenticationIdentity]    Script Date: 1/13/2015 1:35:00 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_AuthenticationIdentityKey_AuthenticationIdentity] ON [dbo].[AuthenticationIdentityKey_AuthenticationIdentity]
(
	[AuthenticationIdentityId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ContactGroup]    Script Date: 1/13/2015 1:35:00 PM ******/
CREATE NONCLUSTERED INDEX [IX_ContactGroup] ON [dbo].[Contact_ContactGroup]
(
	[ContactGroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ContactId]    Script Date: 1/13/2015 1:35:00 PM ******/
CREATE NONCLUSTERED INDEX [IX_ContactId] ON [dbo].[Contact_ContactType]
(
	[ContactId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Patient]    Script Date: 1/13/2015 1:35:00 PM ******/
CREATE NONCLUSTERED INDEX [IX_Patient] ON [dbo].[Contact_Patient]
(
	[PatientId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_ContactGroupName]    Script Date: 1/13/2015 1:35:00 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_ContactGroupName] ON [dbo].[ContactGroup]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_ContactInformationType]    Script Date: 1/13/2015 1:35:00 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_ContactInformationType] ON [dbo].[ContactInformationType]
(
	[TypeName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_ContactTypeName]    Script Date: 1/13/2015 1:35:00 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_ContactTypeName] ON [dbo].[ContactType]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Employee_ContactInformation]    Script Date: 1/13/2015 1:35:00 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Employee_ContactInformation] ON [dbo].[Employee_ContactInformation]
(
	[ContactInformationId] ASC,
	[EmployeeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Medication]    Script Date: 1/13/2015 1:35:00 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Medication] ON [dbo].[Medication]
(
	[MedicationBrandId] ASC,
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_MedicationBrand]    Script Date: 1/13/2015 1:35:00 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_MedicationBrand] ON [dbo].[MedicationBrand]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Patient_ContactInformation]    Script Date: 1/13/2015 1:35:00 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Patient_ContactInformation] ON [dbo].[Patient_ContactInformation]
(
	[ContactInformationId] ASC,
	[PatientId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_PatientDescription]    Script Date: 1/13/2015 1:35:00 PM ******/
CREATE NONCLUSTERED INDEX [IX_PatientDescription] ON [dbo].[PatientDescription]
(
	[PatientId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Address]  WITH CHECK ADD  CONSTRAINT [FK_Address_Town] FOREIGN KEY([TownId])
REFERENCES [dbo].[Town] ([Id])
GO
ALTER TABLE [dbo].[Address] CHECK CONSTRAINT [FK_Address_Town]
GO
ALTER TABLE [dbo].[Address_Postalcode]  WITH CHECK ADD  CONSTRAINT [FK_Address_Postalcode_Address] FOREIGN KEY([AddressId])
REFERENCES [dbo].[Address] ([Id])
GO
ALTER TABLE [dbo].[Address_Postalcode] CHECK CONSTRAINT [FK_Address_Postalcode_Address]
GO
ALTER TABLE [dbo].[AuthenticationIdentityKey_AuthenticationIdentity]  WITH CHECK ADD  CONSTRAINT [FK_AuthenticationIdentityKey_AuthenticationIdentity_AuthenticationIdentity] FOREIGN KEY([AuthenticationIdentityId])
REFERENCES [dbo].[AuthenticationIdentity] ([Id])
GO
ALTER TABLE [dbo].[AuthenticationIdentityKey_AuthenticationIdentity] CHECK CONSTRAINT [FK_AuthenticationIdentityKey_AuthenticationIdentity_AuthenticationIdentity]
GO
ALTER TABLE [dbo].[AuthenticationIdentityKey_AuthenticationIdentity]  WITH CHECK ADD  CONSTRAINT [FK_AuthenticationIdentityKey_AuthenticationIdentity_AuthenticationIdentityKey] FOREIGN KEY([AuthenticationKey])
REFERENCES [dbo].[AuthenticationIdentityKey] ([AuthenticationKey])
GO
ALTER TABLE [dbo].[AuthenticationIdentityKey_AuthenticationIdentity] CHECK CONSTRAINT [FK_AuthenticationIdentityKey_AuthenticationIdentity_AuthenticationIdentityKey]
GO
ALTER TABLE [dbo].[AuthenticationLog]  WITH CHECK ADD  CONSTRAINT [FK_AuthenticationLog_AuthenticationIdentity] FOREIGN KEY([AuthenticationId])
REFERENCES [dbo].[AuthenticationIdentity] ([Id])
GO
ALTER TABLE [dbo].[AuthenticationLog] CHECK CONSTRAINT [FK_AuthenticationLog_AuthenticationIdentity]
GO
ALTER TABLE [dbo].[AuthenticationPassword]  WITH CHECK ADD  CONSTRAINT [FK_AuthenticationPasswords_AuthenticationIdentity] FOREIGN KEY([AuthenticationIdentityId])
REFERENCES [dbo].[AuthenticationIdentity] ([Id])
GO
ALTER TABLE [dbo].[AuthenticationPassword] CHECK CONSTRAINT [FK_AuthenticationPasswords_AuthenticationIdentity]
GO
ALTER TABLE [dbo].[Contact_Address]  WITH CHECK ADD  CONSTRAINT [FK_Contact_Address_Address] FOREIGN KEY([AddressId])
REFERENCES [dbo].[Address] ([Id])
GO
ALTER TABLE [dbo].[Contact_Address] CHECK CONSTRAINT [FK_Contact_Address_Address]
GO
ALTER TABLE [dbo].[Contact_Address]  WITH CHECK ADD  CONSTRAINT [FK_Contact_Address_Contact] FOREIGN KEY([ContactId])
REFERENCES [dbo].[Contact] ([Id])
GO
ALTER TABLE [dbo].[Contact_Address] CHECK CONSTRAINT [FK_Contact_Address_Contact]
GO
ALTER TABLE [dbo].[Contact_ContactGroup]  WITH CHECK ADD  CONSTRAINT [FK_Contact_ContactGroup_Contact] FOREIGN KEY([ContactId])
REFERENCES [dbo].[Contact] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Contact_ContactGroup] CHECK CONSTRAINT [FK_Contact_ContactGroup_Contact]
GO
ALTER TABLE [dbo].[Contact_ContactGroup]  WITH CHECK ADD  CONSTRAINT [FK_Contact_ContactGroup_ContactGroup] FOREIGN KEY([ContactGroupId])
REFERENCES [dbo].[ContactGroup] ([Id])
GO
ALTER TABLE [dbo].[Contact_ContactGroup] CHECK CONSTRAINT [FK_Contact_ContactGroup_ContactGroup]
GO
ALTER TABLE [dbo].[Contact_ContactInformation]  WITH CHECK ADD  CONSTRAINT [FK_Contact_ContactInformation_Contact] FOREIGN KEY([ContactId])
REFERENCES [dbo].[Contact] ([Id])
GO
ALTER TABLE [dbo].[Contact_ContactInformation] CHECK CONSTRAINT [FK_Contact_ContactInformation_Contact]
GO
ALTER TABLE [dbo].[Contact_ContactInformation]  WITH CHECK ADD  CONSTRAINT [FK_Contact_ContactInformation_ContactInformation] FOREIGN KEY([ContactInformationId])
REFERENCES [dbo].[ContactInformation] ([Id])
GO
ALTER TABLE [dbo].[Contact_ContactInformation] CHECK CONSTRAINT [FK_Contact_ContactInformation_ContactInformation]
GO
ALTER TABLE [dbo].[Contact_ContactType]  WITH CHECK ADD  CONSTRAINT [FK_Contact_ContactType_Contact] FOREIGN KEY([ContactId])
REFERENCES [dbo].[Contact] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Contact_ContactType] CHECK CONSTRAINT [FK_Contact_ContactType_Contact]
GO
ALTER TABLE [dbo].[Contact_ContactType]  WITH CHECK ADD  CONSTRAINT [FK_Contact_ContactType_ContactType] FOREIGN KEY([ContactTypeId])
REFERENCES [dbo].[ContactType] ([Id])
GO
ALTER TABLE [dbo].[Contact_ContactType] CHECK CONSTRAINT [FK_Contact_ContactType_ContactType]
GO
ALTER TABLE [dbo].[Contact_Patient]  WITH CHECK ADD  CONSTRAINT [FK_Contact_Patient_Contact] FOREIGN KEY([ContactId])
REFERENCES [dbo].[Contact] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Contact_Patient] CHECK CONSTRAINT [FK_Contact_Patient_Contact]
GO
ALTER TABLE [dbo].[Contact_Patient]  WITH CHECK ADD  CONSTRAINT [FK_Contact_Patient_Patient] FOREIGN KEY([PatientId])
REFERENCES [dbo].[Patient] ([Id])
GO
ALTER TABLE [dbo].[Contact_Patient] CHECK CONSTRAINT [FK_Contact_Patient_Patient]
GO
ALTER TABLE [dbo].[ContactInformation]  WITH CHECK ADD  CONSTRAINT [FK_ContactInformation_ContactInformationType] FOREIGN KEY([ContactInformationTypeId])
REFERENCES [dbo].[ContactInformationType] ([Id])
GO
ALTER TABLE [dbo].[ContactInformation] CHECK CONSTRAINT [FK_ContactInformation_ContactInformationType]
GO
ALTER TABLE [dbo].[Employee]  WITH CHECK ADD  CONSTRAINT [FK_Employee_Organization] FOREIGN KEY([OrganizationId])
REFERENCES [dbo].[Organization] ([Id])
GO
ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_Employee_Organization]
GO
ALTER TABLE [dbo].[Employee_ContactInformation]  WITH CHECK ADD  CONSTRAINT [FK_Employee_ContactInformation_ContactInformation] FOREIGN KEY([ContactInformationId])
REFERENCES [dbo].[ContactInformation] ([Id])
GO
ALTER TABLE [dbo].[Employee_ContactInformation] CHECK CONSTRAINT [FK_Employee_ContactInformation_ContactInformation]
GO
ALTER TABLE [dbo].[Employee_ContactInformation]  WITH CHECK ADD  CONSTRAINT [FK_Employee_ContactInformation_Employee] FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[Employee] ([Id])
GO
ALTER TABLE [dbo].[Employee_ContactInformation] CHECK CONSTRAINT [FK_Employee_ContactInformation_Employee]
GO
ALTER TABLE [dbo].[EmployeeAuthenticationIdentityKey]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeAuthenticationIdentityKey_AuthenticationIdentityKey] FOREIGN KEY([AuthenticationKey])
REFERENCES [dbo].[AuthenticationIdentityKey] ([AuthenticationKey])
GO
ALTER TABLE [dbo].[EmployeeAuthenticationIdentityKey] CHECK CONSTRAINT [FK_EmployeeAuthenticationIdentityKey_AuthenticationIdentityKey]
GO
ALTER TABLE [dbo].[EmployeeAuthenticationIdentityKey]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeAuthenticationIdentityKey_Employee] FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[Employee] ([Id])
GO
ALTER TABLE [dbo].[EmployeeAuthenticationIdentityKey] CHECK CONSTRAINT [FK_EmployeeAuthenticationIdentityKey_Employee]
GO
ALTER TABLE [dbo].[Medication]  WITH CHECK ADD  CONSTRAINT [FK_Medication_Measure] FOREIGN KEY([MeasureId])
REFERENCES [dbo].[Measure] ([Id])
GO
ALTER TABLE [dbo].[Medication] CHECK CONSTRAINT [FK_Medication_Measure]
GO
ALTER TABLE [dbo].[Medication]  WITH CHECK ADD  CONSTRAINT [FK_Medication_MedicationBrand] FOREIGN KEY([MedicationBrandId])
REFERENCES [dbo].[MedicationBrand] ([Id])
GO
ALTER TABLE [dbo].[Medication] CHECK CONSTRAINT [FK_Medication_MedicationBrand]
GO
ALTER TABLE [dbo].[Medication]  WITH CHECK ADD  CONSTRAINT [FK_Medication_MedicationType] FOREIGN KEY([MedicationTypeId])
REFERENCES [dbo].[MedicationType] ([Id])
GO
ALTER TABLE [dbo].[Medication] CHECK CONSTRAINT [FK_Medication_MedicationType]
GO
ALTER TABLE [dbo].[MedicationSchedule]  WITH CHECK ADD  CONSTRAINT [FK_MedicationSchedule_Patient] FOREIGN KEY([PatientId])
REFERENCES [dbo].[Patient] ([Id])
GO
ALTER TABLE [dbo].[MedicationSchedule] CHECK CONSTRAINT [FK_MedicationSchedule_Patient]
GO
ALTER TABLE [dbo].[MedicationScheduleComplete]  WITH CHECK ADD  CONSTRAINT [FK_MedicationScheduleComplete_Employee] FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[Employee] ([Id])
GO
ALTER TABLE [dbo].[MedicationScheduleComplete] CHECK CONSTRAINT [FK_MedicationScheduleComplete_Employee]
GO
ALTER TABLE [dbo].[MedicationScheduleComplete]  WITH CHECK ADD  CONSTRAINT [FK_MedicationScheduleComplete_MedicationSchedule] FOREIGN KEY([MedicationScheduleId])
REFERENCES [dbo].[MedicationSchedule] ([Id])
GO
ALTER TABLE [dbo].[MedicationScheduleComplete] CHECK CONSTRAINT [FK_MedicationScheduleComplete_MedicationSchedule]
GO
ALTER TABLE [dbo].[MedicationScheduleItem]  WITH CHECK ADD  CONSTRAINT [FK_MedicationScheduleItem_Medication] FOREIGN KEY([MedicationId])
REFERENCES [dbo].[Medication] ([Id])
GO
ALTER TABLE [dbo].[MedicationScheduleItem] CHECK CONSTRAINT [FK_MedicationScheduleItem_Medication]
GO
ALTER TABLE [dbo].[MedicationScheduleItem]  WITH CHECK ADD  CONSTRAINT [FK_MedicationScheduleItem_MedicationSchedule] FOREIGN KEY([MedicationScheduleId])
REFERENCES [dbo].[MedicationSchedule] ([Id])
GO
ALTER TABLE [dbo].[MedicationScheduleItem] CHECK CONSTRAINT [FK_MedicationScheduleItem_MedicationSchedule]
GO
ALTER TABLE [dbo].[MedicationScheduleItemHistory]  WITH CHECK ADD  CONSTRAINT [FK_MedicationScheduleItemHistory_MedicationScheduleItem] FOREIGN KEY([MedicationScheduleItemId])
REFERENCES [dbo].[MedicationScheduleItem] ([Id])
GO
ALTER TABLE [dbo].[MedicationScheduleItemHistory] CHECK CONSTRAINT [FK_MedicationScheduleItemHistory_MedicationScheduleItem]
GO
ALTER TABLE [dbo].[MedicationScheduleItemHistoryEmployee]  WITH CHECK ADD  CONSTRAINT [FK_MedicationScheduleItemHistoryEmployee_MedicationScheduleItemHistory] FOREIGN KEY([MedicationScheduleItemHistoryId])
REFERENCES [dbo].[MedicationScheduleItemHistory] ([Id])
GO
ALTER TABLE [dbo].[MedicationScheduleItemHistoryEmployee] CHECK CONSTRAINT [FK_MedicationScheduleItemHistoryEmployee_MedicationScheduleItemHistory]
GO
ALTER TABLE [dbo].[MedicationScheduleItemRepeat]  WITH CHECK ADD  CONSTRAINT [FK_MedicationScheduleItemRepeat_MedicationScheduleItem] FOREIGN KEY([MedicationScheduleItemId])
REFERENCES [dbo].[MedicationScheduleItem] ([Id])
GO
ALTER TABLE [dbo].[MedicationScheduleItemRepeat] CHECK CONSTRAINT [FK_MedicationScheduleItemRepeat_MedicationScheduleItem]
GO
ALTER TABLE [dbo].[Municipality]  WITH CHECK ADD  CONSTRAINT [FK_Municipality_Region] FOREIGN KEY([RegionId])
REFERENCES [dbo].[Region] ([Id])
GO
ALTER TABLE [dbo].[Municipality] CHECK CONSTRAINT [FK_Municipality_Region]
GO
ALTER TABLE [dbo].[Patient]  WITH CHECK ADD  CONSTRAINT [FK_Patient_Organization] FOREIGN KEY([OrganizationId])
REFERENCES [dbo].[Organization] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Patient] CHECK CONSTRAINT [FK_Patient_Organization]
GO
ALTER TABLE [dbo].[Patient_ContactInformation]  WITH CHECK ADD  CONSTRAINT [FK_Patient_ContactInformation_ContactInformation] FOREIGN KEY([ContactInformationId])
REFERENCES [dbo].[ContactInformation] ([Id])
GO
ALTER TABLE [dbo].[Patient_ContactInformation] CHECK CONSTRAINT [FK_Patient_ContactInformation_ContactInformation]
GO
ALTER TABLE [dbo].[Patient_ContactInformation]  WITH CHECK ADD  CONSTRAINT [FK_Patient_ContactInformation_Patient] FOREIGN KEY([PatientId])
REFERENCES [dbo].[Patient] ([Id])
GO
ALTER TABLE [dbo].[Patient_ContactInformation] CHECK CONSTRAINT [FK_Patient_ContactInformation_Patient]
GO
ALTER TABLE [dbo].[PatientContactPersonRelationship]  WITH CHECK ADD  CONSTRAINT [FK_PatientContactPersonRelationship_Employee] FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[Employee] ([Id])
GO
ALTER TABLE [dbo].[PatientContactPersonRelationship] CHECK CONSTRAINT [FK_PatientContactPersonRelationship_Employee]
GO
ALTER TABLE [dbo].[PatientContactPersonRelationship]  WITH CHECK ADD  CONSTRAINT [FK_PatientContactPersonRelationship_Patient] FOREIGN KEY([PatientId])
REFERENCES [dbo].[Patient] ([Id])
GO
ALTER TABLE [dbo].[PatientContactPersonRelationship] CHECK CONSTRAINT [FK_PatientContactPersonRelationship_Patient]
GO
ALTER TABLE [dbo].[PatientContactPersonRelationshipEnded]  WITH CHECK ADD  CONSTRAINT [FK_PatientContactPersonRelationshipEnded_PatientContactPersonRelationship] FOREIGN KEY([PatientContactPersonRelationshipId])
REFERENCES [dbo].[PatientContactPersonRelationship] ([Id])
GO
ALTER TABLE [dbo].[PatientContactPersonRelationshipEnded] CHECK CONSTRAINT [FK_PatientContactPersonRelationshipEnded_PatientContactPersonRelationship]
GO
ALTER TABLE [dbo].[PatientDescription]  WITH CHECK ADD  CONSTRAINT [FK_PatientDescription_Description] FOREIGN KEY([DescriptionId])
REFERENCES [dbo].[Description] ([Id])
GO
ALTER TABLE [dbo].[PatientDescription] CHECK CONSTRAINT [FK_PatientDescription_Description]
GO
ALTER TABLE [dbo].[PatientDescription]  WITH CHECK ADD  CONSTRAINT [FK_PatientDescription_Patient] FOREIGN KEY([PatientId])
REFERENCES [dbo].[Patient] ([Id])
GO
ALTER TABLE [dbo].[PatientDescription] CHECK CONSTRAINT [FK_PatientDescription_Patient]
GO
ALTER TABLE [dbo].[PatientPost]  WITH CHECK ADD  CONSTRAINT [FK_PatientPost_Employee] FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[Employee] ([Id])
GO
ALTER TABLE [dbo].[PatientPost] CHECK CONSTRAINT [FK_PatientPost_Employee]
GO
ALTER TABLE [dbo].[PatientPost]  WITH CHECK ADD  CONSTRAINT [FK_PatientPost_Organization] FOREIGN KEY([OrganizationId])
REFERENCES [dbo].[Organization] ([Id])
GO
ALTER TABLE [dbo].[PatientPost] CHECK CONSTRAINT [FK_PatientPost_Organization]
GO
ALTER TABLE [dbo].[PatientPost_Patient]  WITH CHECK ADD  CONSTRAINT [FK_PatientPost_Patient_Patient] FOREIGN KEY([PatientId])
REFERENCES [dbo].[Patient] ([Id])
GO
ALTER TABLE [dbo].[PatientPost_Patient] CHECK CONSTRAINT [FK_PatientPost_Patient_Patient]
GO
ALTER TABLE [dbo].[PatientPost_Patient]  WITH CHECK ADD  CONSTRAINT [FK_PatientPost_Patient_PatientPost] FOREIGN KEY([PatientPostId])
REFERENCES [dbo].[PatientPost] ([Id])
GO
ALTER TABLE [dbo].[PatientPost_Patient] CHECK CONSTRAINT [FK_PatientPost_Patient_PatientPost]
GO
ALTER TABLE [dbo].[Region]  WITH CHECK ADD  CONSTRAINT [FK_Region_Country] FOREIGN KEY([CountryId])
REFERENCES [dbo].[Country] ([Id])
GO
ALTER TABLE [dbo].[Region] CHECK CONSTRAINT [FK_Region_Country]
GO
ALTER TABLE [dbo].[Town]  WITH CHECK ADD  CONSTRAINT [FK_Town_Region] FOREIGN KEY([RegionId])
REFERENCES [dbo].[Region] ([Id])
GO
ALTER TABLE [dbo].[Town] CHECK CONSTRAINT [FK_Town_Region]
GO
ALTER TABLE [dbo].[TownMunicipality]  WITH CHECK ADD  CONSTRAINT [FK_TownMunicipality_Municipality] FOREIGN KEY([MunicipalityId])
REFERENCES [dbo].[Municipality] ([Id])
GO
ALTER TABLE [dbo].[TownMunicipality] CHECK CONSTRAINT [FK_TownMunicipality_Municipality]
GO
ALTER TABLE [dbo].[TownMunicipality]  WITH CHECK ADD  CONSTRAINT [FK_TownMunicipality_Town] FOREIGN KEY([TownId])
REFERENCES [dbo].[Town] ([Id])
GO
ALTER TABLE [dbo].[TownMunicipality] CHECK CONSTRAINT [FK_TownMunicipality_Town]
GO
ALTER TABLE [dbo].[TownPostalcode]  WITH CHECK ADD  CONSTRAINT [FK_TownPostalcode_Town] FOREIGN KEY([TownId])
REFERENCES [dbo].[Town] ([Id])
GO
ALTER TABLE [dbo].[TownPostalcode] CHECK CONSTRAINT [FK_TownPostalcode_Town]
GO
