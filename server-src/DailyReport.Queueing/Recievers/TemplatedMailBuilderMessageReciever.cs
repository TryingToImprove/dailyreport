﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain;
using DailyReport.Domain.Infrastructure.Dispensers;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Messages;
using DailyReport.Services;
using DailyReport.Templating.Models;
using MessageQueuer;
using Newtonsoft.Json.Linq;

namespace DailyReport.Queueing.Recievers
{
    [MqReciever(Name = MessageQueues.TemplatedMailBuilder, Handlers = 2)]
    public class TemplatedMailBuilderMessageReciever : IMqReciever<TemplatedMailBuilderMessage>
    {
        private readonly ITemplateService _templateService;
        private readonly IMailDispenser _mailDispenser;

        public TemplatedMailBuilderMessageReciever(ITemplateService templateService, IMailDispenser mailDispenser)
        {
            _templateService = templateService;
            _mailDispenser = mailDispenser;
        }

        public async void Invoke(TemplatedMailBuilderMessage message)
        {
            // Because the message.TemplateViewModel is type of object, it will be deserialized to a 
            // JOjbject. We can use that to transform the model to whatever type we want - message.TemplateViewModelType
            var viewModel = ((JObject) message.TemplateViewModel).ToObject(message.TemplateViewModelType);

            // Build HTML
            var htmlBody = _templateService.Render(message.Template, viewModel);

            // Send the mail to the mail sender queue
            await _mailDispenser.SendAsync(message.Information, htmlBody);
        }
    }
}
