﻿using System;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using DailyReport.Domain;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Messages;
using MessageQueuer;

namespace DailyReport.Queueing.Recievers
{
    [MqReciever(Name = MessageQueues.MailSender, Handlers = 2)]
    public class MailSenderMessageReciever : IMqReciever<MailSenderMessage>
    {
        private readonly IMailService _mailService;

        public MailSenderMessageReciever(IMailService mailService)
        {
            _mailService = mailService;
        }
        
        public void Invoke(MailSenderMessage message)
        {
            var recipients = message.Information.Recipients.Select(x => new MailAddress(x));
            var fromAddress = new MailAddress(message.Information.From);

            _mailService.Send(fromAddress, recipients, message.Information.Subject, message.Body);
        }
    }
}
