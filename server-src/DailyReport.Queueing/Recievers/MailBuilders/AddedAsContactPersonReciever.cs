﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain;
using DailyReport.Domain.Infrastructure.Dispensers;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Messages;
using DailyReport.Templating.Models;
using MessageQueuer;

namespace DailyReport.Queueing.Recievers.MailBuilders
{
    [MqReciever(Name = MessageQueues.MailBuilders.AddedAsContactPerson, Handlers = 2)]
    public class AddedAsContactPersonReciever : IMqReciever<AddedAsContactPersonMessage>
    {
        private readonly IMailDispenser _mailDispenser;
        private readonly IPatientService _patientService;
        private readonly IEmployeeService _employeeService;

        public AddedAsContactPersonReciever(IMailDispenser mailDispenser, IPatientService patientService, IEmployeeService employeeService)
        {
            _mailDispenser = mailDispenser;
            _patientService = patientService;
            _employeeService = employeeService;
        }

        public void Invoke(AddedAsContactPersonMessage message)
        {
            // We need to fetch the patient
            var patient = _patientService.GetById(message.PatientId);
            var employee = _employeeService.GetById(message.EmployeeId);

            // Create the template viewmodel
            var templateViewModel = new AddedAsContactPersonModel
            {
                Employee = employee,
                Patient = patient
            };

            // Send mail to the employe
            _mailDispenser.SendTemplatedAsync("AddedAsContactPersonView", templateViewModel, new MailInformation
            {
                From = "system@dagsrapporten.dk",
                Recipients = new[] { employee.Email },
                Subject = "Du er blevet kontaktperson"
            });
        }
    }
}
