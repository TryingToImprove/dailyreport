﻿using System;
using DailyReport.Domain.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DailyReport.Domain.Models.Core;
using Moq;
using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Infrastructure;
using DailyReport.Tests.Helpers;

namespace DailyReport.Services.Tests
{
    [TestClass]
    public class EmployeeServiceTests
    {
        [TestMethod]
        public void EmployeeService_FindAll_ShouldCallFindAllOnceOnRepository()
        {
            var organizationId = 1;
            var employeeRepositoryMock = new Mock<IEmployeeRepository>();

            var dataContextMock = new Mock<IDataContext>();
            dataContextMock.Setup(x => x.Employees).Returns(employeeRepositoryMock.Object);

            var employeeService = new EmployeeService(dataContextMock.Object);

            var results = employeeService.FindAll(organizationId);

            employeeRepositoryMock.Verify(x => x.FindAll(organizationId), Times.Once);
        }

        [TestMethod]
        public void EmployeeService_Insert_ShouldThrowExceptionIfValidationFails()
        {
            var dataContextMock = new Mock<IDataContext>();

            var employeeService = new EmployeeService(dataContextMock.Object);

            ExtendedAssert.Throws<ArgumentNullException>(() =>
            {
                employeeService.Insert(null);
            });

            ExtendedAssert.Throws<ArgumentNullException>(() =>
            {
                var employee = GetValidEmployee();
                employee.Firstname = null;

                employeeService.Insert(employee);
            });

            ExtendedAssert.Throws<ArgumentNullException>(() =>
            {
                var employee = GetValidEmployee();
                employee.Lastname = null;

                employeeService.Insert(employee);
            });

            ExtendedAssert.Throws<ArgumentOutOfRangeException>(() =>
            {
                var employee = GetValidEmployee();
                employee.OrganizationId = 0;

                employeeService.Insert(employee);
            });
        }

        [TestMethod]
        public void EmployeeService_Insert_ShouldCallInsertOnEmployeeRepository()
        {
            var employee = GetValidEmployee();
            var employeeRepositoryMock = new Mock<IEmployeeRepository>();

            var dataContextMock = new Mock<IDataContext>();
            dataContextMock.Setup(x => x.Employees).Returns(employeeRepositoryMock.Object);

            var employeeService = new EmployeeService(dataContextMock.Object);
            employeeService.Insert(employee);

            employeeRepositoryMock.Verify(x => x.Insert(employee), Times.Once);
        }

        [TestMethod]
        public void EmployeeService_Insert_IfTimeCreatedIsMinValue_SetDateTimeNow()
        {
            var employee = GetValidEmployee();
            employee.TimeCreated = DateTime.MinValue;

            var employeeRepositoryMock = new Mock<IEmployeeRepository>();

            var dataContextMock = new Mock<IDataContext>();
            dataContextMock.Setup(x => x.Employees).Returns(employeeRepositoryMock.Object);

            var employeeService = new EmployeeService(dataContextMock.Object);
            employeeService.Insert(employee);

            Assert.IsTrue(employee.TimeCreated > DateTime.MinValue);
        }

        [TestMethod]
        public void EmployeeService_Insert_ShouldReturnEmployeeResult()
        {
            var returnEmployee = GetValidEmployee();
            returnEmployee.Id = 1;

            var employee = GetValidEmployee();

            var employeeRepositoryMock = new Mock<IEmployeeRepository>();
            employeeRepositoryMock.Setup(x => x.Insert(employee)).Returns(returnEmployee);

            var dataContextMock = new Mock<IDataContext>();
            dataContextMock.Setup(x => x.Employees).Returns(employeeRepositoryMock.Object);

            var employeeService = new EmployeeService(dataContextMock.Object);
            employee = employeeService.Insert(employee);

            Assert.AreEqual(returnEmployee.Id, employee.Id);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void EmployeeService_GetById_ShouldThrowExceptionIfIdIsLowerThanOne()
        {
            var employeeRepositoryMock = new Mock<IEmployeeRepository>();
            var dataContextMock = new Mock<IDataContext>();
            dataContextMock.Setup(x => x.Employees).Returns(employeeRepositoryMock.Object);

            var employeeService = new EmployeeService(dataContextMock.Object);
            employeeService.GetById(0);
        }

        [TestMethod]
        public void EmployeeService_GetById_ShouldReturnEmployee()
        {
            var validEmployee = GetValidEmployee();
            validEmployee.Id = 1;

            var employeeRepositoryMock = new Mock<IEmployeeRepository>();
            employeeRepositoryMock.Setup(x => x.GetById(validEmployee.Id)).Returns(validEmployee);

            var dataContextMock = new Mock<IDataContext>();
            dataContextMock.Setup(x => x.Employees).Returns(employeeRepositoryMock.Object);

            var employeeService = new EmployeeService(dataContextMock.Object);
            var employee = employeeService.GetById(1);

            Assert.IsNotNull(employee);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void EmployeeService_UpdateActiveState_FailIfNonValidArguments()
        {
            var dataContextMock = new Mock<IDataContext>();
            var employeeService = new EmployeeService(dataContextMock.Object);

            employeeService.UpdateActiveState(0, false);
        }

        [TestMethod]
        public void EmployeeService_UpdateActiveState_ShouldCallRepository()
        {
            var employeeRepositoryMock = new Mock<IEmployeeRepository>();

            var dataContextMock = new Mock<IDataContext>();
            dataContextMock.Setup(x => x.Employees).Returns(employeeRepositoryMock.Object);

            var employeeService = new EmployeeService(dataContextMock.Object);
            employeeService.UpdateActiveState(1, false);

            employeeRepositoryMock.Verify(x => x.UpdateActiveState(1, false), Times.Once);
        }

        [TestMethod]
        public void EmployeeService_Update_ShouldThrowExceptionIfValidationFails()
        {
            var employeeRepositoryMock = new Mock<IEmployeeRepository>();
            var dataContextMock = new Mock<IDataContext>();
            dataContextMock.Setup(x => x.Employees).Returns(employeeRepositoryMock.Object);

            var employeeService = new EmployeeService(dataContextMock.Object);

            ExtendedAssert.Throws<ArgumentNullException>(() =>
            {
                employeeService.Update(null);
            });

            ExtendedAssert.Throws<ArgumentNullException>(() =>
            {
                var employee = GetValidEmployee();
                employee.Firstname = null;

                employeeService.Update(employee);
            });

            ExtendedAssert.Throws<ArgumentNullException>(() =>
            {
                var employee = GetValidEmployee();
                employee.Lastname = null;

                employeeService.Update(employee);
            });

            ExtendedAssert.Throws<ArgumentOutOfRangeException>(() =>
            {
                var employee = GetValidEmployee();
                employee.OrganizationId = 0;

                employeeService.Update(employee);
            });
        }

        [TestMethod]
        public void EmployeeService_Update_ShouldCallEmployeeRepositoryUpdate()
        {
            var employee = GetValidEmployee();

            var employeeRepositoryMock = new Mock<IEmployeeRepository>();
            var dataContextMock = new Mock<IDataContext>();
            dataContextMock.Setup(x => x.Employees).Returns(employeeRepositoryMock.Object);

            var employeeService = new EmployeeService(dataContextMock.Object);
            employeeService.Update(employee);

            employeeRepositoryMock.Verify(x => x.Update(employee), Times.Once);
        }

        [TestMethod]
        public void EmployeeService_Authenticate_ThrowExceptionsIfValidationsFails()
        {
            AuthenticationIdentity identity;
            var employeeRepositoryMock = new Mock<IEmployeeRepository>();
            var dataContextMock = new Mock<IDataContext>();
            dataContextMock.Setup(x => x.Employees).Returns(employeeRepositoryMock.Object);

            var employeeService = new EmployeeService(dataContextMock.Object);

            ExtendedAssert.Throws<ArgumentNullException>(() => employeeService.Authenticate(null, "test"));
            ExtendedAssert.Throws<ArgumentException>(() => employeeService.Authenticate(string.Empty, "test"));
            ExtendedAssert.Throws<ArgumentException>(() => employeeService.Authenticate(" ", "test"));

            ExtendedAssert.Throws<ArgumentNullException>(() => employeeService.Authenticate("test", null));
            ExtendedAssert.Throws<ArgumentException>(() => employeeService.Authenticate("test", string.Empty));
            ExtendedAssert.Throws<ArgumentException>(() => employeeService.Authenticate("test", " "));
        }

        [TestMethod]
        public void EmployeeService_Authenticate_ShouldReturnAuthenticationResult()
        {
            var authenticationResult = GetValidAuthenticationResultModel();
            
            var authenticationLogRepositoryMock = new Mock<IAuthenticationLogRepository>();
            var authenticationIdentityRepositoryMock = new Mock<IAuthenticationIdentityRepository>();
            authenticationIdentityRepositoryMock.Setup(x => x.FindEmployee("test", "test!"))
                .Returns(authenticationResult);

            var dataContextMock = new Mock<IDataContext>();
            dataContextMock.Setup(x => x.AuthenticationIdentites).Returns(authenticationIdentityRepositoryMock.Object);
            dataContextMock.Setup(x => x.AuthenticationLogs).Returns(authenticationLogRepositoryMock.Object);

            var employeeService = new EmployeeService(dataContextMock.Object);

            var result = employeeService.Authenticate("test", "test");

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Identity);
            Assert.IsNotNull(result.User);
        }

        [TestMethod]
        public void EmployeeService_Authenticate_Success_ShouldCreateLog()
        {
            var authenticationResult = GetValidAuthenticationResultModel();

            var authenticationLogRepositoryMock = new Mock<IAuthenticationLogRepository>();

            var authenticationIdentityRepositoryMock = new Mock<IAuthenticationIdentityRepository>();
            authenticationIdentityRepositoryMock.Setup(x => x.FindEmployee("test", "test!"))
                .Returns(authenticationResult);

            var dataContextMock = new Mock<IDataContext>();
            dataContextMock.Setup(x => x.AuthenticationIdentites).Returns(authenticationIdentityRepositoryMock.Object);
            dataContextMock.Setup(x => x.AuthenticationLogs).Returns(authenticationLogRepositoryMock.Object);

            var employeeService = new EmployeeService(dataContextMock.Object);

            employeeService.Authenticate("test", "test");

            authenticationLogRepositoryMock.Verify(x => x.Insert(authenticationResult.Identity.Id, It.IsAny<DateTime>()), Times.Once);
        }

        [TestMethod]
        public void EmployeeService_Authenticate_Failed_ShouldNotCreateLog()
        {
            var authenticationLogRepositoryMock = new Mock<IAuthenticationLogRepository>();
            var authenticationIdentityRepositoryMock = new Mock<IAuthenticationIdentityRepository>();

            var dataContextMock = new Mock<IDataContext>();
            dataContextMock.Setup(x => x.AuthenticationIdentites).Returns(authenticationIdentityRepositoryMock.Object);
            dataContextMock.Setup(x => x.AuthenticationLogs).Returns(authenticationLogRepositoryMock.Object);

            var employeeService = new EmployeeService(dataContextMock.Object);

            employeeService.Authenticate("test", "test");

            authenticationLogRepositoryMock.Verify(x => x.Insert(It.IsAny<int>(), It.IsAny<DateTime>()), Times.Never);
        }

        private AuthenticationResultModel<Employee> GetValidAuthenticationResultModel()
        {
            return new AuthenticationResultModel<Employee>
            {
                User = GetValidEmployee(),
                Identity = new AuthenticationIdentity()
                {
                    Id = 1,
                    Username = "test",
                    PasswordHash = "tests!",
                    IsActive = true
                }
            };
        }

        private Employee GetValidEmployee()
        {
            return new Employee
            {
                Firstname = "Kiri",
                Lastname = "Lassen",
                OrganizationId = 1,
                TimeCreated = DateTime.Now
            };
        }
    }
}
