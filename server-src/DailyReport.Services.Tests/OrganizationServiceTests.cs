﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DailyReport.Domain.Models.Core;
using Moq;
using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Infrastructure;

namespace DailyReport.Services.Tests
{
    [TestClass]
    public class OrganizationServiceTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Insert_FailsWithNullArgument()
        {
            var organizationService = new OrganizationService(null);

            organizationService.Insert(null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Insert_FailsWithEmptyName()
        {
            var organizationService = new OrganizationService(null);

            organizationService.Insert(new Organization
            {
                Name = string.Empty
            });
        }

        [TestMethod]
        public void Insert_ShouldCallInsertOnceOnRepository()
        {
            var organizationRepositoryMock = new Mock<IOrganizationRepository>();

            var dataContextMock = new Mock<IDataContext>();
            dataContextMock.Setup(x => x.Organizations).Returns(organizationRepositoryMock.Object);

            var organizationService = new OrganizationService(dataContextMock.Object);

            var organization = new Organization
            {
                Name = "Olivers Bosted",
                TimeCreated = DateTime.Now
            };

            organizationService.Insert(organization);

            organizationRepositoryMock.Verify(x => x.Insert(organization), Times.Once);
        }

        [TestMethod]
        public void Insert_IfOrganizationTimeCreatedIsMinValueThenSetCurrentDateTime()
        {
            var organizationRepositoryMock = new Mock<IOrganizationRepository>();

            var dataContextMock = new Mock<IDataContext>();
            dataContextMock.Setup(x => x.Organizations).Returns(organizationRepositoryMock.Object);
            var organizationService = new OrganizationService(dataContextMock.Object);

            var organization = organizationService.Insert(new Organization
            {
                Name = "Olivers Bosted"
            });

            Assert.IsTrue(organization.TimeCreated > DateTime.MinValue);
        }

        [TestMethod]
        public void FindAll_ShouldCallFindAllOnceOnRepository()
        {
            var organizationRepositoryMock = new Mock<IOrganizationRepository>();

            var dataContextMock = new Mock<IDataContext>();
            dataContextMock.Setup(x => x.Organizations).Returns(organizationRepositoryMock.Object);

            var organizationService = new OrganizationService(dataContextMock.Object);

            var results = organizationService.FindAll();

            organizationRepositoryMock.Verify(x => x.FindAll(), Times.Once);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void GetById_ShouldFailWhenOrganizationIdIsLowerThan1()
        {
            var dataContextMock = new Mock<IDataContext>();
            var organizationService = new OrganizationService(dataContextMock.Object);

            organizationService.GetById(0);
        }

        [TestMethod]
        public void GetById_ShouldReturnResultFromOrganizationRepository()
        {
            var organization = new Organization
            {
                Id = 1,
                Name = "Olivers bosted",
                TimeCreated = DateTime.Now
            };

            var dataContextMock = new Mock<IDataContext>();
            dataContextMock.Setup(x => x.Organizations.GetById(organization.Id)).Returns(organization);

            var organizationService = new OrganizationService(dataContextMock.Object);

            var result = organizationService.GetById(1);

            Assert.IsNotNull(organization);
            Assert.AreEqual(organization.Id, result.Id);
            Assert.AreEqual(organization.Name, result.Name);
            Assert.AreEqual(organization.TimeCreated, result.TimeCreated);
        }

        [TestMethod]
        public void Update_ShouldCallUpdateOnRepository()
        {
            var organization = new Organization
            {
                Id = 1,
                Name = "Updated Name",
                TimeCreated = DateTime.Now
            };

            var organizationRepositoryMock = new Mock<IOrganizationRepository>();

            var dataContextMock = new Mock<IDataContext>();
            dataContextMock.Setup(x => x.Organizations).Returns(organizationRepositoryMock.Object);

            var organizationService = new OrganizationService(dataContextMock.Object);

            organizationService.Update(organization);

            organizationRepositoryMock.Verify(x => x.Update(organization), Times.Once);
        }
    }
}
