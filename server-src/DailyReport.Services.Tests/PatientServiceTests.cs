﻿using System;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Models;
using DailyReport.Tests.Helpers.Fakes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DailyReport.Domain.Models.Core;
using Moq;
using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Infrastructure;
using DailyReport.Tests.Helpers;
using FizzWare.NBuilder;

namespace DailyReport.Services.Tests
{
    [TestClass]
    public class PatientServiceTests
    {
        #region FindAll
        [TestMethod, ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void PatientServicee_FindAll_GivenOrganizationIdLowerThan1_ThrowException()
        {
            var patientService = GetService();

            patientService.FindAll(0);
        }

        [TestMethod]
        public void PatientServicee_FindAll_GivenValidOrganizationId_ShouldReturnResultFromRepository()
        {
            const int organizationId = 1;

            var patients = FakePatient.CreateResultModels(organizationId);

            var dataContext = FakeDataContext.Create()
                .With(x => x.Patients.Setup(y => y.FindAll(organizationId)).Returns(patients));

            var patientService = GetService(dataContext);

            var results = patientService.FindAll(organizationId);

            Assert.AreSame(results, patients);
        }
        #endregion

        #region Insert
        [TestMethod]
        [TestCategory("PatientService.Insert")]
        public void Insert_WhenInvalid_ThrowExceptions()
        {
            var patientService = GetService();

            ExtendedAssert.Throws<ArgumentNullException>(() =>
            {
                var patient = FakePatient.Create();
                patient.Firstname = string.Empty;

                patientService.Insert(patient);
            });

            ExtendedAssert.Throws<ArgumentNullException>(() =>
            {
                var patient = FakePatient.Create();
                patient.Lastname = string.Empty;

                patientService.Insert(patient);
            });

            ExtendedAssert.Throws<ArgumentOutOfRangeException>(() =>
            {
                var patient = FakePatient.Create();
                patient.OrganizationId = 0;

                patientService.Insert(patient);
            });

            ExtendedAssert.Throws<ArgumentNullException>(() =>
            {
                var patient = FakePatient.Create();
                patient.BirthDate = DateTime.MinValue;

                patientService.Insert(patient);
            });
        }

        [TestMethod]
        [TestCategory("PatientService.Insert")]
        public void Insert_WhenValid_CallPatientRepositoryOnce()
        {
            var patient = FakePatient.Create(1);

            var dataContext = FakeDataContext.Create()
                .With(SetupInsertPatientExpression(patient));

            var patientService = GetService(dataContext);

            patientService.Insert(patient);

            dataContext.Patients.Verify(x => x.Insert(patient), Times.Once);
        }

        [TestMethod]
        [TestCategory("PatientService.Insert")]
        public void Insert_WhenValidButNoTimeCreated_SetToDateTimeNow()
        {
            var patient = FakePatient.Create();
            patient.TimeCreated = DateTime.MinValue;

            var dataContext = FakeDataContext.Create()
                .With(SetupInsertPatientExpression(patient));

            var patientService = GetService(dataContext);

            var insertedPatient = patientService.Insert(patient);

            Assert.AreNotEqual(DateTime.MinValue, insertedPatient.TimeCreated);
        }
        #endregion

        #region Deactivate
        [TestMethod, ExpectedException(typeof(ArgumentOutOfRangeException))]
        [TestCategory("PatientService.Deactivate")]
        public void Deactivate_GivenPatientIdIsLowerThanOne_ThrowsException()
        {
            var patientService = GetService();

            patientService.Deactivate(0);
        }

        [TestMethod]
        [TestCategory("PatientService.Deactivate")]
        public void Deactivate_GivenValidPatientId_CallsDeactiveOnceOnPatientRepository()
        {
            var patient = FakePatient.Create();
            var dataContext = FakeDataContext.Create().Intialize(x=> x.Patients);

            var patientService = GetService(dataContext);

            patientService.Deactivate(patient.Id);

            dataContext.Patients.Verify(x => x.Deactivate(patient.Id), Times.Once);
        }
        #endregion

        #region GetById
        [TestMethod, ExpectedException(typeof(ArgumentOutOfRangeException))]
        [TestCategory("PatientService.GetById")]
        public void GetById_GivenPatientIdIsLowerThanOne_ThrowsException()
        {
            var patientService = GetService();

            patientService.GetById(0);
        }

        [TestMethod]
        [TestCategory("PatientService.GetById")]
        public void GetById_GivenValidPatientId_ReturnsResultFromRepository()
        {
            var patient = FakePatient.CreateResultModel();
            var dataContext = FakeDataContext.Create()
                .With(SetupGetPatientByIdExpression(patient));

            var patientService = GetService(dataContext);

            var result = patientService.GetById(patient.Id);

            Assert.AreEqual(patient.Id, result.Id);
            Assert.AreEqual(patient.Firstname, result.Firstname);
            Assert.AreEqual(patient.Lastname, result.Lastname);
            Assert.AreEqual(patient.BirthDate.Ticks, result.BirthDate.Ticks);
            Assert.AreEqual(patient.IsActive, result.IsActive);
            Assert.AreEqual(patient.OrganizationId, result.OrganizationId);
            Assert.AreEqual(patient.TimeCreated.Ticks, result.TimeCreated.Ticks);
        }
        #endregion

        #region CreateDescription
        [TestMethod]
        [TestCategory("PatientService.CreateDescription")]
        public void CreateDescription_GivenInvalidModel_ThrowsException()
        {
            var patientService = GetService();

            ExtendedAssert.Throws<ArgumentOutOfRangeException>(() => patientService.CreateDescription(0, null));

            ExtendedAssert.Throws<ArgumentNullException>(() => patientService.CreateDescription(1, null));

            ExtendedAssert.Throws<ArgumentOutOfRangeException>(() =>
            {
                var description = FakeDescription.Create();
                description.EmployeeId = 0;

                patientService.CreateDescription(1, description);
            });

            ExtendedAssert.Throws<ArgumentException>(() =>
            {
                var description = FakeDescription.Create();
                description.Text = string.Empty;

                patientService.CreateDescription(1, description);
            });

            ExtendedAssert.Throws<ArgumentNullException>(() =>
            {
                var description = FakeDescription.Create();
                description.Text = null;

                patientService.CreateDescription(1, description);
            });
        }

        [TestMethod]
        [TestCategory("PatientService.CreateDescription")]
        public void CreateDescription_WhenTimeCreatedIsMinValue_SetItToNow()
        {
            var description = FakeDescription.Create();

            var patientService = GetService(FakeDataContext.Create()
                .Intialize(x => x.Descriptions));

            description.TimeCreated = DateTime.MinValue;
            Assert.AreEqual(DateTime.MinValue.Ticks, description.TimeCreated.Ticks);

            patientService.CreateDescription(1, description);

        }

        [TestMethod]
        [TestCategory("PatientService.CreateDescription")]
        public void CreateDescription_GivenValidModel_CallsDescriptionRepository()
        {
            var description = FakeDescription.Create();
            var dataContext = FakeDataContext.Create().Intialize(x => x.Descriptions);

            var patientService = GetService(dataContext);

            patientService.CreateDescription(1, description);

            dataContext.Descriptions.Verify(x => x.InsertPatientDescription(1, description), Times.Once);
        }
        #endregion

        #region GetDescriptions
        [TestMethod, ExpectedException(typeof(ArgumentOutOfRangeException))]
        [TestCategory("PatientService.GetDescriptions")]
        public void GetDescriptions_GivenPatientIdLowerThanOne_ThrowsException()
        {
            var patientService = GetService();

            patientService.GetDescriptions(0);
        }

        [TestMethod]
        [TestCategory("PatientService.GetDescriptions")]
        public void GetDescriptions_GivenValidPatientId_ReturnsDescriptionsFromRepository()
        {
            var descriptions = Builder<Description>.CreateListOfSize(10).Build();

            var dataContext = FakeDataContext.Create()
                .With(x => x.Descriptions.Setup(y => y.GetPatientDescriptions(1)).Returns(descriptions));

            var patientService = GetService(dataContext);

            var descriptionResult = patientService.GetDescriptions(1);

            Assert.AreSame(descriptions, descriptionResult);
        }
        #endregion

        #region AddContactPerson
        [TestMethod, ExpectedException(typeof(ArgumentOutOfRangeException))]
        [TestCategory("PatientService.AddContactPersonRelationship")]
        public void AddContactPersonRelationship_GivenPatientIdLowerThanOne_ThrowsException()
        {
            var patientService = GetService();

            patientService.AddContactPerson(0, 1);
        }

        [TestMethod, ExpectedException(typeof(ArgumentOutOfRangeException))]
        [TestCategory("PatientService.AddContactPersonRelationship")]
        public void AddContactPersonRelationship_GivenEmployeeIdLowerThanOne_ThrowsException()
        {
            var patientService = GetService();

            patientService.AddContactPerson(1, 0);
        }

        [TestMethod, ExpectedException(typeof(ArgumentOutOfRangeException))]
        [TestCategory("PatientService.AddContactPersonRelationship")]
        public void AddContactPersonRelationship_WithCompletePreviousContactPersonsFlag_GivenPatientIdIsLowerThanOne_ThrowsException()
        {
            var patientService = GetService();

            patientService.AddContactPerson(0, 1, true);
        }

        [TestMethod, ExpectedException(typeof(ArgumentOutOfRangeException))]
        [TestCategory("PatientService.AddContactPersonRelationship")]
        public void AddContactPersonRelationship_WithCompletePreviousContactPersonsFlag_GivenEmployeeIdIsLowerThanOne_ThrowsException()
        {
            var patientService = GetService();

            patientService.AddContactPerson(1, 0, true);
        }

        [TestMethod]
        [TestCategory("PatientService.AddContactPersonRelationship")]
        public void AddContactPersonRelationship_GivenValidData_CallsPatientContactPersonRelationshipRepository()
        {
            var dataContext = FakeDataContext.Create()
                .Intialize(x => x.PatientContactPersonRelationships);

            var patientService = GetService(dataContext);

            patientService.AddContactPerson(1, 1);

            dataContext.PatientContactPersonRelationships.Verify(x => x.Insert(It.IsAny<PatientContactPersonRelationship>(), false), Times.Once);
        }

        [TestMethod]
        [TestCategory("PatientService.AddContactPersonRelationship")]
        public void AddContactPersonRelationship_WithCompletePreviousContactPersonsFlag_GivenValidData_CallsPatientContactPersonRelationshipRepository()
        {
            const bool completePrevious = true;

            var dataContext = FakeDataContext.Create()
                .Intialize(x => x.PatientContactPersonRelationships);

            var patientService = GetService(dataContext);

            patientService.AddContactPerson(1, 1, completePrevious);

            dataContext.PatientContactPersonRelationships.Verify(x => x.Insert(It.IsAny<PatientContactPersonRelationship>(), true), Times.Once);
        }
        #endregion

        #region GetContactPersons
        [TestMethod, ExpectedException(typeof(ArgumentOutOfRangeException))]
        [TestCategory("PatientService.GetContactPersons")]
        public void GetContactPersons_GivenPatientIdIsLowerThanOne_ThrowsException()
        {
            var patientService = GetService();

            patientService.GetContactPersons(0);
        }

        [TestMethod]
        [TestCategory("PatientService.GetContactPersons")]
        public void GetContactPersons_GivenPatientId_ReturnsResultFromRepository()
        {
            var expected = FakePatientContactPersonRelationship.CreateResultModels();

            var patientService = GetService(FakeDataContext.Create()
                .With(x => x.PatientContactPersonRelationships.Setup(y => y.FindAll(1)).Returns(expected)));

            var results = patientService.GetContactPersons(1);
            Assert.AreSame(expected, results);
        }
        #endregion

        #region GetContactInformations

        [TestMethod, ExpectedException(typeof(ArgumentOutOfRangeException))]
        [TestCategory("PatientService.GetContactInformations")]
        public void GetContactInformations_GivenPatientIdIsLowerThanOne_ThrowsException()
        {
            var patientService = GetService();

            patientService.GetContactInformations(0);
        }

        [TestMethod]
        [TestCategory("PatientService.GetContactInformations")]
        public void GetContactInformations_GivenPatientId_ReturnsResultFromRepository()
        {
            var expected = FakeContactInformation.CreateResultModels();

            var patientService = GetService(FakeDataContext.Create()
                .With(x => x.ContactInformations.Setup(y => y.FindPatientContactInformations(1)).Returns(expected)));

            var results = patientService.GetContactInformations(1);
            Assert.AreSame(expected, results);
        }

        #endregion

        private PatientService GetService(FakeDataContext fakeDataContext = null, IContactInformationService contactInformationService = null)
        {
            if (fakeDataContext == null)
                fakeDataContext = FakeDataContext.Create();

            return new PatientService(fakeDataContext.Object, contactInformationService);
        }

        private static Action<FakeDataContext> SetupInsertPatientExpression(Patient patient)
        {
            return x => x.Patients.Setup(y => y.Insert(patient)).Returns(patient);
        }

        private static Action<FakeDataContext> SetupGetPatientByIdExpression(PatientResultModel patient)
        {
            return x => x.Patients.Setup(y => y.GetById(patient.Id)).Returns(patient);
        }
    }
}
