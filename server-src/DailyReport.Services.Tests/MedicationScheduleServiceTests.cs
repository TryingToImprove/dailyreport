﻿using System;
using System.Linq;
using DailyReport.Domain.Infrastructure;
using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using DailyReport.Tests.Helpers;
using FizzWare.NBuilder;
using FizzWare.NBuilder.Generators;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace DailyReport.Services.Tests
{
    [TestClass]
    public class MedicationScheduleServiceTests
    {
        private Mock<IMedicationScheduleRepository> _medicationScheduleRepositoryMock;
        private Mock<IMedicationScheduleItemHistoryRepository> _medicationScheduleItemHistoryRepositoryMock;
        private Mock<IMedicationScheduleItemRepository> _medicationScheduleItemRepositoryMock;
        private Mock<IMedicationScheduleCompleteRepository> _medicationScheduleCompleteRepositoryMock;
        private Mock<IDataContext> _contextMock;

        [TestInitialize]
        public void Initialize()
        {
            _medicationScheduleRepositoryMock = new Mock<IMedicationScheduleRepository>();
            _medicationScheduleItemHistoryRepositoryMock = new Mock<IMedicationScheduleItemHistoryRepository>();
            _medicationScheduleItemRepositoryMock = new Mock<IMedicationScheduleItemRepository>();
            _medicationScheduleCompleteRepositoryMock = new Mock<IMedicationScheduleCompleteRepository>();

            _contextMock = new Mock<IDataContext>();
        }

        #region FindAll
        [TestMethod]
        [TestCategory("MedicationScheduleService.FindAll")]
        public void MedicationScheduleService_FindAll_ShouldReturnAListOfMedications()
        {
            var validMedicationResults = Builder<MedicationScheduleResultModel>.CreateListOfSize(10)
                .Build();

            _medicationScheduleRepositoryMock.Setup(x => x.FindAll()).Returns(validMedicationResults);

            var medicationScheduleService = GetService();
            var result = medicationScheduleService.FindAll();

            Assert.AreEqual(validMedicationResults.Count(), result.Count());
            Assert.AreSame(validMedicationResults, result);
        }
        #endregion

        #region FindUpcomings
        [TestMethod]
        [TestCategory("MedicationScheduleService.FindUpcomings")]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void MedicationScheduleService_FindUpcomings_GivenInvalidOrganizationId_ThrowException()
        {
            var service = GetService();
            service.FindUpcomings(-1);
        }

        [TestMethod]
        [TestCategory("MedicationScheduleService.FindUpcomings")]
        public void MedicationScheduleService_FindUpcomings_ShouldReturnResultsFromRepository()
        {
            var schedules = Builder<MedicationScheduleResultModel>.CreateListOfSize(20).Build();
            _medicationScheduleRepositoryMock.Setup(x => x.FindUpcomings(1)).Returns(schedules);

            var service = GetService();
            var results = service.FindUpcomings(1);

            Assert.AreSame(schedules, results);
        }
        #endregion

        #region GetByPatientId
        [TestMethod]
        [TestCategory("MedicationScheduleService.FindByPatientId")]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void MedicationScheduleService_FindByPatientId_GivenInvalidPatientIdId_ThrowException()
        {
            var service = GetService();
            service.FindByPatientId(-1);
        }

        [TestMethod]
        [TestCategory("MedicationScheduleService.FindByPatientId")]
        public void MedicationScheduleService_FindByPatientId_ShouldReturnResultsFromRepository()
        {
            var schedules = Builder<MedicationScheduleResultModel>.CreateListOfSize(20).Build();
            _medicationScheduleRepositoryMock.Setup(x => x.FindByPatientId(1)).Returns(schedules);

            var service = GetService();
            var results = service.FindByPatientId(1);

            Assert.AreSame(schedules, results);
        }
        #endregion
        
        #region GetById
        [TestMethod]
        [TestCategory("MedicationScheduleService.GetById")]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void MedicationScheduleService_GetById_ShouldThrowExceptionIfIdIsLowerThanOne()
        {
            var medicationScheduleService = GetService();

            medicationScheduleService.GetById(0);
        }

        [TestMethod]
        [TestCategory("MedicationScheduleService.GetById")]
        public void MedicationScheduleService_GetById_ShouldReturnAMedicationScheduleResultModel()
        {
            var validMedicationResult = Builder<MedicationScheduleResultModel>.CreateNew()
                .Build();

            _medicationScheduleRepositoryMock.Setup(x => x.GetById(1)).Returns(validMedicationResult);

            var medicationScheduleService = GetService();
            var result = medicationScheduleService.GetById(1);

            Assert.AreSame(validMedicationResult, result);
        }
        #endregion
        
        #region GetHistories
        [TestMethod]
        [TestCategory("MedicationScheduleService.GetHistories")]
        public void MedicationScheduleService_GetHistories_ShouldReturnAListOfMedications()
        {
            const int medicationScheduleItemId = 1;

            var validMedicationScheduleItemHistoryResults = Builder<MedicationScheduleItemHistoryResultModel>.CreateListOfSize(10)
                .Build();

            _medicationScheduleItemHistoryRepositoryMock.Setup(x => x.FindAll(medicationScheduleItemId)).Returns(validMedicationScheduleItemHistoryResults);

            var medicationScheduleService = GetService();
            var result = medicationScheduleService.GetHistory(medicationScheduleItemId);

            Assert.AreEqual(validMedicationScheduleItemHistoryResults.Count(), result.Count());
            Assert.AreSame(validMedicationScheduleItemHistoryResults, result);
        }

        [TestMethod]
        [TestCategory("MedicationScheduleService.GetHistories")]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void MedicationScheduleService_GetHistories_ShouldThrowExceptionIfIdIsLowerThanOne()
        {
            var medicationScheduleService = GetService();

            medicationScheduleService.GetById(0);
        }
        #endregion

        #region Insert
        [TestMethod]
        [TestCategory("MedicationScheduleService.Insert")]
        public void MedicationScheduleService_Insert_ShouldValidateModel()
        {
            var medicationScheduleService = GetService();

            ExtendedAssert.Throws<ArgumentNullException>(() =>
            {
                medicationScheduleService.Insert(null);
            });

            ExtendedAssert.Throws<ArgumentOutOfRangeException>(() =>
            {
                var model = BuildMedicationScheduleInsertModel();
                model.PatientId = 0;

                medicationScheduleService.Insert(model);
            });

            ExtendedAssert.Throws<ArgumentException>(() =>
            {
                var model = BuildMedicationScheduleInsertModel();
                model.StartDate = DateTime.MinValue;

                medicationScheduleService.Insert(model);
            });
        }

        [TestMethod]
        [TestCategory("MedicationScheduleService.Insert")]
        public void MedicationScheduleService_Insert_ShouldCallInsertOnRepositoryOnce()
        {
            var medicationSchedule = Builder<MedicationSchedule>.CreateNew().Build();
            _medicationScheduleRepositoryMock.Setup(x => x.Insert(It.IsAny<MedicationSchedule>())).Returns(medicationSchedule);

            var medicationScheduleService = GetService();
            var model = BuildMedicationScheduleInsertModel();

            medicationScheduleService.Insert(model);

            _medicationScheduleRepositoryMock.Verify(x => x.Insert(It.IsAny<MedicationSchedule>()), Times.Once);
        }

        [TestMethod]
        [TestCategory("MedicationScheduleService.Insert")]
        public void MedicationScheduleService_Insert_ShouldCallInsertOnMedicationScheduleItemRepository_AsManyTimesThereIsAItem()
        {
            var medicationSchedule = Builder<MedicationSchedule>.CreateNew().Build();
            _medicationScheduleRepositoryMock.Setup(x => x.Insert(It.IsAny<MedicationSchedule>())).Returns(medicationSchedule);

            var medicationScheduleService = GetService();
            var model = BuildMedicationScheduleInsertModel();

            medicationScheduleService.Insert(model);

            _medicationScheduleItemRepositoryMock.Verify(x => x.Insert(It.IsAny<MedicationScheduleItem>()), Times.Exactly(model.Items.Count()));
        }

        [TestMethod]
        [TestCategory("MedicationScheduleService.Insert")]
        public void MedicationScheduleService_Insert_Items_ShouldValidateModel()
        {
            var medicationScheduleService = GetService();

            ExtendedAssert.Throws<ArgumentOutOfRangeException>(() =>
            {
                var model = BuildMedicationScheduleInsertModel();
                model.Items.First().Amouth = 0;

                medicationScheduleService.Insert(model);
            });

            ExtendedAssert.Throws<ArgumentOutOfRangeException>(() =>
            {
                var model = BuildMedicationScheduleInsertModel();
                model.Items.First().MedicationId = 0;

                medicationScheduleService.Insert(model);
            });

            ExtendedAssert.Throws<ArgumentException>(() =>
            {
                var model = BuildMedicationScheduleInsertModel();
                model.Items.First().ExecuteTime = TimeSpan.Zero;

                medicationScheduleService.Insert(model);
            });

            ExtendedAssert.Throws<ArgumentNullException>(() =>
            {
                var model = BuildMedicationScheduleInsertModel();
                model.Items = new MedicationScheduleItemInsertModel[] { null };

                medicationScheduleService.Insert(model);
            });
        }

        [TestMethod]
        [TestCategory("MedicationScheduleService.Insert")]
        public void MedicationScheduleService_Insert_ShouldCallInsertOnMedicationScheduleItemRepository_CreateRepeatInterval_AsManyTimesThereIsAItem_WithRepeat()
        {
            var medicationSchedule = Builder<MedicationSchedule>.CreateNew().Build();
            _medicationScheduleRepositoryMock.Setup(x => x.Insert(It.IsAny<MedicationSchedule>())).Returns(medicationSchedule);
            _medicationScheduleItemRepositoryMock.Setup(x => x.Insert(It.IsAny<MedicationScheduleItem>())).Returns(Builder<MedicationScheduleItem>.CreateNew().Build());
            var medicationScheduleService = GetService();
            var model = BuildMedicationScheduleInsertModel();
            model.Items = Builder<MedicationScheduleItemInsertModel>.CreateListOfSize(2)
                        .All()
                        .With(y =>
                        {
                            y.ExecuteTime = TimeSpan.FromTicks(DateTime.Now.Ticks);
                            y.RepeatInterval = TimeSpan.FromHours(4);
                            return y;
                        })
                        .Build();

            medicationScheduleService.Insert(model);

            _medicationScheduleItemRepositoryMock.Verify(x => x.CreateRepeatInterval(It.IsAny<int>(), It.IsAny<TimeSpan>()), Times.Exactly(model.Items.Count(x => x.RepeatInterval.HasValue)));
        }
        #endregion

        #region CompleteSchedule
        [TestMethod]
        [TestCategory("MedicationScheduleService.CompleteSchedule")]
        public void MedicationScheduleService_CompleteSchedule_ShouldValidateModel()
        {
            var medicationScheduleService = GetService();

            ExtendedAssert.Throws<ArgumentNullException>(() =>
            {
                medicationScheduleService.CompleteSchedule(null);
            });

            ExtendedAssert.Throws<ArgumentOutOfRangeException>(() =>
            {
                var model = BuildMedicationComplete();
                model.MedicationScheduleId = 0;

                medicationScheduleService.CompleteSchedule(model);
            });

            ExtendedAssert.Throws<ArgumentNullException>(() =>
            {
                var model = BuildMedicationComplete();
                model.Description = null;

                medicationScheduleService.CompleteSchedule(model);
            });

            ExtendedAssert.Throws<ArgumentException>(() =>
            {
                var model = BuildMedicationComplete();
                model.Description = string.Empty;

                medicationScheduleService.CompleteSchedule(model);
            });

            ExtendedAssert.Throws<ArgumentOutOfRangeException>(() =>
            {
                var model = BuildMedicationComplete();
                model.EmployeeId = 0;

                medicationScheduleService.CompleteSchedule(model);
            });
        }

        [TestMethod]
        [TestCategory("MedicationScheduleService.CompleteSchedule")]
        public void MedicationScheduleService_CompleteSchedule_ShouldSetCurrentDateTime_WhenMinValue()
        {
            var medicationScheduleService = GetService();

            var model = BuildMedicationComplete();
            model.TimeCreated = DateTime.MinValue;

            medicationScheduleService.CompleteSchedule(model);

            Assert.IsTrue(model.TimeCreated > DateTime.MinValue);
        }

        [TestMethod]
        [TestCategory("MedicationScheduleService.CompleteSchedule")]
        public void MedicationScheduleService_CompleteSchedule_ShouldCallRepository()
        {
            var medicationScheduleService = GetService();

            var model = BuildMedicationComplete();
            medicationScheduleService.CompleteSchedule(model);

            _medicationScheduleCompleteRepositoryMock.Verify(x => x.Insert(It.IsAny<MedicationScheduleComplete>()), Times.Once);
        }
        #endregion

        #region AddHistoryItem
        [TestMethod]
        [TestCategory("MedicationScheduleService.AddHistoryItem")]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AddHistoryItem_GivenNullSchedule_ThrowException()
        {
            var service = GetService();
            service.AddHistoryItem(null, 3);
        }

        [TestMethod]
        [TestCategory("MedicationScheduleService.AddHistoryItem")]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void AddHistoryItem_GivenInvalidEmployeeId_ThrowException()
        {
            var service = GetService();
            service.AddHistoryItem(new MedicationScheduleItemHistory(), -1);
        }

        [TestMethod]
        [TestCategory("MedicationScheduleService.AddHistoryItem")]
        public void AddHistoryItem_GivenMinValue_AsTimeCreated_ShouldSetToCurrentDate()
        {
            var historyItem = Builder<MedicationScheduleItemHistory>.CreateNew()
                    .With(x => x.TimeCreated, DateTime.MinValue)
                    .Build();

            _medicationScheduleItemHistoryRepositoryMock.Setup(x => x.Insert(It.IsAny<MedicationScheduleItemHistory>(), It.IsAny<int>()))
                .Returns(historyItem);

            var service = GetService();

            var result = service.AddHistoryItem(historyItem, 1);

            Assert.IsTrue(result.TimeCreated > DateTime.MinValue);
        }

        [TestMethod]
        [TestCategory("MedicationScheduleService.AddHistoryItem")]
        public void AddHistoryItem_GivenValidScheduleAndEmployeeId_ShouldCallRepository()
        {
            var historyItem = Builder<MedicationScheduleItemHistory>.CreateNew()
                    .Build();

            var service = GetService();

            service.AddHistoryItem(historyItem, 1);

            _medicationScheduleItemHistoryRepositoryMock.Verify(x=> x.Insert(historyItem, 1), Times.Once);
        }
        #endregion

        private MedicationScheduleComplete BuildMedicationComplete()
        {
            return Builder<MedicationScheduleComplete>.CreateNew().Build();
        }

        private MedicationScheduleInsertModel BuildMedicationScheduleInsertModel()
        {
            return Builder<MedicationScheduleInsertModel>.CreateNew()
                .With(x =>
                {
                    x.Items = Builder<MedicationScheduleItemInsertModel>.CreateListOfSize(4)
                        .All()
                        .With(y =>
                        {
                            y.ExecuteTime = TimeSpan.FromTicks(DateTime.Now.Ticks);
                            y.RepeatInterval = null;
                            return y;
                        })
                        .Build();
                    return x;
                }).Build();
        }

        private IMedicationScheduleService GetService()
        {
            _contextMock.Setup(x => x.MedicationSchedules).Returns(_medicationScheduleRepositoryMock.Object);
            _contextMock.Setup(x => x.MedicationScheduleItemHistories).Returns(_medicationScheduleItemHistoryRepositoryMock.Object);
            _contextMock.Setup(x => x.MedicationScheduleItems).Returns(_medicationScheduleItemRepositoryMock.Object);
            _contextMock.Setup(x => x.MedicationScheduleCompletes).Returns(_medicationScheduleCompleteRepositoryMock.Object);

            return new MedicationScheduleService(_contextMock.Object);
        }
    }
}
