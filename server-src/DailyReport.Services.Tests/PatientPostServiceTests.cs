﻿using DailyReport.Domain.Infrastructure;
using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using DailyReport.Tests.Helpers;
using FizzWare.NBuilder;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Services.Tests
{
    [TestClass]
    public class PatientPostServiceTests
    {
        private Mock<IDataContext> _contextMock;
        private IPatientPostService _patientPostService;
        private Mock<IPatientPostRepository> _patientPostRepositoryMock;
        private Mock<IPatientToPatientPostRepository> _patientToPatientPostRepositoryMock;

        [TestInitialize]
        public void Initialize()
        {
            _contextMock = new Mock<IDataContext>();

            _patientPostRepositoryMock = new Mock<IPatientPostRepository>();
            _patientPostRepositoryMock.Setup(x => x.Insert(It.IsAny<PatientPost>())).Returns(GetValidPatientPost());

            _patientToPatientPostRepositoryMock = new Mock<IPatientToPatientPostRepository>();

            _contextMock.Setup(x => x.PatientPosts).Returns(_patientPostRepositoryMock.Object);
            _contextMock.Setup(x => x.PatientPostToPatients).Returns(_patientToPatientPostRepositoryMock.Object);

            _patientPostService = new PatientPostService(_contextMock.Object);
        }

        [TestMethod]
        [TestCategory("PatientPostService.Insert")]
        public void PatientPostService_Insert_ShouldValidateModel()
        {
            ExtendedAssert.Throws<ArgumentNullException>(() =>
            {
                _patientPostService.Insert(null);
            });

            ExtendedAssert.Throws<ArgumentNullException>(() =>
            {
                var model = GetValidPatientPostInsertModel();
                model.Description = null;

                _patientPostService.Insert(model);
            });

            ExtendedAssert.Throws<ArgumentException>(() =>
            {
                var model = GetValidPatientPostInsertModel();
                model.Description = string.Empty;

                _patientPostService.Insert(model);
            });

            ExtendedAssert.Throws<ArgumentOutOfRangeException>(() =>
            {
                var model = GetValidPatientPostInsertModel();
                model.EmployeeId = 0;

                _patientPostService.Insert(model);
            });

            ExtendedAssert.Throws<ArgumentNullException>(() =>
            {
                var model = GetValidPatientPostInsertModel();
                model.Patients = null;

                _patientPostService.Insert(model);
            });

            ExtendedAssert.Throws<ArgumentException>(() =>
            {
                var model = GetValidPatientPostInsertModel();
                model.Patients = new int[0];

                _patientPostService.Insert(model);
            });

            ExtendedAssert.Throws<ArgumentOutOfRangeException>(() =>
            {
                var model = GetValidPatientPostInsertModel();
                model.OrganizationId = 0;

                _patientPostService.Insert(model);
            });
        }

        [TestMethod]
        [TestCategory("PatientPostService.Insert")]
        public void PatientPostService_Insert_ShouldInsertPatientPost()
        {
            var insertModel = GetValidPatientPostInsertModel();

            _patientPostService.Insert(insertModel);

            _patientPostRepositoryMock.Verify(x => x.Insert(It.IsAny<PatientPost>()), Times.Once);
        }

        [TestMethod]
        [TestCategory("PatientPostService.Insert")]
        public void PatientPostService_Insert_ShouldInsertAllReferenceForPatients()
        {
            var numberOfPatients = 4;

            var insertModel = GetValidPatientPostInsertModel(numberOfPatients);

            _patientPostService.Insert(insertModel);

            _patientToPatientPostRepositoryMock.Verify(x => x.Insert(It.IsAny<int>(), It.IsAny<int>()), Times.Exactly(numberOfPatients));
        }

        [TestMethod]
        [TestCategory("PatientPostService.FindAll")]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void PatientPostService_FindAll_ShouldFailIfOrganizationIdIsLowerThanOne()
        {
            _patientPostService.FindAll(0);
        }

        [TestMethod]
        [TestCategory("PatientPostService.FindAll")]
        public void PatientPostService_FindAll_ShouldReturnAListOfPatientPosts()
        {
            var patientPosts = Builder<PatientPostResultModel>.CreateListOfSize(10).All().With(x =>
            {
                x.PatientPost = Builder<PatientPost>.CreateNew().With(y =>
                {
                    y.OrganizationId = 1;
                    return y;
                }).Build();

                x.Employee = Builder<Employee>.CreateNew().With(y =>
                {
                    y.OrganizationId = 1;
                    return y;
                }).Build();

                return x;
            }).Build();

            _patientPostRepositoryMock.Setup(x => x.FindAll(1)).Returns(patientPosts);

            var returnedPatientPosts = _patientPostService.FindAll(1);

            _patientPostRepositoryMock.Verify(x => x.FindAll(1), Times.Once);

            Assert.AreSame(patientPosts, returnedPatientPosts);
        }

        [TestMethod]
        [TestCategory("PatientPostService.FindAllByPatient")]
        public void PatientPostService_FindAllByPatient_ShouldReturnAListOfPatientPosts()
        {
            var patientPosts = Builder<PatientPostResultModel>.CreateListOfSize(10).All().With(x =>
            {
                x.PatientPost = Builder<PatientPost>.CreateNew().With(y =>
                {
                    y.OrganizationId = 1;
                    return y;
                }).Build();

                x.Employee = Builder<Employee>.CreateNew().With(y =>
                {
                    y.OrganizationId = 1;
                    return y;
                }).Build();

                x.Patients = Builder<Patient>.CreateListOfSize(3).All().With(y =>
                {
                    y.Id = 3;
                    return y;
                }).Build();

                return x;
            }).Build();

            _patientPostRepositoryMock.Setup(x => x.FindAllByPatient(1)).Returns(patientPosts);

            var returnedPatientPosts = _patientPostService.FindAllByPatient(1);

            _patientPostRepositoryMock.Verify(x => x.FindAllByPatient(1), Times.Once);

            Assert.AreSame(patientPosts, returnedPatientPosts);
        }

        [TestMethod]
        [TestCategory("PatientPostService.FindAllByPatient")]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void PatientPostService_FindAllByPatient_ShouldThrowExceptionIfPatientIdIsLowerThanOne()
        {
            _patientPostService.FindAllByPatient(0);
        }

        private PatientPostInsertModel GetValidPatientPostInsertModel(int numberOfPatients = 10)
        {
            return Builder<PatientPostInsertModel>.CreateNew()
                .With(x =>
                {
                    x.Patients = Builder<int>.CreateListOfSize(numberOfPatients).Build();
                    return x;
                })
                .Build();
        }

        private PatientPost GetValidPatientPost()
        {
            return Builder<PatientPost>.CreateNew().Build();
        }
    }
}
