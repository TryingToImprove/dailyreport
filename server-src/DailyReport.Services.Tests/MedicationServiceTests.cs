﻿using System;
using System.Linq;
using DailyReport.Domain.Infrastructure;
using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using DailyReport.Tests.Helpers;
using FizzWare.NBuilder;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace DailyReport.Services.Tests
{
    [TestClass]
    public class MedicationServiceTests
    {
        private Mock<IMedicationRepository> _medicationRepositoryMock;
        private Mock<IMedicationBrandRepository> _medicationBrandRepositoryMock;
        private Mock<IMeasureRepository> _measureRepositoryMock;
        private Mock<IMedicationTypeRepository> _medicationTypeRepositoryMock;
        private Mock<IDataContext> _contextMock;

        [TestInitialize]
        public void Initialize()
        {
            _medicationBrandRepositoryMock = new Mock<IMedicationBrandRepository>();
            _medicationRepositoryMock = new Mock<IMedicationRepository>();
            _measureRepositoryMock = new Mock<IMeasureRepository>();
            _medicationTypeRepositoryMock = new Mock<IMedicationTypeRepository>();

            _contextMock = new Mock<IDataContext>();
        }

        [TestMethod]
        [TestCategory("MedicationService.FindAll")]
        public void MedicationService_FindAll_ShouldReturnAListOfMedications()
        {
            var validMedications = Builder<MedicationResultModel>.CreateListOfSize(10)
                .Build();

            _medicationRepositoryMock.Setup(x => x.FindAll()).Returns(validMedications);

            var medicationService = GetService();
            var result = medicationService.FindAll();

            Assert.AreEqual(validMedications.Count(), result.Count());
            Assert.AreSame(validMedications, result);
        }

        [TestMethod]
        [TestCategory("MedicationService.FindBrands")]
        public void MedicationService_FindBrands_ShouldReturnAListOfMedicationBrands()
        {
            var validMedicationBrands = Builder<MedicationBrand>.CreateListOfSize(10)
                .Build();

            _medicationBrandRepositoryMock.Setup(x => x.FindAll()).Returns(validMedicationBrands);

            var medicationService = GetService();
            var result = medicationService.FindBrands();

            Assert.AreEqual(validMedicationBrands.Count(), result.Count());
            Assert.AreSame(validMedicationBrands, result);
        }

        [TestMethod]
        [TestCategory("MedicationService.GetMeasures")]
        public void MedicationService_GetMeasures_ShouldReturnAListOfMeasures()
        {
            var measures = Builder<Measure>.CreateListOfSize(10).Build();
            _measureRepositoryMock.Setup(x => x.FindAll()).Returns(measures);

            var medicationService = GetService();
            var result = medicationService.GetMeasures();

            Assert.IsNotNull(result);
            Assert.AreEqual(measures.Count(), result.Count());
        }

        [TestMethod]
        [TestCategory("MedicationService.GetMedicationTypes")]
        public void MedicationService_GetMedicationTypes_ShouldReturnAListOfMedicationTypes()
        {
            var medicationTypes = Builder<MedicationType>.CreateListOfSize(10).Build();
            _medicationTypeRepositoryMock.Setup(x => x.FindAll()).Returns(medicationTypes);

            var medicationService = GetService();
            var result = medicationService.GetMedicationTypes();

            Assert.IsNotNull(result);
            Assert.AreEqual(medicationTypes.Count(), result.Count());
        }

        [TestMethod]
        [TestCategory("MedicationService.Insert")]
        public void MedicationService_Insert_ShouldValidateMedication()
        {
            var medicationService = GetService();

            ExtendedAssert.Throws<ArgumentNullException>(() =>
            {
                medicationService.Insert(BuildMedicationInsertModel(null, null));
            });

            ExtendedAssert.Throws<ArgumentNullException>(() =>
            {
                var medication = GetValidMedication();
                medication.Name = null;

                medicationService.Insert(BuildMedicationInsertModel(medication, null));
            });

            ExtendedAssert.Throws<ArgumentException>(() =>
            {
                var medication = GetValidMedication();
                medication.Name = String.Empty;

                medicationService.Insert(BuildMedicationInsertModel(medication, null));
            });

            ExtendedAssert.Throws<ArgumentException>(() =>
            {
                var medication = GetValidMedication();
                medication.Name = " ";

                medicationService.Insert(BuildMedicationInsertModel(medication, null));
            });
        }

        [TestMethod]
        [TestCategory("MedicationService.Insert")]
        [ExpectedException(typeof(InvalidOperationException))]
        public void MedicationService_Insert_IfMedicationMedicationBrandIdIsZero_And_MedicationBrandIsNull_ThrowException()
        {
            var medicationService = GetService();

            var medication = GetValidMedication();
            medication.MedicationBrandId = 0;

            medicationService.Insert(BuildMedicationInsertModel(medication, null));
        }

        [TestMethod]
        [TestCategory("MedicationService.Insert")]
        [ExpectedException(typeof(ArgumentNullException))]
        public void MedicationService_Insert_ShouldThrowExceptionIfModelIsNull()
        {
            var medicationService = GetService();

            medicationService.Insert(null);
        }

        [TestMethod]
        [TestCategory("MedicationService.Insert")]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void MedicationService_Insert_GivenInvalidWeight_ThrowsException()
        {
            var model = BuildMedicationInsertModel(GetValidMedication(), GetValidMedicationBrand());
            model.Medication.Weight = 0;

            var medicationService = GetService();

            medicationService.Insert(model);
        }

        [TestMethod]
        [TestCategory("MedicationService.Insert")]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void MedicationService_Insert_GivenInvalidMedicationType_ThrowsException()
        {
            var model = BuildMedicationInsertModel(GetValidMedication(), GetValidMedicationBrand());
            model.Medication.MedicationTypeId = 0;

            var medicationService = GetService();

            medicationService.Insert(model);
        }

        [TestMethod]
        [TestCategory("MedicationService.Insert")]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void MedicationService_Insert_GivenInvalidMeasure_ThrowsException()
        {
            var model = BuildMedicationInsertModel(GetValidMedication(), GetValidMedicationBrand());
            model.Medication.MeasureId = 0;

            var medicationService = GetService();

            medicationService.Insert(model);
        }

        [TestMethod]
        [TestCategory("MedicationService.Insert")]
        public void MedicationService_Insert_IfMedicationMedicationBrandIdIsZero_ShouldValidateMedicationBrand()
        {
            var medicationService = GetService();

            var medication = GetValidMedication();
            medication.MedicationBrandId = 0;

            ExtendedAssert.Throws<ArgumentNullException>(() =>
            {
                var medicationBrand = GetValidMedicationBrand();
                medication.Name = null;

                medicationService.Insert(BuildMedicationInsertModel(medication, medicationBrand));
            });

            ExtendedAssert.Throws<ArgumentException>(() =>
            {
                var medicationBrand = GetValidMedicationBrand();
                medicationBrand.Name = String.Empty;

                medicationService.Insert(BuildMedicationInsertModel(medication, medicationBrand));
            });

            ExtendedAssert.Throws<ArgumentException>(() =>
            {
                var medicationBrand = GetValidMedicationBrand();
                medicationBrand.Name = " ";

                medicationService.Insert(BuildMedicationInsertModel(medication, medicationBrand));
            });
        }

        [TestMethod]
        [TestCategory("MedicationService.Insert")]
        public void MedicationService_Insert_IfMedicationMedicationBrandIdIsZero_ShouldInsertMedicationBrand()
        {
            var medicationBrand = GetValidMedicationBrand();

            var medicationBrandResult = GetValidMedicationBrand();
            _medicationBrandRepositoryMock.Setup(x => x.Insert(medicationBrand)).Returns(medicationBrandResult);

            var medication = GetValidMedication();
            medication.MedicationBrandId = 0;

            _medicationRepositoryMock.Setup(x => x.Insert(medication)).Returns(GetMedicationResult());

            var medicationService = GetService();


            medicationService.Insert(BuildMedicationInsertModel(medication, medicationBrand));

            _medicationBrandRepositoryMock.Verify(x => x.Insert(medicationBrand), Times.Once);
        }

        [TestMethod]
        [TestCategory("MedicationService.Insert")]
        public void MedicationService_Insert_GivenMedicationBrand_ShouldNotInsertMedicationBrand()
        {
            var medicationBrand = GetValidMedicationBrand();

            var medication = GetValidMedication();
            medication.MedicationBrandId = medicationBrand.Id;

            _medicationBrandRepositoryMock.Setup(x => x.GetById(medication.MedicationBrandId)).Returns(medicationBrand);
            _medicationRepositoryMock.Setup(x => x.Insert(medication)).Returns(GetMedicationResult());

            var medicationService = GetService();

            medicationService.Insert(BuildMedicationInsertModel(medication, null));

            _medicationBrandRepositoryMock.Verify(x => x.Insert(It.IsAny<MedicationBrand>()), Times.Never);
        }

        [TestMethod]
        [TestCategory("MedicationService.Insert")]
        public void MedicationService_Insert_GivenMedicationBrand_ShouldGetMedicationBrandById()
        {
            var medicationBrand = GetValidMedicationBrand();

            var medication = GetValidMedication();
            medication.MedicationBrandId = medicationBrand.Id;

            _medicationBrandRepositoryMock.Setup(x => x.GetById(medication.MedicationBrandId)).Returns(medicationBrand);
            _medicationRepositoryMock.Setup(x => x.Insert(medication)).Returns(GetMedicationResult());

            var medicationService = GetService();

            medicationService.Insert(BuildMedicationInsertModel(medication, null));

            _medicationBrandRepositoryMock.Verify(x => x.GetById(medicationBrand.Id), Times.Once);
        }

        private MedicationResultModel GetMedicationResult()
        {
            return Builder<MedicationResultModel>.CreateNew().Build();
        }

        private MedicationInsertModel BuildMedicationInsertModel(Medication medication, MedicationBrand medicationBrand)
        {
            return new MedicationInsertModel
            {
                Medication = medication,
                Brand = medicationBrand
            };
        }

        private Medication GetValidMedication()
        {
            return Builder<Medication>.CreateNew().Build();
        }

        private MedicationBrand GetValidMedicationBrand()
        {
            return Builder<MedicationBrand>.CreateNew().Build();
        }

        private IMedicationService GetService()
        {
            _contextMock.Setup(x => x.MedicationBrands).Returns(_medicationBrandRepositoryMock.Object);
            _contextMock.Setup(x => x.Medications).Returns(_medicationRepositoryMock.Object);
            _contextMock.Setup(x => x.Measures).Returns(_measureRepositoryMock.Object);
            _contextMock.Setup(x => x.MedicationTypes).Returns(_medicationTypeRepositoryMock.Object);

            return new MedicationService(_contextMock.Object);
        }
    }
}
