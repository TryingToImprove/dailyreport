﻿using System;
using DailyReport.Domain.Models;
using DailyReport.Tests.Helpers.Fakes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DailyReport.Domain.Models.Core;
using Moq;
using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Infrastructure;
using DailyReport.Tests.Helpers;
using FizzWare.NBuilder;

namespace DailyReport.Services.Tests
{
    [TestClass]
    public class ContactInformationServiceTests
    {
        #region GetTypes
        [TestMethod]
        public void GetTypes_ShouldReturnResultFromRepository()
        {
            var types = FakeContactInformationType.CreateTypes();

            var dataContext = FakeDataContext.Create()
                .With(x => x.ContactInformationTypes.Setup(y => y.FindAll()).Returns(types));

            var contactInformationService = GetService(dataContext);

            var results = contactInformationService.GetTypes();

            Assert.AreSame(types, results);
        }
        #endregion

        #region GetById
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void GetById_WhenContactInformationIdIsLowerThanOne_Throws()
        {
            var contactInformationService = GetService();
            contactInformationService.GetById(0);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void GetById_ShouldReturnResultFromRepository()
        {
            var contactInformation = FakeContactInformation.CreateResultModel();

            var dataContext = FakeDataContext.Create()
                .With(x => x.ContactInformations.Setup(y => y.GetById(1)).Returns(contactInformation));

            var contactInformationService = GetService(dataContext);
            var result = contactInformationService.GetById(1);

            Assert.AreSame(contactInformation, result);
        }

        #endregion

        private ContactInformationService GetService(FakeDataContext fakeDataContext = null)
        {
            if (fakeDataContext == null)
                fakeDataContext = FakeDataContext.Create();

            return new ContactInformationService(fakeDataContext.Object);
        }
    }
}
