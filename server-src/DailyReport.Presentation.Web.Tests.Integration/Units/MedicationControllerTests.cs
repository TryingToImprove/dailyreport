﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Models.Core;
using DailyReport.Presentation.Web.Controllers;
using DailyReport.Presentation.Web.Models;
using DailyReport.Services;
using Dapper;
using FizzWare.NBuilder;
using FizzWare.NBuilder.Generators;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DailyReport.Presentation.Web.Tests.Integration.Units
{
    [TestClass]
    public class MedicationControllerTests : BaseControllerTests<MedicationController>
    {
        private IMedicationService _medicationService;

        protected override void Setup()
        {
            _medicationService = new MedicationService(DataContext);
        }

        [TestMethod]
        public void Create_GET_ShouldReturnMeasures_AsSelectList()
        {
            var measures = _medicationService.GetMeasures();
            var model = GetCreateViewModel(GetController(), x => x.Create());

            Assert.IsNotNull(model.MeasureList);
            Assert.AreEqual(measures.Count(), model.MeasureList.Count());
        }

        [TestMethod]
        public void Create_GET_ShouldReturnMedicationTypes_AsSelectList()
        {
            var medicationTypes = _medicationService.GetMedicationTypes();
            var model = GetCreateViewModel(GetController(), x => x.Create());

            Assert.IsNotNull(model.MedicationTypeList);
            Assert.AreEqual(medicationTypes.Count(), model.MedicationTypeList.Count());
        }

        [TestMethod]
        public void Create_POST_GivenAValidViewModel_AndUnkownMedication_ShouldCreateMedication()
        {
            var viewModel = Builder<MedicationCreateViewModel>.CreateNew()
                .With(x =>
                {
                    x.Name = GetRandom.String(25);
                    return x;
                })
                .Build();

            var controller = GetController();
            controller.Create(viewModel);

            var medications = Connection.ExecuteScalar<int>("SELECT COUNT(Id) FROM Medication WHERE Medication.Name = @Name", new
            {
                Name = viewModel.Name
            });
            Assert.AreEqual(1, medications);
        }

        [TestMethod]
        public void Create_POST_GivenAValidViewModel_AndUnkownMedicationBrand_ShouldCreateMedicationBrand()
        {
            var viewModel = Builder<MedicationCreateViewModel>.CreateNew()
                .With(x =>
                {
                    x.BrandName = GetRandom.String(25);
                    return x;
                })
                .Build();

            var controller = GetController();
            controller.Create(viewModel);

            var medicationBrands = Connection.ExecuteScalar<int>("SELECT COUNT(Id) FROM MedicationBrand WHERE MedicationBrand.Name = @Name", new
            {
                Name = viewModel.BrandName
            });
            Assert.AreEqual(1, medicationBrands);
        }

        protected override MedicationController CreateController()
        {
            return new MedicationController(_medicationService);
        }

        private static MedicationCreateViewModel GetCreateViewModel(MedicationController controller, Func<MedicationController, ActionResult> actionFunc)
        {
            var result = actionFunc.Invoke(controller) as ViewResult;
            var model = result.Model as MedicationCreateViewModel;

            return model;
        }
    }
}
