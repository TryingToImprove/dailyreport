﻿using System;
using System.Linq;
using System.Web.Mvc;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Presentation.Web.Areas.Administration.Controllers;
using DailyReport.Presentation.Web.Areas.Administration.Models;
using DailyReport.Presentation.Web.Areas.PatientModule.Models;
using DailyReport.Presentation.Web.Tests.Integration.Helpers;
using DailyReport.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DailyReport.Presentation.Web.Tests.Integration.Units.Administration
{
    [TestClass]
    public class PatientControllerTests : BaseControllerTests<PatientController>
    {
        private IPatientService _patientService;
        private IOrganizationService _organizationService;
        private IPatientPostService _patientPostService;
        private IMedicationScheduleService _medicationScheduleService;
        private IEmployeeService _employeeService;
        private IContactInformationService _contactInformationService;

        protected override void Setup()
        {
            _patientService = new PatientService(DataContext, null);
            _organizationService = new OrganizationService(DataContext);
            _patientPostService = new PatientPostService(DataContext);
            _medicationScheduleService = new MedicationScheduleService(DataContext);
            _employeeService = new EmployeeService(DataContext);
            _contactInformationService = new ContactInformationService(DataContext);
        }

        protected override PatientController CreateController()
        {
            return new PatientController(_patientService, _organizationService, _patientPostService, _medicationScheduleService, _employeeService, _contactInformationService);
        }

        #region Logbook
        [TestMethod]
        public void Logbook_GivenAValidOrganizationId_OrganizationShouldNotBeNull()
        {
            OrganizationTestHelper.OrganizationShouldNotBeNull(GetController(), controller => controller.Logbook(1, 1));
        }

        [TestMethod]
        public void Logbook_GivenAValidOrganizationId_ShouldReturnOrganizationWithThatId()
        {
            OrganizationTestHelper.ShouldReturnCorrectOrganization(GetController(), controller => controller.Logbook(1, 1));
        }

        [TestMethod]
        public void Logbook_GivenAValidPatientId_PatientShouldNotBeNull()
        {
            PatientTestHelper.PatientShouldNotBeNull(GetController(), controller => controller.Logbook(1, 1));
        }

        [TestMethod]
        public void Logbook_GivenAValidPatientId_ShouldReturnPatientWithThatId()
        {
            PatientTestHelper.ShouldReturnCorrectPatient(GetController(), controller => controller.Logbook(1, 1));
        }

        [TestMethod]
        public void Logbook_ShouldReturnAListOfPosts()
        {
            var controller = GetController();
            var result = controller.Logbook(1, 1) as ViewResult;
            var model = result.Model as PatientLogbookViewModel;

            Assert.IsNotNull(model.Posts);
        }
        #endregion

        #region Details
        [TestMethod]
        public void Details_GivenAValidOrganizationId_OrganizationShouldNotBeNull()
        {
            OrganizationTestHelper.OrganizationShouldNotBeNull(GetController(), controller => controller.Details(1, 1));
        }

        [TestMethod]
        public void Details_GivenAValidOrganizationId_ShouldReturnOrganizationWithThatId()
        {
            OrganizationTestHelper.ShouldReturnCorrectOrganization(GetController(), controller => controller.Details(1, 1));
        }

        [TestMethod]
        public void Details_GivenAValidPatientId_PatientShouldNotBeNull()
        {
            PatientTestHelper.PatientShouldNotBeNull(GetController(), controller => controller.Details(1, 1));
        }

        [TestMethod]
        public void Details_GivenAValidPatientId_ShouldReturnPatientWithThatId()
        {
            PatientTestHelper.ShouldReturnCorrectPatient(GetController(), controller => controller.Details(1, 1));
        }

        [TestMethod]
        public void Details_ShouldReturnAListOfPosts()
        {
            var controller = GetController();
            var result = controller.Details(1, 1) as ViewResult;
            var model = result.Model as PatientDetailsViewModel;

            Assert.IsNotNull(model.Posts);
        }
        #endregion

        #region Edit
        [TestMethod]
        public void Edit_GivenAValidOrganizationId_OrganizationShouldNotBeNull()
        {
            OrganizationTestHelper.OrganizationShouldNotBeNull(GetController(), controller => controller.Edit(1, 1));
        }

        [TestMethod]
        public void Edit_GivenAValidOrganizationId_ShouldReturnOrganizationWithThatId()
        {
            OrganizationTestHelper.ShouldReturnCorrectOrganization(GetController(), controller => controller.Edit(1, 1));
        }

        [TestMethod]
        public void Edit_GivenAValidPatientId_PatientShouldNotBeNull()
        {
            PatientTestHelper.PatientShouldNotBeNull(GetController(), controller => controller.Edit(1, 1));
        }

        [TestMethod]
        public void Edit_GivenAValidPatientId_ShouldReturnPatientWithThatId()
        {
            PatientTestHelper.ShouldReturnCorrectPatient(GetController(), controller => controller.Edit(1, 1));
        }

        [TestMethod]
        public void Edit_GivenAValidPatientId_ContactPersonShouldNotBeNull()
        {
            var controller = GetController();

            var result = controller.Edit(1, 1) as ViewResult;
            var model = result.Model as PatientEditViewModel;

            Assert.IsNotNull(model.ContactPersons);
        }

        [TestMethod]
        public void Edit_GivenAValidPatientId_ShouldReturnAListOfContactPersons()
        {
            var controller = GetController();

            var result = controller.Edit(1, 1) as ViewResult;
            var model = result.Model as PatientEditViewModel;

            Assert.AreEqual(1, model.ContactPersons.Count());
        }
        #endregion
    }
}
