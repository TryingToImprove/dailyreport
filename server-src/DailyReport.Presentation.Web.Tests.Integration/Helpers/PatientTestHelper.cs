﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using DailyReport.Presentation.Web.Areas.Administration.Models;
using DailyReport.Presentation.Web.Areas.PatientModule.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DailyReport.Presentation.Web.Tests.Integration.Helpers
{
    public static class PatientTestHelper
    {
        public static void PatientShouldNotBeNull<T>(T controller, Func<T, ActionResult> actionFunc)
        {
            var result = actionFunc.Invoke(controller) as ViewResult;
            var model = result.Model as PatientBaseViewModel;

            Assert.IsNotNull(model.Patient);
        }

        public static void ShouldReturnCorrectPatient<T>(T controller, Func<T, ActionResult> actionFunc)
        {
            var result = actionFunc.Invoke(controller) as ViewResult;
            var model = result.Model as PatientBaseViewModel;

            Assert.AreEqual(1, model.Patient.Id);
        }
    }
}
