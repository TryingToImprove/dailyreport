﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using DailyReport.Presentation.Web.Areas.Administration.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DailyReport.Presentation.Web.Tests.Integration.Helpers
{
    public static class OrganizationTestHelper
    {
        public static void OrganizationShouldNotBeNull<T>(T controller, Func<T, ActionResult> actionFunc)
        {
            var result = actionFunc.Invoke(controller) as ViewResult;
            var model = result.Model as OrganizationBaseViewModel;

            Assert.IsNotNull(model.Organization);
        }

        public static void ShouldReturnCorrectOrganization<T>(T controller, Func<T, ActionResult> actionFunc)
        {
            var result = actionFunc.Invoke(controller) as ViewResult;
            var model = result.Model as OrganizationBaseViewModel;

            Assert.AreEqual(1, model.Organization.Id);
        }
    }
}
