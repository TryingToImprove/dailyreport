﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using DailyReport.DataAccess;
using DailyReport.Domain.Infrastructure;
using DailyReport.Presentation.Web.App_Start;
using DailyReport.Tests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DailyReport.Presentation.Web.Tests.Integration
{
    public abstract class BaseControllerTests<T> where T : Controller
    {
        protected IDbConnection Connection;
        protected IDataContext DataContext;

        protected virtual void Setup()
        {
        }

        protected abstract T CreateController();

        [TestInitialize]
        public void Initialize()
        {
            Connection = TestDatabase.Create();

            DataContext = DataContextHelper.Create(Connection);

            AutomapperWebConfiguration.Configure();

            Setup();
        }


        [TestCleanup]
        public void Teardown()
        {
            DataContext = null;

            Connection.Close();
            Connection.Dispose();
            Connection = null;
        }

        protected T GetController(bool invalidModelState = false)
        {
            var controller = CreateController();

            if (invalidModelState)
                controller.ModelState.AddModelError("test", "test");

            return controller;
        }
    }
}
