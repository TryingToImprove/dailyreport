﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CuttingEdge.Conditions;
using DailyReport.Domain.Infrastructure;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Models;

namespace DailyReport.Services
{
    public class ContactContactInformationRelationshipService : BaseContactInformationRelationshipService, IContactInformationRelationshipService
    {
        private readonly IDataContext _context;

        public ContactContactInformationRelationshipService(IDataContext context)
            : base(context)
        {
            _context = context;
        }

        public IEnumerable<ContactInformationResultModel> FindAll(int contactId)
        {
            Condition.Requires(contactId).IsGreaterThan(0);

            return _context.ContactInformations.FindContactContactInformations(contactId);
        }

        public override ContactInformationResultModel Add(int contactId, int creatorEmployeeId, int contactInformationTypeId, string data)
        {
            Condition.Requires(contactId).IsGreaterThan(0);
            Condition.Requires(contactInformationTypeId).IsGreaterThan(0);
            Condition.Requires(data).IsNotNullOrWhiteSpace();

            using (var transaction = _context.BeginTransaction())
            {
                var contactInformation = Insert(creatorEmployeeId, contactInformationTypeId, data);

                _context.ContactInformations.RegisterContactRelationship(contactInformation.Id, contactId);

                return contactInformation;
            }
        }
    }
}
