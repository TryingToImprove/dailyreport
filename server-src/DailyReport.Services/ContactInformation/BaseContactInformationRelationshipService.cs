using System;
using CuttingEdge.Conditions;
using DailyReport.Domain.Infrastructure;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;

namespace DailyReport.Services
{
    public abstract class BaseContactInformationRelationshipService
    {
        private readonly IDataContext _context;

        protected BaseContactInformationRelationshipService(IDataContext context)
        {
            _context = context;
        }

        protected ContactInformationResultModel Insert(int creatorEmployeeId, int contactInformationTypeId, string data)
        {
            Condition.Requires(contactInformationTypeId).IsGreaterThan(0);
            Condition.Requires(data).IsNotNullOrWhiteSpace();
            Condition.Requires(creatorEmployeeId).IsGreaterThan(0);

            var timeCreated = DateTime.Now;

            return _context.ContactInformations.Insert(new ContactInformation()
            {
                ContactInformationTypeId = contactInformationTypeId,
                Data = data,
                TimeCreated = timeCreated,
                EmployeeId = creatorEmployeeId
            });
        }

        public abstract ContactInformationResultModel Add(int relationshipId, int employeeId, int contactInformationTypeId, string data);

        public virtual ContactInformationResultModel Add(int relationshipId, int employeeId, string dataType, string data)
        {
            Condition.Requires(dataType).IsNotNullOrWhiteSpace();

            var contactInformationType = _context.ContactInformationTypes.Find(dataType);

            return Add(relationshipId, employeeId, contactInformationType.Id, data);
        }
    }
}