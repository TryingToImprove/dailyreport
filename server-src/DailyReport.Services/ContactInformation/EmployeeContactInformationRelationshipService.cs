﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CuttingEdge.Conditions;
using DailyReport.Domain.Infrastructure;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Models;

namespace DailyReport.Services
{
    public class EmployeeContactInformationRelationshipService : BaseContactInformationRelationshipService, IContactInformationRelationshipService
    {
        private readonly IDataContext _context;

        public EmployeeContactInformationRelationshipService(IDataContext context)
            : base(context)
        {
            _context = context;
        }

        public IEnumerable<ContactInformationResultModel> FindAll(int employeeId)
        {
            Condition.Requires(employeeId).IsGreaterThan(0);

            return _context.ContactInformations.FindEmployeeContactInformations(employeeId);
        }

        public override ContactInformationResultModel Add(int employeeId, int creatorEmployeeId, int contactInformationTypeId, string data)
        {
            Condition.Requires(employeeId).IsGreaterThan(0);
            Condition.Requires(contactInformationTypeId).IsGreaterThan(0);
            Condition.Requires(data).IsNotNullOrWhiteSpace();

            using (var transaction = _context.BeginTransaction())
            {
                var contactInformation = Insert(creatorEmployeeId, contactInformationTypeId, data);

                _context.ContactInformations.RegisterEmployeeRelationship(contactInformation.Id, employeeId);

                return contactInformation;
            }
        }
    }
}
