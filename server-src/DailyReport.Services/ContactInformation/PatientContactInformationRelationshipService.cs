﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CuttingEdge.Conditions;
using DailyReport.Domain.Infrastructure;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Models;

namespace DailyReport.Services
{
    public class PatientContactInformationRelationshipService : BaseContactInformationRelationshipService, IContactInformationRelationshipService
    {
        private readonly IDataContext _context;

        public PatientContactInformationRelationshipService(IDataContext context)
            : base(context)
        {
            _context = context;
        }

        public IEnumerable<ContactInformationResultModel> FindAll(int patientId)
        {
            Condition.Requires(patientId).IsGreaterThan(0);

            return _context.ContactInformations.FindPatientContactInformations(patientId);
        }

        public override ContactInformationResultModel Add(int patientId, int creatorEmployeeId, int contactInformationTypeId, string data)
        {
            Condition.Requires(patientId).IsGreaterThan(0);
            Condition.Requires(contactInformationTypeId).IsGreaterThan(0);
            Condition.Requires(data).IsNotNullOrWhiteSpace();

            using (var transaction = _context.BeginTransaction())
            {
                var contactInformation = Insert(creatorEmployeeId, contactInformationTypeId, data);

                _context.ContactInformations.RegisterPatientRelationship(contactInformation.Id, patientId);

                return contactInformation;
            }
        }
    }
}
