﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CuttingEdge.Conditions;
using DailyReport.Domain.Infrastructure;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Models;

namespace DailyReport.Services
{
    public class ContactbookService : IContactbookService
    {
        private readonly IDataContext _context;

        public IContactInformationRelationshipService ContactInformations { get; private set; }

        public ContactbookService(IDataContext context, IContactInformationRelationshipService contactInformationRelationshipService)
        {
            _context = context;

            ContactInformations = contactInformationRelationshipService;
        }

        public IEnumerable<ContactResultModel> FindAll()
        {
            return _context.Contacts.FindAll();
        }

        public IContactService GetContactService(int contactId)
        {
            return new ContactService(_context, contactId);
        }

        public void Remove(int contactId)
        {
            Condition.Requires(contactId).IsGreaterThan(0);

            using (var transaction = _context.BeginTransaction())
                _context.Contacts.Delete(contactId);
        }

        public ContactResultModel GetById(int id)
        {
            Condition.Requires(id).IsGreaterThan(0);

            return _context.Contacts.GetById(id);
        }

        public ContactGroupResultModel GetGroup(int id)
        {
            Condition.Requires(id).IsGreaterThan(0);

            return _context.ContactGroups.GetById(id);
        }
    }
}
