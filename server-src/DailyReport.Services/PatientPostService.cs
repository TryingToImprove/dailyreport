﻿using DailyReport.Domain.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Models.Core;
using DailyReport.Domain.Infrastructure;
using DailyReport.Domain.Models;
using CuttingEdge.Conditions;

namespace DailyReport.Services
{
    public class PatientPostService : IPatientPostService
    {
        private readonly IDataContext _context;

        public PatientPostService(IDataContext context)
        {
            _context = context;
        }

        public IEnumerable<PatientPostResultModel> FindAll(int organizationId)
        {
            Condition.Requires(organizationId).IsGreaterThan(0);

            return _context.PatientPosts.FindAll(organizationId);
        }

        public IEnumerable<PatientPostResultModel> FindAllByPatient(int patientId)
        {
            Condition.Requires(patientId).IsGreaterThan(0);

            return _context.PatientPosts.FindAllByPatient(patientId);
        }

        public void Insert(PatientPostInsertModel model)
        {
            Condition.Requires(model).IsNotNull("model");
            Condition.Requires(model.Description).IsNotNullOrEmpty("model.Description");
            Condition.Requires(model.EmployeeId).IsGreaterThan(0, "model.EmployeeId");
            Condition.Requires(model.Patients).IsNotNull("model.Patients").IsNotEmpty("model.Patients");
            Condition.Requires(model.OrganizationId).IsGreaterThan(0, "model.OrganizationId");

            using (var transactionScope = _context.BeginTransaction())
            {
                var patientPost = _context.PatientPosts.Insert(new PatientPost
                {
                    OrganizationId = model.OrganizationId,
                    EmployeeId = model.EmployeeId,
                    TimeCreated = DateTime.Now,
                    Description = model.Description
                });

                // Add a reference for each patients to the patientPost
                foreach (var patientId in model.Patients)
                    _context.PatientPostToPatients.Insert(patientId, patientPost.Id);
            }
        }
    }
}
