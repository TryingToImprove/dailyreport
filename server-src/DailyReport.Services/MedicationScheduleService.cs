﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CuttingEdge.Conditions;
using DailyReport.Domain.Infrastructure;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;

namespace DailyReport.Services
{
    public class MedicationScheduleService : IMedicationScheduleService
    {
        public IMedicationScheduleHistoryRelationshipService Histories { get; private set; }

        private readonly IDataContext _context;

        public MedicationScheduleService(IDataContext context, IMedicationScheduleHistoryRelationshipService histories)
        {
            _context = context;

            Histories = histories;
        }

        public IEnumerable<MedicationScheduleResultModel> FindAll()
        {
            return _context.MedicationSchedules.FindAll();
        }

        public IEnumerable<MedicationScheduleResultModel> FindByPatientId(int patientId)
        {
            Condition.Requires(patientId).IsGreaterThan(0);

            return _context.MedicationSchedules.FindByPatientId(patientId);
        }

        public IEnumerable<MedicationScheduleResultModel> FindAllTodayByPatient(int patientId)
        {
            Condition.Requires(patientId).IsGreaterThan(0);

            return _context.MedicationSchedules.FindAllTodayByPatient(patientId);
        }

        public IEnumerable<MedicationScheduleResultModel> FindUpcomings(int organizationId)
        {
            Condition.Requires(organizationId).IsGreaterThan(0);

            return _context.MedicationSchedules.FindUpcomings(organizationId);
        }

        public MedicationScheduleResultModel GetById(int id)
        {
            Condition.Requires(id).IsGreaterThan(0);

            return _context.MedicationSchedules.GetById(id);
        }
        
        public MedicationScheduleResultModel Insert(MedicationScheduleInsertModel medicationScheduleInsertModel)
        {
            Condition.Requires(medicationScheduleInsertModel).IsNotNull();
            Condition.Requires(medicationScheduleInsertModel.PatientId).IsGreaterThan(0);
            Condition.Requires(medicationScheduleInsertModel.StartDate).IsNotEqualTo(DateTime.MinValue);

            using (var transactionScope = _context.BeginTransaction())
            {
                var medicationSchedule = _context.MedicationSchedules.Insert(new MedicationSchedule
                {
                    PatientId = medicationScheduleInsertModel.PatientId,
                    TimeCreated = DateTime.Now,
                    StartDate = medicationScheduleInsertModel.StartDate
                });

                foreach (var medicationScheduleItemModel in medicationScheduleInsertModel.Items)
                    InsertScheduleItem(medicationSchedule.Id, medicationScheduleItemModel);

                return GetById(medicationSchedule.Id);
            }
        }

        public IEnumerable<MedicationScheduleItemResultModel> GetPatientSchema(int patientId)
        {
            Condition.Requires(patientId).IsGreaterThan(0);

            return _context.MedicationSchedules.GetPatientSchema(patientId);
        }

        public MedicationScheduleResultModel CompleteSchedule(MedicationScheduleComplete medicationComplete)
        {
            Condition.Requires(medicationComplete).IsNotNull();
            Condition.Requires(medicationComplete.MedicationScheduleId).IsGreaterThan(0);
            Condition.Requires(medicationComplete.Description).IsNotNullOrEmpty(); //TODO: https://trello.com/c/yZkxwdPS/1-medicinskema
            Condition.Requires(medicationComplete.EmployeeId).IsGreaterThan(0);

            if (medicationComplete.TimeCreated == DateTime.MinValue)
                medicationComplete.TimeCreated = DateTime.Now;

            using (var transactionScope = _context.BeginTransaction())
                return _context.MedicationSchedules.Complete(medicationComplete);
        }

        private void InsertScheduleItem(int medicationScheduleId, MedicationScheduleItemInsertModel medicationScheduleItemModel)
        {
            Condition.Requires(medicationScheduleId).IsGreaterThan(0);
            Condition.Requires(medicationScheduleItemModel.MedicationId).IsGreaterThan(0);
            Condition.Requires(medicationScheduleItemModel.Amouth).IsGreaterThan(0);
            Condition.Requires(medicationScheduleItemModel.ExecuteTime).IsNotEqualTo(TimeSpan.Zero);

            var medicationScheduleItem = _context.MedicationScheduleItems.Insert(new MedicationScheduleItem
            {
                Amouth = medicationScheduleItemModel.Amouth,
                ExecuteTime = medicationScheduleItemModel.ExecuteTime,
                MedicationId = medicationScheduleItemModel.MedicationId,
                MedicationScheduleId = medicationScheduleId
            });

            // If there is a repeat interval, then we also need to create that
            if (medicationScheduleItemModel.RepeatInterval.HasValue)
                _context.MedicationScheduleItems.CreateRepeatInterval(medicationScheduleItem.Id, medicationScheduleItemModel.RepeatInterval.Value);
        }
    }
}
