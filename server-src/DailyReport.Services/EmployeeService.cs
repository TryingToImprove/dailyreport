﻿using CuttingEdge.Conditions;
using DailyReport.Domain.Infrastructure;
using DailyReport.Domain.Infrastructure.Dispensers;
using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Messages;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Templating.Models;

namespace DailyReport.Services
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IDataContext _context;
        private readonly IMailDispenser _mailDispenser;
        private readonly IAuthenticationService _authenticationService;

        public IContactInformationRelationshipService ContactInformations { get; private set; }

        public EmployeeService(IDataContext context, IContactInformationRelationshipService contactInformationRelationshipService, IMailDispenser mailDispenser, IAuthenticationService authenticationService)
        {
            _context = context;
            _mailDispenser = mailDispenser;
            _authenticationService = authenticationService;

            ContactInformations = contactInformationRelationshipService;
        }

        public IEnumerable<Employee> FindAll(int organizationId)
        {
            return _context.Employees.FindAll(organizationId);
        }

        public Employee Insert(EmployeeInsertModel employee)
        {
            Condition.Requires(employee).IsNotNull();
            Condition.Requires(employee.Firstname).IsNotNullOrWhiteSpace();
            Condition.Requires(employee.Lastname).IsNotNullOrWhiteSpace();
            Condition.Requires(employee.Email).IsNotNullOrEmpty();
            Condition.Requires(employee.OrganizationId).IsGreaterThan(0);

            if (employee.TimeCreated == DateTime.MinValue)
                employee.TimeCreated = DateTime.Now;

            using (_context.BeginTransaction())
            {
                // Create a authentication key
                var authenticationKey = _authenticationService.CreateAuthenticationKey();

                // Save the employee
                var createdEmployee = _context.Employees.Insert(employee, authenticationKey);

                // Create the template viewmodel
                var templateViewModel = new EmployeeCreatedModel
                {
                    Employee = createdEmployee,
                    CompleteRegistrationUrl = string.Format("http://localhost:7989/Account/CompleteRegistration?secret={0}", authenticationKey)
                };

                // Send mail to the employe
                _mailDispenser.SendTemplatedAsync("EmployeeCreatedView", templateViewModel, new MailInformation
                {
                    From = "system@dagsrapporten.dk",
                    Recipients = new[] { createdEmployee.Email },
                    Subject = "Færdiggør din bruger"
                });

                return createdEmployee;
            }
        }

        public EmployeeResultModel GetById(int employeeId)
        {
            Condition.Requires(employeeId).IsGreaterThan(0);

            return _context.Employees.GetById(employeeId);
        }

        public EmployeeResultModel GetBySecret(string secret)
        {
            Condition.Requires(secret).IsNotNullOrWhiteSpace();

            return _context.Employees.GetBySecret(secret);
        }

        public void Update(Employee employee)
        {
            Condition.Requires(employee).IsNotNull();
            Condition.Requires(employee.Firstname).IsNotNullOrEmpty();
            Condition.Requires(employee.Lastname).IsNotNullOrEmpty();
            Condition.Requires(employee.Email).IsNotNullOrEmpty();
            Condition.Requires(employee.OrganizationId).IsGreaterThan(0);

            using (var transactionScope = _context.BeginTransaction())
                _context.Employees.Update(employee);
        }

        public void UpdateActiveState(int employeeId, bool isActive)
        {
            Condition.Requires(employeeId).IsGreaterThan(0);

            using (var transactionScope = _context.BeginTransaction())
                _context.Employees.UpdateActiveState(employeeId, isActive);
        }

        public AuthenticationResultModel<Employee> Authenticate(string username, string password)
        {
            Condition.Requires(username).IsNotNullOrWhiteSpace();
            Condition.Requires(password).IsNotNullOrWhiteSpace();

            // Find the employee
            var result = _authenticationService.Employees.Find(username, password);

            if (result == null)
                return null;

            // Add a login log
            using (var transactionScope = _context.BeginTransaction())
                _context.AuthenticationLogs.Insert(result.Identity.Id, DateTime.Now);

            return result;
        }
    }
}
