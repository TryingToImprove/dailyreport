﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;
using CuttingEdge.Conditions;
using DailyReport.Domain.Infrastructure;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;

namespace DailyReport.Services
{
    public class PatientAddressRelationshipService : IPatientAddressService
    {
        private readonly IDataContext _context;

        public PatientAddressRelationshipService(IDataContext context)
        {
            _context = context;
        }

        public PatientAddressResultModel Insert(int patientId, PatientAddressInsertModel model)
        {
            Condition.Requires(model).IsNotNull();
            Condition.Requires(model.Street).IsNotNullOrWhiteSpace();
            Condition.Requires(model.PostalCode).IsNotNullOrWhiteSpace();
            Condition.Requires(model.HouseNumber).IsNotNullOrWhiteSpace();
            Condition.Requires(model.City).IsNotNullOrWhiteSpace();

            using (_context.BeginTransaction())
                return _context.Addresses.AddPatientAddress(patientId, model);
        }

        public IEnumerable<PatientAddressResultModel> FindAll(int patientId)
        {
            Condition.Requires(patientId).IsGreaterThan(0);

            return _context.Addresses.FindAllByPatient(patientId);
        }

        public void Remove(int patientId, int addressId)
        {
            Condition.Requires(patientId).IsGreaterThan(0);
            Condition.Requires(addressId).IsGreaterThan(0);

            using (_context.BeginTransaction())
                _context.Addresses.RemovePatientAddress(patientId, addressId);
        }

        public PatientAddressResultModel Update(int patientId, int addressId, PatientAddressUpdateModel model)
        {
            Condition.Requires(patientId).IsGreaterThan(0);
            Condition.Requires(addressId).IsGreaterThan(0);
            Condition.Requires(model).IsNotNull();

            using (_context.BeginTransaction())
                return _context.Addresses.UpdatePatientAddress(patientId, addressId, model);
        }

        public IEnumerable<PatientAddressResultModel> FindPrimary(int patientId)
        {
            Condition.Requires(patientId).IsGreaterThan(0);

            return _context.Addresses.FindPrimaryPatient(patientId);
        }

        public PatientAddressResultModel FindById(int patientId, int addressId)
        {
            Condition.Requires(patientId).IsGreaterThan(0);
            Condition.Requires(addressId).IsGreaterThan(0);

            return _context.Addresses.FindByIdPatient(patientId, addressId);
        }
    }
}
