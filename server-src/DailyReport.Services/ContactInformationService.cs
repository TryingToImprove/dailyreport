﻿using DailyReport.Domain.Infrastructure;
using DailyReport.Domain.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using CuttingEdge.Conditions;

namespace DailyReport.Services
{
    public class ContactInformationService : IContactInformationService
    {
        private readonly IDataContext _context;

        public ContactInformationService(IDataContext context)
        {
            _context = context;
        }

        public IEnumerable<ContactInformationType> GetTypes()
        {
            return _context.ContactInformationTypes.FindAll();
        }

        public ContactInformationResultModel GetById(int contactInformationId)
        {
            Condition.Requires(contactInformationId).IsGreaterThan(0);

            return _context.ContactInformations.GetById(contactInformationId);
        }

        public void Update(int contactInformationId, int contactInformationTypeId, string data)
        {
            Condition.Requires(contactInformationId).IsGreaterThan(0);
            Condition.Requires(contactInformationTypeId).IsGreaterThan(0);
            Condition.Requires(data).IsNotNullOrWhiteSpace();

            using (var transaction = _context.BeginTransaction())
            {
                _context.ContactInformations.Update(new ContactInformation
                {
                    Id = contactInformationId,
                    ContactInformationTypeId = contactInformationTypeId,
                    Data = data
                });
            }
        }

        public ContactInformationResultModel Deactivate(int contactInformationId)
        {
            Condition.Requires(contactInformationId).IsGreaterThan(0);

            using (var transaction = _context.BeginTransaction())
                return _context.ContactInformations.Deactivate(contactInformationId);
        }
    }
}
