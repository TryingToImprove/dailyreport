﻿using CuttingEdge.Conditions;
using DailyReport.Domain;
using DailyReport.Domain.Infrastructure;
using DailyReport.Domain.Infrastructure.Dispensers;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Messages;
using DailyReport.Domain.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Templating.Models;
using MessageQueuer;

namespace DailyReport.Services
{
    public class OrganizationService : IOrganizationService
    {
        private readonly IDataContext _context;

        public OrganizationService(IDataContext context)
        {
            _context = context;
        }

        public IEnumerable<Organization> FindAll()
        {
            return _context.Organizations.FindAll();
        }

        public Organization Insert(Organization organization)
        {
            Condition.Requires(organization).IsNotNull();
            Condition.Requires(organization.Name).IsNotNullOrWhiteSpace();

            if (organization.TimeCreated.Equals(DateTime.MinValue))
                organization.TimeCreated = DateTime.Now;

            using (var transactionScope = _context.BeginTransaction())
            {
                _context.Organizations.Insert(organization);
            }

            return organization;
        }

        public Organization GetById(int organizationId)
        {
            Condition.Requires(organizationId).IsGreaterThan(0);

            return _context.Organizations.GetById(organizationId);
        }

        public void Update(Organization organization)
        {
            using (var transactionScope = _context.BeginTransaction())
            {
                _context.Organizations.Update(organization);
            }
        }
    }
}
