﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CuttingEdge.Conditions;
using DailyReport.Domain;
using DailyReport.Domain.Infrastructure.Dispensers;
using DailyReport.Domain.Messages;

namespace DailyReport.Services.Distributed
{
    public class MailDispenser : IMailDispenser
    {
        private readonly QueueService _queueService;

        public MailDispenser(QueueService queueService)
        {
            _queueService = queueService;
        }
        public Task SendAsync(MailInformation information, string body)
        {
            Condition.Requires(information).IsNotNull();
            Condition.Requires(information.From).IsNotNullOrEmpty();
            Condition.Requires(information.Recipients).IsNotNull().IsNotEmpty();
            Condition.Requires(information.Subject).IsNotNullOrWhiteSpace();
            Condition.Requires(body).IsNotNull();

            return Task.Run(() => _queueService.Send(MessageQueues.MailSender, new MailSenderMessage()
            {
                Body = body,
                Information = information
            }));
        }

        public Task SendTemplatedAsync<T>(string template, T model, MailInformation information) where T : class
        {
            Condition.Requires(template).IsNotNullOrWhiteSpace();
            Condition.Requires(model).IsNotNull();
            Condition.Requires(information).IsNotNull();
            Condition.Requires(information.From).IsNotNullOrEmpty();
            Condition.Requires(information.Recipients).IsNotNull().IsNotEmpty();
            Condition.Requires(information.Subject).IsNotNullOrWhiteSpace();

            return Task.Run(() => _queueService.Send(MessageQueues.TemplatedMailBuilder, new TemplatedMailBuilderMessage
            {
                Template = template,
                TemplateViewModel = model,
                TemplateViewModelType = typeof(T),
                Information = information
            }));
        }
    }
}
