﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CuttingEdge.Conditions;
using DailyReport.Domain.Infrastructure;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Models;
using DailyReport.Services.Extensions;
using Newtonsoft.Json;

namespace DailyReport.Services
{
    public class TimelineService : ITimelineService
    {
        private readonly IDataContext _context;

        public TimelineService(IDataContext context)
        {
            _context = context;
        }

        public IEnumerable<TimelineEntryResult> FindByPatient(int patientId)
        {
            Condition.Requires(patientId).IsGreaterThan(0);

            return _context.TimelineEntries.FindByPatient(patientId).Transform();
        }
    }
}
