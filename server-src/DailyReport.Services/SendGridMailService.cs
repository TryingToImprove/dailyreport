﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using CuttingEdge.Conditions;
using DailyReport.Domain.Infrastructure.Services;
using SendGrid;

namespace DailyReport.Services
{
    public class SendGridMailService : IMailService
    {
        private readonly Web _transportWeb;

        public SendGridMailService(NetworkCredential credentials)
        {
            _transportWeb = new Web(credentials);
        }

        public void Send(MailAddress fromAddress, IEnumerable<MailAddress> recipientAddresses, string subject, string htmlBody)
        {
            SendAsync(fromAddress, recipientAddresses, subject, htmlBody).Wait();
        }

        public void Send(MailAddress fromAddress, IEnumerable<MailAddress> recipientAddresses, string subject, string htmlBody, string textBody)
        {
            SendAsync(fromAddress, recipientAddresses, subject, htmlBody, textBody).Wait();
        }

        public async Task SendAsync(MailAddress fromAddress, IEnumerable<MailAddress> recipientAddresses, string subject, string htmlBody)
        {
            await SendAsync(fromAddress, recipientAddresses, subject, htmlBody, string.Empty);
        }

        public async Task SendAsync(MailAddress fromAddress, IEnumerable<MailAddress> recipientAddresses, string subject, string htmlBody, string textBody)
        {
            var mailAddresses = recipientAddresses as MailAddress[] ?? recipientAddresses.ToArray();

            Condition.Requires(fromAddress).IsNotNull();
            Condition.Requires(mailAddresses).IsNotNull().IsNotEmpty();
            Condition.Requires(subject).IsNotNullOrWhiteSpace();
            Condition.Requires(htmlBody).IsNotNullOrWhiteSpace();

            var message = CreateMessage(fromAddress, subject, htmlBody, textBody, mailAddresses);

            await _transportWeb.DeliverAsync(message);
        }

        private static SendGridMessage CreateMessage(MailAddress fromAddress, string subject, string htmlBody, string textBody, MailAddress[] mailAddresses)
        {
            var message = new SendGridMessage
            {
                From = fromAddress,
                Subject = subject,
                Html = htmlBody,
                To = mailAddresses
            };

            if (!string.IsNullOrWhiteSpace(textBody))
                message.Text = textBody;

            return message;
        }
    }
}
