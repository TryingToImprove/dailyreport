﻿using DailyReport.Domain.Infrastructure.Services;

namespace DailyReport.Services
{
    public abstract class BaseAuthenticationRelationshipService
    {
        private readonly IHashService _hashService;

        protected BaseAuthenticationRelationshipService(IHashService hashService)
        {
            _hashService = hashService;
        }

        protected string GenerateHash(string password)
        {
            return _hashService.Create(password);
        }
    }
}