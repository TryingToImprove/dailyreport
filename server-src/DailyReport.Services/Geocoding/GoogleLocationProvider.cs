﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DailyReport.Domain.Infrastructure;
using Improved.GoogleMaps.Places;

namespace DailyReport.Services.Geocoding
{
    public class GoogleLocationProvider : ILocationProvider
    {
        private readonly PlacesService _placesService = new PlacesService("AIzaSyDosYK7n8AnE71aagl5dVdusblald027QE", "da");

        public async Task<IEnumerable<Location>> SearchAsync(string input)
        {
            return await _placesService.AutocompleteAsync(input, new AutocompleteRequest { Types = "address" })
                .ContinueWith(x => x.Result.Select(y => Format(y.Result)));
        }

        private static Location Format(DetailsResult.ResultModel result)
        {
            return new Location()
            {
                Id = result.Id,
                Address = result.FormattedAddress,
                City = GetLongNameByType("locality", result.AddressComponents),
                HouseNumber = GetLongNameByType("street_number", result.AddressComponents),
                PostalCode = GetLongNameByType("postal_code", result.AddressComponents),
                Street = GetLongNameByType("route", result.AddressComponents),
                Country = new LocationCountry
                {
                    Name = GetLongNameByType("country", result.AddressComponents),
                    Code = GetShortNameByType("country", result.AddressComponents)
                }
            };
        }

        private static string GetLongNameByType(string type, IEnumerable<DetailsResult.AddressComponent> addressComponents)
        {
            var component = GetComponent(type, addressComponents);
            return component == null ? null : component.LongName;
        }

        private static string GetShortNameByType(string type, IEnumerable<DetailsResult.AddressComponent> addressComponents)
        {
            var component = GetComponent(type, addressComponents);
            return component == null ? null : component.ShortName;
        }

        private static DetailsResult.AddressComponent GetComponent(string type, IEnumerable<DetailsResult.AddressComponent> addressComponents)
        {
            return addressComponents.FirstOrDefault(x => x.Types.Contains(type, StringComparer.InvariantCultureIgnoreCase));
        }
    }
}
