﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Infrastructure.Services;

namespace DailyReport.Services.Security
{
    public class HashService : IHashService
    {
        private const string StorageFormat = "{0}-{1}";
        private const string Salt = "a],-3=J15 l)8)q-c'c7!b_Zke@&|onQ";

        public string Create(string input)
        {
            var md5 = MD5.Create();
            var inputBytes = Encoding.ASCII.GetBytes(string.Format(StorageFormat, Salt, input));
            var hashBytes = md5.ComputeHash(inputBytes);

            var sb = new StringBuilder();

            foreach (var hashByte in hashBytes)
                sb.Append(hashByte.ToString("X2"));

            return sb.ToString();
            
        }
    }
}
