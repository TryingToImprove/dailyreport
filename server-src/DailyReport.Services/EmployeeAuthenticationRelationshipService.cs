﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CuttingEdge.Conditions;
using DailyReport.Domain.Infrastructure;
using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;

namespace DailyReport.Services
{
    public class EmployeeAuthenticationRelationshipService : BaseAuthenticationRelationshipService, IAuthenticationRelationshipService<Employee>
    {
        private readonly IDataContext _context;

        public EmployeeAuthenticationRelationshipService(IDataContext context, IHashService hashService)
            : base(hashService)
        {
            _context = context;
        }

        public void RegisterRelationship(int employeeId, int authenticationIdentityId)
        {
            _context.AuthenticationIdentites.RegisterEmployeeRelationship(authenticationIdentityId, employeeId);
        }

        public AuthenticationResultModel<Employee> Find(string username, string password)
        {
            var passwordHash = GenerateHash(password);

            return _context.AuthenticationIdentites.FindEmployee(username, passwordHash);
        }
    }
}
