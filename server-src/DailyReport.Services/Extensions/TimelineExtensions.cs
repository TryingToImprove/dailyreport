﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Models;
using Newtonsoft.Json;

namespace DailyReport.Services.Extensions
{
    public static class TimelineExtensions
    {
        public static IEnumerable<TimelineEntryResult> Transform(this IEnumerable<TimelineEntry> entries)
        {
            return entries.Select(entry => new TimelineEntryResult
            {
                EntryId = entry.EntryId,
                EntryData = JsonConvert.DeserializeObject(entry.EntryData),
                EntryKey = entry.EntryKey,
                EntryDateTime = entry.EntryDateTime
            });
        }
    }
}
