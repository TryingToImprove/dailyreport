﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CuttingEdge.Conditions;
using DailyReport.Domain.Infrastructure;
using DailyReport.Domain.Models;

namespace DailyReport.Services
{
    public abstract class BaseContactbookRelationshipService
    {
        protected readonly IDataContext Context;

        protected BaseContactbookRelationshipService(IDataContext context)
        {
            Context = context;
        }

        protected ContactResultModel Insert(ContactbookInsertModel model)
        {
            Condition.Requires(model).IsNotNull();
            Condition.Requires(model.Firstname).IsNotNullOrWhiteSpace();
            Condition.Requires(model.Lastname).IsNotNullOrWhiteSpace();
            Condition.Requires(model.Type).IsNotEmpty();
            Condition.Requires(model.Groups).IsNotEmpty();
            Condition.Requires(model.EmployeeId).IsGreaterThan(0);

            if (model.TimeCreated == DateTime.MinValue)
                model.TimeCreated = DateTime.Now;

            return Context.Contacts.Insert(model);
        }
    }
}
