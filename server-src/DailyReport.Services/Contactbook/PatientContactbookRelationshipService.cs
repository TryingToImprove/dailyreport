﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CuttingEdge.Conditions;
using DailyReport.Domain.Infrastructure;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;

namespace DailyReport.Services
{
    public class PatientContactbookRelationshipService : BaseContactbookRelationshipService, IContactbookRelationshipService
    {
        public PatientContactbookRelationshipService(IDataContext context)
            : base(context)
        {
        }

        public IEnumerable<ContactResultModel> FindAll(int patientId)
        {
            Condition.Requires(patientId).IsGreaterThan(0);

            return Context.Contacts.FindByPatient(patientId);
        }

        public IEnumerable<ContactGroupResultModel> GetGroups(int patientId)
        {
            Condition.Requires(patientId).IsGreaterThan(0);

            return Context.ContactGroups.FindByPatient(patientId);
        }

        public IEnumerable<ContactResultModel> FindByGroup(int patientId, int groupId)
        {
            Condition.Requires(patientId).IsGreaterThan(0);
            Condition.Requires(groupId).IsGreaterThan(0);

            return Context.Contacts.FindByPatient(patientId, groupId);
        }

        public ContactResultModel Add(int patientId, ContactbookInsertModel model)
        {
            Condition.Requires(patientId).IsGreaterThan(0);
            Condition.Requires(model).IsNotNull();
            Condition.Requires(model.EmployeeId).IsGreaterThan(0);
            Condition.Requires(model.Firstname).IsNotNullOrWhiteSpace();
            Condition.Requires(model.Lastname).IsNotNullOrWhiteSpace();

            using (var transaction = Context.BeginTransaction())
            {
                var contact = Insert(model);

                Context.Contacts.RegisterPatientReference(patientId, contact.Id);

                return contact;
            }
        }
    }
}
