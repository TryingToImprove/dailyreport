﻿using DailyReport.Domain.Infrastructure;
using DailyReport.Domain.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using CuttingEdge.Conditions;

namespace DailyReport.Services
{
    public class PatientService : IPatientService
    {
        private readonly IDataContext _context;

        public IContactInformationRelationshipService ContactInformations { get; private set; }

        public IContactbookRelationshipService Contacts { get; private set; }

        public IContactPersonRelationshipService ContactPersons { get; private set; }

        public IPatientAddressService Addresses { get; private set; }

        public PatientService(IDataContext context, IContactInformationRelationshipService contactInformationRelationshipService, IContactbookRelationshipService contactbookRelationshipService, IContactPersonRelationshipService contactPersonRelationshipService, IPatientAddressService addressRelationshipService)
        {
            _context = context;

            ContactInformations = contactInformationRelationshipService;
            Contacts = contactbookRelationshipService;
            ContactPersons = contactPersonRelationshipService;
            Addresses = addressRelationshipService;
        }

        public IEnumerable<Patient> FindAll(int organizationId)
        {
            Condition.Requires(organizationId).IsGreaterThan(0);

            return _context.Patients.FindAll(organizationId);
        }

        public IEnumerable<Patient> FindByEmployee(int employeeId)
        {
            Condition.Requires(employeeId).IsGreaterThan(0);

            return _context.Patients.FindByEmployee(employeeId);
        }

        public PatientResultModel Insert(PatientInsertModel model)
        {
            Condition.Requires(model.Firstname).IsNotNullOrEmpty();
            Condition.Requires(model.Lastname).IsNotNullOrEmpty();
            Condition.Requires(model.OrganizationId).IsGreaterThan(0);
            Condition.Requires(model.BirthDate).IsNotEqualTo(DateTime.MinValue);
            Condition.Requires(model.CreatorEmployeeId).IsGreaterThan(0);

            using (_context.BeginTransaction())
            {
                var patient = _context.Patients.Insert(new Patient
                {
                    Firstname = model.Firstname,
                    BirthDate = model.BirthDate,
                    IsActive = true,
                    Lastname = model.Lastname,
                    OrganizationId = model.OrganizationId,
                    TimeCreated = DateTime.Now
                });

                // Add the contactpersons
                foreach (var contactPersonId in model.ContactPersons)
                    ContactPersons.Add(patient.Id, contactPersonId);

                // Add contact informations
                if (model.ContactInformations != null)
                {
                    foreach (var contactInformation in model.ContactInformations)
                        ContactInformations.Add(patient.Id, model.CreatorEmployeeId, contactInformation.DataTypeId, contactInformation.Data);
                }
                // Add description if there is one
                if (!string.IsNullOrWhiteSpace(model.Description))
                {
                    CreateDescription(patient.Id, new Description
                    {
                        EmployeeId = model.CreatorEmployeeId,
                        Text = model.Description
                    });
                }

                return patient;
            }
        }

        public void Deactivate(int patientId)
        {
            Condition.Requires(patientId).IsGreaterThan(0);

            using (var transactionScope = _context.BeginTransaction())
                _context.Patients.Deactivate(patientId);
        }

        public PatientResultModel GetById(int patientId)
        {
            Condition.Requires(patientId).IsGreaterThan(0);

            return _context.Patients.GetById(patientId);
        }

        public PatientResultModel Update(int patientId, PatientUpdateModel model)
        {
            Condition.Requires(patientId).IsGreaterThan(0);
            Condition.Requires(model).IsNotNull();
            Condition.Requires(model.Firstname).IsNotNullOrWhiteSpace();
            Condition.Requires(model.Lastname).IsNotNullOrWhiteSpace();

            using (_context.BeginTransaction())
                return _context.Patients.Update(patientId, model);
        }

        public void CreateDescription(int patientId, Description description)
        {
            Condition.Requires(patientId).IsGreaterThan(0);
            Condition.Requires(description).IsNotNull();
            Condition.Requires(description.Text).IsNotNullOrWhiteSpace();
            Condition.Requires(description.EmployeeId).IsGreaterThan(0);

            if (description.TimeCreated.Equals(DateTime.MinValue))
                description.TimeCreated = DateTime.Now;

            using (_context.BeginTransaction())
                _context.Descriptions.InsertPatientDescription(patientId, description);
        }

        public IEnumerable<Description> GetDescriptions(int patientId)
        {
            Condition.Requires(patientId).IsGreaterThan(0);

            return _context.Descriptions.GetPatientDescriptions(patientId);
        }
    }
}
