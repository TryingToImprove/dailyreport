﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CuttingEdge.Conditions;
using DailyReport.Domain;
using DailyReport.Domain.Infrastructure;
using DailyReport.Domain.Infrastructure.Dispensers;
using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Messages;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using DailyReport.Templating.Models;

namespace DailyReport.Services
{
    public class MedicationScheduleHistoryRelationshipService : IMedicationScheduleHistoryRelationshipService
    {
        private readonly IDataContext _context;

        public MedicationScheduleHistoryRelationshipService(IDataContext context)
        {
            _context = context;
        }

        public IEnumerable<MedicationScheduleItemHistoryResultModel> FindAll(int patientId)
        {
            Condition.Requires(patientId).IsGreaterThan(0);

            return _context.MedicationScheduleItemHistories.FindByPatient(patientId);
        }

        public IEnumerable<MedicationScheduleItemHistoryResultModel> Get(int medicationScheduleItemId)
        {
            Condition.Requires(medicationScheduleItemId).IsGreaterThan(0);

            return _context.MedicationScheduleItemHistories.FindAll(medicationScheduleItemId);
        }

        public MedicationScheduleItemHistoryResultModel Add(MedicationScheduleItemHistory medicationScheduleItemHistory, int employeeId)
        {
            Condition.Requires(medicationScheduleItemHistory).IsNotNull();
            Condition.Requires(medicationScheduleItemHistory.MedicationScheduleItemId).IsGreaterThan(0);

            if (medicationScheduleItemHistory.TimeCreated == DateTime.MinValue)
                medicationScheduleItemHistory.TimeCreated = DateTime.Now;

            using (var transactionScope = _context.BeginTransaction())
                return _context.MedicationScheduleItemHistories.Insert(medicationScheduleItemHistory, employeeId);
        }
    }
}
