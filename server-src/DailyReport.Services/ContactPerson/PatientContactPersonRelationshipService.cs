﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CuttingEdge.Conditions;
using DailyReport.Domain;
using DailyReport.Domain.Infrastructure;
using DailyReport.Domain.Infrastructure.Dispensers;
using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Messages;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using DailyReport.Templating.Models;

namespace DailyReport.Services
{
    public class PatientContactPersonRelationshipService : IContactPersonRelationshipService
    {
        private readonly IDataContext _context;
        private readonly QueueService _queueService;

        public PatientContactPersonRelationshipService(IDataContext context, QueueService queueService)
        {
            _context = context;
            _queueService = queueService;
        }

        public IEnumerable<PatientContactPersonRelationshipResultModel> FindAll(int patientId)
        {
            Condition.Requires(patientId).IsGreaterThan(0);

            return _context.PatientContactPersonRelationships.FindAll(patientId);
        }

        public IEnumerable<PatientContactPersonRelationshipResultModel> FindCurrents(int patientId)
        {
            Condition.Requires(patientId).IsGreaterThan(0);

            return _context.PatientContactPersonRelationships.FindCurrents(patientId);
        }

        public PatientContactPersonRelationshipResultModel Add(int patientId, int employeeId)
        {
            return Add(patientId, employeeId, false);
        }

        public PatientContactPersonRelationshipResultModel Add(int patientId, int employeeId, bool completePreviousContactPersons)
        {
            Condition.Requires(patientId).IsGreaterThan(0);
            Condition.Requires(employeeId).IsGreaterThan(0);

            using (_context.BeginTransaction())
            {
                var model = _context.PatientContactPersonRelationships.Insert(new PatientContactPersonRelationship
                {
                    PatientId = patientId,
                    EmployeeId = employeeId,
                    TimeCreated = DateTime.Now
                }, completePreviousContactPersons);

                // Queue email
                _queueService.Send(MessageQueues.MailBuilders.AddedAsContactPerson, new AddedAsContactPersonMessage
                {
                    EmployeeId = employeeId,
                    PatientId = patientId
                });
                
                return model;
            }
        }

        public PatientContactPersonRelationshipResultModel Complete(int relationshipId)
        {
            Condition.Requires(relationshipId).IsGreaterThan(0);

            using (var transaction = _context.BeginTransaction())
                return _context.PatientContactPersonRelationships.Complete(relationshipId, DateTime.Now);
        }
    }
}
