﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CuttingEdge.Conditions;
using DailyReport.Domain.Exceptions;
using DailyReport.Domain.Infrastructure;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;

namespace DailyReport.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly IDataContext _context;
        private readonly IHashService _hashService;

        public IAuthenticationRelationshipService<Employee> Employees { get; private set; }

        public AuthenticationService(IDataContext context, IHashService hashService, IAuthenticationRelationshipService<Employee> employeesRelationshipService)
        {
            _context = context;
            _hashService = hashService;

            Employees = employeesRelationshipService;
        }

        public string CreateAuthenticationKey()
        {
            var authenticationKey = _hashService.Create(Guid.NewGuid().ToString());

            _context.AuthenticationIdentites.InsertAuthenticationIdentityKey(authenticationKey, DateTime.Now);

            return authenticationKey;
        }

        public AuthenticationIdentityResultModel Insert(string authenticationKey, string username, string password)
        {
            Condition.Requires(authenticationKey).IsNotNullOrWhiteSpace();
            Condition.Requires(username).IsNotNullOrWhiteSpace();
            Condition.Requires(password).IsNotNullOrWhiteSpace();

            if (!IsValidAuthenticationKey(authenticationKey))
                throw new InvalidAuthenticationKeyException(authenticationKey);

            using (_context.BeginTransaction())
            {
                return _context.AuthenticationIdentites.Insert(new AuthenticationIdentityInsertModel
                {
                    AuthenticationKey = authenticationKey,
                    Username = username,
                    Password = _hashService.Create(password),
                    TimeCreated = DateTime.Now
                });
            }
        }

        public bool IsValidAuthenticationKey(string authenticationKey)
        {
            Condition.Requires(authenticationKey).IsNotNullOrWhiteSpace();

            return _context.AuthenticationIdentites.IsValidAuthenticationKey(authenticationKey);
        }
    }
}
