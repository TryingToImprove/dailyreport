﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CuttingEdge.Conditions;
using DailyReport.Domain.Infrastructure;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Models;

namespace DailyReport.Services
{
    public class ContactService : IContactService
    {
        private readonly int _contactId;
        private readonly IDataContext _context;

        private readonly HashSet<string> _addedGroups = new HashSet<string>();
        private readonly HashSet<string> _removedGroups = new HashSet<string>();
        private readonly Dictionary<string, object> _changedProperties = new Dictionary<string, object>();
        private string _contactType;
        private bool _removeType = false;

        public ContactService(IDataContext context, int contactId)
        {
            _context = context;
            _contactId = contactId;
        }

        public void AddGroup(string groupName)
        {
            Condition.Requires(groupName).IsNotNullOrWhiteSpace();

            if (_addedGroups.Contains(groupName))
                throw new InvalidOperationException(string.Format("'{0}' is already added", groupName));

            if (_removedGroups.Contains(groupName))
                throw new InvalidOperationException(string.Format("'{0}' is already marked as removed", groupName));

            _addedGroups.Add(groupName);
        }

        public void RemoveGroup(string groupName)
        {
            Condition.Requires(groupName).IsNotNullOrWhiteSpace();

            if (_addedGroups.Contains(groupName))
                throw new InvalidOperationException(string.Format("'{0}' is already added", groupName));

            if (_removedGroups.Contains(groupName))
                throw new InvalidOperationException(string.Format("'{0}' is already marked as removed", groupName));

            _removedGroups.Add(groupName);
        }

        public void Update(string firstname, string lastname)
        {
            if (!string.IsNullOrWhiteSpace(firstname))
                _changedProperties.Add("Firstname", firstname);

            if (!string.IsNullOrWhiteSpace(lastname))
                _changedProperties.Add("Lastname", lastname);
        }

        public void ChangeType(string contactType)
        {
            if (_removeType)
                throw new InvalidOperationException(string.Format("ContactType was already sat to be removed")); 
            
            if (_contactType != null)
                throw new InvalidOperationException(string.Format("ContactType was already sat to been sat '{0}'", _contactType));

            if (string.IsNullOrWhiteSpace(contactType))
                _removeType = true;
            else
                _contactType = contactType;
        }

        public void Dispose()
        {
            using (var transaction = _context.BeginTransaction())
            {
                _context.Contacts.Update(_contactId, new ContactUpdateModel
                {
                    RemovedGroups = _removedGroups,
                    AddedGroups = _addedGroups,
                    Type = _contactType,
                    Properties = _changedProperties,
                    RemoveType = _removeType
                });
            }
        }
    }
}
