﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CuttingEdge.Conditions;
using DailyReport.Domain.Infrastructure;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;

namespace DailyReport.Services
{
    public class MedicationService : IMedicationService
    {
        private readonly IDataContext _context;

        public MedicationService(IDataContext context)
        {
            _context = context;
        }

        public IEnumerable<MedicationResultModel> FindAll()
        {
            return _context.Medications.FindAll();
        }

        public IEnumerable<MedicationBrand> FindBrands()
        {
            return _context.MedicationBrands.FindAll();
        }

        public IEnumerable<Measure> GetMeasures()
        {
            return _context.Measures.FindAll();
        }

        public IEnumerable<MedicationType> GetMedicationTypes()
        {
            return _context.MedicationTypes.FindAll();
        }

        public IEnumerable<MedicationResultModel> Search(string query)
        {
            Condition.Requires(query).IsNotNull();

            return _context.Medications.Search(query);
        }

        public MedicationResultModel Insert(MedicationInsertModel model)
        {
            Condition.Requires(model).IsNotNull();
            Condition.Requires(model.Medication).IsNotNull();
            Condition.Requires(model.Medication.Name).IsNotNullOrWhiteSpace();
            Condition.Requires(model.Medication.Weight).IsGreaterThan(0);
            Condition.Requires(model.Medication.MedicationTypeId).IsGreaterThan(0);
            Condition.Requires(model.Medication.MeasureId).IsGreaterThan(0);

            if (model.Medication.MedicationBrandId == 0)
            {
                if (model.Brand == null)
                    throw new InvalidOperationException("MedicationBrandId or Brand is required");

                Condition.Requires(model.Brand.Name).IsNotNullOrWhiteSpace();
            }

            using (var transactionScope = _context.BeginTransaction())
            {
                var medicationBrand = (model.Medication.MedicationBrandId == 0)
                    ? _context.MedicationBrands.Insert(model.Brand)
                    : _context.MedicationBrands.GetById(model.Medication.MedicationBrandId);

                // Map the Medication Brand Id
                model.Medication.MedicationBrandId = medicationBrand.Id;

                // Insert the medication
                return _context.Medications.Insert(model.Medication);
            }
        }
    }
}
