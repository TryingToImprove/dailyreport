﻿using System;
using System.Data;
using DailyReport.DataAccess;
using DailyReport.Domain.Infrastructure;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Models.Core;
using DailyReport.Tests.Helpers;
using FizzWare.NBuilder;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DailyReport.Services.Tests.Integration.Units
{
    [TestClass]
    public class EmployeeServiceTests : BaseServiceTests
    {
        [TestMethod]
        public void Insert_GivenAValidEmployee_ShouldSaveToDatabase()
        {
            var employee = Builder<Employee>.CreateNew().Build();

            var service = GetService();

            service.Insert(employee);

            Assert.AreNotEqual(-1, employee.Id);
            Assert.AreNotEqual(0, employee.Id);
        }

        private IEmployeeService GetService()
        {
            return new EmployeeService(DataContext);
        }
    }
}
