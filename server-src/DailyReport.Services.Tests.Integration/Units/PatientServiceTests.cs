﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using DailyReport.DataAccess;
using DailyReport.Domain.Infrastructure;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Models.Core;
using DailyReport.Tests.Helpers;
using FizzWare.NBuilder;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DailyReport.Services.Tests.Integration.Units
{
    [TestClass]
    public class PatientServiceTests : BaseServiceTests
    {
        [TestMethod]
        public void GetContactPersons_ShouldReturnAListOfContactPersons()
        {
            var contactPersons = DataContext.PatientContactPersonRelationships.FindAll(1);

            var service = GetService();

            var result = service.GetContactPersons(1);

            Assert.IsNotNull(result);
            Assert.AreEqual(contactPersons.Count(), result.Count());
        }

        [TestMethod]
        public void AddContactPersons_GivenValidParameters_ShouldAddARelationship()
        {
            var parameters = new
            {
                PatientId = 2,
                EmployeeId = 3
            };

            var contactPersons = DataContext.PatientContactPersonRelationships.FindAll(parameters.PatientId);
            var numberOfContactPersons = contactPersons.Count();
            var contactPerson = contactPersons.FirstOrDefault(x => x.PatientId == parameters.PatientId && x.Employee.Id == parameters.EmployeeId);
            Assert.IsNull(contactPerson, "Tested contactPersonRelationship does already exists");

            var service = GetService();

            service.AddContactPerson(parameters.PatientId, parameters.EmployeeId);

            contactPersons = DataContext.PatientContactPersonRelationships.FindAll(parameters.PatientId);
            contactPerson = contactPersons.FirstOrDefault(x => x.PatientId == parameters.PatientId && x.Employee.Id == parameters.EmployeeId);

            Assert.AreEqual(numberOfContactPersons + 1, contactPersons.Count(), "Relation was not added to the existing relationships");
            Assert.IsNotNull(contactPerson, "Could not find the correct relationship");
        }

        [TestMethod]
        [ExpectedException(typeof(SqlException))]
        public void AddContactPersons_GivenInvalidPatientId_ThrowsException()
        {
            var service = GetService();

            service.AddContactPerson(99999, 3);
        }

        [TestMethod]
        [ExpectedException(typeof(SqlException))]
        public void AddContactPersons_GivenInvalidEmployeeId_ThrowsException()
        {
            var service = GetService();

            service.AddContactPerson(1, 99999);
        }

        private IPatientService GetService()
        {
            return new PatientService(DataContext, null);
        }
    }
}
