﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using FizzWare.NBuilder;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DailyReport.Services.Tests.Integration.Units
{
    [TestClass]
    public class MedicationServiceTests : BaseServiceTests
    {
        [TestMethod]
        public void Insert_GivenAValidMedicationInsertModel_ShouldInsertMedication()
        {
            var model = Builder<MedicationInsertModel>
                .CreateNew()
                .With(x =>
                {
                    x.Brand = Builder<MedicationBrand>.CreateNew().Build();
                    x.Medication = Builder<Medication>.CreateNew().Build();
                    return x;
                })
                .Build();

            var service = GetService();

            var result = service.Insert(model);

            Assert.IsNotNull(result);
            Assert.AreNotEqual(-1, result.Id);
        }

        [TestMethod]
        public void GetMeasures_ShouldReturnAListOfMeasures()
        {
            var measures = DataContext.Measures.FindAll();

            var service = GetService();
            var result = service.GetMeasures();

            Assert.IsNotNull(result);
            Assert.AreEqual(measures.Count(), result.Count());
        }

        [TestMethod]
        public void GetMedicationTypes_ShouldReturnAListOfMedicationTypes()
        {
            var medicationTypes = DataContext.MedicationTypes.FindAll();

            var service = GetService();
            var result = service.GetMedicationTypes();

            Assert.IsNotNull(result);
            Assert.AreEqual(medicationTypes.Count(), result.Count());
        }

        private IMedicationService GetService()
        {
            return new MedicationService(DataContext);
        }
    }
}
