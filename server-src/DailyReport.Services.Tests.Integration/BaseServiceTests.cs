﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.DataAccess;
using DailyReport.Domain.Infrastructure;
using DailyReport.Tests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DailyReport.Services.Tests.Integration
{
    public abstract class BaseServiceTests
    {
        protected IDbConnection Connection;
        protected IDataContext DataContext;

        protected virtual void Setup()
        {
        }

        [TestInitialize]
        public void Initialize()
        {
            Connection = TestDatabase.Create();

            DataContext = DataContextHelper.Create(Connection);

            Setup();
        }


        [TestCleanup]
        public void Teardown()
        {
            DataContext = null;

            Connection.Close();
            Connection.Dispose();
            Connection = null;
        }
    }
}
