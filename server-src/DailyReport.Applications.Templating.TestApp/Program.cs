﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Templating;
using DailyReport.Templating.Models;

namespace DailyReport.Applications.Templating.TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var templateService = new RazorTemplateService();

            var test = templateService.Render("AccountCreatedView", new EmployeeCreatedModel()
            {
                Username = "olla",
                TimeCreated = DateTime.Now,
                CompleteRegistrationUrl = "http://dagsrapporten.dk/complete-registration?key={registrationKey}"
            });

            Console.WriteLine(test);


            var test2 = templateService.Render("ForgotPasswordView", new ForgotPasswordModel()
            {
                ResetPasswordUrl = "http://dagsrapporten.dk/reset-password?key={registrationKey}"
            });

            Console.WriteLine(test2);

            Console.ReadKey();
        }
    }
}
