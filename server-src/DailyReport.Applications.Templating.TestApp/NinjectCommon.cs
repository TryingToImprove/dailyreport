﻿using System.Data;
using System.Data.SqlClient;
using DailyReport.DependencyInjection;
using Ninject;

namespace DailyReport.Applications.Templating.TestApp
{
    static class NinjectCommon
    {
        public static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();

            kernel.Bind<IDbConnection>().ToMethod(x => new SqlConnection("Server=OLDI\\SQLEXPRESS;Database=DailyReport;Trusted_Connection=True;"));

            IoC.RegisterServices(kernel);

            return kernel;
        }
    }
}
