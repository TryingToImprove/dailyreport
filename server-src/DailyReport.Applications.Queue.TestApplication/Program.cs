﻿using System;
using DailyReport.Domain;
using DailyReport.Queueing;
using MessageQueuer;
using Ninject;

namespace DailyReport.Applications.Queue.TestApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            var kernel = NinjectCommon.CreateKernel();
            var runner = new MqRunner(kernel.Get<MqConfiguration>());

            Console.WriteLine("Starting");
            runner.Start(exception =>
            {
                Console.WriteLine("There was a exception!");
                Environment.Exit(0);
            });

            Console.WriteLine("Running");

            // If running in console application, then keep the app running
            Console.ReadKey();
        }
    }
}
