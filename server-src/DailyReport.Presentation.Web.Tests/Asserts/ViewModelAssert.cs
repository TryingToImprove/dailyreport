﻿using DailyReport.Domain.Models.Core;
using DailyReport.Presentation.Web.Areas.Administration.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Presentation.Web.Tests.Asserts
{
    public static class ViewModelAssert
    {
        public static void AssertOrganizationBaseViewModel(Organization organization, OrganizationBaseViewModel model)
        {
            Assert.IsNotNull(model.Organization);
            Assert.AreEqual(organization.Id, model.Organization.Id);
            Assert.AreEqual(organization.Name, model.Organization.Name);
        }
    }
}
