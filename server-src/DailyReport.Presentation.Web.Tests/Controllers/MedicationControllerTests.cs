﻿using System;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using DailyReport.Presentation.Web.App_Start;
using DailyReport.Presentation.Web.Controllers;
using DailyReport.Presentation.Web.Models;
using FizzWare.NBuilder;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace DailyReport.Presentation.Web.Tests.Controllers
{
    [TestClass]
    public class MedicationControllerTests
    {
        private Mock<IMedicationService> _medicationServiceMock;

        [TestInitialize]
        public void Initialize()
        {
            AutomapperWebConfiguration.Configure();
            _medicationServiceMock = new Mock<IMedicationService>();
        }

        [TestMethod]
        [TestCategory("MedicationController.Create")]
        public void MedicationController_Create_ShouldReturnCorrectView()
        {
            var controller = GetController();

            var result = controller.Create() as ViewResult;

            Assert.IsTrue(string.IsNullOrEmpty(result.ViewName));
        }

        [TestMethod]
        [TestCategory("MedicationController.Create")]
        public void MedicationController_Create_ShouldReturnCorrectViewModel()
        {
            var controller = GetController();

            var result = controller.Create() as ViewResult;

            Assert.IsNotNull(result.Model);
            Assert.IsInstanceOfType(result.Model, typeof(MedicationCreateViewModel));
        }

        [TestMethod]
        [TestCategory("MedicationController.Create")]
        public void MedicationController_Create_ShouldReturnEmptyValuesForViewModel()
        {
            var controller = GetController();

            var result = controller.Create() as ViewResult;
            var model = result.Model as MedicationCreateViewModel;

            Assert.IsTrue(string.IsNullOrEmpty(model.Name));
            Assert.IsTrue(string.IsNullOrEmpty(model.BrandName));
            Assert.IsFalse(model.BrandId.HasValue);
        }

        [TestMethod]
        [TestCategory("MedicationController.Create")]
        public void MedicationController_Create_ShouldReturnAListOfBrand_AsSelectList()
        {
            var medicationBrands = Builder<MedicationBrand>.CreateListOfSize(10).Build();
            _medicationServiceMock.Setup(x => x.FindBrands()).Returns(medicationBrands);

            var controller = GetController();

            var result = controller.Create() as ViewResult;
            var model = result.Model as MedicationCreateViewModel;

            Assert.IsNotNull(model.BrandList);
            Assert.AreEqual(medicationBrands.Count(), model.BrandList.Count());

            var actualBrands = model.BrandList.ToList();
            for (var i = 0; i < actualBrands.Count(); i++)
            {
                var expected = medicationBrands[i];
                var actual = actualBrands[i];

                Assert.AreEqual(expected.Id.ToString(CultureInfo.InvariantCulture), actual.Value);
                Assert.AreEqual(expected.Name, actual.Text);
            }
        }

        [TestMethod]
        [TestCategory("MedicationController.Create")]
        public void MedicationController_Create_ShouldReturnAListOfMedicationTypes_AsSelectList()
        {
            AssertMedicationTypesAsSelectList(GetController(), x => x.Create());
        }

        [TestMethod]
        [TestCategory("MedicationController.Create")]
        public void MedicationController_Create_ShouldReturnAListOfMeasures_AsSelectList()
        {
            AssertMeasuresAsSelectList(GetController(), x => x.Create());
        }

        [TestMethod]
        [TestCategory("MedicationController.Create")]
        public void MedicationController_Create_POST_Invalid_ShouldReturnCorrectView()
        {
            var controller = GetController(true);

            var viewModel = GetCreateViewModel();
            var result = controller.Create(viewModel) as ViewResult;

            Assert.IsTrue(string.IsNullOrEmpty(result.ViewName));
        }

        [TestMethod]
        [TestCategory("MedicationController.Create")]
        public void MedicationController_Create_POST_Invalid_ShouldReturnCorrectViewModel()
        {
            var controller = GetController(true);

            var viewModel = GetCreateViewModel();
            var result = controller.Create(viewModel) as ViewResult;

            Assert.IsNotNull(result.Model);
            Assert.IsInstanceOfType(result.Model, typeof(MedicationCreateViewModel));
        }

        [TestMethod]
        [TestCategory("MedicationController.Create")]
        public void MedicationController_Create_POST_Invalid_ShouldReturnThePostedValuesForViewModel()
        {
            var controller = GetController(true);

            var viewModel = GetCreateViewModel();
            var result = controller.Create(viewModel) as ViewResult;
            var model = result.Model as MedicationCreateViewModel;

            Assert.AreEqual(viewModel.Name, model.Name);
            Assert.AreEqual(viewModel.BrandName, model.BrandName);
            Assert.AreEqual(viewModel.BrandId, model.BrandId);
        }

        [TestMethod]
        [TestCategory("MedicationController.Create")]
        public void MedicationController_Create_POST_IfBrandIdIsNull_And_BrandNameIsNull_ModelStateShouldBeInvalid()
        {
            var controller = GetController();

            var viewModel = GetCreateViewModel();
            viewModel.BrandId = null;
            viewModel.BrandName = null;

            controller.Create(viewModel);

            Assert.IsFalse(controller.ModelState.IsValid);
        }

        [TestMethod]
        [TestCategory("MedicationController.Create")]
        public void MedicationController_Create_POST_ShouldCallInsertOnMedicationService()
        {
            var controller = GetController();

            var viewModel = GetCreateViewModel();

            controller.Create(viewModel);

            _medicationServiceMock.Verify(x => x.Insert(It.IsAny<MedicationInsertModel>()), Times.Once);
        }

        [TestMethod]
        [TestCategory("MedicationController.Create")]
        public void MedicationController_Create_POST_ShouldReturnCreatedView()
        {
            var controller = GetController();

            var viewModel = GetCreateViewModel();
            var result = controller.Create(viewModel) as ViewResult;

            Assert.AreEqual("Created", result.ViewName);
        }

        [TestMethod]
        [TestCategory("MedicationController.Create")]
        public void MedicationController_Create_POST_Invalid_ShouldReturnAListOfBrand_AsSelectList()
        {
            var medicationBrands = Builder<MedicationBrand>.CreateListOfSize(10).Build();
            _medicationServiceMock.Setup(x => x.FindBrands()).Returns(medicationBrands);

            var controller = GetController(true);

            var viewModel = GetCreateViewModel();
            var result = controller.Create(viewModel) as ViewResult;
            var model = result.Model as MedicationCreateViewModel;

            Assert.IsNotNull(model.BrandList);
            Assert.AreEqual(medicationBrands.Count(), model.BrandList.Count());

            var actualBrands = model.BrandList.ToList();
            for (var i = 0; i < actualBrands.Count(); i++)
            {
                var expected = medicationBrands[i];
                var actual = actualBrands[i];

                Assert.AreEqual(expected.Id.ToString(CultureInfo.InvariantCulture), actual.Value);
                Assert.AreEqual(expected.Name, actual.Text);
            }
        }

        [TestMethod]
        [TestCategory("MedicationController.Create")]
        public void MedicationController_Create_POST_Invalid_ShouldReturnAListOfMedicationTypes_AsSelectList()
        {
            AssertMedicationTypesAsSelectList(GetController(invalidModelState: true), x => x.Create(GetCreateViewModel()));
        }

        [TestMethod]
        [TestCategory("MedicationController.Create")]
        public void MedicationController_Create_POST_Invalid_ShouldReturnAListOfMeasures_AsSelectList()
        {
            AssertMeasuresAsSelectList(GetController(invalidModelState: true), x => x.Create(GetCreateViewModel()));
        }

        private MedicationCreateViewModel GetCreateViewModel()
        {
            return Builder<MedicationCreateViewModel>.CreateNew().Build();
        }

        private MedicationController GetController(bool invalidModelState = false)
        {
            var controller = new MedicationController(_medicationServiceMock.Object);

            if (invalidModelState)
                controller.ModelState.AddModelError("test", "test");

            return controller;
        }

        private void AssertMedicationTypesAsSelectList(MedicationController controller, Func<MedicationController, ActionResult> actionFunc)
        {
            var medicationTypes = Builder<MedicationType>.CreateListOfSize(10).Build();
            _medicationServiceMock.Setup(x => x.GetMedicationTypes()).Returns(medicationTypes);

            var model = GetCreateViewModel(controller, actionFunc);

            Assert.IsNotNull(model.MedicationTypeList);
            Assert.AreEqual(medicationTypes.Count(), model.MedicationTypeList.Count());
        }

        private void AssertMeasuresAsSelectList(MedicationController controller, Func<MedicationController, ActionResult> actionFunc)
        {
            var measures = Builder<Measure>.CreateListOfSize(10).Build();
            _medicationServiceMock.Setup(x => x.GetMeasures()).Returns(measures);

            var model = GetCreateViewModel(controller, actionFunc);

            Assert.IsNotNull(model.MeasureList);
            Assert.AreEqual(measures.Count(), model.MeasureList.Count());
        }

        private static MedicationCreateViewModel GetCreateViewModel(MedicationController controller, Func<MedicationController, ActionResult> actionFunc)
        {
            var result = actionFunc.Invoke(controller) as ViewResult;
            var model = result.Model as MedicationCreateViewModel;

            return model;
        }
    }
}
