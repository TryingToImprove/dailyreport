﻿using System;
using DailyReport.Domain.Models;
using DailyReport.Presentation.Web.Areas.PatientModule.Models;
using DailyReport.Presentation.Web.Tests.Helpers;
using DailyReport.Tests.Helpers.Fakes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Presentation.Web.Areas.Administration.Controllers;
using FizzWare.NBuilder;
using DailyReport.Domain.Models.Core;
using DailyReport.Presentation.Web.Areas.Administration.Models;
using System.Web.Mvc;
using System.Linq;
using DailyReport.Presentation.Web.Tests.Asserts;
using NSubstitute;

namespace DailyReport.Presentation.Web.Tests.Controllers.Administration
{
    [TestClass]
    public class PatientControllerTests
    {
        private Organization _validOrganization;
        private Mock<IPatientService> _patientServiceMock;
        private Mock<IPatientPostService> _patientPostServiceMock;
        private Mock<IOrganizationService> _organizationServiceMock;
        private Mock<IMedicationScheduleService> _medicationScheduleServiceMock;
        private Mock<IEmployeeService> _employeeServiceMock;
        private Mock<IContactInformationService> _contactInformationServiceMock;
        private PatientController _controller;

        [TestInitialize]
        public void Initialize()
        {
            _validOrganization = GetValidOrganization();

            _patientPostServiceMock = new Mock<IPatientPostService>();
            _patientServiceMock = new Mock<IPatientService>();
            _organizationServiceMock = new Mock<IOrganizationService>();
            _organizationServiceMock.Setup(x => x.GetById(_validOrganization.Id)).Returns(_validOrganization);
            _medicationScheduleServiceMock = new Mock<IMedicationScheduleService>();
            _employeeServiceMock = new Mock<IEmployeeService>();
            _contactInformationServiceMock = new Mock<IContactInformationService>();

            _controller = new PatientController(_patientServiceMock.Object, _organizationServiceMock.Object, _patientPostServiceMock.Object, _medicationScheduleServiceMock.Object, _employeeServiceMock.Object, _contactInformationServiceMock.Object);
        }

        #region Index
        [TestMethod]
        public void PatientController_Index_ShouldReturnTheCorrectView()
        {
            var patients = Builder<Patient>.CreateListOfSize(12)
                .All().With(x => x.OrganizationId = _validOrganization.Id)
                .Build();

            _patientServiceMock.Setup(x => x.FindAll(_validOrganization.Id)).Returns(patients);

            var result = _controller.Index(_validOrganization.Id) as ViewResult;

            Assert.IsTrue(string.IsNullOrEmpty(result.ViewName));
        }

        [TestMethod]
        public void PatientController_Index_ShouldReturnTheCorrectViewModel()
        {
            var patients = Builder<Patient>.CreateListOfSize(12)
                .All().With(x => x.OrganizationId = _validOrganization.Id)
                .Build();

            _patientServiceMock.Setup(x => x.FindAll(_validOrganization.Id)).Returns(patients);

            var result = _controller.Index(_validOrganization.Id) as ViewResult;

            Assert.IsInstanceOfType(result.Model, typeof(PatientIndexViewModel));
        }

        [TestMethod]
        public void PatientController_Index_ShouldMapToOrganizationBaseViewModel()
        {
            var patients = Builder<Patient>.CreateListOfSize(12)
                .All().With(x => x.OrganizationId = _validOrganization.Id)
                .Build();

            _patientServiceMock.Setup(x => x.FindAll(_validOrganization.Id)).Returns(patients);

            var result = _controller.Index(_validOrganization.Id) as ViewResult;
            var model = result.Model as PatientIndexViewModel;

            ViewModelAssert.AssertOrganizationBaseViewModel(_validOrganization, model);
        }

        [TestMethod]
        public void PatientController_Index_ShouldReturnAListOfPatients()
        {
            var patients = Builder<Patient>.CreateListOfSize(12)
                .All().With(x => x.OrganizationId = _validOrganization.Id)
                .Build();

            _patientServiceMock.Setup(x => x.FindAll(_validOrganization.Id)).Returns(patients);

            var result = _controller.Index(_validOrganization.Id) as ViewResult;
            var model = result.Model as PatientIndexViewModel;

            Assert.AreEqual(patients.Count, model.Patients.Count());
        }
        #endregion

        #region Create
        [TestMethod]
        public void PatientController_Create_ShouldReturnTheCorrectView()
        {
            var result = _controller.Create(_validOrganization.Id) as ViewResult;

            Assert.IsTrue(string.IsNullOrEmpty(result.ViewName));
        }

        [TestMethod]
        public void PatientController_Create_ShouldReturnTheCorrectViewModel()
        {
            var result = _controller.Create(_validOrganization.Id) as ViewResult;

            Assert.IsInstanceOfType(result.Model, typeof(PatientCreateViewModel));
        }

        [TestMethod]
        public void PatientController_Create_ShouldMapToOrganizationBaseViewModel()
        {
            var result = _controller.Create(_validOrganization.Id) as ViewResult;
            var model = result.Model as PatientCreateViewModel;

            ViewModelAssert.AssertOrganizationBaseViewModel(_validOrganization, model);
        }

        [TestMethod]
        public void PatientController_Create_ShouldMapToEmptyViewModel()
        {
            var result = _controller.Create(_validOrganization.Id) as ViewResult;
            var model = result.Model as PatientCreateViewModel;

            Assert.IsNull(model.Firstname);
            Assert.IsNull(model.Lastname);
            Assert.IsNull(model.BirthDate);
        }

        [TestMethod]
        [TestCategory("PatientController.Create.Post")]
        public void PatientController_Create_POST_ShouldInsertUsingPatientService()
        {
            var result = _controller.Create(_validOrganization.Id, GetValidPatientCreateViewModel());

            _patientServiceMock.Verify(x => x.Insert(It.IsAny<Patient>()), Times.Once);
        }

        [TestMethod]
        [TestCategory("PatientController.Create.Post")]
        public void PatientController_Create_POST_ShouldReturnRedirectResult()
        {
            _patientServiceMock.Setup(x => x.Insert(It.IsAny<Patient>())).Returns(GetValidPatient());

            var result = _controller.Create(_validOrganization.Id, GetValidPatientCreateViewModel());

            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult));
        }

        [TestMethod]
        [TestCategory("PatientController.Create.Post")]
        public void PatientController_Create_POST_ShouldRedirectToIndex()
        {
            _patientServiceMock.Setup(x => x.Insert(It.IsAny<Patient>())).Returns(GetValidPatient());

            var result = _controller.Create(_validOrganization.Id, GetValidPatientCreateViewModel()) as RedirectToRouteResult;

            Assert.IsTrue(result.RouteValues.ContainsKey("organizationId"));
            Assert.IsTrue(result.RouteValues.ContainsKey("controller"));
            Assert.IsTrue(result.RouteValues.ContainsKey("action"));
            Assert.IsTrue(result.RouteValues.ContainsKey("area"));

            Assert.AreEqual(_validOrganization.Id, result.RouteValues["organizationId"]);
            Assert.AreEqual("Patient", result.RouteValues["controller"]);
            Assert.AreEqual("Index", result.RouteValues["action"]);
            Assert.AreEqual("Administration", result.RouteValues["area"]);
        }

        [TestMethod]
        [TestCategory("PatientController.Create.Post")]
        public void PatientController_Create_POST_Invalid_ShouldReturnTheCorrectView()
        {
            _controller.ModelState.AddModelError("test", "test"); // Force invalid state

            var result = _controller.Create(_validOrganization.Id, new PatientCreateViewModel()) as ViewResult;

            Assert.IsTrue(string.IsNullOrEmpty(result.ViewName));
        }

        [TestMethod]
        [TestCategory("PatientController.Create.Post")]
        public void PatientController_Create_POST_Invalid_ShouldReturnTheCorrectViewModel()
        {
            _controller.ModelState.AddModelError("test", "test"); // Force invalid state

            var result = _controller.Create(_validOrganization.Id, new PatientCreateViewModel()) as ViewResult;

            Assert.IsInstanceOfType(result.Model, typeof(PatientCreateViewModel));
        }

        [TestMethod]
        [TestCategory("PatientController.Create.Post")]
        public void PatientController_Create_POST_Invalid_ShouldMapToOrganizationBaseViewModel()
        {
            _controller.ModelState.AddModelError("test", "test"); // Force invalid state

            var result = _controller.Create(_validOrganization.Id, new PatientCreateViewModel()) as ViewResult;
            var model = result.Model as PatientCreateViewModel;

            ViewModelAssert.AssertOrganizationBaseViewModel(_validOrganization, model);
        }
        #endregion

        #region Edit
        [TestMethod]
        [TestCategory("PatientController.Edit")]
        public void PatientController_Edit_ShouldReturnTheCorrectView()
        {
            var patient = GetValidPatientResult();

            _patientServiceMock.Setup(x => x.GetById(patient.Id)).Returns(patient);

            var result = _controller.Details(_validOrganization.Id, patient.Id) as ViewResult;

            Assert.IsTrue(string.IsNullOrEmpty(result.ViewName));
        }

        [TestMethod]
        [TestCategory("PatientController.Edit")]
        public void PatientController_Edit_ShouldReturnTheCorrectViewModel()
        {
            var patient = GetValidPatientResult();

            _patientServiceMock.Setup(x => x.GetById(patient.Id)).Returns(patient);

            var result = _controller.Details(_validOrganization.Id, patient.Id) as ViewResult;

            Assert.IsInstanceOfType(result.Model, typeof(PatientDetailsViewModel));
        }

        [TestMethod]
        [TestCategory("PatientController.Edit")]
        public void PatientController_Edit_ShouldMapToOrganizationBaseViewModel()
        {
            var patient = GetValidPatientResult();

            _patientServiceMock.Setup(x => x.GetById(patient.Id)).Returns(patient);

            var result = _controller.Details(_validOrganization.Id, patient.Id) as ViewResult;
            var model = result.Model as PatientDetailsViewModel;

            ViewModelAssert.AssertOrganizationBaseViewModel(_validOrganization, model);
        }

        [TestMethod]
        [TestCategory("PatientController.Edit")]
        public void PatientController_Edit_ShouldMapEditablePropertiesToViewModel()
        {
            var patient = GetValidPatientResult();

            _patientServiceMock.Setup(x => x.GetById(patient.Id)).Returns(patient);

            var result = _controller.Edit(_validOrganization.Id, patient.Id) as ViewResult;
            var model = result.Model as PatientEditViewModel;

            Assert.AreEqual(patient.Firstname, model.Firstname);
            Assert.AreEqual(patient.Lastname, model.Lastname);
            Assert.AreEqual(patient.BirthDate, model.BirthDate);
        }

        [TestMethod]
        [TestCategory("PatientController.Edit")]
        public void PatientController_Edit_ShouldMapPatientResultModelToViewModel()
        {
            var patient = GetValidPatientResult();

            _patientServiceMock.Setup(x => x.GetById(patient.Id)).Returns(patient);

            var result = _controller.Details(_validOrganization.Id, patient.Id) as ViewResult;
            var model = result.Model as PatientDetailsViewModel;

            Assert.AreSame(patient, model.Patient);
        }

        [TestMethod]
        [TestCategory("PatientController.Edit")]
        public void PatientController_Edit_ShouldMapPatientPostsToViewModel()
        {
            var patientPosts = PatientPostHelper.GetValidListOfPatientPosts();

            var patient = GetValidPatientResult();

            _patientServiceMock.Setup(x => x.GetById(patient.Id)).Returns(patient);
            _patientPostServiceMock.Setup(x => x.FindAllByPatient(patient.Id)).Returns(patientPosts);

            var result = _controller.Details(_validOrganization.Id, patient.Id) as ViewResult;
            var model = result.Model as PatientDetailsViewModel;

            Assert.AreSame(patientPosts, model.Posts);
        }

        [TestMethod]
        [TestCategory("PatientController.Edit")]
        public void PatientController_Edit_ShouldMapPatientContactInformations()
        {
            var contactInformations = FakeContactInformation.CreateResultModels();

            var patient = GetValidPatientResult();

            _patientServiceMock.Setup(x => x.GetById(patient.Id)).Returns(patient);
            _patientServiceMock.Setup(x => x.GetContactInformations(patient.Id)).Returns(contactInformations);

            var result = _controller.Edit(_validOrganization.Id, patient.Id) as ViewResult;
            var model = result.Model as PatientEditViewModel;

            Assert.AreSame(contactInformations, model.ContactInformations);
        }
        #endregion

        #region ContactPersons
        [TestMethod]
        [TestCategory("PatientController.ContactPersons")]
        public void ContactPersons_GivenAValidPatientId_ContactPersonsShouldNotBeNull()
        {
            var patient = GetValidPatientResult();

            var contactPersons = new PatientContactPersonRelationshipResultModel[0];
            _patientServiceMock.Setup(x => x.GetContactPersons(patient.Id)).Returns(contactPersons);

            var model = RequestContactPersons(patient);

            Assert.IsNotNull(model.ContactPersons);
        }

        [TestMethod]
        [TestCategory("PatientController.ContactPersons")]
        public void ContactPersons_GivenAValidPatientId_ShouldReturnAListOfContactPersons()
        {
            ContactPersons_ShouldReturnAListOfContactPersons((patient) => RequestContactPersons(patient));
        }

        [TestMethod]
        [TestCategory("PatientController.ContactPersons")]
        public void ContactPersons_GivenAValidOrganizationId_ShouldMapOrganization()
        {
            var patient = GetValidPatientResult();

            var model = RequestContactPersons(patient);

            ViewModelAssert.AssertOrganizationBaseViewModel(_validOrganization, model);
        }

        [TestMethod]
        [TestCategory("PatientController.ContactPersons")]
        public void ContactPersons_GivenAValidPatientId_ShouldMapPatient()
        {
            var patient = GetValidPatientResult();

            var model = RequestContactPersons(patient);

            Assert.AreEqual(patient.Id, model.Patient.Id);
        }

        [TestMethod]
        [TestCategory("PatientController.ContactPersons")]
        public void ContactPersons_GivenAValidPatient_ShouldReturnCorrectView()
        {
            var patient = GetValidPatientResult();
            _patientServiceMock.Setup(x => x.GetById(patient.Id)).Returns(patient);

            var result = _controller.ContactPersons(_validOrganization.Id, patient.Id) as ViewResult;

            Assert.IsTrue(string.IsNullOrEmpty(result.ViewName));
        }

        [TestMethod]
        [TestCategory("PatientController.ContactPersons")]
        public void ContactPersons_ListOfEmployees_ShouldNotContainAnyContactPersons()
        {
            ContactPersons_ShouldReturnAListOfEmployees(
                (patient) => RequestContactPersons(patient));
        }

        [TestMethod]
        [TestCategory("PatientController.ContactPersons")]
        public void ContactPersons_POST_Valid_GivenCompletePreviousContactPersons_ShouldCallAddPersonsWithCompleteParamteter()
        {
            var patient = GetValidPatientResult();

            var viewModel = new PatientContactPersonsViewModel()
            {
                EmployeeId = 1,
                CompletePreviousContactPersons = true
            };

            _controller.ContactPersons(_validOrganization.Id, patient.Id, viewModel);

            _patientServiceMock.Verify(x => x.AddContactPerson(patient.Id, viewModel.EmployeeId, viewModel.CompletePreviousContactPersons), Times.Once);
        }

        [TestMethod]
        [TestCategory("PatientController.ContactPersons")]
        public void ContactPersons_POST_Valid_ShouldRedirectToContactPersons()
        {
            var patient = GetValidPatientResult();

            var viewModel = new PatientContactPersonsViewModel()
            {
                EmployeeId = 1,
                CompletePreviousContactPersons = false
            };

            var result = _controller.ContactPersons(_validOrganization.Id, patient.Id, viewModel) as RedirectToRouteResult;

            Assert.IsTrue(result.RouteValues.ContainsKey("Action"), "Action missing");
            Assert.IsTrue(result.RouteValues.ContainsKey("Controller"), "Controller missing");
            Assert.IsTrue(result.RouteValues.ContainsKey("Area"), "Area missing");
            Assert.IsTrue(result.RouteValues.ContainsKey("Id"), "Id missing");
            Assert.IsTrue(result.RouteValues.ContainsKey("OrganizationId"), "OrganizationId missing");
            Assert.AreEqual("ContactPersons", result.RouteValues["Action"]);
            Assert.AreEqual("Patient", result.RouteValues["Controller"]);
            Assert.AreEqual("Administration", result.RouteValues["Area"]);
            Assert.AreEqual(patient.Id, result.RouteValues["Id"]);
            Assert.AreEqual(_validOrganization.Id, result.RouteValues["OrganizationId"]);
        }

        [TestMethod]
        [TestCategory("PatientController.ContactPersons")]
        public void ContactPersons_POST_Invalid_ShouldNotAddContactPerson()
        {
            var patient = GetValidPatientResult();

            var viewModel = new PatientContactPersonsViewModel();
            RequestContactPersons(patient, viewModel, invalidModelState: true);

            _patientServiceMock.Verify(x => x.AddContactPerson(patient.Id, viewModel.EmployeeId), Times.Never);
        }

        [TestMethod]
        [TestCategory("PatientController.ContactPersons")]
        public void ContactPersons_POST_Invalid_ShouldReturnAListOfContactPersons()
        {
            ContactPersons_ShouldReturnAListOfContactPersons(
                (patient) => RequestContactPersons(patient, new PatientContactPersonsViewModel(), invalidModelState: true));
        }

        [TestMethod]
        [TestCategory("PatientController.ContactPersons")]
        public void ContactPersons_POST_Invalid_ShouldReturnCorrectView()
        {
            _controller.ModelState.AddModelError("test", "test");

            var patient = GetValidPatientResult();
            _patientServiceMock.Setup(x => x.GetById(patient.Id)).Returns(patient);

            var viewModel = new PatientContactPersonsViewModel();
            var result = _controller.ContactPersons(_validOrganization.Id, patient.Id, viewModel) as ViewResult;

            Assert.IsTrue(string.IsNullOrEmpty(result.ViewName));
        }

        [TestMethod]
        [TestCategory("PatientController.ContactPersons")]
        public void ContactPersons_POST_Invalid_ShouldMapOrganization()
        {
            var patient = GetValidPatientResult();

            var viewModel = new PatientContactPersonsViewModel();
            var model = RequestContactPersons(patient, viewModel, invalidModelState: true);

            ViewModelAssert.AssertOrganizationBaseViewModel(_validOrganization, model);
        }

        [TestMethod]
        [TestCategory("PatientController.ContactPersons")]
        public void ContactPersons_POST_Invalid_ShouldMapPatient()
        {
            var patient = GetValidPatientResult();

            var viewModel = new PatientContactPersonsViewModel();
            var model = RequestContactPersons(patient, viewModel, invalidModelState: true);

            Assert.AreEqual(patient.Id, model.Patient.Id);
        }

        [TestMethod]
        [TestCategory("PatientController.ContactPersons")]
        public void ContactPersons_POST_Invalid_ShouldReturnThePostedViewmodel()
        {
            var patient = GetValidPatientResult();

            var viewModel = new PatientContactPersonsViewModel();
            var model = RequestContactPersons(patient, viewModel, invalidModelState: true);

            Assert.AreSame(viewModel, model, "New viewmodel have been created instead of using the posted ViewModel");
        }

        [TestMethod]
        [TestCategory("PatientController.ContactPersons")]
        public void ContactPersons_POST_Invalid_ListOfEmployees_ShouldNotContainAnyContactPersons()
        {
            ContactPersons_ShouldReturnAListOfEmployees(
                (patient) => RequestContactPersons(patient, new PatientContactPersonsViewModel(), invalidModelState: true));
        }

        private void ContactPersons_ShouldReturnAListOfContactPersons(Func<PatientResultModel, PatientContactPersonsViewModel> modelFunc)
        {
            var patient = GetValidPatientResult();

            var contactPersons = Builder<PatientContactPersonRelationshipResultModel>.CreateListOfSize(4).Build();
            _patientServiceMock.Setup(x => x.GetContactPersons(patient.Id)).Returns(contactPersons);

            var model = modelFunc.Invoke(patient);

            Assert.AreEqual(contactPersons.Count(), model.ContactPersons.Count());
        }

        private void ContactPersons_ShouldReturnAListOfEmployees(Func<PatientResultModel, PatientContactPersonsViewModel> modelFunc)
        {
            var patient = GetValidPatientResult();

            var employees = Builder<Employee>.CreateListOfSize(25)
                .All().With(x => x.OrganizationId, 1)
                .Build();
            _employeeServiceMock.Setup(x => x.FindAll(_validOrganization.Id)).Returns(employees);

            var contactPersons = Builder<PatientContactPersonRelationshipResultModel>.CreateListOfSize(4)
                .All().With(x =>
                {
                    x.Employee = employees.Single(y => y.Id == x.Id);
                    return x;
                })
                .Build();
            _patientServiceMock.Setup(x => x.GetContactPersons(patient.Id)).Returns(contactPersons);

            var model = modelFunc.Invoke(patient);

            Assert.IsFalse(model.EmployeesList.Any(x => contactPersons.Any(y => y.Employee.Id.ToString() == x.Value)),
                "There is a employee that area also contact person in the list of possible employees");
        }
        #endregion

        private PatientContactPersonsViewModel RequestContactPersons(PatientResultModel patient, PatientContactPersonsViewModel viewModel = null, bool invalidModelState = false)
        {
            _patientServiceMock.Setup(x => x.GetById(patient.Id)).Returns(patient);

            if (invalidModelState)
                _controller.ModelState.AddModelError("error", "error");

            var result = ((viewModel == null)
                ? _controller.ContactPersons(_validOrganization.Id, patient.Id)
                : _controller.ContactPersons(_validOrganization.Id, patient.Id, viewModel)) as ViewResult;

            var model = result.Model as PatientContactPersonsViewModel;

            return model;
        }

        private static Patient GetValidPatient()
        {
            return Builder<Patient>.CreateNew().Build();
        }

        private PatientResultModel GetValidPatientResult()
        {
            return Builder<PatientResultModel>.CreateNew()
                .With(x =>
                {
                    x.Description = Builder<Description>.CreateNew().Build();
                    return x;
                }).Build();
        }

        private static PatientCreateViewModel GetValidPatientCreateViewModel()
        {
            return Builder<PatientCreateViewModel>.CreateNew().With(x =>
            {
                x.Organization = GetValidOrganization();
                return x;
            }).Build();
        }

        private static Organization GetValidOrganization()
        {
            return Builder<Organization>.CreateNew().Build();
        }
    }
}
