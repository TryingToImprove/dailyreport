﻿using System;
using System.Security.Claims;
using System.Security.Principal;
using DailyReport.Presentation.Web.Areas.PatientModule.Models;
using DailyReport.Presentation.Web.Tests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DailyReport.Presentation.Web.Areas.Administration.Controllers;
using Moq;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Models.Core;
using FizzWare.NBuilder;
using System.Web.Mvc;
using DailyReport.Presentation.Web.Areas.Administration.Models;
using DailyReport.Presentation.Web.Tests.Asserts;
using DailyReport.Domain.Models;

namespace DailyReport.Presentation.Web.Tests.Controllers.Administration
{
    [TestClass]
    public class PatientDescriptionControllerTests
    {
        private Organization _validOrganization;
        private Patient _validPatient;
        private PatientResultModel _validPatientResult;

        private Mock<IPatientService> _patientServiceMock;
        private Mock<IOrganizationService> _organizationServiceMock;
        
        private PatientDescriptionController _controller;

        [TestInitialize]
        public void Initialize()
        {
            _validOrganization = GetValidOrganization();
            _validPatient = GetValidPatient();
            _validPatientResult = GetValidPatientResultModel();

            _validPatient.OrganizationId = _validOrganization.Id;

            _patientServiceMock = new Mock<IPatientService>();
            _patientServiceMock.Setup(x => x.GetById(_validPatient.Id)).Returns(_validPatientResult);

            _organizationServiceMock = new Mock<IOrganizationService>();
            _organizationServiceMock.Setup(x => x.GetById(_validOrganization.Id)).Returns(_validOrganization);

            _controller = new PatientDescriptionController(_patientServiceMock.Object, _organizationServiceMock.Object);

            var userMock = new Mock<IIdentity>();
            userMock.Setup(x => x.Name).Returns("1");

            var controllerContextMock = new Mock<ControllerContext>();
            controllerContextMock.Setup(x => x.HttpContext.User).Returns(new ClaimsPrincipal(userMock.Object));
            _controller.ControllerContext = controllerContextMock.Object;
        }

        [TestMethod]
        [TestCategory("PatientDescriptionController.Index")]
        public void PatientDescriptionController_Index_ShouldReturnTheCorrectView()
        {
            var result = _controller.Index(_validPatient.OrganizationId, _validPatient.Id) as ViewResult;

            Assert.IsTrue(string.IsNullOrEmpty(result.ViewName));
        }

        [TestMethod]
        [TestCategory("PatientDescriptionController.Index")]
        public void PatientDescriptionController_Index_ShouldReturnTheCorrectViewModel()
        {
            var result = _controller.Index(_validPatient.OrganizationId, _validPatient.Id) as ViewResult;

            Assert.IsInstanceOfType(result.Model, typeof(PatientDescriptionIndexViewModel));
        }

        [TestMethod]
        [TestCategory("PatientDescriptionController.Index")]
        public void PatientDescriptionController_Index_ShouldMapToOrganizationBaseViewModel()
        {
            var result = _controller.Index(_validPatient.OrganizationId, _validPatient.Id) as ViewResult;
            var model = result.Model as PatientDescriptionIndexViewModel;

            ViewModelAssert.AssertOrganizationBaseViewModel(_validOrganization, model);
        }

        [TestMethod]
        [TestCategory("PatientDescriptionController.Index")]
        public void PatientDescriptionController_Index_ShouldReturnAListOfDescriptions()
        {
            var descriptions = DescriptionHelper.GetValidListOfPatientDescriptions();
            _patientServiceMock.Setup(x => x.GetDescriptions(_validPatient.Id)).Returns(descriptions);

            var result = _controller.Index(_validPatient.OrganizationId, _validPatient.Id) as ViewResult;
            var model = result.Model as PatientDescriptionIndexViewModel;

            Assert.AreSame(descriptions, model.Descriptions);
        }

        [TestMethod]
        [TestCategory("PatientDescriptionController.Index")]
        public void PatientDescriptionController_Index_ShouldReturnAPatient()
        {
            var descriptions = DescriptionHelper.GetValidListOfPatientDescriptions();
            _patientServiceMock.Setup(x => x.GetDescriptions(_validPatient.Id)).Returns(descriptions);

            var result = _controller.Index(_validPatient.OrganizationId, _validPatient.Id) as ViewResult;
            var model = result.Model as PatientDescriptionIndexViewModel;

            Assert.AreSame(_validPatientResult, model.Patient);
        }

        [TestMethod]
        [TestCategory("PatientDescriptionController.Index")]
        public void PatientDescriptionController_Index_ShouldMapText()
        {
            var descriptions = DescriptionHelper.GetValidListOfPatientDescriptions();
            _patientServiceMock.Setup(x => x.GetDescriptions(_validPatient.Id)).Returns(descriptions);

            var result = _controller.Index(_validPatient.OrganizationId, _validPatient.Id) as ViewResult;
            var model = result.Model as PatientDescriptionIndexViewModel;

            Assert.AreEqual(_validPatientResult.Description.Text, model.Text);
        }

        [TestMethod]
        [TestCategory("PatientDescriptionController.Index")]
        public void PatientDescriptionController_Index_IfDescriptionIsNullTextShouldReturnEmptyString()
        {
            var patient = GetValidPatientResultModel();
            patient.Description = null;

            _patientServiceMock.Setup(x => x.GetById(_validPatientResult.Id)).Returns(patient);

            var descriptions = DescriptionHelper.GetValidListOfPatientDescriptions();
            _patientServiceMock.Setup(x => x.GetDescriptions(_validPatient.Id)).Returns(descriptions);

            var result = _controller.Index(_validPatient.OrganizationId, _validPatient.Id) as ViewResult;
            var model = result.Model as PatientDescriptionIndexViewModel;

            Assert.AreEqual(string.Empty, model.Text);
        }

        [TestMethod]
        [TestCategory("PatientDescriptionController.Index")]
        public void PatientDescriptionController_Index_POST_Invalid_ShouldReturnTheCorrectView()
        {
            _controller.ModelState.AddModelError("test", "test");

            var viewModel = new PatientDescriptionIndexViewModel();
            var result = _controller.Index(_validPatient.OrganizationId, _validPatient.Id, viewModel) as ViewResult;

            Assert.IsTrue(string.IsNullOrEmpty(result.ViewName));
        }

        [TestMethod]
        [TestCategory("PatientDescriptionController.Index")]
        public void PatientDescriptionController_Index_POST_Invalid_ShouldReturnTheCorrectViewModel()
        {
            _controller.ModelState.AddModelError("test", "test");

            var viewModel = new PatientDescriptionIndexViewModel();
            var result = _controller.Index(_validPatient.OrganizationId, _validPatient.Id, viewModel) as ViewResult;

            Assert.IsInstanceOfType(result.Model, typeof(PatientDescriptionIndexViewModel));
        }

        [TestMethod]
        [TestCategory("PatientDescriptionController.Index")]
        public void PatientDescriptionController_Index_POST_Invalid_ShouldMapToOrganizationBaseViewModel()
        {
            _controller.ModelState.AddModelError("test", "test");

            var viewModel = new PatientDescriptionIndexViewModel();
            var result = _controller.Index(_validPatient.OrganizationId, _validPatient.Id, viewModel) as ViewResult;
            var model = result.Model as PatientDescriptionIndexViewModel;

            ViewModelAssert.AssertOrganizationBaseViewModel(_validOrganization, model);
        }

        [TestMethod]
        [TestCategory("PatientDescriptionController.Index")]
        public void PatientDescriptionController_Index_POST_Invalid_ShouldReturnAListOfDescriptions()
        {
            _controller.ModelState.AddModelError("test", "test");

            var descriptions = DescriptionHelper.GetValidListOfPatientDescriptions();
            _patientServiceMock.Setup(x => x.GetDescriptions(_validPatient.Id)).Returns(descriptions);

            var viewModel = new PatientDescriptionIndexViewModel();
            var result = _controller.Index(_validPatient.OrganizationId, _validPatient.Id, viewModel) as ViewResult;
            var model = result.Model as PatientDescriptionIndexViewModel;

            Assert.AreSame(descriptions, model.Descriptions);
        }

        [TestMethod]
        [TestCategory("PatientDescriptionController.Index")]
        public void PatientDescriptionController_Index_POST_Invalid_ShouldReturnAPatient()
        {
            _controller.ModelState.AddModelError("test", "test");

            var descriptions = DescriptionHelper.GetValidListOfPatientDescriptions();
            _patientServiceMock.Setup(x => x.GetDescriptions(_validPatient.Id)).Returns(descriptions);

            var viewModel = new PatientDescriptionIndexViewModel();
            var result = _controller.Index(_validPatient.OrganizationId, _validPatient.Id, viewModel) as ViewResult;
            var model = result.Model as PatientDescriptionIndexViewModel;

            Assert.AreSame(_validPatientResult, model.Patient);
        }

        [TestMethod]
        [TestCategory("PatientDescriptionController.Index")]
        public void PatientDescriptionController_Index_POST_Invalid_ShouldMapText()
        {
            _controller.ModelState.AddModelError("test", "test");

            var descriptions = DescriptionHelper.GetValidListOfPatientDescriptions();
            _patientServiceMock.Setup(x => x.GetDescriptions(_validPatient.Id)).Returns(descriptions);

            var viewModel = new PatientDescriptionIndexViewModel()
            {
                Text = "IAmALegend"
            };

            var result = _controller.Index(_validPatient.OrganizationId, _validPatient.Id, viewModel) as ViewResult;
            var model = result.Model as PatientDescriptionIndexViewModel;

            Assert.AreEqual(viewModel.Text, model.Text);
        }

        [TestMethod]
        [TestCategory("PatientDescriptionController.Index")]
        public void PatientDescriptionController_Index_POST_Valid_ShouldCallService()
        {
            var viewModel = new PatientDescriptionIndexViewModel()
            {
                Text = "IAmLegend"
            };

            var result = _controller.Index(_validPatient.OrganizationId, _validPatient.Id, viewModel) as ViewResult;

            _patientServiceMock.Verify(x => x.CreateDescription(_validPatient.Id, It.IsAny<Description>()), Times.Once);
        }

        [TestMethod]
        [TestCategory("PatientDescriptionController.Index")]
        public void PatientDescriptionController_Index_POST_Valid_ShouldRedirectToIndexGet()
        {
            var viewModel = new PatientDescriptionIndexViewModel()
            {
                Text = "IAmLegend"
            };

            var result = _controller.Index(_validPatient.OrganizationId, _validPatient.Id, viewModel) as RedirectToRouteResult;

            Assert.IsTrue(result.RouteValues.ContainsKey("action"));
            Assert.IsTrue(result.RouteValues.ContainsKey("controller"));
            Assert.IsTrue(result.RouteValues.ContainsKey("area"));
            Assert.IsTrue(result.RouteValues.ContainsKey("organizationId"));
            Assert.IsTrue(result.RouteValues.ContainsKey("patientId"));
            Assert.AreEqual("Index", result.RouteValues["action"]);
            Assert.AreEqual("PatientDescription", result.RouteValues["controller"]);
            Assert.AreEqual("Administration", result.RouteValues["area"]);
            Assert.AreEqual(_validPatient.OrganizationId, result.RouteValues["organizationId"]);
            Assert.AreEqual(_validPatient.Id, result.RouteValues["patientId"]);
        }


        private static Organization GetValidOrganization()
        {
            return Builder<Organization>.CreateNew().Build();
        }

        private static Patient GetValidPatient()
        {
            return Builder<Patient>.CreateNew().Build();
        }

        private PatientResultModel GetValidPatientResultModel()
        {
            return Builder<PatientResultModel>.CreateNew()
                .With(x =>
                {
                    x.Description = Builder<Description>.CreateNew().Build();
                    return x;
                })
                .Build();
        }
    }
}
