﻿using System;
using System.Security.Claims;
using System.Security.Principal;
using DailyReport.Presentation.Web.Tests.Helpers;
using Microsoft.AspNet.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DailyReport.Presentation.Web.Areas.Administration.Controllers;
using Moq;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Models.Core;
using FizzWare.NBuilder;
using System.Web.Mvc;
using DailyReport.Presentation.Web.Areas.Administration.Models;
using DailyReport.Presentation.Web.Tests.Asserts;
using DailyReport.Domain.Models;

namespace DailyReport.Presentation.Web.Tests.Controllers.Administration
{
    [TestClass]
    public class PostControllerTests
    {
        private Organization _validOrganization;

        private Mock<IPatientPostService> _patientPostServiceMock;
        private Mock<IPatientService> _patientServiceMock;
        private Mock<IOrganizationService> _organizationServiceMock;
        private PostController _controller;

        [TestInitialize]
        public void Initialize()
        {
            _validOrganization = GetValidOrganization();

            _patientPostServiceMock = new Mock<IPatientPostService>();
            _patientServiceMock = new Mock<IPatientService>();

            _organizationServiceMock = new Mock<IOrganizationService>();
            _organizationServiceMock.Setup(x => x.GetById(_validOrganization.Id)).Returns(_validOrganization);

            _controller = new PostController(_patientPostServiceMock.Object, _patientServiceMock.Object, _organizationServiceMock.Object);

            var userMock = new Mock<IIdentity>();
            userMock.Setup(x => x.Name).Returns("1");

            var controllerContextMock = new Mock<ControllerContext>();
            controllerContextMock.Setup(x=> x.HttpContext.User).Returns(new ClaimsPrincipal(userMock.Object));
            _controller.ControllerContext = controllerContextMock.Object;
        }

        [TestMethod]
        [TestCategory("PostController.Index")]
        public void PostController_Index_ShouldReturnTheCorrectView()
        {
            var result = _controller.Index(_validOrganization.Id) as ViewResult;

            Assert.IsTrue(string.IsNullOrEmpty(result.ViewName));
        }

        [TestMethod]
        [TestCategory("PostController.Index")]
        public void PostController_Index_ShouldReturnTheCorrectViewModel()
        {
            var result = _controller.Index(_validOrganization.Id) as ViewResult;

            Assert.IsInstanceOfType(result.Model, typeof(PatientPostIndexViewModel));
        }

        [TestMethod]
        [TestCategory("PostController.Index")]
        public void PostController_Index_ShouldMapToOrganizationBaseViewModel()
        {
            var result = _controller.Index(_validOrganization.Id) as ViewResult;
            var model = result.Model as PatientPostIndexViewModel;

            ViewModelAssert.AssertOrganizationBaseViewModel(_validOrganization, model);
        }

        [TestMethod]
        [TestCategory("PostController.Index")]
        public void PostController_Index_ShouldReturnAListOfPosts()
        {
            var patientPosts = PatientPostHelper.GetValidListOfPatientPosts();
            _patientPostServiceMock.Setup(x => x.FindAll(_validOrganization.Id)).Returns(patientPosts);

            var result = _controller.Index(_validOrganization.Id) as ViewResult;
            var model = result.Model as PatientPostIndexViewModel;

            Assert.AreSame(patientPosts, model.Posts);
        }

        [TestMethod]
        [TestCategory("PostController.Create")]
        public void PostController_Create_ShouldReturnTheCorrectView()
        {
            var result = _controller.Create(_validOrganization.Id) as ViewResult;

            Assert.IsTrue(string.IsNullOrEmpty(result.ViewName));
        }

        [TestMethod]
        [TestCategory("PostController.Create")]
        public void PostController_Create_ShouldReturnTheCorrectViewModel()
        {
            var result = _controller.Create(_validOrganization.Id) as ViewResult;

            Assert.IsInstanceOfType(result.Model, typeof(PatientPostCreateViewModel));
        }

        [TestMethod]
        [TestCategory("PostController.Create")]
        public void PostController_Create_ShouldMapToOrganizationBaseViewModel()
        {
            var result = _controller.Create(_validOrganization.Id) as ViewResult;
            var model = result.Model as PatientPostCreateViewModel;

            ViewModelAssert.AssertOrganizationBaseViewModel(_validOrganization, model);
        }

        [TestMethod]
        [TestCategory("PostController.Create")]
        public void PostController_Create_ShouldReturnAListOfPatients()
        {
            var patients = Builder<Patient>.CreateListOfSize(5).All().With(x =>
            {
                x.OrganizationId = _validOrganization.Id;
                x.IsActive = true;
                return x;
            }).Build();

            _patientServiceMock.Setup(x => x.FindAll(_validOrganization.Id)).Returns(patients);

            var result = _controller.Create(_validOrganization.Id) as ViewResult;
            var model = result.Model as PatientPostCreateViewModel;

            Assert.AreSame(patients, model.Patients);
        }

        [TestMethod]
        [TestCategory("PostController.Create")]
        public void PostController_Create_POST_Invalid_ShouldReturnTheCorrectView()
        {
            _controller.ModelState.AddModelError("test", "test"); // force invalid state

            var viewModel = GetValidPatientPostCreateViewModel();
            var result = _controller.Create(_validOrganization.Id, viewModel) as ViewResult;

            Assert.IsTrue(string.IsNullOrEmpty(result.ViewName));
        }

        [TestMethod]
        [TestCategory("PostController.Create")]
        public void PostController_Create_POST_Invalid_ShouldReturnTheCorrectViewModel()
        {
            _controller.ModelState.AddModelError("test", "test"); // force invalid state

            var viewModel = GetValidPatientPostCreateViewModel();
            var result = _controller.Create(_validOrganization.Id, viewModel) as ViewResult;

            Assert.IsInstanceOfType(result.Model, typeof(PatientPostCreateViewModel));
        }

        [TestMethod]
        [TestCategory("PostController.Create")]
        public void PostController_Create_POST_Invalid_ShouldMapToOrganizationBaseViewModel()
        {
            _controller.ModelState.AddModelError("test", "test"); // force invalid state

            var viewModel = GetValidPatientPostCreateViewModel();
            var result = _controller.Create(_validOrganization.Id, viewModel) as ViewResult;
            var model = result.Model as PatientPostCreateViewModel;

            ViewModelAssert.AssertOrganizationBaseViewModel(_validOrganization, model);
        }

        [TestMethod]
        [TestCategory("PostController.Create")]
        public void PostController_Create_POST_Invalid_ShouldReturnAListOfPatients()
        {
            _controller.ModelState.AddModelError("test", "test"); // force invalid state

            var patients = Builder<Patient>.CreateListOfSize(5).All().With(x =>
            {
                x.OrganizationId = _validOrganization.Id;
                x.IsActive = true;
                return x;
            }).Build();

            _patientServiceMock.Setup(x => x.FindAll(_validOrganization.Id)).Returns(patients);

            var viewModel = GetValidPatientPostCreateViewModel();
            var result = _controller.Create(_validOrganization.Id, viewModel) as ViewResult;
            var model = result.Model as PatientPostCreateViewModel;

            Assert.AreSame(patients, model.Patients);
        }

        [TestMethod]
        [TestCategory("PostController.Create")]
        public void PostController_Create_POST_Valid_ShouldReturnTheCorrectView()
        {
            var viewModel = GetValidPatientPostCreateViewModel();
            var result = _controller.Create(_validOrganization.Id, viewModel) as ViewResult;

            Assert.AreEqual("Created", result.ViewName);
        }

        [TestMethod]
        [TestCategory("PostController.Create")]
        public void PostController_Create_POST_Valid_ShouldInsertPatientPost()
        {
            var viewModel = GetValidPatientPostCreateViewModel();
            var result = _controller.Create(_validOrganization.Id, viewModel) as ViewResult;

            _patientPostServiceMock.Verify(x => x.Insert(It.IsAny<PatientPostInsertModel>()), Times.Once);
        }

        [TestMethod]
        [TestCategory("PostController.Create")]
        public void PostController_Create_POST_Valid_ShouldReturnCreatedViewModel()
        {
            var viewModel = GetValidPatientPostCreateViewModel();
            var result = _controller.Create(_validOrganization.Id, viewModel) as ViewResult;

            Assert.IsInstanceOfType(result.Model, typeof(PatientPostCreatedViewModel));
        }

        [TestMethod]
        [TestCategory("PostController.Create")]
        public void PostController_Create_POST_Valid_ShouldMapToOrganizationBaseViewModel()
        {
            var viewModel = GetValidPatientPostCreateViewModel();
            var result = _controller.Create(_validOrganization.Id, viewModel) as ViewResult;
            var model = result.Model as PatientPostCreatedViewModel;

            ViewModelAssert.AssertOrganizationBaseViewModel(_validOrganization, model);
        }

        private PatientPostCreateViewModel GetValidPatientPostCreateViewModel()
        {
            return Builder<PatientPostCreateViewModel>.CreateNew()
                .With(x =>
                {
                    x.Patients = new[]{
                        new Patient{ Id = 1 }
                    };

                    return x;
                }).Build();
        }

        private static Organization GetValidOrganization()
        {
            return Builder<Organization>.CreateNew().Build();
        }
    }
}
