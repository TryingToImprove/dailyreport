﻿using System;
using DailyReport.Presentation.Web.Tests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DailyReport.Presentation.Web.Areas.Administration.Controllers;
using Moq;
using DailyReport.Domain.Infrastructure;
using System.Web.Mvc;
using DailyReport.Services;
using DailyReport.Presentation.Web.Areas.Administration.Models;
using System.ComponentModel.DataAnnotations;
using DailyReport.Domain.Models.Core;
using DailyReport.Domain.Infrastructure.Repositories;
using System.Linq;
using DailyReport.Domain.Infrastructure.Services;
using FizzWare.NBuilder;
using DailyReport.Presentation.Web.Tests.Asserts;
using System.Collections.Generic;
using DailyReport.Domain.Models;

namespace DailyReport.Presentation.Web.Tests.Controllers.Administration
{
    [TestClass]
    public class OrganizationControllerTests
    {
        private Organization _validOrganization;

        private IEnumerable<PatientPostResultModel> _validPatientPosts;

        private Mock<IOrganizationService> _organizationServiceMock;

        private Mock<IPatientPostService> _patientPostServiceMock;

        private Mock<IMedicationScheduleService> _medicationScheduleServiceMock;

        private OrganizationController _controller;

        [TestInitialize]
        public void Initialize()
        {
            _validOrganization = GetValidOrganization();
            _validPatientPosts = PatientPostHelper.GetValidListOfPatientPosts();

            _patientPostServiceMock = new Mock<IPatientPostService>();
            _patientPostServiceMock.Setup(x => x.FindAll(_validOrganization.Id)).Returns(_validPatientPosts);

            _organizationServiceMock = new Mock<IOrganizationService>();
            _organizationServiceMock.Setup(x => x.GetById(It.IsAny<int>())).Returns(_validOrganization);

            _medicationScheduleServiceMock = new Mock<IMedicationScheduleService>();

            _controller = new OrganizationController(_organizationServiceMock.Object, _patientPostServiceMock.Object, _medicationScheduleServiceMock.Object);
        }

        [TestMethod]
        public void OrganizationController_Index_ReturnsAModel()
        {
            var result = _controller.Index() as ViewResult;

            Assert.IsNotNull(result.Model);
            Assert.IsInstanceOfType(result.Model, typeof(OrganizationIndexViewModel));

            var model = (OrganizationIndexViewModel)result.Model;
            Assert.IsNotNull(model.Organizations);
        }

        [TestMethod]
        public void OrganizationController_Create_ReturnsAModel()
        {
            var result = _controller.Create() as ViewResult;

            Assert.IsNotNull(result.Model);
        }

        [TestMethod]
        public void OrganizationController_Create_POST_Invalid_ReturnCreateWithViewModel()
        {
            var viewModel = GetValidOrganizationCreateViewModel();

            _controller.ModelState.AddModelError("test", "test"); // Add a error, so ModelState is invalid

            var result = _controller.Create(viewModel) as ViewResult;

            Assert.IsTrue(string.IsNullOrEmpty(result.ViewName));
            Assert.AreEqual(viewModel, result.Model);
        }

        [TestMethod]
        public void OrganizationController_Create_POST_Valid_ShouldCallInsert()
        {
            var viewModel = GetValidOrganizationCreateViewModel();
            var result = _controller.Create(viewModel) as ViewResult;

            _organizationServiceMock.Verify(x => x.Insert(It.IsAny<Organization>()));
        }

        [TestMethod]
        public void OrganizationController_Create_POST_Valid_ShouldRedirectToIndex()
        {
            var viewModel = GetValidOrganizationCreateViewModel();

            var result = _controller.Create(viewModel) as RedirectToRouteResult;

            Assert.IsTrue(result.RouteValues.ContainsKey("action"));
            Assert.AreEqual("Index", result.RouteValues["action"]);
        }

        [TestMethod]
        public void OrganizationController_Edit_ShouldReturnAViewModel()
        {
            var result = _controller.Edit(_validOrganization.Id) as ViewResult;

            Assert.IsNotNull(result.Model);
            Assert.IsInstanceOfType(result.Model, typeof(OrganizationEditViewModel));
        }

        [TestMethod]
        public void OrganizationController_Edit_ShouldMapToViewModel()
        {
            var result = _controller.Edit(_validOrganization.Id) as ViewResult;
            var viewModel = result.Model as OrganizationEditViewModel;

            Assert.AreEqual(_validOrganization.Name, viewModel.Name);
            Assert.AreEqual(_validOrganization.TimeCreated, viewModel.TimeCreated);
            Assert.AreEqual(_validOrganization.Id, viewModel.Id);
        }

        [TestMethod]
        public void OrganizationController_Edit_ShouldMapToOrganizationBaseViewModel()
        {
            var result = _controller.Edit(_validOrganization.Id) as ViewResult;
            var viewModel = result.Model as OrganizationEditViewModel;

            ViewModelAssert.AssertOrganizationBaseViewModel(_validOrganization, viewModel);
        }

        [TestMethod]
        public void OrganizationController_Edit_ShouldReturnAListOfPatientPosts()
        {
            var result = _controller.Edit(_validOrganization.Id) as ViewResult;
            var viewModel = result.Model as OrganizationEditViewModel;

            Assert.AreEqual(_validPatientPosts.Count(), viewModel.Posts.Count());
            Assert.AreSame(_validPatientPosts, viewModel.Posts);
        }

        [TestMethod]
        public void OrganizationController_Edit_POST_Invalid_ShouldNotCallUpdate()
        {
            var viewModel = GetValidOrganizationEditViewModel();

            _controller.ModelState.AddModelError("test", "test"); // Force modelstate invalid

            var result = _controller.Edit(_validOrganization.Id, viewModel);

            _organizationServiceMock.Verify(x => x.Update(_validOrganization), Times.Never);
        }

        [TestMethod]
        public void OrganizationController_Edit_POST_Valid_ShouldCallUpdate()
        {
            var viewModel = GetValidOrganizationEditViewModel();

            var result = _controller.Edit(_validOrganization.Id, viewModel);

            _organizationServiceMock.Verify(x => x.Update(_validOrganization), Times.Once);
        }

        [TestMethod]
        public void OrganizationController_Edit_POST_Valid_ShouldMapToViewModel()
        {
            var viewModel = GetValidOrganizationEditViewModel();

            var result = _controller.Edit(_validOrganization.Id, viewModel) as ViewResult;
            var model = result.Model as OrganizationEditViewModel;

            Assert.AreEqual(_validOrganization.Name, model.Name);
            Assert.AreEqual(_validOrganization.TimeCreated, model.TimeCreated);
            Assert.AreEqual(_validOrganization.Id, model.Id);
        }

        [TestMethod]
        public void OrganizationController_Edit_POST_Valid_ShouldMapToOrganizationBaseViewModel()
        {
            var viewModel = GetValidOrganizationEditViewModel();
            var result = _controller.Edit(_validOrganization.Id, viewModel) as ViewResult;
            var resultViewModel = result.Model as OrganizationEditViewModel;

            ViewModelAssert.AssertOrganizationBaseViewModel(_validOrganization, resultViewModel);
        }

        [TestMethod]
        public void OrganizationController_Edit_POST_Valid_ShouldReturnAListOfPatientPosts()
        {
            var viewModel = GetValidOrganizationEditViewModel();
            var result = _controller.Edit(_validOrganization.Id, viewModel) as ViewResult;
            var resultViewModel = result.Model as OrganizationEditViewModel;

            Assert.AreEqual(_validPatientPosts.Count(), resultViewModel.Posts.Count());
            Assert.AreSame(_validPatientPosts, resultViewModel.Posts);
        }


        private OrganizationCreateViewModel GetValidOrganizationCreateViewModel()
        {
            return Builder<OrganizationCreateViewModel>.CreateNew().Build();
        }

        private Organization GetValidOrganization()
        {
            return Builder<Organization>.CreateNew().Build();
        }

        private OrganizationEditViewModel GetValidOrganizationEditViewModel()
        {
            return Builder<OrganizationEditViewModel>.CreateNew().With(x =>
            {
                x.Id = _validOrganization.Id;
                return x;
            }).Build();
        }
    }
}
