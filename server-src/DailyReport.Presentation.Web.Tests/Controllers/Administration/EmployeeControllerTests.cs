﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DailyReport.Presentation.Web.Areas.Administration.Controllers;
using Moq;
using DailyReport.Domain.Infrastructure;
using System.Web.Mvc;
using DailyReport.Services;
using DailyReport.Presentation.Web.Areas.Administration.Models;
using System.ComponentModel.DataAnnotations;
using DailyReport.Domain.Models.Core;
using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Infrastructure.Services;
using FizzWare.NBuilder;
using DailyReport.Presentation.Web.Tests.Asserts;

namespace DailyReport.Presentation.Web.Tests.Controllers.Administration
{
    [TestClass]
    public class EmployeeControllerTests
    {
        private Organization _validOrganization;

        private Mock<IEmployeeService> _employeeServiceMock;
        private Mock<IOrganizationService> _organizationServiceMock;
        private EmployeeController _controller;

        [TestInitialize]
        public void Initialize()
        {
            _validOrganization = GetValidOrganization();

            _employeeServiceMock = new Mock<IEmployeeService>();

            _organizationServiceMock = new Mock<IOrganizationService>();
            _organizationServiceMock.Setup(x => x.GetById(_validOrganization.Id)).Returns(_validOrganization);

            _controller = new EmployeeController(_organizationServiceMock.Object, _employeeServiceMock.Object);
        }


        [TestMethod]
        [TestCategory("EmployeeController.Index")]
        public void EmployeeController_Index_ShouldReturnTheCorrectView()
        {
            var result = _controller.Index(_validOrganization.Id) as ViewResult;

            Assert.IsTrue(string.IsNullOrEmpty(result.ViewName));
        }

        [TestMethod]
        [TestCategory("EmployeeController.Index")]
        public void EmployeeController_Index_ShouldReturnTheCorrectViewModel()
        {
            var result = _controller.Index(_validOrganization.Id) as ViewResult;

            Assert.IsInstanceOfType(result.Model, typeof(EmployeeIndexViewModel));
        }

        [TestMethod]
        [TestCategory("EmployeeController.Index")]
        public void EmployeeController_Index_ShouldMapToOrganizationBaseViewModel()
        {
            var result = _controller.Index(_validOrganization.Id) as ViewResult;
            var model = result.Model as EmployeeIndexViewModel;

            ViewModelAssert.AssertOrganizationBaseViewModel(_validOrganization, model);
        }

        [TestMethod]
        [TestCategory("EmployeeController.Index")]
        public void EmployeeController_Index_ShouldReturnAListOfEmployees()
        {
            var employees = Builder<Employee>.CreateListOfSize(5).All().With(x =>
            {
                x.OrganizationId = _validOrganization.Id;
                x.IsActive = true;
                return x;
            }).Build();

            _employeeServiceMock.Setup(x => x.FindAll(_validOrganization.Id)).Returns(employees);

            var result = _controller.Index(_validOrganization.Id) as ViewResult;
            var model = result.Model as EmployeeIndexViewModel;

            Assert.AreSame(employees, model.Employees);
        }
       
        #region Create
        [TestMethod]
        public void EmployeeController_Create_ReturnsAModel()
        {
            var result = _controller.Create(_validOrganization.Id) as ViewResult;

            Assert.IsNotNull(result.Model);
        }

        [TestMethod]
        public void EmployeeController_Create_ShouldReturnAValidViewModel()
        {
            var result = _controller.Create(_validOrganization.Id) as ViewResult;
            var model = result.Model as EmployeeCreateViewModel;

            Assert.IsTrue(string.IsNullOrEmpty(model.Firstname));
            Assert.IsTrue(string.IsNullOrEmpty(model.Lastname));
            ViewModelAssert.AssertOrganizationBaseViewModel(_validOrganization, model);
        }

        [TestMethod]
        public void Create_POST_Invalid_ReturnCreateWithViewModel()
        {
            var viewModel = GetValidEmployeeCreateViewModel(); ;
            _controller.ModelState.AddModelError("test", "test"); // Add a error, so ModelState is invalid

            var result = _controller.Create(_validOrganization.Id, viewModel) as ViewResult;

            Assert.IsTrue(string.IsNullOrEmpty(result.ViewName));
            Assert.AreEqual(viewModel, result.Model);

            var model = result.Model as EmployeeCreateViewModel;

            ViewModelAssert.AssertOrganizationBaseViewModel(_validOrganization, model);
        }

        [TestMethod]
        public void Create_POST_Valid_ShouldCallInsert()
        {
            var viewModel = GetValidEmployeeCreateViewModel();
            var result = _controller.Create(_validOrganization.Id, viewModel) as ViewResult;

            _employeeServiceMock.Verify(x => x.Insert(It.IsAny<Employee>()));
        }

        [TestMethod]
        public void Create_POST_Valid_ShouldRedirectToOrganizationIndex()
        {
            var viewModel = GetValidEmployeeCreateViewModel();
            var result = _controller.Create(_validOrganization.Id, viewModel) as RedirectToRouteResult;

            Assert.IsTrue(result.RouteValues.ContainsKey("action"));
            Assert.IsTrue(result.RouteValues.ContainsKey("controller"));
            Assert.IsTrue(result.RouteValues.ContainsKey("area"));
            Assert.IsTrue(result.RouteValues.ContainsKey("id"));
            Assert.AreEqual("Edit", result.RouteValues["action"]);
            Assert.AreEqual("Organization", result.RouteValues["controller"]);
            Assert.AreEqual("Administration", result.RouteValues["area"]);
            Assert.AreEqual(_validOrganization.Id, result.RouteValues["id"]);
        }
        #endregion

        [TestMethod]
        public void Delete_ShouldReturnConfirmationView()
        {
            var employee = GetValidEmployee();
            _employeeServiceMock.Setup(x => x.GetById(employee.Id)).Returns(employee);

            var result = _controller.Delete(1) as ViewResult;

            Assert.AreEqual("ConfirmDelete", result.ViewName);
        }

        [TestMethod]
        public void Delete_ShouldReturnValidViewModel()
        {
            var employee = GetValidEmployee();
            _employeeServiceMock.Setup(x => x.GetById(employee.Id)).Returns(employee);

            var result = _controller.Delete(1) as ViewResult;
            var model = result.Model as EmployeeConfirmDeleteViewModel;

            Assert.AreEqual(employee.OrganizationId, model.Organization.Id);
            Assert.AreEqual(employee.Id, model.EmployeeId);
            Assert.AreEqual(employee.Firstname, model.Firstname);
            Assert.AreEqual(employee.Lastname, model.Lastname);
            Assert.AreEqual(employee.TimeCreated, model.TimeCreated);

            ViewModelAssert.AssertOrganizationBaseViewModel(_validOrganization, model);
        }

        [TestMethod]
        public void Delete_POST_ShouldCallUpdateActiveStateWithFalse()
        {
            var employee = GetValidEmployee();
            _employeeServiceMock.Setup(x => x.GetById(employee.Id)).Returns(employee);

            var result = _controller.Delete(employee.Id, null);

            _employeeServiceMock.Verify(x => x.UpdateActiveState(employee.Id, false), Times.Once);
        }

        [TestMethod]
        public void Delete_POST_ShouldRedirectToOrganization()
        {
            var employee = GetValidEmployee();
            _employeeServiceMock.Setup(x => x.GetById(employee.Id)).Returns(employee);

            var result = _controller.Delete(employee.Id, null) as RedirectToRouteResult;

            Assert.IsTrue(result.RouteValues.ContainsKey("action"));
            Assert.IsTrue(result.RouteValues.ContainsKey("controller"));
            Assert.IsTrue(result.RouteValues.ContainsKey("area"));
            Assert.IsTrue(result.RouteValues.ContainsKey("id"));
            Assert.AreEqual("Edit", result.RouteValues["action"]);
            Assert.AreEqual("Organization", result.RouteValues["controller"]);
            Assert.AreEqual("Administration", result.RouteValues["area"]);
            Assert.AreEqual(employee.OrganizationId, result.RouteValues["id"]);
        }

        [TestMethod]
        public void Edit_ShouldReturnView()
        {
            var employee = GetValidEmployee();
            _employeeServiceMock.Setup(x => x.GetById(employee.Id)).Returns(employee);

            var result = _controller.Edit(employee.Id) as ViewResult;

            Assert.IsTrue(string.IsNullOrEmpty(result.ViewName));
        }

        [TestMethod]
        public void Edit_POST_ShouldReturnView()
        {
            var employee = GetValidEmployee();
            var viewModel = GetValidEmployeeEditViewModel();

            _employeeServiceMock.Setup(x => x.GetById(employee.Id)).Returns(employee);

            var result = _controller.Edit(employee.Id, viewModel) as ViewResult;

            Assert.IsTrue(string.IsNullOrEmpty(result.ViewName));
        }

        [TestMethod]
        public void Edit_ShouldReturnValidViewModel()
        {
            var employee = GetValidEmployee();
            var viewModel = GetValidEmployeeEditViewModel();

            _employeeServiceMock.Setup(x => x.GetById(employee.Id)).Returns(employee);

            var result = _controller.Edit(employee.Id, viewModel) as ViewResult;
            Assert.IsNotNull(result.Model);
            Assert.IsInstanceOfType(result.Model, typeof(EmployeeEditViewModel));

            var model = result.Model as EmployeeEditViewModel;
            Assert.AreEqual(employee.OrganizationId, model.Organization.Id);
            Assert.AreEqual(employee.Id, model.Id);
            Assert.AreEqual(employee.Firstname, model.Firstname);
            Assert.AreEqual(employee.Lastname, model.Lastname);

            ViewModelAssert.AssertOrganizationBaseViewModel(_validOrganization, model);
        }

        [TestMethod]
        public void Edit_POST_IfModelStateIsInvalidThenDoNotCallUpdateOnEmployeeService()
        {
            var employee = GetValidEmployee();
            var viewModel = GetValidEmployeeEditViewModel();

            _employeeServiceMock.Setup(x => x.GetById(employee.Id)).Returns(employee);
            _controller.ModelState.AddModelError("test", "test"); // Force invalid modelstate

            var result = _controller.Edit(employee.Id, viewModel) as ViewResult;

            _employeeServiceMock.Verify(x => x.Update(employee), Times.Never);
        }

        [TestMethod]
        public void Edit_POST_IfModelStateIsValidThenCallUpdateOnEmployeeService()
        {
            var employee = GetValidEmployee();
            var viewModel = GetValidEmployeeEditViewModel();

            _employeeServiceMock.Setup(x => x.GetById(employee.Id)).Returns(employee);

            var result = _controller.Edit(employee.Id, viewModel) as ViewResult;

            _employeeServiceMock.Verify(x => x.Update(employee), Times.Once);
        }

        [TestMethod]
        public void Edit_POST_ShouldReturnValidViewModel()
        {
            var employee = GetValidEmployee();
            var viewModel = GetValidEmployeeEditViewModel();

            _employeeServiceMock.Setup(x => x.GetById(employee.Id)).Returns(employee);

            var result = _controller.Edit(employee.Id, viewModel) as ViewResult;
            Assert.IsNotNull(result.Model);
            Assert.IsInstanceOfType(result.Model, typeof(EmployeeEditViewModel));

            var model = result.Model as EmployeeEditViewModel;
            Assert.AreEqual(employee.OrganizationId, model.Organization.Id);
            Assert.AreEqual(employee.Id, model.Id);
            Assert.AreEqual(viewModel.Firstname, model.Firstname);
            Assert.AreEqual(viewModel.Lastname, model.Lastname);

            ViewModelAssert.AssertOrganizationBaseViewModel(_validOrganization, model);
        }

        private Employee GetValidEmployee()
        {
            return Builder<Employee>.CreateNew().With(x =>
            {
                x.OrganizationId = _validOrganization.Id;
                return x;
            }).Build();
        }

        private Organization GetValidOrganization()
        {
            return Builder<Organization>.CreateNew().Build();
        }

        private EmployeeCreateViewModel GetValidEmployeeCreateViewModel()
        {
            return Builder<EmployeeCreateViewModel>.CreateNew().With(x =>
            {
                x.Organization = _validOrganization;
                return x;
            }).Build();
        }

        private EmployeeEditViewModel GetValidEmployeeEditViewModel()
        {
            return Builder<EmployeeEditViewModel>.CreateNew().With(x =>
            {
                x.Organization = _validOrganization;
                return x;
            }).Build();
        }
    }
}
