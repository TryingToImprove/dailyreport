﻿using System.Linq;
using System.Web.Mvc;
using DailyReport.Domain.Extensions;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using DailyReport.Presentation.Web.App_Start;
using DailyReport.Presentation.Web.Areas.Administration.Controllers;
using DailyReport.Presentation.Web.Areas.Administration.Models;
using DailyReport.Presentation.Web.Tests.Asserts;
using FizzWare.NBuilder;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Globalization;
using System;
using System.Security.Claims;
using System.Security.Principal;

namespace DailyReport.Presentation.Web.Tests.Controllers.Administration
{
    [TestClass]
    public class MedicationScheduleControllerTests
    {
        private Mock<IMedicationService> _medicationServiceMock;
        private Mock<IMedicationScheduleService> _medicationScheduleServiceMock;
        private Mock<IOrganizationService> _organizationServiceMock;
        private Mock<IPatientService> _patientServiceMock;

        private Organization _validOrganization;

        [TestInitialize]
        public void Initialize()
        {
            AutomapperWebConfiguration.Configure();

            _validOrganization = Builder<Organization>.CreateNew().Build();

            _medicationServiceMock = new Mock<IMedicationService>();
            _medicationScheduleServiceMock = new Mock<IMedicationScheduleService>();

            _organizationServiceMock = new Mock<IOrganizationService>();
            _organizationServiceMock.Setup(x => x.GetById(_validOrganization.Id)).Returns(_validOrganization);

            _patientServiceMock = new Mock<IPatientService>();
        }

        [TestMethod]
        [TestCategory("MedicationScheduleController.Index")]
        public void MedicationScheduleController_Index_ShouldReturnCorrectView()
        {
            var controller = GetController();

            var result = controller.Index(1) as ViewResult;

            Assert.IsTrue(string.IsNullOrEmpty(result.ViewName));
        }

        [TestMethod]
        [TestCategory("MedicationScheduleController.Index")]
        public void MedicationScheduleController_Index_ShouldReturnCorrectViewModel()
        {
            var controller = GetController();

            var result = controller.Index(1) as ViewResult;

            Assert.IsNotNull(result.Model);
            Assert.IsInstanceOfType(result.Model, typeof(MedicationScheduleIndexViewModel));
        }

        [TestMethod]
        [TestCategory("MedicationScheduleController.Index")]
        public void MedicationScheduleController_Index_ShouldReturnMappedOrganization()
        {
            var controller = GetController();

            var result = controller.Index(1) as ViewResult;
            var model = result.Model as MedicationScheduleIndexViewModel;

            ViewModelAssert.AssertOrganizationBaseViewModel(_validOrganization, model);
        }

        [TestMethod]
        [TestCategory("MedicationScheduleController.Index")]
        public void MedicationScheduleController_Index_ShouldReturnAListOfMedicationSchedules()
        {
            var medicationSchedules = Builder<MedicationScheduleResultModel>.CreateListOfSize(10).Build();
            _medicationScheduleServiceMock.Setup(x => x.FindAll()).Returns(medicationSchedules);

            var controller = GetController();

            var result = controller.Index(1) as ViewResult;
            var model = result.Model as MedicationScheduleIndexViewModel;

            Assert.AreSame(medicationSchedules, model.MedicationSchedules);
        }

        [TestMethod]
        [TestCategory("MedicationScheduleController.Edit")]
        public void MedicationScheduleController_Edit_ShouldReturnCorrectView()
        {
            var medicationSchedule = GetMedicationSchedule();
            var controller = GetController();

            var result = controller.Edit(1, medicationSchedule.Id) as ViewResult;

            Assert.IsTrue(string.IsNullOrEmpty(result.ViewName));
        }

        [TestMethod]
        [TestCategory("MedicationScheduleController.Edit")]
        public void MedicationScheduleController_Edit_ShouldReturnCorrectViewModel()
        {
            var medicationSchedule = GetMedicationSchedule();
            var controller = GetController();

            var result = controller.Edit(1, medicationSchedule.Id) as ViewResult;

            Assert.IsNotNull(result.Model);
            Assert.IsInstanceOfType(result.Model, typeof(MedicationScheduleEditViewModel));
        }

        [TestMethod]
        [TestCategory("MedicationScheduleController.Edit")]
        public void MedicationScheduleController_Edit_ShouldReturnMappedOrganization()
        {
            var medicationSchedule = GetMedicationSchedule();
            var controller = GetController();

            var result = controller.Edit(1, medicationSchedule.Id) as ViewResult;
            var model = result.Model as MedicationScheduleEditViewModel;

            ViewModelAssert.AssertOrganizationBaseViewModel(_validOrganization, model);
        }

        [TestMethod]
        [TestCategory("MedicationScheduleController.Edit")]
        public void MedicationScheduleController_Edit_ShouldReturnAMedicationSchedule()
        {
            var medicationSchedule = GetMedicationSchedule();
            _medicationScheduleServiceMock.Setup(x => x.GetById(medicationSchedule.Id)).Returns(medicationSchedule);

            var controller = GetController();

            var result = controller.Edit(1, medicationSchedule.Id) as ViewResult;
            var model = result.Model as MedicationScheduleEditViewModel;

            Assert.AreSame(medicationSchedule, model.MedicationSchedule);
        }

        [TestMethod]
        [TestCategory("MedicationScheduleController.Create")]
        public void MedicationScheduleController_Create_ShouldReturnCorrectView()
        {
            var medicationSchedule = GetMedicationSchedule();
            var controller = GetController();

            var result = controller.Create(1) as ViewResult;

            Assert.IsTrue(string.IsNullOrEmpty(result.ViewName));
        }

        [TestMethod]
        [TestCategory("MedicationScheduleController.Create")]
        public void MedicationScheduleController_Create_ShouldReturnCorrectViewModel()
        {
            var medicationSchedule = GetMedicationSchedule();
            var controller = GetController();

            var result = controller.Create(1) as ViewResult;

            Assert.IsNotNull(result.Model);
            Assert.IsInstanceOfType(result.Model, typeof(MedicationScheduleCreateViewModel));
        }

        [TestMethod]
        [TestCategory("MedicationScheduleController.Create")]
        public void MedicationScheduleController_Create_ShouldReturnMappedOrganization()
        {
            var medicationSchedule = GetMedicationSchedule();
            var controller = GetController();

            var result = controller.Create(1) as ViewResult;
            var model = result.Model as MedicationScheduleCreateViewModel;

            ViewModelAssert.AssertOrganizationBaseViewModel(_validOrganization, model);
        }

        [TestMethod]
        [TestCategory("MedicationScheduleController.Create")]
        public void MedicationScheduleController_Create_ShouldReturnASelectListOfMedications()
        {
            var medications = Builder<MedicationResultModel>.CreateListOfSize(10)
                .All()
                .With(x =>
                {
                    x.Brand = Builder<MedicationBrand>.CreateNew().Build();
                    return x;
                })
                .Build();

            var medicationList = medications.Select(x => new SelectListItem { Text = x.GetDisplayName(), Value = x.Id.ToString(CultureInfo.InvariantCulture) });
            _medicationServiceMock.Setup(x => x.FindAll()).Returns(medications);

            var medicationSchedule = GetMedicationSchedule();
            var controller = GetController();

            var result = controller.Create(1) as ViewResult;
            var model = result.Model as MedicationScheduleCreateViewModel;

            Assert.IsNotNull(model.MedicationList);
            Assert.AreEqual(medicationList.Count(), model.MedicationList.Count());
            for (var i = 0; i < model.MedicationList.Count(); i++)
            {
                var actualMedicationItem = model.MedicationList.ElementAt(i);
                var expectedMedicationItem = medicationList.ElementAt(i);

                Assert.AreEqual(expectedMedicationItem.Text, actualMedicationItem.Text);
                Assert.AreEqual(expectedMedicationItem.Value, actualMedicationItem.Value);
            }
        }

        [TestMethod]
        [TestCategory("MedicationScheduleController.Create")]
        public void MedicationScheduleController_Create_ShouldReturnASelectListOfPatients()
        {
            var patients = Builder<Patient>.CreateListOfSize(10)
                .Build();

            var patientList = patients.Select(x => new SelectListItem { Text = x.GetDisplayName(), Value = x.Id.ToString(CultureInfo.InvariantCulture) });
            _patientServiceMock.Setup(x => x.FindAll(1)).Returns(patients);

            var medicationSchedule = GetMedicationSchedule();
            var controller = GetController();

            var result = controller.Create(1) as ViewResult;
            var model = result.Model as MedicationScheduleCreateViewModel;

            Assert.IsNotNull(model.PatientList);
            Assert.AreEqual(patientList.Count(), model.PatientList.Count());
            for (var i = 0; i < model.PatientList.Count(); i++)
            {
                var actualPatientItem = model.PatientList.ElementAt(i);
                var expectedPatientItem = patientList.ElementAt(i);

                Assert.AreEqual(expectedPatientItem.Text, actualPatientItem.Text);
                Assert.AreEqual(expectedPatientItem.Value, actualPatientItem.Value);
            }
        }

        [TestMethod]
        [TestCategory("MedicationScheduleController.Create")]
        public void MedicationScheduleController_Create_ShouldReturnCurrentDateAsStartDate()
        {
            var medicationSchedule = GetMedicationSchedule();
            var controller = GetController();

            var result = controller.Create(1) as ViewResult;
            var model = result.Model as MedicationScheduleCreateViewModel;

            Assert.AreEqual(DateTime.Now.Date, model.StartDate);
        }

        [TestMethod]
        [TestCategory("MedicationScheduleController.Create")]
        public void MedicationScheduleController_Create_POST_Invalid_ShouldReturnCorrectViewModel()
        {
            var medicationSchedule = GetMedicationSchedule();
            var controller = GetController(invalidModelState: true);

            var result = controller.Create(1, GetCreateViewModel()) as ViewResult;

            Assert.IsNotNull(result.Model);
            Assert.IsInstanceOfType(result.Model, typeof(MedicationScheduleCreateViewModel));
        }

        [TestMethod]
        [TestCategory("MedicationScheduleController.Create")]
        public void MedicationScheduleController_Create_POST_Invalid_ShouldReturnMappedOrganization()
        {
            var medicationSchedule = GetMedicationSchedule();
            var controller = GetController(invalidModelState: true);

            var result = controller.Create(1, GetCreateViewModel()) as ViewResult;
            var model = result.Model as MedicationScheduleCreateViewModel;

            ViewModelAssert.AssertOrganizationBaseViewModel(_validOrganization, model);
        }

        [TestMethod]
        [TestCategory("MedicationScheduleController.Create")]
        public void MedicationScheduleController_Create_POST_Invalid_ShouldReturnASelectListOfMedications()
        {
            var medications = Builder<MedicationResultModel>.CreateListOfSize(10)
                .All()
                .With(x =>
                {
                    x.Brand = Builder<MedicationBrand>.CreateNew().Build();
                    return x;
                })
                .Build();

            var medicationList = medications.Select(x => new SelectListItem { Text = x.GetDisplayName(), Value = x.Id.ToString(CultureInfo.InvariantCulture) });
            _medicationServiceMock.Setup(x => x.FindAll()).Returns(medications);

            var medicationSchedule = GetMedicationSchedule();
            var controller = GetController(invalidModelState: true);

            var result = controller.Create(1, GetCreateViewModel()) as ViewResult;
            var model = result.Model as MedicationScheduleCreateViewModel;

            Assert.IsNotNull(model.MedicationList);
            Assert.AreEqual(medicationList.Count(), model.MedicationList.Count());
            for (var i = 0; i < model.MedicationList.Count(); i++)
            {
                var actualMedicationItem = model.MedicationList.ElementAt(i);
                var expectedMedicationItem = medicationList.ElementAt(i);

                Assert.AreEqual(expectedMedicationItem.Text, actualMedicationItem.Text);
                Assert.AreEqual(expectedMedicationItem.Value, actualMedicationItem.Value);
            }
        }

        [TestMethod]
        [TestCategory("MedicationScheduleController.Create")]
        public void MedicationScheduleController_Create_POST_Invalid_ShouldReturnASelectListOfPatients()
        {
            var patients = Builder<Patient>.CreateListOfSize(10)
                .Build();

            var patientList = patients.Select(x => new SelectListItem { Text = x.GetDisplayName(), Value = x.Id.ToString(CultureInfo.InvariantCulture) });
            _patientServiceMock.Setup(x => x.FindAll(1)).Returns(patients);

            var medicationSchedule = GetMedicationSchedule();
            var controller = GetController(invalidModelState: true);

            var result = controller.Create(1, GetCreateViewModel()) as ViewResult;
            var model = result.Model as MedicationScheduleCreateViewModel;

            Assert.IsNotNull(model.PatientList);
            Assert.AreEqual(patientList.Count(), model.PatientList.Count());
            for (var i = 0; i < model.PatientList.Count(); i++)
            {
                var actualPatientItem = model.PatientList.ElementAt(i);
                var expectedPatientItem = patientList.ElementAt(i);

                Assert.AreEqual(expectedPatientItem.Text, actualPatientItem.Text);
                Assert.AreEqual(expectedPatientItem.Value, actualPatientItem.Value);
            }
        }

        [TestMethod]
        [TestCategory("MedicationScheduleController.Create")]
        public void MedicationScheduleController_Create_POST_ShouldReturnRedirectResult()
        {
            var controller = GetController();
            var result = controller.Create(1, GetCreateViewModel());

            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult));
        }

        [TestMethod]
        [TestCategory("MedicationScheduleController.Create")]
        public void MedicationScheduleController_Create_POST_ShouldCallCreateOnService()
        {
            var controller = GetController();
            var result = controller.Create(1, GetCreateViewModel());

            _medicationScheduleServiceMock.Verify(x => x.Insert(It.IsAny<MedicationScheduleInsertModel>()), Times.Once);
        }

        [TestMethod]
        [TestCategory("MedicationScheduleController.Create")]
        public void MedicationScheduleController_Create_POST_ShouldRedirectToIndex()
        {
            var controller = GetController();
            var result = controller.Create(1, GetCreateViewModel()) as RedirectToRouteResult;

            Assert.IsTrue(result.RouteValues.ContainsKey("organizationId"));
            Assert.IsTrue(result.RouteValues.ContainsKey("controller"));
            Assert.IsTrue(result.RouteValues.ContainsKey("action"));
            Assert.IsTrue(result.RouteValues.ContainsKey("area"));

            Assert.AreEqual(_validOrganization.Id, result.RouteValues["organizationId"]);
            Assert.AreEqual("MedicationSchedule", result.RouteValues["controller"]);
            Assert.AreEqual("Index", result.RouteValues["action"]);
            Assert.AreEqual("Administration", result.RouteValues["area"]);
        }

        [TestMethod]
        [TestCategory("MedicationScheduleController.Details")]
        public void MedicationScheduleController_Details_ShouldReturnCorrectView()
        {
            var controller = GetController();

            var result = controller.Details(1, 1, 1) as ViewResult;

            Assert.IsTrue(string.IsNullOrEmpty(result.ViewName));
        }

        [TestMethod]
        [TestCategory("MedicationScheduleController.Details")]
        public void MedicationScheduleController_Details_ShouldReturnCorrectViewModel()
        {
            var controller = GetController();

            var result = controller.Details(1, 1, 1) as ViewResult;

            Assert.IsNotNull(result.Model);
            Assert.IsInstanceOfType(result.Model, typeof(MedicationScheduleDetailsViewModel));
        }

        [TestMethod]
        [TestCategory("MedicationScheduleController.Details")]
        public void MedicationScheduleController_Details_ShouldReturnMappedOrganization()
        {
            var controller = GetController();

            var result = controller.Details(1, 1, 1) as ViewResult;
            var model = result.Model as MedicationScheduleDetailsViewModel;

            ViewModelAssert.AssertOrganizationBaseViewModel(_validOrganization, model);
        }

        [TestMethod]
        [TestCategory("MedicationScheduleController.Details")]
        public void MedicationScheduleController_Details_ShouldReturnMedicationSchedule()
        {
            var medicationSchedule = GetMedicationSchedule();
            _medicationScheduleServiceMock.Setup(x => x.GetById(1)).Returns(medicationSchedule);
            var controller = GetController();

            var result = controller.Details(1, 1, 1) as ViewResult;
            var model = result.Model as MedicationScheduleDetailsViewModel;

            Assert.AreEqual(medicationSchedule, model.MedicationSchedule);
        }

        [TestMethod]
        [TestCategory("MedicationScheduleController.Details")]
        public void MedicationScheduleController_Details_ShouldReturnAListOfMedicationScheduleItemHistories()
        {
            var medicationHistories = Builder<MedicationScheduleItemHistoryResultModel>.CreateListOfSize(10)
                .All()
                .With(x =>
                {
                    x.MedicationScheduleItemHistoryId = 1;

                    return x;
                })
                .Build();
            _medicationScheduleServiceMock.Setup(x => x.GetHistory(1)).Returns(medicationHistories);

            var controller = GetController();

            var result = controller.Details(1, 1, 1) as ViewResult;
            var model = result.Model as MedicationScheduleDetailsViewModel;

            Assert.AreSame(medicationHistories, model.Histories);
        }

        [TestMethod]
        [TestCategory("MedicationScheduleController.Complete")]
        public void MedicationScheduleController_Complete_ShouldReturnCorrectView()
        {
            var medicationSchedule = GetMedicationSchedule();
            _medicationScheduleServiceMock.Setup(x => x.GetById(1)).Returns(medicationSchedule);

            var controller = GetController();

            var result = controller.Complete(1, 1) as ViewResult;

            Assert.IsTrue(string.IsNullOrEmpty(result.ViewName));
        }

        [TestMethod]
        [TestCategory("MedicationScheduleController.Complete")]
        public void MedicationScheduleController_Complete_ShouldReturnCorrectViewModel()
        {
            var medicationSchedule = GetMedicationSchedule();
            _medicationScheduleServiceMock.Setup(x => x.GetById(1)).Returns(medicationSchedule);

            var controller = GetController();

            var result = controller.Complete(1, 1) as ViewResult;

            Assert.IsNotNull(result.Model);
            Assert.IsInstanceOfType(result.Model, typeof(MedicationScheduleCompleteViewModel));
        }

        [TestMethod]
        [TestCategory("MedicationScheduleController.Complete")]
        public void MedicationScheduleController_Complete_ShouldReturnMappedOrganization()
        {
            var medicationSchedule = GetMedicationSchedule();
            _medicationScheduleServiceMock.Setup(x => x.GetById(1)).Returns(medicationSchedule);

            var controller = GetController();

            var result = controller.Complete(1, 1) as ViewResult;
            var model = result.Model as MedicationScheduleCompleteViewModel;

            ViewModelAssert.AssertOrganizationBaseViewModel(_validOrganization, model);
        }

        [TestMethod]
        [TestCategory("MedicationScheduleController.Complete")]
        public void MedicationScheduleController_Complete_ShouldReturnMedicationSchedule()
        {
            var medicationSchedule = GetMedicationSchedule();
            _medicationScheduleServiceMock.Setup(x => x.GetById(1)).Returns(medicationSchedule);
            var controller = GetController();

            var result = controller.Complete(1, 1) as ViewResult;
            var model = result.Model as MedicationScheduleCompleteViewModel;

            Assert.AreEqual(medicationSchedule, model.MedicationSchedule);
        }

        [TestMethod]
        [TestCategory("MedicationScheduleController.Complete")]
        public void MedicationScheduleController_Complete_ShouldReturnNullDescription()
        {
            var medicationSchedule = GetMedicationSchedule();
            _medicationScheduleServiceMock.Setup(x => x.GetById(1)).Returns(medicationSchedule);
            var controller = GetController();

            var result = controller.Complete(1, 1) as ViewResult;
            var model = result.Model as MedicationScheduleCompleteViewModel;

            Assert.AreEqual(null, model.Description);
        }
        
        [TestMethod]
        [TestCategory("MedicationScheduleController.Complete")]
        [ExpectedException(typeof(InvalidOperationException))]
        public void MedicationScheduleController_Complete_POST_Invalid_ShouldThrowAExceptionIfScheduleAlreadyIsCompleted()
        {
            var medicationSchedule = GetMedicationSchedule();
            medicationSchedule.Completed = Builder<MedicationScheduleComplete>.CreateNew().Build();
            _medicationScheduleServiceMock.Setup(x => x.GetById(1)).Returns(medicationSchedule);

            var controller = GetController(invalidModelState: true);
            controller.Complete(1, 1);
        }

        [TestMethod]
        [TestCategory("MedicationScheduleController.Complete")]
        public void MedicationScheduleController_Complete_POST_Invalid_ShouldReturnCorrectView()
        {
            var medicationSchedule = GetMedicationSchedule();
            _medicationScheduleServiceMock.Setup(x => x.GetById(1)).Returns(medicationSchedule);

            var controller = GetController(invalidModelState: true);

            var result = controller.Complete(1, 1, GetMedicationCompleteViewModel()) as ViewResult;

            Assert.IsTrue(string.IsNullOrEmpty(result.ViewName));
        }

        [TestMethod]
        [TestCategory("MedicationScheduleController.Complete")]
        public void MedicationScheduleController_Complete_POST_Invalid_ShouldReturnCorrectViewModel()
        {
            var medicationSchedule = GetMedicationSchedule();
            _medicationScheduleServiceMock.Setup(x => x.GetById(1)).Returns(medicationSchedule);

            var controller = GetController(invalidModelState: true);

            var result = controller.Complete(1, 1, GetMedicationCompleteViewModel()) as ViewResult;

            Assert.IsNotNull(result.Model);
            Assert.IsInstanceOfType(result.Model, typeof(MedicationScheduleCompleteViewModel));
        }

        [TestMethod]
        [TestCategory("MedicationScheduleController.Complete")]
        public void MedicationScheduleController_Complete_POST_Invalid_ShouldReturnMappedOrganization()
        {
            var medicationSchedule = GetMedicationSchedule();
            _medicationScheduleServiceMock.Setup(x => x.GetById(1)).Returns(medicationSchedule);
            
            var controller = GetController(invalidModelState: true);

            var result = controller.Complete(1, 1, GetMedicationCompleteViewModel()) as ViewResult;
            var model = result.Model as MedicationScheduleCompleteViewModel;

            ViewModelAssert.AssertOrganizationBaseViewModel(_validOrganization, model);
        }

        [TestMethod]
        [TestCategory("MedicationScheduleController.Complete")]
        public void MedicationScheduleController_Complete_POST_Invalid_ShouldReturnMedicationSchedule()
        {
            var medicationSchedule = GetMedicationSchedule();
            _medicationScheduleServiceMock.Setup(x => x.GetById(1)).Returns(medicationSchedule);
         
            var controller = GetController(invalidModelState: true);

            var result = controller.Complete(1, 1, GetMedicationCompleteViewModel()) as ViewResult;
            var model = result.Model as MedicationScheduleCompleteViewModel;

            Assert.AreEqual(medicationSchedule, model.MedicationSchedule);
        }

        [TestMethod]
        [TestCategory("MedicationScheduleController.Complete")]
        public void MedicationScheduleController_Complete_POST_Invalid_ShouldReturnPostedDescription()
        {
            var medicationSchedule = GetMedicationSchedule();
            _medicationScheduleServiceMock.Setup(x => x.GetById(1)).Returns(medicationSchedule);
  
            var controller = GetController(invalidModelState: true);

            var viewModel = GetMedicationCompleteViewModel();
            var result = controller.Complete(1, 1, viewModel) as ViewResult;
            var model = result.Model as MedicationScheduleCompleteViewModel;

            Assert.AreEqual(viewModel.Description, model.Description);
        }

        [TestMethod]
        [TestCategory("MedicationScheduleController.Complete")]
        [ExpectedException(typeof(InvalidOperationException))]
        public void MedicationScheduleController_Complete_ShouldThrowAExceptionIfScheduleAlreadyIsCompleted()
        {
            var medicationSchedule = GetMedicationSchedule();
            medicationSchedule.Completed = Builder<MedicationScheduleComplete>.CreateNew().Build();
            _medicationScheduleServiceMock.Setup(x => x.GetById(1)).Returns(medicationSchedule);

            var controller = GetController(invalidModelState: true);

            var viewModel = GetMedicationCompleteViewModel();
            
            controller.Complete(1, 1, viewModel);
        }

        [TestMethod]
        [TestCategory("MedicationScheduleController.Complete")]
        public void MedicationScheduleController_Complete_POST_ShouldCallService()
        {
            var medicationSchedule = GetMedicationSchedule();
            _medicationScheduleServiceMock.Setup(x => x.GetById(1)).Returns(medicationSchedule);

            var controller = GetController();

            controller.Complete(1, 1, GetMedicationCompleteViewModel());

            _medicationScheduleServiceMock.Verify(x => x.CompleteSchedule(It.IsAny<MedicationScheduleComplete>()), Times.Once);
        }

        [TestMethod]
        [TestCategory("MedicationScheduleController.Complete")]
        public void MedicationScheduleController_Complete_POST_ShouldRedirect()
        {
            var medicationSchedule = GetMedicationSchedule();
            _medicationScheduleServiceMock.Setup(x => x.GetById(1)).Returns(medicationSchedule);

            var controller = GetController();

            var result = controller.Complete(1, 1, GetMedicationCompleteViewModel()) as RedirectToRouteResult;

            Assert.IsTrue(result.RouteValues.ContainsKey("action"));
            Assert.IsTrue(result.RouteValues.ContainsKey("controller"));
            Assert.IsTrue(result.RouteValues.ContainsKey("area"));
            Assert.IsTrue(result.RouteValues.ContainsKey("organizationId"));
            Assert.IsTrue(result.RouteValues.ContainsKey("id"));
            Assert.AreEqual("Edit", result.RouteValues["action"]);
            Assert.AreEqual("MedicationSchedule", result.RouteValues["controller"]);
            Assert.AreEqual("Administration", result.RouteValues["area"]);
            Assert.AreEqual(1, result.RouteValues["organizationId"]);
            Assert.AreEqual(1, result.RouteValues["id"]);
        }

        private MedicationScheduleCompleteViewModel GetMedicationCompleteViewModel()
        {
            return Builder<MedicationScheduleCompleteViewModel>.CreateNew().Build();
        }

        private MedicationScheduleResultModel GetMedicationSchedule()
        {
            return Builder<MedicationScheduleResultModel>.CreateNew().Build();
        }

        private MedicationScheduleCreateViewModel GetCreateViewModel()
        {
            return Builder<MedicationScheduleCreateViewModel>.CreateNew()
                .With(x =>
                {
                    x.Items = Builder<MedicationScheduleCreateItemViewModel>.CreateListOfSize(4).Build();

                    return x;
                }).Build();
        }

        private MedicationScheduleController GetController(bool invalidModelState = false)
        {
            var controller = new MedicationScheduleController(_organizationServiceMock.Object, _medicationScheduleServiceMock.Object, _medicationServiceMock.Object, _patientServiceMock.Object);

            if (invalidModelState)
                controller.ModelState.AddModelError("test", "test");

            var userMock = new Mock<IIdentity>();
            userMock.Setup(x => x.Name).Returns("1");

            var controllerContextMock = new Mock<ControllerContext>();
            controllerContextMock.Setup(x => x.HttpContext.User).Returns(new ClaimsPrincipal(userMock.Object));

            controller.ControllerContext = controllerContextMock.Object;

            return controller;
        }
    }
}
