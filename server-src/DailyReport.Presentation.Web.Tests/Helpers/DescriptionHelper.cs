﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using FizzWare.NBuilder;

namespace DailyReport.Presentation.Web.Tests.Helpers
{
    public static class DescriptionHelper
    {
        public static IEnumerable<Description> GetValidListOfPatientDescriptions()
        {
            return Builder<Description>.CreateListOfSize(10).All().With(x => x).Build();
        }
    }
}
