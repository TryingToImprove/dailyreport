﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using FizzWare.NBuilder;

namespace DailyReport.Presentation.Web.Tests.Helpers
{
    public static class PatientPostHelper
    {
        public static IEnumerable<PatientPostResultModel> GetValidListOfPatientPosts()
        {
            return Builder<PatientPostResultModel>.CreateListOfSize(10).All().With(x =>
            {
                x.PatientPost = Builder<PatientPost>.CreateNew().With(y =>
                {
                    y.OrganizationId = 1;
                    return y;
                }).Build();

                x.Employee = Builder<Employee>.CreateNew().With(y =>
                {
                    y.OrganizationId = 1;
                    return y;
                }).Build();

                x.Patients = Builder<Patient>.CreateListOfSize(3).All().With(y =>
                {
                    y.Id = 3;
                    return y;
                }).Build();

                return x;
            }).Build();
        }
    }
}
