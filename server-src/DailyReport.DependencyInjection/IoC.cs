﻿using System.Data;
using System.Net;
using DailyReport.DataAccess;
using DailyReport.Domain.Infrastructure;
using DailyReport.Domain.Infrastructure.Dispensers;
using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Services;
using DailyReport.Services.Distributed;
using DailyReport.Services.Geocoding;
using DailyReport.Services.Security;
using DailyReport.Templating;
using MessageQueuer;
using Ninject;

namespace DailyReport.DependencyInjection
{
    public static class IoC
    {
        public static void RegisterServices(IKernel kernel)
        {
            // Repository profile
            kernel.Bind<IOrganizationRepository>().ToMethod(x => new OrganizationRepository(kernel.Get<IDbConnection>()));
            kernel.Bind<IEmployeeRepository>().ToMethod(x => new EmployeeRepository(kernel.Get<IDbConnection>()));
            kernel.Bind<IAuthenticationIdentityRepository>().ToMethod(x => new AuthenticationIdentityRepository(kernel.Get<IDbConnection>()));
            kernel.Bind<IPatientRepository>().ToMethod(x => new PatientRepository(kernel.Get<IDbConnection>()));
            kernel.Bind<IPatientPostRepository>().ToMethod(x => new PatientPostRepository(kernel.Get<IDbConnection>()));
            kernel.Bind<IPatientToPatientPostRepository>().ToMethod(x => new PatientToPatientPostRepository(kernel.Get<IDbConnection>()));
            kernel.Bind<IDescriptionRepository>().ToMethod(x => new DescriptionRepository(kernel.Get<IDbConnection>()));
            kernel.Bind<IAuthenticationLogRepository>().ToMethod(x => new AuthenticationLogRepository(kernel.Get<IDbConnection>()));
            kernel.Bind<IMedicationRepository>().ToMethod(x => new MedicationRepository(kernel.Get<IDbConnection>()));
            kernel.Bind<IMedicationBrandRepository>().ToMethod(x => new MedicationBrandRepository(kernel.Get<IDbConnection>()));
            kernel.Bind<IMedicationScheduleRepository>().ToMethod(x => new MedicationScheduleRepository(kernel.Get<IDbConnection>()));
            kernel.Bind<IMedicationTypeRepository>().ToMethod(x => new MedicationTypeRepository(kernel.Get<IDbConnection>()));
            kernel.Bind<IMedicationScheduleItemRepository>().ToMethod(x => new MedicationScheduleItemRepository(kernel.Get<IDbConnection>()));
            kernel.Bind<IMedicationScheduleItemHistoryRepository>().ToMethod(x => new MedicationScheduleItemHistoryRepository(kernel.Get<IDbConnection>()));
            kernel.Bind<IMedicationScheduleCompleteRepository>().ToMethod(x => new MedicationScheduleCompleteRepository(kernel.Get<IDbConnection>()));
            kernel.Bind<IMeasureRepository>().ToMethod(x => new MeasureRepository(kernel.Get<IDbConnection>()));
            kernel.Bind<IPatientContactPersonRelationshipRepository>().ToMethod(x => new PatientContactPersonRelationshipRepository(kernel.Get<IDbConnection>()));
            kernel.Bind<IContactInformationRepository>().ToMethod(x => new ContactInformationRepository(kernel.Get<IDbConnection>()));
            kernel.Bind<IContactInformationTypeRepository>().ToMethod(x => new ContactInformationTypeRepository(kernel.Get<IDbConnection>()));
            kernel.Bind<IContactRepository>().ToMethod(x => new ContactRepository(kernel.Get<IDbConnection>()));
            kernel.Bind<IContactGroupRepository>().ToMethod(x => new ContactGroupRepository(kernel.Get<IDbConnection>()));
            kernel.Bind<ITimelineRepository>().ToMethod(x => new TimelineRepository(kernel.Get<IDbConnection>()));
            kernel.Bind<IAddressRepository>().ToMethod(x => new AddressRepository(kernel.Get<IDbConnection>()));

            kernel.Bind<IDataContext>().ToMethod(x => new DataContext(kernel.Get<IDbConnection>())
            {
                Employees = kernel.Get<IEmployeeRepository>(),
                AuthenticationIdentites = kernel.Get<IAuthenticationIdentityRepository>(),
                Organizations = kernel.Get<IOrganizationRepository>(),
                Patients = kernel.Get<IPatientRepository>(),
                PatientPosts = kernel.Get<IPatientPostRepository>(),
                PatientPostToPatients = kernel.Get<IPatientToPatientPostRepository>(),
                Descriptions = kernel.Get<IDescriptionRepository>(),
                AuthenticationLogs = kernel.Get<IAuthenticationLogRepository>(),
                MedicationBrands = kernel.Get<IMedicationBrandRepository>(),
                Medications = kernel.Get<IMedicationRepository>(),
                MedicationSchedules = kernel.Get<IMedicationScheduleRepository>(),
                MedicationTypes = kernel.Get<IMedicationTypeRepository>(),
                MedicationScheduleItemHistories = kernel.Get<IMedicationScheduleItemHistoryRepository>(),
                MedicationScheduleItems = kernel.Get<IMedicationScheduleItemRepository>(),
                MedicationScheduleCompletes = kernel.Get<IMedicationScheduleCompleteRepository>(),
                Measures = kernel.Get<IMeasureRepository>(),
                PatientContactPersonRelationships = kernel.Get<IPatientContactPersonRelationshipRepository>(),
                ContactInformations = kernel.Get<IContactInformationRepository>(),
                ContactInformationTypes = kernel.Get<IContactInformationTypeRepository>(),
                Contacts = kernel.Get<IContactRepository>(),
                ContactGroups = kernel.Get<IContactGroupRepository>(),
                TimelineEntries = kernel.Get<ITimelineRepository>(),
                Addresses = kernel.Get<IAddressRepository>()
            });

            // Bind queue
            kernel.Bind<MqConfiguration>().ToMethod(x => new MqConfiguration()
            {
                Resolver = type => kernel.Get(type)
            });

            kernel.Bind<QueueService>().ToMethod(x => new QueueService(kernel.Get<MqConfiguration>()));

            // Templating
            kernel.Bind<ITemplateService>().ToMethod(x => new RazorTemplateService());

            // Contact information profile
            kernel.Bind<EmployeeContactInformationRelationshipService>().ToMethod(x => new EmployeeContactInformationRelationshipService(kernel.Get<IDataContext>()));
            kernel.Bind<PatientContactInformationRelationshipService>().ToMethod(x => new PatientContactInformationRelationshipService(kernel.Get<IDataContext>()));
            kernel.Bind<ContactContactInformationRelationshipService>().ToMethod(x => new ContactContactInformationRelationshipService(kernel.Get<IDataContext>()));

            // Contactbook profile
            kernel.Bind<PatientContactbookRelationshipService>().ToMethod(x => new PatientContactbookRelationshipService(kernel.Get<IDataContext>()));

            // Address profile
            kernel.Bind<PatientAddressRelationshipService>().ToMethod(x => new PatientAddressRelationshipService(kernel.Get<IDataContext>()));

            // Contact persons profile
            kernel.Bind<PatientContactPersonRelationshipService>().ToMethod(x => new PatientContactPersonRelationshipService(kernel.Get<IDataContext>(), kernel.Get<QueueService>()));

            // Authentication profile
            kernel.Bind<EmployeeAuthenticationRelationshipService>().ToMethod(x => new EmployeeAuthenticationRelationshipService(kernel.Get<IDataContext>(), kernel.Get<IHashService>()));

            // Medication schedule profile
            kernel.Bind<MedicationScheduleHistoryRelationshipService>().ToMethod(x => new MedicationScheduleHistoryRelationshipService(kernel.Get<IDataContext>()));

            // Dispenser profiler
            kernel.Bind<IMailDispenser>().ToMethod(x => new MailDispenser(kernel.Get<QueueService>()));

            // Geocoding profile
            kernel.Bind<ILocationProvider>().ToMethod(x => new GoogleLocationProvider());

            // Services profile
            kernel.Bind<IHashService>().To<HashService>();
            kernel.Bind<IAuthenticationService>().ToMethod(x => new AuthenticationService(kernel.Get<IDataContext>(), kernel.Get<IHashService>(), kernel.Get<EmployeeAuthenticationRelationshipService>()));
            kernel.Bind<IOrganizationService>().ToMethod(x => new OrganizationService(kernel.Get<IDataContext>()));
            kernel.Bind<IEmployeeService>().ToMethod(x => new EmployeeService(kernel.Get<IDataContext>(), kernel.Get<EmployeeContactInformationRelationshipService>(), kernel.Get<IMailDispenser>(), kernel.Get<IAuthenticationService>()));
            kernel.Bind<IPatientService>().ToMethod(x => new PatientService(kernel.Get<IDataContext>(), kernel.Get<PatientContactInformationRelationshipService>(), kernel.Get<PatientContactbookRelationshipService>(), kernel.Get<PatientContactPersonRelationshipService>(), kernel.Get<PatientAddressRelationshipService>()));
            kernel.Bind<IPatientPostService>().ToMethod(x => new PatientPostService(kernel.Get<IDataContext>()));
            kernel.Bind<IMedicationService>().ToMethod(x => new MedicationService(kernel.Get<IDataContext>()));
            kernel.Bind<IMedicationScheduleService>().ToMethod(x => new MedicationScheduleService(kernel.Get<IDataContext>(), kernel.Get<MedicationScheduleHistoryRelationshipService>()));
            kernel.Bind<IContactInformationService>().ToMethod(x => new ContactInformationService(kernel.Get<IDataContext>()));
            kernel.Bind<ITimelineService>().ToMethod(x => new TimelineService(kernel.Get<IDataContext>()));
            kernel.Bind<IContactbookService>().ToMethod(x => new ContactbookService(kernel.Get<IDataContext>(), kernel.Get<ContactContactInformationRelationshipService>()));
            kernel.Bind<IMailService>().ToMethod(x => new SendGridMailService(new NetworkCredential("DailyReport", "123123qwe")));
        }
    }
}
