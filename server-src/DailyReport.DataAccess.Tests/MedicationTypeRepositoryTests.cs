﻿using System;
using System.Linq;
using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Models.Core;
using Dapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DailyReport.DataAccess.Tests
{
    [TestClass]
    public class MedicationTypeRepositoryTests : BaseRepositoryTests
    {
        private IMedicationTypeRepository _medicationTypeRepository;

        protected override void Setup()
        {
            _medicationTypeRepository = new MedicationTypeRepository(Connection);
        }

        [TestMethod]
        [TestCategory("MedicationTypeRepository.FindAll")]
        public void MedicationTypeRepository_FindAll_IsAbleToFetch()
        {
            var medicationTypes = _medicationTypeRepository.FindAll();

            Assert.AreEqual(3, medicationTypes.Count());
        }
    }
}
