﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.DataAccess.Tests.Helpers
{
    public static class SqlHelper
    {
        public static DateTime GetDateTime(DateTime sqlDateTime)
        {
            return (DateTime)(SqlDateTime)sqlDateTime;
        }
    }
}
