﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DailyReport.DataAccess.Tests.Asserts
{
    public static class MedicationAssert
    {
        public static void AssertMedicationWithIdOne(MedicationResultModel medication)
        {
            Assert.IsNotNull(medication);
            Assert.AreEqual(1, medication.Id);
            Assert.AreEqual("Panodil", medication.Name);

            Assert.IsNotNull(medication.Brand);
            Assert.AreEqual(1, medication.Brand.Id);
            Assert.AreEqual("Novo Nordisk", medication.Brand.Name);

            Assert.AreEqual("Tabletter", medication.Type);
            Assert.AreEqual("mg", medication.Measure);
            Assert.AreEqual(2.5, medication.Weight);
        }
    }
}
