﻿using System;
using System.Linq;
using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Models.Core;
using Dapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DailyReport.DataAccess.Tests
{
    [TestClass]
    public class MeasureRepositoryTests : BaseRepositoryTests
    {
        private IMeasureRepository _measureRepository;

        protected override void Setup()
        {
            _measureRepository = new MeasureRepository(Connection);
        }

        [TestMethod]
        [TestCategory("MeasureRepository.FindAll")]
        public void MedicationTypeRepository_FindAll_IsAbleToFetch()
        {
            var measures = _measureRepository.FindAll();

            Assert.AreEqual(3, measures.Count());
        }
    }
}
