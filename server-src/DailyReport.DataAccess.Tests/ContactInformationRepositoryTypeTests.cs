﻿using System;
using System.Linq;
using DailyReport.DataAccess.Tests.Helpers;
using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using Dapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DailyReport.DataAccess.Tests
{
    [TestClass]
    public class ContactInformationRepositoryTypeTests : BaseRepositoryTests
    {
        private IContactInformationTypeRepository _contactInformationTypeRepository;

        protected override void Setup()
        {
            _contactInformationTypeRepository = new ContactInformationTypeRepository(Connection);
        }

        [TestMethod]
        [TestCategory("ContactInformation.FindAll")]
        public void FindAll_ShouldReturnAllContactInformations()
        {
            var contactInformations = _contactInformationTypeRepository.FindAll();

            Assert.AreEqual(3, contactInformations.Count());
        }
    }
}
