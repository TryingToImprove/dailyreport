﻿using System;
using System.Data.SqlClient;
using System.Linq;
using DailyReport.DataAccess.Tests.Asserts;
using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using Dapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.SqlTypes;

namespace DailyReport.DataAccess.Tests
{
    [TestClass]
    public class PatientContactPersonRelationshipRepositoryTests : BaseRepositoryTests
    {
        private IPatientContactPersonRelationshipRepository _patientContatPersonRelationshipRepository;

        protected override void Setup()
        {
            _patientContatPersonRelationshipRepository = new PatientContactPersonRelationshipRepository(Connection);
        }

        #region Insert
        [TestMethod]
        [TestCategory("PatientContactPersonRelationshipRepository.Insert")]
        public void Insert_ShouldBeAbleToInsert()
        {
            var model = new PatientContactPersonRelationship
            {
                PatientId = 2,
                EmployeeId = 1,
                TimeCreated = (DateTime)(SqlDateTime)DateTime.Now
            };

            var result = _patientContatPersonRelationshipRepository.Insert(model, false);

            Assert.IsTrue(result.Id > 0);
            Assert.AreEqual(model.PatientId, result.PatientId);
            Assert.IsNotNull(result.Employee);
            Assert.AreEqual(model.EmployeeId, result.Employee.Id);
            Assert.AreEqual(model.TimeCreated, result.TimeStarted);
            Assert.AreEqual(model.PatientId, result.PatientId);
        }

        [TestMethod]
        [TestCategory("PatientContactPersonRelationshipRepository.Insert")]
        public void Insert_GivenCompletePreviousRelationships_False_ShouldIncreaseTheNumberOfRelationships()
        {
            var model = new PatientContactPersonRelationship
            {
                PatientId = 2,
                EmployeeId = 1,
                TimeCreated = (DateTime)(SqlDateTime)DateTime.Now
            };

            var numberOfActiveRelationships = GetActiveRelationshipsByPatientCount(model.PatientId);

            _patientContatPersonRelationshipRepository.Insert(model, false);

            Assert.AreEqual(numberOfActiveRelationships + 1, GetActiveRelationshipsByPatientCount(model.PatientId));
        }

        [TestMethod]
        [TestCategory("PatientContactPersonRelationshipRepository.Insert")]
        public void Insert_GivenCompletePreviousRelationships_True_ShouldEndPreviousRelationships()
        {
            var model = new PatientContactPersonRelationship
            {
                PatientId = 2,
                EmployeeId = 1,
                TimeCreated = (DateTime)(SqlDateTime)DateTime.Now
            };

            _patientContatPersonRelationshipRepository.Insert(model, true);

            var numberOfActiveRelationships = GetActiveRelationshipsByPatientCount(model.PatientId);
            Assert.AreEqual(1, numberOfActiveRelationships);
        }

        [TestMethod]
        [TestCategory("PatientContactPersonRelationshipRepository.Insert")]
        public void Insert_GivenCompletePreviousRelationships_True_ShouldBeAbleToInsert()
        {
            var model = new PatientContactPersonRelationship
            {
                PatientId = 2,
                EmployeeId = 1,
                TimeCreated = (DateTime)(SqlDateTime)DateTime.Now
            };

            var result = _patientContatPersonRelationshipRepository.Insert(model, true);

            Assert.IsTrue(result.Id > 0);
            Assert.AreEqual(model.PatientId, result.PatientId);
            Assert.IsNotNull(result.Employee);
            Assert.AreEqual(model.EmployeeId, result.Employee.Id);
            Assert.AreEqual(model.TimeCreated, result.TimeStarted);
            Assert.AreEqual(null, result.TimeEnded);
            Assert.AreEqual(model.PatientId, result.PatientId);
        }
        
        [TestMethod]
        [TestCategory("PatientContactPersonRelationshipRepository.Insert")]
        [ExpectedException(typeof(SqlException), "It was possible to create a invalid reference to a patient")]
        public void Insert_GivenInvalidPatientId_ThrowsException()
        {
            var model = new PatientContactPersonRelationship
            {
                PatientId = 99999,
                EmployeeId = 1,
                TimeCreated = (DateTime)(SqlDateTime)DateTime.Now
            };

            _patientContatPersonRelationshipRepository.Insert(model, false);
        }

        [TestMethod]
        [TestCategory("PatientContactPersonRelationshipRepository.Insert")]
        [ExpectedException(typeof(SqlException), "It was possible to create a invalid reference to a employee")]
        public void Insert_GivenInvalidEmployeeId_ThrowsException()
        {
            var model = new PatientContactPersonRelationship
            {
                PatientId = 1,
                EmployeeId = 9999,
                TimeCreated = (DateTime)(SqlDateTime)DateTime.Now
            };

            _patientContatPersonRelationshipRepository.Insert(model, false);
        }
        #endregion

        private int GetActiveRelationshipsByPatientCount(int patientId)
        {
            const string sql = "SELECT COUNT(1) FROM PatientContactPersonRelationship " +
                               "WHERE NOT EXISTS (SELECT * FROM PatientContactPersonRelationshipEnded WHERE PatientContactPersonRelationshipId = Id) AND PatientId = @PatientId";

            return Connection.ExecuteScalar<int>(sql, new { PatientId = patientId });
        }

        [TestMethod]
        [TestCategory("PatientContactPersonRelationshipRepository.FindAll")]
        public void FindAll_ShouldBeAbleToFetch()
        {
            var results = _patientContatPersonRelationshipRepository.FindAll(1);

            Assert.AreEqual(1, results.Count());
        }
    }
}
