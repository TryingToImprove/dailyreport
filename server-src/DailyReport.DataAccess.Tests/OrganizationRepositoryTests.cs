﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Models.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.SqlClient;
using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.DataAccess.Tests;

namespace DailyReport.DataAccess.Tests
{
    [TestClass]
    public class OrganizationRepositoryTests : BaseRepositoryTests
    {
        private IOrganizationRepository _organizationRepository;

        protected override void Setup()
        {
            _organizationRepository = new OrganizationRepository(Connection);
        }

        [TestMethod]
        public void OrganizationRepository_CreateShouldSaveToDatabase()
        {
            var organization = _organizationRepository.Insert(new Organization
            {
                Name = "Olivers bosted",
                TimeCreated = DateTime.Now
            });

            Assert.IsNotNull(_organizationRepository.GetById(organization.Id));
        }

        [TestMethod]
        public void OrganizationRepostiory_GetByIdShouldNotReturnNull()
        {
            var organization = _organizationRepository.GetById(1);

            Assert.IsNotNull(organization);
            Assert.AreEqual("Olivers bosted", organization.Name);
            Assert.AreEqual(DateTime.Parse("8/16/2014 2:48:44 PM").Ticks, organization.TimeCreated.Ticks);
        }

        [TestMethod]
        public void OrganizationRepository_Update()
        {
            var organization = _organizationRepository.GetById(2);
            organization.Name = "Olivers Bosted UPDATED";
            organization.TimeCreated = DateTime.Parse("8/16/2010 10:48:00");

            _organizationRepository.Update(organization);

            var updatedOrganization = _organizationRepository.GetById(organization.Id);
            Assert.AreEqual(organization.Name, updatedOrganization.Name);
            Assert.AreEqual(organization.TimeCreated.Ticks, updatedOrganization.TimeCreated.Ticks);
            Assert.AreEqual(organization.Id, updatedOrganization.Id);
        }
    }
}
