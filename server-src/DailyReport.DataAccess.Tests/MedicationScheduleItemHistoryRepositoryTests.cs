﻿using System;
using System.Data.SqlTypes;
using System.Linq;
using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Models.Core;
using Dapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DailyReport.DataAccess.Tests
{
    [TestClass]
    public class MedicationScheduleItemHistoryRepositoryTests : BaseRepositoryTests
    {
        private IMedicationScheduleItemHistoryRepository _medicationScheduleItemHistoryRepository;

        protected override void Setup()
        {
            _medicationScheduleItemHistoryRepository = new MedicationScheduleItemHistoryRepository(Connection);
        }

        [TestMethod]
        [TestCategory("MedicationScheduleItemHistoryRepository.Insert")]
        public void MedicationScheduleItemHistoryRepository_Insert_ShouldBeAbleToInsert()
        {
            var medicationScheduleItemHistory = new MedicationScheduleItemHistory
            {
                TimeCreated = (DateTime)(SqlDateTime)DateTime.Now,
                MedicationScheduleItemId = 1
            };

            var result = _medicationScheduleItemHistoryRepository.Insert(medicationScheduleItemHistory, 1);

            Assert.IsTrue(result.Id > 0);
            Assert.AreEqual(medicationScheduleItemHistory.MedicationScheduleItemId, result.MedicationScheduleItemId);
            Assert.AreEqual(medicationScheduleItemHistory.TimeCreated.Ticks, result.TimeCreated.Ticks);
        }

        [TestMethod]
        [TestCategory("MedicationScheduleItemHistoryRepository.FindAll")]
        public void MedicationScheduleItemHistoryRepository_FindAll_ShouldBeAbleToFetchAll()
        {
            var result = _medicationScheduleItemHistoryRepository.FindAll();

            Assert.IsNotNull(result);
            Assert.AreNotEqual(0, result.Count());
        }

        [TestMethod]
        [TestCategory("MedicationScheduleItemHistoryRepository.FindAll")]
        public void MedicationScheduleItemHistoryRepository_FindAll_ShouldBeAbleToFetchAll_WithMedicationScheduleItemHistoryId()
        {
            var result = _medicationScheduleItemHistoryRepository.FindAll(1);

            Assert.IsNotNull(result);
            Assert.AreNotEqual(0, result.Count());
            Assert.IsTrue(result.All(x => x.MedicationScheduleItemId == 1));
        }
    }
}
