﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Tests.Helpers;
using Dapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DailyReport.DataAccess.Tests
{
    public abstract class BaseRepositoryTests
    {
        protected IDbConnection Connection;

        protected abstract void Setup();

        [TestInitialize]
        public void Initialize()
        {
            Connection = TestDatabase.Create();

            Setup();
        }

        [TestCleanup]
        public void Teardown()
        {
            Connection.Close();
            Connection.Dispose();
            Connection = null;
        }
    }
}
