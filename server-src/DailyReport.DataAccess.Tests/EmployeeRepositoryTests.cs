﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Models.Core;
using Dapper;
using FizzWare.NBuilder;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.SqlClient;
using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.DataAccess.Tests;

namespace DailyReport.DataAccess.Tests
{
    [TestClass]
    public class EmployeeRepositoryTests : BaseRepositoryTests
    {
        private IEmployeeRepository _employeeRepository;

        protected override void Setup()
        {
            _employeeRepository = new EmployeeRepository(Connection);
        }

        [TestMethod]
        public void EmployeeRepository_IsAbleToInsertToTheDatabase()
        {
            var authenticationIdentityId = Connection.ExecuteScalar<int>("INSERT INTO AuthenticationIdentity (Username, PasswordHash, IsActive) VALUES (@Username, @PasswordHash, 1);" +
                                                                            "SELECT SCOPE_IDENTITY()", Builder<AuthenticationIdentity>.CreateNew().Build());

            var employee = new Employee
            {
                Firstname = "Sarah",
                Lastname = "Lassen",
                OrganizationId = 2,
                TimeCreated = DateTime.Parse("8/16/2014 3:29:13 PM"),
                AuthenticationId = authenticationIdentityId
            };

            employee = _employeeRepository.Insert(employee);

            var createdEmployee = _employeeRepository.GetById(employee.Id);

            Assert.IsNotNull(createdEmployee);
            Assert.AreEqual(employee.Firstname, createdEmployee.Firstname);
            Assert.AreEqual(employee.Lastname, createdEmployee.Lastname);
            Assert.AreEqual(employee.OrganizationId, createdEmployee.OrganizationId);
            Assert.AreEqual(employee.TimeCreated.Ticks, createdEmployee.TimeCreated.Ticks);
        }

        [TestMethod]
        public void EmployeeRepository_GetByIdShouldNotReturnNull()
        {
            var employee = _employeeRepository.GetById(1);

            Assert.IsNotNull(employee);
            Assert.AreEqual("Oliver", employee.Firstname);
            Assert.AreEqual("Lassen", employee.Lastname);
            Assert.AreEqual(1, employee.OrganizationId);
            Assert.AreEqual(DateTime.Parse("8/16/2014 2:48:44 PM").Ticks, employee.TimeCreated.Ticks);
        }

        [TestMethod]
        public void EmployeeRepository_Update()
        {
            var employee = _employeeRepository.GetById(3);
            employee.Firstname = "Oliver UPDATED";
            employee.Lastname = "Lassen UPDATED";
            employee.OrganizationId = 2;
            employee.TimeCreated = DateTime.Parse("8/16/2010 10:48:00 AM");

            var isActive = employee.IsActive;
            employee.IsActive = !isActive;

            _employeeRepository.Update(employee);

            var updatedEmployee = _employeeRepository.GetById(employee.Id);

            Assert.AreEqual(employee.Id, updatedEmployee.Id);
            Assert.AreEqual(employee.Firstname, updatedEmployee.Firstname);
            Assert.AreEqual(employee.Lastname, updatedEmployee.Lastname);
            Assert.AreEqual(employee.OrganizationId, updatedEmployee.OrganizationId);
            Assert.AreEqual(employee.TimeCreated.Ticks, updatedEmployee.TimeCreated.Ticks);
            Assert.AreEqual(isActive, updatedEmployee.IsActive); // Should not change the active state
        }

        [TestMethod]
        public void EmployeeRepository_UpdateActiveState_IsAbleToChange()
        {
            var employee = _employeeRepository.GetById(5);
            employee.IsActive = !employee.IsActive;

            _employeeRepository.UpdateActiveState(employee.Id, employee.IsActive);

            var updatedEmployee = _employeeRepository.GetById(5);
            Assert.AreEqual(employee.IsActive, updatedEmployee.IsActive);
        }

        [TestMethod]
        public void EmployeeRepository_FindAll_ShouldReturnEmployeesFromOrganizationThatAreActive()
        {
            var employees = _employeeRepository.FindAll(1);

            Assert.AreEqual(3, employees.Count());

            foreach (var employee in employees)
            {
                Assert.IsTrue(employee.IsActive);
            }
        }
    }
}
