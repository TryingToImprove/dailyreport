﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using DailyReport.DataAccess.Tests.Asserts;
using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using Dapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DailyReport.DataAccess.Tests
{
    [TestClass]
    public class MedicationScheduleRepositoryTests : BaseRepositoryTests
    {
        private IMedicationScheduleRepository _medicationScheduleRepository;
        protected override void Setup()
        {
            _medicationScheduleRepository = new MedicationScheduleRepository(Connection);
        }

        [TestMethod]
        [TestCategory("MedicationScheduleRepository.GetById")]
        public void MedicationScheduleRepository_GetById_IsAbleToFetchAndMap()
        {
            var expectedResult = new MedicationScheduleResultModel
            {
                Id = 2,
                Patient = new Patient()
                {
                    Id = 1,
                    Firstname = "Oliver",
                    Lastname = "Lassen"
                },
                StartDate = (DateTime)SqlDateTime.Parse("8/16/2014 2:48:44 PM"),
                TimeCreated = (DateTime)SqlDateTime.Parse("8/16/2014 2:48:44 PM"),
                Schedules = new List<MedicationScheduleItemResultModel>()
                {
                    new MedicationScheduleItemResultModel()
                    {
                        Id = 1,
                        RepeatTimeSpan = 1409771469
                    }
                },
                Completed = new MedicationScheduleComplete
                {
                    Description = "testString",
                    EmployeeId = 1
                }
            };

            var actualResult = _medicationScheduleRepository.GetById(expectedResult.Id);

            Assert.AreEqual(expectedResult.Id, actualResult.Id);
            Assert.AreEqual(expectedResult.Patient.Firstname, actualResult.Patient.Firstname);
            Assert.AreEqual(expectedResult.Patient.Lastname, actualResult.Patient.Lastname);
            Assert.AreEqual(expectedResult.Patient.Id, actualResult.Patient.Id);
            Assert.AreEqual(expectedResult.Schedules.Count(), actualResult.Schedules.Count());

            var schedule = actualResult.Schedules.SingleOrDefault(x => x.Id == 5);
            Assert.IsNotNull(schedule);
            Assert.AreEqual(5, schedule.Id);
            MedicationAssert.AssertMedicationWithIdOne(schedule.Medication);

            Assert.IsTrue(actualResult.IsCompleted);
            Assert.IsNotNull(actualResult.Completed);
        }


        [TestMethod]
        [TestCategory("MedicationScheduleRepository.FindAll")]
        public void MedicationScheduleRepository_FindAll_IsAbleToFetch_WhereNoneIsCompleted()
        {
            var medicationSchedules = _medicationScheduleRepository.FindAll();
            
            foreach (var medicationSchedule in medicationSchedules)
            {
                Assert.IsFalse(medicationSchedule.IsCompleted, "Medication schedule should not be completed");
                Assert.IsTrue(medicationSchedule.Patient.IsActive, "The patient is not active");
            }
        }
    }
}
