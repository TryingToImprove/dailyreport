﻿using System;
using System.Data.SqlClient;
using System.Linq;
using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Models.Core;
using Dapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DailyReport.DataAccess.Tests
{
    [TestClass]
    public class PatientToPatientPostRepositoryTests : BaseRepositoryTests
    {
        private IPatientToPatientPostRepository _patientToPatientPostRepository;

        protected override void Setup()
        {
            _patientToPatientPostRepository = new PatientToPatientPostRepository(Connection);
        }

        [TestMethod]
        [TestCategory("PatientToPatientPostRepository.Contains")]
        public void PatientToPatientPostRepositoryTests_Contains_ShouldReturnTrueIfThereIsAReference()
        {
            var reference = new PatientPostReference
            {
                PatientId = 1,
                PatientPostId = 2
            };

            var isReferenced = _patientToPatientPostRepository.Contains(reference.PatientId, reference.PatientPostId);
            Assert.IsTrue(isReferenced);
        }

        [TestMethod]
        [TestCategory("PatientToPatientPostRepository.Contains")]
        public void PatientToPatientPostRepositoryTests_Contains_ShouldReturnFalseIfThereIsNotAReference()
        {
            var reference = new PatientPostReference
            {
                PatientId = -1,
                PatientPostId = -1
            };

            var isReferenced = _patientToPatientPostRepository.Contains(reference.PatientId, reference.PatientPostId);

            Assert.IsFalse(isReferenced);
        }

        [TestMethod]
        [TestCategory("PatientToPatientPostRepository.Remove")]
        public void PatientToPatientPostRepositoryTests_Remove_ShouldBeAbleToRemoveAReference()
        {
            var reference = new PatientPostReference
            {
                PatientId = 2,
                PatientPostId = 3
            };

            // Arrange
            reference = Connection.Query<PatientPostReference>("SELECT * FROM PatientPost_Patient WHERE PatientId = @PatientId AND PatientPostId = @PatientPostId", reference).FirstOrDefault();
            Assert.IsNotNull(reference, "The test expected a reference on the database to verify removeal");

            // Act
            _patientToPatientPostRepository.Remove(reference.PatientId, reference.PatientPostId);
            
            // Assert
            reference = Connection.Query<PatientPostReference>("SELECT * FROM PatientPost_Patient WHERE PatientId = @PatientId AND PatientPostId = @PatientPostId", reference).FirstOrDefault(); 
            Assert.IsNull(reference);
        }

        [TestMethod]
        [TestCategory("PatientToPatientPostRepository.Insert")]
        public void PatientToPatientPostRepositoryTests_Insert_IsAbleToInsert()
        {
            var referenceToCreate = new PatientPostReference
            {
                PatientId = 1,
                PatientPostId = 1
            };

            // Arrange
            var reference = Connection.Query<PatientPostReference>("SELECT * FROM PatientPost_Patient WHERE PatientId = @PatientId AND PatientPostId = @PatientPostId", referenceToCreate).FirstOrDefault();
            Assert.IsNull(reference, "The test expected that there is no reference on the database to verify creation");

            _patientToPatientPostRepository.Insert(referenceToCreate.PatientId, referenceToCreate.PatientPostId);

            reference = Connection.Query<PatientPostReference>("SELECT * FROM PatientPost_Patient WHERE PatientId = @PatientId AND PatientPostId = @PatientPostId", referenceToCreate).FirstOrDefault();
            Assert.IsNotNull(reference);
        }

        [TestMethod]
        [TestCategory("PatientToPatientPostRepository.Insert")]
        [ExpectedException(typeof(SqlException))]
        public void PatientToPatientPostRepositoryTests_Insert_IsNotAbleToCreate_WhenDuplicatingReference()
        {
            var referenceToCreate = new PatientPostReference
            {
                PatientId = 1,
                PatientPostId = 1
            };

            // Arrange
            var reference = Connection.Query<PatientPostReference>("SELECT * FROM PatientPost_Patient WHERE PatientId = @PatientId AND PatientPostId = @PatientPostId", referenceToCreate).FirstOrDefault();
            Assert.IsNull(reference, "The test expected that there is no reference on the database to verify creation");

            _patientToPatientPostRepository.Insert(referenceToCreate.PatientId, referenceToCreate.PatientPostId);
            _patientToPatientPostRepository.Insert(referenceToCreate.PatientId, referenceToCreate.PatientPostId);
        }
    }
}
