﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DailyReport.Domain.Infrastructure.Repositories;
using System.Linq;
using DailyReport.Domain.Models.Core;
using FizzWare.NBuilder;

namespace DailyReport.DataAccess.Tests
{
    [TestClass]
    public class PatientRepositoryTests : BaseRepositoryTests
    {
        private IPatientRepository _patientRepository;

        protected override void Setup()
        {
            _patientRepository = new PatientRepository(Connection);
        }

        [TestMethod]
        public void PatientRepository_FindAll_ShouldFetchByOrganizationId()
        {
            var patients = _patientRepository.FindAll(1);

            Assert.AreEqual(1, patients.Count());

            foreach (var patient in patients)
            {
                Assert.IsTrue(patient.IsActive);
            }
        }

        [TestMethod]
        public void PatientRepository_Insert_IsAbleToInsert()
        {
            var patientToInsert = Builder<Patient>.CreateNew().Build();

            var patientInserted = _patientRepository.Insert(patientToInsert);

            Assert.AreEqual(patientToInsert.Id, patientInserted.Id);
            Assert.AreEqual(patientToInsert.Firstname, patientInserted.Firstname);
            Assert.AreEqual(patientToInsert.Lastname, patientInserted.Lastname);
            Assert.AreEqual(patientToInsert.TimeCreated, patientInserted.TimeCreated);
            Assert.AreEqual(patientToInsert.BirthDate, patientInserted.BirthDate);
            Assert.AreEqual(patientToInsert.OrganizationId, patientInserted.OrganizationId);
        }

        [TestMethod]
        [TestCategory("PatientRepository.GetById")]
        public void PatientRepository_GetById_IsAbleToGetPatientById()
        {
            var patientId = 3;

            var patient = _patientRepository.GetById(patientId);

            Assert.AreEqual("Firstname", patient.Firstname);
            Assert.AreEqual("Lastname", patient.Lastname);
            Assert.AreEqual(DateTime.Parse("06-19-1993").Ticks, patient.BirthDate.Ticks);
            Assert.AreEqual(4, patient.OrganizationId);
        }

        [TestMethod]
        [TestCategory("PatientRepository.Deactivate")]
        public void PatientRepository_Deactivate_IsAbleToDeactivate()
        {
            var patientId = 4;

            var patientBeforeDeactivation = _patientRepository.GetById(patientId);

            _patientRepository.Deactivate(patientBeforeDeactivation.Id);

            var patientAfterDeactivation = _patientRepository.GetById(patientId);

            Assert.AreNotEqual(patientBeforeDeactivation.IsActive, patientAfterDeactivation.IsActive);
            Assert.IsFalse(patientAfterDeactivation.IsActive);
        }
    }
}
