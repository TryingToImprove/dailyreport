﻿using System;
using System.Linq;
using DailyReport.DataAccess.Tests.Helpers;
using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using Dapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DailyReport.DataAccess.Tests
{
    [TestClass]
    public class ContactInformationRepositoryTests : BaseRepositoryTests
    {
        private IContactInformationRepository _contactInformationRepository;

        protected override void Setup()
        {
            _contactInformationRepository = new ContactInformationRepository(Connection);
        }

        [TestMethod]
        [TestCategory("ContactInformation.Insert")]
        public void Insert_IsAbleToInsert()
        {
            var model = new ContactInformation
            {
                ContactInformationTypeId = 2,
                Data = "test@test",
                TimeCreated = SqlHelper.GetDateTime(DateTime.Now)
            };

            var resultModel = _contactInformationRepository.Insert(model);

            Assert.IsNotNull(resultModel);
            Assert.IsTrue(resultModel.Id > 0);
            Assert.AreEqual("E-mail", resultModel.Type);
            Assert.AreEqual(model.Data, resultModel.Data);
            Assert.AreEqual(model.TimeCreated, resultModel.TimeCreated);
        }

        [TestMethod]
        [TestCategory("ContactInformation.RegisterPatientRelationship")]
        public void RegisterPatientRelationship_IsAbleToInsert()
        {
            // This test is just expected to not throw any exceptions (TODO: Make this better ;) )
            _contactInformationRepository.RegisterPatientRelationship(3, 1);
        }

        [TestMethod]
        [TestCategory("ContactInformation.RegisterEmployeeRelationship")]
        public void RegisterEmployeeRelationship_IsAbleToInsert()
        {
            // This test is just expected to not throw any exceptions (TODO: Make this better ;) )
            _contactInformationRepository.RegisterEmployeeRelationship(2, 1);
        }

        [TestMethod]
        [TestCategory("ContactInformation.FindAll")]
        public void FindAll_ShouldReturnAllContactInformations()
        {
            var contactInformations = _contactInformationRepository.FindAll();

            Assert.AreEqual(3, contactInformations.Count());
        }
        
        [TestMethod]
        [TestCategory("ContactInformation.FindPatientContactInformations")]
        public void FindPatientContactInformations_ShouldReturnAllContactInformations()
        {
            var contactInformations = _contactInformationRepository.FindPatientContactInformations(1);

            Assert.AreEqual(2, contactInformations.Count());
        }

        [TestMethod]
        [TestCategory("ContactInformation.FindEmployeeContactInformations")]
        public void FindEmployeeContactInformations_ShouldReturnAllContactInformations()
        {
            var contactInformations = _contactInformationRepository.FindEmployeeContactInformations(1);

            Assert.AreEqual(1, contactInformations.Count());
        }

        [TestMethod]
        [TestCategory("ContactInformation.FindAll")]
        public void FindAll_ShouldMapCorrectly()
        {
            var contactInformations = _contactInformationRepository.FindAll();
            var contactInformation = contactInformations.SingleOrDefault(x => x.Id == 1);

            AssertContactInformation(contactInformation);
        }

        [TestMethod]
        [TestCategory("ContactInformation.FindAll")]
        public void GetById_ShouldMapCorrectly()
        {
            var contactInformation = _contactInformationRepository.GetById(1);

            // Assert
            AssertContactInformation(contactInformation);
        }

        private static void AssertContactInformation(ContactInformationResultModel contactInformation)
        {
            Assert.IsNotNull(contactInformation);
            Assert.AreEqual(1, contactInformation.Id);
            Assert.AreEqual("Mobil", contactInformation.Type);
            Assert.AreEqual("+45 27150869", contactInformation.Data);
            Assert.AreEqual(1, contactInformation.TypeId);
        }
    }
}
