﻿using System;
using System.Data.SqlTypes;
using System.Linq;
using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Models.Core;
using Dapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DailyReport.DataAccess.Tests
{
    [TestClass]
    public class MedicationScheduleCompleteRepositoryTests : BaseRepositoryTests
    {
        private IMedicationScheduleCompleteRepository _medicationScheduleCompleteRepository;

        protected override void Setup()
        {
            _medicationScheduleCompleteRepository = new MedicationScheduleCompleteRepository(Connection);
        }

        [TestMethod]
        [TestCategory("MedicationScheduleCompleteRepository.Insert")]
        public void MedicationScheduleCompleteRepository_Insert_ShouldBeAbleToInsert()
        {
            var model = new MedicationScheduleComplete
            {
                Description = "Test",
                EmployeeId = 1,
                MedicationScheduleId = 1,
                TimeCreated = (DateTime)(SqlDateTime)DateTime.Now
            };

            var result = _medicationScheduleCompleteRepository.Insert(model);

            Assert.IsNotNull(result);
            Assert.AreEqual(model.MedicationScheduleId, result.MedicationScheduleId);
            Assert.AreEqual(model.Description, result.Description);
            Assert.AreEqual(model.EmployeeId, result.EmployeeId);
            Assert.AreEqual(model.TimeCreated.Ticks, result.TimeCreated.Ticks);
        }
    }
}
