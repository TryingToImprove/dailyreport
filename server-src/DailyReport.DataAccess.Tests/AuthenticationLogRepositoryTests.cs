﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Models.Core;
using Dapper;
using FizzWare.NBuilder;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DailyReport.DataAccess.Tests
{
    [TestClass]
    public class AuthenticationLogRepositoryTests : BaseRepositoryTests
    {
        private IAuthenticationLogRepository _authenticationLogRepository;

        protected override void Setup()
        {
            _authenticationLogRepository = new AuthenticationLogRepository(Connection);
        }

        [TestMethod]
        public void AuthenticationLogRepository_Insert_CanInsertLog()
        {
            var numberOfAuthenticationLogs = Connection.ExecuteScalar<int>("SELECT COUNT(*) FROM AuthenticationLog");

            _authenticationLogRepository.Insert(1, DateTime.Now);

            Assert.AreEqual(numberOfAuthenticationLogs + 1, Connection.ExecuteScalar<int>("SELECT COUNT(*) FROM AuthenticationLog"));
        }
    }
}
