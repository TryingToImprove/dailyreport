﻿using System;
using System.Linq;
using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Models.Core;
using Dapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DailyReport.DataAccess.Tests
{
    [TestClass]
    public class MedicationBrandRepositoryTests : BaseRepositoryTests
    {
        private IMedicationBrandRepository _medicationBrandRepository;

        protected override void Setup()
        {
            _medicationBrandRepository = new MedicationBrandRepository(Connection);
        }

        [TestMethod]
        [TestCategory("MedicationBrandRepository.Insert")]
        public void MedicationBrandRepository_Insert_ShouldBeAbleToInsert()
        {
            var medicationBrand = new MedicationBrand
            {
                Name = "JegEerEnTest.. Der er ingen der skal hedde dette!"
            };

            Assert.IsFalse(Connection.ExecuteScalar<bool>("SELECT (CASE WHEN EXISTS (SELECT 1 FROM MedicationBrand WHERE Name = @Name) THEN 1 ELSE 0 END)", medicationBrand));

            var result = _medicationBrandRepository.Insert(medicationBrand);

            Assert.IsTrue(Connection.ExecuteScalar<bool>("SELECT (CASE WHEN EXISTS (SELECT 1 FROM MedicationBrand WHERE Name = @Name) THEN 1 ELSE 0 END)", medicationBrand));
            Assert.IsTrue(result.Id > 0);
            Assert.AreEqual(medicationBrand.Name, result.Name);
        }

        [TestMethod]
        [TestCategory("MedicationBrandRepository.GetById")]
        public void MedicationBrandRepository_GetById_IsAbleToGetById()
        {
            var medicationBrand = _medicationBrandRepository.GetById(1);

            Assert.AreEqual(1, medicationBrand.Id);
            Assert.AreEqual("Novo Nordisk", medicationBrand.Name);
        }

        [TestMethod]
        [TestCategory("MedicationBrandRepository.FindAll")]
        public void MedicationBrandRepository_FindAll_IsAbleToFetch()
        {
            var medicationBrands = _medicationBrandRepository.FindAll();

            Assert.AreEqual(1, medicationBrands.Count());
        }
    }
}
