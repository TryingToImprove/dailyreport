﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.DataAccess.Tests.Helpers;
using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Models.Core;
using Dapper;
using FizzWare.NBuilder;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DailyReport.DataAccess.Tests
{
    [TestClass]
    public class DescriptionRepositoryTests : BaseRepositoryTests
    {
        private IDescriptionRepository _descriptionRepository;

        protected override void Setup()
        {
            _descriptionRepository = new DescriptionRepository(Connection);
        }

        [TestMethod]
        public void DescriptionRepository_InsertPatientDescription_CanInsertDescription()
        {
            var description = GetDescription();
            _descriptionRepository.InsertPatientDescription(1, GetDescription());

            var descriptionResult = Connection.Query<Description>("SELECT * FROM Description ORDER BY Id DESC").FirstOrDefault();
            Assert.IsNotNull(descriptionResult);
            Assert.AreEqual(description.EmployeeId, descriptionResult.EmployeeId);
            Assert.AreEqual(description.Text, descriptionResult.Text);
            Assert.AreEqual(description.TimeCreated.Ticks, descriptionResult.TimeCreated.Ticks);
        }

        [TestMethod]
        public void DescriptionRepository_InsertPatientDescription_CanInsertPatientDescription()
        {
            _descriptionRepository.InsertPatientDescription(1, GetDescription());

            var descriptionResult = Connection.Query<Description>("SELECT * FROM Description ORDER BY Id DESC").FirstOrDefault();
            var patientDescriptionResult = Connection.Query<PatientDescription>("SELECT * FROM PatientDescription WHERE PatientId = @PatientId AND DescriptionId = @DescriptionId", new PatientDescription
                    {
                        PatientId = 1,
                        DescriptionId = descriptionResult.Id
                    });

            Assert.IsNotNull(patientDescriptionResult);
        }

        [TestMethod]
        public void DescriptionRepository_ShouldBeAbleToFetchDescriptions()
        {
            var patientDescriptions = Connection.Query<PatientDescription>(
               "SELECT * FROM PatientDescription WHERE PatientId = @PatientId", new
               {
                   PatientId = 1
               });

            var descriptions = _descriptionRepository.GetPatientDescriptions(1);

            Assert.AreEqual(patientDescriptions.Count(), descriptions.Count());
        }

        private static Description GetDescription()
        {
            var description = new Description()
            {
                EmployeeId = 1,
                Text = "Test string",
                TimeCreated = SqlHelper.GetDateTime(DateTime.Now)
            };
            return description;
        }
    }
}
