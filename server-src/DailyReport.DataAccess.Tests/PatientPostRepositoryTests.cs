﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DailyReport.Domain.Infrastructure.Repositories;
using System.Linq;
using DailyReport.Domain.Models.Core;
using FizzWare.NBuilder;
using System.Data.SqlClient;

namespace DailyReport.DataAccess.Tests
{
    [TestClass]
    public class PatientPostRepositoryTests : BaseRepositoryTests
    {
        private IPatientPostRepository _patientPostRepository;

        protected override void Setup()
        {
            _patientPostRepository = new PatientPostRepository(Connection);
        }

        [TestMethod]
        [TestCategory("PatientPostRepository.FindAll")]
        public void PatientPostRepository_FindAll_ShouldBeAbleToFetchPatientPostsByOrganizationId()
        {
            var results = _patientPostRepository.FindAll(2);

            Assert.AreEqual(4, results.Count());

            foreach (var result in results)
            {
                Assert.IsNotNull(result.Employee);
                Assert.IsNotNull(result.PatientPost);
                Assert.IsNotNull(result.Patients);
            }
        }

        [TestMethod]
        [TestCategory("PatientPostRepository.FindAllByPatient")]
        public void PatientPostRepository_FindAllByPatient_ShouldBeAbleToFetchPatientPostsForAPatient()
        {
            var results = _patientPostRepository.FindAllByPatient(3);

            Assert.AreEqual(3, results.Count());

            foreach (var result in results)
            {
                Assert.IsNotNull(result.Employee);
                Assert.IsNotNull(result.PatientPost);
                Assert.IsNotNull(result.Patients);
            }
        }

        [TestMethod]
        [TestCategory("PatientPostRepository.Insert")]
        public void PatientPostRepository_Insert_IsAbleToInsert()
        {
            var patientPostToInsert = Builder<PatientPost>.CreateNew()
                .With(x =>
                {
                    x.Id = default(int);
                    x.EmployeeId = 1;
                    x.OrganizationId = 1;
                    return x;
                })
                .Build();

            var patientPostInserted = _patientPostRepository.Insert(patientPostToInsert);

            Assert.IsTrue(patientPostInserted.Id > 0);
            Assert.AreEqual(patientPostToInsert.Id, patientPostInserted.Id);
            Assert.AreEqual(patientPostInserted.OrganizationId, patientPostInserted.OrganizationId);
            Assert.AreEqual(patientPostToInsert.EmployeeId, patientPostInserted.EmployeeId);
            Assert.AreEqual(patientPostToInsert.TimeCreated, patientPostInserted.TimeCreated);
            Assert.AreEqual(patientPostToInsert.Description, patientPostInserted.Description);
        }

        [TestMethod]
        [TestCategory("PatientPostRepository.Insert")]
        [ExpectedException(typeof(SqlException))]
        public void PatientPostRepository_Insert_ShouldThrowASqlExceptionIfEmployeeNotExists()
        {
            var patientPostToInsert = Builder<PatientPost>.CreateNew()
                .With(x =>
                {
                    x.EmployeeId = 99999;
                    return x;
                })
                .Build();

            _patientPostRepository.Insert(patientPostToInsert);
        }
    }
}
