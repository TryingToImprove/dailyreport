﻿using System;
using System.Linq;
using DailyReport.DataAccess.Tests.Asserts;
using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using Dapper;
using FizzWare.NBuilder.Generators;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DailyReport.DataAccess.Tests
{
    [TestClass]
    public class MedicationRepositoryTests : BaseRepositoryTests
    {
        private IMedicationRepository _medicationRepository;

        protected override void Setup()
        {
            _medicationRepository = new MedicationRepository(Connection);
        }

        [TestMethod]
        [TestCategory("MedicationRepository.Insert")]
        public void MedicationRepository_Insert_ShouldBeAbleToInsert()
        {
            var medication = new Medication
            {
                Name = GetRandom.String(30),
                MedicationBrandId = 1,
                MedicationTypeId = 1,
                MeasureId = 1,
                Weight = 500
            };

            var result = _medicationRepository.Insert(medication);

            Assert.IsTrue(Connection.ExecuteScalar<bool>("SELECT (CASE WHEN EXISTS (SELECT 1 FROM Medication WHERE Name = @Name AND MedicationBrandId = @MedicationBrandId) THEN 1 ELSE 0 END)", medication));
            Assert.IsTrue(result.Id > 0);
            Assert.AreEqual(medication.MedicationBrandId, result.Brand.Id);
            Assert.AreEqual(medication.Name, result.Name);
            Assert.AreEqual(medication.MedicationTypeId, 1);
            Assert.AreEqual(medication.MeasureId, 1);
            Assert.AreEqual(medication.Weight, 500);
        }

        [TestMethod]
        [TestCategory("MedicationRepository.GetById")]
        public void MedicationRepository_GetById_IsAbleToGetById()
        {
            var medication = _medicationRepository.GetById(1);

            MedicationAssert.AssertMedicationWithIdOne(medication);
        }

        [TestMethod]
        [TestCategory("MedicationRepository.FindAll")]
        public void MedicationRepository_FindAll_IsAbleToFetch()
        {
            var medications = _medicationRepository.FindAll();

            Assert.AreEqual(4, medications.Count());

            var medication = medications.SingleOrDefault(x => x.Id == 1);
            MedicationAssert.AssertMedicationWithIdOne(medication);
        }
    }
}
