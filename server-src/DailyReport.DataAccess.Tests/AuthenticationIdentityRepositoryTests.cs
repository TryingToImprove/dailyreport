﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Models.Core;
using Dapper;
using FizzWare.NBuilder;
using FizzWare.NBuilder.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DailyReport.DataAccess.Tests
{
    [TestClass]
    public class AuthenticationIdentityRepositoryTests : BaseRepositoryTests
    {
        private IAuthenticationIdentityRepository _authenticationIdentityRepository;

        protected override void Setup()
        {
            _authenticationIdentityRepository = new AuthenticationIdentityRepository(Connection);
        }

        [TestMethod]
        public void AuthenticationIdentityRepository_Insert_CanInsertAuthenticationIdentity()
        {
            var identity = new AuthenticationIdentity
            {
                Username = "ola",
                PasswordHash = "123123qwe!",
                IsActive = true
            };

            var result = _authenticationIdentityRepository.Insert(identity);

            Assert.IsTrue(result.Id > 0);
            Assert.AreEqual(identity.Username, result.Username);
            Assert.AreEqual(identity.PasswordHash, result.PasswordHash);
            Assert.IsTrue(result.IsActive);
        }

        [TestMethod]
        public void AuthenticationIdentityRepository_FindEmployee_ShouldReturnAuthenticationResultModel()
        {
            var parameters = new
            {
                Username = "ola",
                Password = "123123qwe!"
            };

            var result = _authenticationIdentityRepository.FindEmployee(parameters.Username, parameters.Password);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Identity);
            Assert.IsNotNull(result.User);
        }
    }
}
