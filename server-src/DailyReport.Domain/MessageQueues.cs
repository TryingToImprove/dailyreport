﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain
{
    // The format for messages queues is:
    // {project}.queues.{name} /LOWERCASE
    // where {name} uses '-' instead of ' '(spaces)
    public struct MessageQueues
    {
        public const string MailSender = ".\\PRIVATE$\\dailyreport.queues.mail-sender";

        public const string TemplatedMailBuilder = ".\\PRIVATE$\\dailyreport.queues.templated-mail-builder";

        public struct MailBuilders
        {
            public const string AddedAsContactPerson = ".\\PRIVATE$\\dailyreport.queues.mail-builder.added-as-contact-person";
        }
    }
}
