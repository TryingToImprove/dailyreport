﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Messages;

namespace DailyReport.Domain.Infrastructure.Dispensers
{
    public interface IMailDispenser
    {
        Task SendAsync(MailInformation information, string body);

        Task SendTemplatedAsync<T>(string template, T model, MailInformation information) where T : class;
    }
}
