﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Infrastructure
{
    public interface ILocationProvider
    {
        Task<IEnumerable<Location>> SearchAsync(string input);
    }

    public class Location
    {
        public string Id { get; set; }

        public string Address { get; set; }

        public string Street { get; set; }

        public string HouseNumber { get; set; }

        public string PostalCode { get; set; }

        public string City { get; set; }

        public LocationCountry Country { get; set; }
    }

    public class LocationCountry
    {
        public string Name { get; set; }

        public string Code { get; set; }
    }
}
