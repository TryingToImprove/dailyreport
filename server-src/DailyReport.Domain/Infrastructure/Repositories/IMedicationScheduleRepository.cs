﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;

namespace DailyReport.Domain.Infrastructure.Repositories
{
    public interface IMedicationScheduleRepository
    {
        MedicationScheduleResultModel GetById(int id);

        IEnumerable<MedicationScheduleResultModel> FindAll();

        IEnumerable<MedicationScheduleResultModel> FindByPatientId(int patientId);

        IEnumerable<MedicationScheduleResultModel> FindAllTodayByPatient(int patientId);

        IEnumerable<MedicationScheduleResultModel> FindUpcomings(int organizationId);

        MedicationSchedule Insert(MedicationSchedule medicationSchedule);

        IEnumerable<MedicationScheduleItemResultModel> GetPatientSchema(int patientId);
        
        MedicationScheduleResultModel Complete(MedicationScheduleComplete medicationComplete);
    }
}
