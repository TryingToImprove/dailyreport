﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;

namespace DailyReport.Domain.Infrastructure.Repositories
{
    public interface IContactInformationRepository
    {
        ContactInformationResultModel Insert(ContactInformation model);

        IEnumerable<ContactInformationResultModel> FindAll();

        IEnumerable<ContactInformationResultModel> FindPatientContactInformations(int patientId);

        IEnumerable<ContactInformationResultModel> FindEmployeeContactInformations(int employeeId);

        IEnumerable<ContactInformationResultModel> FindContactContactInformations(int contactId);

        ContactInformationResultModel GetById(int contactInformationId);

        void RegisterPatientRelationship(int contactInformationId, int patientId);

        void RegisterEmployeeRelationship(int contactInformationId, int employeeId);

        void RegisterContactRelationship(int contactInformationId, int contactId);

        void Update(ContactInformation model);

        ContactInformationResultModel Deactivate(int contactInformationId);

        ContactInformationResultModel Reactivate(int contactInformationId);
    }
}
