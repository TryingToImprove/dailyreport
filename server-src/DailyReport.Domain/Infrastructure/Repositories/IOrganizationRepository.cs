﻿using DailyReport.Domain.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Infrastructure.Repositories
{
    public interface IOrganizationRepository
    {
        Organization Insert(Organization organization);

        IEnumerable<Organization> FindAll();

        Organization GetById(int organizationId);

        void Update(Organization organization);
    }
}
