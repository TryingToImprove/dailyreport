﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Models;

namespace DailyReport.Domain.Infrastructure.Repositories
{
    public interface IContactRepository
    {
        IEnumerable<ContactResultModel> FindAll();

        IEnumerable<ContactResultModel> FindByPatient(int patientId);

        IEnumerable<ContactResultModel> FindByPatient(int patientId, int contactGroupId);

        ContactResultModel GetById(int id);

        ContactResultModel Insert(ContactbookInsertModel model);

        void RegisterPatientReference(int patientId, int contactId);

        void Update(int contactId, ContactUpdateModel model);

        void Delete(int contactId);
    }
}
