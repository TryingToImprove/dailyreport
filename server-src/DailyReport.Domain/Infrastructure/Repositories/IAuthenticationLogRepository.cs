﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Infrastructure.Repositories
{
    public interface IAuthenticationLogRepository
    {
        void Insert(int authenticationIdentityId, DateTime timeCreated);
    }
}
