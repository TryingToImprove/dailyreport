﻿using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Infrastructure.Repositories
{
    public interface IPatientRepository
    {
        IEnumerable<Patient> FindAll(int organizationId);

        PatientResultModel Insert(Patient patient);

        void Deactivate(int patientId);

        PatientResultModel GetById(int patientId);

        PatientResultModel Update(int patientId, PatientUpdateModel model);

        IEnumerable<Patient> FindByEmployee(int employeeId);
    }
}
