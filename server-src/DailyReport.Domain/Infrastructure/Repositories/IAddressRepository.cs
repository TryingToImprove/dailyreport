﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;

namespace DailyReport.Domain.Infrastructure.Repositories
{
    public interface IAddressRepository
    {
        PatientAddressResultModel AddPatientAddress(int patientId, PatientAddressInsertModel model);

        IEnumerable<PatientAddressResultModel> FindAllByPatient(int patientId);

        void RemovePatientAddress(int patientId, int addressId);

        PatientAddressResultModel UpdatePatientAddress(int patientId, int addressId, PatientAddressUpdateModel model);
        
        IEnumerable<PatientAddressResultModel> FindPrimaryPatient(int patientId);

        PatientAddressResultModel FindByIdPatient(int patientId, int addressId);
    }
}
