﻿using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Infrastructure.Repositories
{
    public interface IPatientContactPersonRelationshipRepository
    {
        IEnumerable<PatientContactPersonRelationshipResultModel> FindAll(int patientId);

        IEnumerable<PatientContactPersonRelationshipResultModel> FindCurrents(int patientId);

        PatientContactPersonRelationshipResultModel Insert(PatientContactPersonRelationship patientContactPersonRelationship, bool completePreviousContactPersons);

        PatientContactPersonRelationshipResultModel Complete(int relationshipId, DateTime timeCreated);
    }
}
