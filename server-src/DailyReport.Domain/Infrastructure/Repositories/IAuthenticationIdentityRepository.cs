﻿using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Infrastructure.Repositories
{
    public interface IAuthenticationIdentityRepository
    {
        AuthenticationIdentityResultModel GetById(int authenticationIdentityId);

        AuthenticationIdentityResultModel Insert(AuthenticationIdentityInsertModel model);

        AuthenticationResultModel<Employee> FindEmployee(string username, string passwordHash);

        void InsertAuthenticationIdentityKey(string authenticationKey, DateTime timeCreated);

        bool IsValidAuthenticationKey(string authenticationKey);

        void RegisterEmployeeRelationship(int authenticationIdentityId, int employeeId);
    }
}
