﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;

namespace DailyReport.Domain.Infrastructure.Repositories
{
    public interface IContactGroupRepository
    {
        IEnumerable<ContactGroupResultModel> FindAll();

        IEnumerable<ContactGroupResultModel> FindByPatient(int patientId);

        ContactGroupResultModel GetById(int id);
    }
}
