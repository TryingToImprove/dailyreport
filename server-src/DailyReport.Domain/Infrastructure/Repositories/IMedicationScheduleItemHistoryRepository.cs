﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Models.Core;
using DailyReport.Domain.Models;

namespace DailyReport.Domain.Infrastructure.Repositories
{
    public interface IMedicationScheduleItemHistoryRepository
    {
        MedicationScheduleItemHistoryResultModel Insert(MedicationScheduleItemHistory medicationScheduleItemHistory, int employeeId);

        IEnumerable<MedicationScheduleItemHistoryResultModel> FindAll();

        IEnumerable<MedicationScheduleItemHistoryResultModel> FindAll(int medicationScheduleItemId);
        
        IEnumerable<MedicationScheduleItemHistoryResultModel> FindByPatient(int patientId);
    }
}
