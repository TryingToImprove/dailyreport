﻿using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Infrastructure.Repositories
{
    public interface IMedicationRepository
    {
        IEnumerable<MedicationResultModel> FindAll();

        MedicationResultModel Insert(Medication medication);

        MedicationResultModel GetById(int id);
        
        IEnumerable<MedicationResultModel> Search(string query);
    }
}
