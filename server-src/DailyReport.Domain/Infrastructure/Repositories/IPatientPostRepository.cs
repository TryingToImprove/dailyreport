﻿using System.Collections;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Infrastructure.Repositories
{
    public interface IPatientPostRepository
    {
        PatientPost Insert(PatientPost patientPost);

        IEnumerable<PatientPostResultModel> FindAll(int organizationId);

        IEnumerable<PatientPostResultModel> FindAllByPatient(int patientId);
    }
}
