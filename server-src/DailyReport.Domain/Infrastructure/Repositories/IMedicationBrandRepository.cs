﻿using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Infrastructure.Repositories
{
    public interface IMedicationBrandRepository
    {
        IEnumerable<MedicationBrand> FindAll();

        MedicationBrand Insert(MedicationBrand medication);

        MedicationBrand GetById(int id);
    }
}
