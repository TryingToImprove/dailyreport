﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Models.Core;

namespace DailyReport.Domain.Infrastructure.Repositories
{
    public interface IDescriptionRepository
    {
        void InsertPatientDescription(int patientId, Description description);

        IEnumerable<Description> GetPatientDescriptions(int patientId);
    }
}
