﻿using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Infrastructure.Repositories
{
    public interface IEmployeeRepository
    {
        Employee Insert(EmployeeInsertModel model, string authenticationKey);

        IEnumerable<Employee> FindAll(int organizationId);

        EmployeeResultModel GetById(int employeeId);

        EmployeeResultModel GetBySecret(string secret);

        void Update(Employee employee);

        void UpdateActiveState(int employeeId, bool isActive);
    }
}
