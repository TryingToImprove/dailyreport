﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Infrastructure.Repositories
{
    public interface IPatientToPatientPostRepository
    {
        void Insert(int patientId, int patientPostId);

        void Remove(int patientId, int patientPostId);

        bool Contains(int patientId, int patientPostId);
    }
}
