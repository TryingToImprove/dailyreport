﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;

namespace DailyReport.Domain.Infrastructure.Services
{
    public interface IMedicationScheduleService
    {
        IMedicationScheduleHistoryRelationshipService Histories { get; }

        IEnumerable<MedicationScheduleResultModel> FindAll();

        IEnumerable<MedicationScheduleResultModel> FindByPatientId(int patientId);

        IEnumerable<MedicationScheduleResultModel> FindAllTodayByPatient(int patientId);

        IEnumerable<MedicationScheduleResultModel> FindUpcomings(int orgnaizationId);

        MedicationScheduleResultModel GetById(int id);

        MedicationScheduleResultModel Insert(MedicationScheduleInsertModel medicationScheduleInsertModel);

        MedicationScheduleResultModel CompleteSchedule(MedicationScheduleComplete medicationScheduleComplete);

        IEnumerable<MedicationScheduleItemResultModel> GetPatientSchema(int patientId);
    }
}
