﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Models;

namespace DailyReport.Domain.Infrastructure.Services
{
    public interface IContactPersonRelationshipService
    {
        IEnumerable<PatientContactPersonRelationshipResultModel> FindAll(int patientId);

        IEnumerable<PatientContactPersonRelationshipResultModel> FindCurrents(int patientId);

        PatientContactPersonRelationshipResultModel Add(int patientId, int employeeId);

        PatientContactPersonRelationshipResultModel Add(int patientId, int employeeId, bool completePreviousContactPersons);

        PatientContactPersonRelationshipResultModel Complete(int relationshipId);
    }
}
