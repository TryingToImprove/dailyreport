﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;

namespace DailyReport.Domain.Infrastructure.Services
{
    public interface IAuthenticationRelationshipService<T>
    {
        void RegisterRelationship(int relationshipId, int authenticationIdentityId);

        AuthenticationResultModel<T> Find(string username, string password);
    }
}
