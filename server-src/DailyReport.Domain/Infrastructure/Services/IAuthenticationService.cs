﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;

namespace DailyReport.Domain.Infrastructure.Services
{
    public interface IAuthenticationService
    {
        IAuthenticationRelationshipService<Employee> Employees { get; }

        string CreateAuthenticationKey();

        AuthenticationIdentityResultModel Insert(string authenticationKey, string username, string password);

        bool IsValidAuthenticationKey(string secret);
    }
}
