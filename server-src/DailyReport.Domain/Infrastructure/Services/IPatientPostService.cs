﻿using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Infrastructure.Services
{
    public interface IPatientPostService
    {
        void Insert(PatientPostInsertModel patientPost);

        IEnumerable<PatientPostResultModel> FindAll(int organizationId);

        IEnumerable<PatientPostResultModel> FindAllByPatient(int p);
    }
}
