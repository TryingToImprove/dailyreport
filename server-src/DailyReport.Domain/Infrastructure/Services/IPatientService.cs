﻿using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Infrastructure.Services
{
    public interface IPatientService
    {
        IContactInformationRelationshipService ContactInformations { get; }

        IContactbookRelationshipService Contacts { get; }

        IContactPersonRelationshipService ContactPersons { get; }

        IPatientAddressService Addresses { get; }

        IEnumerable<Patient> FindAll(int organizationId);

        IEnumerable<Patient> FindByEmployee(int employeeId);

        PatientResultModel Insert(PatientInsertModel patient);

        void Deactivate(int patientId);

        PatientResultModel GetById(int patientId);

        PatientResultModel Update(int patientId, PatientUpdateModel model);

        void CreateDescription(int patientId, Description description);

        IEnumerable<Description> GetDescriptions(int patientId);
    }
}
