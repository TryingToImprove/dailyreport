﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;

namespace DailyReport.Domain.Infrastructure.Services
{
    public interface IMedicationScheduleHistoryRelationshipService
    {
        IEnumerable<MedicationScheduleItemHistoryResultModel> FindAll(int relationshipId);

        IEnumerable<MedicationScheduleItemHistoryResultModel> Get(int medicationScheduleItemId);

        MedicationScheduleItemHistoryResultModel Add(MedicationScheduleItemHistory medicationScheduleItemHistory, int employeeId);
    }
}
