﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;

namespace DailyReport.Domain.Infrastructure.Services
{
    public interface IContactInformationService
    {
        IEnumerable<ContactInformationType> GetTypes();

        ContactInformationResultModel GetById(int contactInformationId);

        void Update(int contactInformationId, int contactInformationTypeId, string data);

        ContactInformationResultModel Deactivate(int contactInformationId);
    }
}
