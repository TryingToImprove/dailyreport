﻿using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Infrastructure.Services
{
    public interface IEmployeeService
    {
        IContactInformationRelationshipService ContactInformations { get; }

        IEnumerable<Employee> FindAll(int organizationId);

        Employee Insert(EmployeeInsertModel employee);

        EmployeeResultModel GetById(int employeeId);

        EmployeeResultModel GetBySecret(string secret);

        void Update(Employee employee);

        void UpdateActiveState(int employeeId, bool isActive);

        AuthenticationResultModel<Employee> Authenticate(string username, string password);
    }
}
