﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Infrastructure.Services
{
    public interface IContactService : IDisposable
    {
        void AddGroup(string groupName);

        void RemoveGroup(string groupName);

        void Update(string firstname, string lastname);

        void ChangeType(string contactType);
    }
}
