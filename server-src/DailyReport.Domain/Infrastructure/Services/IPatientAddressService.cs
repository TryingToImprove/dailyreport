﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Models;

namespace DailyReport.Domain.Infrastructure.Services
{
    public interface IPatientAddressService
    {
        PatientAddressResultModel Insert(int patientId, PatientAddressInsertModel model);

        IEnumerable<PatientAddressResultModel> FindAll(int patientId);

        void Remove(int patientId, int addressId);

        PatientAddressResultModel Update(int patientId, int addressId, PatientAddressUpdateModel model);

        IEnumerable<PatientAddressResultModel> FindPrimary(int patientId);

        PatientAddressResultModel FindById(int patientId, int addressId);
    }
}
