﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Models;

namespace DailyReport.Domain.Infrastructure.Services
{
    public interface ITimelineService
    {
        IEnumerable<TimelineEntryResult> FindByPatient(int patientId);
    }
}
