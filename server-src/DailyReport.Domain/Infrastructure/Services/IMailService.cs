﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Infrastructure.Services
{
    public interface IMailService
    {
        void Send(MailAddress fromAddress, IEnumerable<MailAddress> recipientAddresses, string subject, string htmlBody);

        void Send(MailAddress fromAddress, IEnumerable<MailAddress> recipientAddresses, string subject, string htmlBody, string textBody);

        Task SendAsync(MailAddress fromAddress, IEnumerable<MailAddress> recipientAddresses, string subject, string htmlBody);

        Task SendAsync(MailAddress fromAddress, IEnumerable<MailAddress> recipientAddresses, string subject, string htmlBody, string textBody);
    }
}
