﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;

namespace DailyReport.Domain.Infrastructure.Services
{
    public interface IMedicationService
    {
        IEnumerable<MedicationResultModel> FindAll();

        IEnumerable<MedicationBrand> FindBrands();

        MedicationResultModel Insert(MedicationInsertModel model);

        IEnumerable<Measure> GetMeasures();

        IEnumerable<MedicationType> GetMedicationTypes();

        IEnumerable<MedicationResultModel> Search(string query);
    }
}
