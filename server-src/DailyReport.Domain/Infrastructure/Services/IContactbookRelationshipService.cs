﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;

namespace DailyReport.Domain.Infrastructure.Services
{
    public interface IContactbookRelationshipService
    {
        IEnumerable<ContactResultModel> FindAll(int relationshipId);

        IEnumerable<ContactGroupResultModel> GetGroups(int relationshipId);

        IEnumerable<ContactResultModel> FindByGroup(int relationshipId, int groupId);

        ContactResultModel Add(int relationshipId, ContactbookInsertModel model);
    }
}
