﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Infrastructure.Services
{
    public interface ITemplateService
    {
        string Render<T>(string templateName, T viewModel) where T : new();
    }
}
