﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;

namespace DailyReport.Domain.Infrastructure.Services
{
    public interface IContactbookService
    {
        IContactInformationRelationshipService ContactInformations { get; }

        IEnumerable<ContactResultModel> FindAll();

        IContactService GetContactService(int contactId);

        void Remove(int contactId);

        ContactResultModel GetById(int id);

        ContactGroupResultModel GetGroup(int id);
    }
}
