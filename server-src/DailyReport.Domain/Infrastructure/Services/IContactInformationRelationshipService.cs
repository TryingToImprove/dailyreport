﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Models;

namespace DailyReport.Domain.Infrastructure.Services
{
    public interface IContactInformationRelationshipService
    {
        IEnumerable<ContactInformationResultModel> FindAll(int relationshipId);

        ContactInformationResultModel Add(int relationshipId, int employeeId, int contactInformationTypeId, string data);

        ContactInformationResultModel Add(int relationshipId, int employeeId, string dataType, string data);
    }
}
