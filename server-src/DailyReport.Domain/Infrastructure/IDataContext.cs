﻿using DailyReport.Domain.Infrastructure.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Models;

namespace DailyReport.Domain.Infrastructure
{
    public interface IDataContext
    {
        IOrganizationRepository Organizations { get; set; }

        IEmployeeRepository Employees { get; set; }

        IAuthenticationIdentityRepository AuthenticationIdentites { get; set; }

        IPatientRepository Patients { get; set; }

        IPatientPostRepository PatientPosts { get; set; }

        IPatientToPatientPostRepository PatientPostToPatients { get; set; }

        IDescriptionRepository Descriptions { get; set; }

        IAuthenticationLogRepository AuthenticationLogs { get; set; }

        IMedicationBrandRepository MedicationBrands { get; set; }

        IMedicationRepository Medications { get; set; }

        IMedicationTypeRepository MedicationTypes { get; set; }

        IMedicationScheduleRepository MedicationSchedules { get; set; }

        IMedicationScheduleItemRepository MedicationScheduleItems { get; set; }

        IMedicationScheduleItemHistoryRepository MedicationScheduleItemHistories { get; set; }

        IMedicationScheduleCompleteRepository MedicationScheduleCompletes { get; set; }

        IMeasureRepository Measures { get; set; }

        IPatientContactPersonRelationshipRepository PatientContactPersonRelationships { get; set; }

        IContactInformationRepository ContactInformations { get; set; }

        IContactInformationTypeRepository ContactInformationTypes { get; set; }

        IContactRepository Contacts { get; set; }

        IContactGroupRepository ContactGroups { get; set; }

        ITimelineRepository TimelineEntries { get; set; }

        IAddressRepository Addresses { get; set; }

        IDataContextScope BeginTransaction();
    }
}
