﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Messages
{
    public class MailInformation
    {
        public string From { get; set; }

        public IEnumerable<string> Recipients { get; set; }

        public string Subject { get; set; }
    }
}
