﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Messages
{
    public class TemplatedMailBuilderMessage
    {
        public MailInformation Information { get; set; }

        public string Template { get; set; }

        public object TemplateViewModel { get; set; }

        public Type TemplateViewModelType { get; set; }
    }
}
