﻿namespace DailyReport.Domain.Messages
{
    public class AddedAsContactPersonMessage
    {
        public int EmployeeId { get; set; }

        public int PatientId { get; set; }
    }
}