﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Messages
{
    public class MailSenderMessage
    {
        public MailInformation Information { get; set; }

        public string Body { get; set; }
    }
}
