﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Exceptions
{
    public class InvalidAuthenticationKeyException : Exception
    {
        public InvalidAuthenticationKeyException(string authenticationKey)
            : base("The authenticaiton key \"" + authenticationKey + "\" is not valid")
        {
        }
    }
}
