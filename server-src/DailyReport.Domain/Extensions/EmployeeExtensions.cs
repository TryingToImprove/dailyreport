﻿using DailyReport.Domain.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Extensions
{
    public static class EmployeeExtensions
    {
        public static string GetDisplayName(this Employee employee)
        {
            return employee.Firstname + " " + employee.Lastname;
        }
    }
}
