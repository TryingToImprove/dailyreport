﻿using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Extensions
{
    public static class MedicationExtensions
    {
        public static string GetDisplayName(this MedicationResultModel medication)
        {
            return string.Format("{0} af {2} {3} {4} ({1})", medication.Name, medication.Brand.Name, medication.Weight, medication.Measure, medication.Type.ToLower());
        }
    }
}
