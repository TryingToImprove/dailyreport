﻿using DailyReport.Domain.Models;

namespace DailyReport.Domain.Extensions
{
    public static class ContactExtensions
    {
        public static string GetDisplayName(this ContactResultModel contact)
        {
            return contact.Firstname + " " + contact.Lastname;
        }
    }
}
