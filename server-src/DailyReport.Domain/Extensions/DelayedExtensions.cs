﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Models;

namespace DailyReport.Domain.Extensions
{
    public static class DelayedExtensions
    {
        private const double Normal = 100;

        private const double Low = 100.694444444;

        private const double Medium = 101.388888889;

        private const double High = 102.083333333;

        public static DelayedState GetDelayedState(this MedicationScheduleItemResultModel schedule, DateTime startDate)
        {
            return GetDelayedState(startDate, schedule.ExecuteTime);
        }

        private static DelayedState GetDelayedState(DateTime startDate, TimeSpan executeDate)
        {
            if (startDate.Date > DateTime.Now.Date)
                return 0;

            var startTicks = DateTime.Now.TimeOfDay.Ticks;
            var executeTicks = executeDate.Ticks;

            var precent = (startTicks / (double)executeTicks) * 100;

            if (precent < Normal)
                return DelayedState.Normal;

            if (precent < Low)
                return DelayedState.Low;

            if (precent < Medium)
                return DelayedState.Medium;

            if (precent < High)
                return DelayedState.High;

            return DelayedState.Abandoned;
        }
    }
}
