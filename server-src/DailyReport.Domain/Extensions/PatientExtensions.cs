﻿using DailyReport.Domain.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Extensions
{
    public static class PatientExtensions
    {
        public static string GetDisplayName(this Patient patient)
        {
            return patient.Firstname + " " + patient.Lastname;
        }
    }
}
