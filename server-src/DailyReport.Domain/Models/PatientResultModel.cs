﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Models.Core;

namespace DailyReport.Domain.Models
{
    public class PatientResultModel : Patient
    {
        public Description Description { get; set; }
    }
}
