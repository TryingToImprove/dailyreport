﻿namespace DailyReport.Domain.Models
{
    public class ContactInformationModel
    {
        public string Data { get; set; }

        public int DataTypeId { get; set; }
    }
}