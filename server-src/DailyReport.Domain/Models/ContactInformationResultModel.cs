﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Models
{
    public class ContactInformationResultModel
    {
        public int Id { get; set; }

        public string Type { get; set; }

        public int TypeId { get; set; }

        public string Data { get; set; }

        public DateTime TimeCreated { get; set; }

        public bool IsActive { get; set; }
    }
}
