﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Models.Core
{
    public class MedicationScheduleItemHistory
    {
        public int Id { get; set; }

        public int MedicationScheduleItemId { get; set; }

        public DateTime TimeCreated { get; set; }
    }
}
