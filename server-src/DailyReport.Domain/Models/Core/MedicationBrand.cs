﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Models.Core
{
    public class MedicationBrand
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
