﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Models.Core
{
    public class PatientDescription
    {
        public int PatientId { get; set; }

        public int DescriptionId { get; set; }
    }
}
