﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Models.Core
{
    public class PatientPostReference
    {
        public int PatientId { get; set; }

        public int PatientPostId { get; set; }

        public PatientPostReference()
        {
        }

        public PatientPostReference(int patientId, int patientPostId)
        {
            PatientId = patientId;
            PatientPostId = patientPostId;
        }
    }
}
