﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Models.Core
{
    public class MedicationScheduleItem
    {
        public int Id { get; set; }

        public int MedicationScheduleId { get; set; }

        public int MedicationId { get; set; }

        public TimeSpan ExecuteTime { get; set; }

        public double Amouth { get; set; }
    }
}
