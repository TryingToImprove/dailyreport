﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Models.Core
{
    public class Medication
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int MedicationBrandId { get; set; }

        public int MedicationTypeId { get; set; }

        public int MeasureId { get; set; }

        public double Weight { get; set; }
    }
}
