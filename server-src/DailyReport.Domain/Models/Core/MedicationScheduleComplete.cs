﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Models.Core
{
    public class MedicationScheduleComplete
    {
        public int MedicationScheduleId { get; set; }

        public DateTime TimeCreated { get; set; }

        public string Description { get; set; }

        public int EmployeeId { get; set; }
    }
}
