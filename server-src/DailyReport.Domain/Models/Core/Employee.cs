﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Models.Core
{
    public class Employee
    {
        public static readonly Employee SYSTEM = new Employee()
        {
            Id = -1,
            OrganizationId = -1,
            Firstname = "SYSTEM",
            Lastname = string.Empty,
            TimeCreated = DateTime.MinValue,
            IsActive = false
        };

        public Employee()
        {
            IsActive = true;
        }

        public int Id { get; set; }

        public int OrganizationId { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public DateTime TimeCreated { get; set; }

        public bool IsActive { get; set; }

        public string Email { get; set; }
    }
}
