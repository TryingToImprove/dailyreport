﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Models.Core
{
    public class MedicationSchedule
    {
        public int Id { get; set; }

        public int PatientId { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime StartDate { get; set; }
    }
}
