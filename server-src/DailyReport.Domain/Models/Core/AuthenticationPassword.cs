﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Models.Core
{
    public class AuthenticationPassword
    {
        public int Id { get; set; }

        public int AuthenticationIdentityId { get; set; }

        public DateTime TimeCreated { get; set; }

        public string Password { get; set; }

        public bool IsActive { get; set; }
    }
}
