﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Models.Core
{
    public class AuthenticationIdentity
    {
        public int Id { get; set; }

        public string Username { get; set; }

        public DateTime TimeCreated { get; set; }

        public bool IsActive { get; set; }
    }
}
