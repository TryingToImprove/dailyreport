﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Models.Core
{
    public class ContactInformation
    {
        public ContactInformation()
        {
            IsActive = true;
        }

        public int Id { get; set; }

        public int ContactInformationTypeId { get; set; }

        public string Data { get; set; }

        public DateTime TimeCreated { get; set; }

        public int EmployeeId { get; set; }

        public bool IsActive { get; set; }
    }
}
