﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Models.Core
{
    public class MedicationScheduleItemRepeat
    {
        public int MedicationScheduleItemId { get; set; }

        public Int64 RepeatTimeSpan { get; set; }
    }
}
