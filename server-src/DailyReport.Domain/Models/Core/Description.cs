﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Models.Core
{
    public class Description
    {
        public int Id { get; set; }

        public int EmployeeId { get; set; }

        public DateTime TimeCreated { get; set; }

        public string Text { get; set; }
    }
}
