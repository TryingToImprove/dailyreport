﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Models.Core
{
    public class Patient
    {
        public Patient()
        {
            IsActive = true;
        }

        public int Id { get; set; }

        public int OrganizationId { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime BirthDate { get; set; }

        public bool IsActive { get; set; }
    }
}
