﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Models
{
    public enum DelayedState
    {
        Normal,
        Low,
        Medium,
        High,
        Abandoned
    }
}
