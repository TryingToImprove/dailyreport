﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Models.Core;

namespace DailyReport.Domain.Models
{
    public class PatientContactPersonRelationshipResultModel
    {
        public int Id { get; set; }

        public int PatientId { get; set; }

        public DateTime TimeStarted { get; set; }

        public DateTime? TimeEnded { get; set; }

        public Employee Employee { get; set; }

        public bool IsEnded
        {
            get { return TimeEnded.HasValue && TimeEnded.Value > DateTime.MinValue; }
        }
    }
}
