﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Models
{
    public struct TimeRange
    {
        public TimeSpan Min { get; private set; }

        public TimeSpan Max { get; private set; }

        public TimeRange(TimeSpan min, TimeSpan max) : this()
        {
            Min = min;
            Max = max;
        }
    }
}
