﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Models
{
    public class MedicationScheduleInsertModel
    {
        public int PatientId { get; set; }

        public DateTime StartDate { get; set; }

        public IEnumerable<MedicationScheduleItemInsertModel> Items { get; set; }
    }
}
