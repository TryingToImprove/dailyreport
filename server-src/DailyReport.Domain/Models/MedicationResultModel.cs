﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Models.Core;

namespace DailyReport.Domain.Models
{
    public class MedicationResultModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
        
        public string Type { get; set; }

        public string Measure { get; set; }

        public double Weight { get; set; }

        public MedicationBrand Brand { get; set; }
    }
}
