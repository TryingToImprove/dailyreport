﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Models
{
    public class AuthenticationIdentityInsertModel
    {
        public string Username { get; set; }

        public string Password { get; set; }

        public string AuthenticationKey { get; set; }

        public DateTime TimeCreated { get; set; }
    }
}
