﻿using DailyReport.Domain.Models.Core;
using System.Collections.Generic;

namespace DailyReport.Domain.Models
{
    public class PatientPostResultModel
    {
        public PatientPost PatientPost { get; set; }

        public Employee Employee { get; set; }

        public IList<Patient> Patients { get; set; }
    }
}
