﻿namespace DailyReport.Domain.Models
{
    public enum ScheduleType
    {
        Single = 0,
        Multiple = 1
    }
}