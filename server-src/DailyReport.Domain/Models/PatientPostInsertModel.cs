﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Models
{
    public class PatientPostInsertModel
    {
        public int OrganizationId { get; set; }

        public int EmployeeId { get; set; }

        public string Description { get; set; }

        public DateTime TimeCreated { get; set; }

        public IEnumerable<int> Patients { get; set; }
    }
}
