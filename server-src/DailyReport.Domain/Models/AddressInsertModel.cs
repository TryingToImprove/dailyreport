﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Models
{
    public class AddressInsertModel
    {
        public string Street { get; set; }

        public string HouseNumber { get; set; }

        public string City { get; set; }

        public string PostalCode { get; set; }
    }

    public class PatientAddressInsertModel : AddressInsertModel
    {
        public bool IsPrimary { get; set; }

        public bool ClearCurrentPrimary { get; set; }
    }

    public class PatientAddressUpdateModel : AddressInsertModel
    {
        public bool? IsPrimary { get; set; }

        public bool ClearCurrentPrimary { get; set; }
    }
}
