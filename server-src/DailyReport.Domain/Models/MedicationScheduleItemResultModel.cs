﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Models
{
    public class MedicationScheduleItemResultModel
    {
        public int Id { get; set; }

        public MedicationResultModel Medication { get; set; }

        public TimeSpan ExecuteTime { get; set; }

        public Int64? RepeatTimeSpan { get; set; }

        public double Amouth { get; set; }

        public DateTime? NextTime { get; set; }
    }
}
