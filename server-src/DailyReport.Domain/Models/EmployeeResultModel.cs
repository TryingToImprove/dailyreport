﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Models.Core;

namespace DailyReport.Domain.Models
{
    public class EmployeeResultModel : Employee
    {
        public DateTime? LastAuthenticationDateTime { get; set; }
    }
}
