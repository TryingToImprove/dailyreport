﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Models
{
    public class ContactbookInsertModel
    {
        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public DateTime TimeCreated { get; set; }

        public string Type { get; set; }

        public IEnumerable<string> Groups { get; set; }

        public int EmployeeId { get; set; }

        public IEnumerable<ContactInformationInsertModel> ContactInformations { get; set; }
    }
}
