﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Models
{
    public class ContactUpdateModel
    {
        public Dictionary<string, object> Properties { get; set; }

        public string Type { get; set; }

        public IEnumerable<string> RemovedGroups { get; set; }

        public IEnumerable<string> AddedGroups { get; set; }

        public bool RemoveType { get; set; }
    }
}
