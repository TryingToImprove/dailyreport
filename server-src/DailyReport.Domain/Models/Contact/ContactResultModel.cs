﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Models.Core;

namespace DailyReport.Domain.Models
{
    public class ContactResultModel : Contact
    {
        public ContactType ContactType { get; set; }

        public IList<ContactGroup> Groups { get; set; }

        public IList<ContactInformationResultModel> ContactInformations { get; set; }
    }
}
