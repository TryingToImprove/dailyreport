﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Models.Core;
using Microsoft.Build.Framework;

namespace DailyReport.Domain.Models
{
    public class PatientInsertModel
    {
        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public DateTime BirthDate { get; set; }

        public int OrganizationId { get; set; }

        public IEnumerable<int> ContactPersons { get; set; }

        public string Description { get; set; }

        public int CreatorEmployeeId { get; set; }

        public IEnumerable<ContactInformationModel> ContactInformations { get; set; }
    }
}
