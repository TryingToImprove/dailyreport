﻿using DailyReport.Domain.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Models
{
    public class MedicationScheduleItemHistoryResultModel
    {
        public MedicationScheduleItemHistoryResultModel()
        {
            Employee = Employee.SYSTEM;
        }

        public int Id { get; set; }

        public DateTime TimeCreated { get; set; }

        public Employee Employee { get; set; }

        public int MedicationScheduleItemId { get; set; }

        public MedicationResultModel Medication { get; set; }
    }
}
