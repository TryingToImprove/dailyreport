﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Models
{
    public class EmployeeInsertModel
    {
        public int OrganizationId { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public DateTime TimeCreated { get; set; }

        public string Email { get; set; }

        public string AuthenticationKey { get; set; }
    }
}
