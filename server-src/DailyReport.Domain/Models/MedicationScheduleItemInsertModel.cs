﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Models
{
    public class MedicationScheduleItemInsertModel
    {
        public int MedicationId { get; set; }

        public double Amouth { get; set; }

        public TimeSpan ExecuteTime { get; set; }

        public TimeSpan? RepeatInterval { get; set; }
    }
}
