﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Models.Core;

namespace DailyReport.Domain.Models
{
    public class MedicationScheduleResultModel
    {
        public int Id { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime StartDate { get; set; }

        public Patient Patient { get; set; }

        public IList<MedicationScheduleItemResultModel> Schedules { get; set; }

        public MedicationScheduleComplete Completed { get; set; }

        public bool IsCompleted { get { return Completed != null; } }

        public ScheduleType ScheduleType { get; set; }

        public DateTime? NextTime
        {
            get
            {
                if (Schedules == null || !Schedules.Any())
                    return null;

                return Schedules.OrderBy(x => x.NextTime)
                                .Select(x => x.NextTime)
                                .FirstOrDefault();
            }
        }
    }
}
