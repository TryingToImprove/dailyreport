﻿using System;

namespace DailyReport.Domain.Models
{
    public class TimelineEntryResult
    {
        public DateTime EntryDateTime { get; set; }

        public string EntryKey { get; set; }

        public object EntryData { get; set; }

        public object EntryId { get; set; }

        public string UniqueKey
        {
            get
            {
                return EntryKey + "-" + EntryId; 
            }
        }
    }
}