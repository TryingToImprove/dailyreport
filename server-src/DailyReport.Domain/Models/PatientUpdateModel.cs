﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Models
{
    public class PatientUpdateModel
    {
        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public string SecurityNumber { get; set; }
    }
}
