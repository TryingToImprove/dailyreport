﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Domain.Models
{
    public class TimelineEntry
    {
        public DateTime EntryDateTime { get; set; }

        public string EntryKey { get; set; }

        public string EntryData { get; set; }

        public object EntryId { get; set; }
    }
}
