﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Models.Core;
using FizzWare.NBuilder;

namespace DailyReport.Tests.Helpers.Fakes
{
    public class FakeDescription
    {
        public static Description Create()
        {
            return Builder<Description>.CreateNew().Build();
        }
    }
}
