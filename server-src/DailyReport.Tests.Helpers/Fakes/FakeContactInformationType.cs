﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Models.Core;
using FizzWare.NBuilder;

namespace DailyReport.Tests.Helpers.Fakes
{
    public class FakeContactInformationType
    {
        public static IEnumerable<ContactInformationType> CreateTypes()
        {
            return Builder<ContactInformationType>.CreateListOfSize(10)
                        .Build();
        } 
    }
}
