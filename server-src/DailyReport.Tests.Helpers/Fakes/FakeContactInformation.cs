﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using FizzWare.NBuilder;

namespace DailyReport.Tests.Helpers.Fakes
{
    public class FakeContactInformation
    {
        public static IEnumerable<ContactInformationResultModel> CreateResultModels()
        {
            return Builder<ContactInformationResultModel>.CreateListOfSize(10)
                        .Build();
        }

        public static ContactInformationResultModel CreateResultModel()
        {
            return Builder<ContactInformationResultModel>.CreateNew()
                        .Build();
        }
    }
}
