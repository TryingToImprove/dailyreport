﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using FizzWare.NBuilder;

namespace DailyReport.Tests.Helpers.Fakes
{
    public class FakePatient
    {
        public static Patient Create()
        {
            return Builder<Patient>.CreateNew().Build();
        }

        public static Patient Create(int organizationId)
        {
            return Builder<Patient>.CreateNew()
                .With(x => x.OrganizationId, organizationId)
                .Build();
        }

        public static PatientResultModel CreateResultModel()
        {
            return Builder<PatientResultModel>.CreateNew()
                .With(x =>
                {
                    x.Description = Builder<Description>.CreateNew().Build();
                    return x;
                })
                .Build();
        }

        public static IEnumerable<PatientResultModel> CreateResultModels(int organizationId)
        {
            return Builder<PatientResultModel>.CreateListOfSize(10)
                .All()
                .With(x =>
                {
                    x.OrganizationId = organizationId;
                    x.Description = Builder<Description>.CreateNew().Build();
                    return x;
                })
                .Build();
        }
    }
}
