﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Infrastructure;
using DailyReport.Domain.Infrastructure.Repositories;
using Moq;

namespace DailyReport.Tests.Helpers.Fakes
{
    public class FakeDataContext
    {
        public static FakeDataContext Create()
        {
            return new FakeDataContext();
        }

        public Mock<IDataContext> DataContextMock
        {
            get
            {
                var dataContextMock = new Mock<IDataContext>();

                if (_patientContactPersonRelationships != null)
                    dataContextMock.Setup(x => x.PatientContactPersonRelationships).Returns(_patientContactPersonRelationships.Object);

                if (_descriptions != null)
                    dataContextMock.Setup(x => x.Descriptions).Returns(_descriptions.Object);

                if (_patients != null)
                    dataContextMock.Setup(x => x.Patients).Returns(_patients.Object);

                if (_contactInformations != null)
                    dataContextMock.Setup(x=> x.ContactInformations).Returns(_contactInformations.Object);

                if (_contactInformationTypes != null)
                    dataContextMock.Setup(x => x.ContactInformationTypes).Returns(_contactInformationTypes.Object);

                return dataContextMock;
            }
        }

        public IDataContext Object { get { return DataContextMock.Object; } }

        public FakeDataContext With(Action<FakeDataContext> action)
        {
            action.Invoke(this);

            return this;
        }

        public FakeDataContext Intialize<T>(Func<FakeDataContext, T> func)
        {
            func.Invoke(this);

            return this;
        }

        public Mock<IOrganizationRepository> Organizations { get; private set; }
        public Mock<IEmployeeRepository> Employees { get; private set; }
        public Mock<IAuthenticationIdentityRepository> AuthenticationIdentites { get; set; }

        private Mock<IPatientRepository> _patients;
        public Mock<IPatientRepository> Patients
        {
            get
            {
                return _patients ?? (_patients = new Mock<IPatientRepository>());
            }
        }

        public Mock<IPatientPostRepository> PatientPosts { get; set; }
        public Mock<IPatientToPatientPostRepository> PatientPostToPatients { get; set; }

        private Mock<IDescriptionRepository> _descriptions;

        public Mock<IDescriptionRepository> Descriptions
        {
            get { return _descriptions ?? (_descriptions = new Mock<IDescriptionRepository>()); }
        }

        public Mock<IAuthenticationLogRepository> AuthenticationLogs { get; set; }
        public Mock<IMedicationBrandRepository> MedicationBrands { get; set; }
        public Mock<IMedicationRepository> Medications { get; set; }
        public Mock<IMedicationTypeRepository> MedicationTypes { get; set; }
        public Mock<IMedicationScheduleRepository> MedicationSchedules { get; set; }
        public Mock<IMedicationScheduleItemRepository> MedicationScheduleItems { get; set; }
        public Mock<IMedicationScheduleItemHistoryRepository> MedicationScheduleItemHistories { get; set; }
        public Mock<IMedicationScheduleCompleteRepository> MedicationScheduleCompletes { get; set; }
        public Mock<IMeasureRepository> Measures { get; set; }

        private Mock<IPatientContactPersonRelationshipRepository> _patientContactPersonRelationships;
        public Mock<IPatientContactPersonRelationshipRepository> PatientContactPersonRelationships
        {
            get
            {
                return _patientContactPersonRelationships ??
                       (_patientContactPersonRelationships = new Mock<IPatientContactPersonRelationshipRepository>());
            }
        }

        private Mock<IContactInformationRepository> _contactInformations;
        public Mock<IContactInformationRepository> ContactInformations
        {
            get
            {
                return _contactInformations ??
                       (_contactInformations = new Mock<IContactInformationRepository>());
            }
        }

        private Mock<IContactInformationTypeRepository> _contactInformationTypes;
        public Mock<IContactInformationTypeRepository> ContactInformationTypes
        {
            get
            {
                return _contactInformationTypes ??
                       (_contactInformationTypes = new Mock<IContactInformationTypeRepository>());
            }
        }

        public IDataContextScope BeginTransaction()
        {
            throw new NotImplementedException();
        }

    }
}
