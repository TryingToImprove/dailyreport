﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Models;
using FizzWare.NBuilder;

namespace DailyReport.Tests.Helpers.Fakes
{
    public class FakePatientContactPersonRelationship
    {
        public static IEnumerable<PatientContactPersonRelationshipResultModel> CreateResultModels()
        {
            return Builder<PatientContactPersonRelationshipResultModel>.CreateListOfSize(10)
                        .Build();
        } 
    }
}
