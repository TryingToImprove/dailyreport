﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyReport.Tests.Helpers
{
    public static class TestDatabase
    {
        private static string _structureScripts;
        private static string _initializeScripts;
        private static IEnumerable<string> _commands;

        private static string StructureScripts
        {
            get
            {
                return _structureScripts ??
                       (_structureScripts = _structureScripts = string.Join("\n", File.ReadAllLines("Script.sql")));
            }
        }
        private static string InitalizeScripts
        {
            get
            {
                return _initializeScripts ??
                       (_initializeScripts = _initializeScripts = string.Join("\n", File.ReadAllLines("StartupScript.sql")));
            }
        }

        private static IEnumerable<string> Commands
        {
            get
            {
                if (_commands == null)
                {
                    var sql = StructureScripts;
                    sql += "\n\n";
                    sql += InitalizeScripts;

                    _commands = sql.Split(new[] { "GO\r\n", "GO ", "GO\t", "GO" }, StringSplitOptions.RemoveEmptyEntries);
                }

                return _commands;
            }
        }

        public static IDbConnection Create()
        {
            var connection = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=""|DataDirectory|LocalDatabase4.mdf"";Integrated Security=True;Connect Timeout=30");

            connection.Open();

            try
            {
                foreach (var c in Commands)
                {
                    using (var command = new SqlCommand(c, connection))
                        command.ExecuteNonQuery();
                }
            }
            finally
            {
                connection.Close();
            }

            return connection;
        }
    }
}
