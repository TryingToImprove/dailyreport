﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DailyReport.Tests.Helpers
{
    public static class ExtendedAssert
    {
        //Code from http://stackoverflow.com/a/5634337/1514875
        public static void Throws<T>(Action func) where T: Exception
        {
            var exceptionThrown = false;

            try
            {
                func.Invoke();
            }
            catch
            {
                exceptionThrown = true;
            }

            if(!exceptionThrown)
                throw new AssertFailedException(string.Format("An exception of type {0} was expected, but not thrown", typeof(T)));
        }
    }
}
