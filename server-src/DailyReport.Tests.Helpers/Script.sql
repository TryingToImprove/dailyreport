IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PatientPost_Patient_PatientPost]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientPost_Patient]'))
ALTER TABLE [dbo].[PatientPost_Patient] DROP CONSTRAINT [FK_PatientPost_Patient_PatientPost]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PatientPost_Patient_Patient]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientPost_Patient]'))
ALTER TABLE [dbo].[PatientPost_Patient] DROP CONSTRAINT [FK_PatientPost_Patient_Patient]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PatientPost_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientPost]'))
ALTER TABLE [dbo].[PatientPost] DROP CONSTRAINT [FK_PatientPost_Organization]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PatientPost_Employee]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientPost]'))
ALTER TABLE [dbo].[PatientPost] DROP CONSTRAINT [FK_PatientPost_Employee]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PatientDescription_Patient]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientDescription]'))
ALTER TABLE [dbo].[PatientDescription] DROP CONSTRAINT [FK_PatientDescription_Patient]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PatientDescription_Description]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientDescription]'))
ALTER TABLE [dbo].[PatientDescription] DROP CONSTRAINT [FK_PatientDescription_Description]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PatientContactPersonRelationshipEnded_PatientContactPersonRelationship]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientContactPersonRelationshipEnded]'))
ALTER TABLE [dbo].[PatientContactPersonRelationshipEnded] DROP CONSTRAINT [FK_PatientContactPersonRelationshipEnded_PatientContactPersonRelationship]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PatientContactPersonRelationship_Patient]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientContactPersonRelationship]'))
ALTER TABLE [dbo].[PatientContactPersonRelationship] DROP CONSTRAINT [FK_PatientContactPersonRelationship_Patient]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PatientContactPersonRelationship_Employee]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientContactPersonRelationship]'))
ALTER TABLE [dbo].[PatientContactPersonRelationship] DROP CONSTRAINT [FK_PatientContactPersonRelationship_Employee]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Patient_ContactInformation_Patient]') AND parent_object_id = OBJECT_ID(N'[dbo].[Patient_ContactInformation]'))
ALTER TABLE [dbo].[Patient_ContactInformation] DROP CONSTRAINT [FK_Patient_ContactInformation_Patient]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Patient_ContactInformation_ContactInformation]') AND parent_object_id = OBJECT_ID(N'[dbo].[Patient_ContactInformation]'))
ALTER TABLE [dbo].[Patient_ContactInformation] DROP CONSTRAINT [FK_Patient_ContactInformation_ContactInformation]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Patient_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[Patient]'))
ALTER TABLE [dbo].[Patient] DROP CONSTRAINT [FK_Patient_Organization]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MedicationScheduleItemRepeat_MedicationScheduleItem]') AND parent_object_id = OBJECT_ID(N'[dbo].[MedicationScheduleItemRepeat]'))
ALTER TABLE [dbo].[MedicationScheduleItemRepeat] DROP CONSTRAINT [FK_MedicationScheduleItemRepeat_MedicationScheduleItem]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MedicationScheduleItemHistoryEmployee_MedicationScheduleItemHistory]') AND parent_object_id = OBJECT_ID(N'[dbo].[MedicationScheduleItemHistoryEmployee]'))
ALTER TABLE [dbo].[MedicationScheduleItemHistoryEmployee] DROP CONSTRAINT [FK_MedicationScheduleItemHistoryEmployee_MedicationScheduleItemHistory]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MedicationScheduleItemHistory_MedicationScheduleItem]') AND parent_object_id = OBJECT_ID(N'[dbo].[MedicationScheduleItemHistory]'))
ALTER TABLE [dbo].[MedicationScheduleItemHistory] DROP CONSTRAINT [FK_MedicationScheduleItemHistory_MedicationScheduleItem]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MedicationScheduleItem_MedicationSchedule]') AND parent_object_id = OBJECT_ID(N'[dbo].[MedicationScheduleItem]'))
ALTER TABLE [dbo].[MedicationScheduleItem] DROP CONSTRAINT [FK_MedicationScheduleItem_MedicationSchedule]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MedicationScheduleItem_Medication]') AND parent_object_id = OBJECT_ID(N'[dbo].[MedicationScheduleItem]'))
ALTER TABLE [dbo].[MedicationScheduleItem] DROP CONSTRAINT [FK_MedicationScheduleItem_Medication]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MedicationScheduleComplete_MedicationSchedule]') AND parent_object_id = OBJECT_ID(N'[dbo].[MedicationScheduleComplete]'))
ALTER TABLE [dbo].[MedicationScheduleComplete] DROP CONSTRAINT [FK_MedicationScheduleComplete_MedicationSchedule]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MedicationScheduleComplete_Employee]') AND parent_object_id = OBJECT_ID(N'[dbo].[MedicationScheduleComplete]'))
ALTER TABLE [dbo].[MedicationScheduleComplete] DROP CONSTRAINT [FK_MedicationScheduleComplete_Employee]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MedicationSchedule_Patient]') AND parent_object_id = OBJECT_ID(N'[dbo].[MedicationSchedule]'))
ALTER TABLE [dbo].[MedicationSchedule] DROP CONSTRAINT [FK_MedicationSchedule_Patient]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Medication_MedicationType]') AND parent_object_id = OBJECT_ID(N'[dbo].[Medication]'))
ALTER TABLE [dbo].[Medication] DROP CONSTRAINT [FK_Medication_MedicationType]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Medication_MedicationBrand]') AND parent_object_id = OBJECT_ID(N'[dbo].[Medication]'))
ALTER TABLE [dbo].[Medication] DROP CONSTRAINT [FK_Medication_MedicationBrand]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Medication_Measure]') AND parent_object_id = OBJECT_ID(N'[dbo].[Medication]'))
ALTER TABLE [dbo].[Medication] DROP CONSTRAINT [FK_Medication_Measure]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Employee_ContactInformation_Employee]') AND parent_object_id = OBJECT_ID(N'[dbo].[Employee_ContactInformation]'))
ALTER TABLE [dbo].[Employee_ContactInformation] DROP CONSTRAINT [FK_Employee_ContactInformation_Employee]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Employee_ContactInformation_ContactInformation]') AND parent_object_id = OBJECT_ID(N'[dbo].[Employee_ContactInformation]'))
ALTER TABLE [dbo].[Employee_ContactInformation] DROP CONSTRAINT [FK_Employee_ContactInformation_ContactInformation]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Employee_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[Employee]'))
ALTER TABLE [dbo].[Employee] DROP CONSTRAINT [FK_Employee_Organization]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Employee_AuthenticationIdentity]') AND parent_object_id = OBJECT_ID(N'[dbo].[Employee]'))
ALTER TABLE [dbo].[Employee] DROP CONSTRAINT [FK_Employee_AuthenticationIdentity]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ContactInformation_ContactInformationType]') AND parent_object_id = OBJECT_ID(N'[dbo].[ContactInformation]'))
ALTER TABLE [dbo].[ContactInformation] DROP CONSTRAINT [FK_ContactInformation_ContactInformationType]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AuthenticationLog_AuthenticationIdentity]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuthenticationLog]'))
ALTER TABLE [dbo].[AuthenticationLog] DROP CONSTRAINT [FK_AuthenticationLog_AuthenticationIdentity]
GO
/****** Object:  Table [dbo].[PatientPost_Patient]    Script Date: 11/21/2014 12:12:08 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PatientPost_Patient]') AND type in (N'U'))
DROP TABLE [dbo].[PatientPost_Patient]
GO
/****** Object:  Table [dbo].[PatientPost]    Script Date: 11/21/2014 12:12:08 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PatientPost]') AND type in (N'U'))
DROP TABLE [dbo].[PatientPost]
GO
/****** Object:  Table [dbo].[PatientDescription]    Script Date: 11/21/2014 12:12:08 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PatientDescription]') AND type in (N'U'))
DROP TABLE [dbo].[PatientDescription]
GO
/****** Object:  Table [dbo].[PatientContactPersonRelationshipEnded]    Script Date: 11/21/2014 12:12:08 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PatientContactPersonRelationshipEnded]') AND type in (N'U'))
DROP TABLE [dbo].[PatientContactPersonRelationshipEnded]
GO
/****** Object:  Table [dbo].[PatientContactPersonRelationship]    Script Date: 11/21/2014 12:12:08 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PatientContactPersonRelationship]') AND type in (N'U'))
DROP TABLE [dbo].[PatientContactPersonRelationship]
GO
/****** Object:  Table [dbo].[Patient_ContactInformation]    Script Date: 11/21/2014 12:12:08 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Patient_ContactInformation]') AND type in (N'U'))
DROP TABLE [dbo].[Patient_ContactInformation]
GO
/****** Object:  Table [dbo].[Patient]    Script Date: 11/21/2014 12:12:08 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Patient]') AND type in (N'U'))
DROP TABLE [dbo].[Patient]
GO
/****** Object:  Table [dbo].[Organization]    Script Date: 11/21/2014 12:12:08 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Organization]') AND type in (N'U'))
DROP TABLE [dbo].[Organization]
GO
/****** Object:  Table [dbo].[MedicationType]    Script Date: 11/21/2014 12:12:08 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MedicationType]') AND type in (N'U'))
DROP TABLE [dbo].[MedicationType]
GO
/****** Object:  Table [dbo].[MedicationScheduleItemRepeat]    Script Date: 11/21/2014 12:12:08 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MedicationScheduleItemRepeat]') AND type in (N'U'))
DROP TABLE [dbo].[MedicationScheduleItemRepeat]
GO
/****** Object:  Table [dbo].[MedicationScheduleItemHistoryEmployee]    Script Date: 11/21/2014 12:12:08 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MedicationScheduleItemHistoryEmployee]') AND type in (N'U'))
DROP TABLE [dbo].[MedicationScheduleItemHistoryEmployee]
GO
/****** Object:  Table [dbo].[MedicationScheduleItemHistory]    Script Date: 11/21/2014 12:12:08 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MedicationScheduleItemHistory]') AND type in (N'U'))
DROP TABLE [dbo].[MedicationScheduleItemHistory]
GO
/****** Object:  Table [dbo].[MedicationScheduleItem]    Script Date: 11/21/2014 12:12:08 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MedicationScheduleItem]') AND type in (N'U'))
DROP TABLE [dbo].[MedicationScheduleItem]
GO
/****** Object:  Table [dbo].[MedicationScheduleComplete]    Script Date: 11/21/2014 12:12:08 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MedicationScheduleComplete]') AND type in (N'U'))
DROP TABLE [dbo].[MedicationScheduleComplete]
GO
/****** Object:  Table [dbo].[MedicationSchedule]    Script Date: 11/21/2014 12:12:08 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MedicationSchedule]') AND type in (N'U'))
DROP TABLE [dbo].[MedicationSchedule]
GO
/****** Object:  Table [dbo].[MedicationBrand]    Script Date: 11/21/2014 12:12:08 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MedicationBrand]') AND type in (N'U'))
DROP TABLE [dbo].[MedicationBrand]
GO
/****** Object:  Table [dbo].[Medication]    Script Date: 11/21/2014 12:12:08 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Medication]') AND type in (N'U'))
DROP TABLE [dbo].[Medication]
GO
/****** Object:  Table [dbo].[Measure]    Script Date: 11/21/2014 12:12:08 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Measure]') AND type in (N'U'))
DROP TABLE [dbo].[Measure]
GO
/****** Object:  Table [dbo].[Employee_ContactInformation]    Script Date: 11/21/2014 12:12:08 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Employee_ContactInformation]') AND type in (N'U'))
DROP TABLE [dbo].[Employee_ContactInformation]
GO
/****** Object:  Table [dbo].[Employee]    Script Date: 11/21/2014 12:12:08 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Employee]') AND type in (N'U'))
DROP TABLE [dbo].[Employee]
GO
/****** Object:  Table [dbo].[Description]    Script Date: 11/21/2014 12:12:08 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Description]') AND type in (N'U'))
DROP TABLE [dbo].[Description]
GO
/****** Object:  Table [dbo].[ContactInformationType]    Script Date: 11/21/2014 12:12:08 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ContactInformationType]') AND type in (N'U'))
DROP TABLE [dbo].[ContactInformationType]
GO
/****** Object:  Table [dbo].[ContactInformation]    Script Date: 11/21/2014 12:12:08 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ContactInformation]') AND type in (N'U'))
DROP TABLE [dbo].[ContactInformation]
GO
/****** Object:  Table [dbo].[AuthenticationLog]    Script Date: 11/21/2014 12:12:08 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AuthenticationLog]') AND type in (N'U'))
DROP TABLE [dbo].[AuthenticationLog]
GO
/****** Object:  Table [dbo].[AuthenticationIdentity]    Script Date: 11/21/2014 12:12:08 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AuthenticationIdentity]') AND type in (N'U'))
DROP TABLE [dbo].[AuthenticationIdentity]
GO
/****** Object:  Table [dbo].[AuthenticationIdentity]    Script Date: 11/21/2014 12:12:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AuthenticationIdentity]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AuthenticationIdentity](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](50) NOT NULL,
	[PasswordHash] [nvarchar](255) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_AuthenticationIdentity] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[AuthenticationLog]    Script Date: 11/21/2014 12:12:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AuthenticationLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AuthenticationLog](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TimeCreated] [datetime] NOT NULL,
	[AuthenticationId] [int] NOT NULL,
 CONSTRAINT [PK_AuthenticationLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ContactInformation]    Script Date: 11/21/2014 12:12:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ContactInformation]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ContactInformation](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ContactInformationTypeId] [int] NOT NULL,
	[Data] [text] NOT NULL,
	[TimeCreated] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_ContactInformation] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ContactInformationType]    Script Date: 11/21/2014 12:12:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ContactInformationType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ContactInformationType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TypeName] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_ContactInformationType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Description]    Script Date: 11/21/2014 12:12:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Description]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Description](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Text] [text] NOT NULL,
	[TimeCreated] [datetime] NOT NULL,
	[EmployeeId] [int] NOT NULL,
 CONSTRAINT [PK_Description] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Employee]    Script Date: 11/21/2014 12:12:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Employee]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Employee](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationId] [int] NOT NULL,
	[Firstname] [nvarchar](150) NOT NULL,
	[Lastname] [nvarchar](150) NOT NULL,
	[TimeCreated] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Employee_IsActive]  DEFAULT ((1)),
	[AuthenticationId] [int] NOT NULL,
 CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Employee_ContactInformation]    Script Date: 11/21/2014 12:12:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Employee_ContactInformation]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Employee_ContactInformation](
	[ContactInformationId] [int] NOT NULL,
	[EmployeeId] [int] NOT NULL,
 CONSTRAINT [PK_Employee_ContactInformation] PRIMARY KEY CLUSTERED 
(
	[ContactInformationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Measure]    Script Date: 11/21/2014 12:12:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Measure]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Measure](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Measure] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Medication]    Script Date: 11/21/2014 12:12:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Medication]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Medication](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[MedicationBrandId] [int] NOT NULL,
	[MedicationTypeId] [int] NOT NULL,
	[MeasureId] [int] NOT NULL,
	[Weight] [decimal](16, 2) NOT NULL,
 CONSTRAINT [PK_Medication] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[MedicationBrand]    Script Date: 11/21/2014 12:12:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MedicationBrand]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MedicationBrand](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
 CONSTRAINT [PK_MedicationBrand] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[MedicationSchedule]    Script Date: 11/21/2014 12:12:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MedicationSchedule]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MedicationSchedule](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PatientId] [int] NOT NULL,
	[TimeCreated] [datetime] NOT NULL,
	[StartDate] [date] NOT NULL,
 CONSTRAINT [PK_MedicationSchedule] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[MedicationScheduleComplete]    Script Date: 11/21/2014 12:12:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MedicationScheduleComplete]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MedicationScheduleComplete](
	[MedicationScheduleId] [int] NOT NULL,
	[TimeCreated] [datetime] NOT NULL,
	[Description] [text] NOT NULL,
	[EmployeeId] [int] NOT NULL,
 CONSTRAINT [PK_MedicationScheduleComplete] PRIMARY KEY CLUSTERED 
(
	[MedicationScheduleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[MedicationScheduleItem]    Script Date: 11/21/2014 12:12:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MedicationScheduleItem]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MedicationScheduleItem](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MedicationScheduleId] [int] NOT NULL,
	[MedicationId] [int] NOT NULL,
	[ExecuteTime] [time](7) NOT NULL,
	[Amouth] [decimal](16, 2) NOT NULL,
 CONSTRAINT [PK_MedicationScheduleItem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[MedicationScheduleItemHistory]    Script Date: 11/21/2014 12:12:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MedicationScheduleItemHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MedicationScheduleItemHistory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MedicationScheduleItemId] [int] NOT NULL,
	[TimeCreated] [datetime] NOT NULL,
 CONSTRAINT [PK_MedicationScheduleItemHistory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[MedicationScheduleItemHistoryEmployee]    Script Date: 11/21/2014 12:12:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MedicationScheduleItemHistoryEmployee]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MedicationScheduleItemHistoryEmployee](
	[MedicationScheduleItemHistoryId] [int] NOT NULL,
	[EmployeeId] [int] NOT NULL,
 CONSTRAINT [PK_MedicationScheduleItemHistoryEmployee] PRIMARY KEY CLUSTERED 
(
	[MedicationScheduleItemHistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[MedicationScheduleItemRepeat]    Script Date: 11/21/2014 12:12:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MedicationScheduleItemRepeat]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MedicationScheduleItemRepeat](
	[MedicationScheduleItemId] [int] NOT NULL,
	[RepeatTimeSpan] [bigint] NOT NULL,
 CONSTRAINT [PK_MedicationScheduleItemRepeat] PRIMARY KEY CLUSTERED 
(
	[MedicationScheduleItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[MedicationType]    Script Date: 11/21/2014 12:12:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MedicationType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MedicationType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_MedicationType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Organization]    Script Date: 11/21/2014 12:12:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Organization]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Organization](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[TimeCreated] [datetime] NOT NULL,
 CONSTRAINT [PK_Organization] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Patient]    Script Date: 11/21/2014 12:12:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Patient]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Patient](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationId] [int] NOT NULL,
	[Firstname] [nvarchar](150) NOT NULL,
	[Lastname] [nvarchar](150) NOT NULL,
	[TimeCreated] [datetime] NOT NULL,
	[BirthDate] [date] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_Patient] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Patient_ContactInformation]    Script Date: 11/21/2014 12:12:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Patient_ContactInformation]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Patient_ContactInformation](
	[ContactInformationId] [int] NOT NULL,
	[PatientId] [int] NOT NULL,
 CONSTRAINT [PK_Patient_ContactInformation] PRIMARY KEY CLUSTERED 
(
	[ContactInformationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[PatientContactPersonRelationship]    Script Date: 11/21/2014 12:12:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PatientContactPersonRelationship]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PatientContactPersonRelationship](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PatientId] [int] NOT NULL,
	[EmployeeId] [int] NOT NULL,
	[TimeCreated] [datetime] NOT NULL,
 CONSTRAINT [PK_PatientContactPersonRelationship] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[PatientContactPersonRelationshipEnded]    Script Date: 11/21/2014 12:12:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PatientContactPersonRelationshipEnded]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PatientContactPersonRelationshipEnded](
	[PatientContactPersonRelationshipId] [int] NOT NULL,
	[TimeCreated] [datetime] NOT NULL,
 CONSTRAINT [PK_PatientContactPersonRelationshipEnded] PRIMARY KEY CLUSTERED 
(
	[PatientContactPersonRelationshipId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[PatientDescription]    Script Date: 11/21/2014 12:12:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PatientDescription]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PatientDescription](
	[PatientId] [int] NOT NULL,
	[DescriptionId] [int] NOT NULL,
 CONSTRAINT [PK_PatientDescription] PRIMARY KEY CLUSTERED 
(
	[PatientId] ASC,
	[DescriptionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[PatientPost]    Script Date: 11/21/2014 12:12:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PatientPost]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PatientPost](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeId] [int] NOT NULL,
	[TimeCreated] [datetime] NOT NULL,
	[Description] [text] NOT NULL,
	[OrganizationId] [int] NOT NULL,
 CONSTRAINT [PK_PatientPost] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[PatientPost_Patient]    Script Date: 11/21/2014 12:12:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PatientPost_Patient]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PatientPost_Patient](
	[PatientId] [int] NOT NULL,
	[PatientPostId] [int] NOT NULL,
 CONSTRAINT [PK_PatientPost_Patient] PRIMARY KEY CLUSTERED 
(
	[PatientId] ASC,
	[PatientPostId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AuthenticationLog_AuthenticationIdentity]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuthenticationLog]'))
ALTER TABLE [dbo].[AuthenticationLog]  WITH CHECK ADD  CONSTRAINT [FK_AuthenticationLog_AuthenticationIdentity] FOREIGN KEY([AuthenticationId])
REFERENCES [dbo].[AuthenticationIdentity] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AuthenticationLog_AuthenticationIdentity]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuthenticationLog]'))
ALTER TABLE [dbo].[AuthenticationLog] CHECK CONSTRAINT [FK_AuthenticationLog_AuthenticationIdentity]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ContactInformation_ContactInformationType]') AND parent_object_id = OBJECT_ID(N'[dbo].[ContactInformation]'))
ALTER TABLE [dbo].[ContactInformation]  WITH CHECK ADD  CONSTRAINT [FK_ContactInformation_ContactInformationType] FOREIGN KEY([ContactInformationTypeId])
REFERENCES [dbo].[ContactInformationType] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ContactInformation_ContactInformationType]') AND parent_object_id = OBJECT_ID(N'[dbo].[ContactInformation]'))
ALTER TABLE [dbo].[ContactInformation] CHECK CONSTRAINT [FK_ContactInformation_ContactInformationType]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Employee_AuthenticationIdentity]') AND parent_object_id = OBJECT_ID(N'[dbo].[Employee]'))
ALTER TABLE [dbo].[Employee]  WITH CHECK ADD  CONSTRAINT [FK_Employee_AuthenticationIdentity] FOREIGN KEY([AuthenticationId])
REFERENCES [dbo].[AuthenticationIdentity] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Employee_AuthenticationIdentity]') AND parent_object_id = OBJECT_ID(N'[dbo].[Employee]'))
ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_Employee_AuthenticationIdentity]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Employee_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[Employee]'))
ALTER TABLE [dbo].[Employee]  WITH CHECK ADD  CONSTRAINT [FK_Employee_Organization] FOREIGN KEY([OrganizationId])
REFERENCES [dbo].[Organization] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Employee_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[Employee]'))
ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_Employee_Organization]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Employee_ContactInformation_ContactInformation]') AND parent_object_id = OBJECT_ID(N'[dbo].[Employee_ContactInformation]'))
ALTER TABLE [dbo].[Employee_ContactInformation]  WITH CHECK ADD  CONSTRAINT [FK_Employee_ContactInformation_ContactInformation] FOREIGN KEY([ContactInformationId])
REFERENCES [dbo].[ContactInformation] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Employee_ContactInformation_ContactInformation]') AND parent_object_id = OBJECT_ID(N'[dbo].[Employee_ContactInformation]'))
ALTER TABLE [dbo].[Employee_ContactInformation] CHECK CONSTRAINT [FK_Employee_ContactInformation_ContactInformation]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Employee_ContactInformation_Employee]') AND parent_object_id = OBJECT_ID(N'[dbo].[Employee_ContactInformation]'))
ALTER TABLE [dbo].[Employee_ContactInformation]  WITH CHECK ADD  CONSTRAINT [FK_Employee_ContactInformation_Employee] FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[Employee] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Employee_ContactInformation_Employee]') AND parent_object_id = OBJECT_ID(N'[dbo].[Employee_ContactInformation]'))
ALTER TABLE [dbo].[Employee_ContactInformation] CHECK CONSTRAINT [FK_Employee_ContactInformation_Employee]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Medication_Measure]') AND parent_object_id = OBJECT_ID(N'[dbo].[Medication]'))
ALTER TABLE [dbo].[Medication]  WITH CHECK ADD  CONSTRAINT [FK_Medication_Measure] FOREIGN KEY([MeasureId])
REFERENCES [dbo].[Measure] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Medication_Measure]') AND parent_object_id = OBJECT_ID(N'[dbo].[Medication]'))
ALTER TABLE [dbo].[Medication] CHECK CONSTRAINT [FK_Medication_Measure]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Medication_MedicationBrand]') AND parent_object_id = OBJECT_ID(N'[dbo].[Medication]'))
ALTER TABLE [dbo].[Medication]  WITH CHECK ADD  CONSTRAINT [FK_Medication_MedicationBrand] FOREIGN KEY([MedicationBrandId])
REFERENCES [dbo].[MedicationBrand] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Medication_MedicationBrand]') AND parent_object_id = OBJECT_ID(N'[dbo].[Medication]'))
ALTER TABLE [dbo].[Medication] CHECK CONSTRAINT [FK_Medication_MedicationBrand]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Medication_MedicationType]') AND parent_object_id = OBJECT_ID(N'[dbo].[Medication]'))
ALTER TABLE [dbo].[Medication]  WITH CHECK ADD  CONSTRAINT [FK_Medication_MedicationType] FOREIGN KEY([MedicationTypeId])
REFERENCES [dbo].[MedicationType] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Medication_MedicationType]') AND parent_object_id = OBJECT_ID(N'[dbo].[Medication]'))
ALTER TABLE [dbo].[Medication] CHECK CONSTRAINT [FK_Medication_MedicationType]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MedicationSchedule_Patient]') AND parent_object_id = OBJECT_ID(N'[dbo].[MedicationSchedule]'))
ALTER TABLE [dbo].[MedicationSchedule]  WITH CHECK ADD  CONSTRAINT [FK_MedicationSchedule_Patient] FOREIGN KEY([PatientId])
REFERENCES [dbo].[Patient] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MedicationSchedule_Patient]') AND parent_object_id = OBJECT_ID(N'[dbo].[MedicationSchedule]'))
ALTER TABLE [dbo].[MedicationSchedule] CHECK CONSTRAINT [FK_MedicationSchedule_Patient]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MedicationScheduleComplete_Employee]') AND parent_object_id = OBJECT_ID(N'[dbo].[MedicationScheduleComplete]'))
ALTER TABLE [dbo].[MedicationScheduleComplete]  WITH CHECK ADD  CONSTRAINT [FK_MedicationScheduleComplete_Employee] FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[Employee] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MedicationScheduleComplete_Employee]') AND parent_object_id = OBJECT_ID(N'[dbo].[MedicationScheduleComplete]'))
ALTER TABLE [dbo].[MedicationScheduleComplete] CHECK CONSTRAINT [FK_MedicationScheduleComplete_Employee]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MedicationScheduleComplete_MedicationSchedule]') AND parent_object_id = OBJECT_ID(N'[dbo].[MedicationScheduleComplete]'))
ALTER TABLE [dbo].[MedicationScheduleComplete]  WITH CHECK ADD  CONSTRAINT [FK_MedicationScheduleComplete_MedicationSchedule] FOREIGN KEY([MedicationScheduleId])
REFERENCES [dbo].[MedicationSchedule] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MedicationScheduleComplete_MedicationSchedule]') AND parent_object_id = OBJECT_ID(N'[dbo].[MedicationScheduleComplete]'))
ALTER TABLE [dbo].[MedicationScheduleComplete] CHECK CONSTRAINT [FK_MedicationScheduleComplete_MedicationSchedule]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MedicationScheduleItem_Medication]') AND parent_object_id = OBJECT_ID(N'[dbo].[MedicationScheduleItem]'))
ALTER TABLE [dbo].[MedicationScheduleItem]  WITH CHECK ADD  CONSTRAINT [FK_MedicationScheduleItem_Medication] FOREIGN KEY([MedicationId])
REFERENCES [dbo].[Medication] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MedicationScheduleItem_Medication]') AND parent_object_id = OBJECT_ID(N'[dbo].[MedicationScheduleItem]'))
ALTER TABLE [dbo].[MedicationScheduleItem] CHECK CONSTRAINT [FK_MedicationScheduleItem_Medication]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MedicationScheduleItem_MedicationSchedule]') AND parent_object_id = OBJECT_ID(N'[dbo].[MedicationScheduleItem]'))
ALTER TABLE [dbo].[MedicationScheduleItem]  WITH CHECK ADD  CONSTRAINT [FK_MedicationScheduleItem_MedicationSchedule] FOREIGN KEY([MedicationScheduleId])
REFERENCES [dbo].[MedicationSchedule] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MedicationScheduleItem_MedicationSchedule]') AND parent_object_id = OBJECT_ID(N'[dbo].[MedicationScheduleItem]'))
ALTER TABLE [dbo].[MedicationScheduleItem] CHECK CONSTRAINT [FK_MedicationScheduleItem_MedicationSchedule]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MedicationScheduleItemHistory_MedicationScheduleItem]') AND parent_object_id = OBJECT_ID(N'[dbo].[MedicationScheduleItemHistory]'))
ALTER TABLE [dbo].[MedicationScheduleItemHistory]  WITH CHECK ADD  CONSTRAINT [FK_MedicationScheduleItemHistory_MedicationScheduleItem] FOREIGN KEY([MedicationScheduleItemId])
REFERENCES [dbo].[MedicationScheduleItem] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MedicationScheduleItemHistory_MedicationScheduleItem]') AND parent_object_id = OBJECT_ID(N'[dbo].[MedicationScheduleItemHistory]'))
ALTER TABLE [dbo].[MedicationScheduleItemHistory] CHECK CONSTRAINT [FK_MedicationScheduleItemHistory_MedicationScheduleItem]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MedicationScheduleItemHistoryEmployee_MedicationScheduleItemHistory]') AND parent_object_id = OBJECT_ID(N'[dbo].[MedicationScheduleItemHistoryEmployee]'))
ALTER TABLE [dbo].[MedicationScheduleItemHistoryEmployee]  WITH CHECK ADD  CONSTRAINT [FK_MedicationScheduleItemHistoryEmployee_MedicationScheduleItemHistory] FOREIGN KEY([MedicationScheduleItemHistoryId])
REFERENCES [dbo].[MedicationScheduleItemHistory] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MedicationScheduleItemHistoryEmployee_MedicationScheduleItemHistory]') AND parent_object_id = OBJECT_ID(N'[dbo].[MedicationScheduleItemHistoryEmployee]'))
ALTER TABLE [dbo].[MedicationScheduleItemHistoryEmployee] CHECK CONSTRAINT [FK_MedicationScheduleItemHistoryEmployee_MedicationScheduleItemHistory]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MedicationScheduleItemRepeat_MedicationScheduleItem]') AND parent_object_id = OBJECT_ID(N'[dbo].[MedicationScheduleItemRepeat]'))
ALTER TABLE [dbo].[MedicationScheduleItemRepeat]  WITH CHECK ADD  CONSTRAINT [FK_MedicationScheduleItemRepeat_MedicationScheduleItem] FOREIGN KEY([MedicationScheduleItemId])
REFERENCES [dbo].[MedicationScheduleItem] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MedicationScheduleItemRepeat_MedicationScheduleItem]') AND parent_object_id = OBJECT_ID(N'[dbo].[MedicationScheduleItemRepeat]'))
ALTER TABLE [dbo].[MedicationScheduleItemRepeat] CHECK CONSTRAINT [FK_MedicationScheduleItemRepeat_MedicationScheduleItem]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Patient_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[Patient]'))
ALTER TABLE [dbo].[Patient]  WITH CHECK ADD  CONSTRAINT [FK_Patient_Organization] FOREIGN KEY([OrganizationId])
REFERENCES [dbo].[Organization] ([Id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Patient_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[Patient]'))
ALTER TABLE [dbo].[Patient] CHECK CONSTRAINT [FK_Patient_Organization]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Patient_ContactInformation_ContactInformation]') AND parent_object_id = OBJECT_ID(N'[dbo].[Patient_ContactInformation]'))
ALTER TABLE [dbo].[Patient_ContactInformation]  WITH CHECK ADD  CONSTRAINT [FK_Patient_ContactInformation_ContactInformation] FOREIGN KEY([ContactInformationId])
REFERENCES [dbo].[ContactInformation] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Patient_ContactInformation_ContactInformation]') AND parent_object_id = OBJECT_ID(N'[dbo].[Patient_ContactInformation]'))
ALTER TABLE [dbo].[Patient_ContactInformation] CHECK CONSTRAINT [FK_Patient_ContactInformation_ContactInformation]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Patient_ContactInformation_Patient]') AND parent_object_id = OBJECT_ID(N'[dbo].[Patient_ContactInformation]'))
ALTER TABLE [dbo].[Patient_ContactInformation]  WITH CHECK ADD  CONSTRAINT [FK_Patient_ContactInformation_Patient] FOREIGN KEY([PatientId])
REFERENCES [dbo].[Patient] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Patient_ContactInformation_Patient]') AND parent_object_id = OBJECT_ID(N'[dbo].[Patient_ContactInformation]'))
ALTER TABLE [dbo].[Patient_ContactInformation] CHECK CONSTRAINT [FK_Patient_ContactInformation_Patient]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PatientContactPersonRelationship_Employee]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientContactPersonRelationship]'))
ALTER TABLE [dbo].[PatientContactPersonRelationship]  WITH CHECK ADD  CONSTRAINT [FK_PatientContactPersonRelationship_Employee] FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[Employee] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PatientContactPersonRelationship_Employee]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientContactPersonRelationship]'))
ALTER TABLE [dbo].[PatientContactPersonRelationship] CHECK CONSTRAINT [FK_PatientContactPersonRelationship_Employee]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PatientContactPersonRelationship_Patient]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientContactPersonRelationship]'))
ALTER TABLE [dbo].[PatientContactPersonRelationship]  WITH CHECK ADD  CONSTRAINT [FK_PatientContactPersonRelationship_Patient] FOREIGN KEY([PatientId])
REFERENCES [dbo].[Patient] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PatientContactPersonRelationship_Patient]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientContactPersonRelationship]'))
ALTER TABLE [dbo].[PatientContactPersonRelationship] CHECK CONSTRAINT [FK_PatientContactPersonRelationship_Patient]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PatientContactPersonRelationshipEnded_PatientContactPersonRelationship]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientContactPersonRelationshipEnded]'))
ALTER TABLE [dbo].[PatientContactPersonRelationshipEnded]  WITH CHECK ADD  CONSTRAINT [FK_PatientContactPersonRelationshipEnded_PatientContactPersonRelationship] FOREIGN KEY([PatientContactPersonRelationshipId])
REFERENCES [dbo].[PatientContactPersonRelationship] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PatientContactPersonRelationshipEnded_PatientContactPersonRelationship]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientContactPersonRelationshipEnded]'))
ALTER TABLE [dbo].[PatientContactPersonRelationshipEnded] CHECK CONSTRAINT [FK_PatientContactPersonRelationshipEnded_PatientContactPersonRelationship]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PatientDescription_Description]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientDescription]'))
ALTER TABLE [dbo].[PatientDescription]  WITH CHECK ADD  CONSTRAINT [FK_PatientDescription_Description] FOREIGN KEY([DescriptionId])
REFERENCES [dbo].[Description] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PatientDescription_Description]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientDescription]'))
ALTER TABLE [dbo].[PatientDescription] CHECK CONSTRAINT [FK_PatientDescription_Description]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PatientDescription_Patient]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientDescription]'))
ALTER TABLE [dbo].[PatientDescription]  WITH CHECK ADD  CONSTRAINT [FK_PatientDescription_Patient] FOREIGN KEY([PatientId])
REFERENCES [dbo].[Patient] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PatientDescription_Patient]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientDescription]'))
ALTER TABLE [dbo].[PatientDescription] CHECK CONSTRAINT [FK_PatientDescription_Patient]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PatientPost_Employee]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientPost]'))
ALTER TABLE [dbo].[PatientPost]  WITH CHECK ADD  CONSTRAINT [FK_PatientPost_Employee] FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[Employee] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PatientPost_Employee]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientPost]'))
ALTER TABLE [dbo].[PatientPost] CHECK CONSTRAINT [FK_PatientPost_Employee]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PatientPost_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientPost]'))
ALTER TABLE [dbo].[PatientPost]  WITH CHECK ADD  CONSTRAINT [FK_PatientPost_Organization] FOREIGN KEY([OrganizationId])
REFERENCES [dbo].[Organization] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PatientPost_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientPost]'))
ALTER TABLE [dbo].[PatientPost] CHECK CONSTRAINT [FK_PatientPost_Organization]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PatientPost_Patient_Patient]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientPost_Patient]'))
ALTER TABLE [dbo].[PatientPost_Patient]  WITH CHECK ADD  CONSTRAINT [FK_PatientPost_Patient_Patient] FOREIGN KEY([PatientId])
REFERENCES [dbo].[Patient] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PatientPost_Patient_Patient]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientPost_Patient]'))
ALTER TABLE [dbo].[PatientPost_Patient] CHECK CONSTRAINT [FK_PatientPost_Patient_Patient]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PatientPost_Patient_PatientPost]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientPost_Patient]'))
ALTER TABLE [dbo].[PatientPost_Patient]  WITH CHECK ADD  CONSTRAINT [FK_PatientPost_Patient_PatientPost] FOREIGN KEY([PatientPostId])
REFERENCES [dbo].[PatientPost] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PatientPost_Patient_PatientPost]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientPost_Patient]'))
ALTER TABLE [dbo].[PatientPost_Patient] CHECK CONSTRAINT [FK_PatientPost_Patient_PatientPost]
GO
