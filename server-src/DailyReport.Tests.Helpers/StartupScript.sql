INSERT INTO Organization (Name, TimeCreated) VALUES ('Olivers bosted', '8/16/2014 2:48:44 PM');
INSERT INTO Organization (Name, TimeCreated) VALUES ('Olivers bosted 2', CURRENT_TIMESTAMP);
INSERT INTO Organization (Name, TimeCreated) VALUES ('Olivers bosted 3', CURRENT_TIMESTAMP);
INSERT INTO Organization (Name, TimeCreated) VALUES ('Olivers bosted 4', CURRENT_TIMESTAMP);
INSERT INTO Organization (Name, TimeCreated) VALUES ('Olivers bosted 5', CURRENT_TIMESTAMP);

INSERT INTO AuthenticationIdentity (Username, PasswordHash, IsActive) VALUES ('ola', '123123qwe!', 1);
INSERT INTO Employee (OrganizationId, Firstname, Lastname, TimeCreated, AuthenticationId) VALUES(1, 'Oliver', 'Lassen', '8/16/2014 2:48:44 PM', 1); 

INSERT INTO AuthenticationIdentity (Username, PasswordHash, IsActive) VALUES ('dit', '123123qwe!', 1);
INSERT INTO Employee (OrganizationId, Firstname, Lastname, TimeCreated, AuthenticationId) VALUES(1, 'Ditte', 'Ruusunen', '8/16/2014 2:48:44 PM', 2);

INSERT INTO AuthenticationIdentity (Username, PasswordHash, IsActive) VALUES ('and', '123123qwe!', 1);
INSERT INTO Employee (OrganizationId, Firstname, Lastname, TimeCreated, AuthenticationId) VALUES(2, 'Anders', 'Lassen', CURRENT_TIMESTAMP, 3);

INSERT INTO AuthenticationIdentity (Username, PasswordHash, IsActive) VALUES ('fre', '123123qwe!', 1);
INSERT INTO Employee (OrganizationId, Firstname, Lastname, TimeCreated, AuthenticationId) VALUES(1, 'Frederik', 'Kaspersen', CURRENT_TIMESTAMP, 4); -- Change update active state

INSERT INTO AuthenticationIdentity (Username, PasswordHash, IsActive) VALUES ('kas', '123123qwe!', 1);
INSERT INTO Employee (OrganizationId, Firstname, Lastname, TimeCreated, IsActive, AuthenticationId) VALUES(1, 'Kasper', 'Kaspersen', CURRENT_TIMESTAMP, 0, 5); -- Inactive

INSERT INTO Patient (OrganizationId, Firstname, Lastname, TimeCreated, BirthDate, IsActive) VALUES (1, 'Oliver', 'Lassen', CURRENT_TIMESTAMP, '06/19/1993', 1);
INSERT INTO Patient (OrganizationId, Firstname, Lastname, TimeCreated, BirthDate, IsActive) VALUES (1, 'Ditte', 'Ruusunen', CURRENT_TIMESTAMP, '07/04/1993', 0);
INSERT INTO Patient (OrganizationId, Firstname, Lastname, TimeCreated, BirthDate, IsActive) VALUES (4, 'Firstname', 'Lastname', CURRENT_TIMESTAMP, '06/19/1993', 1); -- GetById()
INSERT INTO Patient (OrganizationId, Firstname, Lastname, TimeCreated, BirthDate, IsActive) VALUES (5, 'Firstname', 'Lastname', CURRENT_TIMESTAMP, '06/19/1993', 1); -- Deactivate()
INSERT INTO Patient (OrganizationId, Firstname, Lastname, TimeCreated, BirthDate, IsActive) VALUES (1, 'Firstname', 'Lastname', CURRENT_TIMESTAMP, '06/19/1993', 0);

INSERT INTO PatientContactPersonRelationship (PatientId, EmployeeId, TimeCreated) VALUES (1, 2, CURRENT_TIMESTAMP);

INSERT INTO PatientContactPersonRelationship (PatientId, EmployeeId, TimeCreated) VALUES (2, 2, CURRENT_TIMESTAMP);
INSERT INTO PatientContactPersonRelationship (PatientId, EmployeeId, TimeCreated) VALUES (2, 4, CURRENT_TIMESTAMP);


INSERT INTO PatientPost (OrganizationId, EmployeeId, TimeCreated, [Description]) VALUES (1, 1, CURRENT_TIMESTAMP, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consectetur, lectus vitae aliquam ullamcorper, velit ante rhoncus libero, vitae suscipit ex orci vel sem. Nulla porttitor nisi maximus ex tincidunt euismod. Aenean iaculis iaculis urna, vitae dapibus sem rutrum vel. Ut fringilla pharetra maximus. Maecenas ex odio, dignissim eget augue eu, mollis vestibulum ligula. Nulla posuere cursus diam, ut consequat purus luctus a. Donec eu malesuada ipsum, gravida lacinia lectus. Mauris nec sapien consectetur, blandit augue rh');
INSERT INTO PatientPost (OrganizationId, EmployeeId, TimeCreated, [Description]) VALUES (1, 1, CURRENT_TIMESTAMP, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consectetur, lectus vitae aliquam ullamcorper, velit ante rhoncus libero, vitae suscipit ex orci vel sem. Nulla porttitor nisi maximus ex tincidunt euismod. Aenean iaculis iaculis urna, vitae dapibus sem rutrum vel. Ut fringilla pharetra maximus. Maecenas ex odio, dignissim eget augue eu, mollis vestibulum ligula. Nulla posuere cursus diam, ut consequat purus luctus a. Donec eu malesuada ipsum, gravida lacinia lectus. Mauris nec sapien consectetur, blandit augue rh');
INSERT INTO PatientPost (OrganizationId, EmployeeId, TimeCreated, [Description]) VALUES (1, 2, CURRENT_TIMESTAMP, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consectetur, lectus vitae aliquam ullamcorper, velit ante rhoncus libero, vitae suscipit ex orci vel sem. Nulla porttitor nisi maximus ex tincidunt euismod. Aenean iaculis iaculis urna, vitae dapibus sem rutrum vel. Ut fringilla pharetra maximus. Maecenas ex odio, dignissim eget augue eu, mollis vestibulum ligula. Nulla posuere cursus diam, ut consequat purus luctus a. Donec eu malesuada ipsum, gravida lacinia lectus. Mauris nec sapien consectetur, blandit augue rh');

INSERT INTO PatientPost (OrganizationId, EmployeeId, TimeCreated, [Description]) VALUES (2, 3, CURRENT_TIMESTAMP, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consectetur, lectus vitae aliquam ullamcorper, velit ante rhoncus libero, vitae suscipit ex orci vel sem. Nulla porttitor nisi maximus ex tincidunt euismod. Aenean iaculis iaculis urna, vitae dapibus sem rutrum vel. Ut fringilla pharetra maximus. Maecenas ex odio, dignissim eget augue eu, mollis vestibulum ligula. Nulla posuere cursus diam, ut consequat purus luctus a. Donec eu malesuada ipsum, gravida lacinia lectus. Mauris nec sapien consectetur, blandit augue rh');
INSERT INTO PatientPost (OrganizationId, EmployeeId, TimeCreated, [Description]) VALUES (2, 3, CURRENT_TIMESTAMP, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consectetur, lectus vitae aliquam ullamcorper, velit ante rhoncus libero, vitae suscipit ex orci vel sem. Nulla porttitor nisi maximus ex tincidunt euismod. Aenean iaculis iaculis urna, vitae dapibus sem rutrum vel. Ut fringilla pharetra maximus. Maecenas ex odio, dignissim eget augue eu, mollis vestibulum ligula. Nulla posuere cursus diam, ut consequat purus luctus a. Donec eu malesuada ipsum, gravida lacinia lectus. Mauris nec sapien consectetur, blandit augue rh');
INSERT INTO PatientPost (OrganizationId, EmployeeId, TimeCreated, [Description]) VALUES (2, 3, CURRENT_TIMESTAMP, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consectetur, lectus vitae aliquam ullamcorper, velit ante rhoncus libero, vitae suscipit ex orci vel sem. Nulla porttitor nisi maximus ex tincidunt euismod. Aenean iaculis iaculis urna, vitae dapibus sem rutrum vel. Ut fringilla pharetra maximus. Maecenas ex odio, dignissim eget augue eu, mollis vestibulum ligula. Nulla posuere cursus diam, ut consequat purus luctus a. Donec eu malesuada ipsum, gravida lacinia lectus. Mauris nec sapien consectetur, blandit augue rh');
INSERT INTO PatientPost (OrganizationId, EmployeeId, TimeCreated, [Description]) VALUES (2, 3, CURRENT_TIMESTAMP, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consectetur, lectus vitae aliquam ullamcorper, velit ante rhoncus libero, vitae suscipit ex orci vel sem. Nulla porttitor nisi maximus ex tincidunt euismod. Aenean iaculis iaculis urna, vitae dapibus sem rutrum vel. Ut fringilla pharetra maximus. Maecenas ex odio, dignissim eget augue eu, mollis vestibulum ligula. Nulla posuere cursus diam, ut consequat purus luctus a. Donec eu malesuada ipsum, gravida lacinia lectus. Mauris nec sapien consectetur, blandit augue rh');

INSERT INTO PatientPost_Patient(PatientPostId, PatientId) VALUES (2, 1);
INSERT INTO PatientPost_Patient(PatientPostId, PatientId) VALUES (3, 2);
INSERT INTO PatientPost_Patient(PatientPostId, PatientId) VALUES (4, 3);
INSERT INTO PatientPost_Patient(PatientPostId, PatientId) VALUES (5, 3);
INSERT INTO PatientPost_Patient(PatientPostId, PatientId) VALUES (6, 3);

INSERT INTO Measure (Name) VALUES ('ml');
INSERT INTO Measure (Name) VALUES ('mg');
INSERT INTO Measure (Name) VALUES ('g');

INSERT INTO MedicationType (Name) VALUES ('Tabletter');
INSERT INTO MedicationType (Name) VALUES ('Væske');
INSERT INTO MedicationType (Name) VALUES ('Indsprøjtning');

INSERT INTO MedicationBrand (Name) VALUES ('Novo Nordisk'); -- GetById (1)

INSERT INTO Medication (Name, MedicationBrandId, MedicationTypeId, MeasureId, Weight) VALUES ('Panodil', 1, 1, 2, 2.5); -- GetById (1)
INSERT INTO Medication (Name, MedicationBrandId, MedicationTypeId, MeasureId, Weight) VALUES ('Panodil Zapp', 1, 1, 2, 5); -- GetById (2)
INSERT INTO Medication (Name, MedicationBrandId, MedicationTypeId, MeasureId, Weight) VALUES ('Panodil Morgen', 1, 2, 1, 500); -- GetById (3)
INSERT INTO Medication (Name, MedicationBrandId, MedicationTypeId, MeasureId, Weight) VALUES ('Panodil Aften', 1, 3, 1, 200); -- GetById (4)

INSERT INTO MedicationSchedule (PatientId, TimeCreated, StartDate) VALUES (1, '8/16/2014 2:48:44 PM', '8/16/2014 2:48:44 PM');
INSERT INTO MedicationSchedule (PatientId, TimeCreated, StartDate) VALUES (1, '8/16/2014 2:48:44 PM', '8/16/2014 2:48:44 PM');
INSERT INTO MedicationSchedule (PatientId, TimeCreated, StartDate) VALUES (5, '8/16/2014 2:48:44 PM', '8/16/2014 2:48:44 PM');

INSERT INTO MedicationScheduleComplete (MedicationScheduleId, Description, TimeCreated, EmployeeId) VALUES (2, 'testString', '8/16/2014 2:48:44 PM', 1);

INSERT INTO MedicationScheduleItem (MedicationScheduleId, MedicationId, ExecuteTime, Amouth) VALUES (1, 4, CURRENT_TIMESTAMP, 1); 
INSERT INTO MedicationScheduleItem (MedicationScheduleId, MedicationId, ExecuteTime, Amouth) VALUES (1, 3, CURRENT_TIMESTAMP, 2); 
INSERT INTO MedicationScheduleItem (MedicationScheduleId, MedicationId, ExecuteTime, Amouth) VALUES (1, 2, CURRENT_TIMESTAMP, 0.5); 
INSERT INTO MedicationScheduleItem (MedicationScheduleId, MedicationId, ExecuteTime, Amouth) VALUES (1, 1, CURRENT_TIMESTAMP, 1); 
INSERT INTO MedicationScheduleItem (MedicationScheduleId, MedicationId, ExecuteTime, Amouth) VALUES (2, 1, '8/16/2014 2:48:44 PM', 1); 
INSERT INTO MedicationScheduleItem (MedicationScheduleId, MedicationId, ExecuteTime, Amouth) VALUES (3, 1, CURRENT_TIMESTAMP, 1); 


INSERT INTO MedicationScheduleItemRepeat (MedicationScheduleItemId, RepeatTimeSpan) VALUES (1, 1409771469);
INSERT INTO MedicationScheduleItemRepeat (MedicationScheduleItemId, RepeatTimeSpan) VALUES (2, 1409771469);

INSERT INTO MedicationScheduleItemHistory (MedicationScheduleItemId, TimeCreated) VALUES (1, CURRENT_TIMESTAMP);


INSERT INTO ContactInformationType (TypeName) VALUES ('Mobil');
INSERT INTO ContactInformationType (TypeName) VALUES ('E-mail');
INSERT INTO ContactInformationType (TypeName) VALUES ('Hjemmeside');

INSERT INTO ContactInformation (ContactInformationTypeId, Data, TimeCreated) VALUES (1, '+45 27150869', CURRENT_TIMESTAMP);
INSERT INTO ContactInformation (ContactInformationTypeId, Data, TimeCreated) VALUES (2, 'privat.oliver.lassen@gmail.com', CURRENT_TIMESTAMP);
INSERT INTO ContactInformation (ContactInformationTypeId, Data, TimeCreated) VALUES (3, 'http://google.com', CURRENT_TIMESTAMP);

INSERT INTO Employee_ContactInformation (ContactInformationId, EmployeeId) VALUES (3, 1);

INSERT INTO Patient_ContactInformation (ContactInformationId, PatientId) VALUES (1, 1);
INSERT INTO Patient_ContactInformation (ContactInformationId, PatientId) VALUES (2, 1);