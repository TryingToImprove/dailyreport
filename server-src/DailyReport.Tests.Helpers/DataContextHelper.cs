﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.DataAccess;
using DailyReport.Domain.Infrastructure;

namespace DailyReport.Tests.Helpers
{
    public static class DataContextHelper
    {
        public static IDataContext Create(IDbConnection connection)
        {
            return new DataContext(connection)
            {
                Medications = new MedicationRepository(connection),
                MedicationBrands = new MedicationBrandRepository(connection),
                Measures = new MeasureRepository(connection),
                MedicationTypes = new MedicationTypeRepository(connection),
                Employees = new EmployeeRepository(connection),
                Patients = new PatientRepository(connection),
                Organizations = new OrganizationRepository(connection),
                PatientContactPersonRelationships = new PatientContactPersonRelationshipRepository(connection),
                PatientPostToPatients = new PatientToPatientPostRepository(connection),
                PatientPosts = new PatientPostRepository(connection)
            };
        }
    }
}
