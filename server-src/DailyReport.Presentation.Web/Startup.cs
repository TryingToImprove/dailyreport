﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DailyReport.Presentation.Web.Startup))]
namespace DailyReport.Presentation.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
