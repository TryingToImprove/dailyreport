﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DailyReport.Presentation.Web.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Ninject;

namespace DailyReport.Presentation.Web.Authentication
{
    // Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.
    public class ApplicationUserManager : UserManager<ApplicationUser, int>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser, int> store)
            : base(store)
        {
        }

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        {
            var kernel = context.Get<IKernel>();
            var manager = new ApplicationUserManager(kernel.Get<EmployeeStore>());

            return manager;
        }
    }
}