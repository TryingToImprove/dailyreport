﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Models.Core;
using DailyReport.Presentation.Web.Models;
using Microsoft.AspNet.Identity;

namespace DailyReport.Presentation.Web.Authentication
{
    public class EmployeeStore : IUserStore<ApplicationUser, int>
    {
        private readonly IEmployeeService _employeeService;

        public EmployeeStore(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }
        
        public async Task<ApplicationUser> FindAsync(string userName, string password)
        {
            var authenticationResult = _employeeService.Authenticate(userName, password);

            if (authenticationResult == null)
                return null;

            return new ApplicationUser()
            {
                Id = authenticationResult.User.Id,
                UserName = authenticationResult.Identity.Username,
                Firstname = authenticationResult.User.Firstname,
                Lastname = authenticationResult.User.Lastname,
                OrganizationId = authenticationResult.User.OrganizationId
            };
        }

        public Task CreateAsync(ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public Task DeleteAsync(ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public Task<ApplicationUser> FindByIdAsync(int userId)
        {
            throw new NotImplementedException();
        }

        public async Task<ApplicationUser> FindByNameAsync(string userName)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync(ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}