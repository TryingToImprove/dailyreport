using DailyReport.DependencyInjection;
using DailyReport.Presentation.Web.Authentication;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(DailyReport.Presentation.Web.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(DailyReport.Presentation.Web.App_Start.NinjectWebCommon), "Stop")]

namespace DailyReport.Presentation.Web.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using System.Data.SqlClient;
    using DailyReport.Domain.Infrastructure.Repositories;
    using DailyReport.Domain.Infrastructure.Services;
    using DailyReport.DataAccess;
    using DailyReport.Services;
    using DailyReport.Domain.Infrastructure;
    using System.Data;

    public static class NinjectWebCommon
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();
        private static IKernel _kernel;

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        public static IKernel CreateKernel()
        {
            if (_kernel != null)
                return _kernel;

            _kernel = new StandardKernel();

            try
            {
                _kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                _kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(_kernel);
                return _kernel;
            }
            catch
            {
                _kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<IDbConnection>()
                .ToMethod(x => new SqlConnection("Server=.\\SQLEXPRESS;Database=DailyReport;Trusted_Connection=True;"))
                .InRequestScope();

            IoC.RegisterServices(kernel);

            // Store profiles (authentication)
            kernel.Bind<EmployeeStore>().ToMethod(x => new EmployeeStore(kernel.Get<IEmployeeService>()));
        }
    }
}
