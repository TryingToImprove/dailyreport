﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using DailyReport.Presentation.Web.Mapping;

namespace DailyReport.Presentation.Web.App_Start
{
    public static class AutomapperWebConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(configuration =>
            {
                // Configure profiles
                configuration.AddProfile(new MedicationProfile());
                configuration.AddProfile(new MedicationScheduleProfile());
                configuration.AddProfile(new ContactInformationProfile());
                configuration.AddProfile(new ContactbookProfile());
                configuration.AddProfile(new EmployeeProfile());
                configuration.AddProfile(new PatientProfile());
            });
        }
    }
}