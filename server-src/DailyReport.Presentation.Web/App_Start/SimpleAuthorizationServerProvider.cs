using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Presentation.Web.App_Start;
using DailyReport.Presentation.Web.Authentication;
using DailyReport.Presentation.Web.Extensions;
using Microsoft.Owin.Security.OAuth;
using Ninject;

namespace DailyReport.Presentation.Web
{
    public class SimpleAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        private readonly IOrganizationService _organizationService;

        public SimpleAuthorizationServerProvider(IOrganizationService organizationService)
        {
            _organizationService = organizationService;
        }

        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var employeeStore = NinjectWebCommon.CreateKernel().Get<EmployeeStore>();
            var user = await employeeStore.FindAsync(context.UserName, context.Password);

            if (user == null)
            {
                context.SetError("invalid_grant", "The user name or password is incorrect.");
                return;
            }

            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            identity.AddClaim(new Claim("Firstname", user.Firstname));
            identity.AddClaim(new Claim("Lastname", user.Lastname));
            identity.AddClaim(new Claim("OrganizationId", user.OrganizationId.ToString()));
            identity.AddClaim(new Claim("EmployeeId", user.Id.ToString()));
            identity.AddClaim(new Claim("Employee", "FullAccess"));

            context.Validated(identity);
        }
    }
}