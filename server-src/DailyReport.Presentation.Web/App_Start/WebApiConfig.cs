﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;

namespace DailyReport.Presentation.Web
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            // Get formatters
            var formatters = GlobalConfiguration.Configuration.Formatters;

            // Remove XMLFormatter
            formatters.Remove(formatters.XmlFormatter);

            // We want camelCasing from JSON
            var formatter = formatters.JsonFormatter;
            var settings = formatter.SerializerSettings;
            settings.Formatting = Formatting.Indented;
            settings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            // Map routes
            config.Routes.MapHttpRoute(
                name: "PatientApi",
                routeTemplate: "api/patient/{patientId}/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
