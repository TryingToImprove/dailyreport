﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using DailyReport.Presentation.Web.Areas.AdministrationModule.Models;
using DailyReport.Presentation.Web.Models;

namespace DailyReport.Presentation.Web.Mapping
{
    public class MedicationProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<MedicationCreateViewModel, MedicationInsertModel>()
                .ForMember(x => x.Medication, x => x.MapFrom(y => new Medication
                {
                    Name = y.Name,
                    MedicationBrandId = GetMedicationBrandId(y),
                    MeasureId = y.MeasureId,
                    MedicationTypeId = y.MedicationTypeId,
                    Weight = y.Weight
                }))
                .ForMember(x => x.Brand, x => x.MapFrom(y => new MedicationBrand
                {
                    Id = GetMedicationBrandId(y),
                    Name = y.BrandName
                }));

            CreateMap<MedicationBrand, SelectListItem>()
                .ForMember(x => x.Value, x => x.MapFrom(y => y.Id.ToString(CultureInfo.InvariantCulture)))
                .ForMember(x => x.Text, x => x.MapFrom(y => y.Name));
        }

        private static int GetMedicationBrandId(MedicationCreateViewModel y)
        {
            return string.IsNullOrWhiteSpace(y.BrandName)
                ? y.BrandId.GetValueOrDefault(0)
                : 0;
        }
    }
}