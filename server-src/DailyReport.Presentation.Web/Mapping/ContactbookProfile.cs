﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using DailyReport.Presentation.Web.Areas.PatientModule.Models;
using DailyReport.Presentation.Web.Models;

namespace DailyReport.Presentation.Web.Mapping
{
    public class ContactbookProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<ContactbookAddViewModel, ContactbookInsertModel>()
                .ForMember(x => x.Firstname, x => x.MapFrom(y => y.Firstname))
                .ForMember(x => x.Lastname, x => x.MapFrom(y => y.Lastname))
                .ForMember(x => x.Type, x => x.MapFrom(y => y.Type))
                .ForMember(x => x.Groups, x => x.MapFrom(y => y.Groups));
        }
    }
}