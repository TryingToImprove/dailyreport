﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using DailyReport.Presentation.Web.Areas.Administration.Models;
using DailyReport.Presentation.Web.Areas.OrganizationModule.Models;
using DailyReport.Presentation.Web.Areas.PatientModule.Models;
using DailyReport.Presentation.Web.Extensions;
using DailyReport.Presentation.Web.Models;

namespace DailyReport.Presentation.Web.Mapping
{
    public class PatientProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<PatientCreateViewModel, PatientInsertModel>()
                .ForMember(x => x.Firstname, x => x.MapFrom(y => y.Firstname))
                .ForMember(x => x.Lastname, x => x.MapFrom(y => y.Lastname))
                .ForMember(x => x.BirthDate, x => x.MapFrom(y => y.BirthDate.GetValueOrDefault()))
                .ForMember(x => x.ContactPersons, x => x.MapFrom(y => y.ContactPersons))
                .ForMember(x => x.Description, x => x.MapFrom(y => y.Description))
                .ForMember(x => x.ContactInformations, x => x.ResolveUsing(y => y.ContactInformations != null ? y.ContactInformations.Select(z => new Domain.Models.ContactInformationModel { DataTypeId = z.DataTypeId, Data = z.Data }) : null))
                .ForMember(x => x.OrganizationId, x => x.MapFrom(y => ClaimsPrincipal.Current.GetOrganizationId()))
                .ForMember(x => x.CreatorEmployeeId, x => x.MapFrom(y => ClaimsPrincipal.Current.GetEmployeeId()));

            CreateMap<PatientUpdateViewModel, PatientUpdateModel>()
                .ForMember(x => x.Firstname, x => x.MapFrom(y => y.Firstname))
                .ForMember(x => x.Lastname, x => x.MapFrom(y => y.Lastname));
        }
    }
}