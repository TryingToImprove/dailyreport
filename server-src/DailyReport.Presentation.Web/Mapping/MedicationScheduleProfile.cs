﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using DailyReport.Presentation.Web.Models;
using DailyReport.Presentation.Web.Areas.Administration.Models;

namespace DailyReport.Presentation.Web.Mapping
{
    public class MedicationScheduleProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<MedicationScheduleViewModel.CreatePatientSchema, MedicationScheduleInsertModel>()
                .ForMember(x => x.StartDate, x => x.MapFrom(y => y.StartDate))
                .ForMember(x => x.PatientId, x => x.MapFrom(y => y.PatientId))
                .ForMember(x => x.Items, x => x.MapFrom(y => y.Items.Select(z => new MedicationScheduleItemInsertModel
                    {
                        MedicationId = z.MedicationId,
                        Amouth = z.Amouth,
                        ExecuteTime = z.ExecuteTime,
                        RepeatInterval = z.RepeatInterval
                    })));
        }
    }
}