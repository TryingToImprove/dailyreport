﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DailyReport.Presentation.Web.Models
{
    public abstract class MedicationViewModel
    {
        public class Search
        {
            [Required]
            public string Query { get; set; }
        }
    }
}