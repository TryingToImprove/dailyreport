﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DailyReport.Presentation.Web.Models
{
    public abstract class ContactInformationViewModel
    {
        public class Add
        {
            [Required]
            public string Type { get; set; }

            [Required]
            public string Value { get; set; }
        }
    }
}