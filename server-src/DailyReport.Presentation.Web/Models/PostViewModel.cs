﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DailyReport.Presentation.Web.Models
{
    public abstract class PostViewModel
    {
        public class Create
        {
            [Required]
            public IEnumerable<int> Patients { get; set; }

            [Required]
            public string Description { get; set; }
        }
    }
}