﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;

namespace DailyReport.Presentation.Web.Models
{
    public class AccountDetailsViewModel
    {
        public EmployeeResultModel Account { get; set; }

        public Organization Organization { get; set; }
    }
}