﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DailyReport.Presentation.Web.Models
{
    public abstract class ContactViewModel
    {
        public class Add
        {
            [Required]
            public string Firstname { get; set; }

            [Required]
            public string Lastname { get; set; }

            [Required]
            public IEnumerable<string> Groups { get; set; }

            [Required]
            public string Title { get; set; }

            public IEnumerable<ContactInformation> ContactInformations { get; set; }

            public class ContactInformation
            {
                [Required]
                public string Label { get; set; }

                [Required]
                public string Value { get; set; }
            }
        }
    }
}