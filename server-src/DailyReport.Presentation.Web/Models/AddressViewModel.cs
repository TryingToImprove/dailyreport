﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DailyReport.Presentation.Web.Models
{
    public abstract class AddressViewModel
    {
        public class Add
        {
            [Required]
            public string Street { get; set; }

            [Required]
            public string HouseNumber { get; set; }

            [Required]
            public string City { get; set; }

            [Required]
            public string PostalCode { get; set; }

            [Required]
            public bool IsPrimary { get; set; }

            [Required]
            public bool ClearCurrentPrimary { get; set; }
        }

        public class Update
        {
            public string Street { get; set; }

            public string HouseNumber { get; set; }

            public string City { get; set; }

            public string PostalCode { get; set; }

            public bool IsPrimary { get; set; }

            public bool ClearCurrentPrimary { get; set; }
        }

        public class Delete
        {
            [Required]
            public int AddressId { get; set; }
        }
    }
}