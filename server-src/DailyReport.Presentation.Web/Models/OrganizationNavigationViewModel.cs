﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DailyReport.Domain.Models.Core;

namespace DailyReport.Presentation.Web.Models
{
    public class OrganizationNavigationViewModel
    {
        public Organization Organization { get; set; }
    }
}