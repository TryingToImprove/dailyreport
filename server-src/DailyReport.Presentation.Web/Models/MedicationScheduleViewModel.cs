﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DailyReport.Presentation.Web.Models
{
    public abstract class MedicationScheduleViewModel
    {
        public class CreatePatientSchema
        {
            [Required]
            public int PatientId { get; set; }

            [Required]
            public DateTime StartDate { get; set; }

            public IList<ScheduleItem> Items { get; set; }

            public class ScheduleItem
            {
                [Required]
                public int MedicationId { get; set; }

                [Required]
                public double Amouth { get; set; }

                [Required]
                public TimeSpan ExecuteTime { get; set; }

                public TimeSpan? RepeatInterval { get; set; }
            }
        }

        public class Complete
        {
            [Required]
            public string Description { get; set; }
        }
    }
}