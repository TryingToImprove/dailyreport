﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DailyReport.Domain.Models.Core;

namespace DailyReport.Presentation.Web.Models
{
    public class CompleteAccountRegisteringViewModel
    {
        [Required]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public Employee Employee { get; set; }
    }
}