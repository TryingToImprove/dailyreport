using System.ComponentModel.DataAnnotations;

namespace DailyReport.Presentation.Web.Models
{
    public abstract class MedicationScheduleHistoryViewModel
    {
        public class Add
        {
            [Required]
            public int MedicationScheduleItemId { get; set; }
        }
    }
}