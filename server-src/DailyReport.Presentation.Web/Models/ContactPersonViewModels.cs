﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DailyReport.Presentation.Web.Models
{
    public class ContactPersonViewModels
    {
        public class Add
        {
            [Required]
            public int EmployeeId { get; set; }

            [Required]
            public bool CompletePrevious { get; set; }
        }

        public class Complete
        {
            [Required]
            public int Id { get; set; }
        }
    }
}