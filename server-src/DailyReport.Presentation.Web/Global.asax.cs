﻿using DailyReport.Domain.Models.Core;
using DailyReport.Presentation.Web.App_Start;
using DailyReport.Presentation.Web.Binders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace DailyReport.Presentation.Web
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            ModelBinders.Binders.Add(typeof(IEnumerable<Patient>), new PatientModelBinder());
            ModelBinders.Binders.Add(typeof(IEnumerable<string>), new StringSplitOnModelBinder());

            AutomapperWebConfiguration.Configure();
        }
    }
}
