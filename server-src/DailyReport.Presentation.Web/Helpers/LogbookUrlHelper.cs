﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DailyReport.Presentation.Web.Helpers
{
    public static class LogbookUrlHelper
    {
        private const string ModuleName = "Administration";

        public static string GetLookbookUrl(this UrlHelper url, params int[] patientIds)
        {
            return url.Action("Index", "Logbook", new { patientId = patientIds, Area = ModuleName });
        }
    }
}