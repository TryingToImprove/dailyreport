﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DailyReport.Presentation.Web.Helpers
{
    public static class EmployeeUrlHelper
    {
        private const string ModuleName = "OrganizationModule";

        public static string GetEmployeesUrl(this UrlHelper url)
        {
            return url.Action("Index", "Employee", new { Area = ModuleName });
        }

        public static string GetEmployeeCreateUrl(this UrlHelper url)
        {
            return url.Action("Create", "Employee", new { Area = ModuleName });
        }

        public static string GetEmployeeEditUrl(this UrlHelper url, int employeeId)
        {
            return url.Action("Edit", "Employee", new { Area = ModuleName, id = employeeId });
        }

        public static string GetEmployeeUrl(this UrlHelper url, int employeeId)
        {
            return url.Action("Edit", "Employee", new { Area = ModuleName, id = employeeId });
        }

        public static string GetEmployeeDeleteUrl(this UrlHelper url, int employeeId)
        {
            return url.Action("Delete", "Employee", new { Area = ModuleName, id = employeeId });
        }
    }
}