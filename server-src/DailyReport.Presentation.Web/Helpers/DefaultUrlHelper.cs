﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DailyReport.Presentation.Web.Helpers
{
    public static class DefaultUrlHelper
    {
        private const string ModuleName = "";

        public static string GetDashboardUrl(this UrlHelper url)
        {
            return url.Action("Index", "Dashboard", new { Area = ModuleName });
        }

    }
}