﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DailyReport.Presentation.Web.Helpers
{
    public static class ProfileUrlHelper
    {
        private const string ModuleName = "ProfileModule";

        public static string GetMyProfileUrl(this UrlHelper url)
        {
            return url.Action("Index", "Profile", new { Area = ModuleName });
        }

        public static string GetMyPatientsUrl(this UrlHelper url)
        {
            return url.Action("Index", "Patients", new { Area = ModuleName });
        }

        public static string GetMyContactInformationsUrl(this UrlHelper url)
        {
            return url.Action("Index", "ContactInformation", new { Area = ModuleName });
        }

        public static string GetMyContactInformationEditUrl(this UrlHelper url, int contactInformationId)
        {
            return url.Action("Edit", "ContactInformation", new { Area = ModuleName, id = contactInformationId });
        }
    }
}