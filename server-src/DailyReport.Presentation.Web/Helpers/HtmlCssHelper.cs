﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DailyReport.Presentation.Web.Helpers
{
    public static class HtmlCssHelper
    {
        public static string GetActiveCssClass(this HtmlHelper helper, string actionName)
        {
            var routeData = helper.ViewContext.RouteData.Values;

            if (String.Equals(actionName, routeData["action"] as string, StringComparison.InvariantCultureIgnoreCase))
                return "active";

            return null;
        }
    }
}