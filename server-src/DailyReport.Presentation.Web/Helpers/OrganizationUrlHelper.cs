﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DailyReport.Presentation.Web.Helpers
{
    public static class OrganizationUrlHelper
    {
        private const string ModuleName = "OrganizationModule";

        public static string GetOrganizationSettingsEditUrl(this UrlHelper url)
        {
            return url.Action("Edit", "Settings", new { Area = ModuleName });
        }
    }
}