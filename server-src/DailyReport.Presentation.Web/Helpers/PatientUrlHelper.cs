﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DailyReport.Presentation.Web.Helpers
{
    public static class PatientUrlHelper
    {
        private const string ModuleName = "PatientModule";

        public static string GetPatientUrl(this UrlHelper url, int patientId)
        {
            return url.Action("Index", "Dashboard", new { patientId, Area = ModuleName });
        }

        public static string GetPatientCreateUrl(this UrlHelper url)
        {
            return url.Action("Create", "Manage", new { Area = ModuleName });
        }

        public static string GetPatientEditUrl(this UrlHelper url, int patientId)
        {
            return url.Action("Index", "Manage", new { patientId, Area = ModuleName });
        }

        public static string GetPatientMedicationSchedulesUrl(this UrlHelper url, int patientId)
        {
            return url.Action("Index", "Medication", new { patientId, Area = ModuleName });
        }

        public static string GetPatientLogbookUrl(this UrlHelper url, int patientId)
        {
            return url.Action("Index", "Logbook", new { patientId, Area = ModuleName });
        }

        public static string GetPatientContactPersonsUrl(this UrlHelper url, int patientId)
        {
            return url.Action("Index", "ContactPerson", new { patientId, Area = ModuleName });
        }

        public static string GetPatientContactInformationUrl(this UrlHelper url, int patientId)
        {
            return url.Action("Index", "ContactInformation", new { patientId, Area = ModuleName });
        }

        public static string GetPatientContactInformationEditUrl(this UrlHelper url, int patientId, int contactInformationId)
        {
            return url.Action("Edit", "ContactInformation", new { patientId, Area = ModuleName, id = contactInformationId });
        }

        public static string GetPatientContactbookUrl(this UrlHelper url, int patientId)
        {
            return url.Action("Index", "Contactbook", new { patientId, Area = ModuleName });
        }

        public static string GetPatientContactbookContactUrl(this UrlHelper url, int patientId, int id)
        {
            return url.Action("Contact", "Contactbook", new { patientId, id, Area = ModuleName });
        }

        public static string GetPatientContactbookGroupUrl(this UrlHelper url, int patientId, int id)
        {
            return url.Action("Group", "Contactbook", new { patientId, id, Area = ModuleName });
        }

        public static string GetPatientContactbookAddUrl(this UrlHelper url, int patientId)
        {
            return url.Action("Add", "Contactbook", new { patientId, Area = ModuleName });
        }
    }
}