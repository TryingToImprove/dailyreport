﻿$(function () {
    // Setup select2 for selectors with a specific class
    $(".default-select2").select2();

    // Setup tagit selector with a specific class
    $(".default-tagit").tagit();
})