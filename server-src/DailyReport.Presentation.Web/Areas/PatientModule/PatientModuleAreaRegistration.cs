﻿using System.Web.Mvc;

namespace DailyReport.Presentation.Web.Areas.PatientModule
{
    public class PatientModuleAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PatientModule";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PatientModule_default",
                "Patient/{patientId}/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "PatientModule_Manage_default",
                "Patient/{action}/{id}",
                new { controller = "Manage", id = UrlParameter.Optional }
            );
        }
    }
}