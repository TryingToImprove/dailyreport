﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Models;
using DailyReport.Presentation.Web.Areas.PatientModule.Models;
using DailyReport.Presentation.Web.Extensions;

namespace DailyReport.Presentation.Web.Areas.PatientModule.Controllers
{
    public class ContactbookController : Controller
    {
        private readonly IPatientService _patientService;
        private readonly IOrganizationService _organizationService;
        private readonly IContactbookService _contactbookService;
        private readonly IContactInformationService _contactInformationService;

        public ContactbookController(IPatientService patientService, IOrganizationService organizationService, IContactbookService contactbookService, IContactInformationService contactInformationService)
        {
            _patientService = patientService;
            _organizationService = organizationService;
            _contactbookService = contactbookService;
            _contactInformationService = contactInformationService;
        }

        public ActionResult Index(int patientId)
        {
            var organization = _organizationService.GetById(ClaimsPrincipal.Current.GetOrganizationId());
            var patient = _patientService.GetById(patientId);

            return View(new ContactbookIndexViewModel
            {
                Organization = organization,
                Patient = patient,
                Groups = _patientService.Contacts.GetGroups(patient.Id),
                Contacts = _patientService.Contacts.FindAll(patient.Id)
            });
        }

        public ActionResult Contact(int patientId, int id)
        {
            var organization = _organizationService.GetById(ClaimsPrincipal.Current.GetOrganizationId());
            var patient = _patientService.GetById(patientId);
            var contact = _contactbookService.GetById(id);
            var contactInformations = _contactbookService.ContactInformations.FindAll(contact.Id);
            var contactInformationTypes = _contactInformationService.GetTypes();

            return View(new ContactbookContactViewModel
            {
                Organization = organization,
                Patient = patient,
                Contact = contact,
                ContactInformations = contactInformations,
                ContactInformationForm = new ContactInformationFormViewModel
                {
                    ContactInformationTypeList = contactInformationTypes.GetSelectList()
                }
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddGroup(int patientId, int id, string groupName)
        {
            using (var contactService = _contactbookService.GetContactService(id))
                contactService.AddGroup(groupName);

            return RedirectToAction("Contact", new { patientId, id });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RemoveGroup(int patientId, int id, string groupName)
        {
            using (var contactService = _contactbookService.GetContactService(id))
                contactService.RemoveGroup(groupName);

            return RedirectToAction("Contact", new { patientId, id });
        }

        public ActionResult Group(int patientId, int id)
        {
            var organization = _organizationService.GetById(ClaimsPrincipal.Current.GetOrganizationId());
            var patient = _patientService.GetById(patientId);
            var group = _contactbookService.GetGroup(id);
            var contacts = _patientService.Contacts.FindByGroup(patient.Id, group.Id);

            return View(new ContactbookGroupViewModel
            {
                Organization = organization,
                Patient = patient,
                Group = group,
                Contacts = contacts
            });
        }

        public ActionResult Add(int patientId)
        {
            var organization = _organizationService.GetById(ClaimsPrincipal.Current.GetOrganizationId());
            var patient = _patientService.GetById(patientId);

            return View(new ContactbookAddViewModel
            {
                Organization = organization,
                Patient = patient
            });
        }

        [System.Web.Mvc.HttpPost]
        public ActionResult Add(int patientId, ContactbookAddViewModel viewModel)
        {
            var organization = _organizationService.GetById(ClaimsPrincipal.Current.GetOrganizationId());
            var patient = _patientService.GetById(patientId);

            if (ModelState.IsValid)
            {
                var model = Mapper.Map<ContactbookInsertModel>(viewModel);
                _patientService.Contacts.Add(patient.Id, model);
            }

            viewModel.Organization = organization;
            viewModel.Patient = patient;
            return View(viewModel);
        }
    }
}