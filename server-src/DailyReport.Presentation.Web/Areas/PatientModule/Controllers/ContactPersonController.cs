﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Presentation.Web.Areas.PatientModule.Models;
using DailyReport.Presentation.Web.Extensions;

namespace DailyReport.Presentation.Web.Areas.PatientModule.Controllers
{
    public class ContactPersonController : Controller
    {
        private readonly IPatientService _patientService;
        private readonly IOrganizationService _organizationService;
        private readonly IEmployeeService _employeeService;

        public ContactPersonController(IPatientService patientService, IOrganizationService organizationService, IContactInformationService contactInformationService, IEmployeeService employeeService)
        {
            _patientService = patientService;
            _organizationService = organizationService;
            _employeeService = employeeService;
        }

        //public ActionResult Index(int patientId)
        //{
        //    var organization = _organizationService.GetById(ClaimsPrincipal.Current.GetOrganizationId());
        //    var patient = _patientService.GetById(patientId);
        //    var contactPersons = _patientService.ContactPersons.FindAll(patient.Id);
        //    var employees = _employeeService.FindAll(organization.Id)
        //        .Where(x => contactPersons.All(y => x.Id != y.Employee.Id || (x.Id == y.Employee.Id && y.IsEnded)));

        //    return View(new PatientContactPersonsViewModel
        //    {
        //        Organization = organization,
        //        Patient = patient,
        //        ContactPersons = contactPersons,
        //        EmployeesList = employees.GetSelectList()
        //    });
        //}

        //[HttpPost]
        //public ActionResult Index(int patientId, PatientContactPersonsViewModel viewModel)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        _patientService.ContactPersons.Add(patientId, viewModel.EmployeeId, viewModel.CompletePreviousContactPersons);

        //        return RedirectToAction("Index", new { Id = patientId });
        //    }

        //    var organization = _organizationService.GetById(ClaimsPrincipal.Current.GetOrganizationId());
        //    var patient = _patientService.GetById(patientId);
        //    var contactPersons = _patientService.ContactPersons.FindAll(patient.Id);
        //    var employees = _employeeService.FindAll(organization.Id)
        //        .Where(x => contactPersons.All(y => x.Id != y.Employee.Id || (x.Id == y.Employee.Id && y.IsEnded)));

        //    viewModel.EmployeesList = employees.GetSelectList();
        //    viewModel.ContactPersons = contactPersons;
        //    viewModel.Organization = organization;
        //    viewModel.Patient = patient;
        //    return View(viewModel);
        //}

        [HttpPost]
        public ActionResult Complete(int patientId, int relationshipId)
        {
            _patientService.ContactPersons.Complete(relationshipId);

            return RedirectToAction("Index", new { patientId });
        }
    }
}