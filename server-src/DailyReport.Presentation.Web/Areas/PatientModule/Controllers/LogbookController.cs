﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Models.Core;
using DailyReport.Presentation.Web.Areas.Administration.Models;
using DailyReport.Presentation.Web.Areas.PatientModule.Models;
using DailyReport.Presentation.Web.Extensions;

namespace DailyReport.Presentation.Web.Areas.PatientModule.Controllers
{
    [Authorize]
    public class LogbookController : Controller
    {
        private readonly IPatientService _patientService;
        private readonly IOrganizationService _organizationService;
        private readonly IPatientPostService _patientPostService;

        public LogbookController(IPatientService patientService, IOrganizationService organizationService, IPatientPostService patientPostService)
        {
            _patientService = patientService;
            _organizationService = organizationService;
            _patientPostService = patientPostService;
        }
        public ActionResult Index(int patientId)
        {
            var organization = _organizationService.GetById(ClaimsPrincipal.Current.GetOrganizationId());
            var patient = _patientService.GetById(patientId);
            var posts = _patientPostService.FindAllByPatient(patient.Id);

            return View(new PatientLogbookViewModel
            {
                Organization = organization,
                Patient = patient,
                Posts = posts
            });
        }
    }
}