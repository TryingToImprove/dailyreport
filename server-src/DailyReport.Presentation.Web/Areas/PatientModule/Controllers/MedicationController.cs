﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Presentation.Web.Areas.PatientModule.Models;
using DailyReport.Presentation.Web.Extensions;

namespace DailyReport.Presentation.Web.Areas.PatientModule.Controllers
{
    [Authorize]
    public class MedicationController : Controller
    {
        private readonly IPatientService _patientService;
        private readonly IOrganizationService _organizationService;
        private readonly IMedicationScheduleService _medicationScheduleService;

        public MedicationController(IPatientService patientService, IOrganizationService organizationService, IMedicationScheduleService medicationScheduleService)
        {
            _patientService = patientService;
            _organizationService = organizationService;
            _medicationScheduleService = medicationScheduleService;
        }

        public ActionResult Index(int patientId)
        {
            var organization = _organizationService.GetById(ClaimsPrincipal.Current.GetOrganizationId());
            var patient = _patientService.GetById(patientId);
            var medicationSchedules = _medicationScheduleService.FindByPatientId(patient.Id);

            return View(new PatientMedicationSchedulesViewModel
            {
                Organization = organization,
                Patient = patient,
                MedicationSchedules = medicationSchedules
            });
        }
    }
}