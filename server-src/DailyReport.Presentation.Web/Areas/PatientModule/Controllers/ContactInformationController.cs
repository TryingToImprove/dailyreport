﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Presentation.Web.Areas.PatientModule.Models;
using DailyReport.Presentation.Web.Extensions;

namespace DailyReport.Presentation.Web.Areas.PatientModule.Controllers
{
    public class ContactInformationController : Controller
    {
        private readonly IPatientService _patientService;
        private readonly IOrganizationService _organizationService;
        private readonly IContactInformationService _contactInformationService;

        public ContactInformationController(IPatientService patientService, IOrganizationService organizationService, IContactInformationService contactInformationService)
        {
            _patientService = patientService;
            _organizationService = organizationService;
            _contactInformationService = contactInformationService;
        }

        public ActionResult Index(int patientId)
        {
            var organization = _organizationService.GetById(ClaimsPrincipal.Current.GetOrganizationId());
            var patient = _patientService.GetById(patientId);
            var contactInformationTypes = _contactInformationService.GetTypes();

            return View(new ContactInformationViewModel
            {
                Organization = organization,
                Patient = patient,
                ContactInformationTypeList = contactInformationTypes.GetSelectList()
            });
        }

        public ActionResult Edit(int patientId, int id)
        {
            var organization = _organizationService.GetById(ClaimsPrincipal.Current.GetOrganizationId());
            var patient = _patientService.GetById(patientId);
            var contactInformationTypes = _contactInformationService.GetTypes();

            // Fetch the contact information
            var contactInformation = _contactInformationService.GetById(id);

            return View(new ContactInformationEditViewModel
            {
                Organization = organization,
                Patient = patient,
                ContactInformationTypeList = contactInformationTypes.GetSelectList(),

                ContactInformationId = contactInformation.Id,
                ContactInformationTypeId = contactInformation.TypeId,
                Data = contactInformation.Data
            });
        }

        [HttpPost]
        public ActionResult Edit(int patientId, int id, ContactInformationEditViewModel viewModel)
        {
            var organization = _organizationService.GetById(ClaimsPrincipal.Current.GetOrganizationId());
            var patient = _patientService.GetById(patientId);

            if (ModelState.IsValid)
            {
                _contactInformationService.Update(id, viewModel.ContactInformationTypeId, viewModel.Data);

                return RedirectToAction("Edit", new { patientId = patient.Id, id = id });
            }


            var contactInformationTypes = _contactInformationService.GetTypes();

            // Remap viewmodel
            viewModel.Organization = organization;
            viewModel.Patient = patient;
            viewModel.ContactInformationId = id;
            viewModel.ContactInformationTypeList = contactInformationTypes.GetSelectList();

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int patientId, int id)
        {
            _contactInformationService.Deactivate(id);

            return RedirectToAction("Index", new { patientId });
        }

        [ChildActionOnly]
        public ActionResult ContactInformations(int patientId, bool disableEditing = false, bool enableText = false)
        {
            return PartialView(new ContactInformationPartialViewModel
            {
                PatientId = patientId,
                ContactInformations = _patientService.ContactInformations.FindAll(patientId),
                DisableEditing = disableEditing,
                EnableText = enableText
            });
        }
    }
}