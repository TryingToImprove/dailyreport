﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using DailyReport.Presentation.Web.Areas.Administration.Models;
using DailyReport.Presentation.Web.Areas.PatientModule.Models;
using DailyReport.Presentation.Web.Extensions;

namespace DailyReport.Presentation.Web.Areas.PatientModule.Controllers
{
    [Authorize]
    public class ManageController : Controller
    {
        private readonly IPatientService _patientService;
        private readonly IOrganizationService _organizationService;
        private readonly IEmployeeService _employeeService;

        public ManageController(IPatientService patientService, IOrganizationService organizationService, IEmployeeService employeeService)
        {
            _patientService = patientService;
            _organizationService = organizationService;
            _employeeService = employeeService;
        }

        public ActionResult Index(int patientId)
        {
            var organization = _organizationService.GetById(ClaimsPrincipal.Current.GetOrganizationId());
            var patient = _patientService.GetById(patientId);
            var contactPersons = _patientService.ContactPersons.FindCurrents(patient.Id);
            var contactInformations = _patientService.ContactInformations.FindAll(patient.Id);

            return View(new PatientEditViewModel
            {
                Organization = organization,
                Patient = patient,
                ContactPersons = contactPersons,
                ContactInformations = contactInformations,
                Firstname = patient.Firstname,
                Lastname = patient.Lastname,
                BirthDate = patient.BirthDate
            });
        }
    }
}