﻿using System.Collections.Generic;
using DailyReport.Domain.Models;

namespace DailyReport.Presentation.Web.Areas.PatientModule.Models
{
    public class PatientMedicationSchedulesViewModel : PatientBaseViewModel
    {
        public IEnumerable<MedicationScheduleResultModel> MedicationSchedules { get; set; }
    }
}