﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DailyReport.Domain.Models;

namespace DailyReport.Presentation.Web.Areas.PatientModule.Models
{
    public class PatientEditViewModel : PatientBaseViewModel
    {
        [Required]
        public string Firstname { get; set; }

        [Required]
        public string Lastname { get; set; }

        [Required]
        public DateTime BirthDate { get; set; }

        public IEnumerable<PatientContactPersonRelationshipResultModel> ContactPersons { get; set; }

        public IEnumerable<ContactInformationResultModel> ContactInformations { get; set; }
    }
}