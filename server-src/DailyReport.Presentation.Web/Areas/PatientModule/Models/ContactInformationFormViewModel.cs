﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DailyReport.Presentation.Web.Areas.PatientModule.Models
{
    public class ContactInformationFormViewModel
    {
        public IEnumerable<SelectListItem> ContactInformationTypeList { get; set; }

        [Required]
        public int ContactInformationTypeId { get; set; }

        [Required]
        public string Data { get; set; }
    }
}