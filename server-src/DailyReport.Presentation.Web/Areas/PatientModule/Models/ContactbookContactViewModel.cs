﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;

namespace DailyReport.Presentation.Web.Areas.PatientModule.Models
{
    public class ContactbookContactViewModel : PatientBaseViewModel
    {
        public ContactResultModel Contact { get; set; }

        public IEnumerable<ContactInformationResultModel> ContactInformations { get; set; }

        public ContactInformationFormViewModel ContactInformationForm { get; set; }
    }
}