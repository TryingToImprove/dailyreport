﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;

namespace DailyReport.Presentation.Web.Areas.PatientModule.Models
{
    public class ContactbookGroupViewModel : PatientBaseViewModel
    {
        public ContactGroupResultModel Group { get; set; }

        public IEnumerable<ContactResultModel> Contacts { get; set; }
    }
}