﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using DailyReport.Presentation.Web.Binders;

namespace DailyReport.Presentation.Web.Areas.PatientModule.Models
{
    public class ContactbookAddViewModel : PatientBaseViewModel
    {
        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public string Type { get; set; }

        [StringSplitOn(SplitOn = ",")]
        public IEnumerable<string> Groups { get; set; }
    }
}