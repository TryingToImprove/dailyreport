﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DailyReport.Domain.Models.Core;
using DailyReport.Presentation.Web.Areas.Administration.Models;

namespace DailyReport.Presentation.Web.Areas.PatientModule.Models
{
    public class PatientDescriptionIndexViewModel : OrganizationBaseViewModel
    {
        [Required]
        [DataType(DataType.MultilineText)]
        public string Text { get; set; }

        public IEnumerable<Description> Descriptions { get; set; }

        public Patient Patient { get; set; }
    }
}