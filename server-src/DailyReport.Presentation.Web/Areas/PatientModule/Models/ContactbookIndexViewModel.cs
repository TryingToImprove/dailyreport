﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;

namespace DailyReport.Presentation.Web.Areas.PatientModule.Models
{
    public class ContactbookIndexViewModel : PatientBaseViewModel
    {
        public IEnumerable<ContactResultModel> Contacts { get; set; }

        public IEnumerable<ContactGroupResultModel> Groups { get; set; }
    }
}