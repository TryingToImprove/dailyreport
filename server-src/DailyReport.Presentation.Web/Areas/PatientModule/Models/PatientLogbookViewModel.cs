﻿using System.Collections.Generic;
using DailyReport.Domain.Models;

namespace DailyReport.Presentation.Web.Areas.PatientModule.Models
{
    public class PatientLogbookViewModel : PatientBaseViewModel
    {
        public IEnumerable<PatientPostResultModel> Posts { get; set; }
    }
}