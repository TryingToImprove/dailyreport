﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DailyReport.Domain.Models;

namespace DailyReport.Presentation.Web.Areas.PatientModule.Models
{
    public class ContactInformationPartialViewModel
    {
        public int PatientId { get; set; }

        public IEnumerable<ContactInformationResultModel> ContactInformations { get; set; }

        public bool DisableEditing { get; set; }

        public bool EnableText { get; set; }
    }
}