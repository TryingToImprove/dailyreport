﻿using DailyReport.Domain.Models;
using DailyReport.Presentation.Web.Areas.Administration.Models;

namespace DailyReport.Presentation.Web.Areas.PatientModule.Models
{
    public abstract class PatientBaseViewModel : OrganizationBaseViewModel
    {
        public PatientResultModel Patient { get; set; }
    }
}