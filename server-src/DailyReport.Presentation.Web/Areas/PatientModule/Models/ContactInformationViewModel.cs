﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using DailyReport.Domain.Models;

namespace DailyReport.Presentation.Web.Areas.PatientModule.Models
{
    public class ContactInformationViewModel : PatientBaseViewModel
    {
        public IEnumerable<SelectListItem> ContactInformationTypeList { get; set; }

        [Required]
        public int ContactInformationTypeId { get; set; }

        [Required]
        public string Data { get; set; }
    }
}