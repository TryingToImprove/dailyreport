﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace DailyReport.Presentation.Web.Areas.AdministrationModule.Models
{
    public class MedicationCreateViewModel
    {
        [Required]
        public string Name { get; set; }

        public int? BrandId { get; set; }

        public string BrandName { get; set; }

        [Required]
        public int MeasureId { get; set; }

        [Required]
        public int MedicationTypeId { get; set; }

        [Required]
        [Range(1, double.MaxValue)]
        public double Weight { get; set; }

        public IEnumerable<SelectListItem> BrandList { get; set; }

        public IEnumerable<SelectListItem> MeasureList { get; set; }

        public IEnumerable<SelectListItem> MedicationTypeList { get; set; }
    }
}