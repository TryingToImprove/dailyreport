﻿using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Models;
using DailyReport.Presentation.Web.Areas.AdministrationModule.Models;
using DailyReport.Presentation.Web.Extensions;

namespace DailyReport.Presentation.Web.Areas.AdministrationModule.Controllers
{
    public class MedicationController : Controller
    {
        private readonly IMedicationService _medicationService;

        public MedicationController(IMedicationService medicationService)
        {
            _medicationService = medicationService;
        }

        public ActionResult Create()
        {
            var medicationBrands = _medicationService.FindBrands();
            var measures = _medicationService.GetMeasures();
            var medicationTypes = _medicationService.GetMedicationTypes();

            return View(new MedicationCreateViewModel
            {
                BrandList = Mapper.Map<IEnumerable<SelectListItem>>(medicationBrands),
                MeasureList = measures.GetSelectList(),
                MedicationTypeList = medicationTypes.GetSelectList()
            });
        }

        [HttpPost]
        public ActionResult Create(MedicationCreateViewModel model)
        {
            if (string.IsNullOrWhiteSpace(model.BrandName) && !model.BrandId.HasValue)
                ModelState.AddModelError("InvalidBrand", "BrandId or BrandName is required");

            if (ModelState.IsValid)
            {
                _medicationService.Insert(
                    model: Mapper.Map<MedicationInsertModel>(model)
                );

                return View("Created");
            }

            var medicationBrands = _medicationService.FindBrands();
            var medicationTypes = _medicationService.GetMedicationTypes();
            var measures = _medicationService.GetMeasures();

            // Map model
            model.BrandList = Mapper.Map<IEnumerable<SelectListItem>>(medicationBrands);
            model.MedicationTypeList = medicationTypes.GetSelectList();
            model.MeasureList = measures.GetSelectList();
            return View(model);
        }
    }
}