﻿using System.Security.Claims;
using System.Web.Routing;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Models.Core;
using DailyReport.Presentation.Web.Areas.Administration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DailyReport.Presentation.Web.Extensions;

namespace DailyReport.Presentation.Web.Areas.Administration.Controllers
{
    [Authorize]
    public class PatientsController : Controller
    {
        private readonly IPatientService _patientService;
        private readonly IOrganizationService _organizationService;

        public PatientsController(IPatientService patientService, IOrganizationService organizationService)
        {
            _patientService = patientService;
            _organizationService = organizationService;
        }

        public ActionResult Index()
        {
            var organization = _organizationService.GetById(ClaimsPrincipal.Current.GetOrganizationId());
            var patients = _patientService.FindAll(organization.Id);

            return View(new PatientIndexViewModel
            {
                Organization = organization,
                Patients = patients
            });
        }
    }
}