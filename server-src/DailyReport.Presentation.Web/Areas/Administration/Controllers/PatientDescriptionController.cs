﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Models.Core;
using DailyReport.Presentation.Web.Areas.Administration.Models;
using DailyReport.Presentation.Web.Areas.PatientModule.Models;
using Microsoft.AspNet.Identity;

namespace DailyReport.Presentation.Web.Areas.Administration.Controllers
{
    [Authorize]
    public class PatientDescriptionController : Controller
    {
        private readonly IPatientService _patientService;
        private readonly IOrganizationService _organizationService;

        public PatientDescriptionController(IPatientService patientService, IOrganizationService organizationService)
        {
            _patientService = patientService;
            _organizationService = organizationService;
        }

        public ActionResult Index(int organizationId, int patientId)
        {
            var patient = _patientService.GetById(patientId);
            var organization = _organizationService.GetById(organizationId);

            // Get valid description text (string)
            var descriptionText = (patient.Description != null)
                ? patient.Description.Text
                : string.Empty;

            return View(new PatientDescriptionIndexViewModel
            {
                Organization = organization,
                Descriptions = _patientService.GetDescriptions(patient.Id),
                Patient = patient,
                Text = descriptionText
            });
        }

        [HttpPost]
        public ActionResult Index(int organizationId, int patientId, PatientDescriptionIndexViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                _patientService.CreateDescription(patientId, new Description
                {
                    EmployeeId = User.Identity.GetUserId<int>(),
                    Text = viewModel.Text
                });

                return RedirectToAction("Index", "PatientDescription", new { Area = "Administration", OrganizationId = organizationId, PatientId = patientId });
            }

            var patient = _patientService.GetById(patientId);
            var organization = _organizationService.GetById(organizationId);

            // Map again, invalid state
            viewModel.Organization = organization;
            viewModel.Patient = patient;
            viewModel.Descriptions = _patientService.GetDescriptions(patient.Id);
            return View(viewModel);
        }
    }
}