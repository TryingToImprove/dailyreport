﻿using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using DailyReport.Presentation.Web.Areas.Administration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace DailyReport.Presentation.Web.Areas.Administration.Controllers
{
    [Authorize]
    public class PostController : Controller
    {
        private readonly IPatientPostService _patientPostService;
        private readonly IPatientService _patientService;
        private readonly IOrganizationService _organizationService;

        public PostController(IPatientPostService patientPostService, IPatientService patientService, IOrganizationService organizationService)
        {
            _patientPostService = patientPostService;
            _patientService = patientService;
            _organizationService = organizationService;
        }

        public ActionResult Index(int organizationId)
        {
            var organization = _organizationService.GetById(organizationId);
            var posts = _patientPostService.FindAll(organization.Id);

            return View(new PatientPostIndexViewModel
            {
                Organization = organization,
                Posts = posts
            });
        }


        public ActionResult Create(int organizationId)
        {
            var organization = _organizationService.GetById(organizationId);
            var patients = _patientService.FindAll(organization.Id);

            return View(new PatientPostCreateViewModel
            {
                Organization = organization,
                Patients = patients
            });
        }

        [HttpPost]
        public ActionResult Create(int organizationId, PatientPostCreateViewModel viewModel)
        {
            var organization = _organizationService.GetById(organizationId);

            if (ModelState.IsValid)
            {
                var patientPostInsertModel = new PatientPostInsertModel
                {
                    OrganizationId = organizationId,
                    Description = viewModel.Description,
                    EmployeeId = User.Identity.GetUserId<int>(),
                    Patients = viewModel.Patients.Select(x => x.Id)
                };

                _patientPostService.Insert(patientPostInsertModel);

                return View("Created", new PatientPostCreatedViewModel
                {
                    Organization = organization
                });
            }

            viewModel.Organization = organization;
            viewModel.Patients = _patientService.FindAll(organization.Id);
            return View(viewModel);
        }
    }
}