﻿using AutoMapper;
using DailyReport.Domain.Extensions;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using DailyReport.Presentation.Web.Areas.Administration.Models;
using DailyReport.Presentation.Web.Extensions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using System.Web.Mvc;

namespace DailyReport.Presentation.Web.Areas.Administration.Controllers
{
    public class MedicationScheduleController : Controller
    {
        private readonly IOrganizationService _organizationService;
        private readonly IMedicationScheduleService _medicationScheduleService;
        private readonly IMedicationService _medicationService;
        private readonly IPatientService _patientService;

        public MedicationScheduleController(IOrganizationService organizationService, IMedicationScheduleService medicationScheduleService, IMedicationService medicationService, IPatientService patientService)
        {
            _organizationService = organizationService;
            _medicationScheduleService = medicationScheduleService;
            _medicationService = medicationService;
            _patientService = patientService;
        }

        public ActionResult Index(int organizationId)
        {
            var organization = _organizationService.GetById(organizationId);
            var medicationSchedules = _medicationScheduleService.FindAll();

            return View(new MedicationScheduleIndexViewModel
            {
                Organization = organization,
                MedicationSchedules = medicationSchedules
            });
        }

        public ActionResult Edit(int id, int organizationId)
        {
            var organization = _organizationService.GetById(organizationId);
            var medicationSchedule = _medicationScheduleService.GetById(id);

            return View(new MedicationScheduleEditViewModel
            {
                Organization = organization,
                MedicationSchedule = medicationSchedule
            });
        }

        public ActionResult Details(int id, int organizationId, int medicationScheduleItemId)
        {
            var organization = _organizationService.GetById(organizationId);
            var medicationSchedule = _medicationScheduleService.GetById(id);
            //var medicationScheduleItemHistories = _medicationScheduleService.GetHistory(medicationScheduleItemId);

            return View(new MedicationScheduleDetailsViewModel
            {
                Organization = organization,
                MedicationSchedule = medicationSchedule,
                //Histories = medicationScheduleItemHistories
            });
        }

        public ActionResult Create(int organizationId)
        {
            var organization = _organizationService.GetById(organizationId);
            var patients = _patientService.FindAll(organization.Id);
            var medications = _medicationService.FindAll();

            return View(new MedicationScheduleCreateViewModel
            {
                Organization = organization,
                MedicationList = medications.GetSelectList(),
                PatientList = patients.GetSelectList(),
                StartDate = DateTime.Now.Date
            });
        }

        [HttpPost]
        public ActionResult Create(int organizationId, MedicationScheduleCreateViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                _medicationScheduleService.Insert(Mapper.Map<MedicationScheduleInsertModel>(viewModel));

                return RedirectToAction("Index", "MedicationSchedule", new { Area = "Administration", OrganizationId = organizationId });
            }

            var organization = _organizationService.GetById(organizationId);
            var patients = _patientService.FindAll(organization.Id);
            var medications = _medicationService.FindAll();

            // Remap data
            viewModel.Organization = organization;
            viewModel.MedicationList = medications.GetSelectList();
            viewModel.PatientList = patients.GetSelectList();

            return View(viewModel);
        }

        public ActionResult Complete(int id, int organizationId)
        {
            var organization = _organizationService.GetById(organizationId);
            var medicationSchedule = _medicationScheduleService.GetById(id);

            if (medicationSchedule.IsCompleted)
                throw new InvalidOperationException("MedicationSchedule is already completed");

            return View(new MedicationScheduleCompleteViewModel
            {
                Organization = organization,
                MedicationSchedule = medicationSchedule
            });
        }

        [HttpPost]
        public ActionResult Complete(int id, int organizationId, MedicationScheduleCompleteViewModel viewModel)
        {
            var medicationSchedule = _medicationScheduleService.GetById(id);

            if (medicationSchedule.IsCompleted)
                throw new InvalidOperationException("MedicationSchedule is already completed");

            if (ModelState.IsValid)
            {
                _medicationScheduleService.CompleteSchedule(new MedicationScheduleComplete
                {
                    Description = viewModel.Description,
                    EmployeeId = User.Identity.GetUserId<int>(),
                    MedicationScheduleId = id
                });

                return RedirectToAction("Edit", "MedicationSchedule", new { Area = "Administration", OrganizationId = organizationId, Id = id });
            }

            var organization = _organizationService.GetById(organizationId);

            viewModel.Organization = organization;
            viewModel.MedicationSchedule = medicationSchedule;
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult AddHistoryItem(int organizationId, int medicationScheduleItemId)
        {
            //_medicationScheduleService.AddHistoryItem(new MedicationScheduleItemHistory
            //{
            //    MedicationScheduleItemId = medicationScheduleItemId
            //}, User.Identity.GetUserId<int>());

            return RedirectToAction("Index", "MedicationSchedule", new { Area = "Administration", OrganizationId = organizationId });
        }
    }
}