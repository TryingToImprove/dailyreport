﻿using DailyReport.DataAccess;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Models.Core;
using DailyReport.Presentation.Web.Areas.Administration.Models;
using DailyReport.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DailyReport.Presentation.Web.Areas.Administration.Controllers
{
    [Authorize]
    public class OrganizationController : Controller
    {
        private readonly IOrganizationService _organizationService;

        private readonly IPatientPostService _patientPostService;

        private readonly IMedicationScheduleService _medicationScheduleService;

        public OrganizationController(IOrganizationService organizationService, IPatientPostService patientPostService, IMedicationScheduleService medicationScheduleService)
        {
            _organizationService = organizationService;
            _patientPostService = patientPostService;
            _medicationScheduleService = medicationScheduleService;
        }

        public ActionResult Index()
        {
            var organizations = _organizationService.FindAll();

            return View(new OrganizationIndexViewModel
            {
                Organizations = organizations
            });
        }

        public ActionResult Create()
        {
            return View(new OrganizationCreateViewModel());
        }

        [HttpPost]
        public ActionResult Create(OrganizationCreateViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return View(viewModel);

            _organizationService.Insert(new Organization
            {
                Name = viewModel.Name
            });

            return RedirectToAction("Index");
        }
    }
}