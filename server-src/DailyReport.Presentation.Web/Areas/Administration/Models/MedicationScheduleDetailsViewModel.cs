﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;

namespace DailyReport.Presentation.Web.Areas.Administration.Models
{
    public class MedicationScheduleDetailsViewModel : OrganizationBaseViewModel
    {
        public MedicationScheduleResultModel MedicationSchedule { get; set; }

        public IEnumerable<MedicationScheduleItemHistoryResultModel> Histories { get; set; }
    }
}