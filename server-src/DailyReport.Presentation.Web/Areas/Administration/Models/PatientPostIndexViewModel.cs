﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DailyReport.Domain.Models;

namespace DailyReport.Presentation.Web.Areas.Administration.Models
{
    public class PatientPostIndexViewModel : OrganizationBaseViewModel
    {
        public IEnumerable<PatientPostResultModel> Posts { get; set; }
    }
}