﻿using DailyReport.Domain.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DailyReport.Presentation.Web.Areas.Administration.Models
{
    public class MedicationScheduleCompleteViewModel : OrganizationBaseViewModel
    {
        public MedicationScheduleResultModel MedicationSchedule { get; set; }

        [Required]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
    }
}