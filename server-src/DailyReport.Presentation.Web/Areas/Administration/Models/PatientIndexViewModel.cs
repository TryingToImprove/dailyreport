﻿using System.Collections.Generic;
using DailyReport.Domain.Models.Core;

namespace DailyReport.Presentation.Web.Areas.Administration.Models
{
    public class PatientIndexViewModel : OrganizationBaseViewModel
    {
        public IEnumerable<Patient> Patients { get; set; }
    }
}