﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DailyReport.Presentation.Web.Areas.Administration.Models
{
    public class OrganizationCreateViewModel
    {
        [Required]
        public string Name { get; set; }
    }
}