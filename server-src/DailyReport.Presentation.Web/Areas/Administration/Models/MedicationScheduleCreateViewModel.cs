﻿using DailyReport.Domain.Models.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DailyReport.Presentation.Web.Areas.Administration.Models
{
    public class MedicationScheduleCreateViewModel : OrganizationBaseViewModel
    {
        [Required]
        public int PatientId { get; set; }

        [Required]
        public DateTime StartDate { get; set; }

        public IList<MedicationScheduleCreateItemViewModel> Items { get; set; }

        public IEnumerable<SelectListItem> MedicationList { get; set; }

        public IEnumerable<SelectListItem> PatientList { get; set; }
    }

    public class MedicationScheduleCreateItemViewModel
    {
        [Required]
        public int MedicationId { get; set; }

        [Required]
        public double Amouth { get; set; }

        [Required]
        public TimeSpan ExecuteTime { get; set; }

        public TimeSpan? RepeatInterval { get; set; }
    }
}