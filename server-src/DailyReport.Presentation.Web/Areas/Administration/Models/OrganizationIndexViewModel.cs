﻿using DailyReport.Domain.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DailyReport.Presentation.Web.Areas.Administration.Models
{
    public class OrganizationIndexViewModel
    {

        public IEnumerable<Organization> Organizations { get; set; }
    }
}