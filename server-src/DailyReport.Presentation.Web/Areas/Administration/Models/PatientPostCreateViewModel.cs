﻿using DailyReport.Domain.Models.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DailyReport.Presentation.Web.Areas.Administration.Models
{
    public class PatientPostCreateViewModel : OrganizationBaseViewModel
    {
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        public IEnumerable<Patient> Patients { get; set; }
    }
}