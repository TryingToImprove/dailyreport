﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;

namespace DailyReport.Presentation.Web.Areas.Administration.Models
{
    public class PatientCreateViewModel
    {
        [Required]
        public string Firstname { get; set; }

        [Required]
        public string Lastname { get; set; }

        //[Required]
        public DateTime? BirthDate { get; set; }

        public int SocialSecurityNumber { get; set; }
        
        public string Description { get; set; }

        public IEnumerable<int> ContactPersons { get; set; }
        
        public IEnumerable<ContactInformationModel> ContactInformations { get; set; }
    }

    public class ContactInformationModel
    {
        [Required]
        public string Data { get; set; }

        [Required]
        public int DataTypeId { get; set; }
    }
}