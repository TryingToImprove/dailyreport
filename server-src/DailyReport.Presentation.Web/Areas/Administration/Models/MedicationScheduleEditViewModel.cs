﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DailyReport.Domain.Models;

namespace DailyReport.Presentation.Web.Areas.Administration.Models
{
    public class MedicationScheduleEditViewModel : OrganizationBaseViewModel
    {
        public MedicationScheduleResultModel MedicationSchedule { get; set; }
    }
}