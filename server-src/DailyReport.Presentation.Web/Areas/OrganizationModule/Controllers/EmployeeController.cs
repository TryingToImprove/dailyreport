﻿using System.Security.Claims;
using System.Web.Mvc;
using AutoMapper;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using DailyReport.Presentation.Web.Areas.Administration.Models;
using DailyReport.Presentation.Web.Areas.OrganizationModule.Models;
using DailyReport.Presentation.Web.Authentication;
using DailyReport.Presentation.Web.Extensions;

namespace DailyReport.Presentation.Web.Areas.OrganizationModule.Controllers
{
    [ClaimsAuthorize("Employee", "FullAccess")]
    public class EmployeeController : Controller
    {
        private readonly IOrganizationService _organizationService;

        private readonly IEmployeeService _employeeService;

        public EmployeeController(IOrganizationService organizationService, IEmployeeService employeeService)
        {
            _organizationService = organizationService;
            _employeeService = employeeService;
        }

        public ActionResult Index()
        {
            var organization = _organizationService.GetById(ClaimsPrincipal.Current.GetOrganizationId());
            var employees = _employeeService.FindAll(organization.Id);

            return View(new EmployeeIndexViewModel
            {
                Employees = employees
            });
        }

        public ActionResult Create()
        {
            return View(new EmployeeCreateViewModel());
        }

        [HttpPost]
        public ActionResult Create(EmployeeCreateViewModel viewModel)
        {
            if (!ModelState.IsValid) return View(viewModel);

            // Map the employee
            var employee = Mapper.Map<EmployeeInsertModel>(viewModel);
            employee.OrganizationId = ClaimsPrincipal.Current.GetOrganizationId();

            // Insert the employee
            var createdEmployee = _employeeService.Insert(employee);

            return View("PendingCreate", createdEmployee);
        }

        public ActionResult Delete(int id)
        {
            var employee = _employeeService.GetById(id);

            return View("ConfirmDelete", new EmployeeConfirmDeleteViewModel
            {
                EmployeeId = employee.Id,
                Firstname = employee.Firstname,
                Lastname = employee.Lastname,
                TimeCreated = employee.TimeCreated
            });
        }

        [HttpPost]
        public ActionResult Delete(int id, object viewModel)
        {
            var employee = _employeeService.GetById(id);

            _employeeService.UpdateActiveState(employee.Id, false);

            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            var employee = _employeeService.GetById(id);

            return View(new EmployeeEditViewModel
            {
                Employee = employee
            });
        }
    }
}