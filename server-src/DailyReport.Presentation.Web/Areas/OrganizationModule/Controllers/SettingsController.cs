﻿using System.Security.Claims;
using System.Web.Mvc;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Presentation.Web.Areas.OrganizationModule.Models;
using DailyReport.Presentation.Web.Extensions;

namespace DailyReport.Presentation.Web.Areas.OrganizationModule.Controllers
{
    [Authorize]
    public class SettingsController : Controller
    {
        private readonly IOrganizationService _organizationService;

        public SettingsController(IOrganizationService organizationService)
        {
            _organizationService = organizationService;
        }


        public ActionResult Edit()
        {
            var organization = _organizationService.GetById(ClaimsPrincipal.Current.GetOrganizationId());

            return View(new SettingsEditViewModel
            {
                Id = organization.Id,
                Name = organization.Name,
                TimeCreated = organization.TimeCreated
            });
        }

        [HttpPost]
        public ActionResult Edit(SettingsEditViewModel viewModel)
        {
            var organization = _organizationService.GetById(ClaimsPrincipal.Current.GetOrganizationId());

            if (ModelState.IsValid)
            {
                organization.Name = viewModel.Name;

                _organizationService.Update(organization);
            }

            // Map again
            viewModel.Id = organization.Id;
            viewModel.TimeCreated = organization.TimeCreated;
            return View(viewModel);
        }
    }
}