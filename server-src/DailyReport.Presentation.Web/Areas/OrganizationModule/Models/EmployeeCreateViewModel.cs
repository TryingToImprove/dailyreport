﻿using System.ComponentModel.DataAnnotations;

namespace DailyReport.Presentation.Web.Areas.OrganizationModule.Models
{
    public class EmployeeCreateViewModel 
    {        
        [Required]
        public string Firstname { get; set; }

        [Required]
        public string Lastname { get; set; }

        [Required]
        public string Email { get; set; }
    }
}