﻿using System.ComponentModel.DataAnnotations;
using DailyReport.Domain.Models;

namespace DailyReport.Presentation.Web.Areas.OrganizationModule.Models
{
    public class EmployeeEditViewModel
    {
        public EmployeeResultModel Employee { get; set; }
    }
}