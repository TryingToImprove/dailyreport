﻿using System.Collections.Generic;
using DailyReport.Domain.Models.Core;

namespace DailyReport.Presentation.Web.Areas.OrganizationModule.Models
{
    public class EmployeeIndexViewModel 
    {
        public IEnumerable<Employee> Employees { get; set; }
    }
}