﻿using System;

namespace DailyReport.Presentation.Web.Areas.OrganizationModule.Models
{
    public class EmployeeConfirmDeleteViewModel
    {
        public int EmployeeId { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public DateTime TimeCreated { get; set; }
    }
}