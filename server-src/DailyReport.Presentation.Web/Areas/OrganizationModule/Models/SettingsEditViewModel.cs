﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DailyReport.Domain.Models;

namespace DailyReport.Presentation.Web.Areas.OrganizationModule.Models
{
    public class SettingsEditViewModel
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public DateTime TimeCreated { get; set; }

        public IEnumerable<PatientPostResultModel> Posts { get; set; }

        public IEnumerable<MedicationScheduleResultModel> UpcomingMedicationSchedules { get; set; }
    }
}