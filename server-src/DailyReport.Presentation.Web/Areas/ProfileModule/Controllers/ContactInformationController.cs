﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Presentation.Web.Areas.ProfileModule.Models;
using DailyReport.Presentation.Web.Extensions;
using Microsoft.AspNet.Identity;

namespace DailyReport.Presentation.Web.Areas.ProfileModule.Controllers
{
    public class ContactInformationController : Controller
    {
        private readonly IEmployeeService _employeeService;
        private readonly IContactInformationService _contactInformationService;

        public ContactInformationController(IEmployeeService employeeService, IContactInformationService contactInformationService)
        {
            _employeeService = employeeService;
            _contactInformationService = contactInformationService;
        }

        public ActionResult Index()
        {
            var contactInformationTypes = _contactInformationService.GetTypes();

            return View(new ContactInformationViewModel
            {
                ContactInformationTypeList = contactInformationTypes.GetSelectList()
            });
        }
        
        public ActionResult Edit(int id)
        {
            var contactInformationTypes = _contactInformationService.GetTypes();

            // Fetch the contact information
            var contactInformation = _contactInformationService.GetById(id);

            return View(new ContactInformationEditViewModel
            {
                ContactInformationTypeList = contactInformationTypes.GetSelectList(),

                ContactInformationId = contactInformation.Id,
                ContactInformationTypeId = contactInformation.TypeId,
                Data = contactInformation.Data
            });
        }

        [HttpPost]
        public ActionResult Edit(int id, ContactInformationEditViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                _contactInformationService.Update(id, viewModel.ContactInformationTypeId, viewModel.Data);

                return RedirectToAction("Edit", new { id });
            }


            var contactInformationTypes = _contactInformationService.GetTypes();

            // Remap viewmodel
            viewModel.ContactInformationId = id;
            viewModel.ContactInformationTypeList = contactInformationTypes.GetSelectList();

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            _contactInformationService.Deactivate(id);

            return RedirectToAction("Index");
        }

        [ChildActionOnly]
        public ActionResult ContactInformations(bool disableEditing = false, bool enableText = false)
        {
            return PartialView(new ContactInformationPartialViewModel
            {
                ContactInformations = _employeeService.ContactInformations.FindAll(User.Identity.GetUserId<int>()),
                DisableEditing = disableEditing,
                EnableText = enableText
            });
        }
    }
}