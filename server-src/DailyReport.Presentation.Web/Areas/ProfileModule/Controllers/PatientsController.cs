﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Presentation.Web.Areas.Administration.Models;
using DailyReport.Presentation.Web.Areas.ProfileModule.Models;
using DailyReport.Presentation.Web.Extensions;
using Microsoft.AspNet.Identity;

namespace DailyReport.Presentation.Web.Areas.ProfileModule.Controllers
{
    public class PatientsController : Controller
    {
        private readonly IPatientService _patientService;

        public PatientsController(IPatientService patientService)
        {
            _patientService = patientService;
        }

        public ActionResult Index()
        {
            var patients = _patientService.FindByEmployee(User.Identity.GetUserId<int>());

            return View(new PatientsIndexViewModel
            {
                Patients = patients
            });
        }
    }
}