﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DailyReport.Domain.Models;

namespace DailyReport.Presentation.Web.Areas.ProfileModule.Models
{
    public class ContactInformationPartialViewModel
    {
        public IEnumerable<ContactInformationResultModel> ContactInformations { get; set; }

        public bool DisableEditing { get; set; }

        public bool EnableText { get; set; }
    }
}