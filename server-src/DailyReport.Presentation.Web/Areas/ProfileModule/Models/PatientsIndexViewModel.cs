﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DailyReport.Domain.Models.Core;

namespace DailyReport.Presentation.Web.Areas.ProfileModule.Models
{
    public class PatientsIndexViewModel
    {
        public IEnumerable<Patient> Patients { get; set; }
    }
}