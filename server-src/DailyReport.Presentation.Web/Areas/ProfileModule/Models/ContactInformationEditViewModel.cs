﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using DailyReport.Domain.Models;

namespace DailyReport.Presentation.Web.Areas.ProfileModule.Models
{
    public class ContactInformationEditViewModel : ContactInformationViewModel
    {
        public int ContactInformationId { get; set; }
    }
}