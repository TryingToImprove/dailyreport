﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using CuttingEdge.Conditions;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using DailyReport.Presentation.Web.Authentication;
using DailyReport.Presentation.Web.Helpers;
using DailyReport.Services;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using DailyReport.Presentation.Web.Models;

namespace DailyReport.Presentation.Web.Controllers
{
    [AllowAnonymous]
    public class AccountController : Controller
    {
        private readonly EmployeeStore _employeeStore;
        private readonly IEmployeeService _employeeService;
        private readonly IAuthenticationService _authenticationService;
        private readonly IOrganizationService _organizationService;

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        public AccountController(EmployeeStore employeeStore, IEmployeeService employeeService, IAuthenticationService authenticationService, IOrganizationService organizationService)
        {
            _employeeStore = employeeStore;
            _employeeService = employeeService;
            _authenticationService = authenticationService;
            _organizationService = organizationService;
        }

        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel viewModel, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                // Try authenticate the user
                var isAuthenticated = await Authenticate(viewModel.Username, viewModel.Password);
                
                // Redirect to dashboard or return url if successfull authenticated
                if (isAuthenticated)
                {
                    var url = string.IsNullOrWhiteSpace(returnUrl)
                        ? Url.GetDashboardUrl()
                        : returnUrl;

                    return Redirect(url);
                }

                ModelState.AddModelError("", "It was not possible to authenticate you");
            }

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Login");
        }

        public ActionResult Register()
        {
            return View(new RegisterAccountViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterAccountViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                // Create the organization
                var organization = _organizationService.Insert(new Organization
                {
                    Name = viewModel.OrganizationName
                });

                // Map the viewmodel to a employeeInsert model
                var employeeModel = Mapper.Map<EmployeeInsertModel>(viewModel);
                employeeModel.OrganizationId = organization.Id;

                // Create the employee
                var employee = _employeeService.Insert(employeeModel);

                return View("PendingComplete", employee);
            }

            return View(viewModel);
        }

        public ActionResult CompleteRegistration(string secret)
        {
            //TODO: This should return a view instead of a exception
            if (!_authenticationService.IsValidAuthenticationKey(secret))
                throw new Exception("AuthenticationKey is not valid");

            var employee = _employeeService.GetBySecret(secret);

            return View(new CompleteAccountRegisteringViewModel
            {
                Employee = employee
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CompleteRegistration(string secret, CompleteAccountRegisteringViewModel viewModel)
        {
            //TODO: This should return a view instead of a exception
            if (!_authenticationService.IsValidAuthenticationKey(secret))
                throw new Exception("AuthenticationKey is not valid");

            // Return model if not valid
            if (ModelState.IsValid)
            {
                // Create the authenticationidentity
                _authenticationService.Insert(secret, viewModel.Username, viewModel.Password);

                // Authenticate the user
                var isAuthenticated = await Authenticate(viewModel.Username, viewModel.Password);

                // Redirect the user to the dashboard
                if (isAuthenticated)
                    return Redirect(Url.GetDashboardUrl());

                ModelState.AddModelError("", "It was not possible to authenticate you");
            }

            return View(viewModel);
        }

        private async Task<bool> Authenticate(string username, string password)
        {
            Condition.Requires(username).IsNotNullOrWhiteSpace();
            Condition.Requires(password).IsNotNullOrWhiteSpace();

            // Get the application user
            var user = await _employeeStore.FindAsync(username, password);

            // Make sure we have a user
            if (user == null)
                return false;

            // Authenticate the user
            await HttpContext.GetOwinContext().Get<ApplicationSignInManager>().SignInAsync(user, false, false);

            return true;
        }
    }
}