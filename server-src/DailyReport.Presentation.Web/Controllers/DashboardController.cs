﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Presentation.Web.Extensions;
using DailyReport.Presentation.Web.Models;

namespace DailyReport.Presentation.Web.Controllers
{
    [Authorize]
    public class DashboardController : Controller
    {
        private readonly IOrganizationService _organizationService;
        private readonly IPatientPostService _patientPostService;
        private readonly IMedicationScheduleService _medicationScheduleService;

        public DashboardController(IOrganizationService organizationService, IPatientPostService patientPostService, IMedicationScheduleService medicationScheduleService)
        {
            _organizationService = organizationService;
            _patientPostService = patientPostService;
            _medicationScheduleService = medicationScheduleService;
        }

        public ActionResult Index()
        {
            var organization = _organizationService.GetById(ClaimsPrincipal.Current.GetOrganizationId());
            var patientPosts = _patientPostService.FindAll(organization.Id);
            var medicationSchedules = _medicationScheduleService.FindUpcomings(organization.Id);
            
            return View(new OrganizationDashboardViewModel
            {
                Organization = organization,
                Id = organization.Id,
                Name = organization.Name,
                TimeCreated = organization.TimeCreated,
                Posts = patientPosts,
                UpcomingMedicationSchedules = medicationSchedules
            });
        }

        [ChildActionOnly]
        public ActionResult Navigation(int organizationId)
        {
            var organization = _organizationService.GetById(organizationId);

            return View("_OrganizationNavigationPartial", new OrganizationNavigationViewModel
            {
                Organization = organization
            });
        }
    }
}