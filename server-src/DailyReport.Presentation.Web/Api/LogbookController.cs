﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Presentation.Web.Extensions;
using Microsoft.AspNet.Identity;

namespace DailyReport.Presentation.Web.Api
{
    public class LogbookController : ApiController
    {
        private readonly IPatientPostService _patientPostService;

        public LogbookController(IPatientPostService patientPostService)
        {
            _patientPostService = patientPostService;
        }

        [HttpGet]
        [Authorize]
        public async Task<IHttpActionResult> GetByPatientId(int patientId)
        {
            var posts = _patientPostService.FindAllByPatient(patientId);

            return Ok(posts);
        }
    }
}
