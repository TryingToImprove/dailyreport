﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using AutoMapper;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Models;
using DailyReport.Presentation.Web.Areas.Administration.Models;
using DailyReport.Presentation.Web.Extensions;
using DailyReport.Presentation.Web.Models;
using Microsoft.AspNet.Identity;

namespace DailyReport.Presentation.Web.Api
{
    public class PatientController : ApiController
    {
        private readonly IPatientService _patientService;

        public PatientController(IPatientService patientService)
        {
            _patientService = patientService;
        }

        [HttpGet]
        [Authorize]
        public async Task<IHttpActionResult> FindAll()
        {
            var patients = _patientService.FindAll(ClaimsPrincipal.Current.GetOrganizationId());

            return Ok(patients);
        }

        [HttpGet]
        [Authorize]
        public async Task<IHttpActionResult> Get(int id)
        {
            var patient = _patientService.GetById(id);

            return Ok(patient);
        }

        [HttpPut]
        [Authorize]
        public async Task<IHttpActionResult> Update(int id, PatientUpdateViewModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var updateModel = Mapper.Map<PatientUpdateModel>(model);

            var patient = _patientService.Update(id, updateModel);

            return Ok(patient);
        }

        [HttpPost]
        [Authorize]
        public async Task<IHttpActionResult> Create(PatientCreateViewModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            // Map data data
            var insertModel = Mapper.Map<PatientInsertModel>(model);
            insertModel.BirthDate = DateTime.Now;

            // Save the patient
            var patient = _patientService.Insert(insertModel);

            return Ok(patient);
        }
    }
}
