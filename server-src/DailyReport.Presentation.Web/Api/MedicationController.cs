﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Presentation.Web.Models;

namespace DailyReport.Presentation.Web.Api
{
    public class MedicationController : ApiController
    {
        private readonly IMedicationService _medicationService;

        public MedicationController(IMedicationService medicationService)
        {
            _medicationService = medicationService;
        }

        [HttpGet]
        public async Task<IHttpActionResult> Search([FromUri]MedicationViewModel.Search model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var medications = _medicationService.Search(model.Query);

            return Ok(medications);
        }

        [HttpGet]
        public async Task<IHttpActionResult> FindAll()
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var medications = _medicationService.FindAll();

            return Ok(medications);
        }
    }
}
