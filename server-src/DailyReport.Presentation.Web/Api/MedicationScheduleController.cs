﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using DailyReport.Presentation.Web.Extensions;
using DailyReport.Presentation.Web.Models;
using Microsoft.AspNet.Identity;

namespace DailyReport.Presentation.Web.Api
{
    //[Authorize]
    public class MedicationScheduleController : ApiController
    {
        private readonly IMedicationScheduleService _medicationScheduleService;

        public MedicationScheduleController(IMedicationScheduleService medicationScheduleService)
        {
            _medicationScheduleService = medicationScheduleService;
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetPatientSchema(int id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);


            var medicationSchedules = _medicationScheduleService.GetPatientSchema(id);

            return Ok(medicationSchedules);
        }

        [HttpGet]
        public async Task<IHttpActionResult> FindAll(int id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);


            var medicationSchedules = _medicationScheduleService.FindByPatientId(id);

            return Ok(medicationSchedules);
        }

        [HttpGet]
        public async Task<IHttpActionResult> FindAllToday(int id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);


            var medicationSchedules = _medicationScheduleService.FindAllTodayByPatient(id);

            return Ok(medicationSchedules);
        }

        public async Task<IHttpActionResult> CreatePatientSchema(MedicationScheduleViewModel.CreatePatientSchema model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var scheduleModel = Mapper.Map<MedicationScheduleInsertModel>(model);

            // Insert the schedule
            var medicationSchedule = _medicationScheduleService.Insert(scheduleModel);

            return Ok(medicationSchedule);
        }

        [HttpPut]
        public async Task<IHttpActionResult> Complete(int id, [FromBody]MedicationScheduleViewModel.Complete model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var medicationSchedule = _medicationScheduleService.CompleteSchedule(new MedicationScheduleComplete
            {
                EmployeeId = ClaimsPrincipal.Current.GetEmployeeId(),
                MedicationScheduleId = id,
                TimeCreated = DateTime.Now,
                Description = model.Description
            });

            return Ok(medicationSchedule);
        }
    }
}
