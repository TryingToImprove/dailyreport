﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using AutoMapper;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Models;
using DailyReport.Presentation.Web.Areas.Administration.Models;
using DailyReport.Presentation.Web.Areas.PatientModule.Models;
using DailyReport.Presentation.Web.Extensions;
using DailyReport.Presentation.Web.Models;
using Microsoft.AspNet.Identity;

namespace DailyReport.Presentation.Web.Api
{
    public class ContactPersonController : ApiController
    {
        private readonly IPatientService _patientService;

        public ContactPersonController(IPatientService patientService)
        {
            _patientService = patientService;
        }

        [HttpGet]
        [Authorize]
        public async Task<IHttpActionResult> FindAll(int patientId)
        {
            var patients = _patientService.ContactPersons.FindAll(patientId);

            return Ok(patients);
        }

        [HttpGet]
        [Authorize]
        public async Task<IHttpActionResult> FindCurrents(int patientId)
        {
            var patients = _patientService.ContactPersons.FindCurrents(patientId);

            return Ok(patients);
        }

        [HttpPost]
        [Authorize]
        public async Task<IHttpActionResult> Add(int patientId, [FromBody]ContactPersonViewModels.Add model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var result = _patientService.ContactPersons.Add(patientId, model.EmployeeId, model.CompletePrevious);

            return Ok(result);
        }

        [HttpPut]
        [Authorize]
        public async Task<IHttpActionResult> Complete([FromBody]ContactPersonViewModels.Complete model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var result = _patientService.ContactPersons.Complete(model.Id);
            
            return Ok(result);
        }

    }
}
