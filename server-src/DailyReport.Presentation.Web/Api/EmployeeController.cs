﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Presentation.Web.Extensions;

namespace DailyReport.Presentation.Web.Api
{
    [Authorize]
    public class EmployeeController : ApiController
    {
        private readonly IEmployeeService _employeeService;

        public EmployeeController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        [HttpGet]
        public async Task<IHttpActionResult> FindAll()
        {
            var employees = _employeeService.FindAll(ClaimsPrincipal.Current.GetOrganizationId());

            return Ok(employees);
        }

        [HttpGet]
        public async Task<IHttpActionResult> Get(int id)
        {
            var employees = _employeeService.GetById(id);

            return Ok(employees);
        }
    }
}
