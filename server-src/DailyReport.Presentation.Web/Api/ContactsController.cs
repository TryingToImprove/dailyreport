﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Models;
using DailyReport.Presentation.Web.Extensions;
using DailyReport.Presentation.Web.Models;

namespace DailyReport.Presentation.Web.Api
{
    public class ContactsController : ApiController
    {
        private readonly IPatientService _patientService;
        private readonly IContactbookService _contactbookService;

        public ContactsController(IPatientService patientService, IContactbookService contactbookService)
        {
            _patientService = patientService;
            _contactbookService = contactbookService;
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetGroups(int patientId)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var groups = _patientService.Contacts.GetGroups(patientId);

            return Ok(groups);
        }

        [HttpGet]
        public async Task<IHttpActionResult> FindByGroup(int patientId, int groupId)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var contacts = _patientService.Contacts.FindByGroup(patientId, groupId);

            return Ok(contacts);
        }

        [HttpGet]
        public async Task<IHttpActionResult> FindAll(int patientId)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var contacts = _patientService.Contacts.FindAll(patientId);

            return Ok(contacts);
        }

        [HttpGet]
        public async Task<IHttpActionResult> FindFrequent(int patientId)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            // TODO: Find frequent
            var contacts = _patientService.Contacts.FindAll(patientId).OrderByDescending(x => x.TimeCreated).Take(3);

            return Ok(contacts);
        }

        [HttpPost]
        public async Task<IHttpActionResult> Add(int patientId, ContactViewModel.Add model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var contact = _patientService.Contacts.Add(patientId, new ContactbookInsertModel
            {
                Firstname = model.Firstname,
                Lastname = model.Lastname,
                Groups = model.Groups,
                Type = model.Title,
                EmployeeId = ClaimsPrincipal.Current.GetEmployeeId(),
                ContactInformations = model.ContactInformations.Select(x=> new ContactInformationInsertModel
                {
                    DataType = x.Label,
                    Value  = x.Value
                })
            });
            
            return Ok(contact);
        }
    }
}
