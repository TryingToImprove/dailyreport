﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using DailyReport.Domain.Infrastructure;

namespace DailyReport.Presentation.Web.Api
{
    public class GeocodingController : ApiController
    {
        private readonly ILocationProvider _locationProvider;

        public GeocodingController(ILocationProvider locationProvider)
        {
            _locationProvider = locationProvider;
        }
        
        [HttpGet]
        public async Task<IHttpActionResult> Autocomplete(string query)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var items = await _locationProvider.SearchAsync(query);

            return Ok(items);
        }
    }
}
