﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Models;
using DailyReport.Presentation.Web.Extensions;
using DailyReport.Presentation.Web.Models;
using Microsoft.AspNet.Identity;

namespace DailyReport.Presentation.Web.Api
{
    public class PostController : ApiController
    {
        private readonly IPatientPostService _patientPostService;

        public PostController(IPatientPostService patientPostService)
        {
            _patientPostService = patientPostService;
        }

        [HttpPost]
        public async Task<IHttpActionResult> Create(PostViewModel.Create model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            _patientPostService.Insert(new PatientPostInsertModel
            {
                TimeCreated = DateTime.Now,
                EmployeeId = ClaimsPrincipal.Current.GetEmployeeId(),
                OrganizationId = ClaimsPrincipal.Current.GetOrganizationId(),
                Description = model.Description,
                Patients = model.Patients
            });

            return Ok();
        }
    }
}
