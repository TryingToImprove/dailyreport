﻿using DailyReport.Domain.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace DailyReport.Presentation.Web.Api
{
    public class OrganizationController : ApiController
    {
        private readonly IOrganizationService _organizationService;

        public OrganizationController(IOrganizationService organizationService)
        {
            _organizationService = organizationService;
        }

        [HttpGet]
        public async Task<IHttpActionResult> FindAll()
        {
            var organizations = _organizationService.FindAll();

            return Ok(Request.CreateResponse(organizations));
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetById(int organizationId)
        {
            var organization = _organizationService.GetById(organizationId);

            return Ok(Request.CreateResponse(organization));
        }
    }
}
