﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using DailyReport.Presentation.Web.Extensions;
using DailyReport.Presentation.Web.Models;
using Microsoft.AspNet.Identity;

namespace DailyReport.Presentation.Web.Api
{
    //[Authorize]
    public class MedicationScheduleHistoryController : ApiController
    {
        private readonly IMedicationScheduleService _medicationScheduleService;

        public MedicationScheduleHistoryController(IMedicationScheduleService medicationScheduleService)
        {
            _medicationScheduleService = medicationScheduleService;
        }

        [HttpGet]
        public async Task<IHttpActionResult> FindAll(int patientId)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var medicationScheduleHistories = _medicationScheduleService.Histories.FindAll(patientId);

            return Ok(medicationScheduleHistories);
        }

        [HttpGet]
        public async Task<IHttpActionResult> Get(int scheduleId)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var medicationScheduleHistories = _medicationScheduleService.Histories.Get(scheduleId);

            return Ok(medicationScheduleHistories);
        }

        [HttpPost]
        public async Task<IHttpActionResult> Add(MedicationScheduleHistoryViewModel.Add model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var medicationScheduleHistory = _medicationScheduleService.Histories.Add(new MedicationScheduleItemHistory
            {
                MedicationScheduleItemId = model.MedicationScheduleItemId,
                TimeCreated = DateTime.Now
            }, ClaimsPrincipal.Current.GetEmployeeId());

            return Ok(medicationScheduleHistory);
        }
    }
}
