﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using AutoMapper;
using CuttingEdge.Conditions;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using DailyReport.Presentation.Web.Extensions;
using DailyReport.Presentation.Web.Models;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace DailyReport.Presentation.Web.Api
{
    public class AccountController : ApiController
    {
        private readonly IOrganizationService _organizationService;
        private readonly IEmployeeService _employeeService;

        public AccountController(IOrganizationService organizationService, IEmployeeService employeeService)
        {
            _organizationService = organizationService;
            _employeeService = employeeService;
        }

        public async Task<IHttpActionResult> Register([FromBody] RegisterAccountViewModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            // Create the organization
            var organization = _organizationService.Insert(new Organization
            {
                Name = model.OrganizationName
            });

            // Map the viewmodel to a employeeInsert model
            var employeeModel = Mapper.Map<EmployeeInsertModel>(model);
            employeeModel.OrganizationId = organization.Id;

            // Create the employee
            var employee = _employeeService.Insert(employeeModel);

            return Ok(employee);
        }

        [Authorize]
        [HttpGet]
        public async Task<IHttpActionResult> Details()
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var organization = _organizationService.GetById(ClaimsPrincipal.Current.GetOrganizationId());
            var employee = _employeeService.GetById(ClaimsPrincipal.Current.GetEmployeeId());
            
            return Ok(new AccountDetailsViewModel
            {
                Account = employee,
                Organization = organization
            });
        }
    }
}
