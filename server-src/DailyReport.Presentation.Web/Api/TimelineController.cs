﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Presentation.Web.Extensions;
using Microsoft.AspNet.Identity;

namespace DailyReport.Presentation.Web.Api
{
    public class TimelineController : ApiController
    {
        private readonly ITimelineService _timelineService;

        public TimelineController(ITimelineService timelineService)
        {
            _timelineService = timelineService;
        }

        [HttpGet]
        public async Task<IHttpActionResult> FindByPatient(int patientId)
        {
            var entries = _timelineService.FindByPatient(patientId);

            return Ok(entries);
        }
    }
}
