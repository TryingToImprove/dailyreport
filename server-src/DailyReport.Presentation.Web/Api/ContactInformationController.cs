﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Presentation.Web.Extensions;
using DailyReport.Presentation.Web.Models;
using Microsoft.AspNet.Identity;

namespace DailyReport.Presentation.Web.Api
{
    public class ContactInformationController : ApiController
    {
        private readonly IPatientService _patientService;
        private readonly IContactInformationService _contactInformationService;

        public ContactInformationController(IPatientService patientService, IContactInformationService contactInformationService)
        {
            _patientService = patientService;
            _contactInformationService = contactInformationService;
        }

        [HttpGet]
        [Authorize]
        public async Task<IHttpActionResult> GetByPatientId(int patientId)
        {
            var contactInformations = _patientService.ContactInformations.FindAll(patientId);

            return Ok(contactInformations);
        }

        [HttpGet]
        [Authorize]
        public async Task<IHttpActionResult> GetTypes()
        {
            var contactInformationTypes = _contactInformationService.GetTypes();

            return Ok(contactInformationTypes);
        }

        [HttpPost]
        public async Task<IHttpActionResult> PatientAdd(int patientId, ContactInformationViewModel.Add model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var contactInformation = _patientService.ContactInformations.Add(patientId, ClaimsPrincipal.Current.GetEmployeeId(), model.Type, model.Value);

            return Ok(contactInformation);
        }

        [HttpDelete]
        public async Task<IHttpActionResult> Remove(int contactInformationId)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var contactInformation = _contactInformationService.Deactivate(contactInformationId);

            return Ok(contactInformation);
        }
    }
}
