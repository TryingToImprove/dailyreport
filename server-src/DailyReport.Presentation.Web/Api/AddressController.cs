﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using DailyReport.Domain.Infrastructure.Services;
using DailyReport.Domain.Models;
using DailyReport.Presentation.Web.Models;

namespace DailyReport.Presentation.Web.Api
{
    public class AddressController : ApiController
    {
        private readonly IPatientService _patientService;

        public AddressController(IPatientService patientService)
        {
            _patientService = patientService;
        }

        [HttpPost]
        public async Task<IHttpActionResult> Add(int patientId, AddressViewModel.Add model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var address = _patientService.Addresses.Insert(patientId, new PatientAddressInsertModel
            {
                City = model.City,
                HouseNumber = model.HouseNumber,
                PostalCode = model.PostalCode,
                Street = model.Street,
                IsPrimary = model.IsPrimary,
                ClearCurrentPrimary = model.ClearCurrentPrimary
            });

            return Ok(address);
        }

        [HttpDelete]
        public async Task<IHttpActionResult> Remove(int patientId, AddressViewModel.Delete model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            _patientService.Addresses.Remove(patientId, model.AddressId);

            return Ok();
        }

        [HttpPut]
        public async Task<IHttpActionResult> Update(int patientId, int id, AddressViewModel.Update model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var address = _patientService.Addresses.Update(patientId, id, new PatientAddressUpdateModel
            {
                City = model.City,
                HouseNumber = model.HouseNumber,
                PostalCode = model.PostalCode,
                Street = model.Street,
                IsPrimary = model.IsPrimary,
                ClearCurrentPrimary = model.ClearCurrentPrimary
            });

            return Ok(address);
        }

        [HttpGet]
        public async Task<IHttpActionResult> FindAll(int patientId)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var addresses = _patientService.Addresses.FindAll(patientId);

            return Ok(addresses);
        }

        [HttpGet]
        public async Task<IHttpActionResult> FindPrimary(int patientId)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var addresses = _patientService.Addresses.FindPrimary(patientId);

            return Ok(addresses);
        }

        [HttpGet]
        public async Task<IHttpActionResult> Find(int patientId, int id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var address = _patientService.Addresses.FindById(patientId, id);

            return Ok(address);
        }
    }
}
