﻿using DailyReport.Domain.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace DailyReport.Presentation.Web.Binders
{
    public class StringSplitOnAttribute : Attribute
    {
        public string SplitOn { get; set; }
    }

    public class StringSplitOnModelBinder : DefaultModelBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if (bindingContext.ModelType != typeof(IEnumerable<string>))
                return base.BindModel(controllerContext, bindingContext);

            var request = controllerContext.HttpContext.Request;

            return request.Form[bindingContext.ModelName]
                 .Split(new[] { ", ", "," }, StringSplitOptions.RemoveEmptyEntries);
        }
    }
}