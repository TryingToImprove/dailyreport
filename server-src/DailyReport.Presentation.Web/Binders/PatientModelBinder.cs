﻿using DailyReport.Domain.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace DailyReport.Presentation.Web.Binders
{
    public class PatientModelBinder : DefaultModelBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if (bindingContext.ModelType != typeof(IEnumerable<Patient>))
                return base.BindModel(controllerContext, bindingContext);

            var request = controllerContext.HttpContext.Request;

            var prefixRegex = new Regex("^" + bindingContext.ModelName + "\\[[\\d]+\\]");
            var patientFormKeys = controllerContext.HttpContext.Request.Form.AllKeys
                .Select(s => prefixRegex.Match(s))
                .Where(m => m != Match.Empty)
                .Select(m => m.Value)
                .Distinct();

            var result = new List<Patient>();

            foreach (var patientFormKey in patientFormKeys)
            {
                int patientId;
                if (int.TryParse(request.Form[patientFormKey + ".Id"], out patientId))
                {
                    result.Add(new Patient
                    {
                        Id = patientId
                    });
                }
            }

            return result;
        }
    }
}