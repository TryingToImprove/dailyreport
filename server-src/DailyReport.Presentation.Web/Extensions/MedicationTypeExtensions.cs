﻿using DailyReport.Domain.Extensions;
using DailyReport.Domain.Models.Core;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;

namespace DailyReport.Presentation.Web.Extensions
{
    public static class MedicationTypeExtensions
    {
        public static IEnumerable<SelectListItem> GetSelectList(this IEnumerable<MedicationType> medicationTypes)
        {
            return medicationTypes.Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString(CultureInfo.InvariantCulture) });
        }
    }
}
