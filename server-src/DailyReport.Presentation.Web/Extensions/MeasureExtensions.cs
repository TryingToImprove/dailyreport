﻿using DailyReport.Domain.Extensions;
using DailyReport.Domain.Models.Core;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;

namespace DailyReport.Presentation.Web.Extensions
{
    public static class MeasureExtensions
    {
        public static IEnumerable<SelectListItem> GetSelectList(this IEnumerable<Measure> measures)
        {
            return measures.Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString(CultureInfo.InvariantCulture) });
        }
    }
}
