﻿using DailyReport.Domain.Extensions;
using DailyReport.Domain.Models;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;

namespace DailyReport.Presentation.Web.Extensions
{
    public static class MedicationExtensions
    {
        public static IEnumerable<SelectListItem> GetSelectList(this IEnumerable<MedicationResultModel> medications)
        {
            return medications.Select(x => new SelectListItem { Text = x.GetDisplayName(), Value = x.Id.ToString(CultureInfo.InvariantCulture) });
        }
    }
}
