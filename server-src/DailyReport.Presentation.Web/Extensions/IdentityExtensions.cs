﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;

namespace DailyReport.Presentation.Web.Extensions
{
    public static class IdentityExtensions
    {
        public static int GetOrganizationId(this ClaimsPrincipal principal)
        {
            return int.Parse(principal.FindFirst("OrganizationId").Value);
        }
        public static int GetOrganizationId(this ClaimsIdentity identity)
        {
            return int.Parse(identity.FindFirst("OrganizationId").Value);
        }

        public static int GetEmployeeId(this ClaimsPrincipal principal)
        {
            return int.Parse(principal.FindFirst("EmployeeId").Value);
        }
    }
}