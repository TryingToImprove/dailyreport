﻿using DailyReport.Domain.Extensions;
using DailyReport.Domain.Models.Core;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;

namespace DailyReport.Presentation.Web.Extensions
{
    public static class ContactInformationExtensions
    {
        public static IEnumerable<SelectListItem> GetSelectList(this IEnumerable<ContactInformationType> types)
        {
            return types.Select(x => new SelectListItem { Text = x.TypeName, Value = x.Id.ToString(CultureInfo.InvariantCulture) });
        }
    }
}
