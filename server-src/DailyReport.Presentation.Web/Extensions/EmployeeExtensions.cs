﻿using DailyReport.Domain.Extensions;
using DailyReport.Domain.Models.Core;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;

namespace DailyReport.Presentation.Web.Extensions
{
    public static class EmployeeExtensions
    {
        public static IEnumerable<SelectListItem> GetSelectList(this IEnumerable<Employee> employees)
        {
            return employees.Select(x => new SelectListItem { Text = x.GetDisplayName(), Value = x.Id.ToString(CultureInfo.InvariantCulture) });
        }
    }
}
