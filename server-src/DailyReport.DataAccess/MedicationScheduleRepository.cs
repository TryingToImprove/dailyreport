﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using Dapper;

namespace DailyReport.DataAccess
{
    public class MedicationScheduleRepository : IMedicationScheduleRepository
    {
        private readonly IDbConnection _connection;

        public MedicationScheduleRepository(IDbConnection connection)
        {
            _connection = connection;
        }

        public MedicationScheduleResultModel GetById(int id)
        {
            const string sql = DefaultMedicationScheduleResultSql
                               + "WHERE MedicationSchedule.Id = @Id";

            return QueryMedicationScheduleResultModels(sql, new { Id = id }).SingleOrDefault();
        }

        public IEnumerable<MedicationScheduleResultModel> FindAll()
        {
            const string sql = DefaultMedicationScheduleResultSql
                                + " WHERE NOT EXISTS (SELECT msc.* FROM MedicationScheduleComplete msc WHERE msc.MedicationScheduleId = MedicationSchedule.Id)"
                                + " AND Patient.IsActive = 1";

            return QueryMedicationScheduleResultModels(sql);
        }

        public IEnumerable<MedicationScheduleResultModel> FindByPatientId(int patientId)
        {
            const string sql = DefaultMedicationScheduleResultSql
                                + " WHERE NOT EXISTS (SELECT msc.* FROM MedicationScheduleComplete msc WHERE msc.MedicationScheduleId = MedicationSchedule.Id AND @CurrentTime >= msc.TimeCreated)"
                                + " AND Patient.Id = @PatientId";

            return QueryMedicationScheduleResultModels(sql, new
            {
                CurrentTime = DateTime.Now,
                PatientId = patientId
            });
        }

        public IEnumerable<MedicationScheduleResultModel> FindAllTodayByPatient(int patientId)
        {
            const string sql = DefaultMedicationScheduleResultSql
                               + " WHERE NOT EXISTS (SELECT msc.* FROM MedicationScheduleComplete msc WHERE msc.MedicationScheduleId = MedicationSchedule.Id)"
                               + " AND Patient.Id = @PatientId"
                               + " AND (CAST(MedicationSchedule.StartDate AS DateTime) >= @CurrentDay AND CAST(MedicationSchedule.StartDate AS DateTime) < @NextDay)"
                               + " AND NextTime.Date < @NextDay"
                               + " AND ((RepeatTimeSpan IS NULL AND NOT EXISTS(SELECT 1 FROM MedicationScheduleItemHistory WHERE MedicationScheduleItemHistory.MedicationScheduleItemId = MedicationScheduleItem.Id)) OR (RepeatTimeSpan IS NOT NULL))";

            return QueryMedicationScheduleResultModels(sql, new
            {
                PatientId = patientId,
                CurrentDay = DateTime.Now.Date,
                NextDay = DateTime.Now.Date.AddDays(1)
            });
        }

        public IEnumerable<MedicationScheduleItemResultModel> GetPatientSchema(int patientId)
        {
            const string sql = @"SELECT 
                                    MedicationScheduleItem.*,
                                    MedicationScheduleItemRepeat.RepeatTimeSpan AS RepeatTimeSpan,
                                    Medication.*,
                                    MedicationType.Name AS Type,
                                    Measure.Name AS Measure,
                                    MedicationBrand.*

                                FROM MedicationScheduleItem
                                JOIN MedicationSchedule ON (MedicationSchedule.Id = MedicationScheduleItem.MedicationScheduleId)
                                JOIN Medication ON (Medication.Id = MedicationScheduleItem.MedicationId)
                                JOIN MedicationBrand ON (MedicationBrand.Id = Medication.MedicationBrandId)
                                JOIN MedicationType ON (MedicationType.Id = Medication.MedicationTypeId)
                                JOIN Measure ON (Measure.Id = Medication.MeasureId)
                                LEFT JOIN MedicationScheduleItemRepeat ON (MedicationScheduleItemRepeat.MedicationScheduleItemId = MedicationScheduleItem.Id)

                                WHERE MedicationSchedule.PatientId = @PatientId
	                                  AND NOT EXISTS (SELECT msc.* FROM MedicationScheduleComplete msc WHERE msc.MedicationScheduleId = MedicationSchedule.Id AND CURRENT_TIMESTAMP >= msc.TimeCreated)";


            return _connection.Query<MedicationScheduleItemResultModel, MedicationResultModel, MedicationBrand, MedicationScheduleItemResultModel>(sql,
                        BuildMedicationScheduleItemResultModel, new { PatientId = patientId }, splitOn: "Id");
        }

        public MedicationScheduleResultModel Complete(MedicationScheduleComplete medicationComplete)
        {
            const string sql = "INSERT INTO MedicationScheduleComplete (MedicationScheduleId, TimeCreated, Description, EmployeeId) VALUES (@MedicationScheduleId, @TimeCreated, @Description, @EmployeeId); "
                                + DefaultMedicationScheduleResultSql + " WHERE MedicationSchedule.Id = @MedicationScheduleId";

            return QueryMedicationScheduleResultModels(sql, medicationComplete).SingleOrDefault();
        }

        public IEnumerable<MedicationScheduleResultModel> FindUpcomings(int organizationId)
        {
            const string sql = DefaultMedicationScheduleResultSql
                                + " WHERE NOT EXISTS (SELECT msc.* FROM MedicationScheduleComplete msc WHERE msc.MedicationScheduleId = MedicationSchedule.Id AND @CurrentTime >= msc.TimeCreated)"
                                + " AND Patient.IsActive = 1"
                                + " AND Patient.OrganizationId = @OrganizationId"
                                + " AND MedicationScheduleItem.ExecuteTime BETWEEN @MinTime AND @MaxTime"
                                + " AND ((RepeatTimeSpan IS NOT NULL AND DATEADD(minute, ((RepeatTimeSpan/600000000) * (SELECT COUNT(1) FROM MedicationScheduleItemHistory WHERE MedicationScheduleItemHistory.MedicationScheduleItemId = MedicationScheduleItem.Id)), CAST(MedicationSchedule.StartDate AS DateTime)) < @CurrentTime) OR (RepeatTimeSpan IS NULL))"
                                + " AND MedicationSchedule.StartDate <= CAST(@CurrentTime AS Date)"
                                + " AND ((RepeatTimeSpan IS NULL AND NOT EXISTS(SELECT 1 FROM MedicationScheduleItemHistory WHERE MedicationScheduleItemHistory.MedicationScheduleItemId = MedicationScheduleItem.Id)) OR (RepeatTimeSpan IS NOT NULL))"
                                + " ORDER BY MedicationScheduleItem.ExecuteTime ASC";

            var timeRange = new TimeRange(DateTime.Now.TimeOfDay, DateTime.Now.AddHours(3).TimeOfDay);
            return QueryMedicationScheduleResultModels(sql, new
            {
                OrganizationId = organizationId,
                CurrentTime = DateTime.Now,
                MinTime = timeRange.Min,
                MaxTime = timeRange.Max
            });
        }

        private IEnumerable<MedicationScheduleResultModel> QueryMedicationScheduleResultModels(string sql, object parameters = null)
        {
            var results = new Dictionary<int, MedicationScheduleResultModel>();

            _connection.Query<MedicationScheduleResultModel, MedicationScheduleItemResultModel, Patient, MedicationResultModel, MedicationBrand, MedicationScheduleComplete, MedicationScheduleResultModel>(sql,
                        (medicationSchedule, medicationScheduleItem, patient, medicationResultModel, medicationBrand, medicationScheduleComplete) =>
                        {
                            MedicationScheduleResultModel viewModel;
                            if (!results.TryGetValue(medicationSchedule.Id, out viewModel))
                            {
                                medicationSchedule.Completed = medicationScheduleComplete;
                                medicationSchedule.Patient = patient;
                                medicationSchedule.Schedules = new List<MedicationScheduleItemResultModel>()
                                {
                                    BuildMedicationScheduleItemResultModel(medicationScheduleItem, medicationResultModel,
                                        medicationBrand)
                                };

                                results.Add(medicationSchedule.Id, medicationSchedule);
                            }
                            else
                            {
                                viewModel.Schedules.Add(BuildMedicationScheduleItemResultModel(medicationScheduleItem, medicationResultModel, medicationBrand));
                            }

                            return viewModel;
                        }, parameters, splitOn: "Id, MedicationScheduleId");

            return results.Values;
        }

        private static MedicationScheduleItemResultModel BuildMedicationScheduleItemResultModel(MedicationScheduleItemResultModel medicationScheduleItem, MedicationResultModel medication, MedicationBrand medicationBrand)
        {
            medication.Brand = medicationBrand;

            medicationScheduleItem.Medication = medication;

            return medicationScheduleItem;
        }

        public MedicationSchedule Insert(MedicationSchedule medicationSchedule)
        {
            medicationSchedule.Id = _connection.ExecuteScalar<int>(@"INSERT INTO MedicationSchedule (PatientId, TimeCreated, StartDate) VALUES (@PatientId, @TimeCreated, @StartDate)
                                                                        SELECT SCOPE_IDENTITY()", medicationSchedule);

            return medicationSchedule;
        }

        private const string DefaultMedicationScheduleResultSql = @"SELECT 
                                                                        MedicationSchedule.*,
                                                                        (CASE WHEN (RepeatTimeSpan IS NULL OR RepeatTimeSpan = 0)
                                                                            THEN 0
                                                                            ELSE 1
                                                                        END) AS ScheduleType,
                                                                        MedicationScheduleItem.*,
                                                                        MedicationScheduleItemRepeat.RepeatTimeSpan AS RepeatTimeSpan,
                                                                        NextTime.Date AS NextTime,
                                                                        Patient.*,
                                                                        Medication.*,
                                                                        MedicationType.Name AS Type,
                                                                        Measure.Name AS Measure,
                                                                        MedicationBrand.*,
                                                                        MedicationScheduleComplete.*

                                                                    FROM MedicationScheduleItem
	                                                                JOIN MedicationSchedule ON (MedicationSchedule.Id = MedicationScheduleItem.MedicationScheduleId)
	                                                                JOIN Patient ON (Patient.Id = MedicationSchedule.PatientId)
	                                                                JOIN Medication ON (Medication.Id = MedicationScheduleItem.MedicationId)
	                                                                JOIN MedicationBrand ON (MedicationBrand.Id = Medication.MedicationBrandId)
	                                                                JOIN MedicationType ON (MedicationType.Id = Medication.MedicationTypeId)
	                                                                JOIN Measure ON (Measure.Id = Medication.MeasureId)
	                                                                LEFT JOIN MedicationScheduleItemRepeat ON (MedicationScheduleItemRepeat.MedicationScheduleItemId = MedicationScheduleItem.Id)
                                                                    LEFT JOIN MedicationScheduleComplete ON (MedicationScheduleComplete.MedicationScheduleId = MedicationSchedule.Id) 

                                                                    OUTER APPLY ( 
                                                                        SELECT (CASE WHEN (RepeatTimeSpan IS NOT NULL AND RepeatTimeSpan >= 1) 
                                                                                    THEN DATEADD(minute, ((RepeatTimeSpan/600000000) * (SELECT COUNT(1) FROM MedicationScheduleItemHistory WHERE MedicationScheduleItemHistory.MedicationScheduleItemId = MedicationScheduleItem.Id)), CAST((CONVERT(nvarchar(30), MedicationSchedule.StartDate)+ ' ' + CONVERT(nvarchar(30), CAST(MedicationScheduleItem.ExecuteTime AS time(0)))) AS DateTime))
                                                                                    ELSE NULL
                                                                               END) AS Date
                                                                    ) AS NextTime ";


    }
}
