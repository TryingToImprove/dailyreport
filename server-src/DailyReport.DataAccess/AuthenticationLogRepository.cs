﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Infrastructure.Repositories;
using Dapper;

namespace DailyReport.DataAccess
{
    public class AuthenticationLogRepository : IAuthenticationLogRepository
    {
        private readonly IDbConnection _connection;

        public AuthenticationLogRepository(IDbConnection connection)
        {
            _connection = connection;
        }

        public void Insert(int authenticationIdentityId, DateTime timeCreated)
        {
            _connection.Execute("INSERT INTO AuthenticationLog (AuthenticationId, TimeCreated) VALUES (@AuthenticationIdentityId, @TimeCreated)", new
                {
                    AuthenticationIdentityId = authenticationIdentityId,
                    TimeCreated = timeCreated
                });
        }
    }
}
