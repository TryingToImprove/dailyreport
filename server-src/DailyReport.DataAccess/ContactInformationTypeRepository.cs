﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Models.Core;
using Dapper;

namespace DailyReport.DataAccess
{
    public class ContactInformationTypeRepository : IContactInformationTypeRepository
    {
        private readonly IDbConnection _connection;

        public ContactInformationTypeRepository(IDbConnection connection)
        {
            _connection = connection;
        }

        public IEnumerable<ContactInformationType> FindAll()
        {
            const string sql = "SELECT * FROM ContactInformationType";

            return _connection.Query<ContactInformationType>(sql);
        }

        public ContactInformationType Find(string dataType)
        {
            const string sql = "SELECT * FROM ContactInformationType WHERE TypeName = @DataType";

            return _connection.Query<ContactInformationType>(sql, new { DataType = dataType }).SingleOrDefault();
        }
    }
}
