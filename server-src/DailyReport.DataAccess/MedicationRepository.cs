﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using Dapper;

namespace DailyReport.DataAccess
{
    public class MedicationRepository : IMedicationRepository
    {
        private readonly IDbConnection _connection;

        public MedicationRepository(IDbConnection connection)
        {
            _connection = connection;
        }

        public IEnumerable<MedicationResultModel> FindAll()
        {
            return QueryMedicationResult(MedicationSql, null);
        }

        public MedicationResultModel Insert(Medication medication)
        {
            return QueryMedicationResult("INSERT INTO Medication (Name, MedicationBrandId, MedicationTypeId, MeasureId, Weight) VALUES (@Name, @MedicationBrandId, @MedicationTypeId, @MeasureId, @Weight);" +
                                         MedicationSql + "WHERE Medication.Id = SCOPE_IDENTITY()", medication).FirstOrDefault();

            //return medication;
        }

        public IEnumerable<MedicationResultModel> Search(string query)
        {
            return QueryMedicationResult(MedicationSql + " WHERE Medication.Name LIKE @Query", new
            {
                Query = string.Format("%{0}%", query)
            });
        }

        public MedicationResultModel GetById(int id)
        {
            const string sql = MedicationSql + "WHERE Medication.Id = @Id";

            return QueryMedicationResult(sql, new { Id = id }).SingleOrDefault();
        }

        private IEnumerable<MedicationResultModel> QueryMedicationResult(string sql, object parameters)
        {
            return _connection.Query<Medication, MedicationBrand, MedicationType, Measure, MedicationResultModel>(sql,
                (medication, medicationBrand, medicationType, measure) => new MedicationResultModel()
                {
                    Id = medication.Id,
                    Name = medication.Name,
                    Weight = medication.Weight,
                    Brand = medicationBrand,
                    Type = medicationType.Name,
                    Measure = measure.Name
                }, parameters);
        }

        private const string MedicationSql = "SELECT Medication.*, MedicationBrand.*, MedicationType.*, Measure.* FROM Medication " +
                                             "JOIN MedicationBrand ON (MedicationBrand.Id = Medication.MedicationBrandId) " +
                                             "JOIN MedicationType ON (MedicationType.Id = Medication.MedicationTypeId) " +
                                             "JOIN Measure ON (Measure.Id = Medication.MeasureId) ";
    }
}
