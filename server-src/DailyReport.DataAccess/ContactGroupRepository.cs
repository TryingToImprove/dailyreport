﻿using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace DailyReport.DataAccess
{
    public class ContactGroupRepository : IContactGroupRepository
    {
        private readonly IDbConnection _connection;

        public ContactGroupRepository(IDbConnection connection)
        {
            _connection = connection;
        }

        public IEnumerable<ContactGroupResultModel> FindAll()
        {
            const string sql = DistinctSqlPieces.SelectSql +
                               DistinctSqlPieces.GroupBySql +
                               DistinctSqlPieces.SortSql;

            return QueryContactGroups(sql);
        }

        public IEnumerable<ContactGroupResultModel> FindByPatient(int patientId)
        {
            const string sql = DistinctSqlPieces.SelectSql +
                               "JOIN Contact_ContactGroup ON (Contact_ContactGroup.ContactGroupId = ContactGroup.Id) " +
                               "JOIN Contact ON (Contact.Id = Contact_ContactGroup.ContactId) " +
                               "JOIN Contact_Patient ON (Contact_Patient.ContactId = Contact.Id) " +
                               "WHERE Contact_Patient.PatientId = @PatientId " +
                               DistinctSqlPieces.GroupBySql +
                               DistinctSqlPieces.SortSql;

            return QueryContactGroups(sql, new
            {
                PatientId = patientId
            });
        }

        public ContactGroupResultModel GetById(int id)
        {
            const string sql = DistinctSqlPieces.SelectSql +
                               "WHERE ContactGroup.Id = @Id " + 
                               DistinctSqlPieces.GroupBySql;

            return QueryContactGroups(sql, new { Id = id }).SingleOrDefault();
        }

        private IEnumerable<ContactGroupResultModel> QueryContactGroups(string sql, object parameters = null)
        {
            return _connection.Query<ContactGroupResultModel>(sql, parameters);
        }

        private static class DistinctSqlPieces
        {
            public const string SelectSql = "SELECT DISTINCT ContactGroup.*, COUNT(1) AS NumberOfContacts " +
                                            "FROM ContactGroup ";

            public const string GroupBySql = "GROUP BY ContactGroup.Id, ContactGroup.Name ";

            public const string SortSql = "ORDER BY COUNT(1) DESC ";
        }
    }
}
