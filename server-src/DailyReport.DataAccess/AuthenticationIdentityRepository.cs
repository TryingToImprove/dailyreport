﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using Dapper;

namespace DailyReport.DataAccess
{
    public class AuthenticationIdentityRepository : IAuthenticationIdentityRepository
    {
        private readonly IDbConnection _connection;

        public AuthenticationIdentityRepository(IDbConnection connection)
        {
            _connection = connection;
        }

        public AuthenticationIdentityResultModel Insert(AuthenticationIdentityInsertModel model)
        {
            const string sql = @"
                -- We need to be 100% sure that the identity getting created is for real
                IF NOT EXISTS (SELECT 1 FROM AuthenticationIdentityKey WHERE AuthenticationIdentityKey.AuthenticationKey = @AuthenticationKey) 
	                RAISERROR ('The authentication key was not found', 16, 1);

                DECLARE @@AuthenticationIdentityId int;

                -- Insert identity
                INSERT INTO AuthenticationIdentity (Username, IsActive, TimeCreated) VALUES (@Username, 1, CURRENT_TIMESTAMP);

                SET @@AuthenticationIdentityId = SCOPE_IDENTITY();

                -- Insert password
                INSERT INTO AuthenticationPassword (AuthenticationIdentityId, IsActive, Password, TimeCreated) VALUES (@@AuthenticationIdentityId, 1, @Password, CURRENT_TIMESTAMP);

                -- Mark the AuthenticationKey as used
                INSERT INTO AuthenticationIdentityKey_AuthenticationIdentity (AuthenticationKey, AuthenticationIdentityId) VALUES (@AuthenticationKey, @@AuthenticationIdentityId); "
                + DefaultSelectSql + " WHERE AuthenticationIdentity.Id = @@AuthenticationIdentityId";

            return _connection.Query<AuthenticationIdentityResultModel>(sql, model).SingleOrDefault();
        }

        public AuthenticationIdentityResultModel GetById(int authenticationIdentityId)
        {
            const string sql = DefaultSelectSql + "WHERE AuthenticationIdentity.Id = @Id";

            return _connection.Query<AuthenticationIdentityResultModel>(sql, new { Id = authenticationIdentityId }).SingleOrDefault();
        }

        public AuthenticationResultModel<Employee> FindEmployee(string username, string password)
        {
            const string sql = @"SELECT Employee.*, AuthenticationIdentity.* FROM Employee
                                JOIN EmployeeAuthenticationIdentityKey ON (EmployeeAuthenticationIdentityKey.EmployeeId = Employee.Id)
                                JOIN AuthenticationIdentityKey ON (AuthenticationIdentityKey.AuthenticationKey = EmployeeAuthenticationIdentityKey.AuthenticationKey)
                                JOIN AuthenticationIdentityKey_AuthenticationIdentity ON (AuthenticationIdentityKey_AuthenticationIdentity.AuthenticationKey = AuthenticationIdentityKey.AuthenticationKey)
                                JOIN AuthenticationIdentity ON (AuthenticationIdentity.Id = AuthenticationIdentityKey_AuthenticationIdentity.AuthenticationIdentityId)

                                OUTER APPLY (
	                                SELECT TOP 1 Password FROM AuthenticationPassword WHERE AuthenticationPassword.AuthenticationIdentityId = AuthenticationIdentity.Id AND AuthenticationPassword.IsActive = 1 ORDER BY TimeCreated DESC
                                ) AS IdentityPassword

                                WHERE AuthenticationIdentity.IsActive = 1 AND Employee.IsActive = 1 AND IdentityPassword.Password = @Password AND AuthenticationIdentity.Username = @Username";

            var parameters = new
            {
                Username = username,
                Password = password
            };

            return _connection.Query<Employee, AuthenticationIdentity, AuthenticationResultModel<Employee>>(sql, (employee, authenticationIdentity) => new AuthenticationResultModel<Employee>()
            {
                User = employee,
                Identity = authenticationIdentity
            }, parameters).SingleOrDefault();
        }

        public void InsertAuthenticationIdentityKey(string authenticationKey, DateTime timeCreated)
        {
            _connection.Execute("INSERT INTO AuthenticationIdentityKey (AuthenticationKey, TimeCreated) VALUES (@AuthenticationKey, @TimeCreated);", new
                {
                    AuthenticationKey = authenticationKey,
                    TimeCreated = timeCreated
                });
        }

        public bool IsValidAuthenticationKey(string authenticationKey)
        {
            return _connection.ExecuteScalar<bool>(@"SELECT CASE WHEN EXISTS (SELECT 1 FROM AuthenticationIdentityKey 
                                                                                WHERE AuthenticationIdentityKey.AuthenticationKey = @AuthenticationKey
                                                                                AND NOT EXISTS (SELECT 1 FROM AuthenticationIdentityKey_AuthenticationIdentity WHERE AuthenticationIdentityKey_AuthenticationIdentity.AuthenticationKey = AuthenticationIdentityKey.AuthenticationKey))
                                                        THEN 1
	                                                    ELSE 0
                                                    END", new { AuthenticationKey = authenticationKey });
        }

        public void RegisterEmployeeRelationship(int authenticationIdentityId, int employeeId)
        {
            const string sql = "INSERT INTO AuthenticationIdentity_Employee (EmployeeId, AuthenticationIdentityId) VALUES (@EmployeeId, @AuthenticationIdentityId); ";

            _connection.Execute(sql, new
            {
                EmployeeId = employeeId,
                AuthenticationIdentityId = authenticationIdentityId
            });
        }

        const string DefaultSelectSql = "SELECT AuthenticationIdentity.Username, IdentityPassword.Password, AuthenticationIdentity.IsActive FROM AuthenticationIdentity " +
                                        "OUTER APPLY (" +
                                        "SELECT TOP 1 Password FROM AuthenticationPassword WHERE AuthenticationPassword.AuthenticationIdentityId = AuthenticationIdentity.Id AND AuthenticationPassword.IsActive = 1 ORDER BY TimeCreated DESC" +
                                        ") IdentityPassword ";
    }
}
