﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using Dapper;

namespace DailyReport.DataAccess
{
    public class MedicationBrandRepository : IMedicationBrandRepository
    {
        private readonly IDbConnection _connection;

        public MedicationBrandRepository(IDbConnection connection)
        {
            _connection = connection;
        }

        IEnumerable<MedicationBrand> IMedicationBrandRepository.FindAll()
        {
            return _connection.Query<MedicationBrand>("SELECT * FROM MedicationBrand");
        }

        public MedicationBrand Insert(MedicationBrand medicationBrand)
        {
            medicationBrand.Id = _connection.ExecuteScalar<int>("INSERT INTO MedicationBrand (Name) VALUES (@Name);" +
                                                                "SELECT SCOPE_IDENTITY()", medicationBrand);

            return medicationBrand;
        }


        public MedicationBrand GetById(int id)
        {
            return _connection.Query<MedicationBrand>("SELECT * FROM MedicationBrand WHERE Id = @Id", new { Id = id })
                        .SingleOrDefault();
        }
    }
}
