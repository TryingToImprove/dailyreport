﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Infrastructure;
using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using Dapper;

namespace DailyReport.DataAccess
{
    public class ContactInformationRepository : IContactInformationRepository
    {
        private readonly IDbConnection _connection;

        public ContactInformationRepository(IDbConnection connection)
        {
            _connection = connection;
        }

        public ContactInformationResultModel Insert(ContactInformation model)
        {
            const string sql = "INSERT INTO ContactInformation (ContactInformationTypeId, Data, TimeCreated, IsActive, EmployeeId) VALUES (@ContactInformationTypeId, @Data, @TimeCreated, @IsActive, @EmployeeId); " +
                               DefaultSelectSql + " WHERE ContactInformation.Id = SCOPE_IDENTITY()";

            return _connection.Query<ContactInformationResultModel>(sql, model).SingleOrDefault();
        }

        public IEnumerable<ContactInformationResultModel> FindAll()
        {
            const string sql = DefaultSelectSql + "WHERE " + DefaultFilterStatement;

            return _connection.Query<ContactInformationResultModel>(sql);
        }

        public IEnumerable<ContactInformationResultModel> FindPatientContactInformations(int patientId)
        {
            const string sql = DefaultSelectSql +
                               "JOIN Patient_ContactInformation ON (Patient_ContactInformation.ContactInformationId = ContactInformation.Id) " +
                               "WHERE Patient_ContactInformation.PatientId = @PatientId AND " + DefaultFilterStatement;


            return _connection.Query<ContactInformationResultModel>(sql, new { PatientId = patientId });
        }

        public IEnumerable<ContactInformationResultModel> FindEmployeeContactInformations(int employeeId)
        {
            const string sql = DefaultSelectSql +
                               "JOIN Employee_ContactInformation ON (Employee_ContactInformation.ContactInformationId = ContactInformation.Id) " +
                               "WHERE Employee_ContactInformation.EmployeeId = @EmployeeId AND " + DefaultFilterStatement;


            return _connection.Query<ContactInformationResultModel>(sql, new { EmployeeId = employeeId });
        }

        public IEnumerable<ContactInformationResultModel> FindContactContactInformations(int contactId)
        {
            const string sql = DefaultSelectSql +
                               "JOIN Contact_ContactInformation ON (Contact_ContactInformation.ContactInformationId = ContactInformation.Id) " +
                               "WHERE Contact_ContactInformation.ContactId = @ContactId AND " + DefaultFilterStatement;


            return _connection.Query<ContactInformationResultModel>(sql, new { ContactId = contactId });
        }

        public ContactInformationResultModel GetById(int contactInformationId)
        {
            const string sql = DefaultSelectSql + " WHERE ContactInformation.Id = @ContactInformationId";

            return _connection.Query<ContactInformationResultModel>(sql, new { ContactInformationId = contactInformationId })
                    .SingleOrDefault();
        }

        public void RegisterPatientRelationship(int contactInformationId, int patientId)
        {
            const string sql = "INSERT INTO Patient_ContactInformation VALUES (@ContactInformationId, @PatientId)";

            _connection.Execute(sql, new
            {
                ContactInformationId = contactInformationId,
                PatientId = patientId
            });
        }

        public void RegisterEmployeeRelationship(int contactInformationId, int employeeId)
        {
            const string sql = "INSERT INTO Employee_ContactInformation VALUES (@ContactInformationId, @EmployeeId)";

            _connection.Execute(sql, new
            {
                ContactInformationId = contactInformationId,
                EmployeeId = employeeId
            });
        }

        public void RegisterContactRelationship(int contactInformationId, int contactId)
        {
            const string sql = "INSERT INTO Contact_ContactInformation (ContactInformationId, ContactId) VALUES (@ContactInformationId, @ContactId)";

            _connection.Execute(sql, new
             {
                 ContactInformationId = contactInformationId,
                 ContactId = contactId
             });
        }

        public void Update(ContactInformation model)
        {
            const string sql = "UPDATE ContactInformation SET ContactInformationTypeId = @ContactInformationTypeId, Data = @Data " +
                               "WHERE ContactInformation.Id = @Id";

            _connection.Execute(sql, model);

        }

        public ContactInformationResultModel Deactivate(int contactInformationId)
        {
            return SetActiveState(contactInformationId, false);
        }

        public ContactInformationResultModel Reactivate(int contactInformationId)
        {
            return SetActiveState(contactInformationId, true);
        }

        private ContactInformationResultModel SetActiveState(int contactInformationId, bool isActive)
        {
            const string sql = "UPDATE ContactInformation SET ContactInformation.IsActive = @IsActive  WHERE ContactInformation.Id = @Id; " +
                               DefaultSelectSql + " WHERE ContactInformation.Id = @Id";

            return _connection.Query<ContactInformationResultModel>(sql, new
            {
                IsActive = isActive,
                Id = contactInformationId
            }).SingleOrDefault();
        }

        const string DefaultSelectSql = "SELECT ContactInformation.*, ContactInformationType.TypeName AS Type, ContactInformation.Id AS TypeId FROM ContactInformation " +
                                        "JOIN ContactInformationType ON (ContactInformationType.Id = ContactInformation.ContactInformationTypeId) ";

        private const string DefaultFilterStatement = "ContactInformation.IsActive = 1";

    }
}
