﻿using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace DailyReport.DataAccess
{
    public class ContactRepository : IContactRepository
    {
        private readonly IDbConnection _connection;

        public ContactRepository(IDbConnection connection)
        {
            _connection = connection;
        }

        public IEnumerable<ContactResultModel> FindAll()
        {
            return QueryResultModels(DefaultSelectSql);
        }

        public IEnumerable<ContactResultModel> FindByPatient(int patientId)
        {
            const string sql = DefaultSelectSql +
                               "JOIN Contact_Patient ON (Contact_Patient.ContactId = Contact.Id) " +
                               "WHERE Contact_Patient.PatientId = @PatientId";

            return QueryResultModels(sql, new
            {
                PatientId = patientId
            });
        }

        public IEnumerable<ContactResultModel> FindByPatient(int patientId, int contactGroupId)
        {
            const string sql = DefaultSelectSql +
                               "JOIN Contact_Patient ON (Contact_Patient.ContactId = Contact.Id) " +
                               "WHERE Contact_Patient.PatientId = @PatientId AND ContactGroup.Id = @ContactGroupId";

            return QueryResultModels(sql, new
            {
                PatientId = patientId,
                ContactGroupId = contactGroupId
            });
        }

        public ContactResultModel GetById(int id)
        {
            const string sql = DefaultSelectSql +
                               "WHERE Contact.Id = @ContactId";

            return QueryResultModels(sql, new { ContactId = id }).SingleOrDefault();
        }

        public ContactResultModel Insert(ContactbookInsertModel model)
        {
            // Create parameters
            var args = new DynamicParameters();
            args.Add("Firstname", model.Firstname);
            args.Add("Lastname", model.Lastname);
            args.Add("TimeCreated", model.TimeCreated);
            args.Add("ContactType", model.Type);
            args.Add("EmployeeId", model.EmployeeId);

            var strBuilder = new StringBuilder();

            strBuilder.AppendLine("DECLARE @@contactId int;");

            // Insert contact
            strBuilder.AppendLine("INSERT INTO Contact (Firstname, Lastname, TimeCreated, EmployeeId) VALUES (@Firstname, @Lastname, @TimeCreated, @EmployeeId);");
            strBuilder.AppendLine("SET @@contactId = SCOPE_IDENTITY()");

            // Insert contactType if exists
            strBuilder.AppendLine("IF NOT @ContactType IS NULL AND @ContactType <> ''");
            strBuilder.AppendLine("BEGIN");

            // Get or create
            strBuilder.Append(GetOrCreateContactType("@ContactType", "@@contactTypeId"));

            // Insert reference
            strBuilder.AppendLine("INSERT INTO Contact_ContactType (ContactId, ContactTypeId) VALUES (@@contactId, @@contactTypeId);");
            strBuilder.AppendLine("END");

            // Insert all groups
            strBuilder.AppendLine("DECLARE @@contactGroupId int;");
            var groups = model.Groups.ToArray();
            for (var i = 0; i < groups.Length; i++)
            {
                var groupName = groups[i];
                var parameterName = string.Format("contactGroupName{0}", i);

                // Add parameter
                args.Add(parameterName, groupName);

                // Add SQL
                strBuilder.Append(GetOrCreateContactGroup("@" + parameterName, "@@contactGroupId"));

                // Add reference
                strBuilder.AppendLine("INSERT INTO Contact_ContactGroup (ContactId, ContactGroupId) VALUES (@@contactId, @@contactGroupId);");
            }

            // Insert all contact informations
            strBuilder.AppendLine("DECLARE @@contactInformationTypeId int;");
            var contactInformations = model.ContactInformations.ToArray();
            for (var i = 0; i < contactInformations.Length; i++)
            {
                var contactInformation = contactInformations[i];
                var dataTypeParameterName = string.Format("contactInformationDataType{0}", i);
                var dataParameterName = string.Format("contactInformationData{0}", i);

                // Add SQL parameter
                args.Add(dataTypeParameterName, contactInformation.DataType);
                args.Add(dataParameterName, contactInformation.Value);

                // Add SQL
                strBuilder.Append(GetOrCreateContactInformationType("@" + dataTypeParameterName, "@@contactInformationTypeId"));
                strBuilder.AppendFormat("INSERT INTO ContactInformation (ContactInformationTypeId, Data, TimeCreated, IsActive, EmployeeId) VALUES (@@contactInformationTypeId, {0}, @TimeCreated, 1, @EmployeeId) ", "@" + dataParameterName);
                strBuilder.Append("INSERT INTO Contact_ContactInformation (ContactId, ContactInformationId) VALUES (@@contactId, SCOPE_IDENTITY()) ");
            }

            strBuilder.AppendLine(DefaultSelectSql + " WHERE Contact.Id = @@contactId");

            return QueryResultModels(strBuilder.ToString(), args).Single();
        }

        public void Update(int contactId, ContactUpdateModel model)
        {
            // Create parameters
            var args = new DynamicParameters();
            args.Add("ContactId", contactId);

            var strBuilder = new StringBuilder();

            // Insert contactType if exists
            if (!string.IsNullOrWhiteSpace(model.Type) && !model.RemoveType)
            {
                args.Add("ContactType", model.Type);

                strBuilder.AppendLine("IF NOT @ContactType IS NULL AND @ContactType <> ''");
                strBuilder.AppendLine("BEGIN");

                // Get or create
                strBuilder.Append(GetOrCreateContactType("@ContactType", "@@contactTypeId"));

                // Remove old reference
                strBuilder.AppendLine("DELETE FROM Contact_ContactType WHERE ContactId = @ContactId;");

                // Insert reference
                strBuilder.AppendLine("INSERT INTO Contact_ContactType (ContactId, ContactTypeId) VALUES (@ContactId, @@contactTypeId);");

                strBuilder.AppendLine("END");
            }
            else if (model.RemoveType)
            {
                // Remove old reference
                strBuilder.AppendLine("DELETE FROM Contact_ContactType WHERE ContactId = @ContactId;");
            }

            // Insert AddedGroups
            var addedGroups = model.AddedGroups.ToArray();
            if (addedGroups.Any())
            {
                strBuilder.AppendLine("DECLARE @@addedContactGroupId int;");

                for (var i = 0; i < addedGroups.Length; i++)
                {
                    var groupName = addedGroups[i];
                    var parameterName = string.Format("contactGroupName{0}", i);

                    // Add parameter
                    args.Add(parameterName, groupName);

                    // Add SQL
                    strBuilder.Append(GetOrCreateContactGroup("@" + parameterName, "@@addedContactGroupId"));

                    // Add reference if it not already exists
                    strBuilder.AppendLine("IF NOT EXISTS (SELECT 1 FROM Contact_ContactGroup WHERE ContactId = @ContactId AND ContactGroupId = @@addedContactGroupId)");
                    strBuilder.AppendLine("INSERT INTO Contact_ContactGroup (ContactId, ContactGroupId) VALUES (@ContactId, @@addedContactGroupId);");
                }
            }

            // Remove RemovedGroups
            var removedGroups = model.RemovedGroups.ToArray();
            if (removedGroups.Any())
            {
                for (var i = 0; i < removedGroups.Length; i++)
                {
                    var groupName = removedGroups[i];
                    var parameterName = string.Format("removeContactGroupName{0}", i);

                    // Add parameter
                    args.Add(parameterName, groupName);

                    strBuilder.AppendFormat("IF EXISTS (SELECT 1 FROM ContactGroup WHERE Name = {0})\n", "@" + parameterName);
                    strBuilder.AppendFormat("DELETE FROM Contact_ContactGroup WHERE ContactId = @ContactId AND ContactGroupId = (SELECT TOP 1 Id FROM ContactGroup WHERE Name = {0})\n", "@" + parameterName);
                }
            }

            // Update properties
            if (model.Properties.Any())
            {
                strBuilder.AppendLine("UPDATE Contact SET ");

                strBuilder.AppendLine(string.Join(", ", model.Properties.Select(x =>
                {
                    var parameterName = "update" + x.Key;
                    args.Add(parameterName, x.Value);

                    return string.Format("{0} = {1}", x.Key, "@" + parameterName);
                })));

                strBuilder.AppendLine(" WHERE Contact.Id = @ContactId");
            }

            _connection.Execute(strBuilder.ToString(), args);
        }

        public void Delete(int contactId)
        {
            const string sql = "DELETE FROM Contact WHERE Id = @ContactId";

            _connection.Execute(sql, new { ContactId = contactId });
        }

        public void RegisterPatientReference(int patientId, int contactId)
        {
            const string sql = "INSERT INTO Contact_Patient (PatientId, ContactId) VALUES (@PatientId, @ContactId); ";

            _connection.Execute(sql, new
            {
                PatientId = patientId,
                ContactId = contactId
            });
        }

        private IEnumerable<ContactResultModel> QueryResultModels(string sql, object parameters = null)
        {
            var results = new Dictionary<int, ContactResultModel>();

            _connection.Query<ContactResultModel, ContactType, ContactGroup, ContactInformationResultModel, ContactResultModel>(sql,
                 (contact, contactType, contactGroup, contactInformation) =>
                 {
                     ContactResultModel model;
                     if (results.TryGetValue(contact.Id, out model))
                     {
                         // This can be faster
                         if (contactGroup != null && model.Groups.All(x => x.Id != contactGroup.Id))
                             model.Groups.Add(contactGroup);

                         // This can be faster
                         if (contactInformation != null && model.ContactInformations.All(x => x.Id != contactInformation.Id))
                             model.ContactInformations.Add(contactInformation);

                         return model;
                     }

                     contact.ContactType = contactType;
                     contact.Groups = new List<ContactGroup>();
                     contact.ContactInformations = new List<ContactInformationResultModel>();

                     // It is possible for the contactGroup to be null
                     if (contactGroup != null)
                         contact.Groups.Add(contactGroup);

                     // It is possible for the contactGroup to be null
                     if (contactInformation != null)
                         contact.ContactInformations.Add(contactInformation);

                     results.Add(contact.Id, contact);

                     return contact;
                 }, parameters);

            return results.Values;
        }

        private static StringBuilder GetOrCreateContactType(string contactType, string contactTypeId)
        {
            var strBuilder = new StringBuilder();

            strBuilder.AppendFormat("DECLARE {0} int;\n", contactTypeId);
            strBuilder.AppendFormat("IF NOT EXISTS (SELECT 1 FROM ContactType WHERE Name = {0})\n", contactType);
            strBuilder.AppendLine("BEGIN");
            strBuilder.AppendFormat("	INSERT INTO ContactType (Name) VALUES ({0});\n", contactType);
            strBuilder.AppendFormat("SET {0} = SCOPE_IDENTITY();\n", contactTypeId);
            strBuilder.AppendLine("END");
            strBuilder.AppendLine("ELSE");
            strBuilder.AppendFormat("SELECT {0} = Id FROM ContactType WHERE Name = {1}\n", contactTypeId, contactType);

            return strBuilder;
        }

        private static StringBuilder GetOrCreateContactGroup(string parameterName, string outputName)
        {
            var strBuilder = new StringBuilder();

            strBuilder.AppendFormat("IF NOT EXISTS (SELECT 1 FROM ContactGroup WHERE Name = {0})\n", parameterName);
            strBuilder.AppendLine("BEGIN");
            strBuilder.AppendFormat("INSERT INTO ContactGroup (Name) VALUES ({0});\n", parameterName);
            strBuilder.AppendFormat("SET {0} = SCOPE_IDENTITY();\n", outputName);
            strBuilder.AppendLine("END");
            strBuilder.AppendLine("ELSE");
            strBuilder.AppendFormat("SELECT {1} = Id FROM ContactGroup WHERE Name = {0}\n", parameterName, outputName);

            return strBuilder;
        }

        private static StringBuilder GetOrCreateContactInformationType(string parameterName, string outputName)
        {
            var strBuilder = new StringBuilder();

            strBuilder.AppendFormat("IF NOT EXISTS (SELECT 1 FROM ContactInformationType WHERE TypeName = {0})\n", parameterName);
            strBuilder.AppendLine("BEGIN");
            strBuilder.AppendFormat("INSERT INTO ContactInformationType (TypeName) VALUES ({0});\n", parameterName);
            strBuilder.AppendFormat("SET {0} = SCOPE_IDENTITY();\n", outputName);
            strBuilder.AppendLine("END");
            strBuilder.AppendLine("ELSE");
            strBuilder.AppendFormat("SELECT {1} = Id FROM ContactInformationType WHERE TypeName = {0}\n", parameterName, outputName);

            return strBuilder;
        }

        private const string DefaultSelectSql = "SELECT Contact.*, ContactType.*, ContactGroup.*, ContactInformation.*, ContactInformationType.TypeName AS Type, ContactInformation.Id AS TypeId FROM Contact " +
                                                "LEFT JOIN Contact_ContactType ON (Contact_ContactType.ContactId = Contact.Id) " +
                                                "LEFT JOIN ContactType ON (ContactType.Id = Contact_ContactType.ContactTypeId) " +
                                                "LEFT JOIN Contact_ContactGroup ON (Contact_ContactGroup.ContactId = Contact.Id) " +
                                                "LEFT JOIN ContactGroup ON (ContactGroup.Id = Contact_ContactGroup.ContactGroupId) " +
                                                "LEFT JOIN Contact_ContactInformation ON (Contact_ContactInformation.ContactId = Contact.Id) " +
                                                "LEFT JOIN ContactInformation ON (ContactInformation.Id = Contact_ContactInformation.ContactInformationId) " +
                                                "LEFT JOIN ContactInformationType ON (ContactInformationType.Id = ContactInformation.ContactInformationTypeId) ";
    }
}
