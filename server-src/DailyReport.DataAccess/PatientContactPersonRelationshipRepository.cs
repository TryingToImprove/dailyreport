﻿using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace DailyReport.DataAccess
{
    public class PatientContactPersonRelationshipRepository : IPatientContactPersonRelationshipRepository
    {
        private readonly IDbConnection _connection;

        public PatientContactPersonRelationshipRepository(IDbConnection connection)
        {
            _connection = connection;
        }

        public PatientContactPersonRelationshipResultModel Insert(PatientContactPersonRelationship patientContactPersonRelationship, bool completePreviousContactPersons)
        {
            var sqlBuilder = new StringBuilder();

            // End all previous relationships if true
            if (completePreviousContactPersons)
            {
                const string endAllPreviousContctPersonsSql = "INSERT INTO PatientContactPersonRelationshipEnded (PatientContactPersonRelationshipId, TimeCreated) " +
                                                              "SELECT Id, @TimeCreated FROM PatientContactPersonRelationship " +
                                                              "WHERE NOT EXISTS (SELECT PatientContactPersonRelationshipId FROM PatientContactPersonRelationshipEnded WHERE PatientContactPersonRelationshipId = Id) " +
                                                              "AND PatientId = @PatientId; ";

                sqlBuilder.Append(endAllPreviousContctPersonsSql);
            }

            // Insert relationship
            sqlBuilder.Append("INSERT INTO PatientContactPersonRelationship (PatientId, EmployeeId, TimeCreated) VALUES (@PatientId, @EmployeeId, @TimeCreated); ");

            // Get the newly inserted relationship
            sqlBuilder.Append(SelectResultModelSql + "WHERE PatientContactPersonRelationship.Id = SCOPE_IDENTITY()");

            return QueryResultModels(sqlBuilder.ToString(), patientContactPersonRelationship).Single();
        }

        public PatientContactPersonRelationshipResultModel Complete(int relationshipId, DateTime timeCreated)
        {
            const string sql = "INSERT INTO PatientContactPersonRelationshipEnded (PatientContactPersonRelationshipId, TimeCreated) VALUES (@RelationshipId, @TimeCreated); "
                                 + SelectResultModelSql + "WHERE PatientContactPersonRelationship.Id = @RelationshipId";

            return QueryResultModels(sql, new
            {
                TimeCreated = timeCreated,
                RelationshipId = relationshipId
            }).Single();
        }

        public IEnumerable<PatientContactPersonRelationshipResultModel> FindAll(int patientId)
        {
            const string sql = SelectResultModelSql +
                                "WHERE PatientContactPersonRelationship.PatientId = @PatientId " +
                                "ORDER BY PatientContactPersonRelationship.TimeCreated DESC, PatientContactPersonRelationshipEnded.TimeCreated ASC";

            return QueryResultModels(sql, new
            {
                PatientId = patientId
            });
        }

        public IEnumerable<PatientContactPersonRelationshipResultModel> FindCurrents(int patientId)
        {
            const string sql = SelectResultModelSql +
                                "WHERE PatientContactPersonRelationship.PatientId = @PatientId AND PatientContactPersonRelationshipEnded.PatientContactPersonRelationshipId IS NULL " +
                                "ORDER BY PatientContactPersonRelationship.TimeCreated DESC, PatientContactPersonRelationshipEnded.TimeCreated ASC";

            return QueryResultModels(sql, new
            {
                PatientId = patientId
            });
        }

        private IEnumerable<PatientContactPersonRelationshipResultModel> QueryResultModels(string sql, object parameters)
        {
            return _connection.Query<PatientContactPersonRelationship, Employee, PatientContactPersonRelationshipEnded, PatientContactPersonRelationshipResultModel>(sql,
                    (relationship, employee, relationshipEnded) => new PatientContactPersonRelationshipResultModel()
                    {
                        Id = relationship.Id,
                        PatientId = relationship.PatientId,
                        Employee = employee,
                        TimeStarted = relationship.TimeCreated,
                        TimeEnded = (relationshipEnded != null)
                                        ? (DateTime?)relationshipEnded.TimeCreated
                                        : null
                    }, parameters, splitOn: "Id, PatientContactPersonRelationshipId");
        }

        private const string SelectResultModelSql = "SELECT PatientContactPersonRelationship.*, " +
                                                    "Employee.*, " +
                                                    "PatientContactPersonRelationshipEnded.* " +
                                                    "FROM PatientContactPersonRelationship " +
                                                    "LEFT JOIN PatientContactPersonRelationshipEnded ON (PatientContactPersonRelationshipEnded.PatientContactPersonRelationshipId = PatientContactPersonRelationship.Id) " +
                                                    "JOIN Employee ON (Employee.Id = PatientContactPersonRelationship.EmployeeId) ";

    }
}
