﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Models.Core;
using Dapper;
using DailyReport.Domain.Models;

namespace DailyReport.DataAccess
{
    public class MedicationScheduleItemHistoryRepository : IMedicationScheduleItemHistoryRepository
    {
        private readonly IDbConnection _connection;

        public MedicationScheduleItemHistoryRepository(IDbConnection connection)
        {
            _connection = connection;
        }

        public MedicationScheduleItemHistoryResultModel Insert(MedicationScheduleItemHistory medicationScheduleItemHistory, int employeeId)
        {
            var args = new DynamicParameters();
            args.AddDynamicParams(medicationScheduleItemHistory);
            args.Add("EmployeeId", employeeId);

            const string sql = "DECLARE @@HistoryItemId int; " +
                               "INSERT INTO MedicationScheduleItemHistory (MedicationScheduleItemId, TimeCreated) VALUES (@MedicationScheduleItemId, @TimeCreated); " +
                               "SET @@HistoryItemId = SCOPE_IDENTITY(); " +
                               "INSERT INTO MedicationScheduleItemHistoryEmployee (MedicationScheduleItemHistoryId, EmployeeId) VALUES (@@HistoryItemId, @EmployeeId) " +
                               DefaultSql + " WHERE MedicationScheduleItemHistory.Id = @@HistoryItemId";

            return Query(sql, args).SingleOrDefault();
        }

        public IEnumerable<MedicationScheduleItemHistoryResultModel> FindAll()
        {
            return Query(DefaultSql);
        }

        public IEnumerable<MedicationScheduleItemHistoryResultModel> FindAll(int medicationScheduleItemId)
        {
            var parameters = new
            {
                MedicationScheduleItemId = medicationScheduleItemId
            };

            const string sql = DefaultSql + "WHERE MedicationScheduleItemHistory.MedicationScheduleItemId = @MedicationScheduleItemId " +
                               "ORDER BY MedicationScheduleItemHistory.TimeCreated DESC";

            return Query(sql, parameters);
        }

        public IEnumerable<MedicationScheduleItemHistoryResultModel> FindByPatient(int patientId)
        {
            const string sql = DefaultSql + "WHERE MedicationSchedule.PatientId = @PatientId " +
                               "ORDER BY MedicationScheduleItemHistory.TimeCreated DESC";

            return Query(sql, new { PatientId = patientId });
        }

        private IEnumerable<MedicationScheduleItemHistoryResultModel> Query(string sql, object parameters = null)
        {
            return _connection.Query<MedicationScheduleItemHistoryResultModel, Employee, MedicationResultModel, MedicationBrand, MedicationScheduleItemHistoryResultModel>(sql,
                (medicationScheduleItemHistory, employee, medication, medicationBrand) =>
                {
                    if (employee != null)
                        medicationScheduleItemHistory.Employee = employee;

                    medication.Brand = medicationBrand;

                    medicationScheduleItemHistory.Medication = medication;
                    
                    return medicationScheduleItemHistory;
                }, parameters);
        }

        private const string DefaultSql = "SELECT " +
                                          "MedicationScheduleItemHistory.*, " +
                                          "Employee.*," +
                                          "Medication.*, " +
                                          "MedicationType.Name AS Type, " +
                                          "Measure.Name AS Measure, " +
                                          "MedicationBrand.* " +
                                          "FROM MedicationScheduleItemHistory " +
                                          "JOIN MedicationScheduleItem ON (MedicationScheduleItem.Id = MedicationScheduleItemHistory.MedicationScheduleItemId) " +
                                          "JOIN Medication ON (Medication.Id = MedicationScheduleItem.MedicationId) " +
                                          "JOIN MedicationType ON (MedicationType.Id = Medication.MedicationTypeId) " +
                                          "JOIN Measure ON (Measure.Id = Medication.MeasureId) " +
                                          "JOIN MedicationBrand ON (MedicationBrand.Id = Medication.MedicationBrandId) " +
                                          "JOIN MedicationSchedule ON (MedicationSchedule.Id = MedicationScheduleItem.MedicationScheduleId) " +
                                          "LEFT JOIN MedicationScheduleItemRepeat ON (MedicationScheduleItemRepeat.MedicationScheduleItemId = MedicationScheduleItemHistory.MedicationScheduleItemId) " +
                                          "LEFT JOIN MedicationScheduleItemHistoryEmployee ON (MedicationScheduleItemHistoryEmployee.MedicationScheduleItemHistoryId = MedicationScheduleItemHistory.Id) " +
                                          "LEFT JOIN Employee ON (Employee.Id = MedicationScheduleItemHistoryEmployee.EmployeeId) ";
    }
}
