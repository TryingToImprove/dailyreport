﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using DailyReport.Domain.Models.Core;
using DailyReport.Domain.Infrastructure.Repositories;

namespace DailyReport.DataAccess
{
    public class OrganizationRepository : IOrganizationRepository
    {
        private readonly IDbConnection _connection;

        public OrganizationRepository(IDbConnection connection)
        {
            _connection = connection;
        }

        public IEnumerable<Organization> FindAll()
        {
            return _connection.Query<Organization>("SELECT * FROM Organization");
        }

        public Organization Insert(Organization organization)
        {
            organization.Id = _connection.ExecuteScalar<int>(@"INSERT INTO Organization (Name, TimeCreated) VALUES (@Name, @TimeCreated)
                                                                SELECT CAST(SCOPE_IDENTITY() AS int)", organization);

            return organization;
        }

        public Organization GetById(int organizationId)
        {
            return _connection.Query<Organization>("SELECT * FROM Organization WHERE Id = @Id", new
            {
                Id = organizationId
            }).SingleOrDefault();
        }


        public void Update(Organization organization)
        {
            _connection.Execute(@"UPDATE Organization SET Name = @Name, TimeCreated = @TimeCreated
                                    WHERE Id = @Id", organization);
        }
    }
}
