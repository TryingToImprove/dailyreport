﻿using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Models.Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace DailyReport.DataAccess
{
    public class MedicationScheduleCompleteRepository : IMedicationScheduleCompleteRepository
    {
        private readonly IDbConnection _connection;

        public MedicationScheduleCompleteRepository(IDbConnection connection)
        {
            _connection = connection;
        }

        public MedicationScheduleComplete Insert(MedicationScheduleComplete medicationScheduleComplete)
        {
            _connection.Execute("INSERT INTO MedicationScheduleComplete (MedicationScheduleId, TimeCreated, Description, EmployeeId) VALUES (@MedicationScheduleId, @TimeCreated, @Description, @EmployeeId)", medicationScheduleComplete);

            return medicationScheduleComplete;
        }
    }
}
