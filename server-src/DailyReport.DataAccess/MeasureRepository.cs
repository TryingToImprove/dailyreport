﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Models.Core;
using Dapper;

namespace DailyReport.DataAccess
{
    public class MeasureRepository : IMeasureRepository
    {
        private readonly IDbConnection _connection;

        public MeasureRepository(IDbConnection connection)
        {
            _connection = connection;
        }

        public IEnumerable<Measure> FindAll()
        {
            return _connection.Query<Measure>("SELECT * FROM Measure");
        }
    }
}
