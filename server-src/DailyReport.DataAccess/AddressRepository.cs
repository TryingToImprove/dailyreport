﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.DataAccess.Factories;
using DailyReport.Domain.Infrastructure;
using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using Dapper;

namespace DailyReport.DataAccess
{
    public class AddressRepository : IAddressRepository
    {
        private readonly IDbConnection _connection;

        public AddressRepository(IDbConnection connection)
        {
            _connection = connection;
        }

        public PatientAddressResultModel AddPatientAddress(int patientId, PatientAddressInsertModel model)
        {
            var sqlBuilder = new StringBuilder();

            // If the ClearCurrentPrimary flag is true, we need to disable current primaries
            if (model.ClearCurrentPrimary)
                sqlBuilder.Append("UPDATE Patient_Address SET IsPrimary = 0 WHERE PatientId = @PatientId; ");

            // Add the insert sql
            sqlBuilder.Append(DefaultInsertSql +
                              "INSERT INTO Patient_Address (PatientId, AddressId, TimeCreated, IsPrimary, IsActive) VALUES (@PatientId, @@AddressId, CURRENT_TIMESTAMP, @IsPrimary, 1); " +
                              DefaultPatientSelectSql + "WHERE Address.Id = @@AddressId");

            // Set the parameters
            var args = new DynamicParameters();
            args.AddDynamicParams(model);
            args.Add("PatientId", patientId);

            return Query(sqlBuilder.ToString(), args).Single();
        }

        public IEnumerable<PatientAddressResultModel> FindAllByPatient(int patientId)
        {
            const string sql = DefaultPatientSelectSql +
                               "WHERE Patient_Address.PatientId = @PatientId AND Patient_Address.IsActive = 1";

            return Query(sql, new { PatientId = patientId });
        }

        public void RemovePatientAddress(int patientId, int addressId)
        {
            const string sql = "UPDATE Patient_Address SET IsActive = 0 WHERE PatientId = @PatientId AND AddressId = @AddressId";

            _connection.Execute(sql, new { PatientId = patientId, AddressId = addressId });
        }

        public PatientAddressResultModel UpdatePatientAddress(int patientId, int addressId, PatientAddressUpdateModel model)
        {
            var updateAddressFactory = new UpdateAddressFactory();
            var sql = updateAddressFactory.Create(patientId, addressId, model) +
                                " " + DefaultPatientSelectSql +
                               "WHERE Patient_Address.PatientId = @PatientId AND Patient_Address.AddressId = @AddressId; ";

            var args = new DynamicParameters();
            args.AddDynamicParams(model);
            args.Add("PatientId", patientId);
            args.Add("AddressId", addressId);

            return Query(sql, args).SingleOrDefault();
        }

        public IEnumerable<PatientAddressResultModel> FindPrimaryPatient(int patientId)
        {
            const string sql = DefaultPatientSelectSql +
                               " WHERE Patient_Address.PatientId = @PatientId AND Patient_Address.IsPrimary = 1";

            return Query(sql, new { PatientId = patientId });
        }

        public PatientAddressResultModel FindByIdPatient(int patientId, int addressId)
        {
            const string sql = DefaultPatientSelectSql +
                               " WHERE Patient_Address.PatientId = @PatientId AND Patient_Address.AddressId = @AddressId";

            return Query(sql, new { PatientId = patientId, AddressId = addressId }).SingleOrDefault();
        }

        private IEnumerable<PatientAddressResultModel> Query(string sql, object args = null)
        {
            return _connection.Query<PatientAddressResultModel>(sql, args);
        }

        private const string DefaultPatientSelectSql = "SELECT Address.Id, Address.Street, Address.HouseNumber, Address_PostalCode.PostalCode, Town.Name AS City, Patient_Address.IsPrimary FROM Address " +
                                                       "JOIN Town ON (Town.Id = Address.TownId) " +
                                                       "LEFT JOIN Address_PostalCode ON (Address_PostalCode.AddressId = Address.Id) " +
                                                       "JOIN Patient_Address ON (Patient_Address.AddressId = Address.Id) ";

        private const string DefaultInsertSql = @"DECLARE @@AddressId int, @@CityId int;
 
                                                -- Create city if city not exists
                                                SET @@CityId = (SELECT TOP 1 Town.Id FROM Town WHERE Town.Name = @City);

                                                IF @@CityId IS NULL BEGIN
	                                                INSERT INTO Town (Name) VALUES (@City);
                                                    SET @@CityId = SCOPE_IDENTITY();
                                                END

                                                -- Create postalCode to city if postalCode not exists
                                                IF NOT EXISTS (
                                                    SELECT 1 FROM TownPostalcode WHERE TownPostalcode.TownId = @@CityId AND TownPostalcode.Postalcode = @PostalCode
                                                ) BEGIN
	                                                INSERT INTO TownPostalcode (TownId, Postalcode) VALUES (@@CityId, @PostalCode);
                                                END

                                                -- Create the address
                                                INSERT INTO Address (Street, HouseNumber, TownId) VALUES (@Street, @HouseNumber, @@CityId)

                                                SET @@AddressId = SCOPE_IDENTITY(); 

                                                INSERT INTO Address_PostalCode (AddressId, PostalCode) VALUES (@@AddressId, @PostalCode); ";
    }
}
