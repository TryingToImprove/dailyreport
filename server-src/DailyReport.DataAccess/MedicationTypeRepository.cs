﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Models.Core;
using Dapper;

namespace DailyReport.DataAccess
{
    public class MedicationTypeRepository : IMedicationTypeRepository
    {
        private readonly IDbConnection _connection;

        public MedicationTypeRepository(IDbConnection connection)
        {
            _connection = connection;
        }

        public IEnumerable<MedicationType> FindAll()
        {
            return _connection.Query<MedicationType>("SELECT * FROM MedicationType");
        }
    }
}
