﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Models;

namespace DailyReport.DataAccess.Factories
{
    internal class UpdateAddressFactory
    {
        public string Create(int patientId, int addressId, PatientAddressUpdateModel model)
        {
            return BuildAddressUpdateStatement(model, "WHERE Id = @AddressId");
        }

        private static string BuildAddressUpdateStatement(PatientAddressUpdateModel model, string filter)
        {
            var statements = new List<string>()
            {
                "DECLARE @@CityId int;"
            };
            
            var addressUpdates = new List<string>();

            if (model.Street != null)
                addressUpdates.Add("Address.Street = @Street");

            if (model.HouseNumber != null)
                addressUpdates.Add("Address.HouseNumber = @HouseNumber");

            if (model.City != null)
            {
                statements.Add(@"SET @@CityId = (SELECT TOP 1 Town.Id FROM Town WHERE Town.Name = @City);

                               IF @@CityId IS NULL BEGIN
	                               INSERT INTO Town (Name) VALUES (@City);
                                   SET @@CityId = SCOPE_IDENTITY();
                               END");

                addressUpdates.Add("Address.TownId = @@CityId");

                if (model.PostalCode != null)
                {
                    statements.Add(@"IF NOT EXISTS (
                                        SELECT 1 FROM TownPostalcode WHERE TownPostalcode.TownId = @@CityId AND TownPostalcode.Postalcode = @PostalCode
                                    ) BEGIN
	                                    INSERT INTO TownPostalcode (TownId, Postalcode) VALUES (@@CityId, @PostalCode);
                                    END");
                }
            }
            else if (model.PostalCode != null)
            {
                statements.Add(@"SET @@CityId = (SELECT TOP 1 Town.Id FROM Town WHERE Town.Name = @City);
                                     
                                IF NOT EXISTS (
                                    SELECT 1 FROM TownPostalcode WHERE TownPostalcode.TownId = @@CityId AND TownPostalcode.Postalcode = @PostalCode
                                ) BEGIN
	                                INSERT INTO TownPostalcode (TownId, Postalcode) VALUES (@@CityId, @PostalCode);
                                END");
            }

            if (model.PostalCode != null)
            {
                statements.Add("UPDATE Address_PostalCode SET PostalCode = @PostalCode WHERE AddressId = @AddressId");
            }

            if (model.ClearCurrentPrimary)
            {
                statements.Add("UPDATE Patient_Address SET IsPrimary = 0 WHERE PatientId = @PatientId");
            }

            if (model.IsPrimary.HasValue && model.IsPrimary.Value)
            {
                statements.Add("UPDATE PAtient_Address SET IsPrimary = 1 WHERE PatientId = @PatientId AND AddressId = @AddressId");
            }

            if (addressUpdates.Any())
                statements.Add(string.Format("UPDATE Address SET {0} {1};", Join(addressUpdates, "{0}"), filter));

            return Join(statements, "{0};", "\n\n--end-statement\n");
        }

        private static string Join(IEnumerable<string> addressUpdates, string format = "{0}", string seperator = ", ")
        {
            return string.Join(seperator, addressUpdates.Select(x => string.Format(format, x)));
        }
    }
}
