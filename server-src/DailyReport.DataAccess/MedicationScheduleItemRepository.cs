﻿using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Models.Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace DailyReport.DataAccess
{
    public class MedicationScheduleItemRepository : IMedicationScheduleItemRepository
    {
        private readonly IDbConnection _connection;

        public MedicationScheduleItemRepository(IDbConnection connection)
        {
            _connection = connection;
        }

        public MedicationScheduleItem Insert(MedicationScheduleItem medicationScheduleItem)
        {
            medicationScheduleItem.Id = _connection.ExecuteScalar<int>(@"INSERT INTO MedicationScheduleItem (MedicationScheduleId, MedicationId, ExecuteTime, Amouth) VALUES (@MedicationScheduleId, @MedicationId, @ExecuteTime, @Amouth)
                                                                         SELECT SCOPE_IDENTITY()", medicationScheduleItem);

            return medicationScheduleItem;
        }


        public void CreateRepeatInterval(int medicationScheduleItemId, TimeSpan repeatInterval)
        {
            _connection.Execute("INSERT INTO MedicationScheduleItemRepeat (MedicationScheduleItemId, RepeatTimeSpan) VALUES (@MedicationScheduleItemId, @RepeatTimeSpan)", new
            {
                MedicationScheduleItemId = medicationScheduleItemId,
                RepeatTimeSpan = repeatInterval.Ticks
            });
        }
    }
}
