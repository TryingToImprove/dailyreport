﻿using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace DailyReport.DataAccess
{
    public class PatientRepository : IPatientRepository
    {
        private readonly IDbConnection _connection;

        public PatientRepository(IDbConnection connection)
        {
            _connection = connection;
        }

        public IEnumerable<Patient> FindAll(int organizationId)
        {
            return _connection.Query<Patient>(@"SELECT * FROM Patient WHERE OrganizationId = @OrganizationId AND IsActive = @IsActive", new
              {
                  IsActive = true,
                  OrganizationId = organizationId
              });
        }


        public IEnumerable<Patient> FindByEmployee(int employeeId)
        {
            const string sql = "SELECT Patient.* FROM Patient " +
                               "JOIN PatientContactPersonRelationship ON (PatientContactPersonRelationship.PatientId = Patient.Id) " +
                               "WHERE PatientContactPersonRelationship.EmployeeId = @EmployeeId AND Patient.IsActive = @IsActive AND NOT EXISTS (SELECT PatientContactPersonRelationshipId FROM PatientContactPersonRelationshipEnded WHERE PatientContactPersonRelationshipId = PatientContactPersonRelationship.Id) ";

            return _connection.Query<Patient>(sql, new
            {
                IsActive = true,
                EmployeeId = employeeId
            });
        }

        public PatientResultModel Insert(Patient patient)
        {
            const string sql = "INSERT INTO Patient (OrganizationId, Firstname, Lastname, TimeCreated, BirthDate, IsActive) VALUES (@OrganizationId, @Firstname, @Lastname, @TimeCreated, @BirthDate, @IsActive); " +
                                DefaultSelectSql + " WHERE Patient.Id = SCOPE_IDENTITY()";

            return Query(sql, patient).SingleOrDefault();
        }

        public void Deactivate(int patientId)
        {
            _connection.Execute("UPDATE Patient SET IsActive = 0 WHERE Id = @PatientId", new
            {
                PatientId = patientId
            });
        }

        public PatientResultModel GetById(int patientId)
        {
            const string sql = DefaultSelectSql + "WHERE Patient.Id = @PatientId";

            return Query(sql, new
            {
                PatientId = patientId
            }).SingleOrDefault();
        }

        public PatientResultModel Update(int patientId, PatientUpdateModel model)
        {
            const string sql = "UPDATE Patient SET Firstname = @Firstname, Lastname = @Lastname WHERE Id = @PatientId; " +
                               @"IF (@SecurityNumber IS NOT NULL) BEGIN
                                        IF EXISTS (SELECT 1 FROM PatientSecurityNumber WHERE PatientId = @PatientId)
                                            UPDATE PatientSecurityNumber SET SecurityNumber = @SecurityNumber WHERE PatientId = @PatientId;
                                        ELSE
                                            INSERT INTO PatientSecurityNumber (PatientId, SecurityNumber) VALUES (@PatientId, @SecurityNumber);
                                     END 
                                ELSE
                                    DELETE FROM PatientSecurityNumber WHERE PatientId =  @PatientId " + 
                                DefaultSelectSql + "WHERE Patient.Id = @PatientId ";

            var args = new DynamicParameters();
            args.Add("Firstname", model.Firstname);
            args.Add("Lastname", model.Lastname);
            args.Add("PatientId", patientId);
            args.Add("SecurityNumber", model.SecurityNumber);
   
            return Query(sql, args).SingleOrDefault();
        }

        private IEnumerable<PatientResultModel> Query(string sql, object parameters)
        {
            return _connection.Query<PatientResultModel, Description, PatientResultModel>(sql, (patient, description) =>
            {
                patient.Description = description;
                return patient;
            }, parameters);
        }

        private const string DefaultSelectSql = @"SELECT 
	                                                Patient.*,
	                                                Description.* 
                                                FROM Patient

                                                OUTER APPLY (
	                                                SELECT TOP 1 * FROM Description
	                                                JOIN PatientDescription ON (PatientDescription.DescriptionId = Description.Id)
	                                                WHERE PatientDescription.PatientId = Patient.Id
                                                    ORDER BY Description.TimeCreated DESC
                                                ) AS Description ";
    }
}
