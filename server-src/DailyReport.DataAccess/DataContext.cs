﻿using DailyReport.Domain.Infrastructure;
using DailyReport.Domain.Infrastructure.Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace DailyReport.DataAccess
{
    public class DataContext : IDataContext
    {
        private readonly IDbConnection _connection;

        public IEmployeeRepository Employees { get; set; }

        public IAuthenticationIdentityRepository AuthenticationIdentites { get; set; }

        public IOrganizationRepository Organizations { get; set; }

        public IPatientRepository Patients { get; set; }

        public IPatientPostRepository PatientPosts { get; set; }

        public IPatientContactPersonRelationshipRepository PatientContactPersonRelationships { get; set; }

        public IPatientToPatientPostRepository PatientPostToPatients { get; set; }

        public IDescriptionRepository Descriptions { get; set; }

        public IAuthenticationLogRepository AuthenticationLogs { get; set; }

        public IMedicationBrandRepository MedicationBrands { get; set; }

        public IMedicationRepository Medications { get; set; }

        public IMedicationTypeRepository MedicationTypes { get; set; }

        public IMedicationScheduleRepository MedicationSchedules { get; set; }

        public IMedicationScheduleItemRepository MedicationScheduleItems { get; set; }

        public IMedicationScheduleItemHistoryRepository MedicationScheduleItemHistories { get; set; }

        public IMedicationScheduleCompleteRepository MedicationScheduleCompletes { get; set; }

        public IMeasureRepository Measures { get; set; }

        public IContactInformationRepository ContactInformations { get; set; }

        public IContactInformationTypeRepository ContactInformationTypes { get; set; }

        public IContactRepository Contacts { get; set; }

        public IContactGroupRepository ContactGroups { get; set; }

        public ITimelineRepository TimelineEntries { get; set; }

        public IAddressRepository Addresses { get; set; }

        public DataContext(IDbConnection connection)
        {
            _connection = connection;
        }

        public IDataContextScope BeginTransaction()
        {
            return new DataContextScope(_connection);
        }

        public class DataContextScope : IDataContextScope
        {
            private readonly IDbConnection _connection;
            private readonly TransactionScope _transaction;

            public DataContextScope(IDbConnection connection)
            {
                _connection = connection;

                _transaction = new TransactionScope(TransactionScopeOption.Required);
            }

            public void Dispose()
            {
                if (Marshal.GetExceptionCode() == 0)
                    _transaction.Complete();

                _connection.Close();
                _transaction.Dispose();
            }
        }
    }
}
