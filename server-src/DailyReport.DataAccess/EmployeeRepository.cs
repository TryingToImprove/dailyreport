﻿using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace DailyReport.DataAccess
{
    public class EmployeeRepository : IEmployeeRepository
    {
        private readonly IDbConnection _connection;

        public EmployeeRepository(IDbConnection connection)
        {
            _connection = connection;
        }

        public Employee Insert(EmployeeInsertModel model, string authenticationKey)
        {
            var args = new DynamicParameters();
            args.AddDynamicParams(model);
            args.Add("AuthenticationKey", authenticationKey);
            
            const string sql = "DECLARE @@EmployeeId int; " +
                               "INSERT INTO Employee (Firstname, Lastname, OrganizationId, TimeCreated, Email, IsActive) VALUES (@Firstname, @Lastname, @OrganizationId, @TimeCreated, @Email, 1); " +
                               "SET @@EmployeeId = SCOPE_IDENTITY(); " +
                               "INSERT INTO EmployeeAuthenticationIdentityKey (EmployeeId, AuthenticationKey, TimeCreated) VALUES (@@EmployeeId, @AuthenticationKey, @TimeCreated)" +
                               DefaultSelectSql + " WHERE Employee.Id = @@EmployeeId";

            return _connection.Query<EmployeeResultModel>(sql, args).SingleOrDefault();
        }

        public IEnumerable<Employee> FindAll(int organizationId)
        {
            var parmeters = new
            {
                OrganizationId = organizationId
            };
            
            return _connection.Query<Employee>(@"SELECT * FROM Employee 
                                                WHERE OrganizationId = @OrganizationId AND IsActive = 1", parmeters);
        }

        public EmployeeResultModel GetById(int employeeId)
        {
            const string sql = DefaultSelectSql + "WHERE Id = @Id";

            return _connection.Query<EmployeeResultModel>(sql, new { Id = employeeId }).SingleOrDefault();
        }

        public EmployeeResultModel GetBySecret(string secret)
        {
            const string sql = DefaultSelectSql +
                               "JOIN EmployeeAuthenticationIdentityKey ON (EmployeeAuthenticationIdentityKey.EmployeeId = Employee.Id) " +
                               "WHERE EmployeeAuthenticationIdentityKey.AuthenticationKey = @Secret";

            return _connection.Query<EmployeeResultModel>(sql, new { Secret = secret }).SingleOrDefault();
        }

        public void Update(Employee employee)
        {
            _connection.Execute(@"UPDATE Employee SET
                                    Firstname = @Firstname,
                                    Lastname = @Lastname,
                                    TimeCreated = @TimeCreated,
                                    OrganizationId = @OrganizationId
                                WHERE Id = @Id", employee);
        }

        public void UpdateActiveState(int employeeId, bool isActive)
        {
            _connection.Execute(@"UPDATE Employee SET IsActive = @IsActive WHERE Id = @Id", new { Id = employeeId, IsActive = isActive });
        }

        private const string DefaultSelectSql = @"SELECT 
	                                                Employee.*,
                                                    NULL AS LastAuthenticationDateTime

                                                FROM Employee ";
    }
}
