﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;
using Dapper;

namespace DailyReport.DataAccess
{
    public class TimelineRepository : ITimelineRepository
    {
        private readonly IDbConnection _connection;

        public TimelineRepository(IDbConnection connection)
        {
            _connection = connection;
        }

        public IEnumerable<TimelineEntry> FindByPatient(int patientId)
        {
            const string sql = @"WITH ContactPersonEntries AS (
	                                SELECT 
		                                PatientContactPersonRelationship.Id AS EntryId,
		                                PatientContactPersonRelationship.TimeCreated EntryDateTime,
		                                'ContactPerson' AS EntryKey,
		                                '{ 
			                                employee: { 
				                                id: ' + CAST(Employee.Id AS NVARCHAR)  + ', 
				                                firstname: ""' + Employee.Firstname + '"",
				                                lastname: ""' + Employee.Lastname + '"" 
			                                }
		                                 }' AS EntryData
	                                FROM PatientContactPersonRelationship 
	                                JOIN Employee ON (Employee.Id = PatientContactPersonRelationship.EmployeeId)

                                    -- Filter    
                                    WHERE PatientContactPersonRelationship.PatientId = @PatientId
                                ),
                                ContactPersonEndedEntries AS (
	                                SELECT 
		                                PatientContactPersonRelationship.Id AS EntryId,
		                                PatientContactPersonRelationshipEnded.TimeCreated EntryDateTime,
		                                'ContactPersonEnded' AS EntryKey,
		                                '{ 
			                                employee: { 
				                                id: ' + CAST(Employee.Id AS NVARCHAR)  + ', 
				                                firstname: ""' + Employee.Firstname + '"",
				                                lastname: ""' + Employee.Lastname + '"" 
			                                }
		                                 }' AS EntryData
	                                FROM PatientContactPersonRelationshipEnded
	                                JOIN PatientContactPersonRelationship ON (PatientContactPersonRelationship.Id = PatientContactPersonRelationshipEnded.PatientContactPersonRelationshipId)
	                                JOIN Employee ON (Employee.Id = PatientContactPersonRelationship.EmployeeId)

                                    -- Filter    
                                    WHERE PatientContactPersonRelationship.PatientId = @PatientId
                                ),
                                MedicationSchedulesEntries AS (
	                                SELECT 
		                                MedicationSchedule.Id AS EntryId,
		                                MedicationSchedule.TimeCreated EntryDateTime,
		                                'MedicationSchedule' AS EntryKey,
		                                '{ 
			                                medications: ['+ STUFF((SELECT ', {
                                                                                id: ' + CAST(MedicationScheduleItem.ID AS NVARCHAR) + ',
											                                    medication: {
												                                    id: ' + CAST(Medication.Id AS NVARCHAR) + ',
												                                    name: ""' + Medication.Name + '""
											                                    },
											                                    brand: {
												                                    id: ' + CAST(MedicationBrand.Id AS nvarchar) + ',
												                                    name: ""' + MedicationBrand.Name + '""
											                                    },
											                                    amouth: ' + CAST(MedicationScheduleItem.Amouth AS NVARCHAR) + ',
											                                    executeTime: ""' + CAST(MedicationScheduleItem.ExecuteTime AS NVARCHAR) + '""
										                                    }' AS EntryData FROM MedicationScheduleItem
				
								                                    JOIN Medication ON (Medication.Id = MedicationScheduleItem.MedicationId)
								                                    JOIN MedicationBrand ON (MedicationBrand.Id = Medication.MedicationBrandId)

								                                    WHERE MedicationScheduleItem.MedicationScheduleId = MedicationSchedule.Id
								                                    FOR XML PATH(''), TYPE).value('.','VARCHAR(max)'), 1, 1, '') + ']
		                                 }' AS EntryData
	                                FROM MedicationSchedule 

                                    -- Filter    
                                    WHERE MedicationSchedule.PatientId = @PatientId
                                ),
                                MedicationSchedulesCompletedEntries AS (
	                                SELECT 
		                                MedicationSchedule.Id AS EntryId,
		                                MedicationScheduleComplete.TimeCreated EntryDateTime,
		                                'MedicationSchedulesComplete' AS EntryKey,
		                                '{}' AS EntryData
	                                FROM MedicationScheduleComplete
                                    JOIN MedicationSchedule ON (MedicationSchedule.Id = MedicationScheduleComplete.MedicationScheduleId)

                                    -- Filter    
                                    WHERE MedicationSchedule.PatientId = @PatientId
                                ),
                                PatientPostEntries AS (
                                    SELECT
	                                    PatientPost.Id AS EntryId,
	                                    PatientPost.TimeCreated EntryDateTime,
	                                    'PatientPost' AS EntryKey,
	                                    '{ 
		                                    employee: { 
			                                    id: ' + CAST(Employee.Id AS NVARCHAR)  + ', 
			                                    firstname: ""' + Employee.Firstname + '"",
			                                    lastname: ""' + Employee.Lastname + '"" 
		                                    },
		                                    description: ""' + CAST(PatientPost.Description AS NVARCHAR(MAX)) + '""
	                                    }' AS EntryData
                                    FROM PatientPost
                                    JOIN Employee ON (Employee.Id = PatientPost.EmployeeId)
                                    WHERE EXISTS (SELECT 1 FROM PatientPost_Patient WHERE PatientPost_Patient.PatientId = @PatientId AND PatientPost_Patient.PatientPostId = PatientPost.Id)
                                ),
                                Entries AS (
	                                SELECT * FROM ContactPersonEntries UNION ALL
	                                SELECT * FROM ContactPersonEndedEntries UNION ALL
	                                SELECT * FROM MedicationSchedulesEntries UNION ALL
	                                SELECT * FROM MedicationSchedulesCompletedEntries UNION ALL
                                    SELECT * FROM PatientPostEntries
                                )

                                SELECT * FROM Entries 
                                WHERE EntryData IS NOT NULL 
                                ORDER BY EntryDateTime DESC";

            return _connection.Query<TimelineEntry>(sql, new { PatientId = patientId });
        }

        
    }

}
