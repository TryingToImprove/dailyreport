﻿using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Models.Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace DailyReport.DataAccess
{
    public class DescriptionRepository : IDescriptionRepository
    {
        private readonly IDbConnection _connection;

        public DescriptionRepository(IDbConnection connection)
        {
            _connection = connection;
        }

        public void InsertPatientDescription(int patientId, Description description)
        {
            Insert(new InsertDescriptionModel
            {
                ReferenceScript = "INSERT INTO PatientDescription (PatientId, DescriptionId) VALUES (@PatientId, SCOPE_IDENTITY());",
                Description = description,
                Parameters = new
                {
                    PatientId = patientId
                }
            });

        }

        public IEnumerable<Description> GetPatientDescriptions(int patientId)
        {
            const string sql = @"SELECT Description.* FROM Description
                                 JOIN PatientDescription ON (PatientDescription.DescriptionId = Description.Id)
                                 WHERE PatientDescription.PatientId = @PatientId
                                 ORDER BY Description.TimeCreated DESC";

            return _connection.Query<Description>(sql, new { PatientId = patientId });
        }

        private void Insert(InsertDescriptionModel model)
        {
            _connection.Execute(model.Sql, model.Arguments);
        }

        private class InsertDescriptionModel
        {
            public string ReferenceScript { private get; set; }

            public object Parameters { private get; set; }

            public Description Description { private get; set; }

            public object Arguments
            {
                get
                {
                    var args = new DynamicParameters();
                    args.AddDynamicParams(Description);
                    args.AddDynamicParams(Parameters);

                    return args;
                }
            }

            public string Sql
            {
                get
                {
                    return "INSERT INTO Description (EmployeeId, TimeCreated, Text) VALUES (@EmployeeId, @TimeCreated, @Text); "
                            + ReferenceScript;
                }
            }
        }
    }
}
