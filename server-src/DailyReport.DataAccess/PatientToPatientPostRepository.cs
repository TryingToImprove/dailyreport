﻿using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Models.Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace DailyReport.DataAccess
{
    public class PatientToPatientPostRepository : IPatientToPatientPostRepository
    {
        private readonly IDbConnection _connection;

        public PatientToPatientPostRepository(IDbConnection connection)
        {
            _connection = connection;
        }

        public void Insert(int patientId, int patientPostId)
        {
            _connection.Execute("INSERT INTO PatientPost_Patient (PatientId, PatientPostId) VALUES (@PatientId, @PatientPostId)", new PatientPostReference(patientId, patientPostId));
        }

        public void Remove(int patientId, int patientPostId)
        {
            _connection.Execute("DELETE FROM PatientPost_Patient WHERE PatientId = @PatientId AND PatientPostId = @PatientPostId", new PatientPostReference(patientId, patientPostId));
        }

        public bool Contains(int patientId, int patientPostId)
        {
            return _connection.ExecuteScalar<bool>(@"SELECT (CASE WHEN EXISTS (SELECT * FROM PatientPost_Patient WHERE PatientPostId = @PatientPostId AND PatientId = @PatientId)
			                                            THEN 1
			                                            ELSE 0
		                                            END) AS IsFound", new PatientPostReference(patientId, patientPostId));
        }
    }
}
