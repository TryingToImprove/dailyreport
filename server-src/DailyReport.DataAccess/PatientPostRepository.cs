﻿using System.Data.SqlClient;
using DailyReport.Domain.Infrastructure.Repositories;
using DailyReport.Domain.Models.Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using DailyReport.Domain.Models;

namespace DailyReport.DataAccess
{
    public class PatientPostRepository : IPatientPostRepository
    {
        private readonly IDbConnection _connection;

        public PatientPostRepository(IDbConnection connection)
        {
            _connection = connection;
        }

        public PatientPost Insert(PatientPost patientPost)
        {
            patientPost.Id = _connection.ExecuteScalar<int>(@"INSERT INTO PatientPost (OrganizationId, EmployeeId, TimeCreated, Description) VALUES (@OrganizationId, @EmployeeId, @TimeCreated, @Description);
                                                            SELECT SCOPE_IDENTITY();", patientPost);

            return patientPost;
        }

        public IEnumerable<PatientPostResultModel> FindAll(int organizationId)
        {
            SqlBuilder sqlBuilder;
            var query = GetPatientPostResultModelSql(out sqlBuilder);
            sqlBuilder.Where("PatientPost.OrganizationId = @OrganizationId", new { OrganizationId = organizationId });
            sqlBuilder.OrderBy("PatientPost.TimeCreated DESC");

            return QueryPatientPostResultModels(query);
        }

        public IEnumerable<PatientPostResultModel> FindAllByPatient(int patientId)
        {
            SqlBuilder sqlBuilder;
            var query = GetPatientPostResultModelSql(out sqlBuilder);
            sqlBuilder.Where("@PatientId IN (SELECT ppp.PatientId FROM PatientPost_Patient ppp WHERE ppp.PatientPostId = PatientPost.Id)", new { PatientId = patientId });
            sqlBuilder.OrderBy("PatientPost.TimeCreated DESC");

            return QueryPatientPostResultModels(query);
        }

        private IEnumerable<PatientPostResultModel> QueryPatientPostResultModels(SqlBuilder.Template query)
        {
            var results = new Dictionary<int, PatientPostResultModel>();

            _connection.Query<PatientPost, Employee, Patient, PatientPostResultModel>(query.RawSql, (x, y, p) =>
            {
                PatientPostResultModel viewModel;
                if (!results.TryGetValue(x.Id, out viewModel))
                {
                    results.Add(x.Id, new PatientPostResultModel
                    {
                        Employee = y,
                        PatientPost = x,
                        Patients = new List<Patient>() { p }
                    });
                }
                else
                {
                    viewModel.Patients.Add(p);
                }
                return viewModel;
            }, query.Parameters);

            return results.Values;
        }

        private static SqlBuilder.Template GetPatientPostResultModelSql(out SqlBuilder builder)
        {
            builder = new SqlBuilder();

            var query = builder.AddTemplate(@"SELECT 
                                                PatientPost.*,
                                                Employee.*,
                                                Patient.*

                                            FROM PatientPost 
                                            JOIN Employee ON (Employee.Id = PatientPost.EmployeeId)
                                            LEFT JOIN PatientPost_Patient ON (PatientPost_Patient.PatientPostId = PatientPost.Id)
                                            LEFT JOIN Patient ON (Patient.Id = PatientPost_Patient.PatientId)
                                            /**where**/ /**orderby**/");
            return query;
        }
    }
}
