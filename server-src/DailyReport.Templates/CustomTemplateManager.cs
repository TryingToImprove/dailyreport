﻿using System;
using System.IO;
using System.Reflection;
using RazorEngine.Templating;

namespace DailyReport.Templating
{
    public class CustomTemplateManager : ITemplateManager
    {
        private const string TemplatePathFormat = "DailyReport.Templating.Templates.{0}.cshtml";

        private readonly Assembly _templateAssembly;

        public CustomTemplateManager()
        {
            _templateAssembly = Assembly.GetExecutingAssembly();
        }

        public ITemplateSource Resolve(ITemplateKey key)
        {
            var path = string.Format(TemplatePathFormat, key.Name);

            using (var stream = _templateAssembly.GetManifestResourceStream(path))
            {
                if (stream == null)
                    throw new FileNotFoundException("Could not locate embedded resource", path);

                using (var reader = new StreamReader(stream))
                {
                    var template = reader.ReadToEnd();

                    return new LoadedTemplateSource(template);
                }
            }
        }

        public ITemplateKey GetKey(string name, ResolveType resolveType, ITemplateKey context)
        {
            return new NameOnlyTemplateKey(name, resolveType, context);
        }

        public void AddDynamic(ITemplateKey key, ITemplateSource source)
        {
            throw new NotImplementedException("dynamic templates are not supported!");
        }
    }
}