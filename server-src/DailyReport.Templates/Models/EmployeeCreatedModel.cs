﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyReport.Domain.Models.Core;
using RazorEngine.Templating;

namespace DailyReport.Templating.Models
{
    public class EmployeeCreatedModel
    {
        public string Username { get; set; }

        public DateTime TimeCreated { get; set; }

        public string CompleteRegistrationUrl { get; set; }

        public Employee Employee { get; set; }
    }
}
