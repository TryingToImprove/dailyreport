﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RazorEngine.Templating;

namespace DailyReport.Templating.Models
{
    public class ForgotPasswordModel
    {
        public string ResetPasswordUrl { get; set; }
    }
}
