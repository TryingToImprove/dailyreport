using DailyReport.Domain.Models;
using DailyReport.Domain.Models.Core;

namespace DailyReport.Templating.Models
{
    public class AddedAsContactPersonModel
    {
        public Employee Employee { get; set; }

        public PatientResultModel Patient { get; set; }
    }
}