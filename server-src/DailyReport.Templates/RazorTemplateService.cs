﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RazorEngine;
using RazorEngine.Configuration;
using RazorEngine.Templating;

namespace DailyReport.Templating
{
    public class RazorTemplateService : Domain.Infrastructure.Services.ITemplateService
    {
        private readonly IRazorEngineService _razorEngineService;

        public RazorTemplateService()
        {
            _razorEngineService = RazorEngineService.Create(new TemplateServiceConfiguration
            {
                Debug = true,
                TemplateManager = new CustomTemplateManager()
            });
        }

        public string Render<T>(string templateName, T viewModel) where T : new()
        {
            return _razorEngineService.RunCompile(templateName, null, viewModel);
        }
    }
}
